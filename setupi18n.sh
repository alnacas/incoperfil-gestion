function i18n_msgfmt() {
	for f in "${1}"/*; do
		if [ -d "${f}" ]; then
			i18n_msgfmt "${f}"
		else
			if [[ "${f}" =~ \.po$ ]]; then
				echo "PO file detected ${f}"
				msgfmt "${f}" -o "${f/.po/.mo}" -v
			fi
		fi
	done
}

echo "--> Actualizando los ficheros de traducción"
i18n_dir=$PWD/PHP/i18n
i18n_msgfmt "${i18n_dir}"
