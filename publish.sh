# ./setupi18n.sh
./setupmaster.sh
echo "--> Añadiendo cambios..."
git add .
echo "--> Realizando commit..."
git commit -m "$1"
echo "--> Haciendo el push a ${2}..."
git push origin $2
echo "--> Cambiando a rama ${3}..."
git checkout $3
echo "--> Uniando rama ${2} con ${3}..."
git merge -X theirs --no-edit $2
echo "--> Haciendo el push a ${3}..."
git push origin $3
echo "--> Cambiando a rama ${2}..."
git checkout $2
./setupdev.sh
