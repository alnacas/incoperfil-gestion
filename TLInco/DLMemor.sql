{
    REPOSITORY: TLinco
    LOCATION: c:\AplCos\TLInco\TLInco.CRF
    DATE: 05/05/2011 TIME: 19:46:59.
}
CREATE DATABASE bdlector
;

{ 

    TABLE: paquetes
}
CREATE TABLE paquetes (
    fichero                          CHAR(5)      NOT NULL UPSHIFT,
    norden                           INTEGER      NOT NULL,
    origen                           INTEGER      NOT NULL,
    cbarra                           CHAR(30)     NOT NULL,
    hojas                            SMALLINT     NOT NULL DEFAULT 0,
    largo                            SMALLINT     NOT NULL DEFAULT 0,
    ancho                            SMALLINT     NOT NULL DEFAULT 0,
    metros                           DECIMAL(7,3) NOT NULL DEFAULT 0,
    etiqueta                         SMALLINT    ,
    id_traspaso                      INTEGER      NOT NULL DEFAULT 0
    )
PRIMARY KEY (fichero,norden)
;
CREATE INDEX paque_cbarra ON paquetes (cbarra);
{ 

    TABLE: calidades
}
CREATE TABLE calidades (
    codigo                           INTEGER      NOT NULL,
    nombre                           CHAR(10)    
    )
PRIMARY KEY (codigo)
;
{ 

    TABLE: caracteristicas
}
CREATE TABLE caracteristicas (
    codigo                           INTEGER      NOT NULL,
    nombre                           CHAR(10)    
    )
PRIMARY KEY (codigo)
;
{ 

    TABLE: medidas
}
CREATE TABLE medidas (
    codigo                           INTEGER      NOT NULL,
    nombre                           CHAR(10)    
    )
PRIMARY KEY (codigo)
;
{ 

    TABLE: calificacion
}
CREATE TABLE calificacion (
    codigo                           INTEGER      NOT NULL,
    nombre                           CHAR(30)    
    )
PRIMARY KEY (codigo)
;
{ 

    TABLE: dlmparametros
}
CREATE TABLE dlmparametros (
    codigo                           SMALLINT     NOT NULL,
    identificacion                   CHAR(3)      NOT NULL UPSHIFT,
    ip_server                        CHAR(20)    ,
    dir_server                       CHAR(50)     LABEL "Directorio Servidor",
    dir_traspaso                     CHAR(50)     LABEL "Directorio Traspasos",
    prox_palet                       INTEGER      NOT NULL DEFAULT 1 LABEL "Proximo Palet",
    dir_copia                        CHAR(50)     LABEL "Directorio copia seguridad"
    )
PRIMARY KEY (codigo)
;
{ 

    TABLE: cabfichero
}
CREATE TABLE cabfichero (
    fichero                          CHAR(5)      NOT NULL UPSHIFT,
    calidad                          INTEGER     ,
    medidas                          INTEGER     ,
    caracteristicas                  INTEGER     ,
    calificacion1                    INTEGER     ,
    calificacion2                    INTEGER     ,
    calificacion3                    INTEGER     ,
    origen_actual                    INTEGER     ,
    fecha_traspaso                   DATE        ,
    id_traspaso                      INTEGER      NOT NULL DEFAULT 0 LABEL "Palet_Traspaso",
    largomaximo                      SMALLINT    
    )
PRIMARY KEY (fichero)
;
{ 

    TABLE: inventario
}
CREATE TABLE inventario (
    sesion                           SMALLINT     NOT NULL,
    palet                            INTEGER      NOT NULL,
    paquetes                         SMALLINT     DEFAULT 0,
    metros                           DECIMAL(9,3) DEFAULT 0 LABEL "Metros reales",
    metros_et                        DECIMAL(9,3) DEFAULT 0 LABEL "Metros etiqueta",
    fecha                            DATE        
    )
PRIMARY KEY (palet)
;
{ 

    TABLE: agencias
}
CREATE TABLE agencias (
    codigo                           SMALLINT     NOT NULL,
    nombre                           CHAR(25)    ,
    telefonos                        CHAR(25)    ,
    matricula                        CHAR(10)     UPSHIFT LABEL "Matricula",
    remolque                         CHAR(10)     UPSHIFT LABEL "Matricula remolque"
    )
PRIMARY KEY (codigo)
;
{ 

    TABLE: hcargacab
}
CREATE TABLE hcargacab (
    numero                           INTEGER      NOT NULL DEFAULT 0 LABEL "Num.Hoja de Carga",
    cliente                          INTEGER      NOT NULL,
    transporte                       CHAR(25)     LABEL "Transportista",
    matricula                        CHAR(10)     UPSHIFT,
    remolque                         CHAR(10)     UPSHIFT LABEL "Matricula remolque",
    portes                           CHAR(1)      NOT NULL DEFAULT "P" UPSHIFT,
    traspaso                         SMALLINT     NOT NULL DEFAULT 0 UPSHIFT LABEL "Traspaso (0=no)"
    )
PRIMARY KEY (numero)
;
{ 

    TABLE: hcargalin
}
CREATE TABLE hcargalin (
    numero                           INTEGER      NOT NULL DEFAULT 0 LABEL "Num.Hoja de Carga",
    palet                            INTEGER      NOT NULL,
    metros                           DECIMAL(9,3) NOT NULL DEFAULT 0,
    largo_max                        INTEGER      DEFAULT 0 LABEL "Largo maximo"
    )
PRIMARY KEY (palet)
;
{ 

    TABLE: dlmclientes
}
CREATE TABLE dlmclientes (
    codigo                           INTEGER      NOT NULL,
    nombre                           CHAR(35)    ,
    portes                           CHAR(1)      UPSHIFT
    )
PRIMARY KEY (codigo)
;
CLOSE DATABASE;
