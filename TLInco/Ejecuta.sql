{
    REPOSITORY: DLMemor
    LOCATION: d:\cosmos\dlmemorMsel\DLMemor.CRF
    DATE: 16/12/2016 TIME: 12:49:55.
}
drop table inventario;
CREATE TABLE inventario (
    sesion                           SMALLINT     NOT NULL,
    cbarra                           CHAR(20)     NOT NULL UPSHIFT LABEL "Cod.Barras",
    paquete                          CHAR(8)      NOT NULL UPSHIFT,
    trozada                          CHAR(2)      DEFAULT "0" UPSHIFT,
    metros                           DECIMAL(9,3) DEFAULT 0 LABEL "Metros reales",
    metros_et                        DECIMAL(9,3) DEFAULT 0 LABEL "Metros etiqueta",
    fecha                            DATE        ,
    zona                             SMALLINT    ,
    norden                           SERIAL       NOT NULL LABEL "NRegistro"
    )
PRIMARY KEY (cbarra)
;
