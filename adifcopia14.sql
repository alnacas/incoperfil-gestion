-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: adif
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `diccionario`
--

DROP TABLE IF EXISTS `diccionario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diccionario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` text,
  `texto` text,
  `elemento` varchar(3) DEFAULT NULL,
  `posicion` varchar(3) DEFAULT NULL,
  `calculo` varchar(3) DEFAULT NULL,
  `en` text,
  `es` text,
  `fr` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diccionario`
--

LOCK TABLES `diccionario` WRITE;
/*!40000 ALTER TABLE `diccionario` DISABLE KEYS */;
INSERT INTO `diccionario` VALUES (1,'caracteristicas_material','acero',NULL,NULL,NULL,'Steel','Acero','Acier'),(2,'caracteristicas_material','acero_cor-ten',NULL,NULL,NULL,'','Acero Cor-Ten','Acier Cor-Ten'),(3,'caracteristicas_material','acero_inoxidable',NULL,NULL,NULL,'','Acero Inoxidable','Acier Inoxidable'),(4,'caracteristicas_material','aluminio',NULL,NULL,NULL,'','Aluminio','Aluminium'),(5,'caracteristicas_material','cobre',NULL,NULL,NULL,'','Cobre','Cuivre'),(6,'caracteristicas_material','zinc',NULL,NULL,NULL,'','Zinc','Zinc'),(7,'caracteristicas_material','magnelis',NULL,NULL,NULL,'','Magnelis','Magnelis'),(8,'caracteristicas_material','apoyado',NULL,NULL,NULL,'','Apoyado','Appuié'),(9,'caracteristicas_material','empotrado',NULL,NULL,NULL,'','Empotrado',''),(10,'caracteristicas_material','voladizo',NULL,NULL,NULL,'','Voladizo','Porte-à-faux'),(11,'caracteristicas_material','horizontal',NULL,NULL,NULL,'','Horizontal','Horizontal/e'),(12,'caracteristicas_material','vertical',NULL,NULL,NULL,'','Vertical','Vertical/e'),(13,'caracteristicas_material','inclinado',NULL,NULL,NULL,'','Inclinado','Incliné/e'),(14,'header','cubierta',NULL,NULL,NULL,'Roof','Cubierta','Toiture'),(15,'header','fachada',NULL,NULL,NULL,'Facade','Fachada','Façade'),(16,'header','cubierta_deck',NULL,NULL,NULL,'Deck Roof','Cubierta Deck','Deck Toiture'),(17,'dimensiones','dimensiones',NULL,NULL,NULL,'Dimensions','Dimensiones','Dimensions'),(18,'dimensiones','ancho_util',NULL,NULL,NULL,'Eff. Width: %s mm','Ancho Útil: %s mm','Largeur utile: %s mm'),(19,'dimensiones','cotas_mm',NULL,NULL,NULL,'Dim. in mm','Cotas en mm','Dim. en mm'),(20,'caracteristicas_material','caracteristicas_material',NULL,NULL,NULL,'Material characteristics ','Características del Material','Caracteristiques du Matériel'),(21,'caracteristicas_material','material',NULL,NULL,NULL,'Material ','Material','Matériel'),(22,'caracteristicas_material','limite_elastico',NULL,NULL,NULL,'Yield Strenght','Límite Elástico','Limite Elastique'),(23,'caracteristicas_material','modulo_elastico',NULL,NULL,NULL,'Elasticity Modulus','Módulo Elasticidad','Module d\'Elasticité'),(24,'caracteristicas_material','densidad',NULL,NULL,NULL,'Density','Densidad','Densité'),(25,'caracteristicas_material','tipo_perforado',NULL,NULL,NULL,'','Tipo Perforado',''),(26,'valores_eficaces','valores_eficaces',NULL,NULL,NULL,'Section values','Valores Eficaces','Valeurs Efficaces'),(27,'valores_eficaces','espesor',NULL,NULL,NULL,'Thickness','Espesor','Épaisseur'),(28,'valores_eficaces','peso',NULL,NULL,NULL,'Weight','Peso','Poids'),(29,'valores_eficaces','area_bruta',NULL,NULL,NULL,'Effective Area','Área Bruta','Surface Efficace'),(30,'valores_eficaces','modulo_inercia',NULL,NULL,NULL,'M. Inertia','M. Inercia','M. Inertie'),(31,'valores_eficaces','modulo_resistente',NULL,NULL,NULL,'Section Modulus','M. Resistente','M. Resistante'),(32,'valores_eficaces','bruta',NULL,NULL,NULL,'Gross','Bruta','Brutte'),(33,'valores_eficaces','eficaz',NULL,NULL,NULL,'Effective','Eficaz','Efficace'),(34,'valores_eficaces','valores_estaticos',NULL,NULL,NULL,'','Valores Estáticos',''),(35,'valores_eficaces','area',NULL,NULL,NULL,'','Área',''),(36,'valores_eficaces','modulo_resistente_plastico',NULL,NULL,NULL,'','M. Resistente Plástico',''),(37,'tablas_uso','tablas_uso',NULL,NULL,NULL,'Load Tables','Tablas de uso','Tableau d\'usage'),(38,'tablas_uso','distancia_maxima',NULL,NULL,NULL,'Maximum Span Lenght','Distancia Máxima entre Apoyos Descendente / Ascendente','Distance Maximale entre Appuis'),(39,'tablas_uso','1_vano',NULL,NULL,NULL,'1 Span','1 Vano','1 Travée'),(40,'tablas_uso','sobrecarga_viento',NULL,NULL,NULL,'Wind Load','Sobrecarga Viento','Surcharge du Vent'),(41,'tablas_uso','espesor',NULL,NULL,NULL,'Thickness','Espesor','Épaisseur'),(42,'tablas_uso','2_vanos',NULL,NULL,NULL,'2 Span','2 Vanos','2 Travées'),(43,'tablas_uso','3_vanos',NULL,NULL,NULL,'3 Span','3 Vanos','3 Travées'),(44,'tablas_uso','comprobaciones',NULL,NULL,NULL,'Calculation of','Comprobaciones realizadas a','Calculs realisés à'),(45,'tablas_uso','comprobaciones_flecha',NULL,NULL,NULL,'','Flecha (L / %s)',''),(46,'tablas_uso','comprobaciones_flexion',NULL,NULL,NULL,'','Flexión',''),(47,'tablas_uso','comprobaciones_cortante',NULL,NULL,NULL,'','Cortante',''),(48,'tablas_uso','comprobaciones_abolladura',NULL,NULL,NULL,'','Abolladura',''),(49,'tablas_uso','carga_maxima',NULL,NULL,NULL,'Admissible Load','Carga Máxima Descendente / Ascendente, Q','Charge Maximale'),(50,'tablas_uso','distancia_maxima_escuadras',NULL,NULL,NULL,'','Distancia Máxima entre Escuadras',''),(51,'tablas_uso','sobrecarga_des_asc',NULL,NULL,NULL,'Live Load','Sobrecarga Descendente / Ascendente','Surcharge'),(52,'tablas_uso','distancia_maxima_apoyos',NULL,NULL,NULL,'','Distancia Máxima entre Apoyos. Carga Descendente / Ascendente',''),(53,'tablas_uso','sobrecarga_uso',NULL,NULL,NULL,'','Sobrecarga Uso / Viento Succión',''),(54,'leyenda_calculo','leyenda_calculo',NULL,NULL,NULL,'Calculus Notes','Leyenda de Cálculo','Légende de Calcul'),(55,'leyenda_calculo','esquema_calculo',NULL,NULL,NULL,'','Esquema de Cálculo',''),(57,'leyenda_calculo','sec4_BF-ELU',NULL,NULL,NULL,'U.L.S.: 1,50 * Wind Load','ELU<sub>#</sub>: Q = 1,50 * Viento','ELU: 1,50 * Surcharge du Vent'),(58,'leyenda_calculo','sec4_BF-ELS',NULL,NULL,NULL,'S.L.S.: 1,00 * Wind Load','ELS<sub>#</sub>: Q = 1,00 * Viento','ELS: 1,00 * Surcharge du Vent'),(59,'leyenda_calculo','sec4_RF-ELU',NULL,NULL,NULL,'ULS: 1,35 * Permanent Load + 1,50 * Live Load','ELU<sub>#</sub>: Q = 1,35 * Peso Propio + 1,50 * Viento','ELU: 1,35 * Poids Propre + 1,50 * Surcharge du Vent'),(60,'leyenda_calculo','sec4_RF-ELS',NULL,NULL,NULL,'SLS: 1,00 * Permanent Load + 1,00 * Live Load','ELS<sub>#</sub>: Q = 1,00 * Peso Propio + 1,00 * Viento','ELS: 1,00 * Poids Propre+ 1,00 *Surcharge du Vent'),(61,'leyenda_calculo','sec4_BC-ELU',NULL,NULL,NULL,'ULS: 1,50 * Live Load','ELU<sub>#</sub>: Q = 1,35 * Peso Propio + 1,50 * Sobrecarga','ELU: 1,50 * Surcharge'),(62,'leyenda_calculo','sec4_BC-ELS',NULL,NULL,NULL,'SLS: 1,00 * Live Load','ELS<sub>#</sub>: Q = 1,00 * Peso Propio + 1,00 * Sobrecarga','ELS: 1,00 * Surcharge'),(63,'leyenda_calculo','sec4_BC-ELU-SUC',NULL,NULL,NULL,'','ELU<sub>#</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento',''),(64,'leyenda_calculo','sec4_RC-ELU',NULL,NULL,NULL,'ULS: 1,35 * Permanent Load + 1,50 * Live Load','ELU<sub>#</sub>: Q = 1,35 * Peso Propio + 1,50 * Sobrecarga','ELU: 1,35 * Poids Propre + 1,50 * Surcharge'),(65,'leyenda_calculo','sec4_RC-ELS',NULL,NULL,NULL,'SLS: 1,00 * Permanent Load + 1,00 * Live Load','ELS<sub>#</sub>: Q = 1,00 * Peso Propio + 1,00 * Sobrecarga','ELS: 1,00 * Poids Propre + 1,00 * Surcharge'),(66,'leyenda_calculo','sec4_BRCCT-Nota',NULL,NULL,NULL,'Note: The result of the maximum load is given in horizontal projection','Nota: El resultado de la carga máxima viene dado en proyección horizontal','Note: Le résultat de la charge maximale est donnée en projection horizontale'),(67,'leyenda_calculo','sec4_BRCS-ELU-1',NULL,NULL,NULL,'ULS: 1,35 * Permanent Load + 1,50 * Live Load','ELU<sub>#</sub>: Q = 1,35 * Peso Propio + 1,50 * Uso','ELU: 1,35 * Poids Propre+ 1,50 * Surcharge d\'Usage'),(68,'leyenda_calculo','sec4_BRCS-ELU-2',NULL,NULL,NULL,'1,35 * Permanent Load + 1,50 * Wind Load + # * Snow Load','Q = 1,35 * Peso Propio + 1,50 * Viento + # * Nieve','1,35 * Poids Propre + 1,50 * Surcharge Vent + # * Surcharge Neige'),(69,'leyenda_calculo','sec4_BRCS-ELU-3',NULL,NULL,NULL,'1,35 * Permanent Load + 1,50 * Wind Load + 0,90 * Snow Load','Q = 1,35 * Peso Propio + 1,50 * Nieve + 0,90 * Viento','1,35 * Poids Propre + 1,50 * Surcharge Neige + 0,90 * Surcharge Vent'),(70,'leyenda_calculo','sec4_BRCS-ELS-1',NULL,NULL,NULL,'SLS: 1,00 * Permanent Load + 1,00 * Live Load','ELS<sub>#</sub>: Q = 1,00 * Peso Propio + 1,00 * Uso','ELS: 1,00 * Poids Propre + 1,00 * Surcharge Usage'),(71,'leyenda_calculo','sec4_BRCS-ELS-2',NULL,NULL,NULL,'1,00 * Permanent Load + 1,00 * Wind Load','Q = 1,00 * Peso Propio + 1,00 * Viento','1,00 * Poids Propre + 1,00 * Surcharge Vent'),(72,'leyenda_calculo','sec4_BRCS-ELS-3',NULL,NULL,NULL,'1,00 * Permanent Load + 1,00 * Wind Load','Q = 1,00 * Peso Propio + 1,00 * Nieve','1,00 * Poids Propre + 1,00 * Surcharge Neige'),(73,'leyenda_calculo','sec4_BRCS-Nota',NULL,NULL,NULL,'Note: Live Load and Snow Loading are introduced in horizontal projection','Nota: Valores de Sobrecarga Uso y Nieve introducidos en proyección horizontal','Note: Valeurs de Surcharge d\'Usage et Neige introduits en projection horizontale'),(74,'leyenda_calculo','sec4_1-3',NULL,NULL,NULL,'Calculation made by the Dpt. of Mechanics of Continuous Media and Theory','Cálculos realizados por el Dpto de Mecánica de los Medios Continuos y Teoría de','Calculs realisés par le Dpt. de Mechanique de Moyens Continus '),(75,'leyenda_calculo','sec4_1-4',NULL,NULL,NULL,'Structures Dpt. of Architecture School of the Polytechnic University of Valencia','Estructuras de la E.T.S. de Arquitectura de la Universidad Politécnica de Valencia','Structures de la E.T.S. de Architecture de l\'Université Polytechnique de Valence'),(76,'leyenda_calculo','sec4_1-5',NULL,NULL,NULL,'','La acción del viento sobre el perfil actua sobre el Eje Z',''),(77,'leyenda_calculo','derecha',NULL,NULL,NULL,'Normative:\nUNE-EN 1993-1-3 Eurocode 3 Part 1-3','Normativa:\nUNE-EN 1993-1-3 Eurocódigo 3 Parte 1-3\nCoeficiente &gamma;<sub>M1</sub>: %s\nDeclaración de prestaciones: www.incoperfil.com/dop','Réglementation:\nUNE-EN 1993-1-3 Eurocode 3 Partie 1-3'),(85,'parametros_calculo','parametros_calculo',NULL,NULL,NULL,'Calculation Parameters','Parámetros de Cálculo','Parametres de Calcul'),(86,'parametros_calculo','sobrecarga_viento',NULL,NULL,NULL,'Wind Load','Sobrecarga Viento','Surcharge Vent'),(87,'parametros_calculo','limite_flecha',NULL,NULL,NULL,'Maximum Deflection','Límite Flecha','Limite Flèche'),(88,'parametros_calculo','sobrecarga_uso',NULL,NULL,NULL,'Live Load','Sobrecarga Uso','Surcharge Usage'),(89,'parametros_calculo','sobrecarga_nieve',NULL,NULL,NULL,'Snow Load','Sobrecarga Nieve','Surcharge Neige'),(90,'parametros_calculo','carga_permanente',NULL,NULL,NULL,'','Carga Permanente',''),(91,'parametros_cubfac','parametros_fachada',NULL,NULL,NULL,'Façade Parameters','Parámetros de Fachada','Paramètres de Façade'),(92,'parametros_cubfac','parametros_cubierta',NULL,NULL,NULL,'Roof Parameters','Parámetros de Cubierta','Paramètres de Toiture'),(93,'parametros_cubfac','numero_vanos',NULL,NULL,NULL,'Number of Spans','Número Vanos','Nombre de Travées'),(94,'parametros_cubfac','separacion_apoyos',NULL,NULL,NULL,'Span Length','Separación Apoyos','Separation Appuis'),(95,'parametros_cubfac','extremo_izq_der',NULL,NULL,NULL,'End Left-Right','Extremo Izq. - Der.','Extrême Gau. - Droi.'),(96,'parametros_cubfac','voladizo_izq_der',NULL,NULL,NULL,'Cantilever Left-Right','Voladizo Izq. - Der.','Porte-à-faux Gau. - Droi.'),(97,'parametros_cubfac','peso_revestimiento',NULL,NULL,NULL,'Cladding Weight','Peso Revestimiento','Poids Revêtement'),(98,'parametros_cubfac','separacion_rastreles',NULL,NULL,NULL,'Purling lenght','Separación Rastreles','Separation Montants'),(99,'parametros_cubfac','posicion',NULL,NULL,NULL,'Position','Posición','Position'),(100,'parametros_cubfac','separacion_mensulas',NULL,NULL,NULL,'Distance between Brackets ','Separación Ménsulas','Separation Patte de Fixation'),(101,'parametros_cubfac','angulo_inclinacion',NULL,NULL,NULL,'Slope Angle ','Ángulo Inclinación','Angle d\'inclinaison'),(102,'parametros_cubfac','anchura_apoyos_intermedios',NULL,NULL,NULL,'Width of Supports','Anchura Apoyos Intermedios','Largeur Appuis'),(103,'parametros_calculo','altitud',NULL,NULL,NULL,'Height','Altitud','Altitude'),(104,'parametros_cubfac','longitud_bandeja',NULL,NULL,NULL,'','Longitud Bandeja',''),(105,'parametros_cubfac','longitud_rastrel',NULL,NULL,NULL,'','Longitud Rastrel',''),(106,'parametros_cubfac','posicion_rastrel',NULL,NULL,NULL,'','Posición Rastrel',''),(107,'parametros_cubfac','posicion_tubular',NULL,NULL,NULL,'','Posición Tubular',''),(108,'parametros_cubfac','posicion_omega',NULL,NULL,NULL,'','Posición Omega',''),(109,'parametros_cubfac','longitud_tubular',NULL,NULL,NULL,'','Longitud Tubular',''),(110,'parametros_cubfac','longitud_omega',NULL,NULL,NULL,'','Longitud Omega',''),(111,'parametros_cubfac','separacion_omegas',NULL,NULL,NULL,'','Separación Omegas',''),(112,'parametros_cubfac','separacion_escuadras',NULL,NULL,NULL,'','Separación Escuadras',''),(113,'parametros_cubfac','anchura_apoyos_extremos',NULL,NULL,NULL,'','Anchura Apoyos Extremos',''),(114,'resultado_calculo','resultado_calculo',NULL,NULL,NULL,'Calculation Result','Resultado del Cálculo','Résultat de Calcul'),(116,'resultado_calculo','sec6-1FB',NULL,NULL,NULL,'','La subestructura se montará con una separación entre rastreles de',''),(117,'resultado_calculo','sec6-1FM',NULL,NULL,NULL,'','La subestructura se montará con el espesor',''),(118,'resultado_calculo','sec6-1FS',NULL,NULL,NULL,'','La subestructura se montará con una separación entre tubulares de',''),(119,'resultado_calculo','sec6-1RS',NULL,NULL,NULL,'The Strip','El rastrel','Le montant'),(121,'resultado_calculo','sec6-2FB',NULL,NULL,NULL,'','utilizando',''),(122,'resultado_calculo','sec6-2FM',NULL,NULL,NULL,'','de la bandeja y con',''),(124,'resultado_calculo','sec6-3FB',NULL,NULL,NULL,'','escuadras de sustentación por rastrel',''),(125,'resultado_calculo','sec6-3FM',NULL,NULL,NULL,'','escuadra de sustentación por rastrel',''),(126,'resultado_calculo','sec6-3FSP',NULL,NULL,NULL,'','escuadras de sustentación por tubular',''),(127,'resultado_calculo','sec6-3FSS',NULL,NULL,NULL,'','escuadra de sustentación por tubular',''),(129,'resultado_calculo','lista_bandejas',NULL,NULL,NULL,'Liste of trays','La lista de bandejas','La liste de plateaux'),(130,'resultado_calculo','lista_rastreles',NULL,NULL,NULL,'Liste of strips','La lista de rastreles','La liste de montants'),(131,'resultado_calculo','no_cumple',NULL,NULL,NULL,'NON SATISFIES','NO CUMPLE','NON SATISFAIT'),(134,'resultado_calculo','comprobaciones_els',NULL,NULL,NULL,'SLS Verifications','Comprobaciones para ELS','Verifictions pour ELS'),(135,'resultado_calculo','comprobaciones_elu',NULL,NULL,NULL,'ULS Verifications','Comprobaciones para ELU','Verifications pour ELU'),(136,'resultado_calculo','descendente',NULL,NULL,NULL,'','Descendente',''),(137,'resultado_calculo','ascendente',NULL,NULL,NULL,'','Ascendente',''),(138,'resultado_calculo','presion',NULL,NULL,NULL,'','Presión',''),(139,'resultado_calculo','succion',NULL,NULL,NULL,'','Succión',''),(140,'resultado_calculo','flexion_positivos',NULL,NULL,NULL,'Positive Bending','Flexión Positivos','Flexion Positifs'),(141,'resultado_calculo','flexion_negativos',NULL,NULL,NULL,'Negative Bending','Flexión Negativos','Flexion Negatifs'),(142,'resultado_calculo','flexion_positivos_y',NULL,NULL,NULL,'Positive Bending Y Axis','Flexión Positivos Eje Y','Flexion Positifs Axe Y'),(143,'resultado_calculo','flexion_negativos_y',NULL,NULL,NULL,'Negative Bending Y Axis','Flexión Negativos Eje Y','Flexion Négatifs Axe Y'),(144,'resultado_calculo','flexion_positivos_z',NULL,NULL,NULL,'Positive Bending Z Axis','Flexión Positivos Eje YZ','Flexion Positifs Axe YZ'),(145,'resultado_calculo','flexion_negativos_z',NULL,NULL,NULL,'Negative Bending Z Axis','Flexión Negativos Eje YZ','Flexion Négatifs Axe YZ'),(146,'resultado_calculo','cortante',NULL,NULL,NULL,'Shear','Cortante','Tranchant'),(147,'resultado_calculo','cortante_y',NULL,NULL,NULL,'Shear Y Axis','Cortante Eje Y','Tranchant Axe Y'),(148,'resultado_calculo','cortante_z',NULL,NULL,NULL,'Shear Z Axis','Cortante Eje Z','Tranchart Axe Z'),(149,'resultado_calculo','abolladura_extremos',NULL,NULL,NULL,'Buckling End Supports','Abolladura Apoyos Extremos','Bosse sur Appuis Extrêmes'),(150,'resultado_calculo','abolladura_intermedios',NULL,NULL,NULL,'Buckling Intermediate Supports','Abolladura Apoyos Intermedios','Bosse sur Appuis Intermédiaires'),(151,'resultado_calculo','abolladura_extremos_y',NULL,NULL,NULL,'Buckling End Supports Y Axis','Abolladura Apoyos Extremos Eje Y','Bosse sur Appuis Extrêmes Axe Y'),(152,'resultado_calculo','abolladura_intermedios_y',NULL,NULL,NULL,'Buckling Intermediate Supports Y Axis','Abolladura Apoyos Intermedios Eje Y','Bosse sur Appuis Intermediaires Axe Y'),(153,'resultado_calculo','abolladura_extremos_z',NULL,NULL,NULL,'Buckling End Supports Z Axis','Abolladura Apoyos Extremos Eje Z','Bosse su Appuis Extrêmes Axe Z'),(154,'resultado_calculo','abolladura_intermedios_z',NULL,NULL,NULL,'Buckling Intermediate Supports Z Axis','Abolladura Apoyos Intermedios Eje Z','Bosse sur Appuis Intermediaires Axe Z'),(155,'resultado_calculo','flecha_maxima',NULL,NULL,NULL,'Maximum Deflection','Flecha Máxima','Flèche Maximale'),(156,'resultado_calculo','flecha_maxima_vano',NULL,NULL,NULL,'Maximum Deflection Span #','Flecha Máxima Vano %s','Flèche Maximale Travée #'),(157,'resultado_calculo','flecha_voladizo_izquierdo',NULL,NULL,NULL,'','Flecha Voladizo Izquierdo',''),(158,'resultado_calculo','flecha_voladizo_derecho',NULL,NULL,NULL,'','Flecha Voladizo Derecho',''),(159,'resultado_calculo','flecha_cento_vano_y',NULL,NULL,NULL,'Midspan Deflection Y Axis','Flecha Centro Vano Eje Y','Flèche Centre Travée Axe Y'),(160,'resultado_calculo','flecha_cento_vano_z',NULL,NULL,NULL,'Midspan Deflection Z Axis','Flecha Centro Vano Eje Z','Flèche Centre Travée Axe Z'),(161,'resultado_calculo','axil',NULL,NULL,NULL,'','Axil',''),(162,'resultado_calculo','flexion',NULL,NULL,NULL,'','Flexión',''),(163,'resultado_calculo','cortante_axil',NULL,NULL,NULL,'','Cortante y Axil',''),(164,'resultado_calculo','flexion_axil',NULL,NULL,NULL,'','Flexión y Axil',''),(165,'resultado_calculo','flexion_cortante_y',NULL,NULL,NULL,'','Flexión y Cortante Eje Y',''),(166,'resultado_calculo','flexion_cortante_z',NULL,NULL,NULL,'','Flexión y Cortante Eje Z',''),(167,'resultado_calculo','sec6-8AH',NULL,NULL,NULL,'','Abolladura H',''),(168,'resultado_calculo','sec6-8AB',NULL,NULL,NULL,'','Abolladura B',''),(169,'resultado_calculo','flecha_y',NULL,NULL,NULL,'','Flecha Eje Y',''),(170,'resultado_calculo','flecha_z',NULL,NULL,NULL,'','Flecha Eje Z',''),(171,'resultado_calculo','sec6-9',NULL,NULL,NULL,'Support Reactions','Reacciones sobre la escuadra','Reactions sur l\'appui'),(172,'resultado_calculo','sec6-9ELU',NULL,NULL,NULL,'Support Reactions (ELU)','Reacciones sobre la escuadra (ELU)','Reactions sur l\'appui (ELU)'),(173,'resultado_calculo','sec6-9ELS',NULL,NULL,NULL,'Support Reactions (ELS)','Reacciones sobre la escuadra (ELS)','Reactions sur l\'appui (ELS)'),(174,'resultado_calculo','sec6-9H',NULL,NULL,NULL,'Horizontal Shear','Reacción Horizontal','Reaction Horizontale'),(175,'resultado_calculo','sec6-9V',NULL,NULL,NULL,'Vertical Shear','Reacción Vertical','Reaction Verticale'),(176,'resultado_calculo','sin_comprobar',NULL,NULL,NULL,'Not checked','Sin comprobar','Sans vérifier'),(177,'resultado_calculo','sec6-11',NULL,NULL,NULL,'',', a una separación entre escuadras de',''),(178,'resultado_calculo','perforado',NULL,NULL,NULL,'','perforado',''),(180,'componentes','componentes','F',NULL,NULL,'','Componentes',''),(181,'componentes','bandeja','F',NULL,NULL,'','Bandeja',''),(182,'componentes','rastrel','F',NULL,NULL,'','Rastrel',''),(183,'componentes','escuadra','F',NULL,NULL,'','Escuadra',''),(184,'componentes','tubular','F',NULL,NULL,'','Tubular',''),(185,'componentes','omega','F',NULL,NULL,'','Omega',''),(186,'escuadras','escuadras','F',NULL,NULL,'','Escuadras',''),(187,'escuadras','modelos_escuadras','F',NULL,NULL,'','Modelos de Escuadras',''),(188,'escuadras','resistencia_vertical','F',NULL,NULL,'','Resistencias de las escuadras fretne a una fuerza vertical',''),(189,'escuadras','resistencia_horizontal','F',NULL,NULL,'','Resistencias de las escuadras fretne a una fuerza horizontal',''),(190,'escuadras','escuadra','F',NULL,NULL,'','Escuadra',''),(191,'escuadras','modelo','F',NULL,NULL,'','Modelo',''),(192,'escuadras','longitud_ala','F',NULL,NULL,'','Long. Ala',''),(193,'escuadras','fuerza','F',NULL,NULL,'','Fuerza',''),(194,'escuadras','sustentacion','F',NULL,NULL,'','Sustentación',''),(195,'escuadras','retencion','F',NULL,NULL,'','Retención',''),(196,'escuadras','horizontal','F',NULL,NULL,'','Horizontal',''),(197,'escuadras','ensayos','F',NULL,NULL,'','Ensayos realizados por el laboratorio AIMME de acuerdo con la normativa europea ETAG 034. Part II.',''),(198,'escuadras','valor_caracteristico','F',NULL,NULL,'','Valor característico (p = 95%) con un nivel de confianza del 75%.',''),(199,'escuadras','valor_fuerza_vertical','F',NULL,NULL,'','Valor de la fuerza característica para un desplazamiento de 3,00 mm frente a una fuerza vertical.',''),(200,'escuadras','valor_fuerza_horizontal','F',NULL,NULL,'','Valor de la fuerza característica para una distorsión residual de 1,00 mm tras el retorno a cero de la acción aplicada durante el ensayo frente a una fuerza horizontal.',''),(201,'footer','izquierda',NULL,NULL,NULL,'Ingeniería y Costrucción del Perfil S.A. reserves the right to carry out any general and/or particular modification in the characteristics and engineering details of its profiles, due to production requirements or technological improvement. Ingenieria y Costrucción del Perfil S.A. is not responsible for the failure to comply with the recommendations given in our technical dossier.','Ingeniería y Construcción del Perfil, S.A. se reserva el derecho a realizar cualquier modificación, en las características y datos técnicos generales y particulares de sus productos, realizados por necesidades de producción o mejora tecnológica. Ingeniería y Construcción del Perfil, S.A. no se hace responsable del incumplimiento de los resultados e indicaciones del presente documento, así como de las recomendaciones de instalación que figuran en los distintos Dossieres Técnicos. En caso de duda sobre el presente documento, contacte con nuestro Departamento Técnico.','Ingeniería y Construcción del Perfil, S.A. se réserve le droit de réaliser toute modification concernant les caractéristiques et données techniques générales et spécifiques de ses profils, en fonction des besoins de production ou des progrès techniques. Ingeniería y Construcción del Perfil, S.A. n\'est pas responsable du non respect des conseils d\'installation qui pourra trouver dans notre dossier technique.'),(205,'footer','derecha',NULL,NULL,NULL,'INCOPERFIL (Ingeniería y Construcción del Perfil, S.A.)\nC/Nou (Polígono Industrial Mas del Polio) 46469 Beniparrell (Valencia)\nTel. +34 961 211 778&nbsp;&nbsp;&nbsp;&nbsp;Fax. +34 961 211 504\ninfo@incoperfil.com&nbsp;&nbsp;&nbsp;&nbsp;www.incoperfil.com','INCOPERFIL (Ingeniería y Construcción del Perfil, S.A.)\nC/Nou (Polígono Industrial Mas del Polio) 46469 Beniparrell (Valencia)\nTel. +34 961 211 778&nbsp;&nbsp;&nbsp;&nbsp;Fax. +34 961 211 504\ninfo@incoperfil.com&nbsp;&nbsp;&nbsp;&nbsp;www.incoperfil.com','INCOPERFIL (Ingeniería y Construcción del Perfil, S.A.)\nC/Nou (Polígono Industrial Mas del Polio) 46469 Beniparrell (Valencia)\nTel. +34 961 211 778&nbsp;&nbsp;&nbsp;&nbsp;Fax. +34 961 211 504\ninfo@incoperfil.com&nbsp;&nbsp;&nbsp;&nbsp;www.incoperfil.com'),(209,'escuadras','es05','F',NULL,NULL,'112','112','112'),(210,'escuadras','es10','F',NULL,NULL,'90','90','90'),(211,'escuadras','es14','F',NULL,NULL,'79','79','79'),(212,'escuadras','es17','F',NULL,NULL,'66','66','66'),(213,'escuadras','eh05','F',NULL,NULL,'139','139','139'),(214,'escuadras','eh10','F',NULL,NULL,'62','62','62'),(215,'escuadras','eh14','F',NULL,NULL,'42','42','42'),(216,'escuadras','eh17','F',NULL,NULL,'33','33','33'),(217,'escuadras','er05','F',NULL,NULL,'72','72','72'),(218,'escuadras','er10','F',NULL,NULL,'62','62','62'),(219,'escuadras','er14','F',NULL,NULL,'60','60','60'),(220,'escuadras','er17','F',NULL,NULL,'54','54','54'),(221,'escuadras','ehh05','F',NULL,NULL,'46','46','46'),(222,'escuadras','ehh10','F',NULL,NULL,'79','79','79'),(223,'escuadras','ehh14','F',NULL,NULL,'117','117','117'),(224,'escuadras','ehh17','F',NULL,NULL,'138','138','138'),(225,'resultado_calculo','reacciones_apoyos',NULL,NULL,NULL,'','Reacciones en los apoyos','Reactions dans les appuis'),(226,'resultado_calculo','apoyo',NULL,NULL,NULL,'','Apoyo','Appui'),(227,'leyenda_calculo','izquierda','P','C','C',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Sobrecarga\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Sobrecarga\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento\n<i>Nota: El resultado de la carga máxima viene dado en proyección horizontal</i>',NULL),(228,'leyenda_calculo','izquierda','P','C','E',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Uso\nQ = 1,35 * Peso Propio + 1,50 * Viento + %s * Nieve \nQ = 1,35 * Peso Propio + 1,50 * Nieve + 0,90 * Viento\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Uso\nQ = 1,00 * Peso Propio + 1,00 * Viento\nQ = 1,00 * Peso Propio + 1,00 * Nieve\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento',NULL),(229,'leyenda_calculo','izquierda','P','C','F',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Sobrecarga\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Sobrecarga\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento\n<i>Nota: El resultado de la carga máxima viene dado en proyección horizontal</i>',NULL),(230,'leyenda_calculo','izquierda','P','C','L',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Uso\nQ = 1,35 * Peso Propio + 1,50 * Viento + %s * Nieve \nQ = 1,35 * Peso Propio + 1,50 * Nieve + 0,90 * Viento\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Uso\nQ = 1,00 * Peso Propio + 1,00 * Viento\nQ = 1,00 * Peso Propio + 1,00 * Nieve\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento',NULL),(231,'leyenda_calculo','izquierda','P','CD','C',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Sobrecarga\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Sobrecarga\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento\n<i>Nota: El resultado de la carga máxima viene dado en proyección horizontal</i>',NULL),(232,'leyenda_calculo','izquierda','P','CD','E',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Uso\nQ = 1,35 * Peso Propio + 1,50 * Viento + %s * Nieve \nQ = 1,35 * Peso Propio + 1,50 * Nieve + 0,90 * Viento\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Uso\nQ = 1,00 * Peso Propio + 1,00 * Viento\nQ = 1,00 * Peso Propio + 1,00 * Nieve\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento',NULL),(233,'leyenda_calculo','izquierda','P','CD','F',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Sobrecarga\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Sobrecarga\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento\n<i>Nota: El resultado de la carga máxima viene dado en proyección horizontal</i>',NULL),(234,'leyenda_calculo','izquierda','P','CD','L',NULL,'Combinación de Acciones:\nELU<sub>Descendente</sub>: Q = 1,35 * Peso Propio + 1,50 * Uso\nQ = 1,35 * Peso Propio + 1,50 * Viento + %s * Nieve \nQ = 1,35 * Peso Propio + 1,50 * Nieve + 0,90 * Viento\nELS<sub>Descendente</sub>: Q = 1,00 * Peso Propio + 1,00 * Uso\nQ = 1,00 * Peso Propio + 1,00 * Viento\nQ = 1,00 * Peso Propio + 1,00 * Nieve\nELU<sub>Ascendente</sub>: Q = 0,80 * (Peso Propio + Carga Permanente) - 1,50 * Viento',NULL),(235,'leyenda_calculo','izquierda','P','F','C',NULL,'Combinación de Acciones:\nELU<sub>Presión</sub>: Q = 1,50 * Viento\nELS<sub>Presión</sub>: Q = 1,00 * Viento\nELU<sub>Succión</sub>: Q = 1,50 * Viento',NULL),(236,'leyenda_calculo','izquierda','P','F','E',NULL,'Combinación de Acciones:\nELU<sub>Presión</sub>: Q = 1,50 * Viento\nELS<sub>Presión</sub>: Q = 1,00 * Viento\nELU<sub>Succión</sub>: Q = 1,50 * Viento',NULL),(237,'leyenda_calculo','izquierda','P','F','F',NULL,'Combinación de Acciones:\nELU<sub>Presión</sub>: Q = 1,50 * Viento\nELS<sub>Presión</sub>: Q = 1,00 * Viento\nELU<sub>Succión</sub>: Q = 1,50 * Viento',NULL),(238,'leyenda_calculo','izquierda','P','F','L',NULL,'Combinación de Acciones:\nELU<sub>Presión</sub>: Q = 1,50 * Viento\nELS<sub>Presión</sub>: Q = 1,00 * Viento\nELU<sub>Succión</sub>: Q = 1,50 * Viento',NULL),(239,'resultado_calculo','conclusion','P',NULL,'C','','El perfil <b>%s</b>%s de <b>%s mm</b> de espesor, con una carga máxima, Q, de <b>%s daN/m&sup2; CUMPLE</b>',''),(240,'resultado_calculo','conclusion','P',NULL,'L','','El perfil <b>%s</b>%s de <b>%s mm</b> de espesor, con una separación entre apoyos de <b>%s m CUMPLE</b>',''),(241,'resultado_calculo','conclusion','P',NULL,'E','','El perfil <b>%s</b>%s de <b>%s mm</b> de espesor <b>CUMPLE</b>',''),(242,'tablas_uso','carga_permanente',NULL,NULL,NULL,'','Carga Permanente','');
/*!40000 ALTER TABLE `diccionario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escuadras_aluminio`
--

DROP TABLE IF EXISTS `escuadras_aluminio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escuadras_aluminio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `espesor` double NOT NULL,
  `reacH` double NOT NULL,
  `reacV` double NOT NULL,
  `nombre` varchar(16) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escuadras_aluminio`
--

LOCK TABLES `escuadras_aluminio` WRITE;
/*!40000 ALTER TABLE `escuadras_aluminio` DISABLE KEYS */;
/*!40000 ALTER TABLE `escuadras_aluminio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escuadras_galvanizado`
--

DROP TABLE IF EXISTS `escuadras_galvanizado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escuadras_galvanizado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `espesor` double NOT NULL,
  `reacH` double NOT NULL,
  `reacV` double NOT NULL,
  `nombre` varchar(16) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escuadras_galvanizado`
--

LOCK TABLES `escuadras_galvanizado` WRITE;
/*!40000 ALTER TABLE `escuadras_galvanizado` DISABLE KEYS */;
INSERT INTO `escuadras_galvanizado` VALUES (1,2,0.72,1.12,'ES/ER05','Omega.jpg'),(2,2,0.33,1.38,'EH17','eh17.jpg');
/*!40000 ALTER TABLE `escuadras_galvanizado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espesores_perfiles`
--

DROP TABLE IF EXISTS `espesores_perfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espesores_perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_perfil` int(11) NOT NULL COMMENT 'es el id del perfil al que esta asociado',
  `espesor` double NOT NULL,
  `peso` double NOT NULL,
  `ancho_util` double NOT NULL,
  `inercia_bruta` double NOT NULL,
  `inercia_positiva` double NOT NULL,
  `inercia_negativa` double NOT NULL,
  `momento_positivo` double NOT NULL,
  `momento_negativo` double NOT NULL,
  `ay` double NOT NULL,
  `e_max` double NOT NULL DEFAULT '0',
  `e_min` double NOT NULL DEFAULT '0',
  `sd` double NOT NULL DEFAULT '0',
  `sum_Is` double NOT NULL DEFAULT '0',
  `area_els` double NOT NULL DEFAULT '0',
  `inercia_els` double NOT NULL DEFAULT '0',
  `m` double DEFAULT NULL,
  `k` double DEFAULT NULL,
  `et` double DEFAULT '0',
  `max_luz` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_idPerfil` (`id_perfil`),
  CONSTRAINT `fk_idPerfil` FOREIGN KEY (`id_perfil`) REFERENCES `perfiles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='almacena todos los espesores de todos los perfiles';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espesores_perfiles`
--

LOCK TABLES `espesores_perfiles` WRITE;
/*!40000 ALTER TABLE `espesores_perfiles` DISABLE KEYS */;
INSERT INTO `espesores_perfiles` VALUES (1,1,0.5,0.045,1100,63392,51740,34191,2135,2223,568,0,0,0,0,0,0,0,0,0,NULL),(2,1,0.55,0.049,1100,69715,60772,38846,2547,2514,625,0,0,0,0,0,0,0,0,0,NULL),(3,1,0.6,0.054,1100,76037,69623,43712,2951,2770,682,0,0,0,0,0,0,0,0,0,NULL),(4,1,0.7,0.062,1100,88674,84748,53726,3613,3284,795,0,0,0,0,0,0,0,0,0,NULL),(5,1,0.75,0.067,1100,94987,92465,60051,3953,3543,852,0,0,0,0,0,0,0,0,0,NULL),(6,1,0.8,0.071,1100,101301,100182,64306,4294,3802,909,0,0,0,0,0,0,0,0,0,NULL),(7,1,0.9,0.08,1100,113920,113920,75326,4883,4324,1023,0,0,0,0,0,0,0,0,0,NULL),(8,1,1,0.089,1100,126532,126532,86731,5409,4848,1136,0,0,0,0,0,0,0,0,0,NULL),(9,1,1.1,0.098,1100,139136,139137,98463,5935,5731,1250,0,0,0,0,0,0,0,0,0,NULL),(10,1,1.2,0.107,1100,151735,151735,110479,6459,5898,1364,0,0,0,0,0,0,0,0,0,NULL),(11,2,0.5,0.047,1050,68049,61699,42985,2704,2658,595,0,0,0,0,0,0,0,0,0,NULL),(12,2,0.55,0.052,1050,74833,69694,48900,3070,2952,655,0,0,0,0,0,0,0,0,0,NULL),(13,2,0.6,0.057,1050,81614,77846,55015,3443,3251,714,0,0,0,0,0,0,0,0,0,NULL),(14,2,0.7,0.066,1050,95165,94361,67746,4199,3849,833,0,0,0,0,0,0,0,0,0,NULL),(15,2,0.75,0.071,1050,101935,101533,74393,4517,4149,893,0,0,0,0,0,0,0,0,0,NULL),(16,2,0.8,0.075,1050,108704,108705,81041,4835,4450,952,0,0,0,0,0,0,0,0,0,NULL),(17,2,0.9,0.085,1050,122231,122231,94768,5422,5054,1071,0,0,0,0,0,0,0,0,0,NULL),(18,2,1,0.094,1050,135746,135746,108848,6009,5654,1190,0,0,0,0,0,0,0,0,0,NULL),(19,2,1.1,0.103,1050,149250,149250,123238,6589,6255,1310,0,0,0,0,0,0,0,0,0,NULL),(20,2,1.2,0.113,1050,162744,162744,137760,7169,6853,1429,0,0,0,0,0,0,0,0,0,NULL),(21,5,0.5,0.0584077380952381,840,527469,431876,344248,8669,7036,744,0,0,0,0,0,0,0,0,0,NULL),(22,5,0.55,0.0642485119047619,840,580267,496532,402035,10402,8394,818,0,0,0,0,0,0,0,0,0,NULL),(23,5,0.6,0.0700892857142857,840,633077,553003,462532,12195,9857,893,0,0,0,0,0,0,0,0,0,NULL),(24,5,0.7,0.0817708333333333,840,738725,673258,592251,16254,13134,1042,0,0,0,0,0,0,0,0,0,NULL),(25,5,0.75,0.0876116071428571,840,791564,745232,698464,17837,16127,1116,0,0,0,0,0,0,0,0,0,NULL),(26,5,0.8,0.093452380952381,840,844414,790899,778881,19159,18404,1190,0,0,0,0,0,0,0,0,0,NULL),(27,5,0.9,0.105133928571429,840,950145,910698,927027,21821,22517,1339,0,0,0,0,0,0,0,0,0,NULL),(28,5,1,0.116815476190476,840,1055919,1029987,1055919,24498,25899,1488,0,0,0,0,0,0,0,0,0,NULL),(29,5,1.2,0.140178571428571,840,1267596,1267597,1267597,29834,31038,1786,0,0,0,0,0,0,0,0,0,NULL),(30,6,0.5,0.058,425,372678,276354,155211,4630,4068,735,6,4.05,71.96,544.83,0,0,0,0,0,NULL),(31,6,0.55,0.064,425,409861,311197,179487,5228,4853,809,6.03,4.06,71.96,598.94,0,0,0,0,0,NULL),(32,6,0.6,0.07,425,447028,346842,204582,5840,5697,882,6.06,4.08,71.96,653.2,0,0,0,0,0,NULL),(33,6,0.7,0.081,425,521315,420630,254345,7113,7383,1029,6.12,4.11,71.96,761.01,0,0,0,0,0,NULL),(34,6,0.75,0.087,425,558431,459035,278535,7779,8188,1103,6.15,4.12,71.96,815,0,0,0,0,0,NULL),(35,6,0.8,0.093,425,595546,497439,302724,8444,8993,1176,6.18,4.14,71.96,869.22,0,0,0,0,0,NULL),(36,6,0.9,0.104,425,669715,576976,352907,9825,10713,1324,6.24,4.17,71.96,997,0,0,0,0,0,NULL),(37,6,1,0.116,425,743736,658635,404576,11251,12529,1471,6.3,4.2,71.96,1082.11,0,0,0,0,0,NULL),(38,6,1.1,0.127,425,817885,742476,457796,12715,14450,1618,6.36,4.23,71.96,1192.41,0,0,0,0,0,NULL),(39,6,1.2,0.139,425,891882,829684,512038,14250,16443,1765,6.49,4.26,71.96,1299.99,0,0,0,0,0,NULL),(40,7,1,0.1189,825,2310769,1967219,1760836,30763,37335,1515,3.43,2,111,761.01,0,0,0,0,0,8.5),(41,9,0.75,0.0981,600,3779235,2620000,3410000,19174,24956,1250,5,5,156.57,572,0,0,0,0,0,NULL),(42,3,0.5,0.0501,980,154249,137172,88938,4071,3937,638,1.98,1.8,50.13,53.94,0,0,0,0,0,NULL),(43,3,0.55,0.0551,980,169678,154539,101043,4624,4387,702,2.01,1.84,50.13,59.35,0,0,0,0,0,NULL),(44,3,0.6,0.0601,980,185107,172264,113587,5185,4841,765,2.04,1.87,50.13,64.78,0,0,0,0,0,NULL),(45,3,0.7,0.0701,980,215970,208515,139844,6337,5764,893,2.09,1.93,50.13,75.61,0,0,0,0,0,NULL),(46,3,0.75,0.0751,980,231404,226911,153492,6922,6231,957,2.11,1.95,50.13,80.69,0,0,0,0,0,NULL),(47,3,0.8,0.0801,980,246214,245402,167450,7512,6700,1020,2.14,1.98,50.13,86.69,0,0,0,0,0,NULL),(48,3,0.9,0.0901,980,277715,277715,196237,8510,7644,1148,2.19,2.04,50.13,97.84,0,0,0,0,0,NULL),(49,3,1,0.1001,980,308597,308597,226033,9359,8594,1276,2.25,2.11,50.13,109.14,0,0,0,0,0,NULL),(50,3,1.1,0.1101,980,339488,339487,256681,10385,9549,1403,2.3,2.16,50.13,120.57,0,0,0,0,0,NULL),(51,3,1.2,0.1202,980,370386,370386,288040,11320,10508,1531,2.36,2.22,50.13,132.14,0,0,0,0,0,NULL),(52,7,0.7,0.0833,825,1499585,1178310,1062620,17413,22174,1061,3.32,1.75,111,598.94,0,0,0,0,0,7),(53,7,0.6,0.0714,825,1386387,923208,848159,13253,17686,909,3.28,1.66,111,544.83,0,0,0,0,0,6.5),(54,7,0.8,0.0952,825,1848560,1406767,1274467,21124,26791,1212,3.35,1.81,111,653.2,0,0,0,0,0,7.5),(55,7,1.2,0.1427,825,2773026,2413300,2199252,38102,47092,1818,3.5,2.11,111,815,0,0,0,0,0,9.5),(56,7,0.75,0.0892,825,1617470,1283980,1155998,19100,24167,1136,3.33,1.79,111,631.9,0,0,0,0,0,7.2),(57,7,1.5,0.1784,825,3466517,3083729,2860355,49166,61868,2273,3.62,2.33,111,869.22,0,0,0,0,0,10.5),(58,8,0.6,0.0841,840,2871947,2315539,2190398,23255,28121,1071,3.42,2.74,169.79,114.600928660022,0,0,0,0,0,6.31),(59,8,0.7,0.0981,840,3350623,2916680,2724710,29957,36365.2,1250,3.45,2.83,169.79,122.337750103359,0,0,0,0,0,7.43),(60,8,0.75,0.1051,840,3589965,3141002,2978876,32336,40104,1339,3.46,2.88,169.79,126.206160825027,0,0,0,0,0,8),(61,8,0.8,0.1121,840,3829309,3450391,3251666,35906,44186,1429,3.47,2.91,169.79,130.074571546696,0,0,0,0,0,8.55),(62,8,1,0.1401,840,4786712,4768768,4480790,51566,62199,1786,3.54,3.08,169.79,145.548214433369,0,0,0,0,0,10.79),(63,8,1.2,0.1682,840,5744164,5902309,5556108,64709,77292,2143,3.58,3.24,169.79,161.021857320043,0,0,0,0,0,11.03),(64,8,1.5,0.2102,840,7180459,7605500,7194412,84555,100585,2679,3.66,3.49,169.79,184.23,0,0,0,0,0,12.15),(65,9,1.2,0.157,600,6147021,5400000,6060000,40119,45073,2000,5,5,156.57,684.7583581,0,0,0,0,0,NULL),(66,9,0.7,0.0915,600,3527235,2381499.9999999995,3163689.9999999995,17300,22995,1167,5,5,156.57,567,0,0,0,0,0,NULL),(67,9,1,0.1308,600,5139081,4410000,5050000,32390,37090,1666,5,5,156.57,618.2,0,0,0,0,0,NULL),(68,10,0.75,0.0876,840,800578,709757,587300,11468,15166,1116,0,0,0,0,1013,755167.5,114.61,0.0578,40.67,NULL),(69,10,0.8,0.0935,840,853919,750041,625334,13353,17453,1190,0,0,0,0,1076,801980.8,114.61,0.0578,40.69,NULL),(70,10,1,0.1168,840,1067438,944285,780989,21422,25736,1488,0,0,0,0,1394,1005861.5,128.52,0.0506,40.77,NULL),(71,10,1.2,0.1402,840,1280925,1196750,942913,30225,32323,1786,0,0,0,0,1708,1238837.5,142.443,0.0433,40.84,NULL),(72,7,0.5,0.0595,825,1155185,693440,610980,9319.5,12243,758,3.24,1.58,111,471.0045,0,0,0,0,0,5.5),(73,4,0.6,0.0633,930,172792,172792,172792,7737,7737,806,0,0,0,0,0,0,0,0,0,NULL),(74,4,0.7,0.0738,930,201603,201603,201603,9011,9011,941,0,0,0,0,0,0,0,0,0,NULL),(75,4,0.75,0.0791,930,216010,216010,216010,9647,9647,1008,0,0,0,0,0,0,0,0,0,NULL),(76,4,0.8,0.0844,930,230419,230419,230419,10281,10281,1075,0,0,0,0,0,0,0,0,0,NULL),(77,4,1,0.1055,930,288070,288070,288070,12802,12802,1344,0,0,0,0,0,0,0,0,0,NULL),(78,11,0.8,0.0935,840,853919,750041,625334,13353,17453,1190,0,0,0,0,1076,801980.8,114.61,0.0578,40.69,NULL),(79,11,1,0.1168,840,1067438,944285,780989,21422,25736,1488,0,0,0,0,1394,1005861.5,128.52,0.0506,40.77,NULL),(80,11,1.2,0.1402,840,1280925,1196750,942913,30225,32323,1786,0,0,0,0,1708,1238837.5,142.443,0.0433,40.84,NULL),(86,8,0.88,0.12329000000000001,840,4212285.899999999,3969375.9999999995,3741128,42191.88,51035.479999999996,1571.4476000000002,3.4964999999999997,2.978256,169.79,136.26408,0,0,0,0,0,8.55);
/*!40000 ALTER TABLE `espesores_perfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espesores_rastreles`
--

DROP TABLE IF EXISTS `espesores_rastreles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `espesores_rastreles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_rastrel` int(11) NOT NULL COMMENT 'es el id del rastrel al que esta asociado',
  `espesor` double NOT NULL,
  `peso` double NOT NULL,
  `inercia_bruta` double NOT NULL,
  `inercia_positiva` double NOT NULL,
  `inercia_negativa` double NOT NULL,
  `momento_positivo` double NOT NULL,
  `momento_negativo` double NOT NULL,
  `inercia_bruta_y` double NOT NULL,
  `momento_y` double NOT NULL,
  `ay` double NOT NULL,
  `az` double NOT NULL,
  `num_almas` int(11) NOT NULL,
  `sw` double NOT NULL DEFAULT '0',
  `angulo` double NOT NULL DEFAULT '0',
  `e_max` double NOT NULL DEFAULT '0',
  `e_min` double NOT NULL DEFAULT '0',
  `bd` double NOT NULL DEFAULT '0',
  `sp_a` double NOT NULL DEFAULT '0',
  `sd` double NOT NULL DEFAULT '0',
  `sum_Is` double NOT NULL DEFAULT '0',
  `sp_c` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_idRastrel` (`id_rastrel`),
  CONSTRAINT `fk_idRastrel` FOREIGN KEY (`id_rastrel`) REFERENCES `rastreles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='almacena todos los espesores de todos los rastreles';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espesores_rastreles`
--

LOCK TABLES `espesores_rastreles` WRITE;
/*!40000 ALTER TABLE `espesores_rastreles` DISABLE KEYS */;
/*!40000 ALTER TABLE `espesores_rastreles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uso_cubierta` tinyint(1) NOT NULL,
  `uso_fachada` tinyint(1) NOT NULL,
  `uso_colaborante` tinyint(1) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `imagen_fachada` varchar(255) DEFAULT NULL,
  `succion` tinyint(1) DEFAULT NULL,
  `ancho_bobina` int(11) DEFAULT NULL,
  `ancho_util` int(11) DEFAULT NULL,
  `altura_chapa` double DEFAULT NULL,
  `valle_ancho_inferior` double DEFAULT NULL,
  `valle_ancho_superior` double DEFAULT NULL,
  `cresta_ancho_inferior` double DEFAULT NULL,
  `cresta_ancho_superior` double DEFAULT NULL,
  `numero_ondas` int(11) DEFAULT NULL,
  `numero_almas` int(11) DEFAULT NULL,
  `radio` double DEFAULT NULL,
  `sw` double DEFAULT NULL,
  `angulo` double DEFAULT NULL,
  `bd` double DEFAULT NULL,
  `bd_succion` double DEFAULT NULL,
  `sp_a` double DEFAULT NULL,
  `sp_a_succion` double DEFAULT NULL,
  `sp_c` double DEFAULT NULL,
  `u_1` double DEFAULT NULL,
  `u_2` double DEFAULT NULL,
  `u_3` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfiles`
--

LOCK TABLES `perfiles` WRITE;
/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
INSERT INTO `perfiles` VALUES (1,1,1,0,'INCO 30.4','INCO_30.4_cubierta.jpg','INCO_30.4_fachada.jpg',0,1250,1100,30,29,80,246,195,4,8,3,39,50,0,0,0,0,0,NULL,NULL,NULL),(2,1,1,0,'INCO 30.5','INCO_30.5_cubierta.jpg','INCO_30.5_fachada.jpg',0,1250,1050,30,24,61,186,149,5,10,3,34.28,60,0,0,0,0,0,NULL,NULL,NULL),(3,1,1,0,'INCO 44.4','INCO_44.4_cubierta.jpg','INCO_44.4_fachada.jpg',0,1250,980,44,29,68,215,177,4,8,3,48.09,70,28.84,175.2,19.22,19.22,19.22,NULL,NULL,NULL),(4,1,1,0,'INCO 44.6 Ondulado','INCO_44.6_Ondulado_cubierta.jpg','INCO_44.6_Ondulado_fachada.jpg',0,1250,1250,44,NULL,NULL,NULL,NULL,6,13,3,44,45,0,0,0,0,0,NULL,NULL,NULL),(5,1,1,0,'INCO 70.4','INCO_70.4_cubierta.jpg','INCO_70.4_fachada.jpg',0,1250,840,70,50,109.7,160,100.3,4,8,3,74.55,66,0,0,0,0,0,NULL,NULL,NULL),(6,1,1,0,'INCO 72.1 Bandeja','INCO_72.1_fachada.jpg','INCO_72.1_fachada.jpg',0,625,425,72,NULL,NULL,NULL,NULL,1,2,3,70,90,426.64,18.66,28.32,19.4,28.13,NULL,NULL,NULL),(7,1,1,0,'INCO 100.3','INCO_100.3_cubierta.jpg','INCO_100.3_fachada.jpg',0,1250,825,100,39,136,236,139,3,6,3,111,67,147,37.51,67.84,29,67,NULL,NULL,NULL),(8,1,1,0,'INCO 155.3','INCO_155.3_cubierta.jpg','INCO_155.3_fachada.jpg',0,1500,840,155,40,170,240,110,3,6,3,166.97,71,110,37.97,33.44,31.97,81.43,NULL,NULL,NULL),(9,1,1,0,'INCO 157.1 Bandeja','INCO_157.1_fachada.jpg','INCO_157.1_fachada.jpg',0,1000,600,157,NULL,NULL,NULL,NULL,1,2,3,156.89,90,37.65,37.65,30,30,68.73,NULL,NULL,NULL),(10,1,1,1,'INCO 70.4 Colaborante','INCO_70.4_fachada.jpg','INCO_70.4_fachada.jpg',0,1250,840,70,50,109.7,160,100.3,4,8,3,74.55,66,0,0,0,0,0,39.981,50.093,43.363),(11,0,0,1,'INCO 70.4 F Collaborant','INCO_70.4_Colaborante.jpg','INCO_70.4_Colaborante.jpg',0,625,840,70,50,109.7,160,100.3,4,8,3,74.55,66,0,0,0,0,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rastreles`
--

DROP TABLE IF EXISTS `rastreles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rastreles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uso_cubierta` tinyint(1) NOT NULL,
  `uso_fachada` tinyint(1) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rastreles`
--

LOCK TABLES `rastreles` WRITE;
/*!40000 ALTER TABLE `rastreles` DISABLE KEYS */;
/*!40000 ALTER TABLE `rastreles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-18 14:00:02
