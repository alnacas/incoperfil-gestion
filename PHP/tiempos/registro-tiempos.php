<?php

require_once __DIR__ . '/../config.php';

$DB_CONN = false;

if (!empty($argv)) {
    if (!isset($argv[1])) {
        exit('No se ha pasado ningún argumento' . PHP_EOL);
    }

    $action = $argv[1];
    incoLogSetFile($action . '_' . date('Ymd'));

    $DB_CONN = incoDbGetConnection(INCO_DB_LOCAL);
    if ($DB_CONN === false) {
        incoLogWrite('No se ha podido conectar a la base de datos');
    }

    switch ($action) {
        case 'insert-in-db':
            $fecha = isset($argv[2]) ? $argv[2] : null;
            if (DateTime::createFromFormat('Y-m-d', $fecha) === false) {
                incoLogWrite('El formato de la fecha no es válido, se tiene que ajustar al formato "Y-m-d"');
            }
            if (!is_null($fecha)) argResetDay($DB_CONN, $fecha);
            argInsertInDb($DB_CONN, true, $fecha);
            $response = false;
            break;
        case 'test':
            $response = false;
            break;
        case 'codigo':
            $response = getCodigo($DB_CONN, $argv[2]);
            break;
        default:
            $response = 'Acción no reconocida' . PHP_EOL;
            break;
    }

    pg_close($DB_CONN);

    if ($response !== false) {
        echo $response . PHP_EOL;
    }
}

if (!empty($_GET)) {
    $DB_CONN = incoDbGetConnection(INCO_DB_LOCAL);
    if ($DB_CONN === false) {
        incoLogWrite('No se ha podido conectar a la base de datos');
    }

    incoLogSetFile($_GET['a'] . '_' . date('Ymd'));
    switch ($_GET['a']) {
        case 'day':
            $response = date('d - m - Y');
            break;
        case 'time':
            $response = date('H : i : s');
            break;
        case 'last-registro':
            $response = getLast10Registros($DB_CONN, $_GET['nave'], $_GET['operario']);
            break;
        case 'operario':
            $response = getOperario($DB_CONN, $_GET['codbar']);
            break;
        case 'codigo':
            $response = getCodigo($DB_CONN, $_GET['codbar']);
            break;
        /*case 'pedalb':
            $response = getPedAlb($_GET['codbar']);
            break;*/
        default:
            $response = '';
            break;
    }

    pg_close($DB_CONN);

    echo $response;
}

if (!empty($_POST)) {
    incoLogSetFile('registro_tiempos_' . date('Ymd'));

    $date = new DateTime();
    $dia = $date->format('Y-m-d');
    $hora = $date->format('H:i:s');
    $operario = $_POST['operario'];
    $codigo = $_POST['codigo'];
    $observaciones = trim($_POST['observaciones']) == '' ? null : trim($_POST['observaciones']);
    $proyecto = $_POST['proyecto'];

    $DB_CONN = incoDbGetConnection(INCO_DB_LOCAL);
    if ($DB_CONN === false) {
        incoLogWrite('No se ha podido conectar a la base de datos');
    }

    if (strlen($operario) != 6 || strlen($codigo) != 14) {
        pg_close($DB_CONN);
        echo json_encode(['status' => 500, 'msg' => 'Código error']);
        return;
    }
    if (getOperario($DB_CONN, $operario) == '') {
        pg_close($DB_CONN);
        echo json_encode(['status' => 500, 'msg' => 'Operario error']);
        return;
    }

    $registro = implode(';', array($dia, $hora, $operario, $codigo));
    // saveRegistro($registro);

    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    pg_prepare($DB_CONN, 'insert_tl', 'INSERT INTO tiempos_lecturas (fecha, hora, operario, codigo, tratado, observaciones, proyecto) VALUES ($1, $2, $3, $4, 0, $5, $6)');
    pg_execute($DB_CONN, 'insert_tl', array($dia, $hora, $operario, $codigo, $observaciones, $proyecto));
    // pg_close($dbconn);

    $actividad = explodeCodbar($codigo)[2];
    /*if ($actividad == 44 || $actividad == 48) {
        argInsertInDb($DB_CONN);

        require_once __DIR__ . '/procesaTiemposJornada.php';
        procesaTiemposJornada($DB_CONN);
    }*/
    $ptj = $actividad == 44 || $actividad == 48;
    argInsertInDb($DB_CONN, $ptj);

    pg_close($DB_CONN);

    echo json_encode(array('status' => 200));
}

/*function saveRegistro(string $registro)
{
    $file = fopen('./registros.txt', 'a');
    fwrite($file, $registro . PHP_EOL);
    fclose($file);
}*/

function getLast10Registros($dbconn, $nave, $operario): string
{
    if (is_null($nave)) $nave = '';
    $operario = is_null($operario) ? 0 : intval($operario);

    $txt = array();
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    if (empty($nave)) {
        $result = pg_query($dbconn, 'SELECT fecha, hora, operario, codigo FROM tiempos_lecturas WHERE fecha = current_date ORDER BY fecha DESC, hora DESC LIMIT 10');
    } else {
        $sql = 'SELECT fecha, hora, operario, codigo FROM tiempos_lecturas WHERE codigo LIKE \'%0__\' AND fecha = current_date ORDER BY fecha DESC, hora DESC LIMIT 10';
        if (strtoupper($nave) == 'A') $sql = 'SELECT fecha, hora, operario, codigo FROM tiempos_lecturas WHERE (codigo LIKE \'%0__\' OR codigo LIKE \'%1__\') AND fecha = current_date ORDER BY fecha DESC, hora DESC LIMIT 10';
        if (strtoupper($nave) == 'B' || strtoupper($nave) == 'C') $sql = 'SELECT fecha, hora, operario, codigo FROM tiempos_lecturas WHERE (codigo LIKE \'%0__\' OR codigo LIKE \'%2__\') AND fecha = current_date OR codigo LIKE \'%3__\' ORDER BY fecha DESC, hora DESC LIMIT 25';
        if (strtoupper($nave) == 'J') {
            $sqlExtraOper = $operario == 0 ? '' : 'AND operario = \'' . str_pad($operario, 6, 0, STR_PAD_LEFT) . '\'';
            $sql = 'SELECT fecha, hora, operario, codigo FROM tiempos_lecturas WHERE (codigo LIKE \'%440__\' OR codigo LIKE \'%480__\') AND fecha = current_date ' . $sqlExtraOper . ' ORDER BY fecha DESC, hora DESC';
        }
        $result = pg_query($dbconn, $sql);
    }
    $registros = pg_fetch_all($result);
    $diffmin = strtoupper($nave) == 'J' && $operario > 0 ? getDiffmin($dbconn, $operario) : 0;
    $strHorario = strtoupper($nave) == 'J' && $operario > 0 ? getHorario($dbconn, $operario) : '';
    for ($i = 0; $i < count($registros); $i++) {
        $fecha = $registros[$i]['fecha'];
        $hora = $registros[$i]['hora'];
        $oper = getOperario($dbconn, $registros[$i]['operario']);
        $oper = explode('|', $oper)[0];
        $info = getCodigo($dbconn, $registros[$i]['codigo']);
        array_push($txt, implode('|', array($fecha, $hora, $oper, $info, $diffmin, $strHorario)));
    }
    pg_free_result($result);
    // pg_close($dbconn);
    return implode(';', $txt);
}

function getDiffmin($dbconn, $empleado): float
{
    $diff = 0;
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    // $sql = 'select (sum(coalesce(horas, 0) - coalesce(horas_efectivas, 0)) * 60)::numeric(10,2) as diffmin from historico_tiempos where empleado = $1 and fecha < current_date';
    // $result = pg_query_params($dbconn, $sql, array($empleado));
    // $diff += pg_num_rows($result) <= 0 ? 0 : pg_fetch_all($result)[0]['diffmin'];
    $sql = 'select extract(hours from diff) * 60 + extract(minutes from diff) as diffmin
        from
            (select ht.empleado, case extract(dow from ht.fecha)
                    when 1 then (e.l_manana_entra_teo - coalesce(ht.manana_entra, e.l_manana_entra_teo)) + (coalesce(ht.manana_sale, e.l_manana_sale_teo) - e.l_manana_sale_teo) + (e.l_tarde_entra_teo - coalesce(ht.tarde_entra, e.l_tarde_entra_teo)) + (coalesce(ht.tarde_sale, e.l_tarde_sale_teo) - e.l_tarde_sale_teo)
                    when 2 then (e.m_manana_entra_teo - coalesce(ht.manana_entra, e.m_manana_entra_teo)) + (coalesce(ht.manana_sale, e.m_manana_sale_teo) - e.m_manana_sale_teo) + (e.m_tarde_entra_teo - coalesce(ht.tarde_entra, e.m_tarde_entra_teo)) + (coalesce(ht.tarde_sale, e.m_tarde_sale_teo) - e.m_tarde_sale_teo)
                    when 3 then (e.x_manana_entra_teo - coalesce(ht.manana_entra, e.x_manana_entra_teo)) + (coalesce(ht.manana_sale, e.x_manana_sale_teo) - e.x_manana_sale_teo) + (e.x_tarde_entra_teo - coalesce(ht.tarde_entra, e.x_tarde_entra_teo)) + (coalesce(ht.tarde_sale, e.x_tarde_sale_teo) - e.x_tarde_sale_teo)
                    when 4 then (e.j_manana_entra_teo - coalesce(ht.manana_entra, e.j_manana_entra_teo)) + (coalesce(ht.manana_sale, e.j_manana_sale_teo) - e.j_manana_sale_teo) + (e.j_tarde_entra_teo - coalesce(ht.tarde_entra, e.j_tarde_entra_teo)) + (coalesce(ht.tarde_sale, e.j_tarde_sale_teo) - e.j_tarde_sale_teo)
                    when 5 then (e.v_manana_entra_teo - coalesce(ht.manana_entra, e.v_manana_entra_teo)) + (coalesce(ht.manana_sale, e.v_manana_sale_teo) - e.v_manana_sale_teo)
                end as diff
            from historico_tiempos ht, empleados e
            where ht.empleado = e.codigo and ht.fecha = current_date
            order by e.nomape) as aux
        where aux.empleado = $1';
    $result = pg_query_params($dbconn, $sql, array($empleado));
    $diff += pg_num_rows($result) <= 0 ? 0 : pg_fetch_all($result)[0]['diffmin'];
    // pg_close($dbconn);
    return round($diff);
}

function getHorario($dbconn, $empleado) {
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    $sql = 'select trim(horario) as horario from empleados where codigo = $1';
    $result = pg_query_params($dbconn, $sql, array($empleado));
    $horario = pg_num_rows($result) <= 0 ? '' : pg_fetch_all($result)[0]['horario'];
    // pg_close($dbconn);
    return $horario;
}

function getOperario($dbconn, string $codbar): string
{
    $res = '';
    $codigo = intval($codbar);
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    $result = pg_query_params($dbconn, 'SELECT nombre, horario FROM empleados WHERE codigo = $1', array($codigo));
    if (pg_num_rows($result) > 0) {
        $res = trim(pg_fetch_all($result)[0]['nombre']) . '|' . trim(pg_fetch_all($result)[0]['horario']) . '|' . getDiffmin($dbconn, $codigo);
    } else {
        incoLogWrite('REGISTRO TIEMPOS - OBTENER NOMBRE OPERARIO: NO SE HA ENCONTRADO EL CÓDIGO ' . $codigo, false);
    }
    pg_free_result($result);
    // pg_close($dbconn);
    return $res;
}

function explodeCodbar(string $codbar): array
{
    return array(
        intval(substr($codbar, 0, 6)),
        intval(substr($codbar, 6, 3)),
        intval(substr($codbar, 9, 2)),
        intval(substr($codbar, 11, 1)),
        intval(substr($codbar, 12, 2))
    );
}

function getCodigo($dbconn, string $codbar): string
{
    list($numeroPedAlb, $codigoProducto, $codigoActividad, $codigoNave, $codigoMaquina) = explodeCodbar($codbar);
    // 8: CARGA
    // $codigoActividad != 8; 27/06/2018 --> TODOS LOS CODIGOS DE BARRAS PASAN A LLEVAR EL NÚMERO DE PEDIDO
    $isPedido = true;
    $strPedAlb = getPedAlb($dbconn, $numeroPedAlb, $isPedido);
    $strProducto = getProducto($dbconn, $codigoProducto);
    $strNave = getNave($codigoNave);
    $strActividad = getActividad($dbconn, $codigoActividad, $strNave, $codigoMaquina);
    $strMaquina = getMaquina($dbconn, $codigoMaquina);
    return implode('|', array($strPedAlb, $strProducto, $strActividad, $strNave, $strMaquina));
}

function getPedAlb($dbconn, int $numero, bool $isPedido): string
{
    $str = '';
    if ($numero > 0) {
        // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
        if ($isPedido) {
            $result = pg_query_params($dbconn, 'SELECT c.nombre, c.alias, p.referencia_obra FROM pedidos p, clientes c WHERE p.numero = $1 AND p.cliente = c.codigo', array($numero));
        } else {
            $result = pg_query_params($dbconn, 'SELECT c.nombre, c.alias, a.referencia_obra FROM albaranes a, clientes c WHERE a.numero = $1 AND a.cliente = c.codigo', array($numero));
        }
        if (pg_num_rows($result) > 0) {
            $alias = trim(pg_fetch_all($result)[0]['nombre']);
            $refobra = trim(pg_fetch_all($result)[0]['referencia_obra']);
            $str = $numero . ' de ' . $alias . ' para ' . $refobra;
        } else {
            incoLogWrite('REGISTRO TIEMPOS - OBTENER CLIENTE Y REF. OBRA PED/ALB: NO SE HA ENCONTRADO EL NUMERO ' . $numero, false);
        }
        pg_free_result($result);
        // pg_close($dbconn);
    }
    return $str;
}

function getProducto($dbconn, int $codigo): string
{
    $nombre = '';
    if ($codigo > 0) {
        // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
        $result = pg_query_params($dbconn, 'SELECT nombre, nombre_comercial FROM productos WHERE codigo = $1', array($codigo));
        if (pg_num_rows($result) > 0) {
            $nombre = trim(pg_fetch_all($result)[0]['nombre']);
        } else {
            incoLogWrite('REGISTRO TIEMPOS - OBTENER NOMBRE PRODUCTO: NO SE HA ENCONTRADO EL CÓDIGO ' . $codigo, false);
        }
        pg_free_result($result);
        // pg_close($dbconn);
    }
    return $nombre;
}

function getActividad($dbconn, int $codigo, string $nave, int $maquina): string
{
    $nombre = '';
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    $result = pg_query_params($dbconn, 'SELECT nombre FROM actividades WHERE codigo = $1 AND nave = $2 AND maquina = $3', array($codigo, $nave, $maquina));
    if (pg_num_rows($result) > 0) {
        $nombre = trim(pg_fetch_all($result)[0]['nombre']);
    } else {
        incoLogWrite('REGISTRO TIEMPOS - OBTENER NOMBRE ACTIVIDAD: NO SE HA ENCONTRADO EL CÓDIGO ' . $codigo, false);
    }
    pg_free_result($result);
    // pg_close($dbconn);
    return $nombre;
}

function getNave(int $codigo): string
{
    switch ($codigo) {
        case 1:
            $nave = 'A';
            break;
        case 2:
            $nave = 'B';
            break;
        case 3:
            $nave = 'C';
            break;
        default:
            incoLogWrite('REGISTRO TIEMPOS - OBTENER NOMBRE NAVE: NO SE HA ENCONTRADO EL CÓDIGO ' . $codigo, false);
            $nave = ' ';
            break;
    }
    return $nave;
}

function getMaquina($dbconn, int $codigo): string
{
    $nombre = '';
    if ($codigo > 0) {
        // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
        $result = pg_query_params($dbconn, 'SELECT nombre FROM maquinas WHERE codigo = $1', array($codigo));
        if (pg_num_rows($result) > 0) {
            $nombre = trim(pg_fetch_all($result)[0]['nombre']);
        } else {
            incoLogWrite('REGISTRO TIEMPOS - OBTENER NOMBRE MAQUINA: NO SE HA ENCONTRADO EL CÓDIGO ' . $codigo, false);
        }
        pg_free_result($result);
        // pg_close($dbconn);
    }
    return $nombre;
}

function argInsertInDb($dbconn, $ptj = true, $fecha = null)
{
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    pg_prepare($dbconn, 'pedido', 'SELECT identificador, identificador_oferta FROM pedidos WHERE numero = $1');
    pg_prepare($dbconn, 'albaran', 'SELECT identificador, identificador_oferta FROM albaranes WHERE numero = $1');
    pg_prepare($dbconn, 'diario_tiempo', 'INSERT INTO diario_tiempo (fecha, operario, actividad, pedido, oferta, inicio, final, tiempo, albaran, nave, maquina, producto, observaciones, proyecto) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)');
    pg_prepare($dbconn, 'tl_operr', 'UPDATE tiempos_lecturas SET tratado = 1 WHERE operario = $1');
    pg_prepare($dbconn, 'tl_optrat', 'UPDATE tiempos_lecturas SET tratado = 1 WHERE operario = $1 AND fecha = $2 AND hora = $3');

    $final = null;
    $tiempo = 0;

    if (is_null($fecha)) $tiemposLecturas_query = 'SELECT fecha, hora, codigo, observaciones, proyecto FROM tiempos_lecturas WHERE tratado = 0 AND operario = $1 AND fecha >= current_date ORDER BY fecha, hora';
    else $tiemposLecturas_query = 'SELECT fecha, hora, codigo, observaciones, proyecto FROM tiempos_lecturas WHERE tratado = 0 AND operario = $1 AND fecha = \'' . $fecha . '\' ORDER BY fecha, hora';

    $result = pg_query($dbconn, 'SELECT DISTINCT operario FROM tiempos_lecturas WHERE tratado = 0 ORDER BY operario');
    if (pg_num_rows($result) <= 0) {
        incoLogWrite('REGISTRO DE TIEMPOS: NO HAY NADA QUE PROCESAR', false);
        return;
    }
    $operarios = pg_fetch_all($result);
    for ($i = 0; $i < count($operarios); $i++) {
        $opcbar = $operarios[$i]['operario'];
        if (!is_numeric($opcbar)) {
            // echo 'REGISTRO DE TIEMPOS: OPERARIO ERRONEO "' . $opcbar . '"' . PHP_EOL;
            pg_execute($dbconn, 'tl_operr', array($opcbar));
            continue;
        }
        $operario = intval($opcbar);
        $result = pg_query_params($dbconn, $tiemposLecturas_query, array($opcbar));
        if ($result === false || pg_num_rows($result) <= 0) continue;
        $tiempos = pg_fetch_all($result);
        for ($j = 0; $j < count($tiempos); $j++) {
            $fecha = $tiempos[$j]['fecha'];
            $hora = $tiempos[$j]['hora'];
            $codbar = $tiempos[$j]['codigo'];
            if (empty($codbar) || is_null($codbar)) {
                continue;
            }
            $observaciones = $tiempos[$j]['observaciones'];
            $proyecto = $tiempos[$j]['proyecto'];

            $oferta = null;
            $pedido = null;
            $albaran = null;
            list($numero, $producto, $actividad, $nave, $maquina) = explodeCodbar($codbar);
            $isPedido = $actividad != 8; // 8: CARGA

            switch ($nave) {
                case 1: $nave = 'A'; break;
                case 2: $nave = 'B'; break;
                case 3: $nave = 'C'; break;
                default: $nave = ' '; break;
            }

            if ($numero > 0) {
                $result = pg_execute($dbconn, ($isPedido ? 'pedido' : 'albaran'), array($numero));
                if (pg_num_rows($result) <= 0) {
                    // echo 'REGISTRO DE TIEMPOS: PEDIDO NO ENCONTRADO "' . $numero . '"' . PHP_EOL;
                    pg_execute($dbconn, 'tl_optrat', array($operario, $fecha, $hora));
                    continue;
                } else {
                    $aux = pg_fetch_all($result)[0];
                    $oferta = $aux['identificador_oferta'];
                    if ($isPedido) {
                        $pedido = $aux['identificador'];
                    } else {
                        $albaran = $aux['identificador'];
                    }
                }
            }

            pg_execute($dbconn, 'diario_tiempo', array($fecha, $operario, $actividad, $pedido, $oferta, $hora, $final, $tiempo, $albaran, $nave, $maquina, $producto, $observaciones, $proyecto));
            pg_execute($dbconn, 'tl_optrat', array($opcbar, $fecha, $hora));
        }

    }

    if ($ptj) {
        require_once __DIR__ . '/procesaTiemposJornada.php';
        procesaTiemposJornada($dbconn, $fecha);
    }

    // pg_close($dbconn);
}

function argResetDay($dbconn, $fecha) {
    // $dbconn = incoDbGetConnection(INCO_DB_LOCAL);
    pg_query_params($dbconn, 'update tiempos_lecturas set tratado = 0 where fecha = $1', [$fecha]);
    pg_query_params($dbconn, 'delete from diario_tiempo where fecha = $1', [$fecha]);
    pg_query_params($dbconn, 'delete from historico_tiempos where fecha = $1', [$fecha]);
    // pg_close($dbconn);
}