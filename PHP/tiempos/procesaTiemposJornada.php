<?php

function procesaTiemposJornada($dbconn, $param_fecha = null)
{
    // echo 'PROCESANDO TIEMPOS JORNADA...' . PHP_EOL;
    if (is_null($param_fecha)) $fecha = new DateTime();
    else $fecha = DateTime::createFromFormat('Y-m-d', $param_fecha);
    $fecha->setTime(0, 0, 0);

    // $dbconnptj = pg_connect($db_string) or incoLogWrite('No se ha podido conectar: ' . pg_last_error());

    $sql = "select codigo, trim(grupo) as grupo, l_jornada, l_manana_descanso_teo, l_tarde_descanso_teo, m_jornada, m_manana_descanso_teo, m_tarde_descanso_teo,
        x_jornada, x_manana_descanso_teo, x_tarde_descanso_teo, j_jornada, j_manana_descanso_teo, j_tarde_descanso_teo, v_jornada, v_manana_descanso_teo,
        v_tarde_descanso_teo from empleados where estado = 1 order by codigo";
    $result = pg_query($dbconn, $sql) or incoLogWrite('La consulta fallo [regtiejor - empleados]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('No hay empleados [regtiejor - empleados]: ' . pg_last_error());
    $empleados = pg_fetch_all($result);

    // buscamos la ultima fecha del historico con la jornada completada
    $sql = 'select to_char(max(fecha), \'YYYY-MM-DD\') as ultfecha from historico_tiempos where empleado = $1 and ((extract(dow from fecha) between 1 and 4 and manana_entra is not null and manana_sale is not null and tarde_entra is not null and tarde_sale is not null) or (extract(dow from fecha) = 5 and manana_entra is not null and manana_sale is not null))';
    if (!is_null($param_fecha)) $sql .= ' and fecha < \'' . $param_fecha . '\'';
    pg_prepare($dbconn, 'ultfecha', $sql);

    $sql = 'select count(*) as lns from historico_tiempos where empleado = $1 and fecha = $2';
    pg_prepare($dbconn, 'select_historico', $sql);

    $sql = 'insert into historico_tiempos (empleado, fecha, manana_entra, manana_sale, tarde_entra, tarde_sale, horas_efectivas, horas_teoricas, horas_extras, total_jornada, horas, observaciones) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)';
    pg_prepare($dbconn, 'insert_historico', $sql);

    $sql = 'update historico_tiempos set manana_entra = $3, manana_sale = $4, tarde_entra = $5, tarde_sale = $6, horas_efectivas = $7, horas_teoricas = $8 , horas_extras = $9 , total_jornada = $10, horas = $11, observaciones = $12 where empleado = $1 and fecha = $2';
    pg_prepare($dbconn, 'update_historico', $sql);

    $sql = 'select to_char(min(dt.inicio), \'HH24:MI\') as iniman from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.inicio <= \'14:30\' and dt.actividad = a.codigo and (dt.actividad = 44 or a.tipo = 5)';
    pg_prepare($dbconn, 'iniman', $sql);

    $sql = 'select to_char(max(dt.final), \'HH24:MI\') as finman from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.final < \'14:45\' and dt.actividad = a.codigo and (dt.actividad = 48 or a.tipo = 5)';
    pg_prepare($dbconn, 'finman', $sql);

    $sql = 'select to_char(min(dt.inicio), \'HH24:MI\') as initar from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.inicio >= \'14:45\' and dt.actividad = a.codigo and (dt.actividad = 44 or a.tipo = 5)';
    pg_prepare($dbconn, 'initar', $sql);

    $sql = 'select to_char(max(dt.final), \'HH24:MI\') as fintar from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.inicio >= \'14:45\' and dt.actividad = a.codigo and (dt.actividad = 48 or a.tipo = 5)';
    pg_prepare($dbconn, 'fintar', $sql);

    $sql = "select string_agg(trim(dt.observaciones), '. ') as obs from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.actividad = a.codigo and (dt.actividad in (44, 48) or a.tipo = 5) and dt.observaciones is not null and trim(dt.observaciones) != ''";
    pg_prepare($dbconn, 'obs', $sql);

    $fechaStr = $fecha->format('d/m/Y');
    $formatoHora = 'H:i';
    $formatoHoraDescanso = 'H:i:s';
    $seisYdiez = DateTime::createFromFormat($formatoHora, '18:10');
    $rango = rand(-3, 3);
    $intervaloDia = new DateInterval('P1D');
    $limite_horasExt = 0.25; // CUANTA COMO EXTRA SI SUPURA LAS 0.25 HORAS (15 MINUTOS), 0.417 HORAS (25 MINUTOS)
    foreach ($empleados as $empleado) {
        if ($empleado['codigo'] == 2) continue;

        $result = pg_execute($dbconn, 'ultfecha', array($empleado['codigo'])) or incoLogWrite('La consulta fallo [regtiejor - ultfecha - ' . $empleado['codigo'] . ']: ' . pg_last_error());
        $ultFecha = DateTime::createFromFormat('Y-m-d', pg_fetch_all($result)[0]['ultfecha']);
        if (!$ultFecha) {
            $ultFecha = new DateTime();
            $ultFecha->setTime(0, 0, 0);
        } else {
            $ultFecha->setTime(0, 0, 0);
            $ultFecha->add($intervaloDia);
        }

        while ($ultFecha <= $fecha) {
            $diaSemana = intval($ultFecha->format('N')); // 1 (para lunes) hasta 7 (para domingo)
            $ultFechaStr = $ultFecha->format('Y-m-d'); // $ultFecha->format('d/m/Y');
            $paramsSelect = array($empleado['codigo'], $ultFechaStr);

            $result = pg_execute($dbconn, 'iniman', $paramsSelect) or incoLogWrite('La consulta fallo [regtiejor - iniman - ' . $empleado['codigo'] . ']: ' . pg_last_error());
            $strIniMan = pg_num_rows($result) <= 0 ? '' : pg_fetch_all($result)[0]['iniman'];

            $result = pg_execute($dbconn, 'finman', $paramsSelect) or incoLogWrite('La consulta fallo [regtiejor - finman - ' . $empleado['codigo'] . ']: ' . pg_last_error());
            $strFinMan = pg_num_rows($result) <= 0 ? '' : pg_fetch_all($result)[0]['finman'];

            $result = pg_execute($dbconn, 'initar', $paramsSelect) or incoLogWrite('La consulta fallo [regtiejor - initar - ' . $empleado['codigo'] . ']: ' . pg_last_error());
            $strIniTar = pg_num_rows($result) <= 0 ? '' : pg_fetch_all($result)[0]['initar'];

            $result = pg_execute($dbconn, 'fintar', $paramsSelect) or incoLogWrite('La consulta fallo [regtiejor - fintar - ' . $empleado['codigo'] . ']: ' . pg_last_error());
            $strFinTar = pg_num_rows($result) <= 0 ? '' : pg_fetch_all($result)[0]['fintar'];

            $result = pg_execute($dbconn, 'obs', $paramsSelect) or incoLogWrite('La consulta fallo [regtiejor - obs - ' . $empleado['codigo'] . ']: ' . pg_last_error());
            $strObs = pg_num_rows($result) <= 0 ? '' : pg_fetch_all($result)[0]['obs'];
            $strObs = empty($strObs) ? '' : trim($strObs);

            if (empty($strIniMan) && empty($strFinMan) && empty($strIniTar) && empty($strFinTar)) {
                $ultFecha->add($intervaloDia);
                continue;
            }

            // OBTENEMOS EL DIA DE LA SEMANA PARA SABER QUE HORARIO DE JORNADA Y DESCANSOS SE UTILIZA
            $jornada = 0;
            $descansoMan = 0;
            $descansoTar = 0;
            switch ($diaSemana) {
                case 1:
                    $jornada = $empleado['l_jornada'];
                    if (!is_null($empleado['l_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['l_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoMan = $horas + $minutos;
                    }
                    if (!is_null($empleado['l_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['l_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoTar = $horas + $minutos;
                    }
                    break;
                case 2:
                    $jornada = $empleado['m_jornada'];
                    if (!is_null($empleado['m_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['m_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoMan = $horas + $minutos;
                    }
                    if (!is_null($empleado['m_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['m_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoTar = $horas + $minutos;
                    }
                    break;
                case 3:
                    $jornada = $empleado['x_jornada'];
                    if (!is_null($empleado['x_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['x_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoMan = $horas + $minutos;
                    }
                    if (!is_null($empleado['x_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['x_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoTar = $horas + $minutos;
                    }
                    break;
                case 4:
                    $jornada = $empleado['j_jornada'];
                    if (!is_null($empleado['j_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['j_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoMan = $horas + $minutos;
                    }
                    if (!is_null($empleado['j_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['j_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoTar = $horas + $minutos;
                    }
                    break;
                case 5:
                    $jornada = $empleado['v_jornada'];
                    if (!is_null($empleado['v_manana_descanso_teo'])) {
                        $dtDescanso = DateTime::createFromFormat($formatoHoraDescanso, $empleado['v_manana_descanso_teo']);
                        $horas = floatval($dtDescanso->format('H'));
                        $minutos = floatval($dtDescanso->format('i')) / 60;
                        $descansoMan = $horas + $minutos;
                    }
                    break;
            }

            $dtIniMan = empty($strIniMan) ? null : DateTime::createFromFormat($formatoHora, $strIniMan);
            $dtFinMan = empty($strFinMan) ? null : DateTime::createFromFormat($formatoHora, $strFinMan);
            $dtIniTar = empty($strIniTar) ? null : DateTime::createFromFormat($formatoHora, $strIniTar);
            $dtFinTar = empty($strFinTar) ? null : DateTime::createFromFormat($formatoHora, $strFinTar);

            $horasOrd = 0;
            if (!is_null($dtIniMan) && !is_null($dtFinMan)) {
                $dif = $dtFinMan->diff($dtIniMan);
                $horas = floatval($dif->format('%h'));
                $minutos = floatval($dif->format('%i')) / 60;
                $horasOrd += ($horas + $minutos - $descansoMan);
            }
            if (!is_null($dtIniTar) && !is_null($dtFinTar)) {
                switch ($empleado['grupo']) {
                    case 'TA1':
                        $dif = intval($seisYdiez->diff($dtFinTar)->format('%i'));
                        if ($dif < 0) {
                            $dtFinTar = DateTime::createFromFormat($formatoHora, '18:00');
                            if ($rango > 0) $dtFinTar->add(new DateInterval('PT' . abs($rango) . 'M'));
                            else $dtFinTar->sub(new DateInterval('PT' . abs($rango) . 'M'));
                            $strFinTar = $dtFinTar->format('H:i');
                        }
                        break;
                    case 'TA2':
                        $difHor = intval($dtFinTar->diff($dtIniTar)->format('%h'));
                        $difMin = intval($dtFinTar->diff($dtIniTar)->format('%i'));
                        $dif = $difHor + $difMin / 60;
                        if ($dif > 3) {
                            $difMin = ($dif - 3) * 60;
                            $dtIniTar->add(new DateInterval('PT' . $difMin . 'M'));
                            if ($rango > 0) $dtIniTar->add(new DateInterval('PT' . abs($rango) . 'M'));
                            else $dtIniTar->sub(new DateInterval('PT' . abs($rango) . 'M'));
                            $strIniTar = $dtIniTar->format('H:i');
                        }
                        break;
                }
                $dif = $dtFinTar->diff($dtIniTar);
                $horas = floatval($dif->format('%h'));
                $minutos = floatval($dif->format('%i')) / 60;
                $horasOrd += ($horas + $minutos - $descansoTar);
            }
            if (!is_null($dtIniMan) && is_null($dtFinMan) && is_null($dtIniTar) && !is_null($dtFinTar)) {
                $dif = $dtFinTar->diff($dtIniMan);
                $horas = floatval($dif->format('%h'));
                $minutos = floatval($dif->format('%i')) / 60;
                $horasOrd += ($horas + $minutos - ($descansoMan + $descansoTar));
            }

            // OBTENEMOS LAS HORAS EXTRAS EN FUNCIÓN DE LAS HORAS DE LA JORNADA
            $horas = $horasOrd;
            $horasExt = 0;
            if ($horasOrd > $jornada) {
                $horasExt = $horasOrd - $jornada;
                $horasOrd = $jornada;

                if ($horasExt <= $limite_horasExt) {
                    $horasExt = 0;
                }
            }

            if ($horasOrd >= $jornada - 0.25) {
                $horasOrd = $jornada;
            }

            $paramsInsertUpdate = array($empleado['codigo'], $ultFechaStr, $strIniMan, $strFinMan, $strIniTar, $strFinTar, $horasOrd, $jornada, $horasExt, $horasOrd + $horasExt, $horas, $strObs);
            $result = pg_execute($dbconn, 'select_historico', $paramsSelect) or incoLogWrite('La consulta fallo [regtiejor - select_historico - ' . $empleado['codigo'] . ']: ' . pg_last_error());
            $select_historico = pg_num_rows($result) <= 0 ? 0 : pg_fetch_all($result)[0]['lns'];
            if ($select_historico == 0) {
                pg_execute($dbconn, 'insert_historico', $paramsInsertUpdate);
            } else {
                $selected = pg_select($dbconn, 'historico_tiempos', ['empleado' => $empleado['codigo'], 'fecha' => $ultFechaStr]);
                if (!empty($selected)) {
                    $paramsInsertUpdate[11] = trim($selected[0]['observaciones']) . trim($paramsInsertUpdate[11]);
                }
                pg_execute($dbconn, 'update_historico', $paramsInsertUpdate);
            }

            $ultFecha->add($intervaloDia);
        }
    }
}