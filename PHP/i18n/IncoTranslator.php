<?php

use Stichoza\GoogleTranslate\TranslateClient;

/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/03/2018
 * Time: 10:55
 */

class IncoTranslator
{
    /**
     * @var string
     */
    private $source;

    /**
     * @var string
     */
    private $target;

    /**
     * @var TranslateClient
     */
    private $gtc;

    /**
     * @var resource
     */
    private $dbconn;

    /**
     * IncoTranslator constructor.
     * @param string $db_string
     * @param string $target
     * @throws Exception
     */
    public function __construct(string $db_string, string $target)
    {
        if (is_null($target) || empty($target) || is_numeric($target)) {
            $target = 'es';
        }

        $this->dbconn = pg_connect($db_string) or $this->log('No se ha podido conectar: ' . pg_last_error());
        $this->source = 'es';
        $this->target = $target;
        $this->gtc = new TranslateClient($this->source, $this->target);
    }

    /**
     * @param string $text
     * @return string
     */
    public function getTranslate(string $text): string
    {
        $textTr = array();

        if ($this->source == $this->target) {
            return $text;
        }

        $words = explode(' ', $text);
        for ($i = 0; $i < count($words); $i++) {
            $word = $words[$i];

            $result = pg_query_params($this->dbconn, 'SELECT fr, en FROM diccionario WHERE es = $1', array($word));
            if ($result === false || pg_num_rows($result) <= 0) {
                $wordTr = '';
            } else {
                $wordTr = trim(pg_fetch_all($result)[0][$this->target]);
            }

            if (empty($wordTr)) {
                try {
                    $txt = $this->gtc->translate(trim($word));
                } catch (Exception $exception) {
                    // echo 'CATCH: ' . $exception->getMessage() . PHP_EOL;
                    $txt = trim($word);
                }
                $textTr[] = $txt;
            } else {
                $textTr[] = $wordTr;
            }
        }

        return implode(' ', $textTr);
    }

    /**
     * @param string $msg
     */
    private function log(string $msg) {
        error_log($msg);
        die();
    }
}