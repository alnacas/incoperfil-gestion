<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/03/2018
 * Time: 10:55
 */

class IncoTranslatorFix
{
    const default_domain = 'es';

    /**
     * @var array
     */
    private static $DIC = array(
        'oferta' => array(
            'title' => array('es' => 'Oferta', 'en' => 'Offer', 'fr' => 'Offre'),
            'h_1' => array('es' => 'Hoja:', 'en' => 'Page:', 'fr' => 'Feuille:'),
            'h_2' => array('es' => 'Oferta Nº', 'en' => 'Offer No.', 'fr' => 'Offre Non'),
            'h_3' => array('es' => 'Fecha', 'en' => 'Date', 'fr' => 'Date'),
            'h_4' => array('es' => 'N.I.F.', 'en' => '', 'fr' => ''),
            'h_5' => array('es' => 'A la atención de', 'en' => 'For the attention of', 'fr' => 'Un le attention de'),
            'h_6' => array('es' => 'Obra', 'en' => 'Work', 'fr' => 'Travail'),
            'b_1' => array('es' => 'Partida', 'en' => 'Departure', 'fr' => 'Départ'),
            'b_2' => array('es' => 'Descripción', 'en' => 'Description', 'fr' => 'Description'),
            'b_3' => array('es' => 'Cantidad', 'en' => 'Quantity', 'fr' => 'Montant'),
            'b_4' => array('es' => 'UM', 'en' => '', 'fr' => ''),
            'b_5' => array('es' => 'Precio', 'en' => 'Price', 'fr' => 'Prix'),
            'b_6' => array('es' => 'Importe', 'en' => 'Amount', 'fr' => 'Montant'),
            'b_7' => array('es' => 'Transporte material', 'en' => 'Transport material', 'fr' => 'Transport materiel'),
            'b_8' => array('es' => 'Incrementos si procede no incluidos en esta oferta, ver hoja anexa de condiciones',
                'en' => 'Increments if proceeds not included in this offer, see annexed conditions',
                'fr' => 'Augmentations si nécessaire no inclus en ce offre, regarder feuille annexe de conditions'),
            'b_9' => array('es' => 'Oferta sujeta a las siguientes condiciones:', 'en' => 'Offer subject to the following conditions:', 'fr' => 'Offre sujet un las suivant conditions:'),
            'b_10' => array('es' => 'Válida hasta', 'en' => 'Valid until', 'fr' => 'Validité'),
            'b_11' => array('es' => 'Plazo de entrega', 'en' => 'Delivery term', 'fr' => 'Délai livraison'),
            'b_12' => array('es' => 'Los precios indicados están sujetos al recargo financiero correspondiente a la forma de pago acordada',
                'en' => 'The indicated prices are based on the financial surcharge corresponding to the agreed payment method.',
                'fr' => 'Les prix indiqués sont soumis à la surtaxe financière correspondant au mode de paiement convenu'),
            'b_13' => array('es' => 'Oferta no válida para cantidades inferiores a las indicadas',
                'en' => 'Offer not valid for amounts below indicated',
                'fr' => 'Offre non valable pour des montants inférieurs à celles indiquées'),
            'b_14' => array('es' => 'Forma de pago:', 'en' => 'Way to pay:', 'fr' => 'Mode de paiement:'),
            'b_15' => array('es' => 'Condiciones de pago:', 'en' => 'Conditions of payment:', 'fr' => 'Conditions de paiement:'),
            'b_16' => array('es' => 'Sujetas a concesión de crédito por entidad aseguradora. En caso de no obtener crédito, el pago será anticipado. Debido a las fluctuaciones en el precio de suministro del acero, nuestra oferta podrá ser revisada en función de dichos incrementos de precio. Precios aplicables exclusivamente a pedidos cuya referencia de obra se haga constar este número de oferta. I.V.A. no incluido. El pedido se ajustará a las condiciones generales de venta CGV (ver hojas anexas).',
                'en' => 'Subject to granting credit by insurance company. In case of not getting credit advance payments. Due to fluctuations in the steel supply price, our offer may be revised based on such price increases. Prices applicable exclusively to orders whose work reference is included in this offer number. I.V.A. not included. The order will be adjusted to the general manufacturing conditions, as well as the price increases, if applicable, according to our current Rate.',
                'fr' => 'Sous réserve de réponse de l’assurance de crédit, en cas de ne pas avoir ligne de crédit, payement à l’avance. En raison des fluctuations du prix de fourniture de l\'acier, notre offre peut être modifiée en fonction de l’augmentation des prix du matériel. Prix applicables exclusivement aux commandes dont la référence de travail est indiqués dans ce devis. TVA non inclus. La commande sera adaptée aux conditions générales de fabrication, ainsi qu’aux augmentations de prix, le cas échéant, selon notre tarif en vigueur.'),
            'b_17' => array('es' => 'Condiciones de embalaje de los perfiles', 'en' => 'Conditions of packing of the profiles', 'fr' => 'Conditions de emballage de los profils'),
            'b_18' => array('es' => 'Cantidad mínima', 'en' => 'Minimum amount', 'fr' => 'Montant minimum'),
            'b_19' => array('es' => 'Cantidad máxima', 'en' => 'Maximum amount', 'fr' => 'Montant maximum'),
            'b_20' => array('es' => 'Unidades pares siempre', 'en' => 'Even units', 'fr' => 'Unités couple toujours'),
            'b_21' => array('es' => 'Nota: Los paquetes inferiores a los indicados en esta tabla tendrán un incremento por paquete de 13 euros',
                'en' => 'Note: Packages lower than those indicated in this table will have an increment per package of 13 euros.',
                'fr' => 'Note : Les paquets inférieurs aux indiqués dans ce tableau ont une augmentation de prix de 13 euros par paquet.'),
            'b_22' => array('es' => 'Condiciones de fabricación de los remates', 'en' => ' Manufacturing conditions of the trimming', 'fr' => 'Conditions de fabrication de los raccordements'),
            'b_23' => array('es' => 'Formato', 'en' => 'Format', 'fr' => 'Format'),
            'b_24' => array('es' => 'Longitud a determinar', 'en' => 'Length to be determined', 'fr' => 'Longueur un déterminer'),
            'b_25' => array('es' => 'Nota:', 'en' => 'Note:', 'fr' => 'Note:'),
            'b_26' => array('es' => 'Siempre se factura formato completo. Solo se utiliza una longitud de remate por formato. Los restos de chapa generados se facturan por m2 como chapa lisa. El precio del remate se entiende por metro lineal siendo la longitud mínima a facturar de 1 ml/pieza.',
                'en' => 'You always invoice full format. Only one finishing length per format is used. The generated sheet metal remains are invoiced per m2 as smooth sheet. The auction price is understood as the linear meter, with the minimum invoicing length of 1 ml / piece.',
                'fr' => 'On facture toujours le format complet. Une seule longueur de finition par format est utilisée. Les restes de tôle générés sont facturés au m2 sous forme de feuille lisse. Le prix du raccordement est par mètre linéaire. La longueur minimum pour facturer 1 ml / pièce.'),
            'b_27' => array('es' => 'Exceso de plegados:', 'en' => 'Excess of bends:', 'fr' => 'Excédent de pliage:'),
            'b_28' => array('es' => 'Los precios son para remates con 6 plegados como máximo. El exceso de plegados se facturará a 0,85 E/ml por plegado añadido.',
                'en' => 'Prices are for auctions with a maximum of 6 folded. The excess of pleats will be billed at 0.85 E / ml per added fold.',
                'fr' => 'Les prix respecte un maximum de 6 plis. Les plies excédentaires seront facturées à 0,85 euros/ml.'),
            'b_29' => array('es' => 'Características de los materiales', 'en' => 'Characteristics of the materials', 'fr' => 'Caractéristiques de los matériaux'),
            'b_30' => array('es' => 'Material Denominación', 'en' => 'Material Denomination', 'fr' => 'Matériau Dénomination'),
            'b_31' => array('es' => 'Anverso Espesor mínimo (um)', 'en' => 'Obverse Minimum thickness (um)', 'fr' => 'Avers Épaisseur minimum (um)'),
            'b_32' => array('es' => 'Reverso Espesor mínimo (um)', 'en' => 'Reverse Minimum thickness (um)', 'fr' => 'Reverso Épaisseur minimum (um)'),
            'b_33' => array('es' => 'Recubrimiento Zinc o AZ (um)', 'en' => 'Zinc or AZ coating (um)', 'fr' => 'Revêtement Zinc o AZ (um)'),
            'b_34' => array('es' => 'Clase Acero', 'en' => 'Steel Class', 'fr' => 'Classe Acier'),
            'b_35' => array('es' => 'CE EN 508', 'en' => '', 'fr' => ''),
            'b_36' => array('es' => 'Certificado Material', 'en' => 'Material Certificate', 'fr' => 'Certificat Matériau'),
            'b_37' => array('es' => 'Transporte no incluido', 'en' => 'Transport not included', 'fr' => 'Transport pas inclus'),
            'b_38' => array('es' => 'Transportes adicionales', 'en' => 'Additional transport', 'fr' => 'Transport supplémentaire'),
            'f_1' => array('es' => 'Fecha, firma y sello del cliente', 'en' => 'Date, signature and seal of the customer', 'fr' => 'Signature et seal del client'),
            'f_2' => array('es' => 'FGE-01-01 Revisión: 1 Fecha: 01/04/05', 'en' => '', 'fr' => ''),
            'f_3' => array('es' => 'Conforme con la oferta y con las CGV.', 'en' => '', 'fr' => ''),
        ),
        'albaran' => array(
            'title' => array('es' => 'Albarán', 'en' => 'Delivery note', 'fr' => 'B. Livraison'),
            'title_2' => array('es' => 'Carta porte', 'en' => 'Carta porte', 'fr' => 'Carta porte'),
            'h_1' => array('es' => 'Hoja:', 'en' => 'Page:', 'fr' => 'Feuille:'),
            'h_2' => array('es' => 'Dirección de entrega de la mercancia', 'en' => 'Delivery address of the merchandise', 'fr' => 'Adresse de livraison de la marchandise'),
            'h_3' => array('es' => 'Ref. Obra:', 'en' => '', 'fr' => ''),
            'h_4' => array('es' => 'Pedido Nº', 'en' => 'Order Nº', 'fr' => 'Commande Nº'),
            'h_5' => array('es' => 'Fecha', 'en' => 'Date', 'fr' => 'Date'),
            'h_6' => array('es' => 'Pedido Cl.', 'en' => 'Order Cl.', 'fr' => 'Commande Cl.'),
            'h_7' => array('es' => 'Cliente / Consignatario', 'en' => 'Cliente / Consignatario', 'fr' => 'Cliente / Consignatario'),
            'h_8' => array('es' => 'Expedidor / Cargador', 'en' => 'Expedidor / Cargador', 'fr' => 'Expedidor / Cargador'),
            'h_9' => array('es' => 'Porte Nº', 'en' => 'Porte Nº', 'fr' => 'Porte Nº'),
            'b_1' => array('es' => 'Cantidad', 'en' => 'Quantity', 'fr' => 'Montant'),
            'b_2' => array('es' => 'Descripción', 'en' => 'Description', 'fr' => 'Description'),
            'b_3' => array('es' => 'Long', 'en' => '', 'fr' => ''),
            'b_4' => array('es' => 'Paq', 'en' => '', 'fr' => ''),
            'b_5' => array('es' => 'Transporte material', 'en' => 'Transport material', 'fr' => 'Transport materiel'),
            'b_6' => array('es' => 'Exceso en número de portes', 'en' => 'Excess in number of postage', 'fr' => 'Excès de nombre d\'affranchissement'),
            'f_1' => array('es' => 'Es responsabilidad del cliente cualquier defecto', 'en' => 'It is the responsibility of the customer any defect', 'fr' => 'Il est de la responsabilité du client tout défaut'),
            'f_2' => array('es' => 'Que se produzca en el material por no llevar toldo', 'en' => 'That occurs in the material for not wearing awning', 'fr' => 'Cela se produit dans le matériau pour ne pas porter auvent'),
            'f_3' => array('es' => 'Naturaleza de la mercancia', 'en' => 'Nature of the merchandise', 'fr' => 'Nature de la marchandise'),
            'f_4' => array('es' => 'Perfiles metálicos', 'en' => 'Metalic profile', 'fr' => 'Profils métalliques'),
            'f_5' => array('es' => 'Peso (kg)', 'en' => 'Weight (kg)', 'fr' => 'Poids (kg)'),
            'f_6' => array('es' => 'Bultos', 'en' => 'Packages', 'fr' => 'Forfaits'),
            'f_7' => array('es' => 'Receptor mercancia. Firma', 'en' => 'Merchandise receiver. Signature', 'fr' => 'Récepteur de marchandises. Signature'),
            'f_8' => array('es' => 'Operadora y cargadora', 'en' => 'Operator and loader', 'fr' => 'Opérateur et chargeur'),
            'f_9' => array('es' => 'Transportista/porteador', 'en' => 'Carrier', 'fr' => 'Transporteur'),
            'f_10' => array('es' => 'Matricula', 'en' => '', 'fr' => ''),
            'f_11' => array('es' => 'Conductor. Firma y dni', 'en' => 'Driver. Signature and ID', 'fr' => 'Pilote. Signature and ID'),
            'f_12' => array('es' => 'Dni', 'en' => '', 'fr' => ''),
            'f_13' => array('es' => 'Inscrita en el Registro Mercantil de Valencia, Folio 127 Tomo 4.361 Libro 1.673 Sección Gral. Hoja V-22.870 Inscripcion 4. NIF A46265526',
                'en' => '',
                'fr' => ''),
        ),
        'factura' => array(
            'title' => array('es' => 'Factura', 'en' => 'Invoice', 'fr' => 'Facture'),
            'title_1' => array('es' => 'Factura anticipo', 'en' => 'Advance invoice', 'fr' => 'Facture per acompte'),
            'title_2' => array('es' => 'Factura rectificativa', 'en' => 'Rectifying invoice', 'fr' => 'Rectification de facture'),
            'title_proforma' => array('es' => 'Factura proforma', 'en' => '', 'fr' => 'Facture proforme'),
            'h_1' => array('es' => 'Hoja:', 'en' => 'Page:', 'fr' => 'Feuille:'),
            'h_2' => array('es' => 'Factura Nº', 'en' => 'Invoice Nº', 'fr' => 'Facture Nº'),
            'h_2_proforma' => array('es' => 'F. Proforma Nº', 'en' => '', 'fr' => 'F. Proforme Nº'),
            'h_3' => array('es' => 'Fecha', 'en' => 'Date', 'fr' => 'Date'),
            'h_4' => array('es' => 'N.I.F', 'en' => '', 'fr' => ''),
            'b_1' => array('es' => 'Cantidad', 'en' => 'Quantity', 'fr' => 'Montant'),
            'b_2' => array('es' => 'Descripción', 'en' => 'Description', 'fr' => 'Description'),
            'b_3' => array('es' => 'Precio (€)', 'en' => 'Price (€)', 'fr' => 'Prix (€)'),
            'b_4' => array('es' => 'Dto.', 'en' => '', 'fr' => ''),
            'b_5' => array('es' => 'Importe (€)', 'en' => 'Amount (€)', 'fr' => 'Montant (€)'),
            'b_6' => array('es' => 'Albarán:', 'en' => 'Delivery Note:', 'fr' => 'B. Livraison:'),
            'b_6_proforma' => array('es' => 'Numero:', 'en' => 'Number:', 'fr' => 'Numéro:'),
            'b_7' => array('es' => 'Pedido:', 'en' => 'Order:', 'fr' => 'Commande:'),
            'b_8' => array('es' => 'Oferta:', 'en' => 'Offer:', 'fr' => 'Offre:'),
            'b_9' => array('es' => 'Ref. Obra:', 'en' => '', 'fr' => ''),
            'b_10' => array('es' => 'Transporte material', 'en' => 'Transport material', 'fr' => 'Transport materiel'),
            'b_11' => array('es' => 'Exceso en número de portes', 'en' => 'Excess in number of postage', 'fr' => 'Excès de nombre d\'affranchissement'),
            'b_12' => array('es' => 'Factura de anticipo nº', 'en' => 'Advance invoice nº', 'fr' => 'Facture per acompte nº'),
            'b_13' => array('es' => 'Sin cargo', 'en' => 'No fee', 'fr' => 'Gratuit'),
            'b_14' => array('es' => 'Ref. Cliente:', 'en' => '', 'fr' => ''),
            'r_1' => array('es' => 'Factura rectificativa a la factura número', 'en' => 'Rectifying invoice to invoice number', 'fr' => 'Rectification de facture au numéro de facture'),
            'r_2' => array('es' => 'Por diferencia de precios', 'en' => 'For differences in prices', 'fr' => 'Par différence de prix'),
            'f_1' => array('es' => 'Total sin recargos', 'en' => 'Total without surcharges', 'fr' => 'Total sans surtaxes'),
            'f_2' => array('es' => 'Descuento Pronto Pago', 'en' => 'Discount Soon Payment', 'fr' => 'Réduction Bientôt Paiement'),
            'f_3' => array('es' => 'Base imponible', 'en' => 'Tax base', 'fr' => 'Base d\'imposition'),
            'f_4' => array('es' => 'Tipo IVA', 'en' => 'Type IVA', 'fr' => 'Type IVA'),
            'f_5' => array('es' => 'Importe IVA', 'en' => 'Amount IVA', 'fr' => 'Montant IVA'),
            'f_6' => array('es' => 'Total factura', 'en' => 'Total invoice', 'fr' => 'Facture totale'),
            'f_7' => array('es' => 'Domicilio de Pago', 'en' => 'Payment address', 'fr' => 'Paiement'),
            'f_8' => array('es' => 'Vencimientos', 'en' => 'Due dates', 'fr' => 'Dates d\'échéance'),
            'f_9' => array('es' => 'Importes', 'en' => 'Amounts', 'fr' => 'Montants'),
            'f_10' => array('es' => 'Anticipado', 'en' => 'Anticipated', 'fr' => 'Anticipé'),
            'f_11' => array('es' => 'Producto fabricado en España', 'en' => 'Producto fabricado en España', 'fr' => 'Produit fabriqué en Espagne'),
            'f_12_proforma' => array(
                'es' => 'El pedido se ajustará a las condiciones generales de venta CGV (ver hojas anexas).',
                'en' => 'The order will be adjusted to the general manufacturing conditions, as well as the price increases, if applicable, according to our current Rate.',
                'fr' => 'La commande sera adaptée aux conditions générales de fabrication, ainsi qu’aux augmentations de prix, le cas échéant, selon notre tarif en vigueur.'
            ),
            'f_13' => array('es' => 'Ver forma de pago arriba (*)', 'en' => 'See payment method above (*)', 'fr' => 'Voir paiement ci-dessus (*)'),
        ),
        'certificado' => array(
            'title' => array('es' => 'Certificado de calidad', 'en' => 'Quality certificate', 'fr' => 'Certification de la qualité'),
            'h_1' => array('es' => 'Hoja:', 'en' => 'Page:', 'fr' => 'Feuille:'),
            'b_1' => array('es' => 'Fecha', 'en' => 'Date', 'fr' => 'Date'),
            'b_2' => array('es' => 'Pedido', 'en' => 'Order', 'fr' => 'Commande'),
            'b_3' => array('es' => 'S/Ref', 'en' => '', 'fr' => ''),
            'b_4' => array('es' => 'Obra', 'en' => 'Work', 'fr' => 'Travail'),
            'b_5' => array('es' => 'Especificaciones Técnicas y de Calidad de los Productos Utilizados',
                'en' => 'Technical and Quality Specifications of the Products Used',
                'fr' => 'Spécifications Techniques et de Qualité des Produits Utilisés'),
            'b_6' => array('es' => 'Número de bobina', 'en' => 'Coil number', 'fr' => 'Numéro de bobine'),
            'b_7' => array('es' => 'Espesor (mm)', 'en' => 'Thickness (mm)', 'fr' => 'Épaisseur (mm)'),
            'b_8' => array('es' => 'Color', 'en' => 'Color', 'fr' => 'Couleur'),
            'b_9' => array('es' => 'Desarrollo (mm)', 'en' => '', 'fr' => ''),
            'b_10' => array('es' => 'Re (N/mm2)', 'en' => '', 'fr' => ''),
            'b_11' => array('es' => 'Calidad', 'en' => 'Quality', 'fr' => 'Qualité'),
            'b_12' => array('es' => 'Remax (N/mm2)', 'en' => '', 'fr' => ''),
            'b_13' => array('es' => 'Zinc (g/m2)', 'en' => '', 'fr' => ''),
            'b_14' => array('es' => 'A%', 'en' => '', 'fr' => ''),
            'b_15' => array('es' => 'Tipo de certificado', 'en' => '', 'fr' => ''),
            'b_16' => array('es' => 'Pintura Cara A', 'en' => '', 'fr' => ''),
            'b_17' => array('es' => 'Pintura Cara B', 'en' => '', 'fr' => ''),
            'f_1' => array('es' => 'Normativa Aplicable', 'en' => 'Applicable regulations', 'fr' => 'Règlements applicables'),
            'f_2' => array('es' => 'Normativa de Calidad', 'en' => 'Quality Regulations', 'fr' => 'Règlement de qualité'),
            'f_3' => array('es' => 'Normativa de Tolerancia', 'en' => 'Tolerance Policy', 'fr' => 'Politique de tolérance'),
            'f_4' => array('es' => 'Normativa de Fabricación', 'en' => 'Manufacturing Regulations', 'fr' => 'Règlement de fabrication'),
            'f_5' => array('es' => 'Resp. Dpto. de Calidad', 'en' => '', 'fr' => ''),
            'f_6' => array('es' => 'Nota: Los datos que aparecen en este certificado son los mismos que aparecen en el certificado original de calidad del fabricante',
                'en' => 'Note: The information that appears in this certificate is the same as the original quality certificate of the manufacturer',
                'fr' => 'Remarque: Les informations figurant dans ce certificat sont identiques à celles du certificat de qualité d\'origine du fabricant'),
        ),
        'proforma' => array(
            'title' => array('es' => 'Factura proforma', 'en' => '', 'fr' => 'Facture proforme'),
            'h_1' => array('es' => 'Hoja:', 'en' => 'Page:', 'fr' => 'Feuille:'),
            'h_2' => array('es' => 'F. Proforma Nº', 'en' => '', 'fr' => 'F. Proforme Nº'),
            'h_3' => array('es' => 'Fecha', 'en' => 'Date', 'fr' => 'Date'),
            'h_4' => array('es' => 'N.I.F', 'en' => '', 'fr' => ''),
            'b_1' => array('es' => 'Cantidad', 'en' => 'Quantity', 'fr' => 'Montant'),
            'b_2' => array('es' => 'Descripción', 'en' => 'Description', 'fr' => 'Description'),
            'b_3' => array('es' => 'Precio', 'en' => 'Price', 'fr' => 'Prix'),
            'b_4' => array('es' => 'Dto.', 'en' => '', 'fr' => ''),
            'b_5' => array('es' => 'Importe', 'en' => 'Amount', 'fr' => 'Montant'),
            'b_6' => array('es' => 'Numero:', 'en' => 'Number:', 'fr' => 'Numéro:'),
            'b_7' => array('es' => 'Pedido:', 'en' => 'Order:', 'fr' => 'Commande:'),
            'b_8' => array('es' => 'Oferta:', 'en' => 'Offer:', 'fr' => 'Offre:'),
            'b_9' => array('es' => 'Ref. obra:', 'en' => '', 'fr' => ''),
            'b_10' => array('es' => 'Transporte material', 'en' => 'Transport material', 'fr' => 'Transport materiel'),
            'b_11' => array('es' => 'Exceso en número de portes', 'en' => 'Excess in number of postage', 'fr' => 'Excès de nombre d\'affranchissement'),
            'f_1' => array('es' => 'Total sin recargos', 'en' => 'Total sans surcharges', 'fr' => 'Total sans surcharges'),
            'f_2' => array('es' => 'Base imponible', 'en' => 'Tax base', 'fr' => 'Base d\'imposition'),
            'f_3' => array('es' => 'Tipo IVA', 'en' => 'Type IVA', 'fr' => 'Type IVA'),
            'f_4' => array('es' => 'Importe IVA', 'en' => 'Amount IVA', 'fr' => 'Montant IVA'),
            'f_5' => array('es' => 'Total factura', 'en' => 'Total invoice', 'fr' => 'Facture totale'),
            'f_6' => array('es' => 'Domicilio de Pago', 'en' => 'Payment address', 'fr' => 'Adresse de paiement'),
            'f_7' => array('es' => 'Producto fabricado en España', 'en' => 'Producto fabricado en España', 'fr' => 'Produit fabriqué en Espagne'),
        ),
        'packinlist' => array(
            'title' => array('es' => 'Packing list', 'en' => '', 'fr' => ''),
            'h_1' => array('es' => 'Hoja:', 'en' => 'Page:', 'fr' => 'Feuille:'),
            'h_2' => array('es' => 'Albarán:', 'en' => 'D. Note:', 'fr' => 'B. Livraison'),
            'h_3' => array('es' => 'Tipo de contenedor:', 'en' => 'Container Type:', 'fr' => 'Type de conteneur:'),
            'h_4' => array('es' => 'Número de contenedor:', 'en' => 'Container number:', 'fr' => 'Numéro du conteneur:'),
            'h_5' => array('es' => 'Factura:', 'en' => 'Invoice:', 'fr' => 'Facture:'),
            'b_1' => array('es' => 'Paq.', 'en' => '', 'fr' => ''),
            'b_2' => array('es' => 'Descripción', 'en' => 'Description', 'fr' => 'Description'),
            'b_3' => array('es' => 'Cantidad', 'en' => 'Quantity', 'fr' => 'Montant'),
            'b_4' => array('es' => 'Long', 'en' => '', 'fr' => ''),
            'b_5' => array('es' => 'Kgs total', 'en' => '', 'fr' => ''),
            'b_6' => array('es' => 'Total numero de bultos', 'en' => 'Total number of packages', 'fr' => 'Nombre total de colis'),
            'b_7' => array('es' => 'Total peso kgs. paq.', 'en' => 'Total weight kgs. paq.', 'fr' => 'Poids total kgs. paq.'),
            'b_8' => array('es' => 'Numero de precinto', 'en' => 'Seal number', 'fr' => 'Numéro de sceau'),
            'f_1' => array('es' => 'Firma:', 'en' => 'Signature:', 'fr' => 'Signature:'),
            'f_2' => array('es' => 'Fecha:', 'en' => 'Date:', 'fr' => 'Date:'),
            'f_3' => array('es' => 'Inscrita en el Registro Mercantil de Valencia, Folio 127 Tomo 4.361 Libro 1.673 Sección Gral. Hoja V-22.870 Inscripcion 4. NIF A46265526',
                'en' => '',
                'fr' => ''),
        )
    );

    /**
     * @var string
     */
    private $pdf;

    /**
     * @var string
     */
    private $target;

    /**
     * @var resource
     */
    private $dbconn;

    /**
     * IncoTranslatorFix constructor.
     * @param resource $dbconn
     * @param string $pdf
     * @param string $target
     */
    public function __construct($dbconn, string $pdf, string $target = 'es')
    {
        $this->dbconn = $dbconn;
        $this->pdf = $pdf;
        $this->target = $target;
    }

    /**
     * @param string $seccion
     * @param bool $upper
     * @return string
     */
    public function getTranslate(string $seccion, bool $upper = false): string
    {
        if (is_null($this->target) || empty($this->target)) {
            $this->target = 'es';
        }
        
        if (isset(self::$DIC[$this->pdf][$seccion])) {
            $wordEs = self::$DIC[$this->pdf][$seccion]['es'];
            $res = self::$DIC[$this->pdf][$seccion][$this->target];

            if (is_null($res) || empty($res)) {
                $res = $wordEs;
            }

            if ($upper) {
                $res = strtoupper($res);
                $res = str_replace(
                	['á', 'à', 'é', 'è', 'í', 'ì', 'ó', 'ò', 'ú', 'ù', 'ñ'],
	                ['Á', 'À', 'É', 'È', 'Í', 'Ì', 'Ó', 'Ò', 'Ú', 'Ù', 'Ñ'],
	                $res
                );
            }

            return $res;
        } else {
            $res = $seccion;

            if ($this->target != 'es') {
                $words = explode(' ', $res);
                $res = '';
                for ($i = 0; $i < count($words); $i++) {
                    $result = pg_query_params($this->dbconn, 'SELECT fr, en FROM diccionario WHERE es = $1', array($words[$i]));

                    if ($result === false || pg_num_rows($result) <= 0) {
                        $wordTr = '';
                    } else {
                        $wordTr = trim(pg_fetch_all($result)[0][$this->target]);
                    }

                    if (empty($wordTr)) {
                        $res .= $words[$i] . ' ';
                    } else {
                        $res .= $wordTr . ' ';
                    }
                }

                $res = trim($res);
            }

            return $res;
        }
    }

    /**
     * @param resource $connection
     * @param string $text
     * @param string $target
     * @param array $options
     * @return array|string|string[]
     */
    public static function translate($connection, string $text, string $target, array $options = [])
    {
        if (empty($target) || $target == self::default_domain) {
            return self::translate_applyOptions($text, $options);
        }

        $words = explode(' ', $text);
        $words = array_map(function ($word) use ($connection, $target) {
            $result = pg_query_params($connection, 'SELECT fr, en FROM diccionario WHERE es = $1', [$word]);
            if ($result === false) return $word;
            $result = pg_fetch_all($result);
            if ($result === false) return $word;
            $word_translated = trim($result[0][$target]);
            return empty($word_translated) ? $word : $word_translated;
        }, $words);
        $text_translated = implode(' ', $words);

        return self::translate_applyOptions($text_translated, $options);
    }

    /**
     * @param string $text
     * @param array $options
     * @return array|string|string[]
     */
    private static function translate_applyOptions(string $text, array $options) {
        if (empty($options)) {
            return $text;
        }

        if (in_array('uppercase', $options)) {
            $text = str_replace(
                ['á', 'à', 'é', 'è', 'í', 'ì', 'ó', 'ò', 'ú', 'ù', 'ñ'],
                ['Á', 'À', 'É', 'È', 'Í', 'Ì', 'Ó', 'Ò', 'Ú', 'Ù', 'Ñ'],
                strtoupper($text)
            );
        }

        return $text;
    }

    /**
     * @param string $msg
     */
    private function log(string $msg) {
        error_log($msg);
        die();
    }

    public static function dic2pot($section) {
        if (!isset(self::$DIC[$section])) exit(sprintf('La sección "%s" no existe en el diccionario', $section));

        $file_pot = fopen(sprintf(__DIR__ . '/%s.pot', $section), 'w');
        $file_po_en = fopen(sprintf(__DIR__ . '/en/LC_MESSAGES/%s.po', $section), 'w');
        $file_po_es = fopen(sprintf(__DIR__ . '/es/LC_MESSAGES/%s.po', $section), 'w');
        $file_po_fr = fopen(sprintf(__DIR__ . '/fr/LC_MESSAGES/%s.po', $section), 'w');
        if ($file_pot === false || $file_po_en === false || $file_po_es === false || $file_po_fr == false) {
            exit('Los ficheros no se han podido crear');
        }

        $dic = self::$DIC[$section];
        foreach ($dic as $key => $langs) {
            fwrite($file_pot, sprintf('# key - %s' . PHP_EOL, $key));
            fwrite($file_pot, sprintf('msgid "%s"' . PHP_EOL, $langs['es']));
            fwrite($file_pot, 'msgstr ""' . PHP_EOL);
            fwrite($file_pot, PHP_EOL);

            fwrite($file_po_en, sprintf('# key - %s' . PHP_EOL, $key));
            fwrite($file_po_en, sprintf('msgid "%s"' . PHP_EOL, $langs['es']));
            fwrite($file_po_en, sprintf('msgstr "%s"' . PHP_EOL, $langs['en']));
            fwrite($file_po_en, PHP_EOL);

            fwrite($file_po_es, sprintf('# key - %s' . PHP_EOL, $key));
            fwrite($file_po_es, sprintf('msgid "%s"' . PHP_EOL, $langs['es']));
            fwrite($file_po_es, sprintf('msgstr "%s"' . PHP_EOL, $langs['es']));
            fwrite($file_po_es, PHP_EOL);

            fwrite($file_po_fr, sprintf('# key - %s' . PHP_EOL, $key));
            fwrite($file_po_fr, sprintf('msgid "%s"' . PHP_EOL, $langs['es']));
            fwrite($file_po_fr, sprintf('msgstr "%s"' . PHP_EOL, $langs['fr']));
            fwrite($file_po_fr, PHP_EOL);
        }

        fclose($file_pot);
        fclose($file_po_en);
        fclose($file_po_es);
        fclose($file_po_fr);

        echo 'POT y POs creado' . PHP_EOL;
    }

}