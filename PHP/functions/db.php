<?php

function incoDbMysqlGetConnection($db_config) {
    $conn = new mysqli(
        $db_config['host'],
        $db_config['user'],
        $db_config['password'],
        $db_config['dbname'],
        $db_config['port']
    );
    if ($conn->connect_error) {
        incoLogWrite( 'No se ha podido establecer la conexión con la base de datos: ' . $conn->connect_error . ' - ' . json_encode($db_config));
    }

    if (isset($db_config['options']['charset'])) $conn->set_charset($db_config['options']['charset']);

    return $conn;
}