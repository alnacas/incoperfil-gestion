<?php

/**
 * @param int $cliente
 * @param int $pedido
 * @return string
 */
function incoPathConfirmacionPedidoFolder($cliente) {
    return sprintf(INCO_DIR_CLIENTES . '%d/ConfirmacionesPedidos/', $cliente);
}

/**
 * @param int $cliente
 * @param int $pedido
 * @return string
 */
function incoPathConfirmacionPedidoPdf($cliente, $pedido) {
    return sprintf(incoPathConfirmacionPedidoFolder($cliente) . 'CP_%06d.pdf', $pedido);
}