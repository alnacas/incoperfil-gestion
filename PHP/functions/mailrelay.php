<?php

/**
 * @return Swift_SmtpTransport
 */
function incoMailrelayGetTransport() {
	return (new Swift_SmtpTransport(INCO_MAILRELAY_SMTP_HOST, INCO_MAILRELAY_SMTP_PORT))
		->setUsername(INCO_MAILRELAY_SMTP_USER)
		->setPassword(INCO_MAILRELAY_SMTP_PASSWORD)
        ->setEncryption('tls');
}

/**
 * @return Swift_Mailer
 */
function incoMailrelayGetMailer() {
	return new Swift_Mailer(incoMailrelayGetTransport());
}