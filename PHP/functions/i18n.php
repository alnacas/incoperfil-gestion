<?php

/**
 * @param string $locale
 * @param string $domain
 */
function incoI18nSetLang(string $locale, string $domain) {
    if ($locale == 'en') $locale = 'en_GB';
    if ($locale == 'es') $locale = 'es_ES';
    if ($locale == 'fr') $locale = 'fr_FR';
    if (empty($locale)) $locale = 'es_ES';

    $locale .= '.utf8';

    if (setlocale(LC_ALL, $locale) === false && !putenv('LANG=' . $locale)) incoLogWrite('No se ha podido configurar el idioma: ' . $locale);
    if (bindtextdomain($domain, INCO_DIR_I18N) === false) incoLogWrite('Se ha producido un error al ejecutar "bindtextdomain" para el dominio "' . $domain . '"');
    if (!textdomain($domain)) incoLogWrite('Se ha producido un error al ejecutar "textdomain" para el dominio "' . $domain . '"');
}