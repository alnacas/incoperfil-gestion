<?php

require_once __DIR__ . '/../../../config.php';

incoLogSetFile('calidad_encuestas_clientes');

incoLogWrite('Se inicia el proceso', false);

$opts = getopt('', ['no-load', 'no-download', 'test', 'test-email::']);
if ((isset($opts['test']) && !isset($opts['test-email'])) || (!isset($opts['test']) && isset($opts['test-email']))) {
    incoLogWrite('Ejecución de pruebas pero faltan datos, argumentos necesario: test, test-email');
}
if (isset($opts['test-email'])) {
    incoLogWrite('---- Ejecución de test, se desactiva la descarga', false);
    $opts['test-email'] = explode(INCO_EMAIL_MESSAGE_ADDRESS_SEPARATOR, $opts['test-email']);
    $opts['no-download'] = '';
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);
$dbconnRemote = incoDbGetConnection(INCO_DB_REMOTE);

// generamos y subimos las nuevas encuestas
if (isset($opts['no-load'])) {
    incoLogWrite('Se desactiva la carga de encuestas', false);
} else {
    $fecha = new DateTime('-2week');
    $resource = pg_query_params($dbconn, 'select cliente, max(fecha) as fecha from albaranes where fecha <= $1 and encuesta_realizada = 0 group by 1 order by 2', [$fecha->format('Y-m-d')]);
    if ($resource === false) {
        incoLogWrite('[DB local] Se ha producido un error al seleccionar los datos: ' . pg_last_error());
    }
    if (pg_num_rows($resource) <= 0) {
        incoLogWrite(sprintf('No hay clientes con albaranes cuya fecha sea anterior o igual al %s', $fecha->format('d/m/Y')), false);
    } else {
        $comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 11]);
        if ($comunicacion === false) incoLogWrite('La comunicación no existe');
        $comunicacion = $comunicacion[0];
        $senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion);

        $mailer = incoMailrelayGetMailer();

        $albaranes = pg_fetch_all($resource);
        foreach ($albaranes as $albaran) {
            if (isset($opts['test'])) {
                $receivers = $opts['test-email'];
            } else {
                $receivers = incoSmtpGetReceiverUsers($dbconn, $comunicacion, $albaran['cliente'], false);
                if (empty($receivers)) {
                    incoLogWrite(sprintf('[DB local] El cliente %s no tiene destinatarios', $albaran['cliente']), false);
                    continue;
                }
            }

            // buscamos un grupo de envio por si el mismo cliente tuviese varios albaranes en la misma fecha
            $result = pg_query($dbconn, 'select coalesce(max(grupo_envio), 0) + 1 as next from comunicaciones_envios');
            $grupoEnvio = isset($opts['test']) ? 0 : pg_fetch_all($result)[0]['next'];

            // buscamos el token para el formulario y simular la encuesta anónima
            if (isset($opts['test'])) {
                $token = sprintf('test_%s', date('Ymd'));
            } else {
                $isValidToken = false;
                $trials = 0;
                while (!$isValidToken && $trials <= 100) {
                    $trials++;
                    $token = md5(uniqid(rand(), true));
                    $result = pg_query_params($dbconn, 'select * from comunicaciones_envios where token = $1', [$token]);
                    if ($result === false) {
                        incoLogWrite('[DB local] No se ha podido generar el token del cliente: ' . pg_last_error($dbconn) . ' - ' . json_encode($albaran), false);
                        continue 2;
                    }
                    $selected = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
                    $isValidToken = empty($selected);
                }
                if ($trials > 100) {
                    incoLogWrite('[DB local] No se ha podido generar el token del cliente (intentos excedidos): ' . pg_last_error($dbconn) . ' - ' . json_encode($albaran), false);
                }
            }

            // insertamos el grupo de albaranes asociados a una misma encuesta (lo normal será que solo haya un albarán por cliente)
            pg_query_params($dbconn, 'insert into comunicaciones_envios (comunicacion, grupo_envio, token, cliente, albaran) select $1, $2, $3, cliente, identificador from albaranes where cliente = $4 and fecha <= $5 and encuesta_realizada = 0',
                [11, $grupoEnvio, $token, $albaran['cliente'], $fecha->format('Y-m-d')]);

            // insertamos la encuesta en la bd remota
            pg_insert($dbconnRemote, 'encuestas', ['token' => $token]);

            // generamos email
            $message = new Swift_Message($comunicacion['email_subject']);
            $body = incoSmtpGetBody($message, 'encuesta', [
                'BODY' => '',
                'SRC_IMAGE_ENCUESTA' => $message->embed(Swift_Image::fromPath(INCO_DIR_IMAGENES . 'mails/templates/templateEncuesta/encuesta.png')),
                'TOKEN' => $token
            ]);
            $message->setFrom(INCO_EMAIL_NOREPLY)
                ->setTo($receivers)
                ->setBody($body, 'text/html');
            if (!INCO_DEV_MODE && $comunicacion['tipo'] == 0) {
                if ($comunicacion['email_cc_interna']) $message->setCc($comunicacion['email_cc_interna']);
            }
            $message->setBcc('joseginer@incoperfil.com');

            $failedReceivers = [];
            $sent = $mailer->send($message, $failedReceivers);
            if ($sent == 0 || $sent != count($receivers)) {
                incoLogWrite(sprintf('[DB local] El cliente %s ha tenido problemas de envío con los destinatarios: %s', $albaran['cliente'], implode(', ', $failedReceivers)), false);
                if ($sent == 0) continue;
            }

            // actualizamos albaranes enviados
            if (!isset($opts['test'])) {
                pg_query_params($dbconn, 'update albaranes set encuesta_realizada = 1 where cliente = $1 and fecha <= $2 and encuesta_realizada = 0', [$albaran['cliente'], $fecha->format('Y-m-d')]);
            }
        }
    }
}

// descargamos las respuestas
if (isset($opts['no-download'])) {
    incoLogWrite('Se desactiva la descarga de encuestas', false);
} else {
    $resource = pg_query($dbconnRemote, 'select codigo, token, fecha, hora, respuesta_01_recomendacion, respuesta_01_comentario from encuestas where sync = 0 and fecha is not null');
    if ($resource === false) {
        incoLogWrite('[DB remota] No se han podido ejecutar la consulta para obtener las respuestas: ' . pg_last_error($dbconnRemote));
    }
    $encuestas = pg_num_rows($resource) > 0 ? pg_fetch_all($resource) : [];
    if ($encuestas === false) {
        incoLogWrite('[DB remota] No se han podido obtener las respuestas: ' . pg_last_error($dbconnRemote));
    }
    foreach ($encuestas as $encuesta) {
        $com_envio = pg_select($dbconn, 'comunicaciones_envios', ['token' => $encuesta['token']]);
        if (empty($com_envio)) {
            incoLogWrite('[DB local] No se ha encontrado el envío de la comunicación con el token: ' . $encuesta['token'], false);
            continue;
        }

        $inserted = pg_insert($dbconn, 'comenvrespuestas', [
            'comunicacion_envio' => $com_envio[0]['codigo'],
            'fecha' => $encuesta['fecha'],
            'hora' => $encuesta['hora'],
            'respuesta_01_recomendacion' => $encuesta['respuesta_01_recomendacion'],
            'respuesta_01_comentario' => $encuesta['respuesta_01_comentario'],
        ]);
        if ($inserted === false) {
            incoLogWrite('[DB local] No se ha podido registrar la respuesta: ' . pg_last_error($dbconn), false);
            continue;
        }

        $updated = pg_update($dbconnRemote, 'encuestas', ['sync' => 1], ['codigo' => $encuesta['codigo']]);
        if ($updated === false) {
            incoLogWrite('[DB remota] No se ha podido actualizar la respuesta como sincronizada: ' . pg_last_error($dbconnRemote), false);
        }
    }
}

incoLogWrite('Se finaliza el proceso', false);

pg_close($dbconn);
pg_close($dbconnRemote);