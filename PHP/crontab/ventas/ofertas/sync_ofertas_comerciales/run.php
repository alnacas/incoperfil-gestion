<?php

require_once __DIR__ . '/../../../../config.php';

incoLogSetFile('sync_ofertas_comerciales');
incoLogWrite('==== Se inicia la sincronización de vendedores y ofertas', false);

$dbconn_local = incoDbGetConnection(INCO_DB_LOCAL);
$dbconn_remote = incoDbGetConnection(INCO_DB_REMOTE);

$result = pg_query($dbconn_local, 'select filtros from scripts_cron where codigo = 1');
if (pg_num_rows($result) <= 0) {
    incoLogWrite('Valor de filtrado no existe');
}
try {
    $MIN_CANTIDAD = intval(trim(pg_fetch_all($result)[0]['filtros']));
} catch (Exception $exception) {
    incoLogWrite('Error al obtener el valor de filtrado: ' . $exception->getMessage());
}

// sincronización de vendedores
$vendedores_local = pg_select($dbconn_local, 'vendedor', ['estado' => 1]);
$hash = password_hash('Incoperfil+A46265526!', PASSWORD_BCRYPT);
$proceso = ['new' => 0, 'updated' => 0, 'error' => 0];
foreach ($vendedores_local as $vendedor_local) {
    $vendedor_remote = pg_select($dbconn_remote, 'vendedores', ['codigo' => $vendedor_local['codigo']]);
    if (empty($vendedor_remote)) {
        $codigo = intval($vendedor_local['codigo']);
        $responsable = in_array($codigo, [1, 2, 4]) ? 1 : 0;
        $result = pg_query_params($dbconn_remote, 'insert into vendedores (codigo, nombre, email, contacto, responsable, passwd) values  ($1, $2, $3, $4, $5, $6)', [$codigo, trim($vendedor_local['nombre']), trim($vendedor_local['email']), trim($vendedor_local['contacto']), $responsable, $hash]);
        if ($result === false) {
            $proceso['error']++;
            incoLogWrite('[Remote][Vendedores] No se ha podido insertar: ' . pg_last_error($dbconn_remote), false);
        } else {
            $proceso['new']++;
        }
    } else {
        $vendedor_remote = $vendedor_remote[0];
        if (trim($vendedor_local['nombre']) != trim($vendedor_remote['nombre']) || trim($vendedor_local['contacto']) != trim($vendedor_remote['contacto']) || trim($vendedor_local['email']) != trim($vendedor_remote['email'])) {
            $result = pg_update($dbconn_remote, 'vendedores',
                ['nombre' => trim($vendedor_local['nombre']), 'contacto' => trim($vendedor_local['contacto']), 'email' => trim($vendedor_local['email'])],
                ['codigo' => $vendedor_local['codigo']]
            );
            if ($result === false) {
                $proceso['error']++;
                incoLogWrite('[Remote][Vendedores] No se ha podido actualizar: ' . pg_last_error($dbconn_remote), false);
            } else {
                $proceso['updated']++;
            }
        }
    }
}
incoLogWrite('[Remote][Vendedores] Resultados: ' . json_encode($proceso), false);

// descarga y actualización de los estados de las ofertas
$ofertas_remote = pg_select($dbconn_remote, 'ofertas', ['sync' => 1]);
if ($ofertas_remote === false) $ofertas_remote = [];
$proceso = ['new' => 0, 'updated' => 0, 'error' => 0];
foreach ($ofertas_remote as $oferta_remote) {
    $result = pg_update($dbconn_local, 'ofertas',
        ['estado' => $oferta_remote['estado'], 'motivo' => $oferta_remote['motivo']],
        ['identificador' => $oferta_remote['identificador']]
    );
    if ($result === false) {
        $proceso['error']++;
        incoLogWrite('[Local][Ofertas] No se ha podido actualizar: ' . pg_last_error($dbconn_local), false);
    } else {
        pg_query_params($dbconn_local, 'insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, $1, $2, $3, $4)',
            ['cron_sync_ofertas_comerciales', 'ofertas', 'ACTUALIZA ESTADO', sprintf('OFERTA %s PASA A ESTADO %s', $oferta_remote['numero'], $oferta_remote['estado'])]);
        $proceso['updated']++;

        if ($oferta_remote['estado'] == 3) {
            $result = pg_update($dbconn_local, 'ofertas',
                ['motivo_obs' => $ofertas_remote['observaciones']],
                ['identificador' => $oferta_remote['identificador']]
            );
            if ($result === false) incoLogWrite('[Local][Ofertas] No se ha podido actualizar: ' . pg_last_error($dbconn_local), false);
        }
        if ($oferta_remote['estado'] != 5) {
            $result = pg_query_params($dbconn_local, 'update ofertaslin set estado = $1 where identificador = $2 and partida != $3', [$oferta_remote['estado'], $oferta_remote['identificador'], 'V']);
            if ($result === false) incoLogWrite('[Local][Ofertaslin] No se han podido actualizar: ' . pg_last_error($dbconn_local), false);
        }
    }
}
incoLogWrite('[Local][Ofertas] Resultados: ' . json_encode($proceso), false);
pg_delete($dbconn_remote, 'ofertas', ['sync' => 1]);

// carga y actualización de las ofertas
$result = pg_query_params($dbconn_local, 'select o.identificador, o.centro, o.ejercicio, o.serie, o.numero, o.revision, o.fecha, o.fecha_validez, o.potencial, o.cliente, trim(o.nombre) as nombre, o.importe, o.estado, trim(o.referencia_obra) as referencia_obra, trim(o.telefono) as telefono, trim(o.mail) as mail, trim(o.contacto) as contacto, o.vendedor, trim(contacto) as contacto from ofertas o, ofertaslin ol where o.fecha_validez <= current_date and (o.estado = 0 or (o.estado = 5 and current_date >= o.fecha_reapertura)) and ol.unidad_medida = 1 and o.identificador = ol.identificador group by o.identificador, o.fecha_validez having sum(ol.cantidad) > $1 order by o.fecha_validez, sum(ol.cantidad)', [$MIN_CANTIDAD]);
if ($result === false) incoLogWrite('[Local][Ofertas] No se han podido seleccionar las ofertas para cargarlas: ' . pg_last_error($dbconn_local));
$ofertas_local = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
$proceso = ['new' => 0, 'updated' => 0, 'error' => 0];
foreach ($ofertas_local as $oferta_local) {
    $oferta_remote = pg_select($dbconn_remote, 'ofertas', ['identificador' => $oferta_local['identificador']]);
    if (empty($oferta_remote)) {
        $result = pg_insert($dbconn_remote, 'ofertas',[
            'identificador' => $oferta_local['identificador'],
            'centro' => $oferta_local['centro'],
            'ejercicio' => $oferta_local['ejercicio'],
            'serie' => $oferta_local['serie'],
            'numero' => $oferta_local['numero'],
            'revision' => $oferta_local['revision'],
            'fecha' => $oferta_local['fecha'],
            'fecha_validez' => $oferta_local['fecha_validez'],
            'cliente' => $oferta_local['cliente'],
            'nombre' => $oferta_local['nombre'],
            'importe' => $oferta_local['importe'],
            'referencia_obra' => $oferta_local['referencia_obra'],
            'vendedor' => $oferta_local['vendedor'],
            'comercial' => $oferta_local['contacto'],
            'telefono' => $oferta_local['telefono'],
            'email' => $oferta_local['mail'],
            'estado' => $oferta_local['estado'],
            'sync' => 0
        ]);
        if ($result === false) {
            $proceso['error']++;
            incoLogWrite('[Remote][Ofertas] No se ha podido insertar: ' . pg_last_error($dbconn_remote), false);
        } else {
            $proceso['new']++;
        }
    } else {
        $result = pg_update($dbconn_remote, 'ofertas',
            [
                'centro' => $oferta_local['centro'],
                'ejercicio' => $oferta_local['ejercicio'],
                'serie' => $oferta_local['serie'],
                'numero' => $oferta_local['numero'],
                'revision' => $oferta_local['revision'],
                'fecha' => $oferta_local['fecha'],
                'fecha_validez' => $oferta_local['fecha_validez'],
                'cliente' => $oferta_local['cliente'],
                'nombre' => $oferta_local['nombre'],
                'importe' => $oferta_local['importe'],
                'referencia_obra' => $oferta_local['referencia_obra'],
                'vendedor' => $oferta_local['vendedor'],
                'comercial' => $oferta_local['contacto'],
                'telefono' => $oferta_local['telefono'],
                'email' => $oferta_local['mail'],
                'estado' => $oferta_local['estado'],
                'sync' => 0
            ],
            ['identificador' => $oferta_local['identificador']]
        );
        if ($result === false) {
            $proceso['error']++;
            incoLogWrite('[Remote][Ofertas] No se ha podido actualizar: ' . pg_last_error($dbconn_remote), false);
        } else {
            $proceso['updated']++;
        }
    }
}
incoLogWrite('[Remote][Ofertas] Resultados: ' . json_encode($proceso), false);

pg_close($dbconn_local);
pg_close($dbconn_remote);