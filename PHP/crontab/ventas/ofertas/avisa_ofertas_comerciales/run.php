<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30/07/2018
 * Time: 9:55
 */

require_once __DIR__ . '/../../../../config.php';

incoLogSetFile('aviso_ofertas_comerciales');

/* ESTE SCRIPT SE EJECUTARÁ CADA 2 MARTES */
$fecha = new DateTime();
$diaSemana = intval($fecha->format('N')); // Representación numérica ISO-8601 del día de la semana --> 1 (para lunes) hasta 7 (para domingo)
$semanaAnyo = intval($fecha->format('W')); // Número de la semana del año ISO-8601, las semanas comienzan en lunes

if ($diaSemana != 2 || $semanaAnyo % 2 != 0) {
    incoLogWrite('Hoy no es día de ejecución');
}

require_once __DIR__ . '/AvisoOfertasComercialesPdf.php';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query($dbconn, 'select filtros from scripts_cron where codigo = 1');
if (pg_num_rows($result) <= 0) {
    incoLogWrite('Valor de filtrado no existe');
}
try {
    $MIN_CANTIDAD = intval(trim(pg_fetch_all($result)[0]['filtros']));
} catch (Exception $exception) {
    incoLogWrite('Error al obtener el valor de filtrado: ' . $exception->getMessage());
}

pg_prepare($dbconn, 'ofertas_ptes', 'select o.identificador from ofertas o, ofertaslin ol where o.vendedor = $1 and o.estado = 0 and o.fecha_validez <= current_date and ol.unidad_medida = 1 and o.identificador = ol.identificador group by o.identificador, o.fecha_validez having sum(ol.cantidad) > $2 order by o.fecha_validez, sum(ol.cantidad)');

pg_prepare($dbconn, 'ofertas_cong', 'select o.identificador from ofertas o, ofertaslin ol where o.vendedor = $1 and o.estado = 5 and o.fecha_validez <= current_date and ol.unidad_medida = 1 and current_date >= o.fecha_reapertura and o.identificador = ol.identificador group by o.identificador, o.fecha_validez having sum(ol.cantidad) > $2 order by o.fecha_validez, sum(ol.cantidad)');

pg_prepare($dbconn, 'ofertas_info', 'select numero, revision, fecha, fecha_validez, fecha_reapertura, potencial, cliente, trim(nombre) as nombre, importe, estado, trim(referencia_obra) as referencia_obra, trim(telefono) as telefono, trim(mail) as mail, trim(contacto) as contacto from ofertas where identificador = $1');

/*pg_prepare($dbconn, 'clipon', 'select trim(nombre) as nombre, trim(comercial) as comercial from (select codigo, nombre, comercial, estado from clientes union all select codigo, nombre, comercial, estado from potenciales) as clipon where estado = 1 and codigo = $1');*/

$result = pg_query_params($dbconn, 'select codigo, trim(nombre) as nombre, trim(contacto) as contacto, lower(trim(email)) as email from vendedor where estado = 1 and enviodoc = $1', ['S']) or die('La consulta fallo [ofertas]: ' . pg_last_error());
$comerciales = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result);

$emails = array();
foreach ($comerciales as $comercial) {
    $resultPtes = pg_execute($dbconn, 'ofertas_ptes', [$comercial['codigo'], $MIN_CANTIDAD]);
    $resOfertasPtes = pg_num_rows($resultPtes) > 0 ? pg_fetch_all($resultPtes) : [];

    $resultCong = pg_execute($dbconn, 'ofertas_cong', [$comercial['codigo'], $MIN_CANTIDAD]);
    $resOfertasCong = pg_num_rows($resultCong) > 0 ? pg_fetch_all($resultCong) : [];

    if (empty($resOfertasPtes) && empty($resOfertasCong)) {
        incoLogWrite('(' . $comercial['codigo'] . ') ' . $comercial['nombre'] . ' --> No tiene ofertas para revisar', false);
        continue;
    }

    $ofertasPtes = array();
    foreach ($resOfertasPtes as $oferta) {
        $result = pg_execute($dbconn, 'ofertas_info', [$oferta['identificador']]);
        if (pg_num_rows($result) <= 0) {
            incoLogWrite('(' . $comercial['codigo'] . ') ' . $comercial['nombre'] . ' --> No hay información de la oferta pendiente "' . $oferta['identificador'] . '"', false);
            continue;
        }
        $ofertaInfo = pg_fetch_all($result)[0];

        /*if (isset($ofertaInfo['nombre']) && isset($ofertaInfo['contacto'])) {
            $nombre = trim($ofertaInfo['nombre']);
            $contacto = trim($ofertaInfo['contacto']);
            $result = pg_execute($dbconn, 'clipon', [$ofertaInfo['cliente']]);
            if (pg_num_rows($result) > 0 && (empty($nombre) || empty($contacto))) {
                $clipon = pg_fetch_all($result)[0];
                echo json_encode($clipon) . PHP_EOL;
                if (empty($nombre)) $ofertaInfo['nombre'] = $clipon['nombre'];
                if (empty($contacto)) $ofertaInfo['contacto'] = $clipon['comercial'];
            }
        }*/

        array_push($ofertasPtes, $ofertaInfo);
    }

    $ofertasCong = array();
    foreach ($resOfertasCong as $oferta) {
        $result = pg_execute($dbconn, 'ofertas_info', [$oferta['identificador']]);
        if (pg_num_rows($result) <= 0) {
            incoLogWrite('(' . $comercial['codigo'] . ') ' . $comercial['nombre'] . ' --> No hay información de la oferta congelada "' . $oferta['identificador'] . '"', false);
            continue;
        }
        $ofertaInfo = pg_fetch_all($result)[0];

        array_push($ofertasCong, $ofertaInfo);
    }

    $nombre = empty($comercial['contacto']) ? $comercial['nombre'] : $comercial['contacto'];

      $pdf = new AvisoOfertasComercialesPdf($comercial, $ofertasPtes, $ofertasCong);

      array_push($emails, ['email' => $comercial['email'], 'nombre' => $nombre, 'pdf' => $pdf->getFilePath()]);
}


// obtenemos el usuario
$result = pg_query_params('SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', array(4)) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];

$userName = trim($usuario['nombreusuario']);
$userName = ucwords(mb_strtolower($userName));
$userEmail = trim($usuario['email']);
$userPasswd = trim($usuario['passwdmail']);
$userMovil = trim($usuario['movil']);
$strFecha = $fecha->format('d-m-Y');

$mailer = incoSmtpGetMailer($userEmail, $userPasswd);

$formatTitle = 'INCOPERFIL | Revisión de ofertas pendientes a fecha %s';
$formatBody = 'Estimado %s,<br><br>le adjunto un listado de ofertas pendientes a fecha %s con la finalidad de realizar una actualización del estado de las mismas.<br><br>Confirme el estado actual del listado de ofertas.<br><br>Los estados tienen el siguiente significado:<br><ul><li>Pendiente: todavía no ha habido respuesta por parte del cliente.</li><li>Cancelado: el cliente decide no comprarnos. Indicar en Notas uno de los siguientes motivo:<ul><li>No cumple en plazo</li><li>Precio elevado</li><li>Falta de crédito</li><li>Servicio incompleto (requiere instalación)</li><li>Coste de transporte elevado</li><li>Cancelación de la obra</li><li>Imposible cambiar la solución prescrita</li><li>Cliente no ha ganado la obra</li></ul></li><li>Congelado: el cliente postpone la respuesta a medio o largo plazo. Indicar en Notas una fecha apróximada de la respuesta.</li></ul><br><br>Saludos.';

foreach ($emails as $email) {
    $msgTitle = sprintf($formatTitle, $strFecha);
    $msgBody = sprintf($formatBody, $email['nombre'], $strFecha);

    $message = new Swift_Message($msgTitle);

    $body = incoSmtpGetBody($message, 'usuario', [
        'BODY' => $msgBody,
        'NOMBRE' => $userName,
        'MOVIL' => $userMovil,
        'MAIL' => $userEmail,
    ]);

    $message->setFrom([$userEmail => $userName])
        ->setTo($email['email'])
        ->setBody($body, 'text/html');

    if (file_exists($email['pdf'])) {
        $message->attach(Swift_Attachment::fromPath($email['pdf']));
    } else {
        incoLogWrite('El correo se envía sin adjunto a ' . $email['email'] . ': ' . $email['pdf'], false);
    }

    try {
        $mailer->send($message);
    } catch (Exception $exception) {
        incoLogWrite('Fallo al enviar correo a ' . $email['email'] . ': ' . $exception->getMessage(), false);
    }

    // incoImapStoreMessage($userEmail, $userPasswd, INCO_IMAP_SENT_BOX, $message->toString());
}

incoLogWrite('Se ha/n enviado ' . count($emails) . ' correo/s', false);

pg_close($dbconn);