<?php

class AvisoOfertasComercialesPdf extends TCPDF
{

    /**
     * 1 --> codigo del comercial
     */
    const PATTERN_DIR_OUTPUT = __DIR__ . '/%s/';

    /**
     * 1 --> fecha del documento Ymd
     */
    const PATTERN_OUTPUT_NAME = 'OfertasPendientes_%s.pdf';

    const WIDTHS = [
        17, // Núm.Rev.: 10 + 7
        16, // Fecha
        16, // Validez
        36, // Razón Social
        16, // Importe
        36, // Ref. Obra
        64, // Persona Contacto + Email + Teléfono: 34 + 34 + 15
        22, // Estado: 34
        64 // Notas
    ];

    const ALIGNS = [
        'R', // Núm.Rev.
        'C', // Fecha
        'C', // Validez
        'L', // Razón Social
        'R', // Importe
        'L', // Ref. Obra
        'L', // Persona Contacto + Email + Teléfono
        'L', // Estado
        'L' // Notas
    ];

    const HEIGHT = 4;

    const MIN_HEIGHT = 9;

    const BORDER = 'B';

    const PATTERN_ESTADO = '<input type="radio" id="pndte_%1$s" name="estado_%1$s" value="0"/>&nbsp;Pendiente<br/><input type="radio" id="cance_%1$s" name="estado_%1$s" value="3"/>&nbsp;Cancelado<br/><input type="radio" id="conge_%1$s" name="estado_%1$s" value="5"/>&nbsp;Congelado';

    const PATTERN_NOTAS = '<textarea cols="35" id="notas_%1$s" name="notas_%1$s"></textarea>';

    /**
     * @var int
     */
    private $page_width;

    /**
     * @var string
     */
    private $dir_output;

    /**
     * @var string
     */
    private $output_name;

    /**
     * @var DateTime
     */
    private $fecha;

    /**
     * @var array
     */
    private $comercial;

    /**
     * @var int
     */
    private $num_ofertas;

    /**
     * DocumentoPdf constructor.
     * @param array $comercial
     * @param array $ofertasPtes
     * @param array $ofertasCong
     * @param string $orientation
     * @param string $unit
     * @param string $format
     * @param bool $unicode
     * @param string $encoding
     * @param bool $diskcache
     * @param bool $pdfa
     * @throws Exception
     */
    public function __construct(array $comercial, array $ofertasPtes, array $ofertasCong, $orientation = 'L', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        $this->fecha = new DateTime();
        $this->page_width = $orientation == 'P' ? 210 : 297;
        $this->dir_output = sprintf(self::PATTERN_DIR_OUTPUT, $comercial['codigo']);
        $this->output_name = sprintf(self::PATTERN_OUTPUT_NAME, $this->fecha->format('Ymd'));
        $this->comercial = $comercial;
        $this->num_ofertas = count($ofertasPtes);

        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);

        $this->SetLineWidth(0.05); // 1px = 0.2645 mm
        $this->SetMargins(5, 25);
        $this->SetFont('dejavusans', '', 8);
        $this->SetAutoPageBreak(true, 5);

        $this->AddPage();

        foreach ($ofertasPtes as $oferta) {
            $this->printLine($oferta);
        }

        if (!empty($ofertasCong)) {
            if (!empty($ofertasPtes)) {
                $this->Ln(self::MIN_HEIGHT);
            }
            $this->SetFont('dejavusans', 'B');
            $this->Cell(0,  self::HEIGHT, 'Ofertas congeladas', self::BORDER, 1, 'R');
            $this->SetFont('dejavusans', '');
        }

        foreach ($ofertasCong as $oferta) {
            $this->printLine($oferta);
        }

        if (!is_dir($this->dir_output)) {
            $oldmask = umask(0);
            mkdir($this->dir_output, 0755, true);
            umask($oldmask);
        }
        $file = $this->dir_output . $this->output_name;
        $this->Output($file, 'F');
    }

    public function Header()
    {
        $this->SetFont('dejavusans', 'B', 8);
        $this->SetY(5);

        $w = ($this->page_width - $this->lMargin - $this->rMargin) / 2;
        $this->Cell($w, self::HEIGHT, 'INGENIERIA Y CONSTRUCCIÓN EL PERFIL, S.A.', 0, 0, 'L');
        $this->Cell($w, self::HEIGHT, 'Ofertas pendientes de revisión a fecha ' . $this->fecha->format('d-m-Y') . ': ' . $this->num_ofertas, 0, 1, 'R');
        $this->Ln();

        $this->SetFont('dejavusans', '');
        $this->Cell(20, self::HEIGHT, 'Comercial:', 0, 0, 'L');
        $nombre = trim($this->comercial['contacto']);
        if (empty($nombre)) {
            $nombre = $this->comercial['nombre'];
        } else {
            $nombre .= ' (' . $this->comercial['nombre'] . ')';
        }
        $this->Cell(0, self::HEIGHT, $nombre, 0, 1, 'L');
        $this->Ln();

        $this->SetFont('dejavusans', 'B');
        $this->setCellPaddings(1, '', 1, '');
        $this->Cell(self::WIDTHS[0], self::HEIGHT, 'Núm.Rev', self::BORDER, 0, self::ALIGNS[0]);
        $this->Cell(self::WIDTHS[1], self::HEIGHT, 'Fecha', self::BORDER, 0, self::ALIGNS[1]);
        $this->Cell(self::WIDTHS[2], self::HEIGHT, 'Validez', self::BORDER, 0, self::ALIGNS[2]);
        $this->Cell(self::WIDTHS[3], self::HEIGHT, 'Razón Social', self::BORDER, 0, self::ALIGNS[3]);
        $this->Cell(self::WIDTHS[4], self::HEIGHT, 'Importe', self::BORDER, 0, self::ALIGNS[4]);
        $this->Cell(self::WIDTHS[5], self::HEIGHT, 'Ref. Obra', self::BORDER, 0, self::ALIGNS[5]);
        $this->Cell(self::WIDTHS[6], self::HEIGHT, 'Contacto', self::BORDER, 0, self::ALIGNS[6]);
        $this->Cell(self::WIDTHS[7], self::HEIGHT, 'Estado', self::BORDER, 0, self::ALIGNS[7]);
        $this->Cell(self::WIDTHS[8], self::HEIGHT, 'Notas', self::BORDER, 1, self::ALIGNS[8]);
    }

    public function Footer()
    {
    }

    public function getFilePath()
    {
        return $this->dir_output . $this->output_name;
    }

    /**
     * @param array $oferta
     */
    private function printLine(array $oferta) {
        $numrev = $oferta['numero'] . '.' . $oferta['revision'];
        $fecha = DateTime::createFromFormat('Y-m-d', $oferta['fecha'])->format('d-m-y');
        $strFechaField = $oferta['estado'] == 5 ? 'fecha_reapertura' : 'fecha_validez';
        $fechaValidez = DateTime::createFromFormat('Y-m-d', $oferta[$strFechaField])->format('d-m-y');
        $importe = floatval($oferta['importe']);
        $importe = number_format($importe, 0, ',', '.');
        $infoContacto = $oferta['contacto'] . PHP_EOL . $oferta['mail'] . PHP_EOL . $oferta['telefono'];
        $estado = sprintf(self::PATTERN_ESTADO, $numrev);
        $notas = sprintf(self::PATTERN_NOTAS, $numrev);

        $h = max(
            $this->getStringHeight(self::WIDTHS[3], $oferta['nombre'] . PHP_EOL, true),
            $this->getStringHeight(self::WIDTHS[5], $oferta['referencia_obra'] . PHP_EOL, true),
            $this->getStringHeight(self::WIDTHS[6], $infoContacto . PHP_EOL, true)
        );
        $h = $h < self::MIN_HEIGHT ? self::MIN_HEIGHT : ceil($h);

        $this->Cell(self::WIDTHS[0], $h, $numrev, self::BORDER, 0, self::ALIGNS[0], false, '', 0, false, 'T', 'T');
        $this->Cell(self::WIDTHS[1], $h, $fecha, self::BORDER, 0, self::ALIGNS[1], false, '', 0, false, 'T', 'T');
        $this->Cell(self::WIDTHS[2], $h, $fechaValidez, self::BORDER, 0, self::ALIGNS[2], false, '', 0, false, 'T', 'T');
        $this->MultiCell(self::WIDTHS[3], $h, $oferta['nombre'] . PHP_EOL, self::BORDER, self::ALIGNS[3], false, 0);
        $this->Cell(self::WIDTHS[4], $h, $importe, self::BORDER, 0, self::ALIGNS[4], false, '', 0, false, 'T', 'T');
        $this->MultiCell(self::WIDTHS[5], $h, $oferta['referencia_obra'] . PHP_EOL, self::BORDER, self::ALIGNS[5], false, 0);
        $this->MultiCell(self::WIDTHS[6], $h, $infoContacto . PHP_EOL, self::BORDER, self::ALIGNS[6], false, 0);
        $this->writeHTMLCell(self::WIDTHS[7], $h, $this->GetX(), $this->GetY(), $estado, self::BORDER, 0);
        $this->writeHTMLCell(self::WIDTHS[8], $h, $this->GetX(), $this->GetY(), $notas, self::BORDER, 1);
    }

}