<?php

require_once __DIR__ . '/../../../../config.php';

incoLogSetFile('avisa_pedidos_terminados');

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$res = pg_query($dbconn, 'select distinct por.codigo from portes por, porteslin porln, pedidos ped where por.estado = 0 and por.codigo = porln.porte and porln.pedido = ped.identificador group by por.codigo having count(distinct ped.identificador) = (select count(distinct ped2.identificador) from porteslin porln2, pedidos ped2 where por.codigo = porln2.porte and porln2.pedido = ped2.identificador and ped2.estado = 2) order by por.codigo') or incoLogWrite('La búsqueda de portes falló');
$cod_portes = pg_fetch_all($res);
if (empty($cod_portes)) {
	incoLogWrite('No hay portes que notificar');
}

pg_prepare($dbconn, 'sel_porte', 'select por.codigo::integer, pro.codigo::integer as agencia_codigo, pro.nombre as agencia_nombre, trim(por.destino_principal) as destino_principal, case por.tipo_camion when 0 then \'Completo\' when 1 then \'Parcial\' end as tipo_camion, coalesce(por.numero_descargas, 0)::integer as numero_descargas, por.metros_camion::numeric(6,2) from portes por, proveedores pro where por.codigo = $1 and por.agencia = pro.codigo');
pg_prepare($dbconn, 'sel_portelns', 'select pedido_numero::integer, trim(destino) as destino, peso::numeric(10,3), bultos::integer, bultos_porte from porteslin where porte = $1 order by linea');
pg_prepare($dbconn, 'upd_porte', 'update portes set estado = 5 where codigo = $1');

$layoutPorte = '<p style="text-align: center; background-color: #C00000; color: #FBFBFB; margin-bottom: 0; padding: 5px;"><b>Porte $PORTE$</b></p>
	<table style="width: 100%; border: solid 1px #C00000;">
		<tbody>
		<tr>
			<td style="width: 50%;  vertical-align: top; padding: 5px 10px; border-right: solid 1px #C00000;">
				<ul style="list-style: none; padding: 0; margin: 0;">
					<li><b>Agencia:</b> $AGENCIA$</li>
					<li><b>Destino ppal.:</b> $DESTPPAL$</li>
					<li><b>Nº de descargas:</b> $NDESCARGAS$</li>
					<li><b>Tipo de camión:</b> $TIPOCAMION$</li>
					<li><b>Metros de camión:</b> $METROSCAMION$</li>
				</ul>
			</td>
			<td style="width: 50%; vertical-align: top; padding: 5px 10px;">
				<table style="width: 100%; border-collapse: collapse;">
					<thead style="border-bottom: solid 1px;">
					<tr>
						<th style="text-align: left;">Pedido</th>
						<th style="text-align: left;">Destino</th>
						<th style="text-align: right;">Peso</th>
						<th style="text-align: right;">Bultos Tot.</th>
						<th style="text-align: right;">Bultos</th>
					</tr>
					</thead>
					<tbody>$PORTELNS$</tbody>
					<tfoot style="border-top: solid 1px;">
					<tr>
						<td></td>
						<td></td>
						<td style="text-align: right;">$TOTPESO$</td>
						<td style="text-align: right;">$TOTBULTOS$</td>
						<td style="text-align: right;">$TOTBULTOSPORTE$</td>
					</tr>
					</tfoot>
				</table>
			</td>
		</tr>
		</tbody>
	</table>';
$layoutPorteln = '<tr>
		<td style="text-align: left; vertical-align: top;">$PEDIDO$</td>
		<td style="text-align: left; vertical-align: top;">$DESTINO$</td>
		<td style="text-align: right; vertical-align: top">$PESO$</td>
		<td style="text-align: right; vertical-align: top">$BULTOS$</td>
		<td style="text-align: right; vertical-align: top">$BULTOSPORTE$</td>
	</tr>';

$msgBody = '';
foreach ($cod_portes as $cod_porte) {
	$res = pg_execute($dbconn, 'sel_porte', [$cod_porte['codigo']]);
	$porte = pg_fetch_all($res)[0];

	$res = pg_execute($dbconn, 'sel_portelns', [$cod_porte['codigo']]);
	$portelns = pg_fetch_all($res);

	pg_execute($dbconn, 'upd_porte', [$cod_porte['codigo']]);

	$html_lineas = '';
	foreach ($portelns as $porteln) {
		$html_lineas .= str_replace(
			['$PEDIDO$', '$DESTINO$', '$PESO$', '$BULTOS$', '$BULTOSPORTE$'],
			[$porteln['pedido_numero'], $porteln['destino'], $porteln['peso'], $porteln['bultos'], $porteln['bultos_porte']],
			$layoutPorteln
		);
	}

	$totPeso = array_sum(array_column($portelns, 'peso'));
	$totBultos = array_sum(array_column($portelns, 'bultos'));
	$totBultosPorte = array_sum(array_column($portelns, 'bultos_porte'));
	$msgBody .= str_replace(
		['$PORTE$', '$AGENCIA$', '$DESTPPAL$', '$NDESCARGAS$', '$TIPOCAMION$', '$METROSCAMION$', '$PORTELNS$', '$TOTPESO$', '$TOTBULTOS$', '$TOTBULTOSPORTE$'],
		[$porte['codigo'], trim($porte['agencia_nombre']), trim($porte['destino_principal']), $porte['numero_descargas'], $porte['tipo_camion'], $porte['metros_camion'], $html_lineas, $totPeso, $totBultos, $totBultosPorte],
		$layoutPorte
	);
}

$mailer = incoSmtpGetMailer(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD);

$message = new Swift_Message('INCOPERFIL | Porte/s para contratación');

$body = incoSmtpGetBody($message, 'noreply', ['BODY' => $msgBody]);

$message->setFrom(INCO_EMAIL_NOREPLY)
        ->setTo(['comercial@incoperfil.com', 'produccion@incoperfil.com'])
        ->setBody($body, 'text/html');

$mailer->send($message);

incoImapStoreMessage(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD, INCO_IMAP_SENT_BOX, $message->toString());