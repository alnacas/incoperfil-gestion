<?php

require __DIR__ . '/../../../../config.php';

incoLogSetFile('avisa_bobinas_misma_ubicacion');

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query($dbconn, 'select ubicacion, count(*) as bobs from bobinas where estado = 1 and almacen in (\'B\', \'C\') group by ubicacion having count(*) > 1 order by ubicacion') or incoLogWrite('La consulta fallo [bobinas]: ' . pg_last_error());
$ubicaciones = pg_num_rows($result) <= 0 ? [] : pg_fetch_all($result);

if (empty($ubicaciones)) {
    exit(0);
}

pg_prepare($dbconn, 'bobinas_ubicacion', 'select codigo from bobinas where ubicacion = $1');

$patt_li = '<li>%s: %s</li>';
$html_li = '';
foreach ($ubicaciones as $ubicacion) {
    $result = pg_execute($dbconn, 'bobinas_ubicacion', [$ubicacion['ubicacion']]);
    $bobinas = pg_num_rows($result) <= 0 ? [] : pg_fetch_all($result);
    $html_li .= sprintf($patt_li, $ubicacion['ubicacion'], implode(', ', array_column($bobinas, 'codigo')));
}
$html = '<h4>Listado de bobinas en la misma ubicación</h4><ul>' . $html_li . '</ul>';

$mailer = incoSmtpGetMailer(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD);

$message = new Swift_Message('INCOPERFIL | Bobinas misma ubicación');

$body = incoSmtpGetBody($message, 'noreply', ['BODY' => $html]);

$message->setFrom(INCO_EMAIL_NOREPLY)
    ->setTo('produccion@incoperfil.com')
    ->setBody($body, 'text/html');

$mailer->send($message);

incoImapStoreMessage(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD, INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);