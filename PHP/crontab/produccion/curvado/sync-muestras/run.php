<?php

require_once __DIR__ . '/../../../../config.php';

incoLogSetFile('crontab_sync_muestras');
incoLogWrite('==== Se inicia la sincronización', false);

$dbconn_local = incoDbGetConnection(INCO_DB_LOCAL);
$dbconn_remote = incoDbMysqlGetConnection(INCO_DB_REMOTE_MYSQL);

$stmt_insMuestras_remote = $dbconn_remote->prepare('insert into muestras (fecha, hora, id_espesor, cuerda, flecha, radio, profundidad, separacion) values (?, ?, ?, ?, ?, ?, ?, ?)');
if ($stmt_insMuestras_remote === false) {
    incoLogWrite('No se ha podido preparar el insert de las muestras: ' . $dbconn_remote->error);
}

$resource = pg_query($dbconn_local, "select 'VC' as tipo, vc.fecha, vc.hora, p.codigo_vps as producto, e.valor_venta as espesor, vc.cuerda, vc.flecha, vc.radio_real,
       case when vc.profundidad != vc.profundidad_ajuste then vc.profundidad_ajuste else vc.profundidad end as profundidad, vc.dist_golpes as separacion
    from verificaciones_curvadora vc, pedidoslin pl, productos p, espesores e
    where vc.dist_golpes >= 0 and
        vc.pedido = pl.identificador and vc.linea = pl.linea and
        pl.producto = p.codigo and
        pl.espesor = e.codigo
    union all
    select 'M' as tipo, mc.fecha, mc.hora, p.codigo_vps as producto, e.valor_venta as espesor, mc.cuerda, mc.flecha, mc.mcradio as radio_real,
       mc.profundidad, mc.separacion
    from muestras_curvadora mc, productos p, espesores e
    where mc.separacion >= 0 and
        mc.perfil = p.codigo and
        mc.espesor = e.codigo
    order by 2,3");
if ($resource === false) {
    incoLogWrite('No se ha podido realizar la consulta: ' . pg_last_error($dbconn_local));
}
$rows = pg_fetch_all($resource);
if (empty($rows)) {
    incoLogWrite('No se han encontrado muestras que sincronizar');
}

$dbconn_remote->query('truncate table muestras');

foreach ($rows as $row) {
    if (is_null($row['producto'])) {
        incoLogWrite('Producto no indicado. ' .json_encode($row) , false);
        continue;
    }

    $result = $dbconn_remote->query('select id from espesores_perfiles where id_perfil = ' . $row['producto'] . ' and espesor = ' . $row['espesor']);
    if ($result === false) {
        incoLogWrite('No se ha podido realizar la consulta del id del espesor. ' .json_encode($row) , false);
        continue;
    }
    $espesores = $result->fetch_all(MYSQLI_ASSOC);
    if (empty($espesores)) {
        incoLogWrite('No se ha encontrado el espesor ' . $row['espesor'] . ' del perfil ' . $row['producto'] . '. ' . json_encode($row), false);
        continue;
    }
    $id_espesor = $espesores[0]['id'];

    // fecha, hora, id_espesor, cuerda, flecha, radio, profundidad, separacion
    $binded = $stmt_insMuestras_remote->bind_param('ssiddidd', $row['fecha'], $row['hora'], $id_espesor, $row['cuerda'], $row['flecha'], $row['radio_real'], $row['profundidad'], $row['separacion']);
    if (!$binded) {
        incoLogWrite('No se ha podido emparejar los valores. ' . json_encode($row), false);
        continue;
    }
    $executed = $stmt_insMuestras_remote->execute();
    if (!$executed) {
        incoLogWrite('No se ha podido ejecutar el insert. ' . json_encode($row), false);
    }
}

incoLogWrite('==== Fin de la sincronización', false);

pg_close($dbconn_local);
$dbconn_remote->close();