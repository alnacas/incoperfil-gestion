<?php

require __DIR__ . '/../../../../config.php';

incoLogSetFile('aviso_caducidad_iso');

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$desde = new DateTime();
$hasta = new DateTime('+1 month');

$resource = pg_query_params($dbconn, 'select codigo, nombre, fecha_caducidad_iso9001 from proveedores where fecha_caducidad_iso9001 between ? and ?',
    [$desde->format('Y-m-d'), $hasta->format('Y-m-d')]);
$proveedores = pg_num_rows($resource) > 0 ? pg_fetch_all($resource) : [];
if (empty($proveedores)) {
    pg_close($dbconn);
    exit(0);
}

$patt_li = '<li>(%d) %s: %s</li>';
$html_li = '';
foreach ($proveedores as $proveedor) {
    $html_li .= sprintf($patt_li, $proveedor['codigo'], trim($proveedor['nombre']), $proveedor['fecha_caducidad_iso9001']);
}
$html = '<h4>Listado de proveedores con la ISO9001 a punto de caducar</h4><ul>' . $html_li . '</ul>';

$mailer = incoSmtpGetMailer(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD);

$message = new Swift_Message('INCOPERFIL | Proveedores: Aviso ISO9001');

$body = incoSmtpGetBody($message, 'noreply', ['BODY' => $html]);

$message->setFrom(INCO_EMAIL_NOREPLY)
    ->setTo('compras@incoperfil.com')
    ->setBody($body, 'text/html');

$mailer->send($message);

incoImapStoreMessage(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD, INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);