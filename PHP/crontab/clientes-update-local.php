<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$localConn = incoDbGetConnection(INCO_DB_LOCAL);
$remoteConn = incoDbGetConnection(INCO_DB_REMOTE);
if (!$localConn || !$remoteConn) {
	incoLogWrite( 'No se ha podido conectar a una de las bases de datos - Err db local: ' . pg_last_error($localConn) . ' - Err db remota: ' . pg_last_error($remoteConn));
}

$res = pg_query($remoteConn, 'select codigo, mensajeria, email_factura, persona_acepta, fecha_actualiza from clientes order by codigo');
if (!$res) {
	incoLogWrite('No se ha podido ejecutar la query');
}
if (pg_num_rows($res) <= 0) {
	incoLogWrite('No se han encontrado resultados');
}
$clientes = pg_fetch_all($res);

foreach ( $clientes as $cliente ) {
	$remoteFechaActualiza = DateTime::createFromFormat('Y-m-d', $cliente['fecha_actualiza']) or false;

	$res = pg_query_params($localConn, 'select fecha_actualiza from clientes where codigo = $1', [$cliente['codigo']]);
	if (!$res) continue;
	if (pg_num_rows($res) <= 0) continue;
	$localFechaActualiza = DateTime::createFromFormat('Y-m-d', pg_fetch_all($res)[0]['fecha_actualiza']) or false;

	if ($localFechaActualiza >= $remoteFechaActualiza) continue;

	$data = [
		'mensajeria' => $cliente['mensajeria'],
		'email_factura' => $cliente['email_factura'],
		'fecha_actualiza' => $cliente['fecha_actualiza']
	];
	$condition = [
		'codigo' => $cliente['codigo']
	];
	$updated = pg_update($localConn, 'clientes', $data, $condition);
	if (!$updated) {
		incoLogWrite( 'No se ha podido acualizar el cliente ' . $cliente['codigo'] . '. ' . pg_last_error($localConn));
	}

	if (trim($cliente['persona_acepta'])) {
		$res = pg_query_params($localConn, 'select max(linea) as linea from clientesobs where codigo = $1', [$cliente['codigo']]);
		if (!$res || pg_num_rows($res) <= 0) continue;
		$clienteobs = pg_fetch_all($res)[0];
		$linea = is_null($clienteobs['linea']) ? 1 : intval($clienteobs['linea']) + 1;
		if (!$clienteobs) continue;
		pg_insert($localConn, 'clientesobs', [
			'codigo' => $cliente['codigo'],
			'linea' => $linea,
			'fecha' => $cliente['fecha_actualiza'],
			'usuario' => 0,
			'observacion' => $cliente['persona_acepta'] . ' ha cambiado la forma de envío de facturas'
		]);
	}
}

pg_close($localConn);
pg_close($remoteConn);