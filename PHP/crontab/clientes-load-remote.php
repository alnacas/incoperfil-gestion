<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$localConn = incoDbGetConnection(INCO_DB_LOCAL);
$remoteConn = incoDbGetConnection(INCO_DB_REMOTE);
if (!$localConn || !$remoteConn) {
	incoLogWrite( 'No se ha podido conectar a una de las bases de datos - Err db local: ' . pg_last_error($localConn) . ' - Err db remota: ' . pg_last_error($remoteConn));
}

// obtenemos clientes
$res = pg_query($localConn, 'select codigo, nif as cif, nombre, idioma, mensajeria, email_factura from clientes where estado = 1 and codigo > 0');
if (!$res) {
	incoLogWrite('No se ha podido ejecutar la query');
}
if (pg_num_rows($res) <= 0) {
	incoLogWrite('No se han encontrado resultados');
}
$clientes = pg_fetch_all($res);

// borramos clientes remotos
pg_prepare($remoteConn, 'truncate_clientes', 'truncate table clientes') or incoLogWrite('No se ha podido preparar la sententica');
pg_execute($remoteConn, 'truncate_clientes', []) or incoLogWrite('No se ha podido ejecutar la sententica');

// cargamos clientes
foreach ( $clientes as $cliente ) {
	if (!isset($cliente['persona_acepta'])) {
		$cliente['persona_acepta'] = '';
	}
	$inserted = pg_insert($remoteConn, 'clientes', $cliente);
	if (!$inserted) {
		incoLogWrite( 'No se ha podido insertar el cliente ' . $cliente['codigo'] . '. ' . pg_last_error($localConn));
	}
}

pg_close($localConn);
pg_close($remoteConn);