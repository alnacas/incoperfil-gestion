<?php

if (!empty($_POST)) {
	require_once __DIR__ . '/../../../../PHP/config.php';

	$action = $_POST['action'] or '';
	switch ($action) {
		case 'verify-cif':
			$response = fnVerifyCif($_POST);
			break;
		case 'choose-mensajeria':
			$response = fnChooseMensajeria($_POST);
			break;
		default:
			$response = [
				'status' => 500,
				'message' => 'Acción no reconocia'
			];
			break;
	}

	exit(json_encode($response));
}

function fnGetConnection() {
	// cuando se suba al VPS hay que cambiarlo por SDB_STRING porque cambia el config.php
	$conn = incoDbGetConnection(INCO_DB_REMOTE);
	if (!$conn) {
		exit(['status' => 500, 'message' => 'No se ha podido conectar']);
	}
	return $conn;
}

/**
 * @param resource $conn
 */
function fnCloseConnection($conn) {
	pg_close($conn);
}

/**
 * @param array $req
 *
 * @return array
 */
function fnVerifyCif(array $req): array {
	$conn = fnGetConnection();

	$resource = pg_query_params($conn, 'select codigo, nombre, cif, mensajeria, email_factura from clientes where codigo = $1', [$req['fvc_cliente']]);
	if (!$resource || pg_num_rows($resource) <= 0) {
		return ['status' => 500, 'message' => 'No se ha obtenido ningún cliente. ' . pg_last_error($conn)];
	}
	$cliente = pg_fetch_all($resource)[0];
	if (trim($req['fvc_cif']) != trim($cliente['cif'])) {
		return ['status' => 500, 'message' => 'El CIF introducido no corresponde con el cliente. ' . pg_last_error($conn)];
	}

	fnCloseConnection($conn);

	return ['status' => 200, 'cliente' => $cliente];
}

/**
 * @param array $req
 *
 * @return array
 */
function fnChooseMensajeria(array $req) {
	$conn = fnGetConnection();

	$data = [
		'mensajeria' => $req['fcm_mensajeria'],
		'persona_acepta' => $req['fcm_persona_acepta'],
		'fecha_actualiza' => (new DateTime())->format('Y-m-d'),
		'email_factura' => implode(';', array_filter($req['fcm_email'], function ($email) { return !empty($email); }))
	];

	$condition = [
		'codigo' => $req['fcm_cliente']
	];

	$upd = pg_update($conn, 'clientes', $data, $condition);
	if (!$upd) {
		return ['status' => 500, 'message' => 'No se ha podido actualizar el cliente. ' . pg_last_error($conn)];
	}

	fnCloseConnection($conn);

	return ['status' => 200, 'message' => 'Datos actualizados con éxito'];
}