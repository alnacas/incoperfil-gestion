const FORM_CHOOSE_MENSAJERIA_EMAIL = '<div class="form-group row fcm-email" data-id="@id@">' +
        '<div class="offset-lg-4 col-lg-8">' +
            '<div class="input-group">' +
                '<input type="email" class="form-control" name="fcm_email[]" value="@val@">' +
                '<div class="input-group-append">' +
                    '<button class="btn btn-danger" type="button" onclick="fnRemoveEmail(\'@id@\');">' +
                        '<i class="fa fa-trash"></i>' +
                    '</button>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>'

let CLIENTE = null;

function fnGetCliente() {
    const urlParams = new URLSearchParams(window.location.search);
    CLIENTE = urlParams.get('cliente');
    $('#fvc_cliente, #fcm_cliente').val(CLIENTE);
}

/**
 *
 * @param {String} email
 */
function fnAddEmail(email = null) {
    console.log('adding email');
    const randId = String(Math.random()).split('.')[1];
    const child = FORM_CHOOSE_MENSAJERIA_EMAIL
        .replace(/@id@/g, randId)
        .replace(/@val@/g, (email == null ? '' : email))
    $('#form-choose-mensajeria-email').append(child);
}

function fnRemoveEmail(id) {
    console.log('removing email');
    $('div[data-id = "' + id + '"]').remove();
}

$(document).ready(function () {
    fnGetCliente();

    $('#form-verify-cif').submit(function (e) {
        const form = $('#form-verify-cif');

        form.find('input').removeClass('is-invalid');
        form.find('div.invalid-feedback').html('');
        form.find('#form-verify-cif-loading').removeClass('d-none');
        form.find('button').addClass('d-none');

        $.post('./php/manager.php', form.serialize(), function (response) {
            if (response['status'] === 500) {
                form.find('#form-verify-cif-loading').addClass('d-none');
                form.find('button').removeClass('d-none');
                form.find('div.invalid-feedback').html(response['message']);
                form.find('input').addClass('is-invalid');
                return;
            }

            const cliente = response['cliente'];
            $('#fcm_cliente_nombre').val(cliente['nombre']);
            $('#fcm_mensajeria').val(cliente['mensajeria']).change();
            if (cliente['email_factura'] != null) {
                const emails = cliente['email_factura'].split(';').filter(value => {
                    return value.trim().length > 0;
                });
                for (let i = 0; i < emails.length; i++) {
                    if (i === 0) {
                        $('#fcm_email').val(emails[i]);
                    } else {
                        fnAddEmail(emails[i]);
                    }
                }
            }

            $('#verify-cif').addClass('d-none');
            $('#choose-mensajeria').removeClass('d-none');
        }, 'json')
            .fail(function() {
                form.find('#form-verify-cif-loading').addClass('d-none');
                form.find('button').removeClass('d-none');
            });

        return false;
    });

    $('#fcm_mensajeria').change(function () {
        const function_ = ['2', '3'].indexOf($(this).val()) >= 0 ? 'removeClass' : 'addClass';
        const req = function_ === 'removeClass';
        $('#form-choose-mensajeria-email')[function_]('d-none')
            .find('input').prop('required', req);
        if (!req) {
            $('div[data-id]').remove();
        }
    });

    $('#form-choose-mensajeria').submit(function (e) {
        const form = $('#form-choose-mensajeria');

        form.find('#form-choose-mensajeria-loading').removeClass('d-none');
        form.find('button').addClass('d-none');

        $.post('./php/manager.php', form.serialize(), function (response) {
            if (response['status'] === 500) {
                form.find('#form-choose-mensajeria-loading').addClass('d-none');
                form.find('button').removeClass('d-none');
                alert(response['message']);
                return;
            }

            $('#choose-mensajeria').addClass('d-none');
            $('#result').removeClass('d-none').find('p').html(response['message']);
            $('#fcm_mensajeria').val('').change();
        }, 'json')
            .fail(function() {
                form.find('#form-choose-mensajeria-loading').addClass('d-none');
                form.find('button').removeClass('d-none');
            });

        return false;
    });
});