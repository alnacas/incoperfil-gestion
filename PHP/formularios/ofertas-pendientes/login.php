<?php

if ($_POST) {
    if (empty($_POST['email']) || empty($_POST['passwd'])) {
        header('Location: login.php?error=No se han enviado los datos necesarios');
        exit();
    }

    require_once __DIR__ . '/php/db_connector.php';

    $result = pg_query_params($_CONN, 'select codigo, nombre, contacto, responsable, passwd from vendedores where email = $1', [$_POST['email']]);
    if ($result === false || pg_num_rows($result) <= 0) {
        header('Location: login.php?error=El usuario o la contraseña no son correctos');
        exit();
    }
    $db_user = pg_fetch_all($result)[0];
    if (!password_verify($_POST['passwd'], $db_user['passwd'])) {
        header('Location: login.php?error=El usuario o la contraseña no son correctos');
        exit();
    }

    session_start(['cookie_lifetime' => 604800]);
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['vendedor'] = $db_user['codigo'];
    $_SESSION['nombre'] = trim($db_user['nombre']);
    $_SESSION['contacto'] = trim($db_user['contacto']);
    $_SESSION['responsable'] = $db_user['responsable'] == '1';
    header('Location: index.php');
    exit();
}

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>INCOPERFIL | Ofertas pendientes</title>
        <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/favicon.ico" type="image/x-icon">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/login.css">
    </head>
    <body>
        <div class="container-lg">
            <div class="form-signin-wrapper">
                <main class="form-signin">
                    <form method="post" action="login.php">
                        <a href="./index.php">
                            <img class="mb-4" src="assets/images/logo.jpg" alt="" width="50%" height="">
                        </a>
                        <h1 class="h3 mb-3 fw-normal">Ofertas pendientes de revisión</h1>
                        <p><?= $_GET['msg'] ?? 'Iniciar sesión' ?></p>
                        <?php if (isset($_GET['error'])) { ?>
                            <p class="text-danger"><?= $_GET['error'] ?></p>
                        <?php } ?>
                        <div class="form-floating">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
                            <label for="email">Email</label>
                        </div>
                        <div class="form-floating">
                            <input type="password" class="form-control" id="passwd" name="passwd" placeholder="Password" required="required">
                            <label for="passwd">Contraseña</label>
                        </div>
                        <!--<div class="checkbox mb-3">
                            <label>
                                <input type="checkbox" value="remember-me"> Mantenerme conectado
                            </label>
                        </div>-->
                        <button class="w-100 btn btn-primary" type="submit">Iniciar sesión</button>
                        <p class="mt-4 text-center small">
                            <a class="text-decoration-none" href="password/forgot.php">¿Has olvidado la contraseña?</a>
                        </p>
                    </form>
                </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    </body>
</html>