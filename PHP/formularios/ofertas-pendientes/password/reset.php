<?php

if (!isset($_GET['token'])) {
    header('Location: forgot.php?error=El token de restablecimiento es inválido');
    exit();
}

require_once __DIR__ . '/../php/db_connector.php';

$result = pg_query_params($_CONN, 'select vendedor, expire_at from reset_password where token = $1', [$_GET['token']]);
if ($result === false || pg_num_rows($result) <= 0) {
    header('Location: forgot.php?error=El token de restablecimiento es inválido');
    exit();
}
$db_user = pg_fetch_all($result)[0];

if ($_POST) {
    if (empty($_POST['password']) || empty($_POST['password_confirm'])) {
        header('Location: forgot.php');
        exit();
    }

    if ($_POST['password'] != $_POST['password_confirm']) {
        header('Location: reset.php?token=' . $_GET['token'] . '&error=Las contraseñas no coinciden');
        exit();
    }

    $passwd = password_hash($_POST['password'], PASSWORD_BCRYPT);
    $result = pg_query_params($_CONN, 'update vendedores set passwd = $1 where codigo = $2', [$passwd, $db_user['vendedor']]);
    pg_delete($_CONN, 'reset_password', ['token' => $_GET['token']]);

    header('Location: ../login.php');
    exit();
}

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>INCOPERFIL | Ofertas pendientes</title>
        <link rel="shortcut icon" href="../assets/favicon.ico" type="image/x-icon">
        <link rel="icon" href="../assets/favicon.ico" type="image/x-icon">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/password-reset.css">
    </head>
    <body>
        <div class="container-lg">
            <div class="form-reset-wrapper">
                <main class="form-reset">
                    <form method="post" action="reset.php">
                        <a href="../index.php">
                            <img class="mb-4" src="../assets/images/logo.jpg" alt="" width="50%" height="">
                        </a>
                        <h1 class="h3 mb-3 fw-normal">Ofertas pendientes de revisión</h1>
                        <p>Introduce la nueva contraseña para poder iniciar sesión</p>
                        <?php if (isset($_GET['error'])) { ?>
                            <p class="text-danger"><?= $_GET['error'] ?></p>
                        <?php } ?>
                        <div class="form-floating">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Email" required="required">
                            <label for="password">Nueva contraseña</label>
                        </div>
                        <div class="form-floating">
                            <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Email" required="required">
                            <label for="password_confirm">Confirmación de contraseña</label>
                        </div>
                        <button class="w-100 btn btn-primary" type="submit">Recuperar</button>
                    </form>
                </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    </body>
</html>
