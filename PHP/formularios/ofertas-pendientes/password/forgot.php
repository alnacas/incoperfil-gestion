<?php

if ($_POST) {
    if (empty($_POST['email'])) {
        header('Location: forgot.php?error=No se han enviado los datos necesarios');
        exit();
    }

    require_once __DIR__ . '/../vendor/autoload.php';
    require_once __DIR__ . '/../php/db_connector.php';

    $msg = 'Se ha recibido una petición de restablecimiento de contraseña. Si tenemos registrado su usuario le llegará un email con las instrucciones a seguir.';

    $result = pg_query_params($_CONN, 'select codigo, nombre, contacto, passwd from vendedores where email = $1', [$_POST['email']]);
    if ($result === false || pg_num_rows($result) <= 0) {
        header('Location: ../login.php?msg=' . urlencode($msg));
        exit();
    }
    $db_user = pg_fetch_all($result)[0];

    $result = pg_query_params($_CONN, 'select id, vendedor, token, expire_at from reset_password where vendedor = $1 and expire_at > current_timestamp', [$db_user['codigo']]);
    $reset_password = ($result === false || pg_num_rows($result) <= 0) ? null : pg_fetch_all($result)[0];
    if (is_null($reset_password)) {
        pg_delete($_CONN, 'reset_password', ['vendedor' => $db_user['codigo']]);
        $token = uniqid();
        $result = pg_insert($_CONN, 'reset_password', [
            'vendedor' => $db_user['codigo'],
            'token' => $token,
            'old_password' => $db_user['passwd'],
            'expire_at' => (new DateTimeImmutable())->modify('+2 hours')->format('Y-m-d H:i:s')
        ]);
        if ($result === false) {
            error_log('forgot password - persisting reset token');
            exit();
        }
    } else {
        $token = $reset_password['token'];
    }

    try {
        $transport = (new Swift_SmtpTransport('imap.panel247.com', 587))
            ->setUsername('noreply@incoperfil.com')
            ->setPassword('e+%Pw79^FX');
        $body = '<p>Utilice el siguiente enlace para restablecer la contraseña:<br>https://apps.incoperfil.com/ofertas-pendientes/password/reset?token=' . $token . '</p>';
        $message = new Swift_Message('Cambio de contraseña | INCOPERFIL - Ofertas pendientes', $body, 'text/html');
        $message->setFrom('noreply@incoperfil.com')
            ->setTo('albertonavarrocastellar@outlook.com');
        (new Swift_Mailer($transport))->send($message);
    } catch (Exception $exception) {
        error_log('forgot password - email error: (' . get_class($exception) . ') ' . $exception->getMessage());
        header('Location: ../login.php?error=' . urlencode('No se ha podido enviar el email de recuperación'));
        exit();
    }

    header('Location: ../login.php?msg=' . urlencode($msg));
    exit();
}

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>INCOPERFIL | Ofertas pendientes</title>
        <link rel="shortcut icon" href="../assets/favicon.ico" type="image/x-icon">
        <link rel="icon" href="../assets/favicon.ico" type="image/x-icon">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link rel="stylesheet" href="../assets/css/style.css">
        <link rel="stylesheet" href="../assets/css/password-forgot.css">
    </head>
    <body>
        <div class="container-lg">
            <div class="form-forgot-wrapper">
                <main class="form-forgot">
                    <form method="post" action="forgot.php">
                        <a href="../index.php">
                            <img class="mb-4" src="../assets/images/logo.jpg" alt="" width="50%" height="">
                        </a>
                        <h1 class="h3 mb-3 fw-normal">Ofertas pendientes de revisión</h1>
                        <p>Introduce tu usuario o email para poder reestablecer tu contraseña. Recibirá un email con un enlace para reestablecer la contraseña.</p>
                        <?php if (isset($_GET['error'])) { ?>
                            <p class="text-danger"><?= $_GET['error'] ?></p>
                        <?php } ?>
                        <div class="form-floating">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required="required">
                            <label for="email">Email</label>
                        </div>
                        <button class="w-100 btn btn-primary" type="submit">Recuperar</button>
                        <p class="mt-4 text-center small">
                            ¿Tienes cuenta? <a class="text-decoration-none" href="../login.php">Inicia sesión</a>
                        </p>
                    </form>
                </main>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    </body>
</html>
