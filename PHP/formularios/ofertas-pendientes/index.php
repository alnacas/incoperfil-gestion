<?php

session_start();

if (!isset($_SESSION['email'])) {
    header('Location: login.php');
    exit();
}

require_once __DIR__ . '/php/db_connector.php';

?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>INCOPERFIL | Ofertas pendientes</title>
        <link rel="shortcut icon" href="assets/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/favicon.ico" type="image/x-icon">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<!--        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">-->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="d-none">
                <input type="hidden" name="vendedor_id" id="vendedor_id" value="<?= $_SESSION['vendedor'] ?>">
                <input type="hidden" name="vendedor_responsable" id="vendedor_responsable" value="<?= $_SESSION['responsable'] ?>">
            </div>
            <div class="d-flex align-items-center">
                <div class="flex-grow-1 w-100">
                    <h1>Ofertas pendientes para revisión</h1>
                    <h4>Bienvenido, <?= $_SESSION['contacto'] ?> (<?= $_SESSION['nombre'] ?>)</h4>
                </div>
                <div>
                    <p class="text-center"><img class="img-fluid" src="assets/images/logo.jpg" alt="Ingeniería y Construcción del Perfil SA" style="max-width: 220px;"></p>
                    <p class="fs-small text-end"><a class="text-danger text-decoration-none" href="logout.php">Cerrar sesión</a></p>
                </div>
            </div>
            <?php if ($_SESSION['responsable']) {
                $res = pg_query($_CONN, 'select codigo, nombre from vendedores order by nombre');
                $vendedores = pg_num_rows($res) > 0 ? pg_fetch_all($res) : [];
                ?>
                <div class="row mt-3">
                    <div class="col-lg-3">
                        <form onsubmit="return false;">
                            <div class="row">
                                <label for="vendedor" class="col-lg-3 col-form-label">Comerciales</label>
                                <div class="col-lg-9">
                                    <select class="form-select" name="vendedor" id="vendedor" onchange="fe_changeVendedor($(this))">
                                        <?php foreach ($vendedores as $vendedor) { ?>
                                            <option value="<?= $vendedor['codigo'] ?>" <?= $_SESSION['vendedor'] == $vendedor['codigo'] ? 'selected' : '' ?>><?= $vendedor['nombre'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-3">
                        <button class="btn btn-secondary" type="button" onclick="fe_toSync()">Sincronizar con gestión</button>
                    </div>
                </div>
            <?php } ?>
            <div class="table-responsive my-3" style="height: 97vh">
                <div class="text-center" id="loading-list" style="display: none">
                    <div class="spinner-border" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                <table class="table table-hover ofertas" id="ofertas" style="display: none">
                    <thead class="bg-white sticky-top">
                    <tr>
                        <th>Número</th>
                        <th class="text-center">Rev.</th>
                        <th>Fecha</th>
                        <th>Validez</th>
                        <th>Razón Social</th>
                        <th class="text-end">Importe</th>
                        <th>Ref. Obra</th>
                        <th>Contacto</th>
                        <th>Estado</th>
                        <th>Notas</th>
                        <?php if ($_SESSION['responsable']) { ?>
                            <th>F. Ult. Act.</th>
                            <th>Sync?</th>
                        <?php } ?>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/e4d0495af5.js" crossorigin="anonymous"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/dataTables.bootstrap5.min.js"></script>
        <script src="./assets/js/index.js"></script>
        <script src="./assets/js/datatables.js"></script>
    </body>
</html>