const DATATABLES_CONFIG_NOORDER_TARGETS = {
    '': [1, 7, 9, 10],
    '0': [1, 7, 9, 10],
    '1': [1, 7, 9, 11, 12]
};

$(document).ready(function () {
    $.extend($.fn.dataTable.defaults, {
        // info: false,
        language: {
            url: 'https://cdn.datatables.net/plug-ins/1.11.5/i18n/es-ES.json'
        },
        paging: false,
        // scrollY: '90vh',
        // scrollCollapse: true,
        searching: false,
    });
});