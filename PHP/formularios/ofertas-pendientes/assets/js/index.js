const ENDPOINT = './php/api.php';
let USER_ID,
    IS_RESPONSABLE,
    DATATABLES_WORKING;

// FRONT END
// funciones solo responsables
function fe_changeVendedor(jq_obj) {
    let id = jq_obj.val();
    if (id === '') id = USER_ID;
    be_loadOfertas(id, IS_RESPONSABLE);
}

function fe_toSync() {
    const vendedor = document.getElementById('vendedor').value;
    be_syncOfertas(vendedor);
}
// fin funciones solo responsables

// funciones generales
function fe_ofertasRender_getDtConfig() {
    return {
        columnDefs: [
            {orderable: false, targets: DATATABLES_CONFIG_NOORDER_TARGETS[IS_RESPONSABLE]}
        ],
        order: [[3, 'asc']]
    };
}

function fe_ofertasRender(data) {
    let jq_ofertas = $('#ofertas');
    if (DATATABLES_WORKING) jq_ofertas.DataTable().destroy();
    $('#ofertas tbody').empty().html(data);
    jq_ofertas.DataTable(fe_ofertasRender_getDtConfig());
    DATATABLES_WORKING = true;
}
/**
 *
 * @param {object} jq_obj
 * @param oferta
 */
function fe_formEnable(jq_obj, oferta) {
    $('span.form-texto-' + oferta).hide();
    jq_obj.hide();
    $('#oferta-' + oferta + '-estado, #oferta-' + oferta + '-nota, #form-action-' + oferta).show();
    if ($('#oferta-' + oferta + '-estado').val() === '3') {
        $('#oferta-' + oferta + '-motivo').show();
    }
}

/**
 *
 * @param {object} jq_obj
 * @param oferta
 */
function fe_formSave(jq_obj, oferta) {
    const estado = $('#oferta-' + oferta + '-estado').val();
    const motivo = $('#oferta-' + oferta + '-motivo').val();
    const nota = $('#oferta-' + oferta + '-nota').val();
    let vendedor;
    if (IS_RESPONSABLE) {
        vendedor = $('#vendedor').val();
        if (vendedor === '') vendedor = USER_ID;
    } else {
        vendedor = USER_ID;
    }
    be_updateOferta(oferta, estado, motivo, nota, {id: vendedor, responsable: IS_RESPONSABLE});
}

/**
 *
 * @param {object} jq_obj
 * @param oferta
 */
function fe_formCancel(jq_obj, oferta) {
    $('#oferta-' + oferta + '-estado, #oferta-' + oferta + '-motivo, #oferta-' + oferta + '-nota, #form-action-' + oferta).hide();
    $('.form-oferta[data-oferta = "' + oferta + '"]').show();
    $('span.form-texto-' + oferta).show();
}

/**
 *
 * @param {object} jq_obj
 * @param {string} slave
 * @param {string} value
 */
function fe_fromChangeEstado(jq_obj, slave, value) {
    if (jq_obj.val() === value) {
        $(slave).show();
    } else {
        $(slave).hide();
    }
}
// fin funciones generales
// FIN FRONT END


// BACK END
// funciones solo responsables
/**
 *
 * @param {number} vendedor
 */
function be_syncOfertas(vendedor) {
    $('#ofertas').hide();
    $('#loading-list').show();
    $.post(ENDPOINT, {action: 'sync-ofertas', vendedor: {id: vendedor}})
        .done(function(data) { // data, textStatus, jqXHR
            fe_ofertasRender(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
        })
        .always(function() { // data|jqXHR, textStatus, jqXHR|errorThrown
            $('#loading-list').hide();
            $('#ofertas').show();
        });
}
// fin funciones solo responsables

// funciones generales
/**
 *
 * @param {number} vendedor
 * @param {boolean} responsable
 */
function be_loadOfertas(vendedor, responsable) {
    $('#ofertas').hide();
    $('#loading-list').show();
    $.get(ENDPOINT, 'action=list-ofertas&id=' + vendedor + '&responsable=' + responsable)
        .done(function(data) { // data, textStatus, jqXHR
            fe_ofertasRender(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
        })
        .always(function() { // data|jqXHR, textStatus, jqXHR|errorThrown
            $('#loading-list').hide();
            $('#ofertas').show();
        });
}

/**
 *
 * @param {number} oferta
 * @param {number} estado
 * @param {number|null} motivo
 * @param {string} nota
 * @param {Object} vendedor
 */
function be_updateOferta(oferta, estado, motivo, nota, vendedor) {
    $('#form-action-' + oferta).hide();
    $('#form-loading-' + oferta).show();
    $.post(ENDPOINT, {action: 'update-oferta', id: oferta, estado: estado, motivo: motivo, nota: nota, vendedor: vendedor})
        .done(function(data) { // data, textStatus, jqXHR
            // const estado_txt = $('#oferta-' + oferta + '-estado').find('option[value = "' + estado + '"]').html();
            /*let estado = data.estado_txt;
            if (data.estado === 3) estado += ' <small>(' + data.motivo_txt + ')</small>';
            $('span.form-texto-' + oferta + '[data-value = "estado"]').html(estado);
            $('span.form-texto-' + oferta + '[data-value = "nota"]').html(data.nota);

            $('#oferta-' + oferta + '-estado, #oferta-' + oferta + '-motivo, #oferta-' + oferta + '-nota, #form-action-' + oferta).hide();
            $('.form-oferta[data-oferta = "' + oferta + '"]').show();
            $('span.form-texto-' + oferta).show();*/
            fe_ofertasRender(data);
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            // $('#form-action-' + oferta).show();
        })
        .always(function() { // data|jqXHR, textStatus, jqXHR|errorThrown
            // $('#form-loading-' + oferta).hide();
        });
}
// fin funciones generales
// FIN BACK END

$(document).ready(function () {
    USER_ID = $('#vendedor_id').val();
    IS_RESPONSABLE = $('#vendedor_responsable').val();
    be_loadOfertas(USER_ID, IS_RESPONSABLE);

    /*$('#vendedor').change(function () {
        let id = $(this).val();
        if (id === '') id = USER_ID;
        be_loadOfertas(id, IS_RESPONSABLE);
    });*/

    $('body')
        /*.on('change', '[data-slave]', function () {
            const slave = $(this).data('slave');
            const value = $(this).data('value');
            if ($(this).val() == value) $(slave).show();
            else $(slave).hide();
        })*/
        /*.on('click', '.form-oferta', function () {
            const oferta = $(this).data('oferta');
            $('span.form-texto-' + oferta).hide();
            $(this).hide();
            $('#oferta-' + oferta + '-estado, #oferta-' + oferta + '-nota, #form-action-' + oferta).show();
            if ($('#oferta-' + oferta + '-estado').val() === '3') {
                $('#oferta-' + oferta + '-motivo').show();
            }
        })*/
        /*.on('click', '.form-oferta-save', function () {
            const oferta = $(this).data('oferta');
            const estado = $('#oferta-' + oferta + '-estado').val();
            const motivo = $('#oferta-' + oferta + '-motivo').val();
            const nota = $('#oferta-' + oferta + '-nota').val();
            let vendedor;
            if (IS_RESPONSABLE) {
                vendedor = $('#vendedor').val();
                if (vendedor === '') vendedor = $('#vendedor_id').val();
            } else {
                vendedor = USER_ID;
            }
            be_updateOferta(oferta, estado, motivo, nota, {id: vendedor, responsable: IS_RESPONSABLE});
        })*/
        /*.on('click', '.form-oferta-cancel', function () {
            const oferta = $(this).data('oferta');
            $('#oferta-' + oferta + '-estado, #oferta-' + oferta + '-motivo, #oferta-' + oferta + '-nota, #form-action-' + oferta).hide();
            $('.form-oferta[data-oferta = "' + oferta + '"]').show();
            $('span.form-texto-' + oferta).show();
        })*/
    ;
});