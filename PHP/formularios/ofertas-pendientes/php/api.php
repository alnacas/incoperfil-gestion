<?php

$sql_ofertas = 'select identificador, numero, revision, fecha, fecha_validez, cliente, nombre, importe, referencia_obra, vendedor, comercial, telefono, email, estado, observaciones, motivo, updated_at, sync from ofertas where vendedor = $1 order by fecha_validez';

if (isset($_GET['action'])) {
    require_once __DIR__ . '/db_connector.php';

    switch ($_GET['action']) {
        case 'list-ofertas':
            if (empty($_GET['id'])) {
                pg_close($_CONN);
                errorResponse(400, 'No se ha indicado el ID del vendedor');
            }
            $result = pg_query_params($_CONN, $sql_ofertas, [$_GET['id']]);
            if ($result == false) {
                pg_close($_CONN);
                errorResponse(500, pg_errormessage($_CONN));
            }
            ob_start();
            renderOfertas(pg_fetch_all($result), $_GET['responsable']);
            $response = ob_get_clean();
            break;
        default:
            pg_close($_CONN);
            errorResponse(404, 'Acción no encontrada');
            break;
    }

    pg_close($_CONN);
    okResponse($response);
}

if ($_POST) {
    require_once __DIR__ . '/db_connector.php';

    switch ($_POST['action']) {
        case 'update-oferta':
            if (empty($_POST['id']) || !isset($_POST['estado']) || strlen($_POST['estado']) == 0) {
                pg_close($_CONN);
                errorResponse(400, 'Faltan parámetros para poder actualizar la oferta');
            }
            $_POST['estado'] = intval($_POST['estado']);
            $_POST['motivo'] = strlen($_POST['motivo']) == 0 ? null : intval($_POST['motivo']);
            $result = pg_query_params($_CONN, 'update ofertas set estado = $1, observaciones = $2, motivo = $3, updated_at = current_date where numero = $4', [$_POST['estado'], $_POST['nota'], $_POST['motivo'], $_POST['id']]);
            if ($result == false) {
                pg_close($_CONN);
                errorResponse(500, pg_errormessage($_CONN));
            }
            $result = pg_query_params($_CONN, $sql_ofertas, [$_POST['vendedor']['id']]);
            if ($result == false) {
                pg_close($_CONN);
                errorResponse(500, pg_errormessage($_CONN));
            }
            ob_start();
            renderOfertas(pg_fetch_all($result), $_POST['vendedor']['responsable']);
            $response = ob_get_clean();
            break;
        case 'sync-ofertas':
            $result = pg_query_params($_CONN, 'update ofertas set sync = 1 where vendedor = $1 and (sync = 0 or sync is null) and updated_at is not null', [$_POST['vendedor']['id']]);
            if ($result == false) {
                pg_close($_CONN);
                errorResponse(500, pg_errormessage($_CONN));
            }
            $result = pg_query_params($_CONN, $sql_ofertas, [$_POST['vendedor']['id']]);
            if ($result == false) {
                pg_close($_CONN);
                errorResponse(500, pg_errormessage($_CONN));
            }
            ob_start();
            renderOfertas(pg_fetch_all($result), true);
            $response = ob_get_clean();
            break;
        default:
            pg_close($_CONN);
            errorResponse(404, 'Acción no encontrada');
            break;
    }

    pg_close($_CONN);
    okResponse($response);
}

// response functions
/**
 * @param $code
 * @param $msg
 * @return void
 */
function errorResponse($code, $msg) {
    http_response_code($code);
    exit(json_encode(['msg' => $msg]));
}

/**
 * @param $response
 * @return void
 */
function okResponse($response) {
    exit($response);
}

// render data functions
function renderOfertas($ofertas, $responsable) {
    if (empty($ofertas)) { ?>
        <tr><td colspan="11">No se han encontrado ofertas</td></tr>
    <?php } else {
        $format_from = 'Y-m-d';
        $format_to = 'd/m/Y';
        $estados = [0,3,5];
        if ($responsable) {
            $estados = [0,3,4,5];
        } else {
            $ofertas = array_filter($ofertas, function ($oferta) { return $oferta['sync'] == 0; });
        }
        foreach ($ofertas as $oferta) { ?>
            <tr class="vendedor-<?= $oferta['vendedor'] ?> oferta-<?= $oferta['numero'] ?>">
                <td class="align-middle"><?= $oferta['numero'] ?></td>
                <td class="align-middle text-center"><?= $oferta['revision'] ?></td>
                <td class="align-middle" data-order="<?= $oferta['fecha'] ?>"><?= DateTime::createFromFormat($format_from, $oferta['fecha'])->format($format_to) ?></td>
                <td class="align-middle" data-order="<?= $oferta['fecha_validez'] ?>"><?= DateTime::createFromFormat($format_from, $oferta['fecha_validez'])->format($format_to) ?></td>
                <td class="align-middle"><?= $oferta['nombre'] ?></td>
                <td class="align-middle text-end" data-order="<?= $oferta['importe'] ?>"><?= number_format($oferta['importe'], 2, ',', '.') . ' €' ?></td>
                <td class="align-middle"><?= $oferta['referencia_obra'] ?></td>
                <td class="align-middle">
                    <?= $oferta['comercial'] ?><br>
                    <?= $oferta['email'] ?><br>
                    <?= $oferta['telefono'] ?>
                </td>
                <td class="align-middle">
                    <span class="form-texto-<?= $oferta['numero'] ?>" data-value="estado">
                        <?= renderOfertas_getEstado($oferta['estado']) ?>
                        <?php if ($oferta['estado'] == 3 && !is_null($oferta['motivo'])) { ?>
                            <small>(<?= renderOfertas_getMotivo($oferta['motivo']) ?>)</small>
                        <?php } ?>
                    </span>
                    <select class="form-select form-oferta-<?= $oferta['numero'] ?>" id="oferta-<?= $oferta['numero'] ?>-estado" onchange="fe_fromChangeEstado($(this), '#oferta-<?= $oferta['numero'] ?>-motivo', '3')" style="display: none;">
                        <?php foreach ($estados as $estado) { ?>
                            <option value="<?= $estado ?>" <?= $estado == $oferta['estado'] ? 'selected' : '' ?>><?= renderOfertas_getEstado($estado) ?></option>
                        <?php } ?>
                    </select>
                    <select class="form-select form-oferta-<?= $oferta['numero'] ?>" id="oferta-<?= $oferta['numero'] ?>-motivo" style="display: none;">
                        <option value="">Selecciona</option>
                        <?php for ($i = 0; $i < 8; $i++) { ?>
                            <option value="<?= $i ?>" <?= !is_null($oferta['motivo']) && $i == $oferta['motivo'] ? 'selected' : '' ?>><?= renderOfertas_getMotivo($i) ?></option>
                        <?php } ?>
                    </select>
                </td>
                <td class="align-middle">
                    <span class="form-texto-<?= $oferta['numero'] ?>" data-value="nota"><?= $oferta['observaciones'] ?></span>
                    <textarea class="from-control form-oferta-<?= $oferta['numero'] ?>" id="oferta-<?= $oferta['numero'] ?>-nota" cols="20" rows="2" style="display: none;"><?= trim($oferta['observaciones']) ?></textarea>
                </td>
                <?php if ($responsable) { ?>
                    <td class="align-middle"><?= is_null($oferta['updated_at']) ? '' : DateTime::createFromFormat($format_from, $oferta['updated_at'])->format($format_to) ?></td>
                    <td class="align-middle text-center"><?= $oferta['sync'] == 1 ? 'Si' : 'No' ?></td>
                <?php } ?>
                <td class="align-middle">
                    <button class="btn btn-secondary form-oferta" data-oferta="<?= $oferta['numero'] ?>" onclick="fe_formEnable($(this), <?= $oferta['numero'] ?>)">
                        <i class="fa fa-pencil"></i>
                    </button>
                    <div class="btn-group-vertical" id="form-action-<?= $oferta['numero'] ?>" style="display: none;">
                        <button class="btn btn-success form-oferta-save" data-oferta="<?= $oferta['numero'] ?>" onclick="fe_formSave($(this), <?= $oferta['numero'] ?>)">
                            <i class="fa fa-check"></i>
                        </button>
                        <button class="btn btn-danger form-oferta-cancel" data-oferta="<?= $oferta['numero'] ?>" onclick="fe_formCancel($(this), <?= $oferta['numero'] ?>)">
                            <i class="fa fa-xmark"></i>
                        </button>
                    </div>
                    <div class="text-center" id="form-loading-<?= $oferta['numero'] ?>" style="display: none">
                        <div class="spinner-border" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </td>
            </tr>
        <?php }
    }
}

function renderOfertas_getEstado($estado) {
    if (is_null($estado)) return '';
    if ($estado == 0) return 'Pendiente';
    if ($estado == 1) return 'Aceptada';
    if ($estado == 2) return 'Fabricación';
    if ($estado == 3) return 'Cancelada';
    if ($estado == 4) return 'Servida';
    if ($estado == 5) return 'Congelada';
    return '';
}

function renderOfertas_getMotivo($motivo) {
    if (is_null($motivo)) return '';
    if ($motivo == 0) return 'No cumple en plazo';
    if ($motivo == 1) return 'Precio elevado';
    if ($motivo == 2) return 'Falta de crédito';
    if ($motivo == 3) return 'Servicio incompleto (requiere instalación)';
    if ($motivo == 4) return 'Coste de transporte elevado';
    if ($motivo == 5) return 'Cancelación de la obra';
    if ($motivo == 6) return 'Imposible cambiar la solución prescrita';
    if ($motivo == 7) return 'Cliente no ha ganado la obra';
    return '';
}