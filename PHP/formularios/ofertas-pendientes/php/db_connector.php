<?php

const INCO_DB = [
    'host' => '192.168.0.100',
    'dbname' => 'db_incoperfil_shared',
    'user' => 'sic3',
    'password' => 'sic3',
    'options' => '\'--client_encoding=UTF8\'',
];

/** @var resource|null|false $_CONN */
$_CONN = null;

function getDBConnection() {
    global $_CONN;

    if (!is_null($_CONN)) return;

    $parts = [];
    foreach (INCO_DB as $key => $value) {
        $parts[] = $key . '=' . $value;
    }
    $db_config = implode(' ', $parts);

    $_CONN = pg_connect($db_config);
    if ($_CONN === false) {
        die('No se ha podido establecer la conexión con la base de datos: ' . json_encode($db_config));
    }
}

getDBConnection();