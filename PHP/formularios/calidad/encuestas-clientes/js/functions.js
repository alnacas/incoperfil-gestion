$(document).ready(function () {
    const queryParams = new URLSearchParams(window.location.search);
    $('#form_token').val(queryParams.get('t'));

    $('#form').submit(function (e) {
        e.preventDefault();

        const form = $(this);
        form.find('input').removeClass('is-invalid');
        form.find('#form_loading').show();

        $.post('./php/manager.php', form.serialize(), null, 'json')
            .done(function (response) {
                if (response['status'] === 500) {
                    form.find('#form_loading').show();
                    return;
                }

                form.find('#table_submit').hide();
                form.find('#alert_success').show('fade');
            })
            .fail(function () {
                form.find('#alert_error').show('fade');
            })
            .always(function () {
                form.find('#form_loading').hide();
                form.get(0).reset();
                setTimeout(() => {
                    form.find('#alert_success').hide('fade');
                    form.find('#alert_error').hide('fade');
                }, 2500);
            })
        ;

        return false;
    });
});