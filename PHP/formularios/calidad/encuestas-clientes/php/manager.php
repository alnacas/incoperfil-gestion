<?php

ini_set('log_errors', true);
ini_set('error_log', __DIR__ . '/error.log');

if (!empty($_POST)) {
	require_once __DIR__ . '/../../../../PHP/config.php';
	// require_once __DIR__ . '/../../config.php';

	$action = $_POST['action'];
	switch ($action) {
        case 'submission':
            $conn = fnGetConnection();
            $data = [
                'fecha' => date('Y-m-d'),
                'hora' => date('H:i:s'),
                'respuesta_01_recomendacion' => $_POST['recomendacion'],
                'respuesta_01_comentario' => $_POST['comentario'],
            ];
            $updated = pg_update($conn, 'encuestas', $data, ['token' => $_POST['token']]);
            if ($updated === false) {
                $response = [
                    'status' => 500,
                    'message' => pg_last_error($conn)
                ];
                break;
            }
            $response = [
                'status' => 200,
                'data' => $_POST
            ];
            fnCloseConnection($conn);
            break;
		default:
			$response = [
				'status' => 500,
				'message' => 'Acción no reconocida'
			];
			break;
	}

	exit(json_encode($response));
}

function fnGetConnection() {
	// cuando se suba al VPS hay que cambiarlo por SDB_STRING porque cambia el config.php
	$conn = incoDbGetConnection(INCO_DB_REMOTE);
	// $conn = pg_connect(SDB_STRING);
	if (!$conn) {
		exit(['status' => 500, 'message' => 'No se ha podido conectar']);
	}
	return $conn;
}

/**
 * @param resource $conn
 */
function fnCloseConnection($conn) {
	pg_close($conn);
}