<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$firmar = boolval($argv[3]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el pedido
$result = pg_query_params($dbconn, 'SELECT fecha, numero, proveedor, observaciones_proveedor AS obser FROM tratamiento_bobinas WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [tratamiento_bobinas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[tratamiento_bobinas] No hay resultado');
$pedido = pg_fetch_all($result)[0];

// obtenemos las lineas del pedido
$result = pg_query_params($dbconn, 'SELECT linea, bobina, ml_enviados, ml_perforar, perforado, precio FROM tratamiento_bobinaslin WHERE identificador = $1 ORDER BY linea', array($identificador)) or incoLogWrite('La consulta fallo [tratamiento_bobinaslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[tratamiento_bobinaslin] No hay resultado');
$pedidolin = pg_fetch_all($result);

$result = pg_query_params($dbconn, 'SELECT DISTINCT tbl.perforado FROM tratamiento_bobinaslin tbl, perforaciones p WHERE tbl.identificador = $1 AND p.tipo = 1 AND tbl.perforado = p.codigo', array($identificador)) or incoLogWrite('La consulta fallo [tratamiento_bobinaslin anexos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) $anexos = array();
else $anexos = pg_fetch_all($result);

// obtenemos el proveedor
$result = pg_query_params($dbconn, 'SELECT codigo, nombre, direccion, codpo, poblacion, provincia, fopago FROM proveedores WHERE codigo = $1', array($pedido['proveedor'])) or incoLogWrite('La consulta fallo [proveedores]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[proveedores] No hay resultado');
$proveedor = pg_fetch_all($result)[0];

$result = pg_query_params($dbconn, 'SELECT nombre FROM fopago WHERE codigo = $1', array($proveedor['fopago'])) or incoLogWrite('La consulta fallo [fopago]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[fopago] No hay resultado');
$fopago = pg_fetch_all($result)[0]['nombre'];

pg_prepare($dbconn, 'bobina', 'SELECT codigo, trim(nombre) AS nombre, ancho, numero_bulto FROM bobinas WHERE codigo = $1');

pg_prepare($dbconn, 'perforacion', 'SELECT trim(nombre_compuesto) AS nombre, tipo, trim(croquis) as croquis FROM perforaciones WHERE codigo = $1');

class PDF extends TCPDF
{
    function Header()
    {
        /*$this->SetY(14);
        $this->Cell(0, 20, '', 1, 1); // Se cambiara por una cabecera
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 5, 'PEDIDOS A PROVEEDORES', 0, 1, 'C');
        $this->Ln(2);*/

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('0', '0', '4', '0');

        $txt = 'Tratamiento de bobina';
        $this->SetFont('dejavusans', 'B', 22);
        $this->SetY(10);
        $this->Cell(0, 4, $txt, 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Ln(); // hace salto demasiado grande
        // $this->Cell(0, 4, '', 0, 1);
        $this->Cell(0, 4, 'Hoja: ' . $this->PageNo(), 0, 1, 'R');

        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
    }

    function Footer()
    {
        global $firmar;

        $this->SetY(-35);

        if ($firmar) {
            $this->Image(INCO_DIR_IMAGENES . 'valpedidobobina.jpg', 238, $this->GetY(), 0, 28, 'jpg', '', 'C', false, 600);
        }

        $this->SetLineWidth(0.51); //1px = 0.085mm
        $this->Line($this->GetX(), $this->GetY(), 287, $this->GetY());
        $this->Ln(2);
        $this->SetFont('dejavusans', 'B', 8);
        $this->setCellPaddings('0', '0', '4', '0');
        $this->Cell(210, 4, 'Revisado por:', 0, 0, 'R');
        $this->setCellPaddings('0', '0', '0', '0');
        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->Cell(0, 20, '', 1, 1);

        $this->SetFont('dejavusans', '');
        $this->SetY($this->GetY() - 4);
        $this->Cell(0, 4, 'Página ' . $this->PageNo(), 0, 1, 'C');

        $this->SetFont('dejavusans', '', 5);
        $this->SetY($this->GetY() - 9);
        $this->Cell(0, 3, 'FCO-01-01', 0, 1, 'L');
        $this->Cell(0, 3, 'REV 1', 0, 1, 'L');
        $this->Cell(0, 3, 'FECHA: 29-05-06', 0, 1, 'L');
    }
}

$pdf = new PDF();

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('TRATAMIENTOS DE BOBINAS');
$pdf->SetTitle('Tratamiento de bobinas ' . $pedido['numero']);
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->SetMargins(10, 26);
$pdf->AddPage('L');

$pdf->SetFont('dejavusans', 'B');
$txt = 'Pedido';
$pdf->Cell(20, 4, $txt . ' Nº', 0, 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(30, 4, $pedido['numero'], 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->SetX(135);
$pdf->Cell(40, 4, 'Fecha de pedido', 0, 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, DateTime::createFromFormat('Y-m-d', $pedido['fecha'])->format('d-m-Y'), 0, 1);
$pdf->Ln();
$yProveedor = $pdf->GetY();

$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Proveedor', 0, 1, 'L');

$yUltimoCampo = $pdf->GetY();

$pdf->SetY($yProveedor);
$pdf->SetFont('dejavusans', 'B', 10);
$pdf->Cell(0, 4, trim($proveedor['nombre']), 0, 1, 'R');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetX(105);
$pdf->MultiCell(0, 4, trim($proveedor['direccion']) . ' ' . trim($proveedor['codpo']) . ' ' . trim($proveedor['poblacion']) . ' ' . trim($proveedor['provincia']) . PHP_EOL, 0, 'R');
$pdf->SetY($yUltimoCampo);

$pdf->Ln(8);

$pdf->SetFont('dejavusans', 'B', 12);
$pdf->Cell(0, 4, 'TRATAMIENTO DE BOBINAS', 'B', 1, 'C');

$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetFont('dejavusans', 'B', 8);

$border = 'B';
$height = 0;
$titles = array('Ref. INCOPERFIL' => array('w' => 30, 'align' => 'L'), 'Ref. Proveedor' => array('w' => 30, 'align' => 'L'), 'Bobina' => array('w' => 107, 'align' => 'L'),
    'Tipo Perforado' => array('w' => 30, 'align' => 'L'), 'ML Enviados' => array('w' => 20, 'align' => 'R'), 'ML A Perforar' => array('w' => 20, 'align' => 'R'),
    'ML Sin Perforar' => array('w' => 20, 'align' => 'R'), '€/ML' => array('w' => 20, 'align' => 'R'));
foreach ($titles as $title => $options) {
    $itemHeight = ceil($pdf->getStringHeight($options['w'], $title));
    if ($itemHeight > $height) $height = $itemHeight;
};
$counter = 0;
foreach ($titles as $title => $options) {
    $ln = $counter >= count($titles) - 1 ? 1 : 0;
    $pdf->MultiCell($options['w'], $height, $title, $border, $options['align'], false, $ln, '', '', true, 0, false, true, $height, 'B');
    $counter++;
}

$pdf->SetFont('dejavusans', '', 8);
for ($i = 0; $i < count($pedidolin); $i++) {
    $result = pg_execute($dbconn, 'bobina', array($pedidolin[$i]['bobina']));
    if (pg_num_rows($result) <= 0) incoLogWrite('[bobina] No hay resultado');
    $bobina = pg_fetch_all($result)[0];
    $nombre = $bobina['nombre'];
    $nombre = explode(' ', $nombre);
    $begin = array_slice($nombre, 0, 2);
    $end = array_slice($nombre, 2);
    $nombre = implode('x', $begin) . ' - ' . implode(' ', $end);
    $bobina['nombre'] = $nombre;

    $result = pg_execute($dbconn, 'perforacion', array($pedidolin[$i]['perforado']));
    if (pg_num_rows($result) <= 0) incoLogWrite('[bobina] No hay resultado');
    $perforado = pg_fetch_all($result)[0];

    $mlEnviado = floatval($pedidolin[$i]['ml_enviados']);
    $mlPerforar = floatval($pedidolin[$i]['ml_perforar']);
    $mlSinPerforar = $mlEnviado - $mlPerforar;

    $heightBob = ceil($pdf->getStringHeight(107, $bobina['nombre'], true));
    $heightPerf = ceil($pdf->getStringHeight(30, $perforado['nombre'], true));
    $height = max($heightBob, $heightPerf);

    if ($perforado['tipo'] == 1) $perforado['nombre'] = '<span style="color: red;">*</span> ' . $perforado['nombre'];

    $pdf->MultiCell(30, $height, $bobina['codigo'], 0, 'L', false, 0);
    $pdf->MultiCell(30, $height, trim($bobina['numero_bulto']), 0, 'L', false, 0);
    $pdf->MultiCell(107, $height, $bobina['nombre'], 0, 'L', false, 0);
    $pdf->writeHTMLCell(30, 4, $pdf->GetX(), $pdf->GetY(), $perforado['nombre']);
    $pdf->MultiCell(20, $height, number_format($mlEnviado, 0, ',', '.'), 0, 'R', false, 0);
    $pdf->MultiCell(20, $height, number_format($mlPerforar, 0, ',', '.'), 0, 'R', false, 0);
    $pdf->MultiCell(20, $height, number_format($mlSinPerforar, 0, ',', '.'), 0, 'R', false, 0);
    $pdf->MultiCell(20, $height, number_format($pedidolin[$i]['precio'], 2, ',', '.'), 0, 'R', false, 1);
}
$pdf->Ln(4);

if (!empty($anexos)) {
    $html = '(<span style="color: red;">*</span>) Ver el anexo correspondiente para la distribución del perforado sobre la bobina.';
    $pdf->writeHTMLCell(0, 4, $pdf->GetX(), $pdf->GetY(), $html, 0, 1);
    $pdf->Ln(4);
}

$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->Cell(0, 4, 'CONDICIONES GENERALES', 'B', 1, 'C');
$pdf->Ln(2);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Entrega de Materiales', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Los materiales deben llevar su correspondiente albarán de entrega y número de pedido' . PHP_EOL);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Facturación', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Sus facturas se deberán recibir en nuestras oficinas como máximo a los 10 días de su fecha de emisión acompañadas con una copia de los albaranes.' . PHP_EOL);
$pdf->Ln(2);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Formas de Pago', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, $fopago, 0, 1, 'L');
$pdf->Ln(12);

$obs = trim($pedido['obser']);
if (!empty($obs)) {
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.51); //1px = 0.085mm
    $pdf->Cell(0, 4, 'COMENTARIOS', 'B', 1, 'C');
    $pdf->SetFontSize(7);
    $pdf->Ln(2);

    $pdf->MultiCell(0, 4, $obs . PHP_EOL);
    $pdf->Ln(4);
}

if (!empty($anexos)) {
    for($i = 0; $i < count($anexos); $i++) {
        $result = pg_execute($dbconn, 'perforacion', array($anexos[$i]['perforado']));
        if (pg_num_rows($result) <= 0) incoLogWrite('[anexos] No hay resultado');
        $perforado = pg_fetch_all($result)[0];

        if (is_null($perforado['croquis']) || empty($perforado['croquis']) || !file_exists($perforado['croquis'])) {
            continue;
        }

        $pdf->AddPage();
        $pdf->SetFont('dejavusans', 'B', 12);
        $pdf->Cell(0, 4, 'ANEXO ' . $perforado['nombre'], 0, 1, 'C');
        $pdf->SetFont('dejavusans', '', 8);
        $pdf->Ln(4);
        $pdf->Image($perforado['croquis'], $pdf->GetX(), $pdf->GetY(), 0, 120, '', '', '', false, 300, 'C');
    }
}

$dir = INCO_DIR_PROVEEDORES . $proveedor['codigo'] . '/Tratamientos/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'TB_' . str_pad($pedido['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
// $fileName = 'PedidoBobinas.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

// pg_free_result($result);
pg_close($dbconn);