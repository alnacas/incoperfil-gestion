<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params($dbconn, 'SELECT codigo, nave, maquina, nombre FROM tt_impeti WHERE sel = $1 ORDER BY nave, soporte, puesto, nombre', array(1)) or incoLogWrite('La consulta fallo [etiquetas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[etiquetas] No hay resultado');
$etiquetas = pg_fetch_all($result);

// preparamos para obtener el cliente
pg_prepare($dbconn, 'maquinas', 'SELECT nombre FROM maquinas WHERE codigo = $1 LIMIT 1');

class PDF extends TCPDF
{
    function Header() {}

    function Footer() {}
}

$pdf = new PDF();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('');
$pdf->SetTitle('Etiquetas');
$pdf->SetFont('dejavusans', '', 12);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(15, 5);
$pdf->SetAutoPageBreak(5, false);
$pdf->AddPage('L');

$H = 11;
$STYLE = array('align' => 'C', 'fitwidth' => true, 'border' => false, 'text' => true, 'font' => 'helvetica', 'fontsize' => 5);

$j = 1;
for ($i = 0; $i < count($etiquetas); $i++) {
    $pedido = 0;
    $producto = 0;
    $actividad = intval($etiquetas[$i]['codigo']);
    $nave = trim($etiquetas[$i]['nave']);
    $maquina = intval($etiquetas[$i]['maquina']);

    $nombreAct = trim($etiquetas[$i]['nombre']);
    $nombreMaq = '';
    switch ($nave) {
        case 'A': $navecod = 1; break;
        case 'B': $navecod = 2; break;
        case 'C': $navecod = 3; break;
        default: $navecod = 0; break;
    }

    if ($navecod > 0) {
        $nombreAct = '(' . $nave . ') ' . $nombreAct;
    }

    if ($maquina > 0) {
        $result = pg_execute($dbconn, 'maquinas', array($maquina));
        $nombreMaq = trim(pg_fetch_all($result)[0]['nombre']);
    }

    $code = str_pad($pedido, 6, '0', STR_PAD_LEFT) .
        str_pad($producto, 3, '0', STR_PAD_LEFT) .
        str_pad($actividad, 2, '0', STR_PAD_LEFT) .
        $navecod .
        str_pad($maquina, 2, '0', STR_PAD_LEFT);

    $xA = $pdf->GetX();
    $yA = $pdf->GetY();
    $pdf->Cell(89, 4, $nombreAct, 0, 1, 'C');
    $pdf->SetX($xA);
    $pdf->Cell(89, 4, $nombreMaq, 0, 1, 'C');
    // $pdf->SetY($pdf->GetY() + 2, false);

    $pdf->write1DBarcode($code, 'C39', $xA + 9, '', 71, $H, 1, $STYLE, '');
    $pdf->SetX($pdf->GetX() + 9);
    // $pdf->write1DBarcode($codbarMontaje, 'C39', '145', '', 65, $H, 1, $STYLE, 'N');

    if ($j % 3 == 0) {
        $pdf->Ln(10);
    } else {
        $pdf->SetY($yA, false);
    }
    $j++;
    if ($j > 21) {
        $j = 1;
        $pdf->AddPage('L');
    }
}

//SALIDA
$dir = INCO_DIR_TALLER;
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'Etiquetas.pdf';
// $fileName = 'CC_2017_006167.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
