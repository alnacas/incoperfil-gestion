<?php

require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/../tiempos/registro-tiempos.php';

// argInsertInDb();

function remakeArr(array $db)
{
    $ret = array();
    for ($i = 0; $i < count($db); $i++) {
        $dia = $db[$i]['dia'];
        unset($db[$i]['dia']);
        $ret[$dia] = $db[$i];
    }
    return $ret;
}

function num2str($number)
{
    $decimals = $number > 0 ? 2 : 0;
    return number_format($number, $decimals, ',', '.');
}

incoLogSetFile($argv[1]);

if (empty($argv[2])) {
    incoLogWrite('No hay argumentos');
}

$fecha = DateTime::createFromFormat('Y-m-d', $argv[2]) or incoLogWrite('Error al procesar la fecha');
$fechaMes = intval($fecha->format('m'));
$fechaMesStr = $fecha->format('F');
$fechaAnyo = intval($fecha->format('Y'));
$fechaIni = new DateTime('first day of ' . $fechaMesStr . ' ' . $fechaAnyo) or incoLogWrite('Error al crear la fecha');
$fechaFin = new DateTime('last day of ' . $fechaMesStr . ' ' . $fechaAnyo) or incoLogWrite('Error al crear la fecha');

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

argInsertInDb($dbconn);

$sql = 'select trim(nombre) as razon_social, trim(nif) as cif, \'\' as ccc from empresa where codigo = 1';
$result = pg_query($dbconn, $sql) or incoLogWrite('La consulta fallo [regtie - empresa]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('No hay empresa [regtie - empresa]: ' . pg_last_error());
$empresa = pg_fetch_all($result)[0];

$sql = 'select codigo, trim(nomape) as nomape, trim(nif) as nif, trim(naf) as naf, trim(horario) as horario, trim(grupo) as grupo from empleados where estado = 1 order by nomape';
$result = pg_query($dbconn, $sql) or incoLogWrite('La consulta fallo [regtie - empleados]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('No hay empleados [regtie - empleados]: ' . pg_last_error());
$empleados = pg_fetch_all($result);

$sql = 'select to_char(manana_entra, \'HH24:MI\') as iniman, to_char(manana_sale, \'HH24:MI\') as finman, to_char(tarde_entra, \'HH24:MI\') as initar, to_char(tarde_sale, \'HH24:MI\') as fintar, horas_efectivas, horas_teoricas, horas_extras, total_jornada, horas from historico_tiempos where empleado = $1 and extract(day from fecha) = $2 and extract(month from fecha) = $3 and extract(year from fecha) = $4';
pg_prepare($dbconn, 'histie', $sql);

$sql = 'select distinct trim(observaciones) as observaciones from historico_tiempos where empleado = $1 and extract(day from fecha) = $2 and extract(month from fecha) = $3 and extract(year from fecha) = $4 order by 1';
pg_prepare($dbconn, 'histieobs', $sql);

class PDF extends TCPDF
{

    /**
     * @var array
     */
    private $empresa;

    public function __construct($empresa, $orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        $this->empresa = $empresa;
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }

    public function Header()
    {
        $this->Ln(15);
        $this->SetFont('dejavusans', '', 8);

        $this->Cell(0, 10, 'REGISTRO DE JORNADA. Trabajadores a tiempo completo', 1, 1, 'C');
        $this->Ln(6);

        $this->Cell(23, 4, 'Razón social:', 0, 0);
        $this->Cell(0, 4, $this->empresa['razon_social'], 0, 1);
        $this->Ln(4);
        $this->Cell(10, 4, 'CIF:', 0, 0);
        $this->Cell(75, 4, $this->empresa['cif'], 0, 0);
        $this->Cell(12, 4, 'CCC:', 0, 0);
        $this->Cell(73, 4, $this->empresa['ccc'], 0, 1);
    }

    public function Footer()
    {
        $this->SetY(-18);
        $this->SetFont('dejavusans', '', 6);
        $this->MultiCell(0, 4, 'Registro basado en la obligación establecida en el art. 35.5 del Texto Refundido del Estatuto de los Trabajadores (RDL 2/2015 de 23 de octubre).' . PHP_EOL, 0);
    }

}

$pdf = new PDF($empresa);
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('Registro de tiempos');
$pdf->SetTitle('Registro de tiempos');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(20, 50);

$fecha = $fechaIni->format('d/m/Y') . ' - ' . $fechaFin->format('d/m/Y');
$formatoHora = 'H:i';
$seisYdiez = DateTime::createFromFormat($formatoHora, '18:10');
$rango = rand(-3, 3);
foreach ($empleados as $empleado) {
    $pdf->AddPage();

    $pdf->Cell(0, 4, 'DATOS DEL TRABAJADOR', 0, 1);
    $pdf->Line($pdf->GetX(), $pdf->GetY(), $pdf->getPageWidth() - $pdf->getMargins()['right'], $pdf->GetY());
    $pdf->Cell(33, 4, 'Nombre y apellidos:', 0, 0);
    $pdf->Cell(0, 4, $empleado['nomape'], 0, 1);
    $pdf->Ln(2);
    $pdf->Cell(25, 4, 'Horario laboral:', 0, 0);
    $pdf->Cell(0, 4, $empleado['horario'], 0, 1);
    $pdf->Ln(2);
    $pdf->Cell(10, 4, 'NIF:', 0, 0);
    $pdf->Cell(75, 4, $empleado['nif'], 0, 0);
    $pdf->Cell(12, 4, 'NAF:', 0, 0);
    $pdf->Cell(73, 4, $empleado['naf'], 0, 1);
    $pdf->Line($pdf->GetX(), $pdf->GetY(), $pdf->getPageWidth() - $pdf->getMargins()['right'], $pdf->GetY());
    $pdf->Ln(6);

    $pdf->Cell(37, 4, 'Periodo de liquidación:', 0, 0);
    $pdf->Cell(48, 4, '', 0, 0);
    $pdf->Cell(12, 4, 'Fecha:', 0, 0);
    $pdf->Cell(73, 4, $fecha, 0, 1);
    $pdf->Ln(6);

    $w = ($pdf->getPageWidth() - $pdf->getMargins()['left'] - $pdf->getMargins()['right']) / 10;
    $h = 5;
    $pdf->SetFillColor(235);
    $pdf->MultiCell($w, $h * 2, 'Día del mes', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
    $pdf->Cell($w * 2, $h, 'Mañana', 1, 0, 'C', true);
    $pdf->Cell($w * 2, $h, 'Tarde', 1, 0, 'C', true);
    $pdf->MultiCell($w, $h * 2, 'Horas', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
    $pdf->MultiCell($w, $h * 2, 'Horas efectivas', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
    $pdf->MultiCell($w, $h * 2, 'Horas teóricas', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
    $pdf->MultiCell($w, $h * 2, 'Horas extras', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
    $pdf->MultiCell($w, $h * 2, 'Total horas jornada', 1, 'C', true, 1, '', '', true, 0, false, true, $h * 2, 'M');
    $pdf->SetY($pdf->GetY() - $h);
    $pdf->SetX($pdf->GetX() + $w);
    $pdf->Cell($w, $h, 'Entrada', 1, 0, 'C', true);
    $pdf->Cell($w, $h, 'Salida', 1, 0, 'C', true);
    $pdf->Cell($w, $h, 'Entrada', 1, 0, 'C', true);
    $pdf->Cell($w, $h, 'Salida', 1, 1, 'C', true);

    $totHorasOrd = 0;
    $totHorasTeo = 0;
    $totHorasExt = 0;
    for ($i = 1; $i <= 31; $i++) {
        $params = array($empleado['codigo'], $i, $fechaMes, $fechaAnyo);
        $result = pg_execute($dbconn, 'histie', $params) or incoLogWrite('La consulta fallo [regtie - histie - ' . $empleado['codigo'] . ']: ' . pg_last_error());
        $histie = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result)[0];
        if (empty($histie)) {
            $pdf->Cell($w, $h, $i, 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 0, 'C');
            $pdf->Cell($w, $h, '', 1, 1, 'C');
            continue;
        }

        $strIniMan = trim($histie['iniman']);
        $strFinMan = trim($histie['finman']);
        $strIniTar = trim($histie['initar']);
        $strFinTar = trim($histie['fintar']);

        $dia = DateTime::createFromFormat('Y-m-d', $fechaAnyo . '-' . str_pad($fechaMes, 2, 0, STR_PAD_LEFT) . '-' . str_pad($i, 2, 0, STR_PAD_LEFT));
        $diaN = intval($dia->format('N'));
        /*if ($diaN == 5 || $empleado['grupo'] == 'OF3') {
            $horasTeoricas = empty($strIniMan) || empty($strFinMan) ? 0 : $histie['horas_teoricas'];
            $horasEfectivas = empty($strIniMan) || empty($strFinMan) ? 0 : $histie['horas_efectivas'];
            $horasExtras = empty($strIniMan) || empty($strFinMan) ? 0 : $histie['horas_extras'];
            $horasJornada = empty($strIniMan) || empty($strFinMan) ? 0 : $histie['total_jornada'];
            $horas = empty($strIniMan) || empty($strFinMan) ? 0 : $histie['horas'];
        } else {
            $horasTeoricas = empty($strIniMan) || empty($strFinMan) || empty($strIniTar) || empty($strFinTar) ? 0 : $histie['horas_teoricas'];
            $horasEfectivas = empty($strIniMan) || empty($strFinMan) || empty($strIniTar) || empty($strFinTar) ? 0 : $histie['horas_efectivas'];
            $horasExtras = empty($strIniMan) || empty($strFinMan) || empty($strIniTar) || empty($strFinTar) ? 0 : $histie['horas_extras'];
            $horasJornada = empty($strIniMan) || empty($strFinMan) || empty($strIniTar) || empty($strFinTar) ? 0 : $histie['total_jornada'];
            $horas = empty($strIniMan) || empty($strFinMan) || empty($strIniTar) || empty($strFinTar) ? 0 : $histie['horas'];
        }*/
        $horasTeoricas = $histie['horas_teoricas'];
        $horasEfectivas = $histie['horas_efectivas'];
        $horasExtras = $histie['horas_extras'];
        $horasJornada = $histie['total_jornada'];
        $horas = $histie['horas'];

        $totHorasOrd += $horasEfectivas;
        $totHorasTeo += $horasTeoricas;
        $totHorasExt += $horasExtras;

        $strHorasOrd = num2str($horasEfectivas);
        $strHorasTeo = num2str($horasTeoricas);
        $strHorasExt = num2str($horasExtras);
        $strHorasJor = num2str($horasJornada);
        $strHoras = num2str($horas);

        $pdf->Cell($w, $h, $i, 1, 0, 'C');
        $pdf->Cell($w, $h, $strIniMan, 1, 0, 'C');
        $pdf->Cell($w, $h, $strFinMan, 1, 0, 'C');
        $pdf->Cell($w, $h, $strIniTar, 1, 0, 'C');
        $pdf->Cell($w, $h, $strFinTar, 1, 0, 'C');
        $pdf->Cell($w, $h, $strHoras, 1, 0, 'C');
        $pdf->Cell($w, $h, $strHorasOrd, 1, 0, 'C');
        $pdf->Cell($w, $h, $strHorasTeo, 1, 0, 'C');
        $pdf->Cell($w, $h, $strHorasExt, 1, 0, 'C');
        $pdf->Cell($w, $h, $strHorasJor, 1, 1, 'C');
    }

    $pdf->Cell($w * 6, $h, 'Total horas mes', 1, 0, 'C', true);
    $pdf->Cell($w, $h, num2str($totHorasOrd), 1, 0, 'C', true);
    $pdf->Cell($w, $h, num2str($totHorasTeo), 1, 0, 'C', true);
    $pdf->Cell($w, $h, num2str($totHorasExt), 1, 0, 'C', true);
    $pdf->Cell($w, $h, num2str($totHorasOrd + $totHorasExt), 1, 1, 'C', true);
    $pdf->Ln(6);

    $pdf->SetFont('dejavusans', 'I', 6);
    $pdf->MultiCell(0, 4, 'Nota: Los 30 minutos del almuerzo están descontados de las horas ordinarias de cada día. Todo el tiempo que no supere las 0,25 horas no se considera como tiempo efectivo.' . PHP_EOL, 0, 'J', false, 1);
    $pdf->SetFont('dejavusans', '', 8);

    $pdf->Ln($h);
    for ($i = 1; $i <= 31; $i++) {
        $params = array($empleado['codigo'], $i, $fechaMes, $fechaAnyo);
        $result = pg_execute($dbconn, 'histieobs', $params) or incoLogWrite('La consulta fallo [regtie - histieobs - ' . $empleado['codigo'] . ']: ' . pg_last_error());
        $histieobs = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result);
        if (empty($histieobs)) {
            continue;
        }

        $dia = DateTime::createFromFormat('Y-m-d', $fechaAnyo . '-' . str_pad($fechaMes, 2, 0, STR_PAD_LEFT) . '-' . str_pad($i, 2, 0, STR_PAD_LEFT));
        $textDia = false;
        foreach ($histieobs as $histieob) {
            if (empty($histieob['observaciones'])) continue;
            if (!$textDia) {
                $pdf->Cell(0, $h, 'Día ' . $dia->format('d/m/Y') . ':', 0, 1);
                $textDia = true;
            }
            $pdf->MultiCell(0, 4, $histieob['observaciones'] . PHP_EOL, 0, 'J', false, 1);
        }
        $pdf->Ln($h);
    }
}

$output = INCO_DIR_DOCUMENTOS . '/InformeTiemposMensual.pdf';
$pdf->Output($output, 'F');