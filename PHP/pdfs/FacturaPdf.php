<?php

use setasign\Fpdi\Tcpdf\Fpdi;

require_once __DIR__ . '/../vendor/autoload.php';

class FacturaPdf extends Fpdi {

    const MAX_CONTENT_HEIGHT = 223;

    const MIN_LINE_HEIGHT = 4;

    const LINE_HEIGHT = 4;

    const LINE_MARGIN_BOTTOM = 1;

    /**
     * [0] + [1] = CANTIDAD ([0]: cantidad, [1]: unidad medida)
     * [2] = DESCRIPCION
     * [3] = PRECIO
     * [4] = DESCUENTO
     * [5] = IMPORTE
     */
    const WIDTH_COLUMNS = [18, 7, 130, 20, 15, 0]; // se quita la columna DESCUENTO y paso el ancho a la columna DESCRIPCION

    /**
     * @var IncoTranslatorFix
     */
    private $tr;

    /**
     * @var array
     * keys: folder (string), print (boolean)
     */
    private $logos;

    /**
     * @var array
     */
    private $cliente;

    /**
     * @var array
     */
    private $factura;

    /**
     * @var array
     */
    private $facturaLns;

    /**
     * @param IncoTranslatorFix $tr
     */
    public function setTr(IncoTranslatorFix $tr)
    {
        $this->tr = $tr;
    }

    /**
     * @param array $logos
     */
    public function setLogos(array $logos)
    {
        $this->logos = $logos;
    }

    /**
     * @param array $cliente
     */
    public function setCliente(array $cliente)
    {
        $this->cliente = $cliente;

        $this->cliente['hasDirAdm'] = !$this->factura['isProforma'] && !$this->logos['print'] && ($cliente['direccion'] != $cliente['direccionadm'] || $cliente['codpo'] != $cliente['codpoadm'] || $cliente['poblacion'] != $cliente['poblacionadm'] || $cliente['provincia'] != $cliente['provinciaadm']);
        if ($this->cliente['hasDirAdm']) {
            $this->AddPage();
        }
    }

    /**
     * @param array $factura
     */
    public function setFactura(array $factura)
    {
        $this->factura = $factura;

        $this->SetSubject('FACTURA');
        $this->SetTitle('Factura ');
    }

    /**
     * @param array $facturaLns
     */
    public function setFacturaLns(array $facturaLns)
    {
        $this->facturaLns = $facturaLns;
    }

    /**
     * FacturaPdf constructor.
     * @param string $orientation
     * @param string $unit
     * @param string $format
     * @param bool $unicode
     * @param string $encoding
     * @param bool $diskcache
     * @param bool $pdfa
     */
    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false)
    {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);

        $this->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
        $this->SetCreator('GESTION');
        $this->SetFont('dejavusans', '', 8);
        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->SetMargins(5, 63); // 70
        $this->SetAutoPageBreak(true, $this->getPageHeight() - self::MAX_CONTENT_HEIGHT);
    }

    public function Header()
    {
        if ($this->logos['print']) {
            $this->Image($this->logos['folder'] . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
            $this->Image($this->logos['folder'] . 'piepagina.jpg', 0, 278, $this->w + 1, 0, 'jpg', '', '', true, 300);
        }

        if ($this->cliente['hasDirAdm']) {
            $this->SetY(32);
            $this->Ln();
            $this->Ln();
            $this->SetFont('dejavusans', '', 8);
            $this->setCellPaddings('3', '1', '3', '1');
            $this->Ln();
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, str_pad($this->cliente['codigo'], 5, '0', STR_PAD_LEFT), 'LTR', 1);
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, trim($this->cliente['nombre']), 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, trim($this->cliente['direccionadm']), 'LR', 1);
            $this->SetX(100);
            $txt = trim($this->cliente['codpoadm']) . ' - ' . trim($this->cliente['poblacionadm']) . ' (' . trim($this->cliente['provinciaadm']) . ')';
            $this->Cell(0, self::LINE_HEIGHT, $txt, 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, trim($this->cliente['pais']['nombre']), 'LRB', 1);
            $this->Ln(2);
        } else {
            /*if (!$this->factura['isProforma']) {
                $this->Image($this->logos['folder'] . 'cyc.jpg', 165, 251, 30, 0, 'jpg', '', '', true, 300);
            }*/

            $txt = gettext('Factura');
            if ($this->factura['isProforma']) $txt = gettext('Factura proforma');
            else {
                if ($this->factura['serie'] == 'FA') $txt = gettext('Factura anticipo');
                if ($this->factura['serie'] == 'FR') $txt = gettext('Factura rectificativa');
            }

            $this->SetY(5);
            $this->SetFont('dejavusans', 'B', 22);
            $this->Cell(0, self::LINE_HEIGHT, $txt, 0, 1, 'R');
            $this->SetFont('dejavusans', '', 8);
            $this->Cell(0, self::LINE_HEIGHT, sprintf(gettext('Hoja: %d'), $this->PageNo()), 0, 1, 'R');
            $this->Ln();
            $y = $this->GetY();

            $this->SetY($y);
            $txt = $this->factura['isProforma'] ? gettext('F. Proforma Nº') : gettext('Factura Nº');
            $this->Cell(25, self::LINE_HEIGHT, $txt, 0, 0);
            $this->SetFont('dejavusans', 'B', 8);
            if ($this->factura['isProforma']) {
                $serie = substr($this->factura['serie'], 0, 1);
            } else {
                $serie = trim($this->factura['serie']) == 'FR' ? 'R' : 0;
            }
            $this->Cell(60, self::LINE_HEIGHT, $serie . '/' . str_pad($this->factura['numero'], 6, '0', STR_PAD_LEFT), 0, 1);
            $this->SetFont('dejavusans', '', 8);
            $this->Cell(25, self::LINE_HEIGHT, gettext('Fecha'), 0, 0);
            $this->Cell(60, self::LINE_HEIGHT, $this->factura['fecha'], 0, 1);
            $this->Cell(25, self::LINE_HEIGHT, gettext('N.I.F'), 0, 0);
            $this->Cell(60, self::LINE_HEIGHT, $this->cliente['nif'], 0, 1);

            $this->SetY($y);
            // $this->Ln();
            // $this->Ln();
            $this->SetFont('dejavusans', '', 8);
            $this->setCellPaddings('3', '1', '3', '1');
            // $this->Ln();
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, str_pad($this->cliente['codigo'], 5, '0', STR_PAD_LEFT), 'LTR', 1);
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, trim($this->cliente['nombre']), 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, trim($this->cliente['direccion']), 'LR', 1);
            $this->SetX(100);
            $txt = trim($this->cliente['codpo']) . ' - ' . trim($this->cliente['poblacion']) . ' (' . trim($this->cliente['provincia']) . ')';
            $this->Cell(0, self::LINE_HEIGHT, $txt, 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, self::LINE_HEIGHT, trim($this->cliente['pais']['nombre']), 'LRB', 1);
            $this->Ln(2);

            $this->SetCellPadding(0);
            $this->SetXY(0, 90);
            $this->Cell(10, 4, '--');
            $this->SetXY(0, 200);
            $this->Cell(10, 4, '--');
        }
    }

    public function Footer()
    {
        if ($this->cliente['hasDirAdm']) {
            $this->cliente['hasDirAdm'] = false;
        } else {
            $this->SetY(-70);
            $this->SetFont('dejavusans', '', 8);
            $this->Cell(30, self::LINE_HEIGHT, gettext('Total sin recargos'), 1, 0, 'C');
            if ($this->factura['descuento_pronto_pago'] == 0) {
                $this->Cell(65, self::LINE_HEIGHT, '', 1, 0, 'C');
            } else {
                $this->Cell(65, self::LINE_HEIGHT, gettext('Descuento Pronto Pago'), 1, 0, 'C');
            }
            $this->Cell(30, self::LINE_HEIGHT, gettext('Base imponible'), 1, 0, 'C');
            $this->Cell(18, self::LINE_HEIGHT, gettext('Tipo IVA'), 1, 0, 'C');
            $this->Cell(25, self::LINE_HEIGHT, gettext('Importe IVA'), 1, 0, 'C');
            $this->Cell(0, self::LINE_HEIGHT, gettext('TOTAL FACTURA'), 1, 1, 'C');

            $this->setCellPaddings('0', '0', '2', '0');
            $this->Cell(30, self::LINE_HEIGHT, number_format($this->factura['bruto'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
            if ($this->factura['descuento_pronto_pago'] == 0) {
                $this->Cell(65, self::LINE_HEIGHT, '', 'LTR', 0, 'C');
            } else {
                $txt = number_format($this->factura['descuento_pronto_pago'], 0) . '% s/ ' . number_format($this->factura['bruto'], 2, ',', '.') . ' € = ' . number_format($this->factura['total_descuento_pronto_pago'] * -1, 2, ',', '.') . ' €';
                $this->Cell(65, self::LINE_HEIGHT, $txt, 'LTR', 0, 'C');
            }

            if ($this->factura['iva'] == 0) {
                $this->Cell(30, self::LINE_HEIGHT, number_format($this->factura['base_exenta'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
                $this->Cell(18, self::LINE_HEIGHT, '0,00 %', 'LTR', 0, 'C');
                $this->Cell(25, self::LINE_HEIGHT, '0,00 €', 'LTR', 0, 'R');
            }
            if ($this->factura['iva'] == 1) {
                $this->Cell(30, self::LINE_HEIGHT, number_format($this->factura['base_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
                $this->Cell(18, self::LINE_HEIGHT, number_format($this->factura['tipo_iva_0'], 0) . ' %', 'LTR', 0, 'C');
                $this->Cell(25, self::LINE_HEIGHT, number_format($this->factura['total_iva_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
            }
            $this->SetFont('dejavusans', 'B');
            $this->Cell(0, self::LINE_HEIGHT, number_format($this->factura['total_factura'], 2, ',', '.') . ' €', 'LTR', 1, 'R');

            $this->SetFont('dejavusans', '');
            if ($this->factura['base_1'] == 0) {
                $this->Cell(30, self::LINE_HEIGHT, '', 'LR', 0, 'R');
                $this->Cell(65, self::LINE_HEIGHT, '', 'LR', 0, 'C');
                $this->Cell(30, self::LINE_HEIGHT, '', 'LR', 0, 'R');
                $this->Cell(18, self::LINE_HEIGHT, '', 'LR', 0, 'C');
                $this->Cell(25, self::LINE_HEIGHT, '', 'LR', 0, 'R');
                $this->Cell(0, self::LINE_HEIGHT, '', 'LR', 1, 'R');
            } else {
                $this->Cell(30, self::LINE_HEIGHT, '', 'LR', 0, 'R');
                $this->Cell(65, self::LINE_HEIGHT, '', 'LR', 0, 'C');
                $this->Cell(30, self::LINE_HEIGHT, number_format($this->factura['base_1'], 2, ',', '.') . ' €', 'LR', 0, 'R');
                $this->Cell(18, self::LINE_HEIGHT, number_format($this->factura['tipo_iva_1'], 0) . ' %', 'LR', 0, 'C');
                $this->Cell(25, self::LINE_HEIGHT, number_format($this->factura['total_iva_1'], 2, ',', '.') . ' €', 'LR', 0, 'R');
                $this->SetFont('dejavusans', 'B');
                $this->Cell(0, self::LINE_HEIGHT, '', 'LR', 1, 'R');
            }

            $this->SetFont('dejavusans', '');
            if ($this->factura['base_2'] == 0) {
                $this->Cell(30, self::LINE_HEIGHT, '', 'LBR', 0, 'R');
                $this->Cell(65, self::LINE_HEIGHT, '', 'LBR', 0, 'C');
                $this->Cell(30, self::LINE_HEIGHT, '', 'LBR', 0, 'R');
                $this->Cell(18, self::LINE_HEIGHT, '', 'LBR', 0, 'C');
                $this->Cell(25, self::LINE_HEIGHT, '', 'LBR', 0, 'R');
                $this->Cell(0, self::LINE_HEIGHT, '', 'LBR', 1, 'R');
            } else {
                $this->Cell(30, self::LINE_HEIGHT, '', 'LBR', 0, 'R');
                $this->Cell(65, self::LINE_HEIGHT, '', 'LBR', 0, 'C');
                $this->Cell(30, self::LINE_HEIGHT, number_format($this->factura['base_2'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
                $this->Cell(18, self::LINE_HEIGHT, number_format($this->factura['tipo_iva_2'], 0) . ' %', 'LBR', 0, 'C');
                $this->Cell(25, self::LINE_HEIGHT, number_format($this->factura['total_iva_2'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
                $this->SetFont('dejavusans', 'B');
                $this->Cell(0, self::LINE_HEIGHT, '', 'LBR', 1, 'R');
            }
            $this->Ln(2);

            $vencimientos = array();
            $importes = array();
            for ($i = 0; $i < count($this->factura['efectos']); $i++) {
                if (intval($this->factura['efectos'][$i]['orden']) == 0) {
                    array_push($vencimientos, gettext('Anticipado'));
                } else {
                    $fvto = explode('-', $this->factura['efectos'][$i]['fvto']);
                    array_push($vencimientos, $fvto[2] . '/' . $fvto[1] . '/' . $fvto[0]);
                }
                array_push($importes, number_format($this->factura['efectos'][$i]['importe'], 2, ',', '.') . ' €');
            }

            $this->SetFont('dejavusans', '');
            $this->SetCellPadding(0);
            $this->Cell(30, self::LINE_HEIGHT, gettext('Domicilio de Pago'), 0, 0);
            // $this->SetFont('dejavusans', 'B');
            if (empty($this->factura['oferta']['forma_pago_alternativa'])) {
                $formaPago = $this->tr->getTranslate($this->factura['forma_pago']['nombre']);
                if (!is_null($this->factura['forma_pago']['banco'])) {
                    $formaPago .= ' - ' . trim($this->factura['forma_pago']['banco']['nombre']);
                    $formaPago .= ' - ' . $this->factura['forma_pago']['banco']['cuenta'];
                }
                if (in_array($this->factura['tipoefec'], ['L', 'A'])) {
                    $formaPago .= ' - ' . $this->cliente['cuenta'];
                }
            } else {
                $formaPago = gettext('Ver forma de pago arriba (*)');
            }
            $yAntesFormaPago = $this->GetY();

            if ($this->factura['isProforma']) {
                $this->MultiCell(0, 8, $formaPago . PHP_EOL, 0, 'J', false, 1);
                $this->Cell(30, self::LINE_HEIGHT, gettext('El pedido se ajustará a las condiciones generales de venta CGV (ver hojas anexas).'), 0, 1);
            } else {
                $this->Cell(0, self::LINE_HEIGHT, $formaPago, 0, 1);

                if ($this->factura['serie'] != 'FR' && !(floatval($this->factura['anticipo_con_factura']) > 0 && floatval($this->factura['total_factura']) == 0)) {
                    $this->SetFont('dejavusans', '');
                    $this->Cell(30, self::LINE_HEIGHT, gettext('Vencimientos'), 0, 0);
                    // $this->SetFont('dejavusans', 'B');
                    for ($i = 0; $i < count($vencimientos); $i++) {
                        $ln = ($i == count($vencimientos) - 1) ? 1 : 0;
                        $this->Cell(20, self::LINE_HEIGHT, $vencimientos[$i], 0, $ln, 'C');
                    }
                    $this->SetFont('dejavusans', '');
                    $this->Cell(30, self::LINE_HEIGHT, gettext('Importes'), 0, 0);
                    // $this->SetFont('dejavusans', 'B');
                    for ($i = 0; $i < count($importes); $i++) {
                        $ln = ($i == count($importes) - 1) ? 1 : 0;
                        $this->Cell(20, self::LINE_HEIGHT, $importes[$i], 0, $ln, 'C');
                    }
                }

            }

            if ($this->factura['serie'] != 'FR') {
                $y = $this->factura['isProforma'] ? 251 : $yAntesFormaPago + 4 + 1;
                $this->SetXY($this->GetX() + 134, $y);
                $this->SetFont('dejavusans', 'B');
                $this->Cell(0, self::LINE_HEIGHT, gettext('PRODUCTO FABRICADO EN ESPAÑA'), 0, 0, 'R');
            }

            $this->SetY($yAntesFormaPago + 10 + 5);
            $this->SetFont('dejavusans', '', 7);
            $rgpd = "Información básica sobre protección de datos:\nResponsable: Ingeniería y Construccion del Perfil, S.A. Finalidades: Gestionar los servicios contratados y facturación. Legitimación: Ejecución de un contrato y consentimiento del interesado. Destinatarios: No se cederán datos a terceros, salvo obligación legal. Derechos: Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la información adicional. Información adicional: Puede consultar la información adicional y detallada sobre Protección de Datos en www.incoperfil.com" . PHP_EOL;
            $this->MultiCell(188, self::LINE_HEIGHT, $rgpd, 0, 'J', false, 1);
        }
    }

    public function AddContent() {
        $this->AddPage();

        $this->AddLinesHeader();

        if (!empty($this->factura['texto_iva'])) {
            $height = $this->GetHeight(115, $this->factura['texto_iva']);

            $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], $height, '', 'LR', 0);
            $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $this->factura['texto_iva'], 'LR', 'L', false, 0);
            $this->Cell(self::WIDTH_COLUMNS[3], $height, '', 'LR', 0);
            // $this->Cell(self::WIDTH_COLUMNS[4], $height, '', 'LR', 0);
            $this->Cell(self::WIDTH_COLUMNS[5], $height, '', 'LR', 1);

            $this->LnLns();
        }

        if ($this->factura['serie'] == 'FA') {
            $importe = $this->factura['iva'] == 0 ? $this->factura['base_exenta'] : $this->factura['base_0'];
            $importe = $this->Num2Str($importe);

            $txt = trim($this->factura['observaciones']);
            $height = $this->GetHeight(115, $txt);

            $this->Cell(self::WIDTH_COLUMNS[0], $height, $this->Num2Str(1), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
            $this->Cell(self::WIDTH_COLUMNS[1], $height, 'UD', 'R', 0, 'C', false, '', 0, false, 'T', 'T');
            $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $txt, 'LR', 'L', false, 0);
            $this->Cell(self::WIDTH_COLUMNS[3], $height, $importe, 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
            // $this->Cell(self::WIDTH_COLUMNS[4], $height, '', 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
            $this->Cell(self::WIDTH_COLUMNS[5], $height, $importe, 'LR', 1, 'R', false, '', 0, false, 'T', 'T');
        }

        foreach ($this->facturaLns as $albaran) {
            if ($this->facturaLns[0] != $albaran) {
                $this->LnLns();
            }

            $pedido = null;
            // $this->AddHeaderAlbaran($albaran['numero'], $albaran['referencia_cliente'], $albaran['identificador_oferta'], $albaran['referencia_obra']);

            foreach ($albaran['lineas'] as $linea) {
                /*if (!$this->factura['isProforma'] && ($linea['cantidad'] == 0 || $linea['importe'] == 0)) {
                    continue;
                }*/
                if (is_null($pedido) || $pedido != $linea['identificador_pedido']) {
                    if (!is_null($pedido)) $this->LnLns();
                    $this->AddHeaderAlbaran($albaran['numero'], $albaran['referencia_cliente'], $linea['identificador_pedido'], $albaran['identificador_oferta'], $albaran['referencia_obra']);
                    $pedido = $linea['identificador_pedido'];
                }

                $descripcion = trim($linea['descripcion_comercial']);
                $descripcion = str_replace(' | ', PHP_EOL, $descripcion);

                if (isset($linea['referencia_plano'])) {
                    $refPla = trim($linea['referencia_plano']);
                    if (!empty($refPla)) {
                        $descripcion .= ' ' . $refPla;
                    }
                }

                if (isset($linea['observaciones'])) {
                    $obs = trim($linea['observaciones']);
                    if (!empty($obs)) {
                        $descripcion .= PHP_EOL . $obs;
                    }
                }

                $descripcion = $this->tr->getTranslate($descripcion);
                $height = $this->GetHeight(self::WIDTH_COLUMNS[2], $descripcion);

                if ($this->checkPageBreak($height, '', false)) {
                    $this->AddLinesHeader(true);
                    // $this->AddHeaderAlbaran($albaran['numero'], $albaran['referencia_cliente'], $albaran['identificador_oferta'], $albaran['referencia_obra']);
                    $this->AddHeaderAlbaran($albaran['numero'], $albaran['referencia_cliente'], $linea['identificador_pedido'], $albaran['identificador_oferta'], $albaran['referencia_obra']);
                }

                $extraBorder = abs(self::MAX_CONTENT_HEIGHT - ($this->GetY() + $height)) < self::MIN_LINE_HEIGHT ? 'B' : '';

                $metros = isset($linea['metros']) ? $this->Num2Str($linea['metros']) : '';
                $unidad_medida = isset($linea['unidad_medida']) ? $linea['unidad_medida'] : '';
                $precio = isset($linea['precio']) ? $this->Num2Str($linea['precio']) : '';
                if ($precio == '') $precio = '--';
                $descuento = isset($linea['descuento']) ? $this->Num2Str($linea['descuento']) : '';
                $importe = isset($linea['importe']) ? $this->Num2Str($linea['importe']) : '';
                // if ($importe == '') $importe = '--';
                if ($importe == '') $importe = gettext('SIN CARGO');

                $this->Cell(self::WIDTH_COLUMNS[0], $height, $metros, 'L' . $extraBorder, 0, 'R', false, '', 0, false, 'T', 'T');
                $this->Cell(self::WIDTH_COLUMNS[1], $height, $unidad_medida, 'R' . $extraBorder, 0, 'C', false, '', 0, false, 'T', 'T');
                $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $descripcion, 'LR' . $extraBorder, 'L', false, 0);
                $this->Cell(self::WIDTH_COLUMNS[3], $height, $precio, 'LR' . $extraBorder, 0, 'R', false, '', 0, false, 'T', 'T');
                // $this->Cell(self::WIDTH_COLUMNS[4], $height, $descuento, 'LR' . $extraBorder, 0, 'R', false, '', 0, false, 'T', 'T');
                $this->Cell(self::WIDTH_COLUMNS[5], $height, $importe, 'LR' . $extraBorder, 1, 'R', false, '', 0, false, 'T', 'T');
            }

            if ($albaran['portes'] != 2 && $albaran['total_portes'] > 0) {
                $cantidad = isset($albaran['numero_portes']) ? $albaran['numero_portes'] : 1;
                $texto = '';
                if ($albaran['portes'] == 0 || $albaran['portes'] == 3) {
                    $texto = gettext('TRANSPORTE MATERIAL');
                }
                if ($albaran['portes'] == 1) {
                    $texto = gettext('EXCESO EN NÚMERO DE PORTES');
                }

                // $isLast = array_search($albaran, $this->facturaLns) == count($this->facturaLns) - 1;
                $textoTransporte = trim($this->factura['oferta']['transporte']);
                if (!empty($textoTransporte)) {
                    if (!empty($texto)) $texto .= PHP_EOL;
                    $texto .= $textoTransporte;
                }

                $height = $this->GetHeight(self::WIDTH_COLUMNS[2], $texto);

                if ($this->checkPageBreak($height, '', false)) {
                    $this->AddLinesHeader(true);
                    // $this->AddHeaderAlbaran($albaran['numero'], $albaran['referencia_cliente'], $albaran['identificador_oferta'], $albaran['referencia_obra']);
                    $this->AddHeaderAlbaran($albaran['numero'], $albaran['referencia_cliente'], '', $albaran['identificador_oferta'], $albaran['referencia_obra']);
                }

                $extraBorder = abs(self::MAX_CONTENT_HEIGHT - ($this->GetY() + $height)) < self::MIN_LINE_HEIGHT ? 'B' : '';

                $this->Cell(self::WIDTH_COLUMNS[0], $height, $cantidad, 'L' . $extraBorder, 0, 'R', false, '', 0, false, 'T', 'T');
                $this->Cell(self::WIDTH_COLUMNS[1], $height, 'UD', 'R' . $extraBorder, 0, 'C', false, '', 0, false, 'T', 'T');
                $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $texto . PHP_EOL, 'LR' . $extraBorder, 'L', false, 0);
                $this->Cell(self::WIDTH_COLUMNS[3], $height, $this->Num2Str($albaran['total_portes']), 'LR' . $extraBorder, 0, 'R', false, '', 0, false, 'T', 'T');
                // $this->Cell(self::WIDTH_COLUMNS[4], $height, '', 'LR' . $extraBorder, 0, 'R', false, '', 0, false, 'T', 'T');
                $this->Cell(self::WIDTH_COLUMNS[5], $height, $this->Num2Str($cantidad * $albaran['total_portes']), 'LR' . $extraBorder, 1, 'R', false, '', 0, false, 'T', 'T');
            }
        }

        $txtFactura = trim($this->factura['oferta']['texto_factura']);
        if (!empty($txtFactura)) {
            $txtFactura = $this->tr->getTranslate($txtFactura);
            $txtFactura = str_replace('|', PHP_EOL, $txtFactura);
            $height = $this->GetHeight(self::WIDTH_COLUMNS[2], $txtFactura);

            if ($this->checkPageBreak($height, '', false)) {
                $this->AddLinesHeader(true);
            } else {
                $this->LnLns();
            }

            $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], $height, '', 'LR', 0, 'R');
            $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $txtFactura . PHP_EOL, 'LR', 'L', false, 0);
            $this->Cell(self::WIDTH_COLUMNS[3], $height, '', 'LR', 0, 'R');
            // $this->Cell(self::WIDTH_COLUMNS[4], $height, '', 'LR', 0, 'R');
            $this->Cell(self::WIDTH_COLUMNS[5], $height, '', 'LR', 1, 'R');
        }

        if (!$this->factura['isProforma']) {
            if (floatval($this->factura['anticipo_con_factura']) > 0 && !is_null($this->factura['factura_anticipado'])) {
                // anticipo facturado
                $this->LnLns();

                $numFactura = intval(substr($this->factura['factura_anticipado'], -7));
                /*if (boolval($this->factura['iva'])) {
                    $anticipo = floatval($this->factura['anticipo_con_factura'] / (1 + ($this->factura['tipo_iva_0'] / 100))) * -1;
                } else {
                    $anticipo = floatval($this->factura['anticipo_con_factura']) * -1;
                }*/
                $anticipo = floatval($this->factura['anticipo_con_factura']) * -1;
                $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], self::LINE_HEIGHT, '', 'LR', 0, 'C');
                $this->Cell(self::WIDTH_COLUMNS[2], self::LINE_HEIGHT, sprintf(gettext('FACTURA DE ANTICIPO NÚMERO %d'), $numFactura), 'LR', 0);
                $this->Cell(self::WIDTH_COLUMNS[3], self::LINE_HEIGHT, '', 'LR', 0, 'R');
                // $this->Cell(self::WIDTH_COLUMNS[4], self::LINE_HEIGHT, '', 'LR', 0, 'R');
                $this->Cell(self::WIDTH_COLUMNS[5], self::LINE_HEIGHT, $this->Num2Str($anticipo), 'LR', 1, 'R');
            }
        }

        if ($this->factura['serie'] == 'FR') {
            $tipo = trim($this->factura['FR']['tipo_rectificativa']);
            $fact = trim($this->factura['FR']['factura_rectificada']);

            switch ($tipo) {
                case 'I': $motivo = gettext('POR DIFERENCIA DE PRECIOS'); break;
                default : $motivo = '';
            }

            $txt = trim(sprintf(gettext('FACTURA RECTIFICATIVA A LA FACTURA NÚMERO %d %s'), intval(substr($fact, 9)), $motivo));

            $height = $this->GetHeight(self::WIDTH_COLUMNS[2], $txt);

            if ($this->checkPageBreak($height, '', false)) {
                $this->AddLinesHeader(true);
            } else {
                $this->LnLns();
            }

            $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], $height, '', 'LR', 0, 'C');
            $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $txt, 'LR', 'L', false, 0);
            $this->Cell(self::WIDTH_COLUMNS[3], $height, '', 'LR', 0, 'C');
            // $this->Cell(self::WIDTH_COLUMNS[4], $height, '', 'LR', 0, 'C');
            $this->Cell(self::WIDTH_COLUMNS[5], $height, '', 'LR', 1, 'C');
        }

        if (!empty($this->factura['oferta']['forma_pago_alternativa'])) {
            $formaPago = '(*) ' . gettext('Domicilio de Pago') . ': ' . $this->factura['forma_pago']['nombre'];
            $height = $this->GetHeight(self::WIDTH_COLUMNS[2], $formaPago);

            if ($this->checkPageBreak($height, '', false)) {
                $this->AddLinesHeader(true);
            } else {
                $this->LnLns();
            }

            $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], $height, '', 'LR', 0, 'R');
            $this->MultiCell(self::WIDTH_COLUMNS[2], $height, $formaPago . PHP_EOL, 'LR', 'L', false, 0);
            $this->Cell(self::WIDTH_COLUMNS[3], $height, '', 'LR', 0, 'R');
            // $this->Cell(self::WIDTH_COLUMNS[4], $height, '', 'LR', 0, 'R');
            $this->Cell(self::WIDTH_COLUMNS[5], $height, '', 'LR', 1, 'R');
        }

        $this->LnLns(self::MAX_CONTENT_HEIGHT - $this->GetY(), 'B');
    }

    /**
     * @param $num
     * @param int $decimals
     * @param string $dec_point
     * @param string $thousands_sep
     * @return string
     */
    private function Num2Str($num, $decimals = 2 , $dec_point = ',' , $thousands_sep = '.') {
        $str = '';
        if ($num != 0) {
            $str = number_format($num, $decimals, $dec_point, $thousands_sep);
        }
        return $str;
    }

    /**
     * @param int $h
     * @param string $extraBorder
     */
    private function LnLns(int $h = self::LINE_HEIGHT, string $extraBorder = '') {
        // SE DESACTIVA PORQUE AL IMPRIMIR LA FACTURA 190838 ENTRO EN BUCLE (revisar abs(...))
        /*if ($h == self::LINE_HEIGHT && ($this->checkPageBreak($h, '', false) || abs(self::MAX_CONTENT_HEIGHT - ($this->GetY() + 2)) < self::MIN_LINE_HEIGHT)) {
            echo 'LnLns AddLinesHeader ' . json_encode([$h, $extraBorder]) . PHP_EOL;
            $this->AddLinesHeader(true);
            return;
        }*/

        $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], $h, '', 'LR' . $extraBorder, 0);
        $this->Cell(self::WIDTH_COLUMNS[2], $h, '', 'LR' . $extraBorder, 0);
        $this->Cell(self::WIDTH_COLUMNS[3], $h, '', 'LR' . $extraBorder, 0);
        // $this->Cell(self::WIDTH_COLUMNS[4], $h, '', 'LR' . $extraBorder, 0);
        $this->Cell(self::WIDTH_COLUMNS[5], $h, '', 'LR' . $extraBorder, 1);
    }

    private function AddLinesHeader(bool $finishPage = false) {
        if ($finishPage) {
            if (self::MAX_CONTENT_HEIGHT - $this->GetY() >= self::MIN_LINE_HEIGHT) {
                $this->LnLns(self::MAX_CONTENT_HEIGHT - $this->GetY(), 'B');
            }
            $this->AddPage();
        }

        $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], self::LINE_HEIGHT, gettext('CANTIDAD'), 1, 0, 'C');
        $this->Cell(self::WIDTH_COLUMNS[2], self::LINE_HEIGHT, gettext('DESCRIPCIÓN'), 1, 0, 'C');
        $this->Cell(self::WIDTH_COLUMNS[3], self::LINE_HEIGHT, gettext('PRECIO (€)'), 1, 0, 'C');
        // $this->Cell(self::WIDTH_COLUMNS[4], self::LINE_HEIGHT, gettext('% DTO.'), 1, 0, 'C');
        $this->Cell(self::WIDTH_COLUMNS[5], self::LINE_HEIGHT, gettext('IMPORTE (€)'), 1, 1, 'C');
    }

    /**
     * @param $numero
     * @param $refCli
     * @param $idPedido
     * @param $idOferta
     * @param $refObra
     */
    private function AddHeaderAlbaran($numero, $refCli, $idPedido, $idOferta, $refObra) {
        if ($this->checkPageBreak(9, '', false)) {
            $this->AddLinesHeader(true);
        }

        if (!is_null($numero) || !is_null($idPedido) || !is_null($idOferta)) {
            $numPedido = substr($idPedido, -6);
            $numOferta = substr(trim($idOferta), 8);
            $info = [
                'albaran' => empty($numero) || intval($numero) == intval($numPedido) || intval($numero) == intval($numOferta) ? '' : sprintf(gettext('ALBARÁN: %s'), intval($numero)),
                'pedido' => empty($idPedido) ? '' : sprintf(gettext('PEDIDO: %s'), intval($numPedido)),
                'oferta' => empty($idOferta) ? '' : sprintf(gettext('OFERTA: %s'), intval($numOferta)),
            ];

            $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], self::LINE_HEIGHT, '', 'LR', 0, 'C');
            $w = self::WIDTH_COLUMNS[2] / 3;
            $this->Cell($w, self::LINE_HEIGHT, $info['albaran'], 'L', 0);
            $this->Cell($w, self::LINE_HEIGHT, $info['pedido'], 0, 0);
            $this->Cell($w, self::LINE_HEIGHT, $info['oferta'], 'R', 0);
            $this->Cell(self::WIDTH_COLUMNS[3], self::LINE_HEIGHT, '', 'LR', 0, 'C');
            // $this->Cell(self::WIDTH_COLUMNS[4], self::LINE_HEIGHT, '', 'LR', 0, 'C');
            $this->Cell(self::WIDTH_COLUMNS[5], self::LINE_HEIGHT, '', 'LR', 1, 'C');
        }

        $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], self::LINE_HEIGHT, '', 'LR', 0, 'C');
        $this->Cell(self::WIDTH_COLUMNS[2], self::LINE_HEIGHT, trim(sprintf(gettext('REF. OBRA: %s'), $refObra)), 'LR', 0, 'L');
        $this->Cell(self::WIDTH_COLUMNS[3], self::LINE_HEIGHT, '', 'LR', 0, 'C');
        // $this->Cell(self::WIDTH_COLUMNS[4], self::LINE_HEIGHT, '', 'LR', 0, 'C');
        $this->Cell(self::WIDTH_COLUMNS[5], self::LINE_HEIGHT, '', 'LR', 1, 'C');

        if (!empty($refCli)) {
            $this->Cell(self::WIDTH_COLUMNS[0] + self::WIDTH_COLUMNS[1], self::LINE_HEIGHT, '', 'LR', 0, 'C');
            $this->Cell(self::WIDTH_COLUMNS[2], self::LINE_HEIGHT, trim(sprintf(gettext('REF. CLIENTE: %s'), $refCli)), 'LR', 0, 'L');
            $this->Cell(self::WIDTH_COLUMNS[3], self::LINE_HEIGHT, '', 'LR', 0, 'C');
            // $this->Cell(self::WIDTH_COLUMNS[4], self::LINE_HEIGHT, '', 'LR', 0, 'C');
            $this->Cell(self::WIDTH_COLUMNS[5], self::LINE_HEIGHT, '', 'LR', 1, 'C');
        }
    }

    /**
     * @param $w
     * @param $txt
     * @return float|int
     */
    private function GetHeight($w, $txt) {
        $height = $this->getStringHeight($w, $txt, true) + self::LINE_MARGIN_BOTTOM;
        if ($height < self::MIN_LINE_HEIGHT) $height = self::MIN_LINE_HEIGHT;
        return $height;
    }

}