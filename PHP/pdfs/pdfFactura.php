<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$serie = substr($identificador, 6, 2);
$idioma = isset($argv[3]) ? trim($argv[3]) : '';
$printLogos = isset($argv[4]) ? boolval($argv[4]) : true;
$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$stmtClientes = 'clientes';
$stmtOfertas = array('identificador' => '', 'referencia_obra' => '', 'condiciones' => '', 'transporte' => '', 'texto_factura' => '', 'forma_pago_alternativa' => '');

pg_prepare($dbconn, 'clientes', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma, cpais as pais, iban, cbanco, csucursal, cdc, cuenta, direccionadm, codpoadm, poblacionadm, provinciaadm, dpago1, dpago2, dpago3 FROM clientes WHERE codigo = $1');

pg_prepare($dbconn, 'potenciales', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma, cpais as pais, iban, cbanco, csucursal, cdc, cuenta, direccionadm, codpoadm, poblacionadm, provinciaadm, dpago1, dpago2, dpago3 FROM potenciales WHERE codigo = $1 LIMIT 1');

pg_prepare($dbconn, 'ofertas', 'SELECT identificador, referencia_obra, condiciones, transporte, texto_factura, forma_pago_alternativa FROM ofertas WHERE identificador = $1');

pg_prepare($dbconn, 'paises', 'SELECT nombre, nombre2, cee FROM paises WHERE codigo = $1');

pg_prepare($dbconn, 'efectos', 'SELECT orden, max(fvto)::DATE AS fvto, sum(importe)::NUMERIC(10,2) AS importe FROM efectos WHERE identif_fra = $1 GROUP BY 1 ORDER BY 2');

pg_prepare($dbconn, 'tipoefec', 'SELECT DISTINCT tipo_efec FROM efectos WHERE identif_fra = $1 AND orden > 0');

pg_prepare($dbconn, 'fopago', 'SELECT nombre, banco, recargo_financiero, descuento_pronto_pago FROM fopago WHERE codigo = $1');

pg_prepare($dbconn, 'bancos', 'SELECT nombre, iban, cbanco, csucursal, cdc, cuenta AS cuenta FROM bancos WHERE codigo = $1');

pg_prepare($dbconn, 'um', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1');

pg_prepare($dbconn, 'textoiva', 'SELECT DISTINCT e.descripcion FROM albaranes a, exenciones e WHERE a.factura = $1 AND a.iva = 0 AND a.tipo_exencion > 0 AND a.tipo_exencion = e.codigo');

//echo 'Serie de la factura: "' . $serie . '"' . PHP_EOL;
switch ($serie) {
    case 'AC':
    case 'AR':
        $result = pg_query_params('SELECT serie, ejercicio, numero, fecha, cliente, forma_pago, portes, total_portes, descuento_pronto_pago, recargo_financiero, base_exenta, base_0, tipo_iva_0, base_1, tipo_iva_1, base_2, tipo_iva_2, total_portes_exceso, iva, identificador_oferta, referencia_obra, referencia_cliente FROM albaranes WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [albaranes]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes] No hay resultado');
        $pdf_Factura = pg_fetch_all($result)[0];

        $pdf_Factura['fecha'] = DateTime::createFromFormat('Y-m-d', $pdf_Factura['fecha'])->format('d/m/Y');

        $pdf_Factura['oferta'] = $stmtOfertas;
        if (!is_null($pdf_Factura['identificador_oferta'])) {
            $result = pg_execute($dbconn, 'ofertas', [$pdf_Factura['identificador_oferta']]) or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
            if (pg_num_rows($result) > 0) {
                $pdf_Factura['oferta'] = pg_fetch_all($result)[0];
            }
        }

        // obtenemos el importe
        $portes = 0;
        if (intval($pdf_Factura['portes']) == 0 || $pdf_Factura['portes'] == 3) $portes = $pdf_Factura['total_portes']; // portes en factura
        if (intval($pdf_Factura['portes']) == 1) $portes = $pdf_Factura['total_portes_exceso']; // portes pagados
        if (is_null($portes)) $portes = 0;

        if ($pdf_Factura['iva'] == 0) $pdf_Factura['base_exenta'] += $portes; // iva no
        if ($pdf_Factura['iva'] == 1) $pdf_Factura['base_0'] += $portes; // iva si

        $pdf_Factura['bruto'] = $pdf_Factura['base_exenta'] + $pdf_Factura['base_0'] + $pdf_Factura['base_1'] + $pdf_Factura['base_2'];

        $dpp_e = $pdf_Factura['base_exenta'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_0 = $pdf_Factura['base_0'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_1 = $pdf_Factura['base_1'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_2 = $pdf_Factura['base_2'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $pdf_Factura['total_descuento_pronto_pago'] = $dpp_e + $dpp_0 + $dpp_1 + $dpp_2;

        $pdf_Factura['base_exenta'] = $pdf_Factura['base_exenta'] - $dpp_e;
        $pdf_Factura['base_0'] = $pdf_Factura['base_0'] - $dpp_0;
        $pdf_Factura['base_1'] = $pdf_Factura['base_1'] - $dpp_1;
        $pdf_Factura['base_2'] = $pdf_Factura['base_2'] - $dpp_2;

        $pdf_Factura['total_iva_0'] = $pdf_Factura['base_0'] * ($pdf_Factura['tipo_iva_0'] / 100);
        $pdf_Factura['total_iva_1'] = $pdf_Factura['base_1'] * ($pdf_Factura['tipo_iva_1'] / 100);
        $pdf_Factura['total_iva_2'] = $pdf_Factura['base_2'] * ($pdf_Factura['tipo_iva_2'] / 100);
        $total_iva = $pdf_Factura['total_iva_0'] + $pdf_Factura['total_iva_1'] + $pdf_Factura['total_iva_2'];

        $base_imp = $pdf_Factura['bruto'] - $pdf_Factura['total_descuento_pronto_pago'];
        $total_factura = $base_imp + $total_iva;
        $total_recargo = $total_factura * ($pdf_Factura['recargo_financiero'] / 100);
        $total_factura += $total_recargo;
        $pdf_Factura['total_factura'] = $total_factura;

        $result = pg_query_params('SELECT identificador_pedido, seccion, descripcion_comercial, precio, unidad_medida, descuento, referencia_plano, posicion, SUM(metros)::NUMERIC(10,2) AS metros, SUM(importe)::NUMERIC(10,2) AS importe FROM albaranes a, albaraneslin al WHERE a.identificador = $1 AND a.identificador = al.identificador GROUP BY identificador_pedido, seccion, producto, espesor, calidad, recubrimiento, pintura, ccolor, unidad_medida, posicion, descripcion_comercial, precio, descuento, referencia_plano ORDER BY identificador_pedido, seccion, importe', array($identificador)) or incoLogWrite('La consulta fallo [albaraneslin]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[albaraneslin] No hay resultado');
        $pdf_FacturaLns = array([
            'identificador' => $identificador,
            'numero' => $pdf_Factura['numero'],
            'referencia_cliente' => $pdf_Factura['referencia_cliente'],
            'identificador_oferta' => $pdf_Factura['identificador_oferta'],
            'portes' => $pdf_Factura['portes'],
            'numero_portes' => 1,
            'total_portes' => $portes,
            'referencia_obra' => $pdf_Factura['referencia_obra'],
            'lineas' => pg_fetch_all($result)
        ]);

        foreach ($pdf_FacturaLns[0]['lineas'] as $key => $linea) {
            $descripcion = trim($pdf_FacturaLns[0]['lineas'][$key]['descripcion_comercial']);
            switch ($pdf_FacturaLns[0]['lineas'][$key]['posicion']) {
                case 1: $descripcion .= ' POSICIÓN CUBIERTA'; break;
                case 2: $descripcion .= ' POSICIÓN FACHADA'; break;
            }
            $pdf_FacturaLns[0]['lineas'][$key]['descripcion_comercial'] = $descripcion;

            $result = pg_execute($dbconn, 'um', array($linea['unidad_medida']));
            $um = pg_num_rows($result) <= 0 ? '' : trim(pg_fetch_all($result)[0]['abreviatura']);
            $um = str_replace('2', html_entity_decode('&sup2;'), $um);
            $pdf_FacturaLns[0]['lineas'][$key]['unidad_medida'] = $um;
        }

        $recargos = [];
        $result = pg_query_params($dbconn, 'select pr.identificador, pr.linea, pr.descripcion, pr.importe from pedidos_recargos pr, pedidos p where pr.identificador in (select distinct al.identificador_pedido from albaraneslin al where al.identificador = $1) and p.aplicar_recargos = 1 and pr.identificador = p.identificador order by pr.linea', [$identificador]);
        if (pg_num_rows($result) > 0) {
            $recargos = pg_fetch_all($result);
        }
        foreach ($recargos as $recargo) {
            $pdf_FacturaLns[0]['lineas'][] = [
                'identificador' => $identificador,
                'seccion' => 0,
                'descripcion_comercial' => $recargo['descripcion'],
                'precio' => $recargo['importe'],
                'unidad_medida' => 'UD',
                'descuento' => '',
                'referencia_plano' => '',
                'posicion' => '',
                'metros' => 1,
                'importe' => $recargo['importe'],
            ];
        }
        break;
    case 'FA':
    case 'FC':
    case 'FR':
        $result = pg_query_params('SELECT ejercicio, serie, numero, cliente, fecha, forma_pago, bruto, descuento_pronto_pago, total_descuento_pronto_pago, recargo_financiero, total_recargo_financiero, base_exenta, base_0, tipo_iva_0, total_iva_0, base_1, tipo_iva_1, total_iva_1, base_2, tipo_iva_2, total_iva_2, total_factura, tipo_portes, portes, anticipo, anticipo_con_factura, factura_anticipado, iva, observaciones FROM facturas WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [factura ' . $identificador . ']: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[factura ' . $identificador . '] No hay resultado');
        $pdf_Factura = pg_fetch_all($result)[0];

        $pdf_Factura['fecha'] = DateTime::createFromFormat('Y-m-d', $pdf_Factura['fecha'])->format('d/m/Y');

        $pdf_Factura['oferta'] = $stmtOfertas;
        $result = pg_query_params('SELECT DISTINCT identificador_oferta FROM albaranes WHERE factura = $1', array($identificador));
        $oferta = pg_num_rows($result) > 0 ? trim(pg_fetch_all($result)[0]['identificador_oferta']) : null;
        if (!is_null($oferta)) {
            $result = pg_execute($dbconn, 'ofertas', [$oferta]) or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
            if (pg_num_rows($result) > 0) {
                $pdf_Factura['oferta'] = pg_fetch_all($result)[0];
            }
        }

        $result = pg_query_params($dbconn, 'SELECT identificador, numero, referencia_cliente, identificador_oferta, referencia_obra, portes, sum(total_portes + total_portes_exceso)::numeric(10, 2) AS total_portes FROM albaranes WHERE factura = $1 GROUP BY identificador ORDER BY identificador', array($identificador)) or incoLogWrite('La consulta fallo [albaranes]: ' . pg_last_error());
        if ($serie == 'FA') {
            $pdf_FacturaLns = array();
        } else {
            if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes] No hay resultado');
            $pdf_FacturaLns = pg_fetch_all($result);
        }

        if ($serie == 'FR') {
            $result = pg_query_params('SELECT DISTINCT factura_rectificada, tipo_rectificativa FROM albaranes WHERE factura = $1', array($identificador));
            $pdf_Factura['FR'] = pg_num_rows($result) <= 0 ? array('factura_rectificada' => '', 'tipo_rectificativa' => '') : pg_fetch_all($result)[0];
        }

        foreach ($pdf_FacturaLns as $key => $albaran) {
            $result = pg_query_params($dbconn, 'SELECT identificador_pedido, seccion, descripcion_comercial, precio, unidad_medida, descuento, referencia_plano, posicion, SUM(cantidad)::numeric(10,2) AS cantidad, SUM(metros)::NUMERIC(10,2) AS metros, SUM(importe)::NUMERIC(10,2) AS importe, string_agg(observaciones, \', \')::character(255) AS observaciones FROM albaraneslin WHERE identificador = $1 GROUP BY identificador_pedido, seccion, producto, espesor, calidad, recubrimiento, pintura, ccolor, unidad_medida, posicion, descripcion_comercial, precio, descuento, referencia_plano ORDER BY identificador_pedido, seccion, importe', array($albaran['identificador']));
            $pdf_FacturaLns[$key]['lineas'] = pg_fetch_all($result);
            if (pg_num_rows($result) <= 0) {
                continue;
            }

            foreach ($pdf_FacturaLns[$key]['lineas'] as $keyLn => $linea) {
                if ($pdf_FacturaLns[$key]['lineas'][$keyLn]['unidad_medida'] == 4) {
                    $pdf_FacturaLns[$key]['lineas'][$keyLn]['metros'] = $pdf_FacturaLns[$key]['lineas'][$keyLn]['cantidad'];
                }

                $result = pg_execute($dbconn, 'um', array($linea['unidad_medida']));
                $um = pg_num_rows($result) <= 0 ? '' : trim(pg_fetch_all($result)[0]['abreviatura']);
                $um = str_replace('2', html_entity_decode('&sup2;'), $um);
                $pdf_FacturaLns[$key]['lineas'][$keyLn]['unidad_medida'] = $um;

                $descripcion = trim($pdf_FacturaLns[$key]['lineas'][$keyLn]['descripcion_comercial']);
                switch ($pdf_FacturaLns[$key]['lineas'][$keyLn]['posicion']) {
                    case 1: $descripcion .= ' POSICIÓN CUBIERTA'; break;
                    case 2: $descripcion .= ' POSICIÓN FACHADA'; break;
                }
                $pdf_FacturaLns[$key]['lineas'][$keyLn]['descripcion_comercial'] = $descripcion;
            }

            $result = pg_query_params($dbconn, 'select pr.identificador, pr.linea, pr.descripcion, pr.importe from pedidos_recargos pr, pedidos p where pr.identificador in (select distinct al.identificador_pedido from albaraneslin al where al.identificador = $1) and p.aplicar_recargos = 1 and pr.identificador = p.identificador order by pr.linea', [$identificador]);
            if (pg_num_rows($result) <= 0) {
                continue;
            }
            $recargos = pg_fetch_all($result);
            foreach ($recargos as $recargo) {
                $pdf_FacturaLns[$key]['lineas'][] = [
                    'identificador' => $identificador,
                    'seccion' => 0,
                    'descripcion_comercial' => $recargo['descripcion'],
                    'precio' => $recargo['importe'],
                    'unidad_medida' => 'UD',
                    'descuento' => '',
                    'referencia_plano' => '',
                    'posicion' => '',
                    'metros' => 1,
                    'importe' => $recargo['importe'],
                ];
            }
        }
        break;
    case 'PC':
        $result = pg_query_params('SELECT centro, ejercicio, serie, numero, fecha, cliente, forma_pago, iva, portes, portes_importe as total_portes, identificador_oferta, portes_numero, referencia_cliente, referencia_obra, aplicar_recargos FROM pedidos WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
        $pdf_Factura = pg_fetch_all($result)[0];
        $aplicar_recargos = intval($pdf_Factura['aplicar_recargos']) == 1;

        $pdf_Factura['centro'] = intval($pdf_Factura['centro']);
        $pdf_Factura['fecha'] = DateTime::createFromFormat('Y-m-d', $pdf_Factura['fecha'])->format('d/m/Y');

        $pdf_Factura['oferta'] = $stmtOfertas;
        if (!is_null($pdf_Factura['identificador_oferta'])) {
            $result = pg_execute($dbconn, 'ofertas', [$pdf_Factura['identificador_oferta']]) or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
            if (pg_num_rows($result) > 0) {
                $pdf_Factura['oferta'] = pg_fetch_all($result)[0];
            }
        }

        $result = pg_query_params('SELECT iva0, iva1, iva2 FROM centrofac WHERE codigo = $1', array($pdf_Factura['centro'])) or incoLogWrite('La consulta fallo [centrofac]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[centrofac] No hay resultado');
        $centrofac = pg_fetch_all($result)[0];

        if (boolval($pdf_Factura['iva'])) {
            $pdf_Factura['tipo_iva_0'] = floatval($centrofac['iva0']);
            $pdf_Factura['tipo_iva_1'] = floatval($centrofac['iva1']);
            $pdf_Factura['tipo_iva_2'] = floatval($centrofac['iva2']);
        } else {
            $pdf_Factura['tipo_iva_0'] = 0.0;
            $pdf_Factura['tipo_iva_1'] = 0.0;
            $pdf_Factura['tipo_iva_2'] = 0.0;
        }

        $result = pg_execute($dbconn, 'fopago', array($pdf_Factura['forma_pago']));
        if (pg_num_rows($result) <= 0) {
            $pdf_Factura['descuento_pronto_pago'] = 0.0;
            $pdf_Factura['recargo_financiero'] = 0.0;
        } else {
            $fopago = pg_fetch_all($result)[0];
            $pdf_Factura['descuento_pronto_pago'] = floatval($fopago['descuento_pronto_pago']);
            $pdf_Factura['recargo_financiero'] = floatval($fopago['recargo_financiero']);
        }
        if ($pdf_Factura['portes'] == 1 && $pdf_Factura['total_portes'] != 0) $pdf_Factura['total_portes'] = 0;

        //  * (1.00 - (pl.descuento / 100))::numeric(10, 2)
        $result = pg_query_params('select pl.identificador as identificador_pedido, pl.seccion, pl.producto, pl.descripcion_comercial, pl.precio, pl.unidad_medida, pl.descuento, pl.referencia_plano, sum(vw.cantidad_total) as metros, sum(vw.importe) as importe FROM pedidoslin pl, vw_pedidoslin_proforma vw WHERE pl.identificador = $1 and pl.identificador = vw.identificador and pl.linea = vw.linea GROUP BY pl.identificador, pl.seccion, producto, pl.espesor, pl.calidad, pl.recubrimiento, pl.pintura, pl.ccolor, pl.unidad_medida, descripcion_comercial, pl.precio, descuento, referencia_plano ORDER BY pl.identificador, seccion', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
        $pedidolin = pg_fetch_all($result);
        $pdf_FacturaLns = array([
            'identificador' => $identificador,
            'numero' => $pdf_Factura['numero'],
            'referencia_cliente' => $pdf_Factura['referencia_cliente'],
            'identificador_oferta' => $pdf_Factura['identificador_oferta'],
            'portes' => $pdf_Factura['portes'],
            'numero_portes' => $pdf_Factura['portes_numero'],
            'total_portes' => $pdf_Factura['total_portes'],
            'referencia_obra' => $pdf_Factura['referencia_obra'],
            'lineas' => pg_fetch_all($result)
        ]);

        $pdf_Factura['base_exenta'] = 0;
        $pdf_Factura['base_0'] = 0;
        $pdf_Factura['base_1'] = 0;
        $pdf_Factura['base_2'] = 0;

        if ($pdf_Factura['iva'] == 1) {
            foreach ($pdf_FacturaLns[0]['lineas'] as $key => $linea) {
                $result = pg_query_params($dbconn, 'SELECT tipo_iva FROM productos WHERE codigo = $1 LIMIT 1', array($linea['producto']));
                if (pg_num_rows($result) <= 0) incoLogWrite('[productos - iva] No hay resultado');
                $tipoiva = intval(pg_fetch_all($result)[0]['tipo_iva']);
                switch ($tipoiva) {
                    case 0:
                        $pdf_Factura['base_0'] += $linea['importe'];
                        break;
                    case 1:
                        $pdf_Factura['base_1'] += $linea['importe'];
                        break;
                    case 2:
                        $pdf_Factura['base_2'] += $linea['importe'];
                        break;
                }

                $result = pg_execute($dbconn, 'um', array($linea['unidad_medida']));
                $um = pg_num_rows($result) <= 0 ? '' : trim(pg_fetch_all($result)[0]['abreviatura']);
                $um = str_replace('2', html_entity_decode('&sup2;'), $um);
                $pdf_FacturaLns[0]['lineas'][$key]['unidad_medida'] = $um;
            }
            if (intval($pdf_Factura['portes']) == 0 || intval($pdf_Factura['portes']) == 3) $pdf_Factura['base_0'] += (intval($pdf_Factura['portes_numero']) * floatval($pdf_Factura['total_portes'])); // portes en factura
        } else {
            foreach ($pdf_FacturaLns[0]['lineas'] as $key => $linea) {
                $pdf_Factura['base_exenta'] += $linea['importe'];

                $result = pg_execute($dbconn, 'um', array($linea['unidad_medida']));
                $um = pg_num_rows($result) <= 0 ? '' : trim(pg_fetch_all($result)[0]['abreviatura']);
                $um = str_replace('2', html_entity_decode('&sup2;'), $um);
                $pdf_FacturaLns[0]['lineas'][$key]['unidad_medida'] = $um;
            }
            if (intval($pdf_Factura['portes']) == 0 || intval($pdf_Factura['portes']) == 3) $pdf_Factura['base_exenta'] += (intval($pdf_Factura['portes_numero']) * floatval($pdf_Factura['total_portes'])); // portes en factura
        }

        $pdf_Factura['bruto'] = $pdf_Factura['base_exenta'] + $pdf_Factura['base_0'] + $pdf_Factura['base_1'] + $pdf_Factura['base_2'];

        $dpp_e = $pdf_Factura['base_exenta'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_0 = $pdf_Factura['base_0'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_1 = $pdf_Factura['base_1'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_2 = $pdf_Factura['base_2'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $pdf_Factura['total_descuento_pronto_pago'] = $dpp_e + $dpp_0 + $dpp_1 + $dpp_2;

        $pdf_Factura['base_exenta'] = $pdf_Factura['base_exenta'] - $dpp_e;
        $pdf_Factura['base_0'] = $pdf_Factura['base_0'] - $dpp_0;
        $pdf_Factura['base_1'] = $pdf_Factura['base_1'] - $dpp_1;
        $pdf_Factura['base_2'] = $pdf_Factura['base_2'] - $dpp_2;

        $pdf_Factura['total_iva_0'] = $pdf_Factura['base_0'] * ($pdf_Factura['tipo_iva_0'] / 100);
        $pdf_Factura['total_iva_1'] = $pdf_Factura['base_1'] * ($pdf_Factura['tipo_iva_1'] / 100);
        $pdf_Factura['total_iva_2'] = $pdf_Factura['base_2'] * ($pdf_Factura['tipo_iva_2'] / 100);
        $total_iva = $pdf_Factura['total_iva_0'] + $pdf_Factura['total_iva_1'] + $pdf_Factura['total_iva_2'];

        $base_imp = $pdf_Factura['bruto'] - $pdf_Factura['total_descuento_pronto_pago'];
        $total_factura = $base_imp + $total_iva;
        $total_recargo = $total_factura * ($pdf_Factura['recargo_financiero'] / 100);
        $total_factura += $total_recargo;
        $pdf_Factura['total_factura'] = $total_factura;

        $recargos = [];
        if ($aplicar_recargos) {
            $result = pg_query_params($dbconn, 'select pr.identificador, pr.linea, pr.descripcion, pr.importe from pedidos_recargos pr where pr.identificador = $1 order by pr.linea', [$identificador]);
            if (pg_num_rows($result) > 0) {
                $recargos = pg_fetch_all($result);
            }
        }
        foreach ($recargos as $recargo) {
            $pdf_FacturaLns[0]['lineas'][] = [
                'identificador' => $identificador,
                'seccion' => 0,
                'descripcion_comercial' => $recargo['descripcion'],
                'precio' => $recargo['importe'],
                'unidad_medida' => 'UD',
                'descuento' => '',
                'referencia_plano' => '',
                'posicion' => '',
                'metros' => 1,
                'importe' => $recargo['importe'],
            ];
        }
        break;
    case 'OC':
        $result = pg_query_params('SELECT identificador, centro, ejercicio, serie, numero, revision, referencia_obra, fecha, cliente, potencial, iva, forma_pago, forma_pago_alternativa, portes, portes_importe as total_portes, transporte, condiciones, texto_factura, portes_numero FROM ofertas WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[oferta] No hay resultado');
        $pdf_Factura = pg_fetch_all($result)[0];

        $pdf_Factura['oferta'] = pg_fetch_all($result)[0];
        $pdf_Factura['centro'] = intval($pdf_Factura['centro']);
        $pdf_Factura['fecha'] = DateTime::createFromFormat('Y-m-d', $pdf_Factura['fecha'])->format('d/m/Y');
        if (filter_var($pdf_Factura['potencial'], FILTER_VALIDATE_BOOLEAN)) $stmtClientes = 'potenciales';

        $result = pg_query_params('SELECT iva0, iva1, iva2 FROM centrofac WHERE codigo = $1', array($pdf_Factura['centro'])) or incoLogWrite('La consulta fallo [centrofac]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[centrofac] No hay resultado');
        $centrofac = pg_fetch_all($result)[0];

        if ($pdf_Factura['iva'] == 0) {
            $pdf_Factura['tipo_iva_0'] = 0.0;
            $pdf_Factura['tipo_iva_1'] = 0.0;
            $pdf_Factura['tipo_iva_2'] = 0.0;
        } else {
            $pdf_Factura['tipo_iva_0'] = floatval($centrofac['iva0']);
            $pdf_Factura['tipo_iva_1'] = floatval($centrofac['iva1']);
            $pdf_Factura['tipo_iva_2'] = floatval($centrofac['iva2']);
        }

        $pdf_Factura['descuento_pronto_pago'] = 0.0;
        $pdf_Factura['recargo_financiero'] = 0.0;
        $formaPagoAlt = trim($pdf_Factura['forma_pago_alternativa']);
        if (!empty($formaPagoAlt)) {
            $pdf_Factura['forma_pago'] = array(
                'nombre' => str_replace('\\n', PHP_EOL, $formaPagoAlt),
                'banco' => null
            );
        }

        $result = pg_query_params('SELECT null as identificador_pedido, seccion, producto, descripcion_comercial, cantidad AS metros, unidad_medida, precio, (cantidad * precio)::NUMERIC(10,2) AS importe FROM ofertaslin WHERE identificador = $1 ORDER BY seccion', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
        $pdf_FacturaLns = array([
            'identificador' => $identificador,
            'numero' => $pdf_Factura['numero'],
            'referencia_cliente' => null,
            'identificador_oferta' => $identificador,
            'portes' => $pdf_Factura['portes'],
            'numero_portes' => $pdf_Factura['portes_numero'],
            'total_portes' => $pdf_Factura['total_portes'],
            'referencia_obra' => $pdf_Factura['referencia_obra'],
            'lineas' => pg_fetch_all($result)
        ]);

        $pdf_Factura['base_exenta'] = 0;
        $pdf_Factura['base_0'] = 0;
        $pdf_Factura['base_1'] = 0;
        $pdf_Factura['base_2'] = 0;

        if ($pdf_Factura['iva'] == 1) {
            foreach ($pdf_FacturaLns[0]['lineas'] as $key => $linea) {
                $result = pg_query_params($dbconn, 'SELECT tipo_iva FROM productos WHERE codigo = $1 LIMIT 1', array($linea['producto']));
                if (pg_num_rows($result) <= 0) incoLogWrite('[productos - iva] No hay resultado');
                $tipoiva = intval(pg_fetch_all($result)[0]['tipo_iva']);
                switch ($tipoiva) {
                    case 0:
                        $pdf_Factura['base_0'] += $linea['importe'];
                        break;
                    case 1:
                        $pdf_Factura['base_1'] += $linea['importe'];
                        break;
                    case 2:
                        $pdf_Factura['base_2'] += $linea['importe'];
                        break;
                }

                $result = pg_execute($dbconn, 'um', array($linea['unidad_medida']));
                $um = pg_num_rows($result) <= 0 ? '' : trim(pg_fetch_all($result)[0]['abreviatura']);
                $um = str_replace('2', html_entity_decode('&sup2;'), $um);
                $pdf_FacturaLns[0]['lineas'][$key]['unidad_medida'] = $um;
            }
            if (intval($pdf_Factura['portes']) == 0 || intval($pdf_Factura['portes']) == 3) $pdf_Factura['base_0'] += (intval($pdf_Factura['portes_numero']) * floatval($pdf_Factura['total_portes'])); // portes en factura
        } else {
            foreach ($pdf_FacturaLns[0]['lineas'] as $key => $linea) {
                $pdf_Factura['base_exenta'] += $linea['importe'];

                $result = pg_execute($dbconn, 'um', array($linea['unidad_medida']));
                $um = pg_num_rows($result) <= 0 ? '' : trim(pg_fetch_all($result)[0]['abreviatura']);
                $um = str_replace('2', html_entity_decode('&sup2;'), $um);
                $pdf_FacturaLns[0]['lineas'][$key]['unidad_medida'] = $um;
            }
            if (intval($pdf_Factura['portes']) == 0 || intval($pdf_Factura['portes']) == 3) $pdf_Factura['base_exenta'] += (intval($pdf_Factura['portes_numero']) * floatval($pdf_Factura['total_portes'])); // portes en factura
        }

        $pdf_Factura['bruto'] = $pdf_Factura['base_exenta'] + $pdf_Factura['base_0'] + $pdf_Factura['base_1'] + $pdf_Factura['base_2'];

        $dpp_e = $pdf_Factura['base_exenta'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_0 = $pdf_Factura['base_0'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_1 = $pdf_Factura['base_1'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $dpp_2 = $pdf_Factura['base_2'] * ($pdf_Factura['descuento_pronto_pago'] / 100);
        $pdf_Factura['total_descuento_pronto_pago'] = $dpp_e + $dpp_0 + $dpp_1 + $dpp_2;

        $pdf_Factura['base_exenta'] = $pdf_Factura['base_exenta'] - $dpp_e;
        $pdf_Factura['base_0'] = $pdf_Factura['base_0'] - $dpp_0;
        $pdf_Factura['base_1'] = $pdf_Factura['base_1'] - $dpp_1;
        $pdf_Factura['base_2'] = $pdf_Factura['base_2'] - $dpp_2;

        $pdf_Factura['total_iva_0'] = $pdf_Factura['base_0'] * ($pdf_Factura['tipo_iva_0'] / 100);
        $pdf_Factura['total_iva_1'] = $pdf_Factura['base_1'] * ($pdf_Factura['tipo_iva_1'] / 100);
        $pdf_Factura['total_iva_2'] = $pdf_Factura['base_2'] * ($pdf_Factura['tipo_iva_2'] / 100);
        $total_iva = $pdf_Factura['total_iva_0'] + $pdf_Factura['total_iva_1'] + $pdf_Factura['total_iva_2'];

        $base_imp = $pdf_Factura['bruto'] - $pdf_Factura['total_descuento_pronto_pago'];
        $total_factura = $base_imp + $total_iva;
        $total_recargo = $total_factura * ($pdf_Factura['recargo_financiero'] / 100);
        $total_factura += $total_recargo;
        $pdf_Factura['total_factura'] = $total_factura;
        break;
    default:
        incoLogWrite('Serie no contemplada');
        break;
}

$result = pg_execute($dbconn, $stmtClientes, array($pdf_Factura['cliente']));
if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
$pdf_Cliente = pg_fetch_all($result)[0];

$iban = str_pad(trim($pdf_Cliente['iban']), 4, '0', STR_PAD_LEFT);
$banco = str_pad(trim($pdf_Cliente['cbanco']), 4, '0', STR_PAD_LEFT);
$sucursal = str_pad(trim($pdf_Cliente['csucursal']), 4, '0', STR_PAD_LEFT);
$dc = str_pad(trim($pdf_Cliente['cdc']), 2, '0', STR_PAD_LEFT);
$cuenta = str_pad(trim($pdf_Cliente['cuenta']), 10, '0', STR_PAD_LEFT);
$pdf_Cliente['cuenta'] = substr($iban . $banco . $sucursal . $dc . $cuenta, 0, 20) . '****';

$result = pg_execute($dbconn, 'paises', array($pdf_Cliente['pais']));
if (pg_num_rows($result) <= 0) incoLogWrite('[paises] No hay resultado');
$pdf_Cliente['pais'] = pg_fetch_all($result)[0];

if (empty($idioma)) $idioma = trim($pdf_Cliente['idioma']);
if ($idioma != 'es') {
    $pdf_Cliente['pais']['nombre'] = trim($pdf_Cliente['pais']['nombre2']);
}

$pdf_Factura['isProforma'] = !in_array($serie, ['FA', 'FC', 'FR']);

if (!isset($pdf_Factura['forma_pago']['nombre'])) {
    $formaPagoAlt = trim($pdf_Factura['oferta']['forma_pago_alternativa']);
    if (empty($formaPagoAlt)) {
        $result = pg_execute($dbconn, 'fopago', array($pdf_Factura['forma_pago']));
        if (pg_num_rows($result) <= 0) incoLogWrite('[forma_pago] No hay resultado');
        $pdf_Factura['forma_pago'] = pg_fetch_all($result)[0];
        $pdf_Factura['forma_pago']['nombre'] = trim($pdf_Factura['forma_pago']['nombre']);
        if (!$pdf_Factura['isProforma']) $pdf_Factura['forma_pago']['nombre'] .= ' - DÍAS FIJOS DE PAGO';

        if ($pdf_Factura['forma_pago']['banco'] > 0) {
            $result = pg_execute($dbconn, 'bancos', array($pdf_Factura['forma_pago']['banco']));
            $pdf_Factura['forma_pago']['banco'] = $result === false ? null : pg_fetch_all($result)[0];
            if (!is_null($pdf_Factura['forma_pago']['banco'])) {
                $iban = sprintf('%04s%04d%04d%02d%010d',
                    $pdf_Factura['forma_pago']['banco']['iban'], $pdf_Factura['forma_pago']['banco']['cbanco'],
                    $pdf_Factura['forma_pago']['banco']['csucursal'], $pdf_Factura['forma_pago']['banco']['cdc'],
                    $pdf_Factura['forma_pago']['banco']['cuenta']
                );
                $pdf_Factura['forma_pago']['banco']['cuenta'] = $iban; // implode('-', str_split($iban, 4));
            }
        } else {
            $pdf_Factura['forma_pago']['banco'] = null;
        }
    } else {
        $pdf_Factura['forma_pago'] = [
            'nombre' => str_replace('\\n', PHP_EOL, $formaPagoAlt),
            'banco' => null
        ];
    }
}

$result = pg_execute($dbconn, 'efectos', array($identificador));
$efectos = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
$pdf_Factura['efectos'] = $efectos;

$result = pg_execute($dbconn, 'tipoefec', array($identificador));
$tipoEfecto = pg_num_rows($result) > 0 ? pg_fetch_all($result)[0]['tipo_efec'] : '';
$pdf_Factura['tipoefec'] = trim($tipoEfecto);

$pdf_Factura['texto_iva'] = '';
if (!$pdf_Factura['isProforma'] && $serie != 'FA' && ($pdf_Cliente['pais']['cee'] == 'N' || ($pdf_Cliente['pais']['cee'] == 'S' && $pdf_Factura['iva'] == 0))) {
    $result = pg_execute($dbconn, 'textoiva', array($identificador));
    $exenciones = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
    $pdf_Factura['texto_iva'] = implode(PHP_EOL, array_map(function ($exencion) {
        return trim($exencion);
    }, array_column($exenciones, 'descripcion')));
}

/************** VARIABLES PDF FIJAS **************/
if (empty($idioma)) $idioma = 'es';

$pdf_Logos = ['folder' => INCO_DIR_IMAGENES, 'print' => $printLogos];
$pdf_Tr = new IncoTranslatorFix($dbconn, 'factura', $idioma);
incoI18nSetLang($idioma, 'factura');

/************** SE GENERA EL PDF **************/
require __DIR__ . '/FacturaPdf.php';

$pdf = new FacturaPdf();

$pdf->setTr($pdf_Tr);
$pdf->setLogos($pdf_Logos);
$pdf->setFactura($pdf_Factura);
$pdf->setFacturaLns($pdf_FacturaLns);
$pdf->setCliente($pdf_Cliente);

$pdf->AddContent();

/**
 * 1 --> tipo de factura: F, FP_P, FP_O, FP_A
 * 2 --> ejercicio
 * 3 --> numero (6 digitos), con posibilidad de añadir idioma (2 caracteres)
 */
$patternFileName = '%s_%s_%s.pdf';
$folder = $stmtClientes == 'clientes' ? INCO_DIR_CLIENTES : INCO_DIR_POTENCIALES;

if ($pdf_Factura['isProforma']) {
    if (file_exists(INCO_DIR_DOCUMENTOS . 'CGV.pdf')) {
        $pdf->setPrintHeader(false);
        try {
            $pageCount = $pdf->setSourceFile(INCO_DIR_DOCUMENTOS . 'CGV.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {
                $tplIdx = $pdf->importPage($i);
                $pdf->AddPage();
                $pdf->setPrintFooter(false);
                $pdf->useTemplate($tplIdx);
            }
        } catch (Exception $exception) {
            incoLogWrite($exception->getMessage());
        }
    }

    $dir = $folder . $pdf_Cliente['codigo'] . '/FacturasProforma/';
    $fileNameTipoFactura = 'FP_' . substr($serie, 0, 1);
    $fileNameIdioma = '';
    if ($serie == 'OC') $fileNameIdioma = '_' . $pdf_Factura['revision'];
} else {
    if ($pdf_Logos['print']) {
        $dir = $folder . $pdf_Cliente['codigo'] . '/Facturas/';
    } else {
        $dir = $folder . $pdf_Cliente['codigo'] . '/FacturasSinLogos/';
    }
    $fileNameTipoFactura = 'F';
    $fileNameIdioma = '_' . $idioma;
}
$fileNameEjercicio = $pdf_Factura['ejercicio'];
$fileNameNumero = str_pad($pdf_Factura['numero'], 6, '0', STR_PAD_LEFT) . $fileNameIdioma;
$fileName = sprintf($patternFileName, $fileNameTipoFactura, $fileNameEjercicio, $fileNameNumero);

if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}

//$dir = $FOLDER_LOG;
$output = $dir . $fileName;

$pdf->Output($output, 'F');
//echo 'Factura ' . $serie . '/' . $pdf_Factura['numero'] . ' generada' . PHP_EOL;

// copiar $output en carpeta contabilidad con nombre E[año:1][serie:1][numero:7])
if (!INCO_DEV_MODE && !$pdf_Factura['isProforma'] && $pdf_Logos['print']) {
    if (!is_dir(INCO_DIR_FACTURAS_EMITIDAS)) {
        $oldmask = umask(0);
        mkdir(INCO_DIR_FACTURAS_EMITIDAS, 0755, true);
        umask($oldmask);
    }

    $serieConta = (trim($pdf_Factura['serie']) == 'FR') ? 'R' : 0;
    $fileNameCont = INCO_DIR_FACTURAS_EMITIDAS . 'E' . substr($pdf_Factura['ejercicio'], -1, 1) . $serieConta . str_pad($pdf_Factura['numero'], 7, '0', STR_PAD_LEFT) . '.pdf';

    copy($output, $fileNameCont);
}

pg_close($dbconn);