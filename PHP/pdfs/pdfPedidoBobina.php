<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$firmar = boolval($argv[3]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el pedido
$result = pg_query_params('SELECT ejercicio, numero, fecha, proveedor, plazo, tipo_embalaje, obs_prove as observaciones, obs_prove_2 FROM pedidos_bobinas WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinas] No hay resultado');
$pedido = pg_fetch_all($result)[0];

// obtenemos las lineas del pedido
// $result = pg_query_params('SELECT pbl.linea, pbl.semana_prevista, pbl.descargar, e.valor AS espesor, c.abreviatura AS calidad, r.abreviatura AS recubrimiento, p.nombre AS pintura, co.nombre AS color, pb.nombre AS pinturab, cob.nombre AS colorb, pbl.ancho, pbl.toneladas, pbl.precio, pbl.certificado FROM pedidos_bobinaslin pbl, espesores e, calidades c, recubrimientos r, pinturas p, colores co, pinturas pb, colores cob WHERE pbl.identificador = $1 AND pbl.espesor = e.codigo AND pbl.calidad = c.codigo AND pbl.recubrimiento = r.codigo AND pbl.pintura = p.codigo AND pbl.ccolor = co.codigo AND pbl.pinturab = pb.codigo AND pbl.ccolorb = cob.codigo ORDER BY pbl.linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinaslin]: ' . pg_last_error());
$result = pg_query_params('SELECT pbl.semana_prevista, pbl.descargar, e.valor AS espesor, c.abreviatura AS calidad, r.abreviatura AS recubrimiento, p.nombre AS pintura, co.nombre AS color, pb.nombre AS pinturab, cob.nombre AS colorb, pbl.ancho, sum(pbl.toneladas) as toneladas, pbl.precio, pbl.certificado FROM pedidos_bobinaslin pbl, espesores e, calidades c, recubrimientos r, pinturas p, colores co, pinturas pb, colores cob WHERE pbl.identificador = $1 AND pbl.espesor = e.codigo AND pbl.calidad = c.codigo AND pbl.recubrimiento = r.codigo AND pbl.pintura = p.codigo AND pbl.ccolor = co.codigo AND pbl.pinturab = pb.codigo AND pbl.ccolorb = cob.codigo GROUP BY pbl.semana_prevista, pbl.descargar, e.valor, c.abreviatura, r.abreviatura, p.nombre, co.nombre, pb.nombre, cob.nombre, pbl.ancho, pbl.precio, pbl.certificado ORDER BY toneladas', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinaslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinaslin] No hay resultado');
$pedidolin = pg_fetch_all($result);

// obtenemos el plazo
$result = pg_query_params('SELECT min(semana_prevista) AS sp FROM pedidos_bobinaslin WHERE identificador LIKE $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinaslin - sp]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinaslin - sp] No hay resultado');
$pedido['plazo'] = 'SEMANA ' . pg_fetch_all($result)[0]['sp'];

// obtenemos almacen descarga
$result = pg_query_params('SELECT min(descargar) AS desca FROM pedidos_bobinaslin WHERE identificador LIKE $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinaslin - descargar]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinaslin - descargar] No hay resultado');
$almacenes = pg_fetch_all($result)[0]['desca'];
$descargar = '';
if ($almacenes == 'A') {
    $descargar = 'NAVE A (C/ NOU, 16)';
} else {
    $descargar = 'NAVE B (C/ NOU, 27)';
}

$result = pg_query_params('SELECT DISTINCT semana_prevista FROM pedidos_bobinaslin WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinaslin - sp]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinaslin - sp] No hay resultado');
$semanas = pg_fetch_all($result);

// obtenemos las toneladas totales que vamos a pedir
$result = pg_query_params('SELECT SUM(toneladas) AS total_toneladas FROM pedidos_bobinaslin WHERE identificador LIKE $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_bobinaslin - sum]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinaslin - sum] No hay resultado');
$totalToneladas = pg_fetch_all($result)[0]['total_toneladas'];

// obtenemos el proveedor
$result = pg_query_params('SELECT codigo, nombre, direccion, codpo, poblacion, provincia, fopago FROM proveedores WHERE codigo = $1', array($pedido['proveedor'])) or incoLogWrite('La consulta fallo [proveedores]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[proveedores] No hay resultado');
$proveedor = pg_fetch_all($result)[0];

$result = pg_query_params('SELECT nombre FROM fopago WHERE codigo = $1', array($proveedor['fopago'])) or incoLogWrite('La consulta fallo [fopago]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[fopago] No hay resultado');
$fopago = pg_fetch_all($result)[0]['nombre'];

class PDF extends TCPDF
{
    function Header()
    {
        /*$this->SetY(14);
        $this->Cell(0, 20, '', 1, 1); // Se cambiara por una cabecera
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 5, 'PEDIDOS A PROVEEDORES', 0, 1, 'C');
        $this->Ln(2);*/

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('0', '0', '4', '0');

        $txt = 'Pedido de bobina';
        $this->SetFont('dejavusans', 'B', 22);
        $this->SetY(10);
        $this->Cell(0, 4, $txt, 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Ln(); // hace salto demasiado grande
        // $this->Cell(0, 4, '', 0, 1);
        $this->Cell(0, 4, 'Hoja: ' . $this->PageNo(), 0, 1, 'R');

        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
    }

    function Footer()
    {
        global $firmar;

        $this->SetY(-35);

        if ($firmar) {
            $this->Image(INCO_DIR_IMAGENES . 'valpedidobobina.jpg', 158, $this->GetY(), 0, 28, 'jpg', '', 'C', false, 600);
        }

        $this->SetLineWidth(0.51); //1px = 0.085mm
        $this->Line($this->GetX(), $this->GetY(), 197, $this->GetY());
        $this->Ln(2);
        $this->SetFont('dejavusans', 'B', 8);
        $this->setCellPaddings('0', '0', '4', '0');
        $this->Cell(130, 4, 'Revisado por:', 0, 0, 'R');
        $this->setCellPaddings('0', '0', '0', '0');
        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->Cell(0, 20, '', 1, 1);

        $this->SetFont('dejavusans', '');
        $this->SetY($this->GetY() - 4);
        $this->Cell(0, 4, 'Página ' . $this->PageNo(), 0, 1, 'C');

        $this->SetFont('dejavusans', '', 5);
        $this->SetY($this->GetY() - 9);
        $this->Cell(0, 3, 'FCO-01-01', 0, 1, 'L');
        $this->Cell(0, 3, 'REV 1', 0, 1, 'L');
        $this->Cell(0, 3, 'FECHA: 29-05-06', 0, 1, 'L');
    }
}

$pdf = new PDF();

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('PEDIDO DE BOBINAS');
$pdf->SetTitle('Pedido de bobinas ' . $pedido['numero']);
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->SetMargins(10, 26);
$pdf->AddPage();

$pdf->SetFont('dejavusans', 'B');
$txt = 'Pedido';
$pdf->Cell(20, 4, $txt . ' Nº', 0, 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(30, 4, $pedido['numero'], 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->SetX(135);
$pdf->Cell(40, 4, 'Fecha de pedido', 0, 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, DateTime::createFromFormat('Y-m-d', $pedido['fecha'])->format('d-m-Y'), 0, 1);
$pdf->Ln();
$yProveedor = $pdf->GetY();

$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Proveedor', 0, 1, 'L');
$pdf->Cell(40, 4, 'Descarga de Mercancía', 0, 0, 'L');
$pdf->SetFont('dejavusans', '');
if (count($semanas) == 1) {
    $pdf->Cell(0, 4, $descargar, 0, 1);
} else {
    $pdf->Cell(0, 4, 'VER COMENTARIOS', 0, 1);
}
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Plazo de Entrega', 0, 0, 'L');
$pdf->SetFont('dejavusans', '');
if (count($semanas) == 1) {
    $pdf->Cell(0, 4, trim($pedido['plazo']), 0, 1);
} else {
    $pdf->Cell(0, 4, 'VER COMENTARIOS', 0, 1);
}
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Tipo de Embalaje', 0, 0, 'L');
$pdf->SetFont('dejavusans', '');
switch (intval($pedido['tipo_embalaje'])) {
    case 1:
        $tipoEmbalaje = 'L06 (Cantos Metálicos + Fleje) Estándar';
        break;
    case 2:
        $tipoEmbalaje = 'L14 (Cantos Metálicos + Plástico + Fleje) Prepintados Baja Rotación';
        break;
    case 3:
        $tipoEmbalaje = 'C16 (Cantos Metálicos + Plástico + Fleje) Galvanizados Baja Rotación';
        break;
    default:
        $tipoEmbalaje = 'C06 (Cantos + Fleje) Estándar';
        break;
}
$pdf->Cell(0, 4, $tipoEmbalaje, 0, 1);
$yUltimoCampo = $pdf->GetY();

$pdf->SetY($yProveedor);
$pdf->SetFont('dejavusans', 'B', 10);
$pdf->Cell(0, 4, trim($proveedor['nombre']), 0, 1, 'R');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetX(105);
$pdf->MultiCell(0, 4, trim($proveedor['direccion']) . ' ' . trim($proveedor['codpo']) . ' ' . trim($proveedor['poblacion']) . ' ' . trim($proveedor['provincia']) . PHP_EOL, 0, 'R');
$pdf->SetY($yUltimoCampo);

$pdf->Ln(4);

$pdf->SetFont('dejavusans', 'B', 12);
$pdf->Cell(0, 4, 'PEDIDO DE BOBINAS', 'B', 0, 'C');
$pdf->SetFont('dejavusans', '', 7);
$pdf->SetXY(5, $pdf->GetY() + 1.5);
$pdf->Cell(0, 4, 'ATENCIÓN: Diametro Interior Bobinas = 500 mm', 0, 1, 'R');

$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetFont('dejavusans', 'B', 8);
$pdf->Cell(8, 6, 'Nº', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(16, 6, 'Calidad', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(16, 6, 'Recubri.', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(41, 6, 'Revestimiento A/B', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(41, 6, 'Color A/B', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(6, 6, 'Cert.', 'B', 0, 'R', false, '', 0, false, '', 'B');
$pdf->Cell(18, 6, 'Desarrollo', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(16, 6, 'Espesor', 'B', 0, '', false, '', 0, false, '', 'B');
$pdf->Cell(13, 6, 'TN', 'B', 0, 'R', false, '', 0, false, '', 'B');
$pdf->Cell(15, 6, '€/TN', 'B', 1, 'R', false, '', 0, false, '', 'B');

$pdf->SetFont('dejavusans', '', 8);
for ($i = 0; $i < count($pedidolin); $i++) {
    $pedidolin[$i]['linea'] = $i + 1;
    $pintura = trim($pedidolin[$i]['pintura']);
    $color = trim($pedidolin[$i]['color']);
    $pinturab = trim($pedidolin[$i]['pinturab']);
    $colorb = trim($pedidolin[$i]['colorb']);
    $precio = number_format($pedidolin[$i]['precio'], 2, ',', '.');
    $cert = '';
    if (intval($pedidolin[$i]['certificado']) == 0) $cert = '2.2';
    if (intval($pedidolin[$i]['certificado']) == 1) $cert = '3.1';

    if (strtoupper($color) == 'GALVANIZADO') {
        $pintura = $color;
        $color = 'MA';
    } else {
        $pintura .= '/' . $pinturab;
        $color .= '/' . $colorb;
    }

    if ($pdf->GetStringWidth($pintura) >= 41 || $pdf->GetStringWidth($color) >= 41) {
        $txt = $pdf->GetStringWidth($pintura) >= $pdf->GetStringWidth($color) ? $pintura : $color;
        $height = $pdf->getStringHeight(41, $txt, true) + 1;
        $pdf->Cell(8, $height, $pedidolin[$i]['linea'], 0, 0, '', false, '', 0, false, 'T', 'T');
        $pdf->Cell(16, $height, trim($pedidolin[$i]['calidad']), 0, 0, '', false, '', 0, false, 'T', 'T');
        $pdf->Cell(16, $height, trim($pedidolin[$i]['recubrimiento']), 0, 0, '', false, '', 0, false, 'T', 'T');
        $pdf->MultiCell(41, $height, $pintura, 0, 'L', false, 0);
        $pdf->MultiCell(41, $height, $color, 0, 'L', false, 0);
        $pdf->Cell(6, $height, $cert, 0, 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell(18, $height, $pedidolin[$i]['ancho'] . ' mm', 0, 0, '', false, '', 0, false, 'T', 'T');
        $pdf->Cell(16, $height, number_format($pedidolin[$i]['espesor'], 2, ',', '.') . ' mm', 0, 0, '', false, '', 0, false, 'T', 'T');
        $pdf->Cell(13, $height, number_format($pedidolin[$i]['toneladas'], 2, ',', '.'), 0, 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell(15, $height, $precio, 0, 1, 'R', false, '', 0, false, 'T', 'T');
    } else {
        $pdf->Cell(8, 4, $pedidolin[$i]['linea'], 0, 0);
        $pdf->Cell(16, 4, trim($pedidolin[$i]['calidad']), 0, 0);
        $pdf->Cell(16, 4, trim($pedidolin[$i]['recubrimiento']), 0, 0);
        $pdf->Cell(41, 4, $pintura, 0, 0);
        $pdf->Cell(41, 4, $color, 0, 0);
        $pdf->Cell(6, 4, $cert, 0, 0, 'R');
        $pdf->Cell(18, 4, $pedidolin[$i]['ancho'] . ' mm', 0, 0);
        $pdf->Cell(16, 4, number_format($pedidolin[$i]['espesor'], 2, ',', '.') . ' mm', 0, 0);
        $pdf->Cell(13, 4, number_format($pedidolin[$i]['toneladas'], 2, ',', '.'), 0, 0, 'R');
        $pdf->Cell(15, 4, $precio, 0, 1, 'R');
    }
}

$pdf->Ln();
$pdf->SetFont('dejavusans', 'B', 8);
$pdf->Cell(120, 6, '', 0, 0);
$pdf->Cell(10, 6, 'Total:', 'LTB', 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(35, 6, number_format($totalToneladas, 2, ',', '.') . ' TN', 'RTB', 1, 'R');
$pdf->Ln(4);

$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->Cell(0, 4, 'CONDICIONES GENERALES', 'B', 1, 'C');
$pdf->Ln(2);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Entrega de Materiales', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Los materiales deben llevar su correspondiente albarán de entrega, número de pedido y certificado de calidad.' . PHP_EOL);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Facturación', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Sus facturas se deberán recibir en nuestras oficinas como máximo a los 10 días de su fecha de emisión acompañadas con una copia de los albaranes y certificados de calidad.' . PHP_EOL);
$pdf->Ln(2);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Formas de Pago', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, $fopago, 0, 1, 'L');
$pdf->Ln(12);

$obs = trim(trim($pedido['observaciones']) . PHP_EOL . trim($pedido['obs_prove_2']));
if (!empty($obs) || count($semanas) > 1) {
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.51); //1px = 0.085mm
    $pdf->Cell(0, 4, 'COMENTARIOS', 'B', 1, 'C');
    $pdf->SetFontSize(7);
    $pdf->Ln(2);
    if (count($semanas) > 1) {
        for ($i = 0; $i < count($pedidolin); $i++) {
            $linea = $pedidolin[$i]['linea'];
            $semana = $pedidolin[$i]['semana_prevista'];
            $almacenes = $pedidolin[$i]['descargar'];
            $descargar = '';
            if ($almacenes == 'A') {
                $descargar = 'NAVE A (C/ NOU, 16)';
            } else {
                $descargar = 'NAVE B (C/ NOU, 27)';
            }

            $pdf->Cell(0, 4, 'Linea ' . $linea . ' - Entrega Semana ' . $semana . ' - Almacén de Descarga ' . $descargar, 0, 1, 'L');
        }
        $pdf->Ln(4);
    }
    $pdf->MultiCell(0, 4, $obs . PHP_EOL);
    $pdf->Ln(4);
}

$pdf->AddPage();
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->Cell(0, 4, 'CONDICIONES DE SUMINISTRO', 'B', 1, 'C');
$pdf->SetFontSize(7);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'CARACTERÍSTICAS DEL MATERIAL', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'El tipo de acero, su composición química, las propiedades mecánicas, la masa del recubrimiento y calidad superficial cumplirán con los requisitos de acuerdo a la normativa UNE EN 10346 (galvanizados) y UNE EN 10169 (prelacados)' . PHP_EOL);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'IDENTIFICACIÓN DEL MATERIAL', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Cada bobina vendrá identificada mediante una etiqueta en español donde debe aparecer:' . PHP_EOL);
$pdf->Cell(0, 4, '- Nombre o marca de la fábrica productora', 0, 1);
$pdf->Cell(0, 4, '- Designación completa de acuerdo a normativa UNE-EN 10169', 0, 1);
$pdf->Cell(0, 4, '- Color', 0, 1);
$pdf->Cell(0, 4, '- Dimensiones nominales del producto', 0, 1);
$pdf->Cell(0, 4, '- Número de identificación o bulto', 0, 1);
$pdf->Cell(0, 4, '- Número de pedido', 0, 1);
$pdf->Cell(0, 4, '- Peso de la bobina', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'CERTIFICADO DE CALIDAD', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Se entregará el certificado de calidad Tipo 2.2 o 3.1 de acuerdo a lo especificado en el pedido, por adelantado o junto con la mercancía. Si es posible dicho certificado se enviará electrónicamente por email. Estos certificados deberán cumplir con la normativa UNE-EN 10204:2006.' . PHP_EOL);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'TOLERANCIAS DIMENSIONALES', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'En ausencia de información adicional en el pedido respecto a las condiciones de suministro, los productos planos se deben suministrar con tolerancias normales en espesor, anchura, longitud, planicidad y ondulación de bordes, de acuerdo a la normativa UNE-EN 10143' . PHP_EOL);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'EMBALAJE', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Los requisitos de embalaje del producto cumplirán con lo acordado al solicitar la oferta y realizar el pedido. De cualquier manera, será suficiente para proteger la bobina durante el transporte y almacenamiento de golpes y condensaciones.' . PHP_EOL);
$pdf->Cell(0, 4, 'Las bobinas se deben expedir con su eje en posición horizontal.', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'DIMENSIONES BOBINA', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, 'Peso Mínimo: 7 Tn.', 0, 1);
$pdf->Cell(0, 4, 'Peso Máximo: 11 Tn.', 0, 1);
$pdf->Cell(0, 4, 'Diámetro Cilindro Interior Bobina: 508 mm', 0, 1);
$pdf->Ln(4);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, 'USO FINAL', 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, 'El uso final de las bobinas es perfiles para cubiertas y fachadas industriales.', 0, 1);
$pdf->Ln(4);

$dir = INCO_DIR_PROVEEDORES . $proveedor['codigo'] . '/Pedidos/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'P_' . str_pad($pedido['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
// $fileName = 'PedidoBobinas.pdf';
$output = $dir . $fileName;
// if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

// pg_free_result($result);
pg_close($dbconn);