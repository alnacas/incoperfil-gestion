<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$idioma = isset($argv[3]) ? trim($argv[3]) : '';

$logos = boolval($argv[4]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos datos de la factura
$result = pg_query_params('SELECT ejercicio, serie, numero, cliente, fecha, forma_pago, bruto, descuento_pronto_pago, total_descuento_pronto_pago, recargo_financiero, total_recargo_financiero, base_exenta, base_0, tipo_iva_0, total_iva_0, base_1, tipo_iva_1, total_iva_1, base_2, tipo_iva_2, total_iva_2, total_factura, tipo_portes, portes, anticipo, anticipo_con_factura, factura_anticipado, iva, observaciones FROM facturas WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [facturas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[facturas] No hay resultado');
$factura = pg_fetch_all($result)[0];

$result = pg_query_params('SELECT DISTINCT identificador_oferta FROM albaranes WHERE factura = $1', array($identificador));
$ofer = trim(pg_fetch_all($result)[0]['identificador_oferta']);
if (is_null($ofer)) {
    $oferta = array('condiciones' => '', 'transporte' => '', 'texto_factura' => '');
} else {
    $result = pg_query_params('SELECT condiciones, transporte, texto_factura FROM ofertas WHERE identificador = $1', array($ofer))
    or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) $oferta = array('condiciones' => '', 'transporte' => '', 'texto_factura' => '');
    else $oferta = pg_fetch_all($result)[0];
}

// obtenemos los albaranes
$result = pg_query_params($dbconn, 'SELECT identificador, numero, referencia_cliente, identificador_oferta, referencia_obra, portes, sum(total_portes + total_portes_exceso)::numeric(10, 2) AS total_portes FROM albaranes WHERE factura = $1 GROUP BY identificador ORDER BY identificador', array($identificador)) or incoLogWrite('La consulta fallo [albaranes]: ' . pg_last_error());
if ($factura['serie'] == 'FA') {
    $albaranes = array();
} else {
    if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes] No hay resultado');
    $albaranes = pg_fetch_all($result);
}

// obtenemos las lineas del albaran
pg_prepare($dbconn, 'albaraneslin', 'SELECT seccion, descripcion_comercial, precio, unidad_medida, descuento, referencia_plano, posicion, SUM(cantidad)::numeric(10,2) AS cantidad, SUM(metros)::NUMERIC(10,2) AS metros, SUM(importe)::NUMERIC(10,2) AS importe, string_agg(observaciones, \', \')::character(255) AS observaciones FROM albaraneslin WHERE identificador = $1 GROUP BY seccion, producto, espesor, calidad, recubrimiento, pintura, ccolor, unidad_medida, posicion, descripcion_comercial, precio, descuento, referencia_plano ORDER BY seccion, importe');

$result = pg_query_params('SELECT orden, max(fvto)::DATE AS fvto, sum(importe)::NUMERIC(10,2) AS importe FROM efectos WHERE identif_fra = $1 GROUP BY 1 ORDER BY 2', array($identificador));
// if (pg_num_rows($result) <= 0) incoLogWrite('[efectos] No hay resultado');
$efectos = pg_fetch_all($result);

$result = pg_query_params('SELECT DISTINCT tipo_efec FROM efectos WHERE identif_fra = $1 AND orden > 0', array($identificador));
// if (pg_num_rows($result) <= 0) incoLogWrite('[efectos] No hay resultado');
$tipoefec = trim(pg_fetch_all($result)[0]['tipo_efec']);

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_facturas', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma, cpais, iban, cbanco, csucursal, cdc, cuenta, direccionadm, codpoadm, poblacionadm, provinciaadm, dpago1, dpago2, dpago3 FROM clientes WHERE codigo = $1 LIMIT 1');

// preparamos para obtener el cliente
pg_prepare($dbconn, 'paises', 'SELECT nombre, nombre2, cee, texto_factura FROM paises WHERE codigo = $1 LIMIT 1');

// preparamos para obtener la forma de pago
pg_prepare($dbconn, 'fopago_facturas', 'SELECT nombre, banco FROM fopago WHERE codigo = $1');

// preparamos para obtener el banco
pg_prepare($dbconn, 'bancos_facturas', 'SELECT nombre, (iban || cbanco || csucursal || cdc || cuenta)::CHARACTER(63) AS cuenta FROM bancos WHERE codigo = $1');

// preparamos para obtener el numero del albaran
pg_prepare($dbconn, 'albaranes_facturas', 'SELECT numero, referencia_cliente, identificador_oferta, referencia_obra FROM albaranes WHERE identificador = $1 LIMIT 1');

// preparamos para obtener la unidad de medida de cada linea del albaran
pg_prepare($dbconn, 'unidadesmedida_albaranlin', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1 LIMIT 1');

$factura['fecha'] = DateTime::createFromFormat('Y-m-d', $factura['fecha'])->format('d/m/Y');
// $pedidoCli = trim($albaran['referencia_cliente']);
$cliente = $factura['cliente'];
$formaPago = trim($factura['forma_pago']);

if (empty($cliente) || is_null($cliente) || $cliente <= 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    $result = pg_execute($dbconn, 'clientes_facturas', array($cliente));
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $aux = pg_fetch_all($result)[0];
    $cliente = array();
    $cliente['codigo'] = $aux['codigo'];
    $cliente['nif'] = trim($aux['nif']);
    $cliente['nombre'] = trim($aux['nombre']);
    $cliente['direccion'] = trim($aux['direccion']);
    $cliente['codpo'] = $aux['codpo'];
    $cliente['poblacion'] = trim($aux['poblacion']);
    $cliente['provincia'] = trim($aux['provincia']);
    $cliente['direccionadm'] = trim($aux['direccionadm']);
    $cliente['codpoadm'] = $aux['codpoadm'];
    $cliente['poblacionadm'] = trim($aux['poblacionadm']);
    $cliente['provinciaadm'] = trim($aux['provinciaadm']);
    $cliente['pais'] = trim($aux['cpais']);
    $cliente['isDiaPagoDijo'] = max($aux['dpago1'], $aux['dpago2'], $aux['dpago3']) > 0;
    $iban = trim($aux['iban']);
    $iban = str_pad($iban, 4, '0', STR_PAD_LEFT);
    $banco = trim($aux['cbanco']);
    $banco = str_pad($banco, 4, '0', STR_PAD_LEFT);
    $sucursal = trim($aux['csucursal']);
    $sucursal = str_pad($sucursal, 4, '0', STR_PAD_LEFT);
    $dc = trim($aux['cdc']);
    $dc = str_pad($dc, 2, '0', STR_PAD_LEFT);
    $cuenta = trim($aux['cuenta']);
    $cuenta = str_pad($cuenta, 10, '0', STR_PAD_LEFT);
    $cliente['cuenta'] = $iban . $banco . $sucursal . $dc . $cuenta;
    $cliente['cuenta'] = substr($cliente['cuenta'], 0, 20) . '****';
    if (empty($idioma)) $idioma = trim($aux['idioma']);
    if (empty($idioma)) $idioma = 'es';
    $tr = new IncoTranslatorFix($dbconn, 'factura', $idioma);

    $isSpain = strtoupper(trim($cliente['pais'])) == 'ES';
    $result = pg_execute($dbconn, 'paises', array($cliente['pais']));
    if (pg_num_rows($result) <= 0) incoLogWrite('[paises] No hay resultado');
    $cliente['pais'] = pg_fetch_all($result)[0];
    /*if ($isSpain) {
        $txtFacts = explode('|', $cliente['pais']['texto_factura']);
        $txtFact = '';
        $result = pg_query_params($dbconn, 'select string_agg(distinct tipo_exencion::text, \'|\')::character(511) as tipos from albaranes where factura = $1 and iva = 0 and tipo_exencion is not null', array($identificador));
        // if (pg_num_rows($result) <= 0) incoLogWrite('[efectos] No hay resultado');
        $tipos = trim(pg_fetch_all($result)[0]['tipos']);
        $tipos = explode('|', $tipos);
        for ($i = 0; $i < count($tipos); $i++) {
            $j = intval($tipos[$i]);
            if (isset($txtFacts[$j])) {
                $txtFact .= $txtFacts[$j];
            }
        }
        $cliente['pais']['texto_factura'] = $txtFact;
    }*/
    if ($idioma != 'es') {
        $cliente['pais']['nombre'] = trim($cliente['pais']['nombre2']);
    }
}

if (empty($formaPago) || is_null($formaPago)) {
    incoLogWrite('[forma_pago] No hay valor');
} else {
    $result = pg_execute($dbconn, 'fopago_facturas', array($formaPago));
    if (pg_num_rows($result) <= 0) incoLogWrite('[forma_pago] No hay resultado');
    $aux = pg_fetch_all($result)[0];
    $formaPago = trim($aux['nombre']) . ' - CON DÍAS FIJOS DE PAGO';

    if ($aux['banco'] == 0) {
        $banco = array();
    } else {
        $result = pg_execute($dbconn, 'bancos_facturas', array($aux['banco']));
        if (pg_num_rows($result) <= 0) incoLogWrite('[banco] No hay resultado');
        $banco = pg_fetch_all($result)[0];
    }
}

class PDF extends TCPDF
{
    /**
     * @var bool
     */
    public $diradm = false;

    /**
     * @var IncoTranslatorFix
     */
    private $tr;

    /**
     * @var array
     */
    private $factura;

    /**
     * @var array
     */
    private $cliente;

    /**
     * @var string
     */
    private $formaPago;

    /**
     * @var array
     */
    private $banco;

    /**
     * @var array
     */
    private $efectos;

    /**
     * @param IncoTranslatorFix $tr
     */
    public function setTr($tr)
    {
        $this->tr = $tr;
    }

    /**
     * @param array $factura
     */
    public function setFactura($factura)
    {
        $this->factura = $factura;
    }

    /**
     * @param array $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @param string $formaPago
     */
    public function setFormaPago($formaPago)
    {
        $this->formaPago = $formaPago;
    }

    /**
     * @param array $banco
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;
    }

    /**
     * @param array $efectos
     */
    public function setEfectos($efectos)
    {
        $this->efectos = $efectos;
    }

    function Header()
    {
	    global $logos;

	    if ($logos) {
            $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', 20, 10, 60, 0, 'jpg', '', '', true, 300);
            $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 247, 211, 0, 'jpg', '', '', true, 300);
        }

        if ($this->diradm) {
            $this->SetY(32);
            $this->Ln();
            $this->Ln();
            $this->SetFont('dejavusans', '', 8);
            $this->setCellPaddings('3', '1', '3', '1');
            $this->Ln();
            $this->SetX(100);
            $this->Cell(0, 4, str_pad($this->cliente['codigo'], 5, '0', STR_PAD_LEFT), 'LTR', 1);
            $this->SetX(100);
            $this->Cell(0, 4, trim($this->cliente['nombre']), 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, 4, trim($this->cliente['direccionadm']), 'LR', 1);
            $this->SetX(100);
            $txt = trim($this->cliente['codpoadm']) . ' - ' . trim($this->cliente['poblacionadm']) . ' (' . trim($this->cliente['provinciaadm']) . ')';
            $this->Cell(0, 4, $txt, 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, 4, trim($this->cliente['pais']['nombre']), 'LRB', 1);
            $this->Ln(2);
        } else {
            $this->Image(INCO_DIR_IMAGENES . 'cyc.jpg', 165, 251, 30, 0, 'jpg', '', '', true, 300);

            $txt = $this->tr->getTranslate('title');
            if ($this->factura['serie'] == 'FA') $txt = $this->tr->getTranslate('title_1');
            if ($this->factura['serie'] == 'FR') $txt = $this->tr->getTranslate('title_2');

            $this->SetY(5);
            $this->SetFont('dejavusans', 'B', 24);
            $this->Cell(0, 4, $txt, 0, 1, 'R');
            $this->SetFont('dejavusans', '', 8);
            $this->Cell(0, 4, $this->tr->getTranslate('h_1') . ' ' . $this->PageNo(), 0, 1, 'R');
            $y = $this->GetY();

            $this->SetY(46);
            $this->Cell(25, 4, $this->tr->getTranslate('h_2'), 0, 0);
            $this->SetFont('dejavusans', 'B', 8);
            $serie = (trim($this->factura['serie']) == 'FR') ? 'R' : 0;
            $this->Cell(60, 4, $serie . '/' . str_pad($this->factura['numero'], 6, '0', STR_PAD_LEFT), 0, 1);
            $this->SetFont('dejavusans', '', 8);
            $this->Cell(25, 4, $this->tr->getTranslate('h_3'), 0, 0);
            $this->Cell(60, 4, $this->factura['fecha'], 0, 1);
            $this->Cell(25, 4, $this->tr->getTranslate('h_4'), 0, 0);
            $this->Cell(60, 4, $this->cliente['nif'], 0, 1);

            $this->SetY($y);
            $this->Ln();
            $this->Ln();
            $this->SetFont('dejavusans', '', 8);
            $this->setCellPaddings('3', '1', '3', '1');
            $this->Ln();
            $this->SetX(100);
            $this->Cell(0, 4, str_pad($this->cliente['codigo'], 5, '0', STR_PAD_LEFT), 'LTR', 1);
            $this->SetX(100);
            $this->Cell(0, 4, trim($this->cliente['nombre']), 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, 4, trim($this->cliente['direccion']), 'LR', 1);
            $this->SetX(100);
            $txt = trim($this->cliente['codpo']) . ' - ' . trim($this->cliente['poblacion']) . ' (' . trim($this->cliente['provincia']) . ')';
            $this->Cell(0, 4, $txt, 'LR', 1);
            $this->SetX(100);
            $this->Cell(0, 4, trim($this->cliente['pais']['nombre']), 'LRB', 1);
            $this->Ln(2);

            // $this->SetX(90);
            // $this->MultiCell(0, 12, $this->observaciones, 0, 'L', false, 1);

            // $this->SetCellPadding(2);
            // $this->setCellPaddings('4', '0', '0', '0');

            $this->SetCellPadding(0);
            $this->SetXY(0, 90);
            $this->Cell(10, 4, '--');
            $this->SetXY(0, 200);
            $this->Cell(10, 4, '--');
        }
    }

    function Footer()
    {
        global $tipoefec;

        if (!$this->diradm) {
            $this->SetY(-70);
            $this->SetFont('dejavusans', '', 8);
            $this->Cell(30, 4, $this->tr->getTranslate('f_1'), 1, 0, 'C');
            if ($this->factura['descuento_pronto_pago'] == 0) {
                $this->Cell(65, 4, '', 1, 0, 'C');
            } else {
                $this->Cell(65, 4, $this->tr->getTranslate('f_2'), 1, 0, 'C');
            }
            $this->Cell(30, 4, $this->tr->getTranslate('f_3'), 1, 0, 'C');
            $this->Cell(18, 4, $this->tr->getTranslate('f_4'), 1, 0, 'C');
            $this->Cell(25, 4, $this->tr->getTranslate('f_5'), 1, 0, 'C');
            $this->Cell(0, 4, $this->tr->getTranslate('f_6', true), 1, 1, 'C');

            $this->setCellPaddings('0', '0', '2', '0');
            $this->Cell(30, 4, number_format($this->factura['bruto'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
            if ($this->factura['descuento_pronto_pago'] == 0) {
                $this->Cell(65, 4, '', 'LTR', 0, 'C');
            } else {
                $this->Cell(65, 4, number_format($this->factura['descuento_pronto_pago'], 0) . '% s/ ' . number_format($this->factura['bruto'], 2, ',', '.') . ' € = ' . number_format($this->factura['total_descuento_pronto_pago'] * -1, 2, ',', '.') . ' €', 'LTR', 0, 'C');
            }

            if ($this->factura['iva'] == 0) {
                $this->Cell(30, 4, number_format($this->factura['base_exenta'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
                $this->Cell(18, 4, '0,00 %', 'LTR', 0, 'C');
                $this->Cell(25, 4, '0,00 €', 'LTR', 0, 'R');
            }
            if ($this->factura['iva'] == 1) {
                $this->Cell(30, 4, number_format($this->factura['base_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
                $this->Cell(18, 4, number_format($this->factura['tipo_iva_0'], 0) . ' %', 'LTR', 0, 'C');
                $this->Cell(25, 4, number_format($this->factura['total_iva_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
            }
            $this->SetFont('dejavusans', 'B');
            $this->Cell(0, 4, number_format($this->factura['total_factura'], 2, ',', '.') . ' €', 'LTR', 1, 'R');

            if ($this->factura['base_1'] == 0) {
                $this->Cell(30, 4, '', 'LBR', 0, 'R');
                $this->Cell(65, 4, '', 'LBR', 0, 'C');
                $this->Cell(30, 4, '', 'LBR', 0, 'R');
                $this->Cell(18, 4, '', 'LBR', 0, 'C');
                $this->Cell(25, 4, '', 'LBR', 0, 'R');
                $this->Cell(0, 4, '', 'LBR', 1, 'R');
            } else {
                $this->Cell(30, 4, '', 'LBR', 0, 'R');
                $this->Cell(65, 4, '', 'LBR', 0, 'C');
                $this->Cell(30, 4, number_format($this->factura['base_1'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
                $this->Cell(18, 4, number_format($this->factura['tipo_iva_1'], 0) . ' %', 'LBR', 0, 'C');
                $this->Cell(25, 4, number_format($this->factura['total_iva_1'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
                $this->SetFont('dejavusans', 'B');
                $this->Cell(0, 4, '', 'LBR', 1, 'R');
            }
            $this->Ln(2);

            $vencimientos = array();
            $importes = array();
            for ($i = 0; $i < count($this->efectos); $i++) {
                if (intval($this->efectos[$i]['orden']) == 0) {
                    array_push($vencimientos, $this->tr->getTranslate('f_10'));
                } else {
                    $fvto = explode('-', $this->efectos[$i]['fvto']);
                    array_push($vencimientos, $fvto[2] . '/' . $fvto[1] . '/' . $fvto[0]);
                }
                array_push($importes, number_format($this->efectos[$i]['importe'], 2, ',', '.') . ' €');
            }

            $this->SetFont('dejavusans', '');
            $this->SetCellPadding(0);
            $this->Cell(30, 4, $this->tr->getTranslate('f_7'), 0, 0);
            // $this->SetFont('dejavusans', 'B');
            $formaPago = $this->tr->getTranslate($this->formaPago);
            if (!empty($this->banco)) {
                $formaPago .= ' -- ' . trim($this->banco['nombre']);
                $aux = str_split(trim($this->banco['cuenta']), 4);
                $formaPago .= ' -- ' . implode('-', $aux);
            }
            if ($tipoefec == 'L' || $tipoefec == 'A') {
                $formaPago .= ' -- ' . $this->cliente['cuenta'];
            }
            $this->Cell(0, 4, $formaPago, 0, 1);
            $yVencimientos = $this->GetY();
            if ($this->factura['serie'] != 'FR' && !(floatval($this->factura['anticipo_con_factura']) > 0 && floatval($this->factura['total_factura']) == 0)) {
                $this->SetFont('dejavusans', '');
                $this->Cell(30, 4, $this->tr->getTranslate('f_8'), 0, 0);
                // $this->SetFont('dejavusans', 'B');
                for ($i = 0; $i < count($vencimientos); $i++) {
                    $ln = ($i == count($vencimientos) - 1) ? 1 : 0;
                    $this->Cell(20, 4, $vencimientos[$i], 0, $ln, 'C');
                }
                $this->SetFont('dejavusans', '');
                $this->Cell(30, 4, $this->tr->getTranslate('f_9'), 0, 0);
                // $this->SetFont('dejavusans', 'B');
                for ($i = 0; $i < count($importes); $i++) {
                    $ln = ($i == count($importes) - 1) ? 1 : 0;
                    $this->Cell(20, 4, $importes[$i], 0, $ln, 'C');
                }
            }
            $y = $this->GetY();
            if ($this->factura['serie'] != 'FR') {
                $this->SetXY($this->GetX() + 134, $yVencimientos + 1);
                $this->SetFont('dejavusans', 'B');
                $this->Cell(0, 4, $this->tr->getTranslate('f_11', true), 0, 0, 'R');
            }
            $this->SetY($y + 5);
            $this->SetFont('dejavusans', '', 7);
            $rgpd = "Información básica sobre protección de datos:\nResponsable: Ingeniería y Construccion del Perfil, S.A. Finalidades: Gestionar los servicios contratados y facturación. Legitimación: Ejecución de un contrato y consentimiento del interesado. Destinatarios: No se cederán datos a terceros, salvo obligación legal. Derechos: Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la información adicional. Información adicional: Puede consultar la información adicional y detallada sobre Protección de Datos en www.incoperfil.com" . PHP_EOL;
            $this->MultiCell(188, 4, $rgpd, 0, 'J', false, 1);
        }
        if ($this->diradm) $this->diradm = false;
    }
}

$pdf = new PDF();
$pdf->setTr($tr);
$pdf->setFactura($factura);
$pdf->setCliente($cliente);
$pdf->setFormaPago($formaPago);
$pdf->setBanco($banco);
$pdf->setEfectos($efectos);

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('FACTURA');
$pdf->SetTitle('Factura X');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 63); // 70

if (!$logos) {
    // AÑADIR PAGINA EN BLANCO PARA LOS QUE SE ENVIAN A UN APARTADO DE CORREO O DIRECCION DE ENVIO DIFERENTE A LA FISCAL
    if ($cliente['direccion'] != $cliente['direccionadm'] || $cliente['codpo'] != $cliente['codpoadm'] || $cliente['poblacion'] != $cliente['poblacionadm'] || $cliente['provincia'] != $cliente['provinciaadm']) {
        $pdf->diradm = true;
        $pdf->AddPage();
    }
}

$pdf->AddPage();

$pdf->Cell(25, 4, $tr->getTranslate('b_1', true), 1, 0, 'C');
$pdf->Cell(115, 4, $tr->getTranslate('b_2', true), 1, 0, 'C');
$pdf->Cell(20, 4, $tr->getTranslate('b_3', true), 1, 0, 'C');
$pdf->Cell(15, 4, '% ' . $tr->getTranslate('b_4', true), 1, 0, 'C');
$pdf->Cell(0, 4, $tr->getTranslate('b_5', true), 1, 1, 'C');

$addTexto = false;
if (trim($cliente['pais']['cee']) == 'S') {
    if (!boolval($factura['iva'])) {
        $addTexto = true;
    }
} else {
    $addTexto = true;
}

if ($addTexto) {
    /*$txt = trim($cliente['pais']['texto_factura']);
    $lines = ceil($pdf->GetStringWidth($txt) / 115);
    $height = $lines * 4;*/

    $result = pg_query_params('SELECT DISTINCT e.descripcion FROM albaranes a, exenciones e WHERE a.factura = $1 AND a.iva = 0 AND a.tipo_exencion > 0 AND a.tipo_exencion = e.codigo', array($identificador));
    // if (pg_num_rows($result) <= 0) incoLogWrite('[efectos] No hay resultado');
    $exenciones = pg_fetch_all($result);
    $txt = implode(PHP_EOL, array_map(function ($exencion) {
        return trim($exencion);
    }, array_column($exenciones, 'descripcion')));
    $height = ceil($pdf->getStringHeight(110, $txt, true));

    $pdf->Cell(18, $height, '', 'L', 0);
    $pdf->Cell(7, $height, '', 'R', 0);
    $pdf->MultiCell(115, $height, $txt, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, '', 'LR', 0);
    $pdf->Cell(15, $height, '', 'LR', 0);
    $pdf->Cell(0, $height, '', 'LR', 1);

    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, '', 'LR', 0, 'C');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');
}

if ($factura['serie'] == 'FA') {
    // obtenemos datos de la factura
    // $result = pg_query_params('SELECT aplicacion, numero_aplicacion FROM anticipos WHERE factura_anticipo = $1', array($identificador))
    /*$result = pg_query_params('SELECT aplicacion, numero_aplicacion as numero FROM anticipos WHERE factura_anticipo = $1', array($identificador))
    or incoLogWrite('La consulta fallo [anticipos]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[anticipos] No hay resultado');
    $anticipo = pg_fetch_all($result)[0];
    $anticipo['numero'] = intval(substr($anticipo['numero'], -7));

    $txt = '';
    if ($anticipo['aplicacion'] == 0) {
        $result = pg_query_params('SELECT referencia_obra FROM ofertas WHERE numero = $1', array($anticipo['numero']))
        or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[ofertas] No hay resultado');
        $txt = trim(pg_fetch_all($result)[0]['referencia_obra']);
    }
    if ($anticipo['aplicacion'] == 1) {
        $result = pg_query_params('SELECT referencia_obra FROM pedidos WHERE numero = $1', array($anticipo['numero']))
        or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
        $txt = trim(pg_fetch_all($result)[0]['referencia_obra']);
    }

    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, $tr->getTranslate('b_9', true) . ' ' . $txt, 'LR', 0, 'L');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');*/

    if ($factura['iva'] == 0) {
        $importe = $factura['base_exenta'];
    } else {
        $importe = $factura['base_0'];
    }

    $txt = trim($factura['observaciones']);
    $height = ceil($pdf->getStringHeight(115, $txt, true));
    $pdf->Cell(18, $height, number_format(1, 2, ',', '.'), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(7, $height, 'UD', 'R', 0, 'C', false, '', 0, false, 'T', 'T');
    $pdf->MultiCell(115, $height, $txt, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, number_format($importe, 2, ',', '.'), 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(15, $height, '', 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(0, $height, number_format($importe, 2, ',', '.'), 'LR', 1, 'R', false, '', 0, false, 'T', 'T');
}

if ($factura['serie'] == 'FR') {
    $result = pg_query_params('SELECT DISTINCT factura_rectificada, tipo_rectificativa FROM albaranes WHERE factura = $1', array($identificador));
    if (pg_num_rows($result) <= 0) $aux = array();
    else $aux = pg_fetch_all($result)[0];

    $tipo = trim($aux['tipo_rectificativa']);
    $fact = trim($aux['factura_rectificada']);

    switch ($tipo) {
        case 'I': $motivo = $tr->getTranslate('r_2', true); break;
        default : $motivo = '';
    }

    $txt = trim($tr->getTranslate('r_1', true) . intval(substr($fact, 9)) . ' ' . $motivo);
    $lines = ceil($pdf->GetStringWidth($txt) / 115);
    $height = $lines * 4;
    $pdf->Cell(25, $height, '', 'LR', 0, 'C');
    $pdf->MultiCell(115, $height, $txt, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, '', 'LR', 0, 'C');
    $pdf->Cell(15, $height, '', 'LR', 0, 'C');
    $pdf->Cell(0, $height, '', 'LR', 1, 'C');
}

$maxHeight = 223;
for ($i = 0; $i < count($albaranes); $i++) {
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(38.33, 4, $tr->getTranslate('b_6', true) . ' ' . intval($albaranes[$i]['numero']), 'L', 0);
    $pdf->Cell(38.33, 4, $tr->getTranslate('b_7', true) . ' ' . trim($albaranes[$i]['referencia_cliente']), 0, 0);
    $pdf->Cell(38.33, 4, $tr->getTranslate('b_8', true) . ' ' . intval(substr(trim($albaranes[$i]['identificador_oferta']), 8)), 'R', 0);
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, $tr->getTranslate('b_9', true) . ' ' . trim($albaranes[$i]['referencia_obra']), 'LR', 0, 'L');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');

    $result = pg_execute($dbconn, 'albaraneslin', array($albaranes[$i]['identificador']));
    $albaraneslin = array();
    if (pg_num_rows($result) > 0) $albaraneslin = pg_fetch_all($result);
    for ($j = 0; $j < count($albaraneslin); $j++) {
        if (intval($albaraneslin[$j]['unidad_medida']) == 4) $cantidad = floatval($albaraneslin[$j]['cantidad']);
        else $cantidad = floatval($albaraneslin[$j]['metros']);
        $result = pg_execute($dbconn, 'unidadesmedida_albaranlin', array(intval($albaraneslin[$j]['unidad_medida'])));
        if (pg_num_rows($result) <= 0) incoLogWrite('[unidades_medida] No hay resultado');
        $unidad_medida = trim(pg_fetch_all($result)[0]['abreviatura']);
        $unidad_medida = str_replace('2', html_entity_decode('&sup2;', ENT_COMPAT, 'UTF-8'), $unidad_medida);
        $descripcion = trim($albaraneslin[$j]['descripcion_comercial']);
        $posicion = intval($albaraneslin[$j]['posicion']);
        switch ($posicion) {
            case 1: $descripcion .= ' POSICIÓN CUBIERTA'; break;
            case 2: $descripcion .= ' POSICIÓN FACHADA'; break;
        }
        $descripcion = $tr->getTranslate($descripcion);
        $descripcion = trim($descripcion . ' ' . trim($albaraneslin[$j]['referencia_plano']));
        $precio = floatval($albaraneslin[$j]['precio']);
        if (floatval($albaraneslin[$j]['descuento']) == 0) {
            $descuento = '';
        } else {
            $descuento = floatval($albaraneslin[$j]['descuento']);
            $descuento = number_format($descuento, 2, ',', '.');
        }
        $importe = floatval($albaraneslin[$j]['importe']);

        if ($cantidad == 0 || $importe == 0) {
            continue;
        }

        // $lines = ceil($pdf->GetStringWidth($descripcion) / 110);
        // $height = $lines * 4;

        $aux = explode(' | ', $descripcion);
        if (count($aux) > 1) {
            $descripcion = str_replace(' | ', PHP_EOL, $descripcion);
        }
        $obs = trim($albaraneslin[$j]['observaciones']);
        if (!is_null($obs) && !empty($obs)) {
            $descripcion .= PHP_EOL . $obs;
        }
        $height = ceil($pdf->getStringHeight(115, $descripcion, true));

        if ($pdf->GetY() + $height >= $maxHeight) {
            $pdf->Cell(25, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(115, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(20, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(15, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(0, $maxHeight - $pdf->GetY(), '', 'LRB', 1);
            $pdf->AddPage();
            $pdf->Cell(25, 4, $tr->getTranslate('b_1', true), 1, 0, 'C');
            $pdf->Cell(115, 4, $tr->getTranslate('b_2', true), 1, 0, 'C');
            $pdf->Cell(20, 4, $tr->getTranslate('b_3', true), 1, 0, 'C');
            $pdf->Cell(15, 4, '% ' . $tr->getTranslate('b_4', true), 1, 0, 'C');
            $pdf->Cell(0, 4, $tr->getTranslate('b_5', true), 1, 1, 'C');
        }

        $pdf->Cell(18, $height, number_format($cantidad, 2, ',', '.'), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell(7, $height, $unidad_medida, 'R', 0, 'C', false, '', 0, false, 'T', 'T');
        $pdf->MultiCell(115, $height, $descripcion, 'LR', 'L', false, 0);
        $pdf->Cell(20, $height, number_format($precio, 2, ',', '.'), 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell(15, $height, $descuento, 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell(0, $height, number_format($importe, 2, ',', '.'), 'LR', 1, 'R', false, '', 0, false, 'T', 'T');
    }

    if ($albaranes[$i]['total_portes'] > 0) {
        if ($albaranes[$i]['portes'] == 0 || $albaranes[$i]['portes'] == 3) $texto = $tr->getTranslate('b_10', true);
        if ($albaranes[$i]['portes'] == 1) $texto = $tr->getTranslate('b_11', true);
        /*$trans = trim($oferta['transporte']);
        if (!empty($trans) && !is_null($trans)) {
            $texto = $trans;
        }*/
        // $lines = ceil($pdf->GetStringWidth($texto) / 113);
        // $height = $lines * 4;
        $height = ceil($pdf->getStringHeight(113, $texto, true));

        $pdf->Cell(18, $height, '1', 'L', 0, 'R');
        $pdf->Cell(7, $height, '', 'R', 0, 'C');
        // $pdf->Cell(115, $height, $txt, 'LR', 0);
        $pdf->MultiCell(115, $height, $texto . PHP_EOL, 'LR', 'L', false, 0);
        $pdf->Cell(20, $height, '', 'LR', 0, 'R');
        $pdf->Cell(15, $height, '', 'LR', 0, 'R');
        $pdf->Cell(0, $height, number_format($albaranes[$i]['total_portes'], 2, ',', '.'), 'LR', 1, 'R');
    }

    if (count($albaranes) > 1 && $i != count($albaranes) - 1) {
        $pdf->Cell(25, 4, '', 'LR', 0, 'C');
        $pdf->Cell(115, 4, '', 'LR', 0, 'C');
        $pdf->Cell(20, 4, '', 'LR', 0, 'C');
        $pdf->Cell(15, 4, '', 'LR', 0, 'C');
        $pdf->Cell(0, 4, '', 'LR', 1, 'C');
    }
}

if (floatval($factura['anticipo_con_factura']) > 0 && !is_null($factura['factura_anticipado'])) {
	// anticipo facturado
	$pdf->Cell(25, 4, '', 'LR', 0, 'C');
	$pdf->Cell(115, 4, '', 'LR', 0, 'C');
	$pdf->Cell(20, 4, '', 'LR', 0, 'C');
	$pdf->Cell(15, 4, '', 'LR', 0, 'C');
	$pdf->Cell(0, 4, '', 'LR', 1, 'C');
	$numFactura = intval(substr($factura['factura_anticipado'], -7));
	/*if (boolval($factura['iva'])) {
        $anticipo = floatval($factura['anticipo_con_factura'] / (1 + ($factura['tipo_iva_0'] / 100))) * -1;
    } else {
        $anticipo = floatval($factura['anticipo_con_factura']) * -1;
    }*/
    $anticipo = floatval($factura['anticipo_con_factura']) * -1;
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, $tr->getTranslate('b_12', true) . ' ' . $numFactura, 'LR', 0);
    $pdf->Cell(20, 4, '', 'LR', 0, 'R');
    $pdf->Cell(15, 4, '', 'LR', 0, 'R');
    $pdf->Cell(0, 4, number_format($anticipo, 2, ',', '.'), 'LR', 1, 'R');
}

$txtFactura = trim($oferta['texto_factura']);
if (!is_null($txtFactura) && !empty($txtFactura)) {
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, '', 'LR', 0, 'C');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');

    $txt = $tr->getTranslate($txtFactura);
    /*$lines = ceil($pdf->GetStringWidth($txt) / 113);
    $height = $lines * 4;*/
    $height = ceil($pdf->getStringHeight(115, $txt, true));

    $pdf->Cell(25, $height, '', 'LR', 0, 'R');
    // $pdf->Cell(115, $height, $txt, 'LR', 0);
    $pdf->MultiCell(115, $height, $txt . PHP_EOL, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, '', 'LR', 0, 'R');
    $pdf->Cell(15, $height, '', 'LR', 0, 'R');
    $pdf->Cell(0, $height, '', 'LR', 1, 'R');
}

$pdf->Cell(25, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(115, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(20, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(15, $maxHeight - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(0, $maxHeight - $pdf->GetY(), '', 'LRB', 1);

// 245

if ($logos) {
    $dir = INCO_DIR_CLIENTES . $cliente['codigo'] . '/Facturas/';
} else {
    $dir = INCO_DIR_CLIENTES . $cliente['codigo'] . '/FacturasSinLogos/';
}
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
// $fileName = 'F_' . str_pad($numero, 6, '0', STR_PAD_LEFT) . '.pdf';
$fileName = 'F_' . $factura['ejercicio'] . '_' . str_pad($factura['numero'], 6, '0', STR_PAD_LEFT) . '_' . $idioma . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

// copiar $output en carpeta contabilidad con nombre E[año:1][serie:1][numero:7]
if ($logos) {
    $serie = (trim($factura['serie']) == 'FR') ? 'R' : 0;
    $fileNameCont = INCO_DIR_FACTURAS_EMITIDAS . 'E' . substr($factura['ejercicio'], -1, 1) . $serie . str_pad($factura['numero'], 7, '0', STR_PAD_LEFT) . '.pdf';
    copy($output, $fileNameCont);
}

// pg_free_result($result);
pg_close($dbconn);