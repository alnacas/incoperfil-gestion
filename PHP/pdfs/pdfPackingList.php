<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$idioma = empty($argv[3]) ? '' : $argv[3];
// $tr = new IncoTranslator($DB_STRING, $idioma);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params('SELECT ejercicio, numero FROM albaranes WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [albaran]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaran] No hay resultado');
$albaran = pg_fetch_all($result)[0];

// obtenemos el packignlist
$result = pg_query_params('SELECT cliente, fecha, nombre, direccion, codigo_postal, poblacion, provincia, nif, tipo_contenedor, numero_contenedor, precinto, total_paquetes, total_peso FROM packinglist WHERE albaran = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [packinglist]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[packinglist] No hay resultado');
$packinglist = pg_fetch_all($result)[0];

$result = pg_query_params('SELECT f.identificador, f.serie, f.numero FROM albaranes a, facturas f WHERE a.identificador = $1 AND a.factura = f.identificador LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [packinglist - factura]: ' . pg_last_error());
// if (pg_num_rows($result) <= 0) incoLogWrite('[packinglist - factura] No hay resultado');
$factura = pg_num_rows($result) <= 0 ? ['identificador' => '', 'serie' => '', 'numero' => ''] : pg_fetch_all($result)[0];

// obtenemos las lineas del packinglist
/*$result = pg_query_params('SELECT descripcion, paquete, max(longitud) AS longitud, sum(peso) AS peso FROM packinglistlin WHERE albaran = $1 GROUP BY paquete, descripcion ORDER BY paquete', array($identificador)) or incoLogWrite('La consulta fallo [packinglistlin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[packinglistlin] No hay resultado');
$packinglistlin = pg_fetch_all($result);*/

$result = pg_query_params('SELECT descripcion, paquete, cantidad, longitud, peso FROM packinglistlin WHERE albaran = $1 ORDER BY paquete, longitud DESC', array($identificador)) or incoLogWrite('La consulta fallo [packinglistlin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[packinglistlin] No hay resultado');
$packinglistlin = pg_fetch_all($result);

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_pl', 'SELECT idioma FROM clientes WHERE codigo = $1 LIMIT 1');
$result = pg_execute($dbconn, 'clientes_pl', array($packinglist['cliente']));
if (pg_num_rows($result) <= 0) incoLogWrite('[clientes_pl] No hay resultado');
$idioma = trim(pg_fetch_all($result)[0]['idioma']);

$date = DateTime::createFromFormat('Y-m-d', $packinglist['fecha']);

$cliente = array();
$cliente['codigo'] = $packinglist['cliente'];
$cliente['nif'] = trim($packinglist['nif']);
$cliente['nombre'] = trim($packinglist['nombre']);
$cliente['direccion'] = trim($packinglist['direccion']);
$cliente['codpo'] = $packinglist['codigo_postal'];
$cliente['poblacion'] = trim($packinglist['poblacion']);
$cliente['provincia'] = trim($packinglist['provincia']);
$cliente['idioma'] = $idioma;
incoI18nSetLang($idioma, 'packing_list');

$container = array();
$container['ref'] = $albaran['numero'];
$container['tipo'] = trim($packinglist['tipo_contenedor']);
$container['numero'] = trim($packinglist['numero_contenedor']);

class PDF extends TCPDF {

	/**
	 * @var array
	 */
	private $cliente;

	/**
	 * @var array
	 */
	private $container;

	/**
	 * @var DateTime
	 */
	private $fecha;

    /**
     * @var array
     */
    public $factura;

	/**
	 * @param array $cliente
	 */
	public function setCliente(array $cliente)
	{
		$this->cliente = $cliente;
	}

	/**
	 * @param DateTime $fecha
	 */
	public function setFecha(DateTime $fecha)
	{
		$this->fecha = $fecha;
	}

	/**
	 * @param array $container
	 */
	public function setContainer(array $container)
	{
		$this->container = $container;
	}

	function Header()
	{
        $this->SetY(5);
        $this->SetFont('dejavusans', 'B', 22);
        $this->Cell(0, 4, gettext('PACKING LIST'), 0, 1, 'R');
        // $this->Cell(50, 4, str_pad($this->numero, 5, '0', STR_PAD_LEFT), 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 4, sprintf(gettext('Hoja: %d'), $this->PageNo()), 0, 1, 'R');
        $this->Ln();
        $y = $this->GetY();

        // $this->SetY(7);
        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 5, 95, 0, 'jpg', '', 'C', false, 600);

        $this->SetY($y);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(75, 4, gettext('EXPEDIDOR / CARGADOR'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->setCellPaddings('1', '', '', '');
        $this->Cell(75, 4, 'INGENIERIA Y CONSTRUCCIÓN DEL PERFIL, S.A.', 'LR', 1, 'L');
        $this->Cell(75, 4, 'Carrer Nou 16. - P.I.Mas del Polio', 'LR', 1, 'L');
        $this->Cell(75, 4, '46469 - BENIPARREL (Valencia)', 'LR', 1, 'L');
        $this->Cell(75, 4, 'Tel. 961 211 778 - Fax. 931 211 504', 'LRB', 1, 'L');

        $this->SetY($y);
        $this->SetFont('dejavusans', 'B', 8);
        $this->SetX(90);
        $this->Cell(0, 4, gettext('CLIENTE / CONSIGNATARIO'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->SetX(90);
        $this->Cell(20, 4, $this->cliente['codigo'], 'LT', 0, 'C');
        $this->Cell(0, 4, $this->cliente['nombre'], 'TR', 1);
        $this->SetX(90);
        // $this->Cell(20, 4, '', 'L', 0);
        $this->Cell(0, 4, $this->cliente['direccion'], 'LR', 1);
        $this->SetX(90);
        // $this->Cell(20, 4, '', 'L', 0);
        $this->Cell(90, 4, $this->cliente['codpo'] . ' ' . $this->cliente['poblacion'], 'L', 0);
        $this->Cell(0, 4, $this->cliente['nif'], 'R', 1);
        $this->SetX(90);
        // $this->Cell(20, 4, '', 'LB', 0);
        if (strtoupper($this->cliente['poblacion']) == strtoupper($this->cliente['provincia'])) {
            $this->Cell(0, 4, '', 'LRB', 1);
        } else {
            $this->Cell(0, 4, $this->cliente['provincia'], 'LRB', 1);
        }
        $this->Ln(2);

        $this->SetFont('dejavusans', 'B', 8);
        $this->SetFillColor(230, 230, 230);
        $this->SetCellPadding(1);
        $this->setCellPaddings('1', '', '', '');
        $serie = (trim($this->factura['serie']) == 'FR') ? 'R' : 0;
        $this->Cell(75, 4, sprintf(gettext('Albarán: %d'), $this->container['ref']), 0, 0, 'L', true);
        $this->Cell(10, 4, '', 0, 0, 'L', true);
        $this->Cell(0, 4, sprintf(gettext('Tipo de contenedor: %s'), $this->container['tipo']), 0, 1, '', true);
        $this->Cell(75, 4, sprintf(gettext('Factura: %s'), $serie . '/' . $this->factura['numero']), 0, 0, 'L', true);
        $this->Cell(10, 4, '', 0, 0, 'L', true);
        $this->Cell(0, 4, sprintf(gettext('Número de contenedor: %s'), $this->container['numero']), 0, 1, '', true);
        $this->Ln(2);

        // $this->SetX(90);
        // $this->MultiCell(0, 12, $this->observaciones, 0, 'L', false, 1);

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('4', '0', '0', '0');

        $this->SetFont('dejavusans', '', 8);
        $this->Cell(10, 4, gettext('PAQ.'), 1, 0, 'C');
        $this->Cell(135, 4, gettext('DESCRIPCIÓN'), 'BT', 0, 'L');
        $this->Cell(20, 4, gettext('CANTIDAD'), 'BT', 0, 'R');
        $this->Cell(15, 4, gettext('LONG'), 'BT', 0, 'R');
        $this->Cell(20, 4, gettext('KGS TOTAL'), 'BTR', 1, 'R');
	}

	public function Footer() {
		$this->SetY(-52);
		$this->SetFont('dejavusans', '', 8);
		$this->setCellPaddings('2', '0', '0', '2');
		$this->Cell(120, 36, gettext('Firma:'), '1', 1, 'L', false, '', 0, false, 'T', 'B');
		$this->Ln(4);

		$this->setCellPaddings('0', '0', '0', '0');
		$this->SetFont('dejavusans', '', 6);
		$this->Cell(0, 3, gettext('Inscrita en el Registro Mercantil de Valencia, Folio 127 Tomo 4.361 Libro 1.673 Sección Gral. Hoja V-22.870 Inscripcion 4. NIF A46265526'), 0, 1, 'C');

		$this->SetFont('dejavusans', '', 8);
		$this->SetXY(140, -36);
		$this->SetFillColor(230, 230, 230);
		$this->SetCellPadding(1);
		$this->Cell(10, 4, gettext('Fecha:'), 0, 0, 'L', true);
		$this->Cell(30, 4, $this->fecha->format('d/m/Y'), 0, 1, 'C', true);
	}
}

$pdf = new PDF();
$pdf->setCliente($cliente);
$pdf->setContainer($container);
$pdf->setFecha($date);
$pdf->factura = $factura;

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('PACKING LIST');
$pdf->SetTitle('Packing List');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 62);
$pdf->SetAutoPageBreak(true, $pdf->getPageHeight() - 240);
$pdf->AddPage();

/*$pdf->Cell(10, 4, gettext('b_1'), 1, 0, 'C');
$pdf->Cell(135, 4, gettext('b_2'), 'BT', 0, 'L');
$pdf->Cell(20, 4, gettext('b_3'), 'BT', 0, 'R');
$pdf->Cell(15, 4, gettext('b_4'), 'BT', 0, 'R');
$pdf->Cell(20, 4, gettext('b_5'), 'BTR', 1, 'R');*/

$pdf->SetFontSize(7);
$peso = 0;
for ($i = 0; $i < count($packinglistlin); $i++) {
    $descripcion = IncoTranslatorFix::translate($dbconn, trim($packinglistlin[$i]['descripcion']), $idioma);

    $height = $pdf->getStringHeight(135, $descripcion, true);
    if ($height < 4) $height = 4;
	
	if (abs(240 - ($pdf->GetY() + $height)) < 4) {
    	$pdf->Cell(10, 240 - $pdf->GetY(), '', 'LRB', 0);
		$pdf->Cell(115, 240 - $pdf->GetY(), '', 'B', 0);
		$pdf->Cell(20, 240 - $pdf->GetY(), '', 'B', 0);
		$pdf->Cell(20, 240 - $pdf->GetY(), '', 'B', 0);
		$pdf->Cell(20, 240 - $pdf->GetY(), '', 'B', 0);
		$pdf->Cell(15, 240 - $pdf->GetY(), '', 'RB', 1);
    }
    
    $pdf->Cell(10, $height, $packinglistlin[$i]['paquete'], 'LR', 0, 'L');
    $pdf->MultiCell(135, $height, $descripcion . PHP_EOL, 'L', 0, false, 0);
    $pdf->Cell(20, $height, $packinglistlin[$i]['cantidad'], '', 0, 'R');
    $pdf->Cell(15, $height, $packinglistlin[$i]['longitud'], '', 0, 'R');
    $pdf->Cell(20, $height, number_format($packinglistlin[$i]['peso'], 2, ',', '.'), 'R', 1, 'R');
    
	$peso += $packinglistlin[$i]['peso'];
}
if ($peso != $packinglist['total_peso']) $peso = $packinglist['total_peso'];

$pdf->Cell(10, 4, '', 'LR', 0);
$pdf->Cell(115, 4, '', 0, 0);
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(15, 4, '', 'R', 1);

$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(10, 4, '', 'LR', 0);
$pdf->Cell(45, 4, '', 0, 0);
$pdf->Cell(70, 4, gettext('TOTAL NUMERO DE BULTOS'), 0, 0);
$pdf->Cell(20, 4, $packinglist['total_paquetes'], 0, 0, 'R');
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(15, 4, '', 'R', 1);

$pdf->Cell(10, 4, '', 'LR', 0);
$pdf->Cell(45, 4, '', 0, 0);
$pdf->Cell(70, 4, gettext('TOTAL PESO KGS. PAQ.'), 0, 0);
$pdf->Cell(20, 4, number_format($peso, 2, ',', '.'), 0, 0, 'R');
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(15, 4, '', 'R', 1);

$pdf->Cell(10, 4, '', 'LR', 0);
$pdf->Cell(45, 4, '', 0, 0);
$pdf->Cell(70, 4, gettext('NUMERO DE PRECINTO'), 0, 0);
$pdf->Cell(20, 4, trim($packinglist['precinto']), 0, 0, 'R');
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(20, 4, '', 0, 0);
$pdf->Cell(15, 4, '', 'R', 1);

$pdf->Cell(10, 240 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(115, 240 - $pdf->GetY(), '', 'B', 0);
$pdf->Cell(20, 240 - $pdf->GetY(), '', 'B', 0);
$pdf->Cell(20, 240 - $pdf->GetY(), '', 'B', 0);
$pdf->Cell(20, 240 - $pdf->GetY(), '', 'B', 0);
$pdf->Cell(15, 240 - $pdf->GetY(), '', 'RB', 1);

$dir = INCO_DIR_CLIENTES . $cliente['codigo'] . '/PackingList/';
if (!is_dir($dir)) {
	$oldmask = umask(0);
	mkdir($dir, 0755, true);
	umask($oldmask);
}
$fileName = 'PL_' . $albaran['ejercicio'] . '_' . str_pad($albaran['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
// $fileName = 'PL_2017_000000.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);