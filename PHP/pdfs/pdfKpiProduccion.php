<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$year_a = $argv[2];
$year_b = $argv[3];

$opciones_keys = ['mostrar_mes_curso'];
$opciones_val = explode(':', $argv[4]); // desglosar
$opciones = array_combine($opciones_keys, $opciones_val);

$currentYear = intval((new DateTime())->format('Y'));
$currentMonth = intval((new DateTime())->format('m'));
if ( boolval( $opciones['mostrar_mes_curso'] ) ) {
	$showUntilCurrentMonth = true;
	$currentMonth++;
} else {
	$showUntilCurrentMonth = ( $year_a == $currentYear || $year_b == $currentYear );
}

function getResult($dbconn, $year) {
	global $showUntilCurrentMonth, $currentMonth;

	$stmts = ['perfilado_m', 'perfilado_min', 'curvado_m', 'curvado_min', 'tiempo_ciclo', 'tiempo_paro_total', 'tiempo_paro', 'nc', 'pedidos'];
	$result = [];

	foreach ( $stmts as $stmt ) {
		$arr = [];
		$pg_result = pg_execute($dbconn, $stmt, [$year]) or incoLogWrite( 'La consulta fallo [config - ' . $stmt . ']');
		$pg_result = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($pg_result as $pg_res) {
			if ($showUntilCurrentMonth && $pg_res['month'] >= $currentMonth) continue;
			/*if (!isset($result[$stmt][$pg_res['month']])) $result[$stmt][$pg_res['month']] = 0;
			$result[$stmt][$pg_res['month']] += $pg_res['value'];*/
			if (!isset($arr[$pg_res['month']])) $arr[$pg_res['month']] = 0;
			$arr[$pg_res['month']] += $pg_res['value'];
		}
		$result[$stmt] = $arr;
	}

	$pg_result = pg_execute($dbconn, 'objetivos', [$year]) or incoLogWrite('La consulta fallo [config - objetivos]');
	$result['objetivos'] = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result)[0];

	$result['name'] = 'Año ' . $year;
	$result['year'] = intval($year);
	return $result;
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

pg_prepare($dbconn, 'perfilado_m', 'select extract(month from dc.fecha) as month, sum(dc.metros_lineales) as value from diario_consumo dc, productos p where extract(year from dc.fecha) = $1 and dc.consume_bobina is true and p.seccion = 1 and dc.producto = p.codigo group by month order by month');
pg_prepare($dbconn, 'perfilado_min', 'select extract(month from dt.fecha) as month, sum(dt.tiempo) as value from diario_tiempo dt where extract(year from dt.fecha) = $1 and dt.actividad in (7, 8, 17, 18) and dt.nave = \'B\' and dt.maquina in (0, 1, 2, 9, 15, 16, 20) group by month order by month');
pg_prepare($dbconn, 'curvado_m', 'select extract(month from dc.fecha) as month, sum(dc.metros_lineales) as value from diario_consumo dc, productos p, procesos_producto pp where extract(year from dc.fecha) = $1 and dc.consume_bobina is true and p.seccion = 1 and pp.proceso = 4 and p.codigo = pp.producto and dc.producto = p.codigo group by month order by month');
pg_prepare($dbconn, 'curvado_min', 'select extract(month from dt.fecha) as month, sum(dt.tiempo) as value from diario_tiempo dt where extract(year from dt.fecha) = $1 and dt.actividad = 6 and dt.nave = \'B\' group by month order by month');
pg_prepare($dbconn, 'tiempo_ciclo', 'select extract(month from afecha) as month, avg(dias) as value from (select p.numero, a.fecha as afecha, min(a.fecha) - min(p.fecha_validado) as dias from pedidos p, albaranes a, albaraneslin al where p.ejercicio = $1 and p.identificador = al.identificador_pedido and al.identificador = a.identificador group by p.numero, afecha order by p.numero) as aux group by month order by month');
pg_prepare($dbconn, 'tiempo_paro_total', 'select extract(month from dt.fecha) as month, sum(dt.tiempo) as value from diario_tiempo dt where extract(year from dt.fecha) = $1 group by month order by month');
pg_prepare($dbconn, 'tiempo_paro', 'select extract(month from dt.fecha) as month, sum(dt.tiempo) as value from diario_tiempo dt where extract(year from dt.fecha) = $1 and dt.actividad in (24, 39, 40) group by month order by month');
pg_prepare($dbconn, 'objetivos', 'select perfilado_m, perfilado_min, perfilado_m_min, curvado_m, curvado_min, curvado_m_min, tiempo_ciclo, tiempo_paro, nc from kpi_produccion_objetivos where year = $1');
pg_prepare($dbconn, 'pedidos', 'select extract(month from fecha) as month, count(*) as value from pedidos where extract(year from fecha) = $1 group by month order by month');
// pg_prepare($dbconn, 'nc', 'select mes as month, val as value from kpi_nc_datos where year = $1 and area = 2 order by mes');
pg_prepare($dbconn, 'nc', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and kpi = \'prod\' and estado != 2 group by month order by month');

$result_a = getResult($dbconn, $year_a);
$result_b = getResult($dbconn, $year_b);

class KPI_Produccion extends TCPDF
{
	/**
	 * @var int
	 */
	public $currentMonth;

	/**
	 * @var int
	 */
	public $currentYear;

	/**
	 * @var array
	 */
	public $legend;

	function Header()
	{
		$this->SetFont('dejavusans', 'B', 14);
		$this->SetY(10);
		$this->Cell(0, 8, 'KPI Producción', 0, 1, 'C');
		$this->SetFont('dejavusans', '', 8);
		$this->Ln(4);
		$this->WriteLegend();
	}

	function Footer() {
		$this->SetY(-18);

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 3;

		$this->SetFont('dejavusans', '', 8);
		$this->Cell($wCell, 4, 'Código: FCA-01-07', 0, 0, 'L');
		$this->Cell($wCell, 4, 'Revisión: 2', 0, 0, 'C');
		$this->Cell($wCell, 4, 'Fecha: 16/12/2019', 0, 1, 'R');
	}

	private function WriteLegend() {
		foreach ( $this->legend as $item ) {
			$this->SetFont('dejavusans', 'B');
			$this->Cell(30, 4, $item['title'], 0, 0, 'L');
			$this->SetFont('dejavusans', '');
			$this->Cell(0, 4, $item['value'], 0, 1, 'L');
		}
	}

	public function WriteTable( array $result_a, array $result_b ) {
		$h = 4.5;

		if (empty($result_a['perfilado_m']) && empty($result_b['perfilado_m'])) {
			$this->Cell(0, $h, 'No hay datos para comparar', 0, 1);
			return;
		}

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 19;

		$rows = [
			[
				['with_mult' => 1, 'txt' => 'Nombre', 'align' => 'L'],
				['with_mult' => 9, 'txt' => '07 Productividad Perfilado', 'align' => 'C'],
				['with_mult' => 9, 'txt' => '08 Productividad Perfilado', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Req.', 'align' => 'L'],
				['with_mult' => 9, 'txt' => 'Tiempos entrega Reducidos', 'align' => 'C'],
				['with_mult' => 9, 'txt' => 'Tiempos entrega Reducidos', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Proceso', 'align' => 'L'],
				['with_mult' => 9, 'txt' => 'PPR 01 Produccion', 'align' => 'C'],
				['with_mult' => 9, 'txt' => 'PPR 01 Produccion', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Cálculo', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Perfilado (m)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Perfilado (min)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Perfilado (m/min)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Curvado (m)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Curvado (min)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Curvado (m/min)', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Mes', 'align' => 'L'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
			]
		];

		$this->SetFont('dejavusans', 'B');
		foreach ( $rows as $row ) {
			foreach ($row as $col) {
				$this->Cell($wCell * $col['with_mult'], $h, $col['txt'], 1, 0, $col['align']);
			}
			$this->Ln();
		}
		$this->SetFont('dejavusans', '');

		$totEficienciaPerfiladoA = [];
		$totEficienciaPerfiladoB = [];
		$totEficienciaCurvadoA = [];
		$totEficienciaCurvadoB = [];

		for ($i = 0; $i < 12; $i++) {
			$month = $i + 1;

			if (($result_a['year'] == $this->currentYear || $result_b['year'] == $this->currentYear) && $month >= $this->currentMonth) {
				for ($j = 0; $j < count(end($rows)); $j++) {
					$this->Cell($wCell, $h, '', 1, 0, 'C');
				}
				$this->Ln();
				continue;
			}

			$perfiladoA = $this->getValue($result_a['perfilado_m'], $month);
			$perfiladoB = $this->getValue($result_b['perfilado_m'], $month);
			$perfiladoAB = $this->getPercentage($perfiladoA, $perfiladoB, -1);
			$perfiladoMinA = $this->getValue($result_a['perfilado_min'], $month);
			$perfiladoMinB = $this->getValue($result_b['perfilado_min'], $month);
			$perfiladoMinAB = $this->getPercentage($perfiladoMinA, $perfiladoMinB, -1);
			$eficienciaPerfiladoA = $perfiladoMinA == 0 ? 0 : $perfiladoA / $perfiladoMinA;
			array_push($totEficienciaPerfiladoA, $eficienciaPerfiladoA);
			$eficienciaPerfiladoB = $perfiladoMinB == 0 ? 0 : $perfiladoB / $perfiladoMinB;
			array_push($totEficienciaPerfiladoB, $eficienciaPerfiladoB);
			$eficienciaPerfiladoAB = $this->getPercentage($eficienciaPerfiladoA, $eficienciaPerfiladoB, -1);
			$curvadoA = $this->getValue($result_a['curvado_m'], $month);
			$curvadoB = $this->getValue($result_b['curvado_m'], $month);
			$curvadoAB = $this->getPercentage($curvadoA, $curvadoB, -1);
			$curvadoMinA = $this->getValue($result_a['curvado_min'], $month);
			$curvadoMinB = $this->getValue($result_b['curvado_min'], $month);
			$curvadoMinAB = $this->getPercentage($curvadoMinA, $curvadoMinB, -1);
			$eficienciaCurvadoA = $curvadoMinA == 0 ? 0 : $curvadoA / $curvadoMinA;
			array_push($totEficienciaCurvadoA, $eficienciaCurvadoA);
			$eficienciaCurvadoB = $curvadoMinB == 0 ? 0 : $curvadoB / $curvadoMinB;
			array_push($totEficienciaCurvadoB, $eficienciaCurvadoB);
			$eficienciaCurvadoAB = $this->getPercentage($eficienciaCurvadoA, $eficienciaCurvadoB, -1);

			$this->Cell($wCell, $h, $month, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($perfiladoA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($perfiladoB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($perfiladoAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($perfiladoMinA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($perfiladoMinB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($perfiladoMinAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($eficienciaPerfiladoA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($eficienciaPerfiladoB, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaPerfiladoAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($curvadoA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($curvadoB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($curvadoAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($curvadoMinA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($curvadoMinB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($curvadoMinAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($eficienciaCurvadoA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($eficienciaCurvadoB, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaCurvadoAB), 1, 1, 'C');
		}

		$totPerfiladoA = array_sum($result_a['perfilado_m']);
		$totPerfiladoB = array_sum($result_b['perfilado_m']);
		$totPerfiladoAB = $this->getPercentage($totPerfiladoA, $totPerfiladoB, -1);
		$totPerfiladoMinA = array_sum($result_a['perfilado_min']);
		$totPerfiladoMinB = array_sum($result_b['perfilado_min']);
		$totPerfiladoMinAB = $this->getPercentage($totPerfiladoMinA, $totPerfiladoMinB, -1);
		$totEficienciaPerfiladoA = array_sum($totEficienciaPerfiladoA) / count($totEficienciaPerfiladoA);
		$totEficienciaPerfiladoB = array_sum($totEficienciaPerfiladoB) / count($totEficienciaPerfiladoB);
		$totEficienciaPerfiladoAB = $this->getPercentage($totEficienciaPerfiladoA, $totEficienciaPerfiladoB, -1);
		$totCurvadoA = array_sum($result_a['curvado_m']);
		$totCurvadoB = array_sum($result_b['curvado_m']);
		$totCurvadoAB = $this->getPercentage($totCurvadoA, $totCurvadoB, -1);
		$totCurvadoMinA = array_sum($result_a['curvado_min']);
		$totCurvadoMinB = array_sum($result_b['curvado_min']);
		$totCurvadoMinAB = $this->getPercentage($totCurvadoMinA, $totCurvadoMinB, -1);
		$totEficienciaCurvadoA = array_sum($totEficienciaCurvadoA) / count($totEficienciaCurvadoA);
		$totEficienciaCurvadoB = array_sum($totEficienciaCurvadoB) / count($totEficienciaCurvadoB);
		$totEficienciaCurvadoAB = $this->getPercentage($totEficienciaCurvadoA, $totEficienciaCurvadoB, -1);
		$this->Cell($wCell, $h, 'Totales', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totPerfiladoA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totPerfiladoB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totPerfiladoAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totPerfiladoMinA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totPerfiladoMinB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totPerfiladoMinAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totEficienciaPerfiladoA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totEficienciaPerfiladoB, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaPerfiladoAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totCurvadoA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totCurvadoB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totCurvadoAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totCurvadoMinA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totCurvadoMinB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totCurvadoMinAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totEficienciaCurvadoA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totEficienciaCurvadoB, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaCurvadoAB), 1, 1, 'C');

		$this->Cell($wCell, $h, 'Media', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 1, 'C');

		$this->Cell($wCell, $h, 'Objs.', 1, 0, 'C');
		$obj_fields = ['perfilado_m', 'perfilado_min', 'perfilado_m_min', 'curvado_m', 'curvado_min', 'curvado_m_min'];
		foreach ( $obj_fields as $obj_field ) {
			if (!isset($result_a['objetivos'][$obj_field])) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				continue;
			}

			if (strpos($result_a['objetivos'][$obj_field], '%') != false) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
			} else {
				$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
				$this->Cell($wCell, $h, $objB, 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
			}
		}
		$this->Ln();
		$this->Ln();

		$this->checkPageBreak($h * 16);

		$rows = [
			[
				['with_mult' => 1, 'txt' => 'Nombre', 'align' => 'L'],
				['with_mult' => 3, 'txt' => '10 Tiempo Ciclo', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '14 Tiempo Paro (%)', 'align' => 'C'],
				['with_mult' => 5, 'txt' => '09 NC y RQ', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Req.', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Tiempo Entrega Reducido', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Medios Prod. Adecuados', 'align' => 'C'],
				['with_mult' => 5, 'txt' => 'Calidad Producto', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Proceso', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'PPR 04 Preservación', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PPR 02 Mantenimiento', 'align' => 'C'],
				['with_mult' => 5, 'txt' => 'PPR 01 Producción', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Cálculo', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Tiempo Ciclo (días)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Tiempo Paro (%)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'NC (ud)', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'NC / Ped (%)', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Mes', 'align' => 'L'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
			]
		];

		$this->SetFont('dejavusans', 'B');
		foreach ( $rows as $row ) {
			foreach ($row as $col) {
				$this->Cell($wCell * $col['with_mult'], $h, $col['txt'], 1, 0, $col['align']);
			}
			$this->Ln();
		}
		$this->SetFont('dejavusans', '');

		$totTiempoParoA = [];
		$totTiempoParoB = [];
		$totNcPedidosA = [];
		$totNcPedidosB = [];
		for ($i = 0; $i < 12; $i++) {
			$month = $i + 1;

			if (($result_a['year'] == $this->currentYear || $result_b['year'] == $this->currentYear) && $month >= $this->currentMonth) {
				for ($j = 0; $j < count(end($rows)); $j++) {
					$this->Cell($wCell, $h, '', 1, 0, 'C');
				}
				$this->Ln();
				continue;
			}

			$tiempoParoA = $this->getPercentage($this->getValue($result_a['tiempo_paro'], $month), $this->getValue($result_a['tiempo_paro_total'], $month));
			array_push($totTiempoParoA, $tiempoParoA);
			$tiempoParoB = $this->getPercentage($this->getValue($result_b['tiempo_paro'], $month), $this->getValue($result_b['tiempo_paro_total'], $month));
			array_push($totTiempoParoB, $tiempoParoB);
			$tiempoParoAB = $this->getPercentage($tiempoParoA, $tiempoParoB, -1);
			$ncUdA = $this->getValue($result_a['nc'], $month);
			$ncUdB = $this->getValue($result_b['nc'], $month);
			$ncUdAB = $this->getPercentage($ncUdA, $ncUdB, -1);
			$ncPedidosA = $this->getPercentage($ncUdA, $this->getValue($result_a['pedidos'], $month));
			array_push($totNcPedidosA, $ncPedidosA);
			$ncPedidosB = $this->getPercentage($ncUdB, $this->getValue($result_b['pedidos'], $month));
			array_push($totNcPedidosB, $ncPedidosB);
			$tiempoCicloA = $this->getValue($result_a['tiempo_ciclo'], $month);
			$tiempoCicloB = $this->getValue($result_b['tiempo_ciclo'], $month);
			$tiempoCicloAB = $this->getPercentage($tiempoCicloA, $tiempoCicloB, -1);
			$this->Cell($wCell, $h, $month, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($tiempoCicloA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($tiempoCicloB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($tiempoCicloAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($tiempoParoA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($tiempoParoB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($tiempoParoAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $ncUdA, 1, 0, 'C');
			$this->Cell($wCell, $h, $ncUdB, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPedidosA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPedidosB, 2), 1, 1, 'C');
		}

		$totTiempoCicloA = array_sum($result_a['tiempo_ciclo']) / count($result_a['tiempo_ciclo']);
		$totTiempoCicloB = array_sum($result_b['tiempo_ciclo']) / count($result_b['tiempo_ciclo']);
		$totTiempoCicloAB = $this->getPercentage($totTiempoCicloA, $totTiempoCicloB, -1);
		$totTiempoParoA = array_sum($totTiempoParoA) / count($totTiempoParoA);
		$totTiempoParoB = array_sum($totTiempoParoB) / count($totTiempoParoB);
		$totTiempoParoAB = $this->getPercentage($totTiempoParoA, $totTiempoParoB, -1);
		$totNcUdA = array_sum($result_a['nc']);
		$totNcUdB = array_sum($result_b['nc']);
		$totNcUdAB = $this->getPercentage($totNcUdA, $totNcUdB, -1);
		$this->Cell($wCell, $h, 'Totales', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totTiempoCicloA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totTiempoCicloB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totTiempoCicloAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totTiempoParoA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totTiempoParoB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totTiempoParoAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $totNcUdA, 1, 0, 'C');
		$this->Cell($wCell, $h, $totNcUdB, 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNcUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 1, 'C');

		$medNcPedidosA = array_sum($totNcPedidosA) / count($totNcPedidosA);
		$medNcPedidosB = array_sum($totNcPedidosB) / count($totNcPedidosB);
		$this->Cell($wCell, $h, 'Media', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medNcPedidosA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medNcPedidosB, 2), 1, 1, 'C');

		$this->Cell($wCell, $h, 'Objs.', 1, 0, 'C');
		$obj_fields = ['tiempo_ciclo', 'tiempo_paro', 'nc'];
		foreach ( $obj_fields as $obj_field ) {
			if (!isset($result_a['objetivos'][$obj_field])) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				continue;
			}

			if (strpos($result_a['objetivos'][$obj_field], '%') != false) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
			} else {
				$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
				$this->Cell($wCell, $h, $objB, 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
			}
		}
		if ( isset( $result_a['objetivos']['nc_pedidos'] ) ) {
			$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
			$this->Cell( $wCell, $h, $result_a['objetivos']['nc_pedidos'], 1, 0, 'C' );
			$this->Cell( $wCell, $h, $objB, 1, 0, 'C' );
		} else {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
		}
	}

	/**
	 * @param array $arr
	 * @param string|int $key
	 *
	 * @return int|mixed
	 */
	private function getValue(array $arr, $key) {
		return isset($arr[$key]) ? $arr[$key] : 0;
	}

    /**
     * @param float|int $dividendo
     * @param float|int $divisor
     *
     * @return float|int
     */
    private function getDivision($dividendo, $divisor) {
        return $divisor == 0 ? $dividendo : $dividendo / $divisor;
    }

	/**
	 * @param float|int $dividendo
	 * @param float|int $divisor
	 * @param float|int $extra
	 *
	 * @return float|int
	 */
	private function getPercentage($dividendo, $divisor, $extra = 0) {
		$percent = ($divisor == 0 ? $dividendo : $dividendo / $divisor + $extra) * 100;
		// return abs($percent) > 100 ? 100 * ($percent / abs($percent)) : $percent;
        return $percent;
	}

	/**
	 * @param float|int $number
	 *
	 * @return string
	 */
	private function number2str($number, $decimals = 0) {
		return number_format($number, $decimals, ',', '.');
	}

	/**
	 * @param float|int $percent
	 *
	 * @return string
	 */
	private function percent2str($percent, $decimals = 0) {
		return $percent == 0 ? '0 %' : $this->number2str($percent, $decimals) . ' %';
	}
}

$pdf = new KPI_Produccion();
$pdf->currentYear = $currentYear;
$pdf->currentMonth = $currentMonth;
$pdf->legend = [
	['title' => 'Propietario', 'value' => 'Resp. Producción.'],
	['title' => 'Actualización', 'value' => 'Mensual.'],
	['title' => 'Revisión', 'value' => 'Trimestral.'],
	['title' => 'A', 'value' => $result_a['name']],
	['title' => 'B', 'value' => $result_b['name']]
];
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('KPI Producción');
$pdf->SetTitle('KPI Producción');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(10, 47);
$pdf->SetAutoPageBreak(true, 24);
$pdf->AddPage('L');

$pdf->WriteTable($result_a, $result_b);

//SALIDA
$dir = INCO_DIR_KPIS;
if (!is_dir($dir)) {
	$oldmask = umask(0);
	$aux = mkdir($dir, 0755, true);
	umask($oldmask);
}
$fileName = 'KPI_Produccion.pdf';
$output = $dir . $fileName;
if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

pg_close($dbconn);
