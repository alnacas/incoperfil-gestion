<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$year_a = $argv[2];
$year_b = $argv[3];

$opciones_keys = ['mostrar_mes_curso'];
$opciones_val = explode(':', $argv[4]); // desglosar
$opciones = array_combine($opciones_keys, $opciones_val);

$currentYear = intval((new DateTime())->format('Y'));
$currentMonth = intval((new DateTime())->format('m'));
if ( boolval( $opciones['mostrar_mes_curso'] ) ) {
	$showUntilCurrentMonth = true;
	$currentMonth++;
} else {
	$showUntilCurrentMonth = ( $year_a == $currentYear || $year_b == $currentYear );
}

function getResult($dbconn, $year) {
	global $showUntilCurrentMonth, $currentMonth;

	$stmts = ['pesos_medios', 'proveedores_x_oferta', 'nc', 'pedidos'];
	$result = [];

	foreach ( $stmts as $stmt ) {
		$arr = [];
		$pg_result = pg_execute($dbconn, $stmt, [$year]) or incoLogWrite( 'La consulta fallo [config - ' . $stmt . ']');
		$pg_result = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($pg_result as $pg_res) {
			if ($showUntilCurrentMonth && $pg_res['month'] >= $currentMonth) continue;
			/*if (!isset($result[$stmt][$pg_res['month']])) $result[$stmt][$pg_res['month']] = 0;
			$result[$stmt][$pg_res['month']] += $pg_res['value'];*/
			if (!isset($arr[$pg_res['month']])) $arr[$pg_res['month']] = 0;
			$arr[$pg_res['month']] += $pg_res['value'];
		}
		$result[$stmt] = $arr;
	}

	$pg_result = pg_execute($dbconn, 'objetivos', [$year]) or incoLogWrite('La consulta fallo [config - objetivos]');
	$result['objetivos'] = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result)[0];

	$result['name'] = 'Año ' . $year;
	$result['year'] = intval($year);
	return $result;
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

pg_prepare($dbconn, 'pesos_medios', 'select extract(month from fecha_entrada) as month, avg(peso_albaran / 1000) as value from bobinas where extract(year from fecha_entrada) = $1 and proveedor != 125 and (origen is null or origen = 0) and ancho >= 1000 group by month order by month');
pg_prepare($dbconn, 'proveedores_x_oferta', 'select extract(month from fecha) as month, avg(proveedores) as value from (select ob.numero, ob.fecha, count(distinct obl.proveedor) as proveedores from ofertas_bobinas ob, ofertas_bobinaslin obl where ob.ejercicio = $1 and ob.identificador = obl.identificador group by ob.numero, ob.fecha order by ob.numero) as aux group by month order by month');
pg_prepare($dbconn, 'objetivos', 'select peso_medio, prove_x_oferta from kpi_compras_objetivos where year = $1');
pg_prepare($dbconn, 'pedidos', 'select extract(month from fecha) as month, count(*) as value from pedidos where extract(year from fecha) = $1 group by month order by month');
// pg_prepare($dbconn, 'nc', 'select mes as month, val as value from kpi_nc_datos where year = $1 and area = 1 order by mes');
pg_prepare($dbconn, 'nc', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and kpi = \'comp\' and estado != 2 group by month order by month');

$result_a = getResult($dbconn, $year_a);
$result_b = getResult($dbconn, $year_b);


class KPI_Compras extends TCPDF
{
	/**
	 * @var int
	 */
	public $currentMonth;

	/**
	 * @var int
	 */
	public $currentYear;

	/**
	 * @var array
	 */
	public $legend;

	function Header()
	{
		$this->SetFont('dejavusans', 'B', 14);
		$this->SetY(10);
		$this->Cell(0, 8, 'KPI Compras', 0, 1, 'C');
		$this->SetFont('dejavusans', '', 8);
		$this->Ln(4);
		$this->WriteLegend();
	}

	function Footer() {
		$this->SetY(-18);

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 3;

		$this->SetFont('dejavusans', '', 8);
		$this->Cell($wCell, 4, 'Código: FCA-01-07', 0, 0, 'L');
		$this->Cell($wCell, 4, 'Revisión: 2', 0, 0, 'C');
		$this->Cell($wCell, 4, 'Fecha: 16/12/2019', 0, 1, 'R');
	}

	private function WriteLegend() {
		foreach ( $this->legend as $item ) {
			$this->SetFont('dejavusans', 'B');
			$this->Cell(30, 4, $item['title'], 0, 0, 'L');
			$this->SetFont('dejavusans', '');
			$this->Cell(0, 4, $item['value'], 0, 1, 'L');
		}
	}

	public function WriteTable( array $result_a, array $result_b ) {
		$h = 4.5;

		if (empty($result_a['pesos_medios']) && empty($result_b['pesos_medios'])) {
			$this->Cell(0, $h, 'No hay datos para comparar', 0, 1);
			return;
		}

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 16;

		$rows = [
			[
				['with_mult' => 1, 'txt' => 'Nombre', 'align' => 'L'],
				['with_mult' => 3, 'txt' => '11 Pesos Bobinas', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '12 Proveedores Solicitud', 'align' => 'C'],
				['with_mult' => 5, 'txt' => '09 NC y RQ', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Requisito', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Tiempos Entrega', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Precios Competitivos', 'align' => 'C'],
				['with_mult' => 5, 'txt' => 'Calidad Servicio', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Proceso', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'PCO Compras', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PCO Compras', 'align' => 'C'],
				['with_mult' => 5, 'txt' => 'PCO Compras', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Cálculo', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Peso Medio (Tn)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Prov. x Oferta (ud)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'NC Proveedor (ud)', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'NC / Ped. (%)', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Mes', 'align' => 'L'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
			]
		];

		$this->SetFont('dejavusans', 'B');
		foreach ( $rows as $row ) {
			foreach ($row as $col) {
				$this->Cell($wCell * $col['with_mult'], $h, $col['txt'], 1, 0, $col['align']);
			}
			$this->Ln();
		}
		$this->SetFont('dejavusans', '');

		$totNcPedidosA = [];
		$totNcPedidosB = [];
		for ($i = 0; $i < 12; $i++) {
			$month = $i + 1;

			if (($result_a['year'] == $this->currentYear || $result_b['year'] == $this->currentYear) && $month >= $this->currentMonth) {
				for ($j = 0; $j < count(end($rows)); $j++) {
					$this->Cell($wCell, $h, '', 1, 0, 'C');
				}
				$this->Ln();
				continue;
			}

			$pesoMedioA = $this->getValue($result_a['pesos_medios'], $month);
			$pesoMedioB = $this->getValue($result_b['pesos_medios'], $month);
			$pesoMedioAB = $this->getPercentage($pesoMedioA, $pesoMedioB, -1);
			$proveXOfertaA = $this->getValue($result_a['proveedores_x_oferta'], $month);
			$proveXOfertaB = $this->getValue($result_b['proveedores_x_oferta'], $month);
			$proveXOfertaAB = $this->getPercentage($proveXOfertaA, $proveXOfertaB, -1);
			$ncUdA = $this->getValue($result_a['nc'], $month);
			$ncUdB = $this->getValue($result_b['nc'], $month);
			$ncUdAB = $this->getPercentage($ncUdA, $ncUdB, -1);
			$ncPedidosA = $this->getPercentage($ncUdA, $this->getValue($result_a['pedidos'], $month));
			array_push($totNcPedidosA, $ncPedidosA);
			$ncPedidosB = $this->getPercentage($ncUdB, $this->getValue($result_b['pedidos'], $month));
			array_push($totNcPedidosB, $ncPedidosB);
			$this->Cell($wCell, $h, $month, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($pesoMedioA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($pesoMedioB, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($pesoMedioAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($proveXOfertaA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($proveXOfertaB, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($proveXOfertaAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $ncUdA, 1, 0, 'C');
			$this->Cell($wCell, $h, $ncUdB, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPedidosA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPedidosB, 2), 1, 1, 'C');
		}

		$totNcUdA = array_sum($result_a['nc']);
		$totNcUdB = array_sum($result_b['nc']);
		$this->Cell($wCell, $h, 'Totales', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNcUdA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNcUdB), 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 1, 'C');

		$medPesoMedioA = $this->getDivision(array_sum($result_a['pesos_medios']), count($result_a['pesos_medios']));
		$medPesoMedioB = $this->getDivision(array_sum($result_b['pesos_medios']), count($result_b['pesos_medios']));
		$medPesoMedioAB = $this->getPercentage($medPesoMedioA, $medPesoMedioB, -1);
		$medProveXOfertaA = $this->getDivision(array_sum($result_a['proveedores_x_oferta']), count($result_a['proveedores_x_oferta']));
		$medProveXOfertaB = $this->getDivision(array_sum($result_b['proveedores_x_oferta']), count($result_b['proveedores_x_oferta']));
		$medProveXOfertaAB = $this->getPercentage($medProveXOfertaA, $medProveXOfertaB, -1);
		$medNcUdA = $this->getDivision($totNcUdA, count($result_a['nc']));
		$medNcUdB = $this->getDivision($totNcUdB, count($result_b['nc']));
		$medNcUdAB = $this->getPercentage($medNcUdA, $medNcUdB, -1);
		$medNcPedidosA = $this->getDivision(array_sum($totNcPedidosA), count($totNcPedidosA));
		$medNcPedidosB = $this->getDivision(array_sum($totNcPedidosB), count($totNcPedidosB));
		$this->Cell($wCell, $h, 'Media', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medPesoMedioA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medPesoMedioB, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medPesoMedioAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medProveXOfertaA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medProveXOfertaB, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medProveXOfertaAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medNcUdA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medNcUdB, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medNcUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medNcPedidosA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($medNcPedidosB, 2), 1, 1, 'C');

		$this->Cell($wCell, $h, 'Objs.', 1, 0, 'C');
		$obj_fields = ['peso_medio', 'prove_x_oferta', 'nc'];
		foreach ( $obj_fields as $obj_field ) {
			if (!isset($result_a['objetivos'][$obj_field])) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				continue;
			}

			if (strpos($result_a['objetivos'][$obj_field], '%') != false) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
			} else {
				$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
				$this->Cell($wCell, $h, $objB, 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
			}
		}
		if ( isset( $result_a['objetivos']['nc_pedidos'] ) ) {
			$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
			$this->Cell( $wCell, $h, $result_a['objetivos']['nc_pedidos'], 1, 0, 'C' );
			$this->Cell( $wCell, $h, $objB, 1, 0, 'C' );
		} else {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
		}
	}

	/**
	 * @param array $arr
	 * @param string|int $key
	 *
	 * @return int|mixed
	 */
	private function getValue(array $arr, $key) {
		return isset($arr[$key]) ? $arr[$key] : 0;
	}

    /**
     * @param float|int $dividendo
     * @param float|int $divisor
     *
     * @return float|int
     */
    private function getDivision($dividendo, $divisor) {
        return $divisor == 0 ? $dividendo : $dividendo / $divisor;
    }

	/**
	 * @param float|int $dividendo
	 * @param float|int $divisor
	 * @param float|int $extra
	 *
	 * @return float|int
	 */
	private function getPercentage($dividendo, $divisor, $extra = 0) {
		$percent = ($divisor == 0 ? $dividendo : $dividendo / $divisor + $extra) * 100;
		// return abs($percent) > 100 ? 100 * ($percent / abs($percent)) : $percent;
        return $percent;
	}

	/**
	 * @param float|int $number
	 *
	 * @return string
	 */
	private function number2str($number, $decimals = 0) {
		return number_format($number, $decimals, ',', '.');
	}

	/**
	 * @param float|int $percent
	 *
	 * @return string
	 */
	private function percent2str($percent, $decimals = 0) {
		return $percent == 0 ? '0 %' : $this->number2str($percent, $decimals) . ' %';
	}
}

$pdf = new KPI_Compras();
$pdf->currentYear = $currentYear;
$pdf->currentMonth = $currentMonth;
$pdf->legend = [
	['title' => 'Propietario', 'value' => 'Resp. Compras.'],
	['title' => 'Actualización', 'value' => 'Mensual.'],
	['title' => 'Revisión', 'value' => 'Trimestral.'],
	['title' => 'A', 'value' => $result_a['name']],
	['title' => 'B', 'value' => $result_b['name']]
];
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('KPI Compras');
$pdf->SetTitle('KPI Compras');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(10, 47);
$pdf->SetAutoPageBreak(true, 24);
$pdf->AddPage('L');

$pdf->WriteTable($result_a, $result_b);

//SALIDA
$dir = INCO_DIR_KPIS;
if (!is_dir($dir)) {
	$oldmask = umask(0);
	$aux = mkdir($dir, 0755, true);
	umask($oldmask);
}
$fileName = 'KPI_Compras.pdf';
$output = $dir . $fileName;
if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

pg_close($dbconn);
