<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el pedido
$result = pg_query_params('SELECT numero, cliente, nombre, referencia_obra, referencia_cliente, observaciones, fecha, fecha_entrega, hora_entrega FROM pedidos WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$pedido = pg_fetch_all($result)[0];

// obtenemos las lineas del pedido
$result = pg_query_params('SELECT linea, producto, descripcion, descripcion_comercial, o_cantidad, o_cantidad_pendiente, o_longitud, paquete, cantidad_total, cantidad_total_pendiente, peso, referencia_plano FROM pedidoslin WHERE identificador = $1 AND seccion = 4 ORDER BY linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
$pedidoslin = pg_fetch_all($result);

$result = pg_query_params('SELECT SUM(peso)::NUMERIC(10,3) AS pesotot FROM pedidoslin WHERE identificador = $1 AND seccion = 4', array($identificador)) or incoLogWrite('La consulta fallo [pesotot]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pesotot] No hay resultado');
$pesotot = pg_fetch_all($result)[0]['pesotot'];

$result = pg_query_params($dbconn, 'SELECT seccion, linea, descripcion, peso, a_modulos, a_longitud, a_malla_antipajaros, e_cantidad, e_malla_antipajaros, l_cantidad, l_longitud, l_alto, l_ancho, o_cantidad, o_longitud, v_cantidad, v_longitud, v_ancho FROM pedidoslin WHERE identificador = $1 AND seccion IN (5, 9, 6, 4, 8) ORDER BY seccion, linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin - resumen]: ' . pg_last_error());
// if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin - resumen] No hay resultado');
$resumenlin = pg_fetch_all($result);

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_pedidos', 'SELECT codigo, nombre FROM clientes WHERE codigo = $1 LIMIT 1');

// preparamos para obtener el perforado
pg_prepare($dbconn, 'perforaciones', 'SELECT codigo, nombre FROM perforaciones WHERE codigo = $1 LIMIT 1');

pg_prepare($dbconn, 'descripcion_corta', 'SELECT pl.linea, (pro.nombre || \' \' || e.nombre || \' \' || pin.abreviatura || \' \' || c.nombre)::VARCHAR(127) AS nombre FROM pedidoslin pl, productos pro, colores c, pinturas pin, espesores e WHERE pl.identificador = $1 AND pl.linea = $2 AND pl.producto = pro.codigo AND pl.espesor = e.codigo AND pl.pintura = pin.codigo AND pl.ccolor = c.codigo');

pg_prepare($dbconn, 'pesopaq', 'SELECT SUM(peso)::NUMERIC(10,3) AS pesopaq FROM pedidoslin WHERE identificador = $1 AND seccion = 1 AND paquete = $2');

$result = pg_query_params($dbconn, 'select count(*)::integer as corte from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 4 and pl.producto = pp.producto and pp.proceso = 1', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarPerfilado]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPerfilado] No hay resultado');
$boCodbarPerfilado = pg_fetch_all($result)[0]['corte'] > 0;

$result = pg_query_params($dbconn, 'select count(*)::integer as plegado from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 4 and pl.producto = pp.producto and pp.proceso = 3', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarPlegado]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPlegado] No hay resultado');
$boCodbarPlegado = pg_fetch_all($result)[0]['plegado'] > 0;

$result = pg_query_params($dbconn, 'select count(*)::integer as punzonado from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 4 and pl.producto = pp.producto and pp.proceso = 5', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarPunzonado]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPunzonado] No hay resultado');
$boCodbarPunzonado = pg_fetch_all($result)[0]['punzonado'] > 0;

$result = pg_query_params($dbconn, 'select count(*)::integer as montaje from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 4 and pl.producto = pp.producto and pp.proceso = 6', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarMontaje]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarMontaje] No hay resultado');
$boCodbarMontaje = pg_fetch_all($result)[0]['montaje'] > 0;

$numero_pedido = $pedido['numero'];
$fecha_entrega = $pedido['fecha_entrega']; // date_format(date_create($pedido['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$hora_entrega = $pedido['hora_entrega']; // date_format(date_create($pedido['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$cliente = intval($pedido['cliente']);
$referencia_obra = trim($pedido['referencia_obra']);
$observaciones = trim($pedido['observaciones']);

if (is_null($cliente) || $cliente < 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    $result = pg_execute($dbconn, 'clientes_pedidos', array($cliente));
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $cliente = pg_fetch_all($result)[0];
    $codigo = $cliente['codigo'];
    $nombre = $cliente['nombre'];
}

class PDF extends TCPDF
{
    /**
     * @var integer
     */
    public $counter_i;

    /**
     * @var bool
     */
    private $resumen;

    private static $H = 9;

    private static $STYLE = array('align' => 'C', 'fitwidth' => true, 'border' => false, 'text' => true, 'font' => 'helvetica', 'fontsize' => 5);

    function Header()
    {
        global $pedido, $pedidoslin, $boCodbarPerfilado, $boCodbarPlegado;

        $this->SetFont('dejavusans', 'B', 24);
        $this->SetY(3);
        if ($this->counter_i == -1) {
            $this->Cell(0, 8, 'RESUMEN', 0, 1, 'C');
            $this->resumen = true;
        } else {
            $this->Cell(0, 8, 'OMEGAS', 0, 1, 'C');
            $this->resumen = false;
        }
        /*$this->SetFont('');
        $this->SetFontSize(8);*/
        $this->SetFont('dejavusans', '', 8);
        $y = $this->GetY();

        if ($this->counter_i != -1 && $boCodbarPerfilado) {
            $this->SetY(0, false);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'PERFILADO', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarPerfilado = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(7, 2, '0', STR_PAD_LEFT) . '1' . '08';
            $this->write1DBarcode($codbarPerfilado, 'C39', '', '', 65, self::$H, 1, self::$STYLE, 'N');
        }
        if ($this->counter_i != -1 && $boCodbarPlegado) {
            $this->SetXY(145, 0, false);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'PLEGADO', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarPlegado = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(2, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarPlegado, 'C39', '145', '', 65, self::$H, 1, self::$STYLE, 'N');
        }
        $this->SetY($y);

        $y = $this->GetY();
        $this->Cell(33, 4, 'CLIENTE', 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(57, 4, trim($pedido['nombre']), 0, 1, 'L');
        $this->SetFont('dejavusans', '');
        $this->Cell(33, 4, 'PEDIDO', 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(57, 4, trim($pedido['numero']), 0, 1, 'L');
        $this->SetFont('dejavusans', '');
        $this->Cell(33, 4, 'REFERENCIA DE OBRA', 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(57, 4, trim($pedido['referencia_obra']), 0, 1, 'L');
        $this->SetFont('dejavusans', '');
        $this->Cell(33, 4, 'PED. CL.', 0, 0);
        $this->Cell(57, 4, trim($pedido['referencia_cliente']), 0, 1, 'L');
        $this->Cell(33, 4, 'OBSERVACIONES', 0, 0);
        $this->Cell(57, 4, trim($pedido['observaciones']), 0, 1, 'L');
        $yObs = $this->GetY() + 1;
        $this->SetY($y);
        $x = 151;
        $this->SetX($x);
        $this->Cell(30, 4, 'FECHA ENT.', 0, 0, 'R');
        if (is_null($pedido['fecha_entrega'])) {
            $fecha = '';
        } else {
            $fe = DateTime::createFromFormat('Y-m-d', $pedido['fecha_entrega']);
            $fecha = $fe->format('d-m-Y');
        }
        $this->Cell(0, 4, $fecha, 0, 1, 'R');
        $this->SetX($x);
        $this->Cell(30, 4, 'FECHA PDO.', 0, 0, 'R');
        $fp = DateTime::createFromFormat('Y-m-d', $pedido['fecha']);
        $this->Cell(0, 4, $fp->format('d-m-Y'), 0, 1, 'R');
        $this->Ln(2);

        if ($this->counter_i != -1) {
            $this->SetX($x);
            $this->SetFont('dejavusans', 'B');
            $this->MultiCell(90, 4, trim($pedidoslin[$this->counter_i]['descripcion']) . PHP_EOL, 0, 'J', false, 1, '', '', false);
            $this->SetFont('dejavusans', '');

            $this->SetY($yObs);
            $this->SetFillColor(224); // Oscuro: 192
            $this->SetFont('dejavusans', '', 8);
            $height = 4;
            $this->Cell(30, $height, 'Operario de Carga', 1, 0, 'C', true);
            $this->Cell(10, $height, '', 1, 0);

            $this->Cell(5, $height, '1', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '2', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '3', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '4', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '5', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '6', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '7', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '8', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '9', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '10', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 1);
            $this->Ln(2);

            $this->Cell(10, $height, 'PQTS.', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(4, $height, '', 0, 0);
            $this->Cell(18, $height, 'FECHA FAB.', 0, 0, 'R');
            $this->Cell(5, $height, '1', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '2', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '3', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '4', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(4, $height, '', 0, 0);
            $this->Cell(18, $height, 'FECHA EXP.', 0, 0, 'R');
            $this->Cell(20, $height, '', 'B', 0);
            $this->Cell(4, $height, '', 0, 1);
        }
    }

    function Footer()
    {
        global $pedido, $pedidoslin, $boCodbarPunzonado, $boCodbarMontaje;

        if (!$this->resumen && $this->counter_i != -1 && $boCodbarPunzonado) {
            $this->SetY(-12);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'PUNZONADO', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarPunzonado = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(3, 2, '0', STR_PAD_LEFT) . '1' . '07';
            $this->write1DBarcode($codbarPunzonado, 'C39', '', '', 65, self::$H, 1, self::$STYLE, 'N');
            $this->SetFont('dejavusans', '', 8);
        }
        if (!$this->resumen && $this->counter_i != -1 && $boCodbarMontaje) {
            $this->SetY(-12);
            $this->SetX(145);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'MONTAJE', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarMontaje = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(4, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarMontaje, 'C39', '145', '', 65, self::$H, 1, self::$STYLE, 'N');
            $this->SetFont('dejavusans', '', 8);
        }
        if ($this->resumen) {
            $this->SetY(-12);
            $this->SetX(145);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'CARGA', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarCarga = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(0, 3, '0', STR_PAD_LEFT) .
                str_pad(8, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarCarga, 'C39', '145', '', 65, self::$H, 1, self::$STYLE, 'N');
            $this->SetFont('dejavusans', '', 8);
        }
    }
}

$pdf = new PDF();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('OFERTA');
$pdf->SetTitle('Oferta X');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 48);
$pdf->SetAutoPageBreak(false);

$pdf->counter_i = -1;
$boCodbarMontaje = false;
$pdf->AddPage();
if ($resumenlin) {
    $pdf->SetFillColor(224); // Oscuro: 192
    $pdf->Cell(10, 4, 'Lin', 1, 0, 'C', true);
    $pdf->Cell(116, 4, 'Descripción', 1, 0, 'C', true);
    $pdf->Cell(10, 4, 'Uds', 1, 0, 'C', true);
    $pdf->Cell(12, 4, 'Largo', 1, 0, 'C', true);
    $pdf->Cell(12, 4, 'Alto', 1, 0, 'C', true);
    $pdf->Cell(12, 4, 'Ancho', 1, 0, 'C', true);
    $pdf->Cell(14, 4, 'Kgs', 1, 0, 'C', true);
    $pdf->Cell(14, 4, 'Montaje', 1, 1, 'C', true);
    $lastseccion = intval($resumenlin[0]['seccion']);
    for ($i = 0; $i < count($resumenlin); $i++) {
        $seccion = intval($resumenlin[$i]['seccion']);
        if ($lastseccion != $seccion) {
            $pdf->Cell(10, 1, '', 0, 0, 'C');
            $pdf->Cell(116, 1, '', 0, 0, 'L');
            $pdf->Cell(10, 1, '', 0, 0, 'R');
            $pdf->Cell(12, 1, '', 0, 0, 'R');
            $pdf->Cell(12, 1, '', 0, 0, 'R');
            $pdf->Cell(12, 1, '', 0, 0, 'R');
            $pdf->Cell(14, 1, '', 0, 0, 'R');
            $pdf->Cell(14, 1, '', 0, 1, 'R');
            $lastseccion = $seccion;
        }

        $lin = intval($resumenlin[$i]['linea']);
        $descripcion = trim($resumenlin[$i]['descripcion']);
        $kgs = round(floatval($resumenlin[$i]['peso']), 0);
        switch ($seccion) {
            case 4: // OMEGAS
                $uds = $resumenlin[$i]['o_cantidad'];
                $largo = $resumenlin[$i]['o_longitud'];
                $alto = '';
                $ancho = '';
                break;
            case 5: // V. LINEAL
                $uds = $resumenlin[$i]['a_modulos'];
                $largo = $resumenlin[$i]['a_longitud'];
                $alto = '';
                $ancho = '';
                if ($resumenlin[$i]['a_malla_antipajaros'] == 1) $descripcion .= ' MALLA';
                if ($resumenlin[$i]['a_malla_antipajaros'] == 2) $descripcion .= ' MOSQUITERA';
                break;
            case 6: // V. LAMAS
                $uds = $resumenlin[$i]['l_cantidad'];
                $largo = $resumenlin[$i]['l_longitud'];
                $alto = $resumenlin[$i]['l_alto'];
                $ancho = $resumenlin[$i]['l_ancho'];
                break;
            case 8: // OTROS
                $uds = $resumenlin[$i]['v_cantidad'];
                $largo = $resumenlin[$i]['v_longitud'];
                $alto = '';
                $ancho = $resumenlin[$i]['v_ancho'];
                break;
            case 9: // V. PUNTUAL
                $uds = $resumenlin[$i]['e_cantidad'];
                $largo = '';
                $alto = '';
                $ancho = '';
                if ($resumenlin[$i]['e_malla_antipajaros'] == 1) $descripcion .= ' MALLA';
                if ($resumenlin[$i]['e_malla_antipajaros'] == 2) $descripcion .= ' MOSQUITERA';
                break;
            default:
                $uds = '';
                $largo = '';
                $alto = '';
                $ancho = '';
                break;
        }

        $lines = ceil($pdf->GetStringWidth($descripcion) / 114);
        $height = $lines * 4;

        $pdf->Cell(10, $height, $lin, 1, 0, 'C');
        // $pdf->Cell(116, $height, $descripcion, 1, 0, 'L');
        $pdf->MultiCell(116, $height, $descripcion . PHP_EOL, 1, 'J', false, 0);
        $pdf->Cell(10, $height, $uds, 1, 0, 'R');
        $pdf->Cell(12, $height, $largo, 1, 0, 'R');
        $pdf->Cell(12, $height, $alto, 1, 0, 'R');
        $pdf->Cell(12, $height, $ancho, 1, 0, 'R');
        $pdf->Cell(14, $height, $kgs, 1, 0, 'R');
        $pdf->Cell(14, $height, '', 1, 1, 'R');
    }
}

$result = pg_query_params('select string_agg(distinct s.nombre, \' - \') as secciones from pedidoslin pl, secciones s where pl.identificador = $1 and pl.seccion = s.codigo', array($identificador)) or incoLogWrite('La consulta fallo [secciones]: ' . pg_last_error());
$secciones = trim(pg_fetch_all($result)[0]['secciones']);
$pdf->Ln(5);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $secciones, 0, 1, 'C');
$pdf->SetFont('dejavusans', '');

$pdf->counter_i = 0;
$pdf->AddPage();
$pdf->SetX(5);
// Productos de la oferta. Cabecera
$pdf->SetFillColor(224); // Oscuro: 192
$pdf->Cell(8, 4, 'Lin', 1, 0, 'C', true);
$pdf->Cell(104, 4, 'Descripción', 1, 0, 'C', true);
$pdf->Cell(30, 4, 'Zona', 1, 0, 'C', true);
$pdf->Cell(10, 4, 'Uds', 1, 0, 'C', true);
$pdf->Cell(14, 4, 'Long.', 1, 0, 'C', true);
$pdf->Cell(8, 4, 'Paq', 1, 0, 'C', true);
$pdf->Cell(12, 4, 'ML', 1, 0, 'C', true);
$pdf->Cell(14, 4, 'Kgs', 1, 1, 'C', true);
// Productos de la oferta. Lineas
$lastPaq = 0;
for ($i = 0; $i < count($pedidoslin); $i++) {
    $linea = $pedidoslin[$i]['linea'];
    $result = pg_execute($dbconn, 'descripcion_corta', array($identificador, $linea));
    if (pg_num_rows($result) <= 0) incoLogWrite('[desc corta] No hay resultado');
    $descripcion = trim(pg_fetch_all($result)[0]['nombre']);
    $cantidad = $pedidoslin[$i]['o_cantidad_pendiente'];
    $longitud = $pedidoslin[$i]['o_longitud'];
    $paquete = $pedidoslin[$i]['paquete'];
    $metroslineales = floatval($pedidoslin[$i]['cantidad_total_pendiente']);
    $metroslineales = number_format($metroslineales, 0, ',', '.');
    $peso = floatval($pedidoslin[$i]['peso']);
    $peso = number_format($peso, 0, ',', '.');
    $refPlano = trim($pedidoslin[$i]['referencia_plano']);

    if ($lastPaq != $paquete) {
        $result = pg_execute($dbconn, 'pesopaq', array($identificador, $paquete));
        if (pg_num_rows($result) <= 0) incoLogWrite('[pesopaq] No hay resultado');
        $peso = pg_fetch_all($result)[0]['pesopaq'];
        $peso = number_format($peso, 0, ',', '.');
        $lastPaq = $paquete;
    } else {
        $peso = '';
    }

    $pdf->SetX(5);
    if ($pdf->GetStringWidth($descripcion) >= 104) {
        $lines = ceil($pdf->GetStringWidth($descripcion) / 104);
        $height = $lines * 4;
        $pdf->Cell(8, $height, $linea, 1, 0, 'R');
        $pdf->MultiCell(104, $height, $descripcion, 1, 'L', false, 0);
        $pdf->Cell(30, $height, $refPlano, 1, 0, 'R');
        $pdf->Cell(10, $height, $cantidad, 1, 0, 'R');
        $pdf->Cell(14, $height, $longitud, 1, 0, 'R');
        $pdf->Cell(8, $height, $paquete, 1, 0, 'R');
        $pdf->Cell(12, $height, $metroslineales, 1, 0, 'R');
        $pdf->Cell(14, $height, $peso, 1, 1, 'R');
    } else {
        $pdf->Cell(8, 5, $linea, 1, 0, 'R');
        $pdf->Cell(104, 5, $descripcion, 1, 0, 'L');
        $pdf->Cell(30, 5, $refPlano, 1, 0, 'R');
        $pdf->Cell(10, 5, $cantidad, 1, 0, 'R');
        $pdf->Cell(14, 5, $longitud, 1, 0, 'R');
        $pdf->Cell(8, 5, $paquete, 1, 0, 'R');
        $pdf->Cell(12, 5, $metroslineales, 1, 0, 'R');
        $pdf->Cell(14, 5, $peso, 1, 1, 'R');
    }
}

//SALIDA
$dir = INCO_DIR_TALLER;
if (!is_dir($dir)) {
    $oldmask = umask(0);
    $aux = mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'TO_' . str_pad($numero_pedido, 6, '0', STR_PAD_LEFT) . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
