<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$idioma = isset($argv[3]) ? trim($argv[3]) : '';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos datos de la factura
/*$result = pg_query_params('SELECT ejercicio, serie, numero, cliente, fecha, forma_pago, bruto, descuento_pronto_pago, total_descuento_pronto_pago, recargo_financiero,
        total_recargo_financiero, base_exenta, base_0, tipo_iva_0, total_iva_0, base_1, tipo_iva_1, total_iva_1, base_2, tipo_iva_2, total_iva_2, total_factura, tipo_portes, portes, anticipo, anticipo_con_factura, factura_anticipado
        FROM facturas
        WHERE identificador = $1', array($identificador))
or incoLogWrite('La consulta fallo [facturas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[facturas] No hay resultado');
$factura = pg_fetch_all($result)[0];*/

// obtenemos datos del albaran
$result = pg_query_params('SELECT ejercicio, numero, fecha, cliente, forma_pago, portes, total_portes, descuento_pronto_pago, recargo_financiero, base_exenta, base_0, tipo_iva_0, base_1, tipo_iva_1, base_2, tipo_iva_2, total_portes_exceso, iva, identificador_oferta FROM albaranes WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [albaranes]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes] No hay resultado');
$albaran = pg_fetch_all($result)[0];

$ofer = trim($albaran['identificador_oferta']);
if (is_null($ofer)) {
    $oferta = array('condiciones' => '', 'transporte' => '', 'texto_factura' => '');
} else {
    $result = pg_query_params('SELECT condiciones, transporte, texto_factura FROM ofertas WHERE identificador = $1', array($ofer))
    or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) $oferta = array('condiciones' => '', 'transporte' => '', 'texto_factura' => '');
    else $oferta = pg_fetch_all($result)[0];
}

// obtenemos las lineas del albaran
$result = pg_query_params('SELECT a.identificador, seccion, descripcion_comercial, precio, unidad_medida, descuento, referencia_plano, SUM(metros)::NUMERIC(10,2) AS metros, SUM(importe)::NUMERIC(10,2) AS importe FROM albaranes a, albaraneslin al WHERE a.identificador = $1 AND a.identificador = al.identificador GROUP BY a.identificador, seccion, producto, espesor, calidad, recubrimiento, pintura, ccolor, unidad_medida, descripcion_comercial, precio, descuento, referencia_plano ORDER BY a.identificador, seccion, importe', array($identificador)) or incoLogWrite('La consulta fallo [albaraneslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaraneslin] No hay resultado');
$albaranlin = pg_fetch_all($result);

// obtenemos el importe
$portes = 0;
if (intval($albaran['portes']) == 0) $portes = $albaran['total_portes']; // portes en factura
if (intval($albaran['portes']) == 1) $portes = $albaran['total_portes_exceso']; // portes pagados
if (is_null($portes)) $portes = 0;

if ($albaran['iva'] == 0) $albaran['base_exenta'] += $portes; // iva no
if ($albaran['iva'] == 1) $albaran['base_0'] += $portes; // iva si

$albaran['bruto'] = $albaran['base_exenta'] + $albaran['base_0'] + $albaran['base_1'] + $albaran['base_2'];

$dpp_e = $albaran['base_exenta'] * ($albaran['descuento_pronto_pago'] / 100);
$dpp_0 = $albaran['base_0'] * ($albaran['descuento_pronto_pago'] / 100);
$dpp_1 = $albaran['base_1'] * ($albaran['descuento_pronto_pago'] / 100);
$dpp_2 = $albaran['base_2'] * ($albaran['descuento_pronto_pago'] / 100);
$albaran['total_descuento_pronto_pago'] = $dpp_e + $dpp_0 + $dpp_1 + $dpp_2;

$albaran['base_exenta'] = $albaran['base_exenta'] - $dpp_e;
$albaran['base_0'] = $albaran['base_0'] - $dpp_0;
$albaran['base_1'] = $albaran['base_1'] - $dpp_1;
$albaran['base_2'] = $albaran['base_2'] - $dpp_2;

$albaran['total_iva_0'] = $albaran['base_0'] * ($albaran['tipo_iva_0'] / 100);
$albaran['total_iva_1'] = $albaran['base_1'] * ($albaran['tipo_iva_1'] / 100);
$albaran['total_iva_2'] = $albaran['base_2'] * ($albaran['tipo_iva_2'] / 100);
$total_iva = $albaran['total_iva_0'] + $albaran['total_iva_1'] + $albaran['total_iva_2'];

$base_imp = $albaran['bruto'] - $albaran['total_descuento_pronto_pago'];
$total_factura = $base_imp + $total_iva;
$total_recargo = $total_factura * ($albaran['recargo_financiero'] / 100);
$total_factura += $total_recargo;
$albaran['total_factura'] = $total_factura;

/*$result = pg_query_params('SELECT fvto, importe FROM efectos WHERE identif_fra = $1', array($identificador));
if (pg_num_rows($result) <= 0) incoLogWrite('[efectos] No hay resultado');
$efectos = pg_fetch_all($result);*/

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_facturas', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma FROM clientes WHERE codigo = $1 LIMIT 1');

// preparamos para obtener la forma de pago
pg_prepare($dbconn, 'fopago_facturas', 'SELECT nombre, banco FROM fopago WHERE codigo = $1');

// preparamos para obtener el banco
pg_prepare($dbconn, 'bancos_facturas', 'SELECT nombre, (iban || cbanco || csucursal || cdc || cuenta)::CHARACTER(63) AS cuenta FROM bancos WHERE codigo = $1');

// preparamos para obtener el numero del albaran
pg_prepare($dbconn, 'albaranes_facturas', 'SELECT numero, referencia_cliente, identificador_oferta, referencia_obra FROM albaranes WHERE identificador = $1 LIMIT 1');

// preparamos para obtener la unidad de medida de cada linea del albaran
pg_prepare($dbconn, 'unidadesmedida_albaranlin', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1 LIMIT 1');

$albaran['fecha'] = DateTime::createFromFormat('Y-m-d', $albaran['fecha'])->format('d/m/Y');
// $pedidoCli = trim($albaran['referencia_cliente']);
$cliente = $albaran['cliente'];
$formaPago = trim($albaran['forma_pago']);

if (empty($cliente) || is_null($cliente) || $cliente <= 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    $result = pg_execute($dbconn, 'clientes_facturas', array($cliente));
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $aux = pg_fetch_all($result)[0];
    $cliente = array();
    $cliente['codigo'] = $aux['codigo'];
    $cliente['nif'] = trim($aux['nif']);
    $cliente['nombre'] = trim($aux['nombre']);
    $cliente['direccion'] = trim($aux['direccion']);
    $cliente['codpo'] = $aux['codpo'];
    $cliente['poblacion'] = trim($aux['poblacion']);
    $cliente['provincia'] = trim($aux['provincia']);
    if (empty($idioma)) $idioma = trim($aux['idioma']);
    $tr = new IncoTranslatorFix($dbconn, 'proforma', $idioma);
}

if (empty($formaPago) || is_null($formaPago)) {
    incoLogWrite('[forma_pago] No hay valor');
} else {
    $result = pg_execute($dbconn, 'fopago_facturas', array($formaPago));
    if (pg_num_rows($result) <= 0) incoLogWrite('[forma_pago] No hay resultado');
    $aux = pg_fetch_all($result)[0];
    $formaPago = trim($aux['nombre']);

    if ($aux['banco'] == 0) {
        $banco = array();
    } else {
        $result = pg_execute($dbconn, 'bancos_facturas', array($aux['banco']));
        if (pg_num_rows($result) <= 0) incoLogWrite('[banco] No hay resultado');
        $banco = pg_fetch_all($result)[0];
    }
}

class PDF extends TCPDF
{
    /**
     * @var IncoTranslatorFix
     */
    private $tr;

    /**
     * @var array
     */
    private $albaran;

    /**
     * @var array
     */
    private $albaraneslin;

    /**
     * @var array
     */
    private $cliente;

    /**
     * @var string
     */
    private $formaPago;

    /**
     * @var array
     */
    private $banco;

    /**
     * @var array
     */
    private $efectos;

    /**
     * @param IncoTranslatorFix $tr
     */
    public function setTr($tr)
    {
        $this->tr = $tr;
    }

    /**
     * @param array $factura
     */
    public function setFactura($factura)
    {
        $this->factura = $factura;
    }

    /**
     * @param array $albaran
     */
    public function setAlbaran($albaran)
    {
        $this->albaran = $albaran;
    }

    /**
     * @param array $albaraneslin
     */
    public function setAlbaraneslin($albaraneslin)
    {
        $this->albaraneslin = $albaraneslin;
    }

    /**
     * @param array $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @param string $formaPago
     */
    public function setFormaPago($formaPago)
    {
        $this->formaPago = $formaPago;
    }

    /**
     * @param array $banco
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;
    }

    /**
     * @param array $efectos
     */
    public function setEfectos($efectos)
    {
        $this->efectos = $efectos;
    }

    function Header()
    {
    	$this->Image(INCO_DIR_IMAGENES . 'logo.jpg', 20, 10, 60, 0, 'jpg', '', '', true, 300);
        $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 247, 211, 0, 'jpg', '', '', true, 300);
        // $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 252, 211, 45, 'jpg', '', '', true, 300);

        $this->SetY(5);
        $this->SetFont('dejavusans', 'B', 24);
        $this->Cell(0, 4, $this->tr->getTranslate('title'), 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 4, $this->tr->getTranslate('h_1') . ' ' . $this->PageNo(), 0, 1, 'R');
        $y = $this->GetY();

        $this->SetY(54);
        $this->Cell(25, 4, $this->tr->getTranslate('h_2'), 0, 0);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(60, 4, 'A/' . str_pad($this->albaran['numero'], 6, '0', STR_PAD_LEFT), 0, 1);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(25, 4, $this->tr->getTranslate('h_3'), 0, 0);
        $this->Cell(60, 4, $this->albaran['fecha'], 0, 1);
        $this->Cell(25, 4, $this->tr->getTranslate('h_4'), 0, 0);
        $this->Cell(60, 4, $this->cliente['nif'], 0, 1);

        $this->SetY($y);
        $this->Ln();
        $this->Ln();
        $this->SetFont('dejavusans', '', 8);
        $this->setCellPaddings('3', '2', '3', '2');
        $this->Ln();
        $this->SetX(100);
        $this->Cell(0, 4, str_pad($this->cliente['codigo'], 5, '0', STR_PAD_LEFT), 'LTR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['nombre']), 'LR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['direccion']), 'LR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['codpo']) . ' - ' . trim($this->cliente['poblacion']), 'LR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['provincia']), 'LRB', 1);
        $this->Ln(2);

        // $this->SetX(90);
        // $this->MultiCell(0, 12, $this->observaciones, 0, 'L', false, 1);

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('4', '0', '0', '0');

        $this->SetCellPadding(0);
        $this->SetXY(0, 90);
        $this->Cell(10, 4, '--');
        $this->SetXY(0, 200);
        $this->Cell(10, 4, '--');
    }

    function Footer()
    {
        $this->SetY(-52);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(30, 4, $this->tr->getTranslate('f_1'), 1, 0, 'C');
        if ($this->albaran['descuento_pronto_pago'] == 0) {
            $this->Cell(65, 4, '', 1, 0, 'C');
        } else {
            $this->Cell(65, 4, $this->tr->getTranslate('Descuento Pronto Pago'), 1, 0, 'C');
        }
        $this->Cell(30, 4, $this->tr->getTranslate('f_2'), 1, 0, 'C');
        $this->Cell(18, 4, $this->tr->getTranslate('f_3'), 1, 0, 'C');
        $this->Cell(25, 4, $this->tr->getTranslate('f_4'), 1, 0, 'C');
        $this->Cell(0, 4, $this->tr->getTranslate('f_5', true), 1, 1, 'C');

        $this->setCellPaddings('0', '0', '2', '0');
        $this->Cell(30, 4, number_format($this->albaran['bruto'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
        if ($this->albaran['descuento_pronto_pago'] == 0) {
            $this->Cell(65, 4, '', 'LTR', 0, 'C');
        } else {
            $this->Cell(65, 4, number_format($this->albaran['descuento_pronto_pago'], 0) . '% s/ ' . number_format($this->albaran['bruto'], 2, ',', '.') . ' € = ' . number_format($this->albaran['total_descuento_pronto_pago'] * -1, 2, ',', '.') . ' €', 'LTR', 0, 'C');
        }

        if ($this->albaran['iva'] == 0) {
            $this->Cell(30, 4, number_format($this->albaran['base_exenta'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
            $this->Cell(18, 4, '0,00 %', 'LTR', 0, 'C');
            $this->Cell(25, 4, '0,00 €', 'LTR', 0, 'R');
        }
        if ($this->albaran['iva'] == 1) {
            $this->Cell(30, 4, number_format($this->albaran['base_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
            $this->Cell(18, 4, number_format($this->albaran['tipo_iva_0'], 0) . ' %', 'LTR', 0, 'C');
            $this->Cell(25, 4, number_format($this->albaran['total_iva_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
        }
        $this->SetFont('dejavusans', 'B');
        $this->Cell(0, 4, number_format($this->albaran['total_factura'], 2, ',', '.') . ' €', 'LTR', 1, 'R');

        if ($this->albaran['base_1'] == 0) {
            $this->Cell(30, 4, '', 'LBR', 0, 'R');
            $this->Cell(65, 4, '', 'LBR', 0, 'C');
            $this->Cell(30, 4, '', 'LBR', 0, 'R');
            $this->Cell(18, 4, '', 'LBR', 0, 'C');
            $this->Cell(25, 4, '', 'LBR', 0, 'R');
            $this->Cell(0, 4, '', 'LBR', 1, 'R');
        } else {
            $this->Cell(30, 4, '', 'LBR', 0, 'R');
            $this->Cell(65, 4, '', 'LBR', 0, 'C');
            $this->Cell(30, 4, number_format($this->albaran['base_1'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
            $this->Cell(18, 4, number_format($this->albaran['tipo_iva_1'], 0) . ' %', 'LBR', 0, 'C');
            $this->Cell(25, 4, number_format($this->albaran['total_iva_1'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
            $this->SetFont('dejavusans', 'B');
            $this->Cell(0, 4, '', 'LBR', 1, 'R');
        }
        $this->Ln(2);

        $vencimientos = array();
        $importes = array();
        for ($i = 0; $i < count($this->efectos); $i++) {
            $fvto = explode('-', $this->efectos[$i]['fvto']);
            array_push($vencimientos, $fvto[2] . '/' . $fvto[1] . '/' . $fvto[0]);
            array_push($importes, number_format($this->efectos[$i]['importe'], 2, ',', '.') . ' €');
        }

        $this->SetFont('dejavusans', '');
        $this->SetCellPadding(0);
        $this->Cell(30, 4, $this->tr->getTranslate('f_6'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $formaPago = $this->tr->getTranslate($this->formaPago);
        if (!empty($this->banco)) {
            $formaPago .= ' -- ' . trim($this->banco['nombre']);
            $aux = str_split(trim($this->banco['cuenta']), 4);
            $formaPago .= ' -- ' . implode('-', $aux);
        }
        $this->Cell(0, 4, $formaPago, 0, 1);
        /*if (!(floatval($this->factura['anticipo_con_factura']) > 0 && floatval($this->factura['total_factura']) == 0)) {
            $this->SetFont('dejavusans', '');
            $this->Cell(30, 4, 'Vencimientos', 0, 0);
            $this->SetFont('dejavusans', 'B');
            for ($i = 0; $i < count($vencimientos); $i++) {
                $ln = ($i == count($vencimientos) - 1) ? 1 : 0;
                $this->Cell(20, 4, $vencimientos[$i], 0, $ln, 'C');
            }
            $this->SetFont('dejavusans', '');
            $this->Cell(30, 4, 'Importes', 0, 0);
            $this->SetFont('dejavusans', 'B');
            for ($i = 0; $i < count($importes); $i++) {
                $ln = ($i == count($importes) - 1) ? 1 : 0;
                $this->Cell(20, 4, $importes[$i], 0, $ln, 'C');
            }
        }*/
        $this->SetY(-30);
        $this->SetX(35);
        $this->Cell(30, 4, $this->tr->getTranslate('f_7', true), 0, 1);
    }
}

$pdf = new PDF();
$pdf->setTr($tr);
$pdf->setAlbaran($albaran);
$pdf->setAlbaraneslin($albaranlin);
$pdf->setCliente($cliente);
$pdf->setFormaPago($formaPago);
$pdf->setBanco($banco);
//$pdf->setEfectos($efectos);

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('FACTURA');
$pdf->SetTitle('Factura X');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 73); // 70
$pdf->AddPage();

$pdf->Cell(25, 4, $tr->getTranslate('b_1', true), 1, 0, 'C');
$pdf->Cell(115, 4, $tr->getTranslate('b_2', true), 1, 0, 'C');
$pdf->Cell(20, 4, $tr->getTranslate('b_3', true), 1, 0, 'C');
$pdf->Cell(15, 4, '% ' . $tr->getTranslate('b_4', true), 1, 0, 'C');
$pdf->Cell(0, 4, $tr->getTranslate('b_5', true), 1, 1, 'C');

$ultimoAlbaran = '';
$cabecera = false;
for ($i = 0; $i < count($albaranlin); $i++) {
    $aux = $albaranlin[$i]['identificador'];
    if ($i == 0) {
        $ultimoAlbaran = $aux;
        $cabecera = true;
    } elseif ($ultimoAlbaran != $aux) {
        $pdf->Cell(25, 4, '', 'LR', 0, 'C');
        $pdf->Cell(115, 4, '', 'LR', 0, 'C');
        $pdf->Cell(20, 4, '', 'LR', 0, 'C');
        $pdf->Cell(15, 4, '', 'LR', 0, 'C');
        $pdf->Cell(0, 4, '', 'LR', 1, 'C');
        $ultimoAlbaran = $aux;
        $cabecera = true;
    }

    if ($cabecera) {
        $result = pg_execute($dbconn, 'albaranes_facturas', array($aux));
        if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes_facturas] No hay resultado');
        $aux = pg_fetch_all($result)[0];
        $pdf->Cell(25, 4, '', 'LR', 0, 'C');
        $pdf->Cell(38.33, 4, $tr->getTranslate('b_6', true) . ': ' . intval($aux['numero']), 'L', 0);
        $pdf->Cell(38.33, 4, $tr->getTranslate('b_7', true) . ': ' . trim($aux['referencia_cliente']), 0, 0);
        $pdf->Cell(38.33, 4, $tr->getTranslate('b_8', true) . ': ' . intval(substr(trim($aux['identificador_oferta']), 8)), 'R', 0);
        $pdf->Cell(20, 4, '', 'LR', 0, 'C');
        $pdf->Cell(15, 4, '', 'LR', 0, 'C');
        $pdf->Cell(0, 4, '', 'LR', 1, 'C');
        $pdf->Cell(25, 4, '', 'LR', 0, 'C');
        $pdf->Cell(115, 4, $tr->getTranslate('b_9', true) . ': ' . trim($aux['referencia_obra']), 'LR', 0, 'L');
        $pdf->Cell(20, 4, '', 'LR', 0, 'C');
        $pdf->Cell(15, 4, '', 'LR', 0, 'C');
        $pdf->Cell(0, 4, '', 'LR', 1, 'C');
        $cabecera = false;
    }

    $cantidad = floatval($albaranlin[$i]['metros']);
    $result = pg_execute($dbconn, 'unidadesmedida_albaranlin', array(intval($albaranlin[$i]['unidad_medida'])));
    if (pg_num_rows($result) <= 0) incoLogWrite('[unidades_medida] No hay resultado');
    $unidad_medida = trim(pg_fetch_all($result)[0]['abreviatura']);
    $unidad_medida = str_replace('2', html_entity_decode('&sup2;', ENT_COMPAT, 'UTF-8'), $unidad_medida);
    $descripcion = $tr->getTranslate(trim($albaranlin[$i]['descripcion_comercial']));
    $descripcion = trim($descripcion . ' ' . trim($albaranlin[$i]['referencia_plano']));
    $precio = floatval($albaranlin[$i]['precio']);
    if (floatval($albaranlin[$i]['descuento']) == 0) {
        $descuento = '';
    } else {
        $descuento = floatval($albaranlin[$i]['descuento']);
        $descuento = number_format($descuento, 2, ',', '.');
    }
    $importe = floatval($albaranlin[$i]['importe']);

    if ($cantidad == 0 || $importe == 0) {
        continue;
    }

    // $lines = ceil($pdf->GetStringWidth($descripcion) / 110);
    // $height = $lines * 4;
    $height = ceil($pdf->getStringHeight(115, $descripcion, true));

    $aux = explode(' | ', $descripcion);
    if (count($aux) > 1) {
        $descripcion = str_replace(' | ', PHP_EOL, $descripcion);
        // $height += 4;
        $height = ceil($pdf->getStringHeight(115, $descripcion, true));
    }

    if ($pdf->GetY() + $height >= 242) {
        $pdf->Cell(25, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(115, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(20, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(15, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(0, 242 - $pdf->GetY(), '', 'LRB', 1);
        $pdf->AddPage();
        $pdf->Cell(25, 4, $tr->getTranslate('b_1', true), 1, 0, 'C');
        $pdf->Cell(115, 4, $tr->getTranslate('b_2', true), 1, 0, 'C');
        $pdf->Cell(20, 4, $tr->getTranslate('b_3', true), 1, 0, 'C');
        $pdf->Cell(15, 4, '% ' . $tr->getTranslate('b_4', true), 1, 0, 'C');
        $pdf->Cell(0, 4, $tr->getTranslate('b_5', true), 1, 1, 'C');
    }

    $pdf->Cell(18, $height, number_format($cantidad, 2, ',', '.'), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(7, $height, $unidad_medida, 'R', 0, 'C', false, '', 0, false, 'T', 'T');
    $pdf->MultiCell(115, $height, $descripcion, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, number_format($precio, 2, ',', '.'), 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(15, $height, $descuento, 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(0, $height, number_format($importe, 2, ',', '.'), 'LR', 1, 'R', false, '', 0, false, 'T', 'T');
}

if (floatval($albaran['total_portes']) > 0 || floatval($albaran['total_portes_exceso']) > 0) {
    // portes en factura
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, '', 'LR', 0, 'C');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');
    if (floatval($albaran['total_portes']) > 0) {
        $portes = floatval($albaran['total_portes']);
        $txt = $tr->getTranslate('b_10', true);
    } else {
        $portes = floatval($albaran['total_portes_exceso']);
        $txt = $tr->getTranslate('b_11', true);
    }
    /*$trans = trim($oferta['transporte']);
    if (!empty($trans) && !is_null($trans)) {
        $txt = $trans;
    }
    $lines = ceil($pdf->GetStringWidth($txt) / 113);
    $lines += substr_count($txt, PHP_EOL);*/
    $height = $height = ceil($pdf->getStringHeight(115, $txt, true));

    $pdf->Cell(18, $height, '1', 'L', 0, 'R');
    $pdf->Cell(7, $height, '', 'R', 0, 'C');
    // $pdf->Cell(115, $height, $txt, 'LR', 0);
    $pdf->MultiCell(115, $height, $txt . PHP_EOL, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, '', 'LR', 0, 'R');
    $pdf->Cell(15, $height, '', 'LR', 0, 'R');
    $pdf->Cell(0, $height, number_format($portes, 2, ',', '.'), 'LR', 1, 'R');
}

$txtFactura = trim($oferta['texto_factura']);
if (!is_null($txtFactura) && !empty($txtFactura)) {
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, '', 'LR', 0, 'C');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');

    $txt = $tr->getTranslate($txtFactura);
    $height = ceil($pdf->getStringHeight(115, $txt));

    $pdf->Cell(25, $height, '', 'LR', 0, 'R');
    // $pdf->Cell(115, $height, $txt, 'LR', 0);
    $pdf->MultiCell(115, $height, $txt . PHP_EOL, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, '', 'LR', 0, 'R');
    $pdf->Cell(15, $height, '', 'LR', 0, 'R');
    $pdf->Cell(0, $height, '', 'LR', 1, 'R');
}

$pdf->Cell(25, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(115, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(20, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(15, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(0, 242 - $pdf->GetY(), '', 'LRB', 1);

// 245

$dir = INCO_DIR_CLIENTES . $cliente['codigo'] . '/FacturasProforma/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'FP_A_' . $albaran['ejercicio'] . '_' . str_pad($albaran['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

// pg_free_result($result);
pg_close($dbconn);
