<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
    incoLogWrite('No hay argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params($dbconn, 'SELECT numero, cliente, nombre, referencia_obra, observaciones, referencia_cliente FROM pedidos WHERE identificador = $1 LIMIT 1', [$identificador]) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$pedido = pg_fetch_all($result)[0];

$result = pg_query_params($dbconn, 'select pl.seccion, pl.descripcion, sum(pl.cantidad_total) as cantot, count(distinct paquete) as paqs, sum(pl.peso) as peso from pedidoslin pl where identificador like $1 group by pl.seccion, pl.descripcion', [$identificador]) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
$pedidoslin = pg_fetch_all($result);

$result = pg_query_params($dbconn, 'select distinct nombre from pedidoslin pl, secciones s where pl.identificador = $1 and pl.seccion = s.codigo', [$identificador]) or incoLogWrite('La consulta fallo [pedidoslin - secciones]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin - secciones] No hay resultado');
$secciones = implode(' - ', array_map('trim', array_column(pg_fetch_all($result), 'nombre')));

class OrdenCarga extends TCPDF
{
    const H = 9;
    const STYLE = array('align' => 'C', 'fitwidth' => true, 'border' => false, 'text' => true, 'font' => 'helvetica', 'fontsize' => 5);

    function Header() {
        $this->SetFont('dejavusans', 'B', 24);
        $this->SetY(10);
        $this->Cell(0, 8, 'ORDEN DE CARGA', 0, 1, 'C');
        $this->SetFont('');
        $this->SetFontSize(8);
        $this->Ln(4);
        $this->Cell(0, 4, 'Hoja: ' . $this->PageNo(), 0, 1, 'R');
    }

    function Footer() {
    }
}

$pdf = new OrdenCarga();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('Orden de carga');
$pdf->SetTitle(sprintf('Order de carga - Pedido %d', 0));
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 33);
$pdf->AddPage();

$yInicio = $pdf->GetY();
$pdf->Cell(40, 4, 'CLIENTE', 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(95, 4, trim(sprintf('%4d - %s', $pedido['cliente'], $pedido['nombre'])), 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(40, 4, 'PEDIDO', 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $pedido['numero'], 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(40, 4, 'REFERENCIA DE OBRA', 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $pedido['referencia_obra'], 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(40, 4, 'PED. CL.', 0, 0);
$pdf->Cell(0, 4, $pedido['referencia_cliente'], 0, 1);
$pdf->Cell(40, 4, 'OBSERVACIONES', 0, 0);
$pdf->Cell(0, 4, $pedido['observaciones'], 0, 1);
$yFin = $pdf->GetY();

$pdf->SetY($yInicio);
$pdf->SetFont('dejavusans', '', 5);
$pdf->SetX($pdf->GetX() + 135);
$pdf->Cell(65, 3, 'ORDEN DE CARGA', 0, 1, 'C');
$pdf->SetX($pdf->GetX() + 135);
$pdf->SetFont('dejavusans', '', 8);
// CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
$codbarCorte = incoBarcodeGenerate($pedido['numero'], 0, 8, 2, 0);
$pdf->write1DBarcode($codbarCorte, 'C39', '', '', 65, $pdf::H, 1, $pdf::STYLE, 'N');
$pdf->SetY($yFin);
$pdf->Ln(2);

$pdf->SetFont('dejavusans', 'b', 8);
$pdf->Cell(0, 4, $secciones, 0, 1, 'C');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Ln(4);

$pdf->SetFillColor(224);
$pdf->Cell(170, 4, 'Descripción', 1, 0, '', true);
$pdf->Cell(10, 4, 'Paqs.', 1, 0, 'R', true);
$pdf->Cell(20, 4, 'Peso (Kg)', 1, 1, 'R', true);
$seccion_prev = null;
foreach ($pedidoslin as $linea) {
    if (is_null($seccion_prev)) $seccion_prev = $linea['seccion'];
    if ($seccion_prev != $linea['seccion']) $pdf->Ln(4);

    $pdf->Cell(170, 4, trim($linea['descripcion']), 1, 0);
    $pdf->Cell(10, 4, $linea['paqs'], 1, 0, 'R');
    $pdf->Cell(20, 4, number_format($linea['peso'], 0, ',', '.'), 1, 1, 'R');
}

//SALIDA
$dir = INCO_DIR_ORDEN_CARGA;
if (!is_dir($dir)) {
    $oldmask = umask(0);
    $aux = mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'OC_' . str_pad($pedido['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_close();