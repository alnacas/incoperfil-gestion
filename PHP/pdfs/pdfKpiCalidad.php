<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$year_a = $argv[2];
$year_b = $argv[3];

$opciones_keys = ['mostrar_mes_curso', 'incluir_nc_proveedor'];
$opciones_val = explode(':', $argv[4]); // desglosar
$opciones = array_combine($opciones_keys, $opciones_val);

$currentYear = intval((new DateTime())->format('Y'));
$currentMonth = intval((new DateTime())->format('m'));
if ( boolval( $opciones['mostrar_mes_curso'] ) ) {
	$showUntilCurrentMonth = true;
	$currentMonth++;
} else {
	$showUntilCurrentMonth = ( $year_a == $currentYear || $year_b == $currentYear );
}

function getResult($dbconn, $year) {
	global $showUntilCurrentMonth, $currentMonth;

	$stmts = ['n_nc_pm' , 'n_ac', 'n_nc', 'n_indicadores'];
	$result = [];

	foreach ( $stmts as $stmt ) {
		$arr = [];
		$pg_result = pg_execute($dbconn, $stmt, [$year]) or incoLogWrite( 'La consulta fallo [config - ' . $stmt . ']');
		$pg_result = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($pg_result as $pg_res) {
			if ($showUntilCurrentMonth && $pg_res['month'] >= $currentMonth) continue;
			/*if (!isset($result[$stmt][$pg_res['month']])) $result[$stmt][$pg_res['month']] = 0;
			$result[$stmt][$pg_res['month']] += $pg_res['value'];*/
			if (!isset($arr[$pg_res['month']])) $arr[$pg_res['month']] = 0;
			$arr[$pg_res['month']] += $pg_res['value'];
		}
		$result[$stmt] = $arr;
	}

	$pg_result = pg_execute($dbconn, 'objetivos', [$year]) or incoLogWrite('La consulta fallo [config - objetivos]');
	$result['objetivos'] = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result)[0];

	$result['name'] = 'Año ' . $year;
	$result['year'] = intval($year);
	return $result;
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// pg_prepare($dbconn, 'n_nc_pm', 'select mes as month, val as value from kpi_calidad_datos where year = $1 and par_id = 0 order by mes');
// pg_prepare($dbconn, 'n_ac', 'select mes as month, val as value from kpi_calidad_datos where year = $1 and par_id = 1 order by mes');
// pg_prepare($dbconn, 'n_nc', 'select mes as month, val as value from kpi_calidad_datos where year = $1 and par_id = 2 order by mes');

if ($opciones['incluir_nc_proveedor']) {
    pg_prepare($dbconn, 'n_nc_pm', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and tipo in (2, 3) and estado != 2 group by month order by month');
    pg_prepare($dbconn, 'n_ac', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and tipo in (1, 5) and (ac = 1 or ap = 1) and estado != 2 group by month order by month');
    pg_prepare($dbconn, 'n_nc', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and tipo in (1, 5) and estado != 2 group by month order by month');
} else {
    pg_prepare($dbconn, 'n_nc_pm', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and tipo in (2, 3) and estado != 2 and area != 7 group by month order by month');
    pg_prepare($dbconn, 'n_ac', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and tipo in (1, 5) and (ac = 1 or ap = 1) and estado != 2 and area != 7 group by month order by month');
    pg_prepare($dbconn, 'n_nc', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and tipo in (1, 5) and estado != 2 and area != 7 group by month order by month');
}
pg_prepare($dbconn, 'n_indicadores', 'select mes as month, val as value from kpi_calidad_datos where year = $1 and par_id = 3 order by mes');
pg_prepare($dbconn, 'objetivos', 'select n_nc_pm, n_ac, n_nc, n_indicadores from kpi_calidad_objetivos where year = $1');

$result_a = getResult($dbconn, $year_a);
$result_b = getResult($dbconn, $year_b);

class KPI_Calidad extends TCPDF
{
	/**
	 * @var int
	 */
	public $currentMonth;

	/**
	 * @var int
	 */
	public $currentYear;

	/**
	 * @var array
	 */
	public $legend;

	function Header()
	{
		$this->SetFont('dejavusans', 'B', 14);
		$this->SetY(10);
		$this->Cell(0, 8, 'KPI Calidad', 0, 1, 'C');
		$this->SetFont('dejavusans', '', 8);
		$this->Ln(4);
		$this->WriteLegend();
	}

	function Footer() {
		$this->SetY(-18);

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 3;

		$this->SetFont('dejavusans', '', 8);
		$this->Cell($wCell, 4, 'Código: FCA-01-07', 0, 0, 'L');
		$this->Cell($wCell, 4, 'Revisión: 2', 0, 0, 'C');
		$this->Cell($wCell, 4, 'Fecha: 16/12/2019', 0, 1, 'R');
	}

	private function WriteLegend() {
		foreach ( $this->legend as $item ) {
			$this->SetFont('dejavusans', 'B');
			$this->Cell(30, 4, $item['title'], 0, 0, 'L');
			$this->SetFont('dejavusans', '');
			$this->Cell(0, 4, $item['value'], 0, 1, 'L');
		}
	}

	/**
	 * @param array $result_a
	 * @param array $result_b
	 */
	public function WriteTable( array $result_a, array $result_b ) {
		$h = 4.5;

		/*if (empty($result_a['n_nc_pm']) && empty($result_b['n_nc_pm'])) {
			$this->Cell(0, $h, 'No hay datos para comparar', 0, 1);
			return;
		}*/

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 12;

		$rows = [
			[
				['with_mult' => 1, 'txt' => 'Nombre', 'align' => 'L'],
				['with_mult' => 3, 'txt' => '01 Efectividad Auditoria', 'align' => 'C'],
				['with_mult' => 6, 'txt' => '02 Mejora Continua', 'align' => 'C'],
				['with_mult' => 2, 'txt' => '16 Indicadores', 'align' => 'C']
			],
			[
				['with_mult' => 1, 'txt' => 'Requisito', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Calidad Servicio', 'align' => 'C'],
				['with_mult' => 6, 'txt' => 'Calidad Servicio', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'Info. toma decisiones', 'align' => 'C']
			],
			[
				['with_mult' => 1, 'txt' => 'Proceso', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'PCA 05 Auditorias', 'align' => 'C'],
				['with_mult' => 6, 'txt' => 'PCA 03 NC y RQ', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'PCA 07 Medición Procesos', 'align' => 'C']
			],
			[
				['with_mult' => 1, 'txt' => 'Cálculo', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Nº NC y PM / Auditoria', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'Nº AC', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'Nº NC', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'AC / NC', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'Indicadores Disp', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Mes', 'align' => 'L'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
			]
		];

		$this->SetFont('dejavusans', 'B');
		foreach ( $rows as $row ) {
			foreach ($row as $col) {
				$this->Cell($wCell * $col['with_mult'], $h, $col['txt'], 1, 0, $col['align']);
			}
			$this->Ln();
		}
		$this->SetFont('dejavusans', '');

		for ($i = 0; $i < 12; $i++) {
			$month = $i + 1;

			if (($result_a['year'] == $this->currentYear || $result_b['year'] == $this->currentYear) && $month >= $this->currentMonth) {
				for ($j = 0; $j < 12; $j++) {
					$this->Cell($wCell, $h, '', 1, 0, 'C');
				}
				$this->Ln();
				continue;
			}

			$ncPmA = $this->getValue($result_a['n_nc_pm'], $month);
			$ncPmB = $this->getValue($result_b['n_nc_pm'], $month);
			$ncPmAB = $this->getPercentage($ncPmA, $ncPmB, -1);
			$nAcA = $this->getValue($result_a['n_ac'], $month);
			$nAcB = $this->getValue($result_b['n_ac'], $month);
			$nNcA = $this->getValue($result_a['n_nc'], $month);
			$nNcB = $this->getValue($result_b['n_nc'], $month);
			$nAcNcA = $this->getPercentage($nAcA, $nNcA);
			$nAcNcB = $this->getPercentage($nAcB, $nNcB);
			$nIndicadorA = $this->getValue($result_a['n_indicadores'], $month);
			$nIndicadorB = $this->getValue($result_b['n_indicadores'], $month);

			$this->Cell($wCell, $h, $month, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($ncPmA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($ncPmB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPmAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($nAcA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($nAcB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($nNcA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($nNcB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($nAcNcA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($nAcNcB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($nIndicadorA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($nIndicadorB), 1, 1, 'C');
		}

		$totNcPmA = array_sum($result_a['n_nc_pm']);
		$totNcPmB = array_sum($result_b['n_nc_pm']);
		$totNcPmAB = $this->getPercentage($totNcPmA, $totNcPmB, -1);
		$totNAcA = array_sum($result_a['n_ac']);
		$totNAcB = array_sum($result_b['n_ac']);
		$totNNcA = array_sum($result_a['n_nc']);
		$totNNcB = array_sum($result_b['n_nc']);
		$totNAcNcA = $this->getPercentage($totNAcA, $totNNcA);
		$totNAcNcB = $this->getPercentage($totNAcB, $totNNcB);
		$totNIndicadorA = count($result_a['n_indicadores']) == 0 ? 0 : array_sum($result_a['n_indicadores']) / count($result_a['n_indicadores']);
		$totNIndicadorB = count($result_b['n_indicadores']) == 0 ? 0 : array_sum($result_b['n_indicadores']) / count($result_b['n_indicadores']);

		$this->Cell($wCell, $h, 'Totales', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNcPmA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNcPmB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNcPmAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNAcA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNAcB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNNcA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNNcB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNAcNcA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNAcNcB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNIndicadorA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totNIndicadorB), 1, 1, 'C');

		$this->Cell($wCell, $h, 'Objs.', 1, 0, 'C');
		if ( isset( $result_a['objetivos']['n_nc_pm'] ) ) {
			if ( strpos( $result_a['objetivos']['n_nc_pm'], '%' ) != false ) {
				$this->Cell( $wCell, $h, '', 1, 0, 'C' );
				$this->Cell( $wCell, $h, '', 1, 0, 'C' );
				$this->Cell( $wCell, $h, $result_a['objetivos']['n_nc_pm'], 1, 0, 'C' );
			} else {
				$this->Cell( $wCell, $h, $result_a['objetivos']['n_nc_pm'], 1, 0, 'C' );
				$this->Cell( $wCell, $h, $result_b['objetivos']['n_nc_pm'], 1, 0, 'C' );
				$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			}
		} else {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
		}

		if ( isset( $result_a['objetivos']['n_ac'] ) && strpos( $result_a['objetivos']['n_ac'], '%' ) != false ) {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, $result_a['objetivos']['n_ac'], 1, 0, 'C' );
			$this->Cell( $wCell, $h, $result_b['objetivos']['n_nc'], 1, 0, 'C' );
		} else {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
		}

		if ( isset( $result_a['objetivos']['n_indicadores'] ) ) {
			$this->Cell( $wCell, $h, $result_a['objetivos']['n_indicadores'], 1, 0, 'C' );
			$this->Cell( $wCell, $h, $result_b['objetivos']['n_indicadores'], 1, 0, 'C' );
		} else {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
		}
	}

	/**
	 * @param array $arr
	 * @param string|int $key
	 *
	 * @return int|mixed
	 */
	private function getValue(array $arr, $key) {
		return isset($arr[$key]) ? $arr[$key] : 0;
	}

	/**
	 * @param float|int $dividendo
	 * @param float|int $divisor
	 * @param float|int $extra
	 *
	 * @return float|int
	 */
	private function getPercentage($dividendo, $divisor, $extra = 0) {
		$percent = ($divisor == 0 ? $dividendo : $dividendo / $divisor + $extra) * 100;
		// return abs($percent) > 100 ? 100 * ($percent / abs($percent)) : $percent;
		return $percent;
	}

	/**
	 * @param float|int $number
	 *
	 * @return string
	 */
	private function number2str($number, $decimals = 0) {
		return number_format($number, $decimals, ',', '.');
	}

	/**
	 * @param float|int $percent
	 *
	 * @return string
	 */
	private function percent2str($percent, $decimals = 0) {
		return $percent == 0 ? '0 %' : $this->number2str($percent, $decimals) . ' %';
	}
}

$pdf = new KPI_Calidad();
$pdf->currentYear = $currentYear;
$pdf->currentMonth = $currentMonth;
$pdf->legend = [
	['title' => 'Propietario', 'value' => 'Resp. Calidad.'],
	['title' => 'Actualización', 'value' => 'Mensual.'],
	['title' => 'Revisión', 'value' => 'Trimestral.'],
	['title' => 'A', 'value' => $result_a['name']],
	['title' => 'B', 'value' => $result_b['name']]
];
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('KPI Compras');
$pdf->SetTitle('KPI Compras');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(10, 47);
$pdf->SetAutoPageBreak(true, 24);
$pdf->AddPage('L');

$pdf->WriteTable($result_a, $result_b);

//SALIDA
$dir = INCO_DIR_KPIS;
if (!is_dir($dir)) {
	$oldmask = umask(0);
	$aux = mkdir($dir, 0755, true);
	umask($oldmask);
}
$fileName = 'KPI_Calidad.pdf';
$output = $dir . $fileName;
if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

pg_close($dbconn);
