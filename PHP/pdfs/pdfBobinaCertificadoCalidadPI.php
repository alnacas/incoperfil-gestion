<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$factura = trim($argv[2]);
if (empty($factura)) {
	incoLogWrite('No hay argumentos');
}
$pedido = $argv[3];
$idioma = empty($argv[4]) ? '' : $argv[4];
// $tr = new IncoTranslator($DB_STRING, $idioma);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params('SELECT ejercicio, fecha, numero, cliente FROM facturas WHERE identificador = $1', array($factura)) or incoLogWrite('La consulta fallo [facturas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[facturas] No hay resultado');
$albafac = pg_fetch_all($result)[0];

$result = pg_query_params('SELECT DISTINCT referencia_obra, referencia_cliente FROM albaranes WHERE factura = $1', array($factura)) or incoLogWrite('La consulta fallo [facturas_pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[facturas_pedidos] No hay resultado');
$aux = pg_fetch_all($result)[0];
$albafac['referencia_cliente'] = $aux['referencia_cliente'];
$albafac['referencia_obra'] = $aux['referencia_obra'];

$result = pg_query_params('SELECT DISTINCT bobina FROM diario_consumo WHERE pedido = $1 AND bobina > 0 ORDER BY bobina', array($pedido)) or incoLogWrite('La consulta fallo [pedidos_bobinas - uno]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinas] No hay resultado');
$bobaux = pg_fetch_all_columns($result);

$bobinas = array();
for ($i = 0; $i < count($bobaux); $i++) {
    $result = pg_query_params('SELECT b.codigo, co.nombre AS color, ca.abreviatura AS calidad, r.abreviatura AS recubrimiento, e.valor_venta AS espesor, b.ancho, b.resistencia, b.resistencia_maxima, b.alargamiento, case b.certificado when 0 then \'2.2\' when 1 then \'3.1\' else \'\' end as certificado, pa.nombre AS pintura_a, pb.nombre AS pintura_b FROM bobinas b, colores co, calidades ca, recubrimientos r, espesores e, pinturas pa, pinturas pb WHERE b.codigo = $1 AND b.ccolor = co.codigo AND b.calidad = ca.codigo AND b.recubrimiento = r.codigo AND b.espesor = e.codigo AND b.pintura = pa.codigo AND b.pinturab = pb.codigo', array(intval($bobaux[$i]))) or incoLogWrite('La consulta fallo [bobinas]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[bobinas] No hay resultado');

    array_push($bobinas, pg_fetch_all($result)[0]);
    $re = intval($bobinas[$i]['resistencia']);
    $remx = intval($bobinas[$i]['resistencia_maxima']);
    $alar = intval($bobinas[$i]['alargamiento']);
    if ($re == 0 || $remx == 0 || $alar == 0) incoLogWrite('[bobinas] Faltan datos en la(s) bobina(s) ' . implode(' - ', $bobaux) . ' (Re/Re Max/Alargamiento)');
}

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma FROM clientes WHERE codigo = $1 LIMIT 1');

/* $result = pg_execute($dbconn, 'unidadesmedida_ofertaslin', array(intval($ofertalin[0]['unidad_medida'])));
if (pg_num_rows($result) <= 0) incoLogWrite('[unidades_medida] No hay resultado');
$unidadMedida = pg_fetch_all($result)[0]['abreviatura'];*/

$fecha = DateTime::createFromFormat('Y-m-d', $albafac['fecha'])->format('d/m/Y'); // 12 Noviembre 2015 --> j F Y
$cliente = intval($albafac['cliente']);
$referencia_obra = trim($albafac['referencia_obra']);
$referencia_cliente = trim($albafac['referencia_cliente']);

if (empty($cliente) || is_null($cliente) || $cliente <= 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    $result = pg_execute($dbconn, 'clientes', array($cliente));
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $cliente = pg_fetch_all($result)[0];
    $codigo = $cliente['codigo'];
    $nif = $cliente['nif'];
    $nombre = $cliente['nombre'];
    $direccion = $cliente['direccion'];
    $codpo = $cliente['codpo'];
    $poblacion = $cliente['poblacion'];
    $provincia = $cliente['provincia'];
    if (empty($idioma)) $idioma = trim($cliente['idioma']);
}
incoI18nSetLang($idioma, 'certificado_calidad_bobinas');

$result = pg_query_params('SELECT DISTINCT al.identificador_pedido FROM albaraneslin al, albaranes a WHERE factura = $1 AND a.identificador = al.identificador', array($factura)) or incoLogWrite('La consulta fallo [facturas_pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[facturas_pedidos] No hay resultado');
$pedidos = pg_fetch_all_columns($result);

for ($i = 0; $i < count($pedidos); $i++) {
    $pedidos[$i] = intval(substr($pedidos[$i], -7));
}
$pedidos = implode(', ', $pedidos);

class PDF extends TCPDF
{

    function Header()
    {
        global $fecha, $pedidos, $referencia_cliente, $referencia_obra, $codigo, $nombre, $direccion, $codpo, $poblacion, $provincia;

        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
        $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 278, $this->w + 1, 0, 'jpg', '', '', true, 300);

        $this->SetFont('dejavusans', 'B', 22);
        $this->SetY(10);
        // $this->SetXY($this->lMargin + 92, 10);
        // $this->MultiCell(0, 8, gettext('CERTIFICADO DE CALIDAD'), 0, 'R');
        $txt = gettext('CERTIFICADO CALIDAD');
        $txt = strtolower($txt);
        $txt[0] = strtoupper($txt[0]);
        $this->Cell(0, 4, $txt, 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Ln(); // hace salto demasiado grande
        // $this->Cell(0, 4, '', 0, 1);
        $this->Cell(0, 4, sprintf(gettext('Hoja: %d'), $this->PageNo()), 0, 1, 'R');
        $this->Ln();
        /// $this->Ln();
        $y = $this->GetY();

        $this->SetY($y);
        $this->setCellPaddings('', 1, '', 1);
        // Datos de la oferta
        // $y = $this->GetY();
        // $this->Cell(87, 4, '', 0, 1);
        $this->Cell(20, 4, gettext('Fecha'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(37, 4, $fecha, 0, 1);
        $this->SetFont('dejavusans', '');
        $this->Cell(20, 4, gettext('Pedido'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(37, 4, $pedidos, 0, 1);
        $this->SetFont('dejavusans', '');
        $this->Cell(20, 4, gettext('S/Ref'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(37, 4, $referencia_cliente, 0, 1);
        $this->SetFont('dejavusans', '');
        $this->Cell(20, 4, gettext('Obra'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(37, 4, $referencia_obra, 0, 1);

        // Datos del cliente
        $this->setCellPaddings(1, 1, 1, 1);
        $this->SetY($y);
        $x = 100;
        $this->SetX($x);
        $this->Cell(0, 4, $codigo, 'LTR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $nombre, 'LR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $direccion, 'LR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $codpo . ' - ' . $poblacion, 'LR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $provincia, 'LRB', 1);
        $this->Ln();

        $this->Cell(0, 4, gettext('Especificaciones Técnicas y de Calidad de los Productos Utilizados'), 1, 1, 'C');
    }

    function Footer()
    {
        $this->SetY(-45);
        $this->SetFont('dejavusans', 'B');
        $this->setCellPaddings('2', '', '2', '');
        $this->Cell(90, 4, gettext('Normativa Aplicable'), 'LTR', 1, 'C');
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(45, 4, gettext('Normativa de Calidad'), 'L', 0);
        $this->Cell(45, 4, 'EN 10142/00', 'R', 1);
        $this->Cell(45, 4, gettext('Normativa de Tolerancia'), 'L', 0);
        $this->Cell(45, 4, 'EN 10143/93', 'R', 1);
        $this->Cell(45, 4, gettext('Normativa de Fabricación'), 'LB', 0);
        $this->Cell(45, 4, 'EN 10169/97 - EN 10346', 'RB', 1);

        $this->SetXY(137,-45);
        $this->Image(INCO_DIR_IMAGENES . 'valcertificadobobina.jpg', $this->GetX() + 5, $this->GetY() - 5, 50,0, 'jpg', '', 'C', false, 600);
        $this->SetFont('dejavusans', 'B', 12);
        $this->Cell(60, 4, gettext('Resp. Dpto. de Calidad'), 'LTR', 1, 'C');
        $this->SetFont('dejavusans', '', 8);
        $this->SetX(137);
        $this->Cell(60, 4, '', 'LR', 1);
        $this->SetX(137);
        $this->Cell(60, 4, 'José Giner Martínez', 'LR', 1, 'C');
        $this->SetX(137);
        $this->Cell(60, 4, '', 'LRB', 1);

        $this->SetFont('dejavusans', '', 6);
        $this->setCellPaddings('0', '', '0', '');
        $this->Cell(0, 4, gettext('Nota: Los datos que aparecen en este certificado son los mismos que aparecen en el certificado original de calidad del fabricante'), 0, 1);
    }

	public function checkPageBreak( $h = 0, $y = '', $addpage = true ) {
		return parent::checkPageBreak( $h, $y, $addpage ); // TODO: Change the autogenerated stub
	}

}

$pdf = new PDF();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('CERTIFICADO DE CALIDAD');
$pdf->SetTitle('Certificado de Calidad ' . $albafac['numero']);
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(13, 67);
$pdf->SetAutoPageBreak(true, 50);
$pdf->AddPage();

$pdf->setCellPaddings('0', '0', '0', '0');
for ($i = 0; $i < count($bobinas); $i++) {
    $nBobina = intval($bobinas[$i]['codigo']);
    $espesor = floatval($bobinas[$i]['espesor']);
    $color = trim($bobinas[$i]['color']);
    $desarrollo = intval($bobinas[$i]['ancho']);
    $re = floatval($bobinas[$i]['resistencia']);
    $calidad = trim($bobinas[$i]['calidad']);
    $remax = floatval($bobinas[$i]['resistencia_maxima']);
    $recubrimiento = trim($bobinas[$i]['recubrimiento']);
    $alargamiento = floatval($bobinas[$i]['alargamiento']);

	$pdf->checkPageBreak(26, $pdf->GetY());

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Número de bobina'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(75, 4, $nBobina, 0, 1);

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Tipo de certificado'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(75, 4, $bobinas[$i]['certificado'], 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Espesor (mm)'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(25, 4, number_format($espesor, 2, ',', '.'), 0, 1);

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Color'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(75, 4, $color, 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Desarrollo (mm)'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(25, 4, $desarrollo, 0, 1);

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Pintura Cara A'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(75, 4, trim($bobinas[$i]['pintura_a']), 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Pintura Cara B'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(25, 4, trim($bobinas[$i]['pintura_b']), 0, 1);
    $pdf->Ln();

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->Cell(105, 4, '', 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Re (N/mm2)'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(50, 4, number_format($re, 2, ',', '.'), 0, 1);

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Calidad'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(75, 4, $calidad, 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Remax (N/mm2)'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(25, 4, number_format($remax, 2, ',', '.'), 0, 1);

    $pdf->SetX($pdf->GetX() + 15);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('Zinc (g/m2)'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(75, 4, $recubrimiento, 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, gettext('A%'), 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(25, 4, number_format($alargamiento, 2, ',', '.'), 0, 1);
    $pdf->Line($pdf->GetX(), $pdf->GetY(), $pdf->GetX() + 184, $pdf->GetY());
}

//SALIDA
$dir = INCO_DIR_CLIENTES . $codigo . '/Certificados/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'CC_' . $albafac['ejercicio'] . '_' . str_pad($albafac['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
// $fileName = 'CC_2017_006167.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);