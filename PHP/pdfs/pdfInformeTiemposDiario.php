<?php

require_once __DIR__ . '/../config.php';
require __DIR__ . '/../tiempos/registro-tiempos.php';

// argInsertInDb(false);

function remakeArr(array $db) {
    $ret = array();
    for ($i = 0; $i < count($db); $i++) {
        $dia = $db[$i]['dia'];
        unset($db[$i]['dia']);
        $ret[$dia] = $db[$i];
    }
    return $ret;
}

function num2str($number) {
    $decimals = $number > 0 ? 2 : 0;
    return number_format($number, $decimals, ',', '.');
}

incoLogSetFile($argv[1]);

if (empty($argv[2])) {
    incoLogWrite('No hay argumentos');
}

$fecha = DateTime::createFromFormat('Y-m-d', $argv[2]) or incoLogWrite('Error al procesar la fecha');
$fechaDia = intval($fecha->format('d'));

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

argInsertInDb($dbconn, false);

$sql = 'select trim(nombre) as razon_social, trim(nif) as cif, \'\' as ccc from empresa where codigo = 1';
$result = pg_query($dbconn, $sql) or incoLogWrite('La consulta fallo [regtie - empresa]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('No hay empresa [regtie - empresa]: ' . pg_last_error());
$empresa = pg_fetch_all($result)[0];

$sql = 'select codigo, trim(nomape) as nomape, trim(nif) as nif, trim(naf) as naf, trim(horario) as horario from empleados where estado = 1 order by nomape';
$result = pg_query($dbconn, $sql) or incoLogWrite('La consulta fallo [regtie - empleados]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('No hay empleados [regtie - empleados]: ' . pg_last_error());
$empleados = pg_fetch_all($result);

// $sql = 'select extract(day from dt.fecha) as dia, to_char(min(dt.inicio), \'HH24:MI\') as iniman from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.inicio <= \'14:30\' and dt.actividad = a.codigo and (dt.actividad = 44 or a.tipo = 5) group by dia';
$sql = 'select extract(day from fecha) as dia, to_char(manana_entra, \'HH24:MI\') as iniman from historico_tiempos where empleado = $1 and fecha = $2';
pg_prepare($dbconn, 'iniman', $sql);

// $sql = 'select extract(day from dt.fecha) as dia, to_char(max(dt.final), \'HH24:MI\') as finman from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.final < \'14:45\' and dt.actividad = a.codigo and (dt.actividad = 48 or a.tipo = 5) group by dia';
$sql = 'select extract(day from fecha) as dia, to_char(manana_sale, \'HH24:MI\') as finman from historico_tiempos where empleado = $1 and fecha = $2';
pg_prepare($dbconn, 'finman', $sql);

// $sql = 'select extract(day from dt.fecha) as dia, to_char(min(dt.inicio), \'HH24:MI\') as initar from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.inicio >= \'14:45\' and dt.actividad = a.codigo and (dt.actividad = 44 or a.tipo = 5) group by dia';
$sql = 'select extract(day from fecha) as dia, to_char(tarde_entra, \'HH24:MI\') as initar from historico_tiempos where empleado = $1 and fecha = $2';
pg_prepare($dbconn, 'initar', $sql);

// $sql = 'select extract(day from dt.fecha) as dia, to_char(max(dt.final), \'HH24:MI\') as fintar from diario_tiempo dt, actividades a where dt.operario = $1 and dt.fecha = $2 and dt.inicio >= \'14:45\' and dt.actividad = a.codigo and (dt.actividad = 48 or a.tipo = 5) group by dia';
$sql = 'select extract(day from fecha) as dia, to_char(tarde_sale, \'HH24:MI\') as fintar from historico_tiempos where empleado = $1 and fecha = $2';
pg_prepare($dbconn, 'fintar', $sql);

class PDF extends TCPDF {

    /**
     * @var array
     */
    private $empresa;

    public function __construct($empresa, $orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {
        $this->empresa = $empresa;
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);
    }

    public function Header() {
        $this->Ln(15);
        $this->SetFont('dejavusans', '', 8);

        $this->Cell(0, 10, 'REGISTRO DE JORNADA. Trabajadores a tiempo completo', 1, 1, 'C');
        $this->Ln(6);

        $this->Cell(23, 4, 'Razón social:', 0, 0);
        $this->Cell(0, 4, $this->empresa['razon_social'], 0, 1);
        $this->Ln(4);
        $this->Cell(10, 4, 'CIF:', 0, 0);
        $this->Cell(75, 4, $this->empresa['cif'], 0, 0);
        $this->Cell(12, 4, 'CCC:', 0, 0);
        $this->Cell(73, 4, $this->empresa['ccc'], 0, 1);
    }

    public function Footer() {
        $this->SetY(-18);
        $this->SetFont('dejavusans', '', 6);
        $this->MultiCell(0, 4, 'Registro basado en la obligación establecida en el art. 35.5 del Texto Refundido del Estatuto de los Trabajadores (RDL 2/2015 de 23 de octubre' . PHP_EOL, 0);
    }

}

$pdf = new PDF($empresa);
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('Registro de tiempos');
$pdf->SetTitle('Registro de tiempos');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(20, 50);
$pdf->AddPage();

$fecha = $fecha->format('d/m/Y');
$pdf->Cell(37, 4, 'Periodo de liquidación:', 0, 0);
$pdf->Cell(48, 4, '', 0, 0);
$pdf->Cell(12, 4, 'Fecha:', 0, 0);
$pdf->Cell(73, 4, $fecha, 0, 1);
$pdf->Ln(6);

$wE = 50;
$w = ($pdf->getPageWidth() - $pdf->getMargins()['left'] - $pdf->getMargins()['right'] - $wE) / 7;
$h = 5;
$pdf->SetFillColor(235);
$pdf->MultiCell($wE, $h * 2, 'Empleado', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
$pdf->Cell($w * 2, $h, 'Mañana', 1, 0, 'C', true);
$pdf->Cell($w * 2, $h, 'Tarde', 1, 0, 'C', true);
$pdf->MultiCell($w, $h * 2, 'Horas ordinarias', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
$pdf->MultiCell($w, $h * 2, 'Horas extras', 1, 'C', true, 0, '', '', true, 0, false, true, $h * 2, 'M');
$pdf->MultiCell($w, $h * 2, 'Total jornada', 1, 'C', true, 1, '', '', true, 0, false, true, $h * 2, 'M');
$pdf->SetY($pdf->GetY() - $h);
$pdf->SetX($pdf->GetX() + $wE);
$pdf->Cell($w, $h, 'Entrada', 1, 0, 'C', true);
$pdf->Cell($w, $h, 'Salida', 1, 0, 'C', true);
$pdf->Cell($w, $h, 'Entrada', 1, 0, 'C', true);
$pdf->Cell($w, $h, 'Salida', 1, 1, 'C', true);

$jornada = 12;
$formatoHora = 'H:i';
foreach ($empleados as $empleado) {
    $params = array($empleado['codigo'], $fecha);

    $result = pg_execute($dbconn, 'iniman', $params) or incoLogWrite('La consulta fallo [regtie - iniman - ' . $empleado['codigo'] . ']: ' . pg_last_error());
    $iniMan = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result);
    $iniMan = remakeArr($iniMan);

    $result = pg_execute($dbconn, 'finman', $params) or incoLogWrite('La consulta fallo [regtie - finman - ' . $empleado['codigo'] . ']: ' . pg_last_error());
    $finMan = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result);
    $finMan = remakeArr($finMan);

    $result = pg_execute($dbconn, 'initar', $params) or incoLogWrite('La consulta fallo [regtie - initar - ' . $empleado['codigo'] . ']: ' . pg_last_error());
    $iniTar = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result);
    $iniTar = remakeArr($iniTar);

    $result = pg_execute($dbconn, 'fintar', $params) or incoLogWrite('La consulta fallo [regtie - fintar - ' . $empleado['codigo'] . ']: ' . pg_last_error());
    $finTar = pg_num_rows($result) <= 0 ? array() : pg_fetch_all($result);
    $finTar = remakeArr($finTar);

    if (empty($iniMan) && empty($finMan) && empty($iniTar) && empty($finTar)) {
        continue;
    }

    $strIniMan = isset($iniMan[$fechaDia]) ? $iniMan[$fechaDia]['iniman'] : '';
    $strFinMan = isset($finMan[$fechaDia]) ? $finMan[$fechaDia]['finman'] : '';
    $strIniTar = isset($iniTar[$fechaDia]) ? $iniTar[$fechaDia]['initar'] : '';
    $strFinTar = isset($finTar[$fechaDia]) ? $finTar[$fechaDia]['fintar'] : '';

    $dtIniMan = empty($strIniMan) ? null : DateTime::createFromFormat($formatoHora, $strIniMan);
    $dtFinMan = empty($strFinMan) ? null : DateTime::createFromFormat($formatoHora, $strFinMan);
    $dtIniTar = empty($strIniTar) ? null : DateTime::createFromFormat($formatoHora, $strIniTar);
    $dtFinTar = empty($strFinTar) ? null : DateTime::createFromFormat($formatoHora, $strFinTar);

    $horasOrd = 0;
    if (!is_null($dtIniMan) && !is_null($dtFinMan)) {
        $dif = $dtFinMan->diff($dtIniMan);
        $horas = floatval($dif->format('%h'));
        $minutos = round(floatval($dif->format('%i') / 60), 2);
        $horasOrd += ($horas + $minutos - 0.5);
    }
    if (!is_null($dtIniTar) && !is_null($dtFinTar)) {
        $dif = $dtFinTar->diff($dtIniTar);
        $horas = floatval($dif->format('%h'));
        $minutos = round(floatval($dif->format('%i') / 60) * 2) / 2;
        $horasOrd += ($horas + $minutos);
    }
    if (!is_null($dtIniMan) && is_null($dtFinMan) && is_null($dtIniTar) && !is_null($dtFinTar)) {
        $dif = $dtFinTar->diff($dtIniMan);
        $horas = floatval($dif->format('%h'));
        $minutos = round(floatval($dif->format('%i') / 60), 2);
        $horasOrd += ($horas + $minutos - 0.5);
    }

    if ($horasOrd > $jornada) {
        $horasExt = $horasOrd - $jornada;
        $horasOrd = $jornada;
    } else {
        $horasExt = 0;
    }

    $strHorasOrd = isset($iniMan[$fechaDia]) ? num2str($horasOrd) : '';
    $strHorasExt = isset($iniMan[$fechaDia]) ? num2str($horasExt) : '';
    $strHorasJor = isset($iniMan[$fechaDia]) ? num2str($horasOrd + $horasExt) : '';

    $hE = $pdf->getStringHeight($wE, $empleado['nomape']);
    $pdf->MultiCell($wE, $h, $empleado['nomape'], 1, 'L', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strIniMan, 1, 'C', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strFinMan, 1, 'C', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strIniTar, 1, 'C', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strFinTar, 1, 'C', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strHorasOrd, 1, 'C', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strHorasExt, 1, 'C', false, 0, '', '', true, 0, false, true, $h, 'M');
    $pdf->MultiCell($w, $h, $strHorasJor, 1, 'C', false, 1, '', '', true, 0, false, true, $h, 'M');
}

$output = INCO_DIR_DOCUMENTOS . '/InformeTiemposDiario.pdf';
$pdf->Output($output, 'F');