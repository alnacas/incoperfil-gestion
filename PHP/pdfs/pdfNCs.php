<?php

require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/NCsPdf.php';

incoLogSetFile($argv[1]);

$data = json_decode($argv[2], true);

$header_title = $data['show_ac_ap'] ? 'Seguimiento de Riesgos y Acciones Correctivas y Preventivas' : 'Listado de No Conformidades';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$sql = '';
$ncs = [];

$getWhere = function ($field, $value) {
    if (strpos($value, ',') === false) return $field . ' = ' . $value;
    else return $field . ' in (' . $value . ')';
};

$additional_info = [
    ['nc_field' => 'area', 'one' => true, 'sql' => 'select codigo, trim(nombre) as nombre from ncs_areas where codigo = $1', 'nc_sql' => 'area'],
    ['nc_field' => 'bobinas', 'one' => false, 'sql' => 'select linea, bobina, ml_nc from ncs_bobinas where nc = $1 order by linea', 'nc_sql' => 'codigo'],
    ['nc_field' => 'proceso', 'one' => true, 'sql' => 'select codigo, trim(nombre) as nombre from ncs_procesos where codigo = $1', 'nc_sql' => 'proceso'],
    ['nc_field' => 'tipo', 'one' => true, 'sql' => 'select codigo, trim(nombre) as nombre from ncs_tipos where codigo = $1', 'nc_sql' => 'tipo'],
    ['nc_field' => 'emisor', 'one' => true, 'sql' => 'select codigo, trim(nombreusuario) as nombre from wusuario where codigo = $1', 'nc_sql' => 'emisor'],
];

$where = '';
if ($data['type'] == 'year') {
    $header_string = 'Año: ' . $data['year'];
    $where = 'where extract(year from fecha) = ' . $data['year'];
}
if ($data['type'] == 'year+responsable') {
    $header_string = 'Año: ' . $data['year'];
    $where = 'where extract(year from fecha) = ' . $data['year'];
    $responsables = pg_fetch_all(pg_query($dbconn, 'select trim(nombre) as nombre from ncs_areas where codigo in (' . $data['responsable'] . ')'));
    $header_string .= ' - Responsable: ' . implode(', ', array_column($responsables, 'nombre'));
    $where .= ' and ' . $getWhere('area', $data['responsable']);
}
if ($data['type'] == 'responsable') {
    $responsables = pg_fetch_all(pg_query($dbconn, 'select trim(nombre) as nombre from ncs_areas where codigo in (' . $data['responsable'] . ')'));
    $header_string = 'Responsable: ' . implode(', ', array_column($responsables, 'nombre'));
    $where = $data['responsable'] ? 'where ' . $getWhere('area', $data['responsable']) : '';
}
if ($data['type'] == 'nc_type') {
    $nc_types = pg_fetch_all(pg_query($dbconn, 'select trim(nombre) as nombre from ncs_tipos where codigo in (' . $data['nc_type'] . ')'));
    $header_string = 'Tipo NC: ' . implode(', ', array_column($nc_types, 'nombre'));
    $where = $data['nc_type'] ? 'where ' . $getWhere('tipo', $data['nc_type']) : '';
}
if ($data['type'] == 'status') {
    $header_string = 'Estado: ' . ($data['status'] == -1 ? 'Todos' : NCsPdf::ESTADO[$data['status']]);
    $where = $data['status'] == -1 ? '' : 'where ' . $getWhere('area', $data['status']);
}
if ($data['type'] == 'user') {
    $user_where = str_replace('@', "'", $data['where']);
    $header_string = 'Usuario: ' . $user_where;
    $where = 'where ' . $user_where;
}

if ($data['show_ac_ap']) {
    $where .= empty($where) ? 'where ' : ' and ';
    $where .= '(ac = 1 or ap = 1)';
}

if (isset($data['proveedor'])) {
    $where .= empty($where) ? 'where ' : ' and ';
    $proveedor = pg_select($dbconn, 'proveedores', ['codigo' => $data['proveedor']])[0];
    $where .= 'proveedor = ' . $proveedor['codigo'];
    $header_string = sprintf('Proveedor: %s - %s', trim($proveedor['nombre']), $header_string);
}

$sql = "select codigo, tipo, fecha, emisor, trim(descripcion) as descripcion, trim(solucion) as solucion, tramitar_rh, cerrada, proveedor, trim(causa) as causa, trim(accion_correctora) as accion_correctora, trim(correccion) as correccion, ac, ap, fecha_cierre, trim(resultado) as resultado, cliente, trim(responsable) as responsable, trim(disposicion) as disposicion, probabilidad, impacto, proceso, fecha_implantacion, recursos_horas, riesgo, recursos_importe, fecha_envio_tramite, kpi, area, estado from ncs " . $where . " order by fecha, codigo";
if (!empty($sql)) {
    $result = pg_query_params($dbconn, $sql, []);
    $ncs = pg_num_rows($result) == 0 ? [] : pg_fetch_all($result);
}

$ncs = array_map(function ($nc) use ($dbconn, $additional_info) {
    foreach ($additional_info as $item) {
        $result = pg_query_params($dbconn, $item['sql'], [$nc[$item['nc_sql']]]);
        if ($result === false) {
            $nc[$item['nc_field']] = [];
            continue;
        }
        $result = pg_fetch_all($result);
        if ($result === false) {
            $nc[$item['nc_field']] = [];
            continue;
        }
        if ($item['one']) $nc[$item['nc_field']] = empty($result) ? [] : $result[0];
        else $nc[$item['nc_field']] = $result;
    }

    return $nc;
}, $ncs);

$pdf = new NCsPdf($ncs, $data['show_ac_ap']);
$pdf->setHeaderData('', 0, $header_title, $header_string);
$pdf->AddContent();

//SALIDA
$dir = INCO_DIR_NCS;
if (!is_dir($dir)) {
    $oldmask = umask(0);
    $aux = mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'Informe.pdf';
$output = $dir . $fileName;
if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

pg_close($dbconn);