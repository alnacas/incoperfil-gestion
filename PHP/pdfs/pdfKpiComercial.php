<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$subarea = $argv[2];

$config_a = explode(':', $argv[3]);
$config_b = explode(':', $argv[4]);

$opciones_keys = ['desglosar', 'mostrar_mes_curso'];
$opciones_val = explode(':', $argv[5]); // desglosar
$opciones = array_combine($opciones_keys, $opciones_val);

$currentYear = intval((new DateTime())->format('Y'));
$currentMonth = intval((new DateTime())->format('m'));
if ( boolval( $opciones['mostrar_mes_curso'] ) ) {
	$showUntilCurrentMonth = true;
	$currentMonth++;
} else {
	$showUntilCurrentMonth = ( intval( $config_a[1] ) == $currentYear || intval( $config_b[1] ) == $currentYear );
}

/**
 * @param string $filter
 *
 * @return string[]
 */
function getSql($filter) {
    global $pg_execute_nc_name, $nc_area;
	switch ($filter) {
		case 'I':
			$sql = 'select codigo, nombre, zona from vendedor where tipo in (1, 2)';
			$name = 'Todos los vendedores Internacionales';
            $pg_execute_nc_name = 'nc';
            $nc_area = 5;
			break;
		case 'N':
			$sql = 'select codigo, nombre, zona from vendedor where tipo in (0, 2)';
			$name = 'Todos los vendedores Nacionales';
            $pg_execute_nc_name = 'nc';
            $nc_area = 3;
			break;
		case 'T':
			$sql = 'select codigo, nombre, zona from vendedor';
			$name = 'Todos los vendedores';
            $pg_execute_nc_name = 'nc_all';
			break;
		default:
			$sql = 'select codigo, nombre, zona from vendedor where codigo = ' . $filter;
			$name = '';
            $pg_execute_nc_name = 'nc_all';
			break;
	}
	return [$sql, $name];
}

/**
 * @param resource $dbconn
 * @param string $sql
 *
 * @return array
 */
function getVendedores($dbconn, $sql) {
	$pg_result = pg_query($dbconn, $sql) or incoLogWrite('La consulta fallo [vendedores]');
	if (pg_num_rows($pg_result) <= 0) incoLogWrite('[venderores] No hay resultado');
	return pg_fetch_all($pg_result);
}

function getResult($dbconn, $sql, $year, $name, $obj_vendedor = 0) {
	global $showUntilCurrentMonth, $currentMonth, $subarea, $pg_execute_nc_name, $nc_area;

	$vendedores = getVendedores($dbconn, $sql);
	$result = [
		'ofertas' => [
			'uds' => [],
			'importe' => [],
		],
		'pedidos' => [
			'uds' => [],
		],
		'albaranes' => [
			'importe' => [],
		],
		'clientes' => [],
		'nc' => [],
	];
	foreach ($vendedores as $vendedor) {
		if (empty($name)) {
			$name = 'Vendedor: ' . trim($vendedor['nombre']);
		}

		$pg_result = pg_execute($dbconn, 'ofertas', [$vendedor['codigo'], $year]) or incoLogWrite('La consulta fallo [config - ofertas]');
		$ofertas = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($ofertas as $oferta) {
			if ($showUntilCurrentMonth && $oferta['month'] >= $currentMonth) continue;
			if (!isset($result['ofertas']['uds'][$oferta['month']])) $result['ofertas']['uds'][$oferta['month']] = 0;
			if (!isset($result['ofertas']['importe'][$oferta['month']])) $result['ofertas']['importe'][$oferta['month']] = 0.0;
			$result['ofertas']['uds'][$oferta['month']] += $oferta['uds'];
			$result['ofertas']['importe'][$oferta['month']] += ($oferta['importe'] / 1000);
		}

		$pg_result = pg_execute($dbconn, 'pedidos', [$vendedor['codigo'], $year]) or incoLogWrite('La consulta fallo [config - pedidos]');
		$pedidos = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($pedidos as $pedido) {
			if ($showUntilCurrentMonth && $pedido['month'] >= $currentMonth) continue;
			if (!isset($result['pedidos']['uds'][$pedido['month']])) $result['pedidos']['uds'][$pedido['month']] = 0;
			$result['pedidos']['uds'][$pedido['month']] += $pedido['uds'];
		}

		$pg_result = pg_execute($dbconn, 'albaranes', [$vendedor['codigo'], $year]) or incoLogWrite('La consulta fallo [config - albaranes]');
		$albaranes = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($albaranes as $albaran) {
			if ($showUntilCurrentMonth && $albaran['month'] >= $currentMonth) continue;
			if (!isset($result['albaranes']['importe'][$albaran['month']])) $result['albaranes']['importe'][$albaran['month']] = 0.0;
			$result['albaranes']['importe'][$albaran['month']] += ($albaran['importe'] / 1000);
		}

		$pg_result = pg_execute($dbconn, 'clientes', [$vendedor['codigo'], $year]) or incoLogWrite('La consulta fallo [config - clientes]');
		$clientes = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
		foreach ($clientes as $cliente) {
			if ($showUntilCurrentMonth && $cliente['month'] >= $currentMonth) continue;
			if (!isset($result['clientes'][$cliente['month']])) $result['clientes'][$cliente['month']] = 0;
			$result['clientes'][$cliente['month']] += $cliente['uds'];
		}
	}

    $args = [$year];
    if ($pg_execute_nc_name == 'nc') $args[] = $nc_area;
	$pg_result = pg_execute($dbconn, $pg_execute_nc_name, $args) or incoLogWrite('La consulta fallo [config - nc]');
	$ncs = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result);
	foreach ($ncs as $nc) {
		if ($showUntilCurrentMonth && $nc['month'] >= $currentMonth) continue;
		if (!isset($result['nc'][$nc['month']])) $result['nc'][$nc['month']] = 0;
		$result['nc'][$nc['month']] += $nc['value'];
	}

	$pg_result = pg_execute($dbconn, 'objetivos', [$year, $subarea, $obj_vendedor]) or incoLogWrite('La consulta fallo [config - objetivos]');
	$result['objetivos'] = pg_num_rows($pg_result) == 0 ? [] : pg_fetch_all($pg_result)[0];

	$result['name'] = $name . ' - ' . $year;
	$result['year'] = intval($year);
	return $result;
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

pg_prepare($dbconn, 'ofertas', 'select extract(month from fecha) as month, count(*) as uds, sum(importe) as importe from ofertas where vendedor = $1 and extract(year from fecha) = $2 and (oferta_master is null or oferta_master = \'\') group by month order by month');
pg_prepare($dbconn, 'pedidos', 'select extract(month from fecha) as month, count(distinct identificador_oferta) as uds from pedidos where vendedor = $1 and extract(year from fecha) = $2 and serie = \'PC\' group by month order by month');
pg_prepare($dbconn, 'albaranes', 'select extract(month from fecha) as month, sum(base_exenta + base_0 + base_1 + base_2) as importe from albaranes where vendedor = $1 and extract(year from fecha) = $2 and serie = \'AC\' group by month order by month');
pg_prepare($dbconn, 'clientes', 'select extract(month from falta) as month, coalesce(count(*), 0) as uds from clientes where vende = $1 and extract(year from falta) = $2 group by month order by month');
pg_prepare($dbconn, 'objetivos', 'select ofertas_ud, pedidos, eficiencia_op, ofertas_me, albaranes, eficiencia_oa, clientes_nuevos, nc, nc_pedidos from kpi_comercial_objetivos where year = $1 and subarea = $2 and vendedor = $3');
// pg_prepare($dbconn, 'nc', 'select mes as month, val as value from kpi_nc_datos where year = $1 and area = 0 and subarea = $2 order by mes');
pg_prepare($dbconn, 'nc', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and kpi = \'come\' and area = $2 and estado != 2 group by month order by month');
pg_prepare($dbconn, 'nc_all', 'select extract(month from fecha) as month, count(*) as value from ncs where extract(year from fecha) = $1 and kpi = \'come\' and estado != 2 group by month order by month');

list($sql_a, $name_a) = getSql($config_a[0]);
list($sql_b, $name_b) = getSql($config_b[0]);

$result_a = getResult($dbconn, $sql_a, $config_a[1], $name_a);
$result_b = getResult($dbconn, $sql_b, $config_b[1], $name_b);

class KPI_Comercial extends TCPDF
{
	/**
	 * @var string
	 */
	public $header_title;

	/**
	 * @var int
	 */
	public $currentMonth;

	/**
	 * @var int
	 */
	public $currentYear;

	/**
	 * @var array
	 */
	public $legend;

	function Header()
	{
		$this->SetFont('dejavusans', 'B', 14);
		$this->SetY(10);
		$this->Cell(0, 8, $this->header_title, 0, 1, 'C');
		$this->SetFont('dejavusans', '', 8);
		$this->Ln(4);
		$this->WriteLegend();
	}

	function Footer() {
		$this->SetY(-18);

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 3;

		$this->SetFont('dejavusans', '', 8);
		$this->Cell($wCell, 4, 'Código: FCA-01-07', 0, 0, 'L');
		$this->Cell($wCell, 4, 'Revisión: 2', 0, 0, 'C');
		$this->Cell($wCell, 4, 'Fecha: 16/12/2019', 0, 1, 'R');
	}

	private function WriteLegend() {
		foreach ( $this->legend as $item ) {
			$this->SetFont('dejavusans', 'B');
			$this->Cell(30, 4, $item['title'], 0, 0, 'L');
			$this->SetFont('dejavusans', '');
			$this->Cell(0, 4, $item['value'], 0, 1, 'L');
		}
	}

	public function WriteTable( array $result_a, array $result_b ) {
		$h = 4.5;

		if (empty($result_a['ofertas']['uds']) && empty($result_b['ofertas']['uds'])) {
			$this->Cell(0, $h, 'No hay datos para comparar', 0, 1);
			return;
		}

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 19;

		$rows = [
			[
				['with_mult' => 1, 'txt' => 'Nombre', 'align' => 'L'],
				['with_mult' => 3, 'txt' => '03 Actividad', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '04 Resultados', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '05 Eficiencia', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '03 Actividad', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '04 Resultados', 'align' => 'C'],
				['with_mult' => 3, 'txt' => '05 Eficiencia', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Req.', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Calidad Servicio', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Precio Competitivos', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Precio Competitivos', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Calidad Servicio', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Precio Competitivos', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Precio Competitivos', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Proceso', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Cálculo', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Ofertas (ud)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Pedidos (ud)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Eficiencia (%)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Ofertas (m€)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Albaranes (m€)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'Eficiencia (%)', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Mes', 'align' => 'L'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
			]
		];

		$this->SetFont('dejavusans', 'B');
		foreach ( $rows as $row ) {
			foreach ($row as $col) {
				$this->Cell($wCell * $col['with_mult'], $h, $col['txt'], 1, 0, $col['align']);
			}
			$this->Ln();
		}
		$this->SetFont('dejavusans', '');

		$totEficienciaUdA = [];
		$totEficienciaUdB = [];
		$totEficienciaImpA = [];
		$totEficienciaImpB = [];

		for ($i = 0; $i < 12; $i++) {
			$month = $i + 1;

			if (($result_a['year'] == $this->currentYear || $result_b['year'] == $this->currentYear) && $month >= $this->currentMonth) {
				for ($j = 0; $j < count(end($rows)); $j++) {
					$this->Cell($wCell, $h, '', 1, 0, 'C');
				}
				$this->Ln();
				continue;
			}

			$ofertasUdA = $this->getValue($result_a['ofertas']['uds'], $month);
			$ofertasUdB = $this->getValue($result_b['ofertas']['uds'], $month);
			$ofertasUdAB = $this->getPercentage($ofertasUdA, $ofertasUdB, -1);
			$pedidosUdA = $this->getValue($result_a['pedidos']['uds'], $month);
			$pedidosUdB = $this->getValue($result_b['pedidos']['uds'], $month);
			$pedidosUdAB = $this->getPercentage($pedidosUdA, $pedidosUdB, -1);
			$eficienciaUdA = $this->getPercentage($pedidosUdA, $ofertasUdA);
			array_push($totEficienciaUdA, $eficienciaUdA);
			$eficienciaUdB = $this->getPercentage($pedidosUdB, $ofertasUdB);
			array_push($totEficienciaUdB, $eficienciaUdB);
			$eficienciaUdAB = $this->getPercentage($eficienciaUdA, $eficienciaUdB, -1);
			$ofertasImpA = $this->getValue($result_a['ofertas']['importe'], $month);
			$ofertasImpB = $this->getValue($result_b['ofertas']['importe'], $month);
			$ofertasImpAB = $this->getPercentage($ofertasImpA, $ofertasImpB, -1);
			$albaranesImpA = $this->getValue($result_a['albaranes']['importe'], $month);
			$albaranesImpB = $this->getValue($result_b['albaranes']['importe'], $month);
			$albaranesImpAB = $this->getPercentage($albaranesImpA, $albaranesImpB, -1);
			$eficienciaImpA = $this->getPercentage($albaranesImpA, $ofertasImpA);
			array_push($totEficienciaImpA, $eficienciaImpA);
			$eficienciaImpB = $this->getPercentage($albaranesImpB, $ofertasImpB);
			array_push($totEficienciaImpB, $eficienciaImpB);
			$eficienciaImpAB = $this->getPercentage($eficienciaImpA, $eficienciaImpB, -1);
			$this->Cell($wCell, $h, $month, 1, 0, 'C');
			$this->Cell($wCell, $h, $ofertasUdA, 1, 0, 'C');
			$this->Cell($wCell, $h, $ofertasUdB, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ofertasUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $pedidosUdA, 1, 0, 'C');
			$this->Cell($wCell, $h, $pedidosUdB, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($pedidosUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaUdA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaUdB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($ofertasImpA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($ofertasImpB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ofertasImpAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($albaranesImpA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->number2str($albaranesImpB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($albaranesImpAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaImpA), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaImpB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($eficienciaImpAB), 1, 1, 'C');
		}

		$totOfertasUdA = array_sum($result_a['ofertas']['uds']);
		$totOfertasUdB = array_sum($result_b['ofertas']['uds']);
		$totOfertasUdAB = $this->getPercentage($totOfertasUdA, $totOfertasUdB, -1);
		$totPedidosUdA = array_sum($result_a['pedidos']['uds']);
		$totPedidosUdB = array_sum($result_b['pedidos']['uds']);
		$totPedidosUdAB = $this->getPercentage($totPedidosUdA, $totPedidosUdB, -1);
		$totEficienciaUdA = $this->getDivision(array_sum($totEficienciaUdA), count($totEficienciaUdA));
		$totEficienciaUdB = $this->getDivision(array_sum($totEficienciaUdB), count($totEficienciaUdB));
		$totEficienciaUdAB = $this->getPercentage($totEficienciaUdA, $totEficienciaUdB, -1);
		$totOfertasImpA = array_sum($result_a['ofertas']['importe']);
		$totOfertasImpB = array_sum($result_b['ofertas']['importe']);
		$totOfertasImpAB = $this->getPercentage($totOfertasImpA, $totOfertasImpB, -1);
		$totAlbaranesImpA = array_sum($result_a['albaranes']['importe']);
		$totAlbaranesImpB = array_sum($result_b['albaranes']['importe']);
		$totAlbaranesImpAB = $this->getPercentage($totAlbaranesImpA, $totAlbaranesImpB, -1);
		$totEficienciaImpA = $this->getDivision(array_sum($totEficienciaImpA), count($totEficienciaImpA));
		$totEficienciaImpB = $this->getDivision(array_sum($totEficienciaImpB), count($totEficienciaImpB));
		$totEficienciaImpAB = $this->getPercentage($totEficienciaImpA, $totEficienciaImpB, -1);
		$this->Cell($wCell, $h, 'Totales', 1, 0, 'C');
		$this->Cell($wCell, $h, $totOfertasUdA, 1, 0, 'C');
		$this->Cell($wCell, $h, $totOfertasUdB, 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totOfertasUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $totPedidosUdA, 1, 0, 'C');
		$this->Cell($wCell, $h, $totPedidosUdB, 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totPedidosUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaUdA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaUdB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totOfertasImpA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totOfertasImpB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totOfertasImpAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totAlbaranesImpA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($totAlbaranesImpB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totAlbaranesImpAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaImpA), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaImpB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totEficienciaImpAB), 1, 1, 'C');

		$medOfertasUdA = empty(count($result_a['ofertas']['uds'])) ? 0 : $totOfertasUdA / count($result_a['ofertas']['uds']);
		$medPedidosUdA = empty(count($result_a['pedidos']['uds'])) ? 0 : $totPedidosUdA / count($result_a['pedidos']['uds']);
		$medOfertasImpA = empty(count($result_a['ofertas']['importe'])) ? 0 : $totOfertasImpA / count($result_a['ofertas']['importe']);
		$medAlbaranesImpA = empty(count($result_a['albaranes']['importe'])) ? 0 : $totAlbaranesImpA / count($result_a['albaranes']['importe']);
		$this->Cell($wCell, $h, 'Medias', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medOfertasUdA), 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medPedidosUdA), 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medOfertasImpA), 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, $this->number2str($medAlbaranesImpA), 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 1, 'C');

		$this->Cell($wCell, $h, 'Objs.', 1, 0, 'C');
		$obj_fields = ['ofertas_ud', 'pedidos', 'eficiencia_op', 'ofertas_me', 'albaranes', 'eficiencia_oa'];
		foreach ( $obj_fields as $obj_field ) {
			if (!isset($result_a['objetivos'][$obj_field])) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				continue;
			}

			if (strpos($result_a['objetivos'][$obj_field], '%') != false) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
			} else {
				$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
				$this->Cell($wCell, $h, $objB, 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
			}
		}
		$this->Ln();
		$this->Ln();

		$this->checkPageBreak($h * 16);

		$rows = [
			[
				['with_mult' => 1, 'txt' => 'Nombre', 'align' => 'L'],
				['with_mult' => 3, 'txt' => '17 Clientes Nuevos', 'align' => 'C'],
				['with_mult' => 5, 'txt' => '09 NC y RQ', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Req.', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Calidad Servicio', 'align' => 'C'],
				['with_mult' => 5, 'txt' => 'Calidad Servicio', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Proceso', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
				['with_mult' => 5, 'txt' => 'PGE 01 Ofertas', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Cálculo', 'align' => 'L'],
				['with_mult' => 3, 'txt' => 'Clientes Nuevos (ud)', 'align' => 'C'],
				['with_mult' => 3, 'txt' => 'NC (ud)', 'align' => 'C'],
				['with_mult' => 2, 'txt' => 'NC / Ped (%)', 'align' => 'C'],
			],
			[
				['with_mult' => 1, 'txt' => 'Mes', 'align' => 'L'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A / B', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'A', 'align' => 'C'],
				['with_mult' => 1, 'txt' => 'B', 'align' => 'C'],
			]
		];

		$this->SetFont('dejavusans', 'B');
		foreach ( $rows as $row ) {
			foreach ($row as $col) {
				$this->Cell($wCell * $col['with_mult'], $h, $col['txt'], 1, 0, $col['align']);
			}
			$this->Ln();
		}
		$this->SetFont('dejavusans', '');

		$totNcPedidosA = [];
		$totNcPedidosB = [];
		for ($i = 0; $i < 12; $i++) {
			$month = $i + 1;

			if (($result_a['year'] == $this->currentYear || $result_b['year'] == $this->currentYear) && $month >= $this->currentMonth) {
				for ($j = 0; $j < count(end($rows)); $j++) {
					$this->Cell($wCell, $h, '', 1, 0, 'C');
				}
				$this->Ln();
				continue;
			}

			$clientesUdA = $this->getValue($result_a['clientes'], $month);
			$clientesUdB = $this->getValue($result_b['clientes'], $month);
			$clientesUdAB = $this->getPercentage($clientesUdA, $clientesUdB, -1);
			$ncUdA = $this->getValue($result_a['nc'], $month);
			$ncUdB = $this->getValue($result_b['nc'], $month);
			$ncUdAB = $this->getPercentage($ncUdA, $ncUdB, -1);
			$ncPedidosA = $this->getPercentage($ncUdA, $this->getValue($result_a['pedidos']['uds'], $month));
			array_push($totNcPedidosA, $ncPedidosA);
			$ncPedidosB = $this->getPercentage($ncUdB, $this->getValue($result_b['pedidos']['uds'], $month));
			array_push($totNcPedidosB, $ncPedidosB);
			$this->Cell($wCell, $h, $month, 1, 0, 'C');
			$this->Cell($wCell, $h, $clientesUdA, 1, 0, 'C');
			$this->Cell($wCell, $h, $clientesUdB, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($clientesUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $ncUdA, 1, 0, 'C');
			$this->Cell($wCell, $h, $ncUdB, 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncUdAB), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPedidosA, 2), 1, 0, 'C');
			$this->Cell($wCell, $h, $this->percent2str($ncPedidosB, 2), 1, 1, 'C');
		}

		$totClientesUdA = array_sum($result_a['clientes']);
		$totClientesUdB = array_sum($result_b['clientes']);
		$totClientesUdAB = $this->getPercentage($totClientesUdA, $totClientesUdB, -1);
		$totNcUdA = array_sum($result_a['nc']);
		$totNcUdB = array_sum($result_b['nc']);
		$totNcUdAB = $this->getPercentage($totNcUdA, $totNcUdB, -1);
		$totNcPedidosA = $this->getDivision(array_sum($totNcPedidosA), count($totNcPedidosA));
		$totNcPedidosB = $this->getDivision(array_sum($totNcPedidosB), count($totNcPedidosB));
		$this->Cell($wCell, $h, 'Totales', 1, 0, 'C');
		$this->Cell($wCell, $h, $totClientesUdA, 1, 0, 'C');
		$this->Cell($wCell, $h, $totClientesUdB, 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totClientesUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $totNcUdA, 1, 0, 'C');
		$this->Cell($wCell, $h, $totNcUdB, 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNcUdAB), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNcPedidosA, 2), 1, 0, 'C');
		$this->Cell($wCell, $h, $this->percent2str($totNcPedidosB, 2), 1, 1, 'C');

		$this->Cell($wCell, $h, 'Medias', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 0, 'C');
		$this->Cell($wCell, $h, '', 1, 1, 'C');

		$this->Cell($wCell, $h, 'Objs.', 1, 0, 'C');
		$obj_fields = ['clientes_nuevos', 'nc'];
		foreach ( $obj_fields as $obj_field ) {
			if (!isset($result_a['objetivos'][$obj_field])) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				continue;
			}

			if (strpos($result_a['objetivos'][$obj_field], '%') != false) {
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
			} else {
				$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
				$this->Cell($wCell, $h, $result_a['objetivos'][$obj_field], 1, 0, 'C');
				$this->Cell($wCell, $h, $objB, 1, 0, 'C');
				$this->Cell($wCell, $h, '', 1, 0, 'C');
			}
		}
		if ( isset( $result_a['objetivos']['nc_pedidos'] ) ) {
			$objB = isset($result_b['objetivos'][$obj_field]) ? $result_b['objetivos'][$obj_field] : '';
			$this->Cell( $wCell, $h, $result_a['objetivos']['nc_pedidos'], 1, 0, 'C' );
			$this->Cell( $wCell, $h, $objB, 1, 0, 'C' );
		} else {
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
			$this->Cell( $wCell, $h, '', 1, 0, 'C' );
		}
	}

	/**
	 * @param array $arr
	 * @param string|int $key
	 *
	 * @return int|mixed
	 */
	private function getValue(array $arr, $key) {
		return isset($arr[$key]) ? $arr[$key] : 0;
	}

    /**
     * @param float|int $dividendo
     * @param float|int $divisor
     * @return float|int
     */
    private function getDivision($dividendo, $divisor) {
        return $divisor == 0 ? $dividendo : $dividendo / $divisor;
    }

	/**
	 * @param float|int $dividendo
	 * @param float|int $divisor
	 * @param float|int $extra
	 *
	 * @return float|int
	 */
	private function getPercentage($dividendo, $divisor, $extra = 0) {
		$percent = ($divisor == 0 ? $dividendo : $dividendo / $divisor + $extra) * 100;
		// return abs($percent) > 100 ? 100 * ($percent / abs($percent)) : $percent;
        return $percent;
	}

	/**
	 * @param float|int $number
	 *
	 * @return string
	 */
	private function number2str($number, $decimals = 0) {
		return number_format($number, $decimals, ',', '.');
	}

	/**
	 * @param float|int $percent
	 *
	 * @return string
	 */
	private function percent2str($percent, $decimals = 0) {
		return $percent == 0 ? '0 %' : $this->number2str($percent, $decimals) . ' %';
	}

	/**
	 * @param array $vendedores_a
	 * @param array $vendedores_b
	 */
	public function WriteDesglose( array $vendedores_a, array $vendedores_b ) {
		for ($i = 0; $i < count($vendedores_a); $i++) {
			$this->header_title = 'KPI COMERCIAL - Desglose';
			$this->legend[3]['value'] = $vendedores_a[$i]['result']['name'];
			$this->legend[4]['value'] = $vendedores_b[$i]['result']['name'];

			$this->AddPage();
			$this->Ln(6);
			$this->WriteTable($vendedores_a[$i]['result'], $vendedores_b[$i]['result']);
		}
	}
}

$pdf = new KPI_Comercial();
$pdf->header_title = 'KPI COMERCIAL';
$pdf->currentYear = $currentYear;
$pdf->currentMonth = $currentMonth;
$pdf->legend = [
	['title' => 'Propietario', 'value' => 'Resp. Comercial.'],
	['title' => 'Actualización', 'value' => 'Mensual.'],
	['title' => 'Revisión', 'value' => 'Trimestral.'],
	['title' => 'A', 'value' => $result_a['name']],
	['title' => 'B', 'value' => $result_b['name']]
];
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('KPI Comercial');
$pdf->SetTitle('KPI Comercial');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(10, 47);
$pdf->SetAutoPageBreak(true, 24);
$pdf->AddPage('L');

$pdf->WriteTable($result_a, $result_b);

if (boolval($opciones['desglosar'])) {
	$vendedores_a = array_map(function ($vendedor) use ($dbconn, $config_a) {
		list($sql, $name) = getSql($vendedor['codigo']);
		$vendedor['result'] = getResult($dbconn, $sql, $config_a[1], $name, $vendedor['codigo']);
		$vendedor['result']['name'] .= ' ' . trim($vendedor['zona']);
		return $vendedor;
	}, getVendedores($dbconn, $sql_a));
	$vendedores_b = array_map(function ($vendedor) use ($dbconn, $config_b) {
		list($sql, $name) = getSql($vendedor['codigo']);
		$vendedor['result'] = getResult($dbconn, $sql, $config_b[1], $name, $vendedor['codigo']);
		$vendedor['result']['name'] .= ' ' . trim($vendedor['zona']);
		return $vendedor;
	}, getVendedores($dbconn, $sql_b));
	$pdf->WriteDesglose($vendedores_a, $vendedores_b);
}

//SALIDA
$dir = INCO_DIR_KPIS;
if (!is_dir($dir)) {
	$oldmask = umask(0);
	$aux = mkdir($dir, 0755, true);
	umask($oldmask);
}
$fileName = 'KPI_Comercial.pdf';
$output = $dir . $fileName;
if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

pg_close($dbconn);
