<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el pedido
$result = pg_query_params('SELECT numero, cliente, referencia_obra, observaciones, fecha, fecha_entrega, fecha_produccion, hora_entrega, referencia_cliente FROM pedidos WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$pedido = pg_fetch_all($result)[0];

// obtenemos las lineas del pedido
$result = pg_query_params('SELECT pl.linea, pl.c_posicion, pl.c_curvado, pl.c_perforado, pl.c_cantidad, pl.c_cantidad_pendiente, pl.c_longitud, paquete, pl.cantidad_total, pl.cantidad_total_pendiente, pl.peso, pl.referencia_plano, (pro.nombre || \' \' || e.nombre || \' \' || pin.abreviatura || \' \' || c.nombre || case pl.ccolorb when 1 then \'\' else \'/\' || c2.nombre end)::VARCHAR(127) AS nombre, pl.estado FROM pedidoslin pl, productos pro, espesores e, pinturas pin, colores c, colores c2 WHERE pl.identificador = $1 AND pl.seccion = 1 AND pl.producto = pro.codigo AND pl.espesor = e.codigo AND pl.pintura = pin.codigo AND pl.ccolor = c.codigo AND pl.ccolorb = c2.codigo ORDER BY pl.paquete, pl.linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
$pedidoslin = pg_fetch_all($result);

$result = pg_query_params('SELECT SUM(peso)::NUMERIC(10,3) AS pesotot, SUM(cantidad_total)::NUMERIC(10,3) AS mltot FROM pedidoslin WHERE identificador = $1 AND seccion = 1', array($identificador)) or incoLogWrite('La consulta fallo [pesotot]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pesotot] No hay resultado');
$pesotot = pg_fetch_all($result)[0]['pesotot'];
$mltot = pg_fetch_all($result)[0]['mltot'];

$result = pg_query_params('SELECT aviso FROM pedidos_avisos WHERE identificador = $1 and tipo = 1', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) $pedido_avisos = '';
else {
    $pedido_avisos = ' - ' . implode('. ', array_map('trim', array_column(pg_fetch_all($result), 'aviso')));
}

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_pedidos', 'SELECT codigo, nombre FROM clientes WHERE codigo = $1 LIMIT 1');

// preparamos para obtener el perforado
pg_prepare($dbconn, 'perforaciones', 'SELECT codigo, nombre FROM perforaciones WHERE codigo = $1 LIMIT 1');

pg_prepare($dbconn, 'pesopaq', 'SELECT SUM(peso)::NUMERIC(10,3) AS pesopaq FROM pedidoslin WHERE identificador = $1 AND seccion = 1 AND paquete = $2');

$numero_pedido = $pedido['numero'];
$fecha_entrega = $pedido['fecha_entrega']; // date_format(date_create($pedido['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$hora_entrega = $pedido['hora_entrega']; // date_format(date_create($pedido['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$cliente = intval($pedido['cliente']);
$referencia_obra = trim($pedido['referencia_obra']);
$referencia_cliente = trim($pedido['referencia_cliente']);
$observaciones = trim($pedido['observaciones']) . $pedido_avisos;

if (is_null($cliente) || $cliente < 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    $result = pg_execute($dbconn, 'clientes_pedidos', array($cliente));
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $cliente = pg_fetch_all($result)[0];
    $codigo = $cliente['codigo'];
    $nombre = $cliente['nombre'];
}

class PDF extends TCPDF
{
    function Header()
    {
        $this->SetFont('dejavusans', 'B', 24);
        $this->SetY(10);
        $this->Cell(0, 8, 'CHAPA PERFILADA', 0, 1, 'C');
        $this->SetFont('');
        $this->SetFontSize(8);
        $this->Ln(4);
        $this->Cell(0, 4, 'Hoja: ' . $this->PageNo(), 0, 1, 'R');
    }

    function Footer()
    {
        global $pesotot, $mltot;
        $this->SetY(-15);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(60, 8, number_format($pesotot, 0, ',', '.') . ' (KG)', 0, 0);
        $this->Cell(60, 8, number_format($mltot, 0, ',', '.') . ' (ML)', 0, 1);
    }
}

$pdf = new PDF();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('OFERTA');
$pdf->SetTitle('Oferta X');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(13, 33);
$pdf->AddPage();

// Datos de la oferta
$y = $pdf->GetY();
$pdf->Cell(40, 4, 'CLIENTE', 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(95, 4, $codigo . ' - ' . $nombre, 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(40, 4, 'PEDIDO', 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $numero_pedido, 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(40, 4, 'REFERENCIA DE OBRA', 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $referencia_obra, 0, 1);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(40, 4, 'PED. CL.', 0, 0);
$pdf->Cell(0, 4, $referencia_cliente, 0, 1);
$pdf->Cell(40, 4, 'OBSERVACIONES', 0, 0);
$pdf->Cell(0, 4, $observaciones, 0, 1);

// Datos del cliente
$pdf->SetY($y);
$x = 151;
$pdf->SetX($x);
$fProd = DateTime::createFromFormat('Y-m-d', $pedido['fecha_produccion']);
$fProd = $fProd ? $fProd->format('d-m-Y') : '';
$pdf->Cell(30, 4, 'F. PRODUCCIÓN: ', 0, 0);
$pdf->Cell(20, 4, $fProd, 0, 1);
$pdf->SetX($x);
$pdf->Cell(50, 4, '', 0, 1); // $pdf->Cell(50, 4, 'FECHA FAB.: ', 0, 1);
// $pdf->Cell(37, 4, '--', 0, 1);
$pdf->SetX($x);
$pdf->Cell(50, 4, '', 0, 1); // $pdf->Cell(50, 4, 'FECHA EXP.: ', 0, 1);
// $pdf->Cell(37, 4, '--', 0, 1);
$pdf->SetX($x);
$pdf->Cell(50, 4, '', 0, 1); // $pdf->Cell(50, 4, 'PQTS.: ', 0, 1);
// $pdf->Cell(37, 4, '--', 0, 1);
$pdf->SetX($x);
$pdf->Cell(50, 4, '', 0, 1); // $pdf->Cell(50, 4, 'ALBARAN: ', 0, 1);
// $pdf->Cell(37, 4, '--', 0, 1);
$pdf->Ln();
$pdf->Ln();

$pdf->SetX(5);
// Productos de la oferta. Cabecera
$pdf->SetFillColor(224); // Oscuro: 192
$pdf->Cell(8, 4, 'Lin', 1, 0, 'C', true);
$pdf->Cell(65, 4, 'Descripción', 1, 0, 'C', true);
$pdf->Cell(9, 4, 'C/F', 1, 0, 'C', true);
$pdf->Cell(9, 4, 'Curv', 1, 0, 'C', true);
$pdf->Cell(9, 4, 'Perf', 1, 0, 'C', true);
$pdf->Cell(30, 4, 'Zona', 1, 0, 'C', true);
$pdf->Cell(10, 4, 'Uds', 1, 0, 'C', true);
$pdf->Cell(14, 4, 'Long.', 1, 0, 'C', true);
$pdf->Cell(8, 4, 'Paq', 1, 0, 'C', true);
$pdf->Cell(12, 4, 'ML', 1, 0, 'C', true);
$pdf->Cell(14, 4, 'Kgs', 1, 0, 'C', true);
$pdf->Cell(12, 4, 'Est', 1, 1, 'C', true);
// Productos de la oferta. Lineas
$lastPaq = 0;
$prevDescripcion = '';
$prevPosicion = '';
for ($i = 0; $i < count($pedidoslin); $i++) {
    $linea = $pedidoslin[$i]['linea'];
    /*$result = pg_execute($dbconn, 'descripcion_corta', array($identificador, $linea));
    if (pg_num_rows($result) <= 0) incoLogWrite('[desc corta] No hay resultado');*/
    $descripcion = trim($pedidoslin[$i]['nombre']);
    // $descripcion = trim($pedidoslin[$i]['descripcion']);
    switch (intval($pedidoslin[$i]['c_posicion'])) {
        case 1: $posicion = 'CUB'; break;
        case 2: $posicion = 'FAC'; break;
        default: $posicion = 'NO'; break;
    }
    switch (intval($pedidoslin[$i]['c_curvado'])) {
        case 1: $curvado = 'NORM'; break;
        case 2: $curvado = 'CUMB'; break;
        case 3: $curvado = 'ESQUI'; break;
        default: $curvado = 'NO'; break;
    }
    $perforado = intval($pedidoslin[$i]['c_perforado']);
    $result = pg_execute($dbconn, 'perforaciones', array($perforado));
    if (pg_num_rows($result) <= 0) incoLogWrite('[perforaciones] No hay resultado');
    $perforado = trim(pg_fetch_all($result)[0]['nombre']);
    $cantidad = $pedidoslin[$i]['c_cantidad'];
    if ($pedidoslin[$i]['c_cantidad'] != $pedidoslin[$i]['c_cantidad_pendiente']) {
    	$cantidad = '*' . $cantidad;
    }
    $longitud = $pedidoslin[$i]['c_longitud'];
    $paquete = $pedidoslin[$i]['paquete'];
    $metroslineales = floatval($pedidoslin[$i]['cantidad_total_pendiente']);
    $metroslineales = number_format($metroslineales, 0, ',', '.');
    $peso = floatval($pedidoslin[$i]['peso']);
    $peso = number_format($peso, 0, ',', '.');
    $refPlano = trim($pedidoslin[$i]['referencia_plano']);
    if ($pedidoslin[$i]['estado'] == 0) $estado = 'PTE';
    elseif ($pedidoslin[$i]['estado'] == 1) $estado = 'TER';
    elseif ($pedidoslin[$i]['estado'] == 2) $estado = 'SER';
    elseif ($pedidoslin[$i]['estado'] == 3) $estado = 'CAN';
    else $estado = 'FAB';


    if ($lastPaq != $paquete) {
        $result = pg_execute($dbconn, 'pesopaq', array($identificador, $paquete));
        if (pg_num_rows($result) <= 0) incoLogWrite('[pesopaq] No hay resultado');
        $peso = pg_fetch_all($result)[0]['pesopaq'];
        $peso = number_format($peso, 0, ',', '.');
        $lastPaq = $paquete;
    } else {
        $peso = '';
    }

    if ((!empty($prevDescripcion) && $prevDescripcion != $descripcion) || (!empty($prevPosicion) && $prevPosicion != $posicion)) {
        $pdf->Ln(5);
    }

    $pdf->SetX(5);
    if ($pdf->GetStringWidth($descripcion) >= 65) {
        $lines = ceil($pdf->GetStringWidth($descripcion) / 65);
        $height = $lines * 4;
        $pdf->Cell(8, $height, $linea, 1, 0, 'R');
        $pdf->MultiCell(65, $height, $descripcion, 1, 'L', false, 0);
        $pdf->Cell(9, $height, $posicion, 1, 0, 'R');
        $pdf->Cell(9, $height, $curvado, 1, 0, 'R');
        $pdf->Cell(9, $height, $perforado, 1, 0, 'R');
        $pdf->Cell(30, $height, $refPlano, 1, 0, 'R');
        $pdf->Cell(10, $height, $cantidad, 1, 0, 'R');
        $pdf->Cell(14, $height, $longitud, 1, 0, 'R');
        $pdf->Cell(8, $height, $paquete, 1, 0, 'R');
        $pdf->Cell(12, $height, $metroslineales, 1, 0, 'R');
        $pdf->Cell(14, $height, $peso, 1, 0, 'R');
        $pdf->Cell(12, $height, $estado, 1, 1, 'C');
    } else {
        $pdf->Cell(8, 5, $linea, 1, 0, 'R');
        $pdf->Cell(65, 5, $descripcion, 1, 0, 'L');
        $pdf->Cell(9, 5, $posicion, 1, 0, 'R');
        $pdf->Cell(9, 5, $curvado, 1, 0, 'R');
        $pdf->Cell(9, 5, $perforado, 1, 0, 'R');
        $pdf->Cell(30, 5, $refPlano, 1, 0, 'R');
        $pdf->Cell(10, 5, $cantidad, 1, 0, 'R');
        $pdf->Cell(14, 5, $longitud, 1, 0, 'R');
        $pdf->Cell(8, 5, $paquete, 1, 0, 'R');
        $pdf->Cell(12, 5, $metroslineales, 1, 0, 'R');
        $pdf->Cell(14, 5, $peso, 1, 0, 'R');
        $pdf->Cell(12, 5, $estado, 1, 1, 'C');
    }

    $prevDescripcion = $descripcion;
    $prevPosicion = $posicion;
}

$result = pg_query_params('select string_agg(distinct s.nombre, \' - \') as secciones from pedidoslin pl, secciones s where pl.identificador = $1 and pl.seccion = s.codigo', array($identificador)) or incoLogWrite('La consulta fallo [secciones]: ' . pg_last_error());
$secciones = trim(pg_fetch_all($result)[0]['secciones']);
$pdf->Ln(5);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $secciones, 0, 1, 'C');
$pdf->SetFont('dejavusans', '');

// CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
$pdf->SetFont('dejavusans', '', 8);
$style = array('align' => 'C', 'fitwidth' => true, 'border' => false, 'text' => true, 'font' => 'helvetica', 'fontsize' => 5);
$result = pg_query_params('select distinct producto from pedidoslin where identificador = $1 and seccion = 1', array($identificador)) or incoLogWrite('La consulta fallo [codbars - 1] ' . pg_last_error());
$productos = pg_fetch_all($result);
foreach ($productos as $row) {
    $pdf->Ln(5);
    $producto = $row['producto'];
    $result = pg_query_params('select nombre from productos where codigo = $1', array($producto)) or incoLogWrite('La consulta fallo [codbars - 2] ' . pg_last_error());
    $nombre = trim(pg_fetch_all($result)[0]['nombre']);
    if (empty($nombre)) {
        continue;
    }

    $result = pg_query_params('select pp.maquina from procesos_producto pp where pp.producto = $1 and pp.proceso = 1', array($producto)) or incoLogWrite('La consulta fallo [codbars - 3] ' . pg_last_error());
    $maquina = trim(pg_fetch_all($result)[0]['maquina']);
    if (empty($maquina)) {
        continue;
    }

    $pdf->Cell(0, 4, $nombre, 0, 1);
    $codbar = str_pad($numero_pedido, 6, '0', STR_PAD_LEFT) .
        str_pad($producto, 3, '0', STR_PAD_LEFT) . '07' . '2' .
        str_pad($maquina, 2, '0', STR_PAD_LEFT);
    $pdf->write1DBarcode($codbar, 'C39', '', '', 65, 9, 1, $style, 'N');
}

//SALIDA
$dir = INCO_DIR_TALLER;
if (!is_dir($dir)) {
    $oldmask = umask(0);
    $aux = mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'TC_' . str_pad($numero_pedido, 6, '0', STR_PAD_LEFT) . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
