<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$idioma = isset($argv[3]) ? trim($argv[3]) : '';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos datos del oferta
$result = pg_query_params('SELECT centro, ejercicio, numero, revision, referencia_obra, fecha, cliente, potencial, iva, forma_pago, forma_pago_alternativa, portes, portes_importe, transporte, condiciones, texto_factura, portes_numero FROM ofertas WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$oferta = pg_fetch_all($result)[0];
$oferta['centro'] = intval($oferta['centro']);
$oferta['fecha'] = DateTime::createFromFormat('Y-m-d', $oferta['fecha'])->format('d/m/Y');

$result = pg_query_params('SELECT iva0, iva1, iva2 FROM centrofac WHERE codigo = $1', array($oferta['centro'])) or incoLogWrite('La consulta fallo [centrofac]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[centrofac] No hay resultado');
$centrofac = pg_fetch_all($result)[0];

if ($oferta['iva'] == 0) {
    $oferta['tipo_iva_0'] = 0.0;
    $oferta['tipo_iva_1'] = 0.0;
    $oferta['tipo_iva_2'] = 0.0;
} else {
    $oferta['tipo_iva_0'] = floatval($centrofac['iva0']);
    $oferta['tipo_iva_1'] = floatval($centrofac['iva1']);
    $oferta['tipo_iva_2'] = floatval($centrofac['iva2']);
}

/*$result = pg_query_params('SELECT pl.importe, pl.importe_exceso FROM portes p, porteslin pl, pedidos pe
    WHERE pe.identificador_oferta = $1 AND p.estado = 0 AND pl.pedido = pe.identificador AND pl.porte = p.codigo', array($identificador))
or incoLogWrite('La consulta fallo [portes]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[portes] No hay resultado');
$porte = pg_fetch_all($result)[0];*/

// preparamos para obtener la forma de pago
pg_prepare($dbconn, 'fopago_facturas', 'SELECT nombre, banco, recargo_financiero, descuento_pronto_pago FROM fopago WHERE codigo = $1');
// preparamos para obtener el banco
pg_prepare($dbconn, 'bancos_facturas', 'SELECT nombre, (iban || cbanco || csucursal || cdc || cuenta)::CHARACTER(63) AS cuenta FROM bancos WHERE codigo = $1');
$formaPago = trim($oferta['forma_pago']);
$formaPagoAlt = trim($oferta['forma_pago_alternativa']);
if ((empty($formaPago) || is_null($formaPago)) && (empty($formaPagoAlt) || is_null($formaPagoAlt))) {
    incoLogWrite('[forma_pago] No hay valor');
} else {
    if (empty($formaPagoAlt) || is_null($formaPagoAlt)) {
        $result = pg_execute($dbconn, 'fopago_facturas', array($formaPago));
        if (pg_num_rows($result) <= 0) incoLogWrite('[forma_pago] No hay resultado');
        $aux = pg_fetch_all($result)[0];
        $oferta['descuento_pronto_pago'] = floatval($aux['descuento_pronto_pago']);
        $oferta['recargo_financiero'] = floatval($aux['recargo_financiero']);
        $formaPago = trim($aux['nombre']);

        if ($aux['banco'] == 0) {
            $banco = array();
        } else {
            $result = pg_execute($dbconn, 'bancos_facturas', array($aux['banco']));
            if (pg_num_rows($result) <= 0) incoLogWrite('[banco] No hay resultado');
            $banco = pg_fetch_all($result)[0];
        }
    } else {
        $banco = array();
        $formaPago = str_replace('\\n', PHP_EOL, $formaPagoAlt);
        $oferta['descuento_pronto_pago'] = floatval(0);
        $oferta['recargo_financiero'] = floatval(0);
    }
}

$result = pg_query_params('SELECT identificador, seccion, producto, descripcion_comercial, cantidad AS metros, unidad_medida, precio, (cantidad * precio)::NUMERIC(10,2) AS importe FROM ofertaslin WHERE identificador = $1 ORDER BY seccion', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
$ofertalin = pg_fetch_all($result);

$oferta['base_exenta'] = 0;
$oferta['base_0'] = 0;
$oferta['base_1'] = 0;
$oferta['base_2'] = 0;

if ($oferta['iva'] == 1) {
    pg_prepare($dbconn, 'productos_tipoiva', 'SELECT tipo_iva FROM productos WHERE codigo = $1 LIMIT 1');
    for ($i = 0; $i < count($ofertalin); $i++) {
        $result = pg_execute($dbconn, 'productos_tipoiva', array($ofertalin[$i]['producto']));
        if (pg_num_rows($result) <= 0) incoLogWrite('[productos - iva] No hay resultado');
        $tipoiva = intval(pg_fetch_all($result)[0]['tipo_iva']);
        switch ($tipoiva) {
            case 0:
                $oferta['base_0'] += $ofertalin[$i]['importe'];
                break;
            case 1:
                $oferta['base_1'] += $ofertalin[$i]['importe'];
                break;
            case 2:
                $oferta['base_2'] += $ofertalin[$i]['importe'];
                break;
        }
    }
    if (intval($oferta['portes']) == 0 || intval($oferta['portes']) == 3) $oferta['base_0'] += (intval($oferta['portes_numero']) * floatval($oferta['portes_importe'])); // portes en factura
} else {
    for ($i = 0; $i < count($ofertalin); $i++) {
        $oferta['base_exenta'] += $ofertalin[$i]['importe'];
    }
    if (intval($oferta['portes']) == 0 || intval($oferta['portes']) == 3) $oferta['base_exenta'] += (intval($oferta['portes_numero']) * floatval($oferta['portes_importe'])); // portes en factura
}

$oferta['bruto'] = $oferta['base_exenta'] + $oferta['base_0'] + $oferta['base_1'] + $oferta['base_2'];

$dpp_e = $oferta['base_exenta'] * ($oferta['descuento_pronto_pago'] / 100);
$dpp_0 = $oferta['base_0'] * ($oferta['descuento_pronto_pago'] / 100);
$dpp_1 = $oferta['base_1'] * ($oferta['descuento_pronto_pago'] / 100);
$dpp_2 = $oferta['base_2'] * ($oferta['descuento_pronto_pago'] / 100);
$oferta['total_descuento_pronto_pago'] = $dpp_e + $dpp_0 + $dpp_1 + $dpp_2;

$oferta['base_exenta'] = $oferta['base_exenta'] - $dpp_e;
$oferta['base_0'] = $oferta['base_0'] - $dpp_0;
$oferta['base_1'] = $oferta['base_1'] - $dpp_1;
$oferta['base_2'] = $oferta['base_2'] - $dpp_2;

$oferta['total_iva_0'] = $oferta['base_0'] * ($oferta['tipo_iva_0'] / 100);
$oferta['total_iva_1'] = $oferta['base_1'] * ($oferta['tipo_iva_1'] / 100);
$oferta['total_iva_2'] = $oferta['base_2'] * ($oferta['tipo_iva_2'] / 100);
$total_iva = $oferta['total_iva_0'] + $oferta['total_iva_1'] + $oferta['total_iva_2'];

$base_imp = $oferta['bruto'] - $oferta['total_descuento_pronto_pago'];
$total_factura = $base_imp + $total_iva;
$total_recargo = $total_factura * ($oferta['recargo_financiero'] / 100);
$total_factura += $total_recargo;
$oferta['total_factura'] = $total_factura;

/*$result = pg_query_params('SELECT fvto, importe FROM efectos WHERE identif_fra = $1', array($identificador));
if (pg_num_rows($result) <= 0) incoLogWrite('[efectos] No hay resultado');
$efectos = pg_fetch_all($result);*/

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma FROM clientes WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'potenciales', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma FROM potenciales WHERE codigo = $1 LIMIT 1');

// preparamos para obtener la unidad de medida de cada linea del pedido
pg_prepare($dbconn, 'unidadesmedida_albaranlin', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1 LIMIT 1');

// $pedidoCli = trim($albaran['referencia_cliente']);
$cliente = $oferta['cliente'];

if (empty($cliente) || is_null($cliente) || $cliente <= 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    if (boolval($oferta['potencial'])) {
        $result = pg_execute($dbconn, 'potenciales', array($cliente));
    } else {
        $result = pg_execute($dbconn, 'clientes', array($cliente));
    }
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $aux = pg_fetch_all($result)[0];
    $cliente = array();
    $cliente['codigo'] = $aux['codigo'];
    $cliente['nif'] = trim($aux['nif']);
    $cliente['nombre'] = trim($aux['nombre']);
    $cliente['direccion'] = trim($aux['direccion']);
    $cliente['codpo'] = $aux['codpo'];
    $cliente['poblacion'] = trim($aux['poblacion']);
    $cliente['provincia'] = trim($aux['provincia']);
    if (empty($idioma)) $idioma = trim($aux['idioma']);
    $tr = new IncoTranslatorFix($dbconn, 'proforma', $idioma);
}

class PDF extends TCPDF
{
    /**
     * @var IncoTranslatorFix
     */
    private $tr;

    /**
     * @var array
     */
    private $oferta;

    /**
     * @var array
     */
    private $ofertalin;

    /**
     * @var array
     */
    private $cliente;

    /**
     * @var string
     */
    private $formaPago;

    /**
     * @var array
     */
    private $banco;

    /**
     * @var array
     */
    private $efectos;

    /**
     * @param IncoTranslatorFix $tr
     */
    public function setTr($tr)
    {
        $this->tr = $tr;
    }

    /**
     * @param array $oferta
     */
    public function setOferta($oferta)
    {
        $this->oferta = $oferta;
    }

    /**
     * @param array $ofertalin
     */
    public function setOfertalin($ofertalin)
    {
        $this->ofertalin = $ofertalin;
    }

    /**
     * @param array $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @param string $formaPago
     */
    public function setFormaPago($formaPago)
    {
        $this->formaPago = $formaPago;
    }

    /**
     * @param array $banco
     */
    public function setBanco($banco)
    {
        $this->banco = $banco;
    }

    /**
     * @param array $efectos
     */
    public function setEfectos($efectos)
    {
        $this->efectos = $efectos;
    }

    function Header()
    {
	    $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', 20, 10, 60, 0, 'jpg', '', '', true, 300);
        $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 247, 211, 0, 'jpg', '', '', true, 300);
        // $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 252, 211, 45, 'jpg', '', '', true, 300);

        $this->SetY(5);
        $this->SetFont('dejavusans', 'B', 24);
        $this->Cell(0, 4, $this->tr->getTranslate('title'), 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 4, $this->tr->getTranslate('h_1') . ' ' . $this->PageNo(), 0, 1, 'R');
        $y = $this->GetY();

        $this->SetY(54);
        $this->Cell(25, 4, $this->tr->getTranslate('h_2'), 0, 0);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(60, 4, 'O/' . str_pad($this->oferta['numero'], 6, '0', STR_PAD_LEFT), 0, 1);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(25, 4, $this->tr->getTranslate('h_3'), 0, 0);
        $this->Cell(60, 4, $this->oferta['fecha'], 0, 1);
        $this->Cell(25, 4, $this->tr->getTranslate('h_4'), 0, 0);
        $this->Cell(60, 4, $this->cliente['nif'], 0, 1);

        $this->SetY($y);
        $this->Ln();
        $this->Ln();
        $this->SetFont('dejavusans', '', 8);
        $this->setCellPaddings('3', '2', '3', '2');
        $this->Ln();
        $this->SetX(100);
        $this->Cell(0, 4, str_pad($this->cliente['codigo'], 5, '0', STR_PAD_LEFT), 'LTR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['nombre']), 'LR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['direccion']), 'LR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['codpo']) . ' - ' . trim($this->cliente['poblacion']), 'LR', 1);
        $this->SetX(100);
        $this->Cell(0, 4, trim($this->cliente['provincia']), 'LRB', 1);
        $this->Ln(2);

        // $this->SetX(90);
        // $this->MultiCell(0, 12, $this->observaciones, 0, 'L', false, 1);

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('4', '0', '0', '0');

        $this->SetCellPadding(0);
        $this->SetXY(0, 90);
        $this->Cell(10, 4, '--');
        $this->SetXY(0, 200);
        $this->Cell(10, 4, '--');
    }

    function Footer()
    {
        $this->SetY(-52);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(30, 4, $this->tr->getTranslate('f_1'), 1, 0, 'C');
        if ($this->oferta['descuento_pronto_pago'] == 0) {
            $this->Cell(65, 4, '', 1, 0, 'C');
        } else {
            $this->Cell(65, 4, $this->tr->getTranslate('Descuento Pronto Pago'), 1, 0, 'C');
        }
        $this->Cell(30, 4, $this->tr->getTranslate('f_2'), 1, 0, 'C');
        $this->Cell(18, 4, $this->tr->getTranslate('f_3'), 1, 0, 'C');
        $this->Cell(25, 4, $this->tr->getTranslate('f_4'), 1, 0, 'C');
        $this->Cell(0, 4, $this->tr->getTranslate('f_5', true), 1, 1, 'C');

        $this->setCellPaddings('0', '0', '2', '0');
        $this->Cell(30, 4, number_format($this->oferta['bruto'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
        if ($this->oferta['descuento_pronto_pago'] == 0) {
            $this->Cell(65, 4, '', 'LTR', 0, 'C');
        } else {
            $this->Cell(65, 4, number_format($this->oferta['descuento_pronto_pago'], 0) . '% s/ ' . number_format($this->oferta['bruto'], 2, ',', '.') . ' € = ' . number_format($this->oferta['total_descuento_pronto_pago'] * -1, 2, ',', '.') . ' €', 'LTR', 0, 'C');
        }
        $this->Cell(30, 4, number_format($this->oferta['base_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
        $this->Cell(18, 4, number_format($this->oferta['tipo_iva_0'], 0) . ' %', 'LTR', 0, 'C');
        $this->Cell(25, 4, number_format($this->oferta['total_iva_0'], 2, ',', '.') . ' €', 'LTR', 0, 'R');
        $this->SetFont('dejavusans', 'B');
        $this->Cell(0, 4, number_format($this->oferta['total_factura'], 2, ',', '.') . ' €', 'LTR', 1, 'R');

        if ($this->oferta['base_1'] == 0) {
            $this->Cell(30, 4, '', 'LBR', 0, 'R');
            $this->Cell(65, 4, '', 'LBR', 0, 'C');
            $this->Cell(30, 4, '', 'LBR', 0, 'R');
            $this->Cell(18, 4, '', 'LBR', 0, 'C');
            $this->Cell(25, 4, '', 'LBR', 0, 'R');
            $this->Cell(0, 4, '', 'LBR', 1, 'R');
        } else {
            $this->Cell(30, 4, '', 'LBR', 0, 'R');
            $this->Cell(65, 4, '', 'LBR', 0, 'C');
            $this->Cell(30, 4, number_format($this->oferta['base_1'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
            $this->Cell(18, 4, number_format($this->oferta['tipo_iva_1'], 0) . ' %', 'LBR', 0, 'C');
            $this->Cell(25, 4, number_format($this->oferta['total_iva_1'], 2, ',', '.') . ' €', 'LBR', 0, 'R');
            $this->SetFont('dejavusans', 'B');
            $this->Cell(0, 4, '', 'LBR', 1, 'R');
        }
        $this->Ln(2);

        $vencimientos = array();
        $importes = array();
        for ($i = 0; $i < count($this->efectos); $i++) {
            if (intval($this->efectos[$i]['orden']) == 0) {
                array_push($vencimientos, 'Anticipado');
            } else {
                $fvto = explode('-', $this->efectos[$i]['fvto']);
                array_push($vencimientos, $fvto[2] . '/' . $fvto[1] . '/' . $fvto[0]);
            }
            array_push($importes, number_format($this->efectos[$i]['importe'], 2, ',', '.') . ' €');
        }

        $this->SetFont('dejavusans', '');
        $this->SetCellPadding(0);
        $this->Cell(30, 4, $this->tr->getTranslate('f_6'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $formaPago = $this->tr->getTranslate($this->formaPago);
        if (!empty($this->banco)) {
            $formaPago .= ' -- ' . trim($this->banco['nombre']);
            $aux = str_split(trim($this->banco['cuenta']), 4);
            $formaPago .= ' -- ' . implode('-', $aux);
        }
        // $this->Cell(0, 4, $formaPago, 0, 1);
        $this->MultiCell(0, 4, $formaPago, 0, 'L', false, 1);
        /*if (!(floatval($this->factura['anticipo_con_factura']) > 0 && floatval($this->factura['total_factura']) == 0)) {
            $this->SetFont('dejavusans', '');
            $this->Cell(30, 4, 'Vencimientos', 0, 0);
            $this->SetFont('dejavusans', 'B');
            for ($i = 0; $i < count($vencimientos); $i++) {
                $ln = ($i == count($vencimientos) - 1) ? 1 : 0;
                $this->Cell(20, 4, $vencimientos[$i], 0, $ln, 'C');
            }
            $this->SetFont('dejavusans', '');
            $this->Cell(30, 4, 'Importes', 0, 0);
            $this->SetFont('dejavusans', 'B');
            for ($i = 0; $i < count($importes); $i++) {
                $ln = ($i == count($importes) - 1) ? 1 : 0;
                $this->Cell(20, 4, $importes[$i], 0, $ln, 'C');
            }
        }*/
        $this->SetY(-30);
        $this->SetX(35);
        $this->Cell(30, 4, $this->tr->getTranslate('f_7', true), 0, 1);
    }
}

$pdf = new PDF();
$pdf->setTr($tr);
$pdf->setOferta($oferta);
$pdf->setOfertalin($ofertalin);
$pdf->setCliente($cliente);
$pdf->setFormaPago($formaPago);
$pdf->setBanco($banco);

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('FACTURA');
$pdf->SetTitle('Factura X');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 73); // 70
$pdf->AddPage();

$pdf->Cell(25, 4, $tr->getTranslate('b_1', true), 1, 0, 'C');
$pdf->Cell(115, 4, $tr->getTranslate('b_2', true), 1, 0, 'C');
$pdf->Cell(20, 4, $tr->getTranslate('b_3', true), 1, 0, 'C');
$pdf->Cell(15, 4, '% ' . $tr->getTranslate('b_4', true), 1, 0, 'C');
$pdf->Cell(0, 4, $tr->getTranslate('b_5', true), 1, 1, 'C');

$ultimoAlbaran = '';
$cabecera = false;
for ($i = 0; $i < count($ofertalin); $i++) {
    $aux = $ofertalin[$i]['identificador'];
    if ($i == 0) {
        $ultimoAlbaran = $aux;
        $cabecera = true;
    } elseif ($ultimoAlbaran != $aux) {
        $pdf->Cell(25, 4, '', 'LR', 0, 'C');
        $pdf->Cell(115, 4, '', 'LR', 0, 'C');
        $pdf->Cell(20, 4, '', 'LR', 0, 'C');
        $pdf->Cell(15, 4, '', 'LR', 0, 'C');
        $pdf->Cell(0, 4, '', 'LR', 1, 'C');
        $ultimoAlbaran = $aux;
        $cabecera = true;
    }

    if ($cabecera) {
        $pdf->Cell(25, 4, '', 'LR', 0, 'C');
        $pdf->Cell(115, 4, $tr->getTranslate('b_9', true) . ': ' . trim($oferta['referencia_obra']), 'LR', 0, 'L');
        $pdf->Cell(20, 4, '', 'LR', 0, 'C');
        $pdf->Cell(15, 4, '', 'LR', 0, 'C');
        $pdf->Cell(0, 4, '', 'LR', 1, 'C');
        $cabecera = false;
    }

    $cantidad = floatval($ofertalin[$i]['metros']);
    $result = pg_execute($dbconn, 'unidadesmedida_albaranlin', array(intval($ofertalin[$i]['unidad_medida'])));
    if (pg_num_rows($result) <= 0) incoLogWrite('[unidades_medida] No hay resultado');
    $unidad_medida = trim(pg_fetch_all($result)[0]['abreviatura']);
    $unidad_medida = str_replace('2', html_entity_decode('&sup2;', ENT_COMPAT, 'UTF-8'), $unidad_medida);
    $descripcion = $tr->getTranslate(trim($ofertalin[$i]['descripcion_comercial']));
    $precio = floatval($ofertalin[$i]['precio']);
    // if (floatval($ofertalin[$i]['descuento']) == 0) {
        $descuento = '';
    /*} else {
        $descuento = floatval($ofertalin[$i]['descuento']);
        $descuento = number_format($descuento, 2, ',', '.');
    }*/
    $importe = floatval($ofertalin[$i]['importe']);
    // $lines = ceil($pdf->GetStringWidth($descripcion) / 110);
    // $height = $lines * 4;

    if ($cantidad == 0 || $importe == 0) {
        continue;
    }

    $height = ceil($pdf->getStringHeight(115, $descripcion, true));
    $aux = explode(' | ', $descripcion);
    if (count($aux) > 1) {
        $descripcion = str_replace(' | ', PHP_EOL, $descripcion);
        // $height += 4;
        $height = ceil($pdf->getStringHeight(115, $descripcion, true));
    }

    if ($pdf->GetY() + $height >= 242) {
        $pdf->Cell(25, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(115, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(20, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(15, 242 - $pdf->GetY(), '', 'LRB', 0);
        $pdf->Cell(0, 242 - $pdf->GetY(), '', 'LRB', 1);
        $pdf->AddPage();
        $pdf->Cell(25, 4, $tr->getTranslate('b_1', true), 1, 0, 'C');
        $pdf->Cell(115, 4, $tr->getTranslate('b_2', true), 1, 0, 'C');
        $pdf->Cell(20, 4, $tr->getTranslate('b_3', true), 1, 0, 'C');
        $pdf->Cell(15, 4, '% ' . $tr->getTranslate('b_4', true), 1, 0, 'C');
        $pdf->Cell(0, 4, $tr->getTranslate('b_5', true), 1, 1, 'C');
    }

    $pdf->Cell(18, $height, number_format($cantidad, 2, ',', '.'), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(7, $height, $unidad_medida, 'R', 0, 'C', false, '', 0, false, 'T', 'T');
    $pdf->MultiCell(115, $height, $descripcion, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, number_format($precio, 2, ',', '.'), 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(15, $height, $descuento, 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
    $pdf->Cell(0, $height, number_format($importe, 2, ',', '.'), 'LR', 1, 'R', false, '', 0, false, 'T', 'T');
}

if (intval($oferta['portes']) == 0 || intval($oferta['portes']) == 3) {
    // portes en factura
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, '', 'LR', 0, 'C');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');
    // $portes = floatval($porte['importe']);
    $portes = intval($oferta['portes_numero']) * floatval($oferta['portes_importe']);
    $txt = $tr->getTranslate('b_10', true);
    /*$trans = trim($oferta['transporte']);
    if (!empty($trans) && !is_null($trans)) {
        $txt = $trans;
    }
    $lines = ceil($pdf->GetStringWidth($txt) / 113);
    $lines += substr_count($txt, PHP_EOL);*/
    $height = $height = ceil($pdf->getStringHeight(115, $txt, true));

    $pdf->Cell(18, $height, $oferta['portes_numero'], 'L', 0, 'R');
    $pdf->Cell(7, $height, '', 'R', 0, 'C');
    // $pdf->Cell(115, $height, $txt, 'LR', 0);
    $pdf->MultiCell(115, $height, $txt . PHP_EOL, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, number_format($oferta['portes_importe'], 2, ',', '.'), 'LR', 0, 'R');
    $pdf->Cell(15, $height, '', 'LR', 0, 'R');
    $pdf->Cell(0, $height, number_format($portes, 2, ',', '.'), 'LR', 1, 'R');
}

$txtFactura = trim($oferta['texto_factura']);
if (!is_null($txtFactura) && !empty($txtFactura)) {
    $pdf->Cell(25, 4, '', 'LR', 0, 'C');
    $pdf->Cell(115, 4, '', 'LR', 0, 'C');
    $pdf->Cell(20, 4, '', 'LR', 0, 'C');
    $pdf->Cell(15, 4, '', 'LR', 0, 'C');
    $pdf->Cell(0, 4, '', 'LR', 1, 'C');

    $txt = $tr->getTranslate($txtFactura);
    $height = ceil($pdf->getStringHeight(115, $txt));

    $pdf->Cell(25, $height, '', 'LR', 0, 'R');
    // $pdf->Cell(115, $height, $txt, 'LR', 0);
    $pdf->MultiCell(115, $height, $txt . PHP_EOL, 'LR', 'L', false, 0);
    $pdf->Cell(20, $height, '', 'LR', 0, 'R');
    $pdf->Cell(15, $height, '', 'LR', 0, 'R');
    $pdf->Cell(0, $height, '', 'LR', 1, 'R');
}

$pdf->Cell(25, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(115, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(20, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(15, 242 - $pdf->GetY(), '', 'LRB', 0);
$pdf->Cell(0, 242 - $pdf->GetY(), '', 'LRB', 1);

// 245
$tipo = boolval($oferta['potencial']) ? INCO_DIR_POTENCIALES : INCO_DIR_CLIENTES;
$dir = $tipo . $cliente['codigo'] . '/FacturasProforma/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'FP_O_' . $oferta['ejercicio'] . '_' . str_pad($oferta['numero'], 6, '0', STR_PAD_LEFT) . '_' . $oferta['revision'] . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
