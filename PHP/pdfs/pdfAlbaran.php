<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$idioma = isset($argv[3]) ? trim($argv[3]) : '';
// $tr = new IncoTranslator($DB_STRING, $idioma);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el albaran
$result = pg_query_params('SELECT ejercicio, numero, fecha, cliente, nombre, direccion, codigo_postal, poblacion, provincia, contacto, referencia_obra, referencia_cliente, observaciones, peso, portes, porte, total_portes, total_portes_exceso, bultos, agencia, transportista, conductor, vehiculo, identificador_oferta, cliente_as_transportista FROM albaranes WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite( 'La consulta fallo [albaranes]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes] No hay resultado');
$albaran = pg_fetch_all($result)[0];

// obtenemos las lineas del albaran
$result = pg_query_params('SELECT linea, seccion, descripcion_comercial, cantidad, metros, unidad_medida, posicion, curvado, perforado, plegados, longitud, paquete, identificador_pedido, linea_pedido, referencia_plano FROM albaraneslin WHERE identificador = $1 ORDER BY seccion ASC, identificador_pedido ASC, paquete ASC, longitud DESC', array($identificador)) or incoLogWrite( 'La consulta fallo [albaraneslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaraneslin] No hay resultado');
$albaranlin = pg_fetch_all($result);

// obtenemos los distintos pedidos a los que pertenecen las lineas del albaran
$result = pg_query_params('SELECT DISTINCT identificador_pedido FROM albaraneslin WHERE identificador LIKE $1', array($identificador)) or incoLogWrite( 'La consulta fallo [albaraneslin - pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaraneslin - pedidos] No hay resultado');
$pedidos = pg_fetch_all($result)[0];

// obtenemos el minimo pedido del albaran para el codigo de barras
$result = pg_query_params('SELECT MIN(DISTINCT identificador_pedido) AS ped FROM albaraneslin WHERE identificador LIKE $1', array($identificador)) or incoLogWrite( 'La consulta fallo [albaraneslin - pedidos codbar]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[albaraneslin - pedidos codbar] No hay resultado');
$pedcodbar = pg_fetch_all($result)[0]['ped'];
$pedcodbar = intval(substr($pedcodbar, -7));

pg_prepare($dbconn, 'pedidos_portes', 'SELECT portes_numero, portes_realizados FROM pedidos WHERE numero = $1');

pg_prepare($dbconn, 'ofertas_portes', 'SELECT portes_numero, portes_realizados FROM ofertas WHERE identificador = $1');

// pg_prepare($dbconn, 'composicion', 'SELECT * FROM pedidoslin2 WHERE identificador = $1 AND linea = $2 AND estado = 1 ORDER BY seccion ASC, linea2');
// pg_prepare($dbconn, 'composicion', 'SELECT * FROM pedidoslin2 WHERE identificador = $1 AND linea = $2 AND estado = 2 ORDER BY seccion ASC, linea2');
pg_prepare($dbconn, 'composicion', 'SELECT * FROM albaraneslin2 WHERE identificador = $1 AND linea = $2 ORDER BY seccion ASC, linea2');

if ($albaran['cliente_as_transportista'] == 1) {
    $result = pg_query_params('SELECT nombre, nif, direccion, codpo, poblacion, provincia FROM clientes WHERE codigo = $1', array($albaran['cliente'])) or incoLogWrite( 'La consulta fallo [albaranes - transportista]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes - transportista] No hay resultado');
    $transportista = pg_fetch_all($result)[0];
    $agencia = $transportista;
} else {
    // obtenemos la agencia
    if (intval($albaran['agencia']) == 0) {
        $agencia = array('nombre' => '', 'nif' => '', 'direccion' => '', 'codpo' => '', 'poblacion' => '', 'provincia' => '');
    } else {
        $result = pg_query_params('SELECT nombre, nif, direccion, codpo, poblacion, provincia FROM proveedores WHERE codigo = $1', array($albaran['agencia'])) or incoLogWrite( 'La consulta fallo [albaranes - agencia]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes - agencia] No hay resultado');
        $agencia = pg_fetch_all($result)[0];
    }

    // obtenemos el transportista
    if (intval($albaran['transportista']) == 0) {
        $transportista = array('nombre' => '', 'nif' => '', 'direccion' => '', 'codpo' => '', 'poblacion' => '', 'provincia' => '');
    } else {
        $result = pg_query_params('SELECT nombre, nif, direccion, codigo_postal AS codpo, poblacion, provincia FROM transportistas WHERE codigo = $1', array($albaran['transportista'])) or incoLogWrite( 'La consulta fallo [albaranes - transportista]: ' . pg_last_error());
        if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes - transportista] No hay resultado');
        $transportista = pg_fetch_all($result)[0];
        if (intval($albaran['portes']) == 2) {
            $agencia = $transportista;
        }
    }
}


// obtenemos el vehiculo
if (intval($albaran['vehiculo']) == 0) {
    $vehiculo = array('matricula' => '', 'matricula_remolque' => '');
} else {
	$result = pg_query_params('SELECT matricula, matricula_remolque FROM vehiculos WHERE codigo = $1', array($albaran['vehiculo'])) or incoLogWrite( 'La consulta fallo [albaranes - agencia]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes - agencia] No hay resultado');
    $vehiculo = pg_fetch_all($result)[0];
}

// obtenemos el conductor
if (intval($albaran['conductor']) == 0) {
    $conductor = array('nombre' => '', 'dni' => '');
} else {
	$result = pg_query_params('SELECT nombre, dni FROM conductores WHERE codigo = $1', array($albaran['conductor'])) or incoLogWrite( 'La consulta fallo [albaranes - conductor]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes - conductor] No hay resultado');
    $conductor = pg_fetch_all($result)[0];
}

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_albaranes', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma FROM clientes WHERE codigo = $1 LIMIT 1');

// preparamos para obtener la unidad de medida de cada linea del albaran
pg_prepare($dbconn, 'unidadesmedida_albaranlin', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1 LIMIT 1');

$numero = $albaran['numero'];
$pedidosNum = array();
foreach ($pedidos as $pedido) {
    $pedidosNum[] = substr($pedido, -6);
}
$pedidos = implode(' ', $pedidosNum);
$fecha = DateTime::createFromFormat('Y-m-d', $albaran['fecha'])->format('d/m/Y');
$porte = $albaran['porte'];
$cliente = $albaran['cliente'];
$dirEntrega = array();
$dirEntrega['referencia_obra'] = trim($albaran['referencia_obra']);
$dirEntrega['nombre'] = trim($albaran['nombre']);
$dirEntrega['direccion'] = trim($albaran['direccion']);
$dirEntrega['codigo_postal'] = trim($albaran['codigo_postal']);
$dirEntrega['poblacion'] = trim($albaran['poblacion']);
$dirEntrega['provincia'] = trim($albaran['provincia']);
$observaciones = trim($albaran['observaciones']);
$peso = $albaran['peso'];
$bultos = $albaran['bultos'];

if (empty($cliente) || is_null($cliente) || $cliente <= 0) {
    incoLogWrite('[cliente] No hay valor');
} else {
    $result = pg_execute($dbconn, 'clientes_albaranes', array($cliente));
    if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
    $aux = pg_fetch_all($result)[0];
    $cliente = array();
    $cliente['codigo'] = $aux['codigo'];
    $cliente['nif'] = trim($aux['nif']);
    $cliente['nombre'] = trim($aux['nombre']);
    $cliente['direccion'] = trim($aux['direccion']);
    $cliente['codpo'] = $aux['codpo'];
    $cliente['poblacion'] = trim($aux['poblacion']);
    $cliente['provincia'] = trim($aux['provincia']);
    if (empty($idioma)) $idioma = trim($aux['idioma']);
}

if ($porte == 0) {
    $portePedidos = 0;
} else {
    $result = pg_query_params('SELECT count(distinct pedido) as total FROM porteslin WHERE porte = $1', [$porte]) or incoLogWrite( 'La consulta fallo [albaranes - porteslin]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[albaranes - porteslin] No hay resultado');
    $portePedidos = pg_fetch_all($result)[0]['total'];
}

incoI18nSetLang($idioma, 'albaran');

class PDF extends TCPDF
{
    private static $STYLE = array('align' => 'C', 'fitwidth' => true, 'border' => false, 'text' => true, 'font' => 'helvetica', 'fontsize' => 5);

    /**
     * @var int
     */
    private $numero;

    /**
     * @var string
     */
    private $pedidos;

    /**
     * @var string
     */
    private $fecha;

    /**
     * @var string
     */
    private $porte;

    private $portePedidos;

    /**
     * @var array
     */
    private $agencia;

    /**
     * @var array
     */
    private $transportista;

    /**
     * @var array
     */
    private $vehiculo;

    /**
     * @var array
     */
    private $conductor;

    /**
     * @var array
     */
    private $dirEntrega;

    /**
     * @var array
     */
    private $cliente;

    /**
     * @var string
     */
    private $observaciones;

    /**
     * @var float
     */
    private $peso;

    /**
     * @var float
     */
    private $bultos;

    /**
     * @param int $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @param string $pedidos
     */
    public function setPedidos($pedidos)
    {
        $this->pedidos = $pedidos;
    }

    /**
     * @param string $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @param string $porte
     */
    public function setPorte($porte)
    {
        $this->porte = $porte;
    }

    /**
     * @param $portePedidos
     */
    public function setPortePedidos($portePedidos)
    {
        $this->portePedidos = $portePedidos;
    }

    /**
     * @param array $agencia
     */
    public function setAgencia($agencia)
    {
        $this->agencia = $agencia;
    }

    /**
     * @param array $transportista
     */
    public function setTransportista($transportista)
    {
        $this->transportista = $transportista;
    }

    /**
     * @param array $vehiculo
     */
    public function setVehiculo($vehiculo)
    {
        $this->vehiculo = $vehiculo;
    }

    /**
     * @param array $conductor
     */
    public function setConductor($conductor)
    {
        $this->conductor = $conductor;
    }

    /**
     * @param array $dirEntrega
     */
    public function setDirEntrega($dirEntrega)
    {
        $this->dirEntrega = $dirEntrega;
    }

    /**
     * @param array $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @param string $observaciones
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    }

    /**
     * @param float $peso
     */
    public function setPeso($peso)
    {
        $this->peso = $peso;
    }

    /**
     * @param float $bultos
     */
    public function setBultos($bultos)
    {
        $this->bultos = $bultos;
    }

    function Header()
    {
        global $pedcodbar;

        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->SetY(5);
        $this->SetFont('dejavusans', 'B', 14);
        $numero = str_pad($this->numero, 6, '0', STR_PAD_LEFT);
        $txt = sprintf('%s / %s', gettext('CARTA PORTE'), sprintf(gettext('ALBARÁN %d'), $numero));
        $this->Cell(0, 4, $txt, 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Ln();
        $this->SetY(14);
        $this->Cell(0, 4, sprintf(gettext('Hoja: %d'), $this->PageNo()), 0, 1, 'R');
        $this->Ln();
        $y = $this->GetY();

        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 5, 95, 0, 'jpg', '', 'C', false, 600);

        $this->SetY($y);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(75, 4, gettext('EXPEDIDOR / CARGADOR'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->setCellPaddings('1', '', '', '');
        $this->Cell(75, 4, 'INGENIERIA Y CONSTRUCCIÓN DEL PERFIL, S.A.', 'LR', 1, 'L');
        $this->Cell(75, 4, 'Carrer Nou 16. - P.I.Mas del Polio', 'LR', 1, 'L');
        $this->Cell(75, 4, '46469 - BENIPARREL (Valencia)', 'LR', 1, 'L');
        $this->Cell(75, 4, 'Tel. 961 211 778 - Fax. 931 211 504', 'LRB', 1, 'L');

        $this->Ln();
        // $this->SetXY(75, 5);
        // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
        // $this->numero;
        $codbar = str_pad($pedcodbar, 6, '0', STR_PAD_LEFT) .
            str_pad(0, 3, '0', STR_PAD_LEFT) .
            str_pad(8, 2, '0', STR_PAD_LEFT) . '2' . '00';
        $this->write1DBarcode($codbar, 'C39', '', '', 65, 8, 1, self::$STYLE, 'N');

        $this->Ln(1);
        $this->Cell(25, 4, gettext('Pedido Nº'), 0, 0);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(60, 4, $this->pedidos, 0, 1);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(25, 4, gettext('Fecha'), 0, 0);
        $this->Cell(60, 4, $this->fecha, 0, 1);
        $this->Cell(25, 4, gettext('Porte Nº'), 0, 0);
        $txt = $this->porte;
        if ($this->portePedidos > 0) $txt .= ' - ' . $this->portePedidos;
        $this->Cell(60, 4, $txt, 0, 1);
        $this->Cell(25, 4, '', 0, 0);
        $this->Cell(60, 4, '', 0, 0);

        $cpPoblProv = $this->cliente['codpo'] . ' ' . trim($this->cliente['poblacion']);
        if (strtoupper(trim($this->cliente['poblacion'])) != strtoupper(trim($this->cliente['provincia']))) {
            $cpPoblProv .= ' (' . trim($this->cliente['provincia']) . ')';
        }
        $this->SetY($y);
        $this->SetFont('dejavusans', '', 8);
        $this->SetX(90);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(0, 4, gettext('CLIENTE / CONSIGNATARIO'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->SetX(90);
        $this->Cell(20, 4, $this->cliente['codigo'], 'L', 0, 'C');
        $this->Cell(0, 4, $this->cliente['nombre'], 'R', 1);
        $this->SetX(90);
        $this->Cell(20, 4, '', 'L', 0);
        $this->Cell(0, 4, $this->cliente['direccion'], 'R', 1);
        $this->SetX(90);
        $this->Cell(20, 4, '', 'LB', 0);
        $this->Cell(70, 4, $cpPoblProv, 'B', 0);
        $this->Cell(0, 4, $this->cliente['nif'], 'BR', 1);
        $this->Ln(2);

        $this->SetFont('dejavusans', 'B', 8);
        $this->SetX(90);
        $this->Cell(0, 4, gettext('DIRECCIÓN DE ENTREGA DE LA MERCANCIA'), 1, 1, 'C');
        $this->SetX(90);
        $this->setCellPaddings('1', '', '', '');
        $this->Cell(0, 4, sprintf(gettext('Ref. Obra: %s'), $this->dirEntrega['referencia_obra']), 'LTR', 1);
        $this->SetX(90);
        $this->Cell(0, 4, $this->dirEntrega['direccion'], 'LR', 1);
        $this->SetX(90);
        $this->Cell(0, 4, $this->dirEntrega['codigo_postal'] . ' ' . $this->dirEntrega['poblacion'] . ' (' . $this->dirEntrega['provincia'] . ')', 'LRB', 1);
        $this->Ln(2);

        $this->SetX(90);
        $this->MultiCell(0, 12, $this->observaciones, 0, 'L', false, 1);

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('4', '0', '0', '0');

        $this->SetFont('dejavusans', '', 8);
        $this->SetY(68);
        $this->Cell(25, 4, gettext('CANTIDAD'), 1, 0, 'C');
        $this->Cell(147, 4, gettext('DESCRIPCIÓN'), 1, 0);
        $this->Cell(12, 4, gettext('LONG'), 1, 0, 'C');
        $this->Cell(8, 4, gettext('PAQ'), 1, 0, 'C');
        $this->Cell(8, 4, '', 1, 1, 'C');
    }

    function Footer()
    {
        $this->SetY(-55);
        $this->Line(5, $this->GetY(),205, $this->GetY());
        $this->SetY(-52);
        $this->SetFont('dejavusans', 'B', 8);
        $this->Cell(100, 4, gettext('ES RESPONSABILIDAD DEL CLIENTE CUALQUIER DEFECTO'), 0, 1);
        $this->Cell(100, 4, gettext('QUE SE PRODUZCA EN EL MATERIAL POR NO LLEVAR TOLDO'), 0, 1);
        $this->SetFont('dejavusans', '', 8);
        $this->setCellPaddings('1', '', '', '');
        $this->Cell(50, 4, gettext('NATURALEZA DE LA MERCANCIA'), 'LT', 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(50, 4, gettext('PERFILES METÁLICOS'), 'TR', 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->Cell(20, 4, gettext('PESO (KG)'), 'LB', 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(40, 4, number_format($this->peso, 0, ',', '.'), 0, 0, 'C');
        $this->SetFont('dejavusans', '');
        $this->Cell(20, 4, gettext('BULTOS'), 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(20, 4, $this->bultos, 'RB', 1, 'C');

        $this->SetY(-52);
        $this->SetX(105);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(0, 4, gettext('RECEPTOR MERCANCIA. FIRMA'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->SetX(105);
        $this->MultiCell(0, 12, '', 1, 'L', false, 1);

        $y = $this->GetY();
        // $this->MultiCell(66.66, 28, '', 1, 'L', false, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(66.66, 4, gettext('OPERADORA Y CARGADORA'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->Cell(66.66, 4, trim($this->agencia['nombre']), 'LR', 1, 'C');
        $this->Cell(66.66, 4, trim($this->agencia['direccion']), 'LR', 1, 'C');
        $this->Cell(66.66, 4, trim($this->agencia['codpo'] . ' ' . $this->agencia['poblacion']), 'LR', 1, 'C');
        $this->Cell(66.66, 4, trim($this->agencia['provincia']), 'LR', 1, 'C');
        $this->Cell(66.66, 4, trim($this->agencia['nif']), 'LR', 1, 'C');
        $this->Cell(66.66, 4, '', 'LRB', 1, 'C');

        $this->SetXY(71.66, $y);
        // $this->MultiCell(66.66, 28, '', 1, 'L', false, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(66.66, 4, gettext('TRANSPORTISTA/PORTEADOR'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->SetX(71.66);
        $this->Cell(66.66, 4, trim($this->transportista['nombre']), 'LR', 1, 'C');
        $this->SetX(71.66);
        $this->Cell(66.66, 4, trim($this->transportista['direccion']), 'LR', 1, 'C');
        $this->SetX(71.66);
        $this->Cell(66.66, 4, $this->transportista['codpo'] . ' ' . trim($this->transportista['poblacion']), 'LR', 1, 'C');
        $this->SetX(71.66);
        $this->Cell(66.66, 4, trim($this->transportista['provincia']), 'LR', 1, 'C');
        $this->SetX(71.66);
        $this->Cell(66.66, 4, trim($this->transportista['nif']), 'LR', 1, 'C');
        $this->SetX(71.66);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(22.22, 4, gettext('MATRICULA'), 'LB', 0, 'C');
        $this->Cell(22.22, 4, trim($this->vehiculo['matricula']), 'B', 0, 'C');
        $this->Cell(22.22, 4, trim($this->vehiculo['matricula_remolque']), 'RB', 1, 'C');

        $this->SetFont('dejavusans', '');
        $this->SetXY(138.32, $y);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(66.66, 4, gettext('CONDUCTOR. FIRMA Y DNI'), 1, 1, 'C');
        $this->SetFont('dejavusans', '');
        $this->SetX(138.32);
        $this->Cell(66.66, 4, trim($this->conductor['nombre']), 'LR', 1, 'C');
        $this->SetX(138.32);
        $this->Cell(66.66, 4, '', 'LR', 1, 'C');
        $this->SetX(138.32);
        $this->Cell(66.66, 4, '', 'LR', 1, 'C');
        $this->SetX(138.32);
        $this->Cell(66.66, 4, '', 'LR', 1, 'C');
        $this->SetX(138.32);
        $this->Cell(66.66, 4, '', 'LR', 1, 'C');
        $this->SetX(138.32);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(10, 4, gettext('DNI'), 'LB', 0, 'C');
        $this->Cell(0, 4, trim($this->conductor['dni']), 'RB', 1, 'C');

        $this->SetFont('dejavusans', '', 6);
        $this->Cell(0, 3, gettext('Inscrita en el Registro Mercantil de Valencia, Folio 127 Tomo 4.361 Libro 1.673 Sección Gral. Hoja V-22.870 Inscripcion 4. NIF A46265526'), 0, 1, 'C');
    }

    public function checkPageBreak($h = 0, $y = '', $addpage = true) {
        return parent::checkPageBreak($h, $y, $addpage); // TODO: Change the autogenerated stub
    }

}

$pdf = new PDF();
$pdf->setNumero($numero);
$pdf->setPedidos($pedidos);
$pdf->setFecha($fecha);
$pdf->setPorte($porte);
$pdf->setPortePedidos($portePedidos);
$pdf->setCliente($cliente);
$pdf->setAgencia($agencia);
$pdf->setTransportista($transportista);
$pdf->setVehiculo($vehiculo);
$pdf->setConductor($conductor);
$pdf->setDirEntrega($dirEntrega);
$pdf->setObservaciones($observaciones);
$pdf->setPeso($peso);
$pdf->setBultos($bultos);

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('ALBARÁN');
$pdf->SetTitle(sprintf(gettext('ALBARÁN %d'), $numero));
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 72);
$pdf->SetAutoPageBreak(true, $pdf->getPageHeight() - 242);
$pdf->AddPage();

if (empty($albaranlin)) {
    $pdf->Cell(25, 242 - $pdf->GetY(), '', 1, 0);
    $pdf->Cell(147, 242 - $pdf->GetY(), '', 1, 0);
    $pdf->Cell(12, 242 - $pdf->GetY(), '', 1, 0);
    $pdf->Cell(8, 242 - $pdf->GetY(), '', 1, 0);
    $pdf->Cell(8, 242 - $pdf->GetY(), '', 1, 1);
} else {
    $ultimaSeccion = 0;
    for ($i = 0; $i < count($albaranlin); $i++) {
        $seccion = intval($albaranlin[$i]['seccion']);
        if ($i == 0) {
            $ultimaSeccion = $seccion;
        } else if ($ultimaSeccion != $seccion) {
            $pdf->Cell(15, 4, '', 'L', 0, 'R');
            $pdf->Cell(10, 4, '', 'R', 0, 'C');
            $pdf->Cell(147, 4, '', 'LR', 0);
            $pdf->Cell(12, 4, '', 'LR', 0, 'C');
            $pdf->Cell(8, 4, '', 'LR', 0, 'C');
            $pdf->Cell(8, 4, '', 'LR', 1, 'C');
            $ultimaSeccion = $seccion;
        }

        if ($seccion == 2) {
            $unidad_medida = 'PZS';
        } else {
            $unidad_medida = 'UDS';
        }
        switch (intval($albaranlin[$i]['posicion'])) {
            case 1: $posicion = 'CU'; break;
            case 2: $posicion = 'FA'; break;
            default: $posicion = ''; break;
        }

        $refplano = trim($albaranlin[$i]['referencia_plano']);
		$descripcion = IncoTranslatorFix::translate($dbconn, trim($albaranlin[$i]['descripcion_comercial']), $idioma);

        $aux = explode(' | ', $descripcion);
        if (count($aux) > 1) {
            $descripcion = str_replace(' | ', PHP_EOL, $descripcion);
        }
        if (!is_null($refplano) && !empty($refplano)) {
            $descripcion .= ' - ' . $refplano;
        }

        $height = $pdf->getStringHeight(147, $descripcion, true) + 0.75;
        if ($pdf->checkPageBreak($height, '', false)) {
            /*$pdf->Cell(25, 242 - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(147, 242 - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(12, 242 - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(8, 242 - $pdf->GetY(), '', 'LRB', 0);
            $pdf->Cell(8, 242 - $pdf->GetY(), '', 'LRB', 1);*/
            $pdf->AddPage();
        } else {
            // echo $descripcion . ' - ' . $pdf->GetY() . ' + ' . $height . ' = ' . ($pdf->GetY() + $height) . PHP_EOL;
            if (242 - ($pdf->GetY() + $height) < 4) {
                $height = 242 - $pdf->GetY();
            }
        }

        $longitud = in_array($seccion, array(7, 8)) ? '' : $albaranlin[$i]['longitud'];

        $pdf->Cell(15, $height, number_format($albaranlin[$i]['cantidad'], 0, ',', '.'), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->Cell(10, $height, $unidad_medida, 'R', 0, 'C', false, '', 0, false, 'T', 'T');
        $pdf->MultiCell(147, $height, $descripcion, 'LR', 'LR', false, 0);
        $pdf->Cell(12, $height, $longitud, 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
        $pdf->SetFont('dejavusans', 'B');
        $pdf->Cell(8, $height, $albaranlin[$i]['paquete'], 'LR', 0, 'C', false, '', 0, false, 'T', 'T');
        $pdf->SetFont('dejavusans', '');
        $pdf->Cell(8, $height, $posicion, 'R', 1, 'C', false, '', 0, false, 'T', 'T');

		if (in_array(intval($albaranlin[$i]['seccion']), array(7, 8))) {
		    $pdf->SetFontSize(6);
            // $result = pg_execute($dbconn, 'composicion', array($albaranlin[$i]['identificador_pedido'], $albaranlin[$i]['linea_pedido']));
            $result = pg_execute($dbconn, 'composicion', array($identificador, $albaranlin[$i]['linea']));
            if (pg_num_rows($result) <= 0) $albalin2 = array();
		    else $albalin2 = pg_fetch_all($result);
		    for ($j = 0; $j < count($albalin2); $j++) {
                if ($albalin2[$j]['seccion'] == 2) $unidad_medida = 'PZS';
                else $unidad_medida = 'UDS';
                $posicion = '';
                $paquete = $albalin2[$j]['paquete'];
                /*switch ($albalin2[$j]['seccion']) {
                    case 2: // REMATES
                        $cantidad = $albalin2[$j]['r_piezas'];
                        $longitud = $albalin2[$j]['r_largo'];
                        break;
                    case 8: // OTROS
                        $cantidad = $albalin2[$j]['v_cantidad'];
                        $longitud = $albalin2[$j]['v_longitud'];
                        break;
                    default:
                        $cantidad = 0;
                        $longitud = '';
                        break;
                }*/
                $cantidad = $albalin2[$j]['cantidad'];
                $longitud = $albalin2[$j]['longitud'];

		        $descripcion2 = IncoTranslatorFix::translate($dbconn, trim($albalin2[$j]['descripcion_comercial']), $idioma);
                $height = $pdf->getStringHeight(125, $descripcion2, true);

                $pdf->Cell(25, $height, '', 'LR', 0);
                $pdf->Cell(12, $height, number_format($cantidad, 0, ',', '.'), 'L', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->Cell(10, $height, $unidad_medida, '', 0, 'C', false, '', 0, false, 'T', 'T');
                $pdf->MultiCell(125, $height, $descripcion2, 'R', 'LR', false, 0);
                $pdf->Cell(12, $height, $longitud, 'LR', 0, 'R', false, '', 0, false, 'T', 'T');
                $pdf->SetFont('dejavusans', 'B');
                $pdf->Cell(8, $height, $paquete, 'LR', 0, 'C', false, '', 0, false, 'T', 'T');
                $pdf->SetFont('dejavusans', '');
                $pdf->Cell(8, $height, $posicion, 'R', 1, 'C', false, '', 0, false, 'T', 'T');
            }
            $pdf->SetFontSize(8);
        }
    }

    $pdf->Cell(15, 4, '', 'L', 0, 'R');
    $pdf->Cell(10, 4, '', 'R', 0, 'C');
    $pdf->Cell(147, 4, '', 'LR', 0);
    $pdf->Cell(12, 4, '', 'LR', 0, 'C');
    $pdf->Cell(8, 4, '', 'LR', 0, 'C');
    $pdf->Cell(8, 4, '', 'LR', 1, 'C');
    if ($albaran['portes'] == 0 || $albaran['portes'] == 3) {
        // portes en factura
        $pdf->Cell(15, 4, '1', 'L', 0, 'R');
        $pdf->Cell(10, 4, '', 'R', 0, 'C');
        $pdf->Cell(147, 4, gettext('TRANSPORTE MATERIAL'), 'LR', 0);
        $pdf->Cell(12, 4, '', 'LR', 0, 'C');
        $pdf->Cell(8, 4, '', 'LR', 0, 'C');
        $pdf->Cell(8, 4, '', 'LR', 1, 'C');
    }
    if ($albaran['portes'] == 1) {
        // portes pagados
        if (is_null($albaran['identificador_oferta'])) {
            $portesNumero = 100;
            $portesRealizados = 0;
            for ($i = 0; $i < count($pedidosNum); $i++) {
                // $result = pg_execute($dbconn, 'ofertas_portes', array($albaran['identificador_oferta']));
                $result = pg_execute($dbconn, 'pedidos_portes', array($pedidosNum[$i]));
                if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos - portes] No hay resultado');
                $aux = pg_fetch_all($result)[0];
                if (intval($aux['portes_numero']) < $portesNumero) {
                    $portesNumero = intval($aux['portes_numero']);
                    $portesRealizados = intval($aux['portes_realizados']);
                }
            }
        } else {
            $result = pg_execute($dbconn, 'ofertas_portes', array($albaran['identificador_oferta']));
            if (pg_num_rows($result) <= 0) incoLogWrite('[ofertas - portes] No hay resultado');
            $aux = pg_fetch_all($result)[0];
            $portesNumero = intval($aux['portes_numero']);
            $portesRealizados = intval($aux['portes_realizados']);
        }

        if ($portesRealizados > $portesNumero && $albaran['total_portes_exceso'] > 0) {
            $pdf->Cell(15, 4, '1', 'L', 0, 'R');
            $pdf->Cell(10, 4, '', 'R', 0, 'C');
            $pdf->Cell(147, 4, gettext('EXCESO EN NÚMERO DE PORTES'), 'LR', 0);
            $pdf->Cell(12, 4, '', 'LR', 0, 'C');
            $pdf->Cell(8, 4, '', 'LR', 0, 'C');
            $pdf->Cell(8, 4, '', 'LR', 1, 'C');
        }
    }
    $pdf->Cell(25, 242 - $pdf->GetY(), '', 'LRB', 0);
    $pdf->Cell(147, 242 - $pdf->GetY(), '', 'LRB', 0);
    $pdf->Cell(12, 242 - $pdf->GetY(), '', 'LRB', 0);
    $pdf->Cell(8, 242 - $pdf->GetY(), '', 'LRB', 0);
    $pdf->Cell(8, 242 - $pdf->GetY(), '', 'LRB', 0);
}
// 245

$dir = INCO_DIR_CLIENTES . $cliente['codigo'] . '/Albaranes/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'A_' . $albaran['ejercicio'] . '_' . str_pad($numero, 6, '0', STR_PAD_LEFT) . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
