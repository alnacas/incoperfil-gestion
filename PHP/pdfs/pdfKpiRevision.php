<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$area = $argv[2];
$sql_where = str_replace('@', '\'', trim($argv[3]));

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$sql = 'select year, trimestre, revision, acciones from kpi_revisiones ' . $sql_where . ' order by year, trimestre';
$pg_result = pg_query($dbconn, $sql);
$revisiones = $pg_result !== false ? (pg_num_rows($pg_result) > 0 ? pg_fetch_all($pg_result) : []) : [];

class KPI_Revision extends TCPDF {

	/**
	 * @var string
	 */
	private $area;

	public function setArea(int $area) {
		switch ($area) {
			case 0:
				$this->area = 'Comercial';
				break;
			case 1:
				$this->area = 'Compras';
				break;
			case 2:
				$this->area = 'Producción';
				break;
			case 3:
				$this->area = 'Calidad';
				break;
		}
	}

	function Header()
	{
		$txt = 'KPI ' . $this->area . ' - Revisiones';
		$this->SetSubject($txt);
		$this->SetTitle($txt);

		$this->SetFont('dejavusans', 'B', 14);
		$this->SetY(10);
		$this->Cell(0, 8, $txt, 0, 1, 'C');
		$this->SetFont('dejavusans', '', 8);
		$this->Ln(4);
	}

	function Footer() {
		$this->SetY(-18);

		$wDisp = $this->w - $this->lMargin - $this->rMargin;
		$wCell = $wDisp / 3;

		$this->SetFont('dejavusans', '', 8);
		$this->Cell($wCell, 4, 'Código: FCA-01-07', 0, 0, 'L');
		$this->Cell($wCell, 4, 'Revisión: 2', 0, 0, 'C');
		$this->Cell($wCell, 4, 'Fecha: 16/12/2019', 0, 1, 'R');
	}

	/**
	 * @param array $revisiones
	 */
	public function WriteRevisiones($revisiones) {
		foreach ($revisiones as $revision) {
			$this->SetFont('dejavusans', 'B');
			$this->Cell(20, 4, 'Año', 0, 0);
			$this->SetFont('dejavusans', '');
			$this->Cell(20, 4, $revision['year'], 0, 0);
			$this->SetX($this->GetX() + 20);
			$this->SetFont('dejavusans', 'B');
			$this->Cell(20, 4, 'Trimestre', 0, 0);
			$this->SetFont('dejavusans', '');
			$this->Cell(20, 4, $revision['trimestre'], 0, 1);

			$this->SetFont('dejavusans', 'B');
			$this->Cell(0, 4, 'Revisión', 0, 1);
			$this->SetFont('dejavusans', '');
			$this->MultiCell(0, 4, $revision['revision'] . PHP_EOL);

			$this->SetFont('dejavusans', 'B');
			$this->Cell(0, 4, 'Acciones', 0, 1);
			$this->SetFont('dejavusans', '');
			$this->MultiCell(0, 4, $revision['acciones'] . PHP_EOL, 'B');
			$this->Ln(2);
		}
	}

}

$pdf = new KPI_Revision();
$pdf->setArea($area);
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(20, 20);
$pdf->SetAutoPageBreak(true, 5);
$pdf->AddPage();

$pdf->WriteRevisiones($revisiones);

//SALIDA
$dir = INCO_DIR_KPIS;
if (!is_dir($dir)) {
	$oldmask = umask(0);
	$aux = mkdir($dir, 0755, true);
	umask($oldmask);
}
$fileName = 'KPI_Revision_' . $area . '.pdf';
$output = $dir . $fileName;
if (file_exists($output)) unlink($output);
$pdf->Output($output, 'F');

pg_close($dbconn);