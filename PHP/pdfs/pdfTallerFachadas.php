<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el pedido
$result = pg_query_params('SELECT numero, cliente, referencia_obra, observaciones, fecha, fecha_entrega, nombre, referencia_cliente FROM pedidos WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$pedido = pg_fetch_all($result)[0];

// obtenemos las lineas del pedido
$result = pg_query_params('SELECT linea, producto, espesor, ccolor, ccolorb, descripcion, descripcion_comercial, cantidad_total_pendiente, referencia_plano FROM pedidoslin WHERE identificador = $1 AND seccion = 7 ORDER BY linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin] No hay resultado');
$pedidoslin = pg_fetch_all($result);

$result = pg_query_params($dbconn, 'SELECT seccion, linea, descripcion, peso, a_modulos, a_longitud, a_malla_antipajaros, e_cantidad, e_malla_antipajaros, l_cantidad, l_longitud, l_alto, l_ancho, o_cantidad, o_longitud, v_cantidad, v_longitud, v_ancho FROM pedidoslin WHERE identificador = $1 AND seccion IN (5, 9, 6, 4, 8) ORDER BY seccion, linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidoslin - resumen]: ' . pg_last_error());
// if (pg_num_rows($result) <= 0) incoLogWrite('[pedidoslin - resumen] No hay resultado');
$resumenlin = pg_fetch_all($result);

// preparamos para obetener los hijos
pg_prepare($dbconn, 'composicion', 'SELECT linea2, seccion, producto, espesor, ccolor, ccolorb, pintura, pinturab, recubrimiento, calidad, descripcion, descripcion_comercial, cantidad_total_pendiente, peso, referencia_plano, r_desarrollo, r_corte, r_largo, r_ancho, r_formatos, r_piezas, r_formacion, r_cantidad_pendiente, r_perforado, v_cantidad, v_longitud, v_ancho FROM pedidoslin2 WHERE identificador = $1 AND linea = $2 AND seccion IN (2, 8) ORDER BY linea2');

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion FROM clientes WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'productos', 'SELECT nombre, montaje FROM productos WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'espesores', 'SELECT valor FROM espesores WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'colores', 'SELECT nombre FROM colores WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'calidades', 'SELECT abreviatura FROM calidades WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'recubrimientos', 'SELECT abreviatura FROM recubrimientos WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'pinturas', 'SELECT abreviatura FROM pinturas WHERE codigo = $1 LIMIT 1');
pg_prepare($dbconn, 'perforados', 'SELECT nombre_compuesto FROM perforaciones WHERE codigo = $1 LIMIT 1');

/*$result = pg_query_params($dbconn, 'select count(*)::integer as corte from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 2', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarCorte]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarCorte] No hay resultado');
$boCodbarCorte = pg_fetch_all($result)[0]['corte'] > 0;*/

/*$result = pg_query_params($dbconn, 'select count(*)::integer as plegado from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 3', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarPlegado]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPlegado] No hay resultado');
$boCodbarPlegado = pg_fetch_all($result)[0]['plegado'] > 0;*/

/*$result = pg_query_params($dbconn, 'select count(*)::integer as punzonado from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 5', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarPunzonado]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPunzonado] No hay resultado');
$boCodbarPunzonado = pg_fetch_all($result)[0]['punzonado'] > 0;*/

/*$result = pg_query_params($dbconn, 'select count(*)::integer as montaje from pedidoslin pl, procesos_producto pp where pl.identificador = $1 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 6', array($identificador)) or incoLogWrite('La consulta fallo [boCodbarMontaje]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarMontaje] No hay resultado');
$boCodbarMontaje = pg_fetch_all($result)[0]['montaje'] > 0;*/
$boCodbarMontaje = true;

class PDF extends TCPDF
{
    /**
     * @var integer
     */
    public $counter_i;

    /**
     * @var bool
     */
    private $resumen;

    private static $H = 9;

    private static $STYLE = array('align' => 'C', 'fitwidth' => true, 'border' => false, 'text' => true, 'font' => 'helvetica', 'fontsize' => 5);

    function Header()
    {
        global $pedido, $pedidoslin, $boCodbarCorte, $boCodbarPlegado;

        $this->SetFont('dejavusans', 'B', 24);
        $this->SetY(3);
        if ($this->counter_i == -1) {
            $this->Cell(0, 8, 'RESUMEN', 0, 1, 'C');
            $this->resumen = true;
        } else {
            $this->Cell(0, 8, 'FACHADAS', 0, 1, 'C');
            $this->resumen = false;
        }
        /*$this->SetFont('');
        $this->SetFontSize(8);*/
        $this->SetFont('dejavusans', '', 8);
        $y = $this->GetY();

        if ($this->counter_i != -1 && $boCodbarCorte) {
            $this->SetY(0, false);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'CORTE', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarCorte = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(1, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarCorte, 'C39', '', '', 65, self::$H, 1, self::$STYLE, 'N');
        }
        if ($this->counter_i != -1 && $boCodbarPlegado) {
            $this->SetXY(140, 0, false);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'PLEGADO', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarPlegado = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(2, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarPlegado, 'C39', '140', '', 65, self::$H, 1, self::$STYLE, 'N');
        }
        $this->SetY($y);

        $y = $this->GetY();
        $this->Cell(33, 4, 'CLIENTE', 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(57, 4, trim($pedido['nombre']), 0, 1, 'L');
        $this->SetFont('dejavusans', '');
        $this->Cell(33, 4, 'PEDIDO', 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(57, 4, trim($pedido['numero']), 0, 1, 'L');
        $this->SetFont('dejavusans', '');
        $this->Cell(33, 4, 'REFERENCIA DE OBRA', 0, 0);
        $this->SetFont('dejavusans', 'B');
        $this->Cell(57, 4, trim($pedido['referencia_obra']), 0, 1, 'L');
        $this->SetFont('dejavusans', '');
        $this->Cell(33, 4, 'PED. CL.', 0, 0);
        $this->Cell(57, 4, trim($pedido['referencia_cliente']), 0, 1, 'L');
        $this->Cell(33, 4, 'OBSERVACIONES', 0, 0);
        $this->Cell(57, 4, trim($pedido['observaciones']), 0, 1, 'L');
        $yObs = $this->GetY() + 1;
        $this->SetY($y);
        $x = 151;
        $this->SetX($x);
        $this->Cell(30, 4, 'FECHA ENT.', 0, 0, 'R');
        if (is_null($pedido['fecha_entrega'])) {
            $fecha = '';
        } else {
            $fe = DateTime::createFromFormat('Y-m-d', $pedido['fecha_entrega']);
            $fecha = $fe->format('d-m-Y');
        }
        $this->Cell(0, 4, $fecha, 0, 1, 'R');
        $this->SetX($x);
        $this->Cell(30, 4, 'FECHA PDO.', 0, 0, 'R');
        $fp = DateTime::createFromFormat('Y-m-d', $pedido['fecha']);
        $this->Cell(0, 4, $fp->format('d-m-Y'), 0, 1, 'R');
        $this->Ln(2);

        if ($this->counter_i != -1) {
            $x = 111;
            $txt = trim(trim($pedidoslin[$this->counter_i]['descripcion']) . ' ' . trim($pedidoslin[$this->counter_i]['referencia_plano']));
            $this->SetX($x);
            $this->SetFont('dejavusans', 'B');
            $this->MultiCell(90, 4, $txt . PHP_EOL, 0, 'J', false, 1, '', '', false);
            $this->SetFont('dejavusans', '');

            $this->SetY($yObs);
            $this->SetFillColor(224); // Oscuro: 192
            $this->SetFont('dejavusans', '', 8);
            $height = 4;
            $this->Cell(30, $height, 'Operario de Carga', 1, 0, 'C', true);
            $this->Cell(10, $height, '', 1, 0);

            $this->Cell(5, $height, '1', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '2', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '3', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '4', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '5', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '6', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '7', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '8', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '9', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '10', 0, 0, 'R');
            $this->Cell(10, $height, '', 'B', 1);
            $this->Ln(2);

            $this->Cell(10, $height, 'PQTS.', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(4, $height, '', 0, 0);
            $this->Cell(18, $height, 'FECHA FAB.', 0, 0, 'R');
            $this->Cell(5, $height, '1', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '2', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '3', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(1, $height, '', 0, 0);
            $this->Cell(5, $height, '4', 0, 0, 'R');
            $this->Cell(12, $height, '', 'B', 0);
            $this->Cell(4, $height, '', 0, 0);
            $this->Cell(18, $height, 'FECHA EXP.', 0, 0, 'R');
            $this->Cell(20, $height, '', 'B', 0);
            $this->Cell(4, $height, '', 0, 1);
        }
    }

    function Footer()
    {
        global $pedido, $pedidoslin, $boCodbarPunzonado, $boCodbarMontaje;

        if (!$this->resumen && $this->counter_i != -1 && $boCodbarPunzonado) {
            $this->SetY(-12);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'PUNZONADO', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarPunzonado = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(3, 2, '0', STR_PAD_LEFT) . '1' . '07';
            $this->write1DBarcode($codbarPunzonado, 'C39', '', '', 65, self::$H, 1, self::$STYLE, 'N');
            $this->SetFont('dejavusans', '', 8);
        }
        if (!$this->resumen && $this->counter_i != -1 && $boCodbarMontaje) {
            $this->SetY(-12);
            $this->SetX(140);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'MONTAJE', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarMontaje = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(trim($pedidoslin[$this->counter_i]['producto']), 3, '0', STR_PAD_LEFT) .
                str_pad(4, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarMontaje, 'C39', '140', '', 65, self::$H, 1, self::$STYLE, 'N');
            $this->SetFont('dejavusans', '', 8);
        }
        if ($this->resumen) {
            $this->SetY(-12);
            $this->SetX(140);
            $this->SetFont('dejavusans', '', 5);
            $this->Cell(65, 3, 'CARGA', 0, 1, 'C');
            $this->SetFont('dejavusans', '', 8);
            // CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]
            $codbarCarga = str_pad(trim($pedido['numero']), 6, '0', STR_PAD_LEFT) .
                str_pad(0, 3, '0', STR_PAD_LEFT) .
                str_pad(8, 2, '0', STR_PAD_LEFT) . '1' . '00';
            $this->write1DBarcode($codbarCarga, 'C39', '140', '', 65, self::$H, 1, self::$STYLE, 'N');
            $this->SetFont('dejavusans', '', 8);
        }
    }
}

$pdf = new PDF();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('OFERTA');
$pdf->SetTitle('Oferta X');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(5, 48);
$pdf->SetAutoPageBreak(false);

$pdf->counter_i = -1;
$boCodbarMontaje = false;
$pdf->AddPage();
$pdf->SetFillColor(224); // Oscuro: 192
$pdf->Cell(10, 4, 'Lin', 1, 0, 'C', true);
$pdf->Cell(116, 4, 'Descripción', 1, 0, 'C', true);
$pdf->Cell(10, 4, 'Uds', 1, 0, 'C', true);
$pdf->Cell(12, 4, 'Largo', 1, 0, 'C', true);
$pdf->Cell(12, 4, 'Alto', 1, 0, 'C', true);
$pdf->Cell(12, 4, 'Ancho', 1, 0, 'C', true);
$pdf->Cell(14, 4, 'Kgs', 1, 0, 'C', true);
$pdf->Cell(14, 4, 'Montaje', 1, 1, 'C', true);
$lastseccion = intval($resumenlin[0]['seccion']);
for ($i = 0; $i < count($resumenlin); $i++) {
    $seccion = intval($resumenlin[$i]['seccion']);
    if ($lastseccion != $seccion) {
        $pdf->Cell(10, 1, '', 0, 0, 'C');
        $pdf->Cell(116, 1, '', 0, 0, 'L');
        $pdf->Cell(10, 1, '', 0, 0, 'R');
        $pdf->Cell(12, 1, '', 0, 0, 'R');
        $pdf->Cell(12, 1, '', 0, 0, 'R');
        $pdf->Cell(12, 1, '', 0, 0, 'R');
        $pdf->Cell(14, 1, '', 0, 0, 'R');
        $pdf->Cell(14, 1, '', 0, 1, 'R');
        $lastseccion = $seccion;
    }

    $lin = intval($resumenlin[$i]['linea']);
    $descripcion = trim($resumenlin[$i]['descripcion']);
    $kgs = round(floatval($resumenlin[$i]['peso']), 0);
    switch ($seccion) {
        case 4: // OMEGAS
            $uds = $resumenlin[$i]['o_cantidad'];
            $largo = $resumenlin[$i]['o_longitud'];
            $alto = '';
            $ancho = '';
            break;
        case 5: // V. LINEAL
            $uds = $resumenlin[$i]['a_modulos'];
            $largo = $resumenlin[$i]['a_longitud'];
            $alto = '';
            $ancho = '';
            if ($resumenlin[$i]['a_malla_antipajaros'] == 1) $descripcion .= ' MALLA';
            if ($resumenlin[$i]['a_malla_antipajaros'] == 2) $descripcion .= ' MOSQUITERA';
            break;
        case 6: // V. LAMAS
            $uds = $resumenlin[$i]['l_cantidad'];
            $largo = $resumenlin[$i]['l_longitud'];
            $alto = $resumenlin[$i]['l_alto'];
            $ancho = $resumenlin[$i]['l_ancho'];
            break;
        case 8: // OTROS
            $uds = $resumenlin[$i]['v_cantidad'];
            $largo = $resumenlin[$i]['v_longitud'];
            $alto = '';
            $ancho = $resumenlin[$i]['v_ancho'];
            break;
        case 9: // V. PUNTUAL
            $uds = $resumenlin[$i]['e_cantidad'];
            $largo = '';
            $alto = '';
            $ancho = '';
            if ($resumenlin[$i]['e_malla_antipajaros'] == 1) $descripcion .= ' MALLA';
            if ($resumenlin[$i]['e_malla_antipajaros'] == 2) $descripcion .= ' MOSQUITERA';
            break;
        default:
            $uds = '';
            $largo = '';
            $alto = '';
            $ancho = '';
            break;
    }

    $lines = ceil($pdf->GetStringWidth($descripcion) / 114);
    $height = $lines * 4;

    $pdf->Cell(10, $height, $lin, 1, 0, 'C');
    // $pdf->Cell(116, $height, $descripcion, 1, 0, 'L');
    $pdf->MultiCell(116, $height, $descripcion . PHP_EOL, 1, 'J', false, 0);
    $pdf->Cell(10, $height, $uds, 1, 0, 'R');
    $pdf->Cell(12, $height, $largo, 1, 0, 'R');
    $pdf->Cell(12, $height, $alto, 1, 0, 'R');
    $pdf->Cell(12, $height, $ancho, 1, 0, 'R');
    $pdf->Cell(14, $height, $kgs, 1, 0, 'R');
    $pdf->Cell(14, $height, '', 1, 1, 'R');
}

$result = pg_query_params('select string_agg(distinct s.nombre, \' - \') as secciones from pedidoslin pl, secciones s where pl.identificador = $1 and pl.seccion = s.codigo', array($identificador)) or incoLogWrite('La consulta fallo [secciones]: ' . pg_last_error());
$secciones = trim(pg_fetch_all($result)[0]['secciones']);
$pdf->Ln(5);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, $secciones, 0, 1, 'C');
$pdf->SetFont('dejavusans', '');

// $boCodbarMontaje = true;
for ($i = 0; $i < count($pedidoslin); $i++) {
    $pdf->counter_i = $i;

    $result = pg_query_params($dbconn, 'select count(*)::integer as corte from pedidoslin2 pl, procesos_producto pp where pl.identificador = $1 AND pl.linea = $2 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 2', array($identificador, $pedidoslin[$i]['linea'])) or incoLogWrite('La consulta fallo [boCodbarCorte]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarCorte] No hay resultado');
    $boCodbarCorte = pg_fetch_all($result)[0]['corte'] > 0;

    $result = pg_query_params($dbconn, 'select count(*)::integer as plegado from pedidoslin2 pl, procesos_producto pp where pl.identificador = $1 AND pl.linea = $2 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 3', array($identificador, $pedidoslin[$i]['linea'])) or incoLogWrite('La consulta fallo [boCodbarPlegado]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPlegado] No hay resultado');
    $boCodbarPlegado = pg_fetch_all($result)[0]['plegado'] > 0;

    $result = pg_query_params($dbconn, 'select count(*)::integer as punzonado from pedidoslin2 pl, procesos_producto pp where pl.identificador = $1 AND pl.linea = $2 AND pl.seccion = 2 and pl.producto = pp.producto and pp.proceso = 5', array($identificador, $pedidoslin[$i]['linea'])) or incoLogWrite('La consulta fallo [boCodbarPunzonado]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarPunzonado] No hay resultado');
    $boCodbarPunzonado = pg_fetch_all($result)[0]['punzonado'] > 0;

    $result = pg_execute($dbconn, 'productos', array(intval($pedidoslin[$i]['producto'])));
    if (pg_num_rows($result) <= 0) incoLogWrite('[boCodbarMontaje] No hay resultado');
    $boCodbarMontaje = pg_fetch_all($result)[0]['montaje'] == 1;

    $pdf->AddPage();

    // Datos del cliente
   /* $y = $pdf->GetY();
    $pdf->Cell(50, 4, 'CLIENTE', 0, 0);
    $pdf->Cell(37, 4, trim($pedido['nombre']), 0, 1, 'R');
    $pdf->Cell(50, 4, 'PEDIDO', 0, 0);
    $pdf->Cell(37, 4, trim($pedido['numero']), 0, 1, 'R');
    $pdf->Cell(50, 4, 'REFERENCIA DE OBRA', 0, 0);
    $pdf->Cell(37, 4, trim($pedido['referencia_obra']), 0, 1, 'R');
    $pdf->Cell(50, 4, 'PED. CL.', 0, 0);
    $pdf->Cell(37, 4, trim($pedido['referencia_cliente']), 0, 1, 'R');
    $pdf->Cell(50, 4, 'OBSERVACIONES', 0, 0);
    $pdf->Cell(37, 4, trim($pedido['observaciones']), 0, 1, 'R');
    $pdf->SetY($y);
    $x = 111;
    $pdf->SetX($x);
    $pdf->Cell(50, 4, 'FECHA ENT.', 0, 0);
    $fe = DateTime::createFromFormat('Y-m-d', $pedido['fecha_entrega']);
    $pdf->Cell(37, 4, $fe->format('d-m-Y'), 0, 1);
    $pdf->SetX($x);
    $pdf->Cell(50, 4, 'FECHA PDO.', 0, 0);
    $fp = DateTime::createFromFormat('Y-m-d', $pedido['fecha']);
    $pdf->Cell(37, 4, $fp->format('d-m-Y'), 0, 1);
    $pdf->Ln(2);
    $pdf->SetX($x);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->MultiCell(90, 4, trim($pedidoslin[$i]['descripcion']) . PHP_EOL, 0, 'J', false, 1, '', '', false);
    $pdf->SetFont('dejavusans', '');
    $pdf->Ln(4);
    $pdf->Ln(4);*/
    // $pdf->SetY(58);

    $result = pg_execute($dbconn, 'composicion', array($identificador, $pedidoslin[$i]['linea']));
    if (pg_num_rows($result) <= 0) {
        // incoLogWrite('[composicion] No hay resultado');
        continue;
    }
    $pedidoslin2 = pg_fetch_all($result);

    for ($k = 1; $k <= count($pedidoslin2); $k++) {
        // Productos del pedido
    	$j = $k - 1;
    	$seccion_comp = intval($pedidoslin2[$j]['seccion']);
        $linea = intval($pedidoslin2[$j]['linea2']);
        /*$result = pg_execute($dbconn, 'productos', array(intval($pedidoslin2[$j]['producto'])));
        if (pg_num_rows($result) <= 0) incoLogWrite('[productos] No hay resultado');
        $descripcion = trim(pg_fetch_all($result)[0]['nombre']);*/
        $result = pg_execute($dbconn, 'espesores', array(intval($pedidoslin2[$j]['espesor'])));
        if (pg_num_rows($result) <= 0) incoLogWrite('[espesores] No hay resultado');
        $descripcion = trim(pg_fetch_all($result)[0]['valor']);

        $result = pg_execute($dbconn, 'colores', array(intval($pedidoslin2[$j]['ccolor'])));
        if (pg_num_rows($result) <= 0) incoLogWrite('[colores] No hay resultado');
        $descripcion .= ' ' . trim(pg_fetch_all($result)[0]['nombre']);

        if (!in_array(intval($pedidoslin2[$j]['ccolorb']), array(0, 1))) {
            $result = pg_execute($dbconn, 'colores', array(intval($pedidoslin2[$j]['ccolorb'])));
            if (pg_num_rows($result) <= 0) incoLogWrite('[colores] No hay resultado');
            $descripcion .= '/' . trim(pg_fetch_all($result)[0]['nombre']);
        }

        if (intval($pedidoslin2[$j]['pintura'] != 0)){
            $result = pg_execute($dbconn, 'pinturas', array(intval($pedidoslin2[$j]['pintura'])));
            if (pg_num_rows($result) <= 0) incoLogWrite('[pinturas] No hay resultado');
            $descripcion .= ' ' . trim(pg_fetch_all($result)[0]['abreviatura']);
        }

        if (intval($pedidoslin2[$j]['pinturab'] != 0)){
            $result = pg_execute($dbconn, 'pinturas', array(intval($pedidoslin2[$j]['pinturab'])));
            if (pg_num_rows($result) <= 0) incoLogWrite('[pinturas] No hay resultado');
            $descripcion .= '/' . trim(pg_fetch_all($result)[0]['abreviatura']);
        }

        if (intval($pedidoslin2[$j]['calidad'] != 0)){
            $result = pg_execute($dbconn, 'calidades', array(intval($pedidoslin2[$j]['calidad'])));
            if (pg_num_rows($result) <= 0) incoLogWrite('[calidad] No hay resultado');
            $descripcion .= ' ' . trim(pg_fetch_all($result)[0]['abreviatura']);
        }

        if (intval($pedidoslin2[$j]['recubrimiento'] != 0)){
            $result = pg_execute($dbconn, 'recubrimientos', array(intval($pedidoslin2[$j]['recubrimiento'])));
            if (pg_num_rows($result) <= 0) incoLogWrite('[colores] No hay resultado');
            $descripcion .= ' ' . trim(pg_fetch_all($result)[0]['abreviatura']);
        }

        if (intval($pedidoslin2[$j]['r_perforado'] != 0)){
            $result = pg_execute($dbconn, 'perforados', array(intval($pedidoslin2[$j]['r_perforado'])));
            if (pg_num_rows($result) <= 0) incoLogWrite('[perforados] No hay resultado');
            $descripcion .= ' ' . trim(pg_fetch_all($result)[0]['nombre_compuesto']);
        }

        $peso = floatval($pedidoslin2[$j]['peso']);
        // $pTotal += $peso;
        $peso = number_format($peso, 0, ',', '.');
	    $cantidad = floatval($pedidoslin2[$j]['cantidad_total_pendiente']);
	    $refplano = trim($pedidoslin2[$j]['referencia_plano']);
	    if (!empty($refplano)) {
		    $refplano = ' - ' . $refplano;
	    }
	    if ($seccion_comp == 2) {
	        $formatos = intval($pedidoslin2[$j]['r_formatos']);
	        $piezas = intval($pedidoslin2[$j]['r_piezas']);
	        $desarrollo = intval($pedidoslin2[$j]['r_desarrollo']);
	        $corte = intval($pedidoslin2[$j]['r_corte']);
	        $ancho = intval($pedidoslin2[$j]['r_ancho']);
	        $largo = intval($pedidoslin2[$j]['r_largo']);
	        if ($corte == 0) {
	            $lcorte = '0 x 0';
	        } else {
	            $lcorte = $largo . ' x ' . $corte;
	        }
	        $cizalla = $largo . ' x ' . $ancho;
	        switch (intval($pedidoslin2[$j]['r_formacion'])) {
	            case 1:
	                $formacion = 'TIRADA';
	                break;
	            case 2:
	                $formacion = 'PENDIENTE';
	                break;
	            case 3:
	                $formacion = 'TIR + PEND';
	                break;
	            default:
	                $formacion = '';
	                break;
	        }
        }
	    if ($seccion_comp == 8) {
	    	$descripcion = trim($pedidoslin2[$j]['descripcion']);
		    $formatos = intval($pedidoslin2[$j]['v_cantidad']);
		    $piezas = '';
		    $desarrollo = '';
		    $ancho = intval($pedidoslin2[$j]['v_ancho']);
		    $largo = intval($pedidoslin2[$j]['v_longitud']);
		    $lcorte = '';
		    $cizalla = '';
		    $formacion = '';
	    }

        $y = $pdf->GetY();
        $pdf->SetFillColor(224); // Oscuro: 192
        $pdf->Cell(10, 6, 'Lin', 1, 0, 'C', true);
        $pdf->Cell(18.5, 6, 'Descripción', 1, 0, 'C', true);
        $pdf->SetFont('dejavusans', 'B');
        $pdf->Cell(95.5, 6, $descripcion, 1, 0, 'L');
        $pdf->SetFont('dejavusans', '');
        $pdf->Cell(0, 6, '', 'LTR', 1, 'C');
        $y = $pdf->GetY();

        $pdf->Cell(10, 40, $pedidoslin[$i]['linea'] . '.' . $linea, 1, 0, 'C');
        $pdf->Cell(14.25, 6, 'Uds', 1, 0, 'C', true);
        $pdf->Cell(28.50, 6, 'L. Corte', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, 'Pzs', 1, 0, 'C', true);
        $pdf->Cell(28.50, 6, 'Cizalla', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, 'ML', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, 'KG', 1, 0, 'C', true);
        $pdf->Cell(0, 6, '', 'LR', 1, 'C');

        $pdf->SetX($pdf->GetX() + 10);
        $pdf->SetFont('dejavusans', 'B');
        $pdf->Cell(14.25, 6, $formatos, 1, 0, 'C');
        $pdf->Cell(28.50, 6, $lcorte, 1, 0, 'C');
        $pdf->Cell(14.25, 6, $piezas, 1, 0, 'C');
        $pdf->Cell(28.50, 6, $cizalla, 1, 0, 'C');
        $pdf->SetFont('dejavusans', '');
        $pdf->Cell(14.25, 6, $cantidad, 1, 0, 'C');
        $pdf->Cell(14.25, 6, $peso, 1, 0, 'C');
        $pdf->Cell(0, 6, '', 'LR', 1, 'C');

        $pdf->SetX($pdf->GetX() + 10);
        $pdf->Cell(14.25, 8, 'Bobina', 1, 0, 'C', true);
        $pdf->Cell(99.75, 8, '', 1, 0, 'R');
        $pdf->Cell(0, 8, '', 'LR', 1, 'C');

        $pdf->SetX($pdf->GetX() + 10);
        $pdf->Cell(14.25, 8, 'Paquete', 1, 0, 'C', true);
        $pdf->Cell(99.75, 8, '', 1, 0, 'R');
        $pdf->Cell(0, 8, '', 'LR', 1, 'C');

        $pdf->SetX($pdf->GetX() + 10);
        $pdf->Cell(14.25, 6, 'Máquina', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, '', 1, 0, 'R');
        $pdf->Cell(14.25, 6, 'Ancho', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, '', 1, 0, 'R');
        $pdf->Cell(14.25, 6, 'Ángulo', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, '', 1, 0, 'R');
        $pdf->Cell(14.25, 6, 'Cota', 1, 0, 'C', true);
        $pdf->Cell(14.25, 6, '', 1, 0, 'R');
        $pdf->Cell(0, 6, '', 'LR', 1, 'C');

        $pdf->SetX($pdf->GetX() + 10);
        $pdf->Cell(28.50, 6, $formacion, 1, 0, 'C');
        $pdf->Cell(85.5, 6, $refplano, 1, 0, 'L');
        $pdf->Cell(0, 6, '', 'LRB', 1, 'C');

        if ($k < count($pedidoslin2)) {
            if ($k % 5 == 0) {
                $pdf->AddPage();
            } else {
                $pdf->Ln(2);
            }
        }
    }
}

//SALIDA
$dir = INCO_DIR_TALLER;
if (!is_dir($dir)) {
    $oldmask = umask(0);
    $aux = mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'TF_' . str_pad($pedido['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
