<?php

use setasign\Fpdi\Tcpdf\Fpdi;

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
    incoLogWrite('No hay argumentos');
}
$solo_pendientes = isset($argv[3]) && $argv[3] == 'S';
$idioma = ''; // empty($argv[4]) ? '' : $argv[4];
// $tr = new IncoTranslator($DB_STRING, $idioma);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos la oferta
$result = pg_query_params($dbconn, 'SELECT ejercicio, numero, fecha, potencial, cliente, contacto, referencia_obra, fecha_validez, fecha_entrega, forma_pago, forma_pago_alternativa, plazo, revision, portes_numero, portes, portes_importe, transporte, condiciones, portes_exceso, portes_importe_exceso, aplicar_recargos, vendedor FROM ofertas WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite( 'La consulta fallo [ofertas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ofertas] No hay resultado');
$oferta = pg_fetch_all($result)[0];

// obtenemos las lineas de la oferta
$result = pg_query_params($dbconn, 'SELECT seccion, producto, espesor, partida, descripcion_comercial, cantidad, precio, unidad_medida, peso, estado FROM ofertaslin WHERE identificador = $1 ORDER BY linea', array($identificador)) or incoLogWrite( 'La consulta fallo [ofertaslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ofertaslin] No hay resultado');
$ofertalin = pg_fetch_all($result);
if ($solo_pendientes) {
    $ofertalin = array_values(
        array_filter($ofertalin, function ($linea) {
            return $linea['estado'] == 0;
        })
    );
}

// preparamos para obtener el cliente
pg_prepare($dbconn, 'clientes_ofertas', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, idioma FROM clientes WHERE codigo = $1 LIMIT 1');

// preparamos para obtener el cliente potencial
pg_prepare($dbconn, 'potenciales_ofertas', 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, idioma FROM potenciales WHERE codigo = $1 LIMIT 1');

// preparamos para obtener la forma de pago de la oferta
pg_prepare($dbconn, 'fopago_ofertas', 'SELECT nombre FROM fopago WHERE codigo = $1 LIMIT 1');

// preparamos para obtener la unidad de medida de cada linea de la oferta
pg_prepare($dbconn, 'unidadesmedida_ofertaslin', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1 LIMIT 1');

pg_prepare($dbconn, 'vendedor', 'SELECT codigo, tipo FROM vendedor WHERE codigo = $1');

/* $result = pg_execute($dbconn, 'unidadesmedida_ofertaslin', array(intval($ofertalin[0]['unidad_medida'])));
if (pg_num_rows($result) <= 0) die('[unidades_medida] No hay resultado');
$unidadMedida = pg_fetch_all($result)[0]['abreviatura'];*/

$numero_oferta = $oferta['numero'];
$numero_revision = $oferta['revision'];
$fecha = DateTime::createFromFormat('Y-m-d', $oferta['fecha']);
$cliente = intval($oferta['cliente']);
$contacto = trim($oferta['contacto']);
$referencia_obra = trim($oferta['referencia_obra']);
$fecha_validez = DateTime::createFromFormat('Y-m-d', $oferta['fecha_validez']);
if (empty($oferta['fecha_validez']) || is_null($oferta['fecha_validez'])) {
    $fecha_entrega = '';
} else {
    $fecha_entrega = DateTime::createFromFormat('Y-m-d', $oferta['fecha_entrega']);
}
$forma_pago = trim($oferta['forma_pago']);
$forma_pago_alternativa = trim($oferta['forma_pago_alternativa']);
$plazo = trim($oferta['plazo']);
$isPotencial = boolval($oferta['potencial']);

if (empty($cliente) || is_null($cliente) || $cliente <= 0) {
    incoLogWrite('[cliente/potencial] No hay valor');
}
$stmt_name = $isPotencial ? 'potenciales_ofertas' : 'clientes_ofertas';
$result = pg_execute($dbconn, $stmt_name, array($cliente));
if (pg_num_rows($result) <= 0) incoLogWrite('[cliente/potencial] No hay resultado');
$cliente = pg_fetch_all($result)[0];
$codigo = $cliente['codigo'];
$nif = $cliente['nif'];
$nombre = $cliente['nombre'];
$direccion = $cliente['direccion'];
$codpo = $cliente['codpo'];
$poblacion = $cliente['poblacion'];
if (empty($idioma)) $idioma = trim($cliente['idioma']);
incoI18nSetLang($idioma, 'oferta');

if ((empty($plazo) || is_null($plazo)) && (empty($fecha_validez) || is_null($fecha_validez)))  {
    $plazoEntrega = '';
} else {
    if (empty($plazo) || is_null($plazo)) {
        $plazoEntrega = ''; // $fecha_entrega->format('d - m - Y');
    } else {
        $plazoEntrega = $plazo;
    }
}

if (empty($forma_pago_alternativa) || is_null($forma_pago_alternativa)) {
    $result = pg_execute($dbconn, 'fopago_ofertas', array($forma_pago));
    if (pg_num_rows($result) <= 0) incoLogWrite('[fopago] No hay resultado');
    $forma_pago = pg_fetch_all($result)[0]['nombre'];
    $forma_pago = IncoTranslatorFix::translate($dbconn, $forma_pago, $idioma);
} else {
    $forma_pago = $forma_pago_alternativa;
}

if (empty($oferta['vendedor'])) {
    $vendedor = ['codigo' => 0, 'tipo' => -1];
} else {
    $result = pg_execute($dbconn, 'vendedor', [$oferta['vendedor']]);
    if (pg_num_rows($result) <= 0) incoLogWrite('[vendedor] No hay resultado');
    $vendedor = pg_fetch_all($result)[0];
}

class PDF extends Fpdi
{

    function Header()
    {
        global $numero_oferta, $numero_revision, $fecha, $nif, $contacto, $referencia_obra, $codigo, $nombre, $direccion, $codpo, $poblacion, $plazoEntrega;

        $this->SetFont('dejavusans', 'B', 22);
        $this->SetY(10);
        $txt = gettext('OFERTA');
        $this->Cell(0, 4, ucwords(strtolower($txt)), 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Cell(0, 4, '', 0, 1); // Ln() hace salto demasiado grande
        $this->Cell(0, 4, sprintf(gettext('Hoja: %d'), $this->PageNo()), 0, 1, 'R');
        $this->Ln();
        $this->Ln();
        $y = $this->GetY();

        $this->SetY(7);
        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
        $this->Image(INCO_DIR_IMAGENES . 'piepagina.jpg', 0, 278, $this->w + 1, 0, 'jpg', '', '', true, 300);

        $this->SetY($y);
        $nOferta = strval($numero_oferta);
        if ($numero_revision > 0) {
            $nOferta .= ' - R' . $numero_revision;
        }
        $this->Cell(35, 4, gettext('Oferta Nº'), 0, 0);
        $this->Cell(42, 4, $nOferta, 0, 1, 'L');
        $this->Cell(35, 4, gettext('Fecha'), 0, 0);
        $this->Cell(42, 4, $fecha->format('d-m-Y'), 0, 1, 'L');
        $this->Cell(35, 4, gettext('N.I.F.'), 0, 0);
        $this->Cell(42, 4, $nif, 0, 1, 'L');
        $this->Cell(35, 4, gettext('A la atención de'), 0, 0);
        $this->Cell(42, 4, $contacto, 0, 1, 'L');
        $this->Cell(35, 4, gettext('Obra'), 0, 0);
        $this->Cell(42, 4, $referencia_obra, 0, 1, 'L');
        $this->Cell(35, 4, gettext('Plazo de entrega'), 0, 0);
        $this->Cell(42, 4, $plazoEntrega, 0, 1, 'L');

        // Datos del cliente
        $this->setCellPaddings('3', '1', '3', '1');
        $this->SetY($y);
        $x = 111;
        $this->SetX($x);
        $this->Cell(0, 4, $codigo, 'LTR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $nombre, 'LR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $direccion, 'LR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, $codpo . ' - ' . $poblacion, 'LR', 1);
        $this->SetX($x);
        $this->Cell(0, 4, '', 'LRB', 1);
        $this->SetCellPadding(0);
    }

    function Footer()
    {
        global $vendedor;

        $this->SetFont('dejavusans', 'B', 8);
        $this->SetY(-55);
        $w_disponible = $this->w - $this->lMargin - $this->rMargin;
        $w = $w_disponible / 2;

        if ($vendedor['tipo'] == 0) { // 0 --> nacional
            $this->Image(INCO_DIR_IMAGENES . 'valoferta.jpg', $this->GetX() + 30, $this->GetY() + 4, 0, 16, 'jpg', '', '', true, 300);
        }
        $this->Cell($w, 4, gettext('Fecha y firma de la empresa'), 0, 0);
        $this->Cell(0, 4, gettext('Fecha, firma y sello del cliente'), 0, 1, 'R');
        $this->SetFont('dejavusans', '', 6);
        $this->Cell($w, 4, date('d-m-Y'), 0, 1);
        if ($vendedor['tipo'] == 0) {
            $this->Cell($w, 4, 'Raúl Gutiérrez', 0, 1);
            $this->Cell($w, 4, 'Director Comercial', 0, 1);
        } else {
            $this->Cell($w, 4, 'Mariola Retamero', 0, 1);
            $this->Cell($w, 4, 'Comercial', 0, 1);
        }

        $this->SetFont('dejavusans', '', 6);
        $this->SetY(-25);
        $this->Cell(0, 4, gettext('FGE-01-01 Revisión: 1 Fecha: 01/04/05'), 0, 0);
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 4, gettext('Conforme con la oferta y con las CGV.'), 0, 1, 'R');
    }
}

$pdf = new PDF();
$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('OFERTA');
$pdf->SetTitle('Oferta ' . $numero_oferta);
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.17); //1px = 0.085mm
$pdf->SetMargins(10, 63);
$pdf->SetAutoPageBreak(true, 60);
$pdf->AddPage();

// Productos de la oferta. Cabecera
$pdf->SetFillColor(224); // Oscuro: 192
$pdf->Cell(16, 4, gettext('Partida'), 1, 0, 'C', true);
$pdf->Cell(88, 4, gettext('Descripción'), 1, 0, 'C', true);
$pdf->Cell(17, 4, gettext('Cantidad'), 1, 0, 'C', true);
$pdf->Cell(10, 4, gettext('UM'), 1, 0, 'C', true);
$pdf->Cell(23, 4, gettext('Precio (€/UM)'), 1, 0, 'C', true);
$pdf->Cell(20, 4, gettext('Importe (€)'), 1, 0, 'C', true);
$pdf->Cell(16, 4, gettext('Peso (kg)'), 1, 1, 'C', true);
// Productos de la oferta. Lineas
$total = 0;
$maxPartida = 0;
for ($i = 0; $i < count($ofertalin); $i++) {
    $partida = trim($ofertalin[$i]['partida']);
    $descripcion = IncoTranslatorFix::translate($dbconn, trim($ofertalin[$i]['descripcion_comercial']), $idioma);
    $cantidad = floatval($ofertalin[$i]['cantidad']);
    $result = pg_execute($dbconn, 'unidadesmedida_ofertaslin', array(intval($ofertalin[$i]['unidad_medida'])));
    if (pg_num_rows($result) <= 0) incoLogWrite('[unidades_medida] No hay resultado');
    $unidad_medida = trim(pg_fetch_all($result)[0]['abreviatura']);
    $unidad_medida = str_replace('2', html_entity_decode('&sup2;', ENT_COMPAT, 'UTF-8'), $unidad_medida);
    $precio = floatval($ofertalin[$i]['precio']);
    $peso = floatval($ofertalin[$i]['peso']);
    if ($partida == 'V') {
        $importe = '';
    } else {
        if (intval($partida) > $maxPartida) {
            $maxPartida = intval($partida);
        }
        $total += round($cantidad * $precio, 2);
        $importe = number_format($cantidad * $precio, 2, ',', '.');
    }
    $cantidad = number_format($cantidad, 2, ',', '.');
    $precio = number_format($precio, 2, ',', '.');
    $peso = number_format($peso, 2, ',', '.');

    // $lines = ceil($pdf->GetStringWidth($descripcion) / 90);
    $height = $pdf->getStringHeight(88, $descripcion); // $lines * 4;
    $aux = explode(' | ', $descripcion);
    if (count($aux) > 1) {
        $descripcion = str_replace(' | ', PHP_EOL, $descripcion);
        // $height += 4;
        $height = $pdf->getStringHeight(88, $descripcion);
    }
    $height = ceil($height);

    $pdf->Cell(16, $height, $partida, 1, 0, 'C');
    $pdf->MultiCell(88, $height, $descripcion, 1, 'L', false, 0);
    $pdf->Cell(17, $height, $cantidad, 1, 0, 'R');
    $pdf->Cell(10, $height, $unidad_medida, 1, 0, 'C');
    $pdf->Cell(23, $height, $precio, 1, 0, 'R');
    $pdf->Cell(20, $height, $importe, 1, 0, 'R');
    $pdf->Cell(16, $height, $peso, 1, 1, 'R');

    if ($i == count($ofertalin) - 1) {
        $pdf->Ln(2);
    }
}

$trans = trim($oferta['transporte']);
$pdf->SetY($pdf->GetY() - 2);
$maxPartida++;
if (intval($oferta['portes']) == 2 ) {
    $txt = gettext('TRANSPORTE NO INCLUIDO');
    $numeroPortes = '';
    $um = '';
    $precio = '';
    $importe = '';
} else {
    $txt = gettext('TRANSPORTE MATERIAL');
    $trans = trim($oferta['transporte']);
    if (!empty($trans) && !is_null($trans)) {
        $txt = $trans;
    }
    $numeroPortes = intval($oferta['portes_numero']);
    $um = 'UD';
    $precio = number_format(floatval($oferta['portes_importe']), 2, ',', '.');
    $importe = $numeroPortes * floatval($oferta['portes_importe']);
    $importe = number_format($importe, 2, ',', '.');
}
$height = $pdf->getStringHeight(88, $txt);
$height = ceil($height);
$pdf->Cell(16, $height, $maxPartida, 1, 0, 'C');
$pdf->MultiCell(88, $height, $txt, 1, 'L', false, 0);
$pdf->Cell(17, $height, $numeroPortes, 1, 0, 'R');
$pdf->Cell(10, $height, $um, 1, 0, 'C');
$pdf->Cell(23, $height, $precio, 1, 0, 'R');
$pdf->Cell(20, $height, $importe, 1, 0, 'R');
$pdf->Cell(16, $height, '', 1, 1, 'R');

$maxPartida++;
$txt = gettext('TRANSPORTES ADICIONALES');
$numeroPortes = '';
$um = '';
$precio = '';
$importe = '';
if (intval($oferta['portes_exceso']) == 2 ) {
	$precio = gettext('SUS MEDIOS');
} else {
	$precio = number_format(floatval($oferta['portes_importe_exceso']), 2, ',', '.');
}
$height = $pdf->getStringHeight(88, $txt);
$height = ceil($height);
$pdf->Cell(16, $height, $maxPartida, 1, 0, 'C');
$pdf->MultiCell(88, $height, $txt, 1, 'L', false, 0);
$pdf->Cell(17, $height, $numeroPortes, 1, 0, 'R');
$pdf->Cell(10, $height, $um, 1, 0, 'C');
$pdf->Cell(23, $height, $precio, 1, 0, 'R');
$pdf->Cell(20, $height, $importe, 1, 0, 'R');
$pdf->Cell(16, $height, '', 1, 1, 'R');
$pdf->Ln(2);

/*$pdf->Cell(164, 4, $tr->getTranslate('Total') . ' (€)', 0, 0, 'R');
$pdf->Cell(20, 4, number_format($total, 2, ',', '.'), 0, 1, 'R');*/

$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, gettext('INCREMENTOS SI PROCEDE NO INCLUIDOS EN ESTA OFERTA, VER HOJA ANEXA DE CONDICIONES'), 0, 1, 'C');

// $pdf->SetY(246 - 66);
$pdf->Ln();
$pdf->Cell(0, 4, gettext('OFERTA SUJETA A LAS SIGUIENTES CONDICIONES:'), 0, 1);
$pdf->SetFont('');
$pdf->Ln();
$condi = trim($oferta['condiciones']);
if (!empty($condi) && !is_null($condi)) {
    $pdf->MultiCell(184, 4, $condi . PHP_EOL, 0, 'J', false, 1);
}

/**** EMPIEZA NUEVO ****/
$secciones = array_unique(array_values(array_column($ofertalin, 'seccion')));
$indexCondiciones = 1;
$subIndexCondiciones = [];

if ($oferta['plazo'] || $oferta['fecha_entrega']) {
    $subIndexCondiciones[$indexCondiciones] = 1;
    $txt = sprintf(gettext('%d. PLAZO DE ENTREGA'), $indexCondiciones);
    $pdf->Cell(0, 4, $txt, 0, 1);
    $txt = gettext('El plazo de entrega se considera a partir de la fecha en que recibamos la confirmación de pedido firmada y las condiciones de pago se hayan resuelto. En caso de necesitar aprovisionamiento de materia prima, estará condicionado al plazo de entrega de la siderúrgica.');
    $pdf->MultiCell(0, 4, $txt . PHP_EOL);
    $pdf->Ln(4);
    $indexCondiciones++;
}

$subIndexCondiciones[$indexCondiciones] = 1;
$txt = sprintf(gettext('%d. CONDICIONES DE SUMINISTROS'), $indexCondiciones);
$pdf->Cell(0, 4, $txt, 0, 1);
$txts = [
    sprintf(gettext('Oferta válida hasta %s.'), $fecha_validez->format('d - m - Y')),
    gettext('Cualquier variación de material, perfil o cantidad supondrá una modificación en el precio.'),
    gettext('Ingeniería y Construcción del Perfil S.A. se reserva el derecho de modificar los precios de la presente oferta, por tanto el producto se facturará sobre la base de la tarifa en vigor en el momento de la validación del pedido (despiece, forma de pago y plazo de entrega) y bajo reserva de disponibilidad.'),
    gettext('Ingeniería y Construcción del Perfil S.A. se reserva el derecho de no fabricar pedidos de perfiles inferiores a 250 M2.')
];
foreach ($txts as $txt) $pdf->MultiCell(0, 4, $txt . PHP_EOL);
$pdf->setFont('', 'b');
$txt = gettext('Para albaranes inferiores a 500 €, se aplicará un recargo por gestión de 150 €.');
$pdf->Cell(0, 4, $txt, 0, 1);
$pdf->setFont('', '');
$pdf->Ln(4);
$indexCondiciones++;

if (in_array(1, $secciones)) {
    $subIndexCondiciones[$indexCondiciones] = 1;
    $txt = sprintf(gettext('%d. CONDICIONES DE SUMINISTRO DE CHAPA'), $indexCondiciones);
    $pdf->Cell(0, 4, $txt, 0, 1);

    $productos = array_unique(
        array_unique(
            array_column(
                array_filter($ofertalin, function ($lin) { return $lin['seccion'] == 1; }),
                'producto'
            )
        )
    );
    $espesores = [];
    foreach ($productos as $producto) {
        $producto_dimmaxs = pg_select($dbconn, 'productos_dimmaxs', ['producto' => $producto]);
        if (empty($producto_dimmaxs)) {
            $espesores[$producto] = [];
        } else {
            $espesores[$producto] = array_unique(
                array_unique(
                    array_column(
                        array_filter($ofertalin, function ($lin) use ($producto) { return $lin['seccion'] == 1 && $lin['producto'] == $producto; }),
                        'espesor'
                    )
                )
            );
        }
    }

    $txt = sprintf(gettext('%d.%d. EMBALAJE'), $indexCondiciones, $subIndexCondiciones[$indexCondiciones]);
    $pdf->Cell(0, 4, $txt, 0, 1);
    $subIndexCondiciones[$indexCondiciones]++;
    foreach ($productos as $producto) {
        $producto_db = pg_select($dbconn, 'productos', ['codigo' => $producto])[0];

        $result = pg_query_params($dbconn, 'select r.codigo, r.recargo, r.penaliza_por, r.operador_logico from recargos r, recargos_productos rp where r.tipo_recargo = $1 and rp.producto = $2 and r.codigo = rp.recargo', [1, $producto]) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
        $recargos = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
        foreach ($recargos as $recargo) {
            $descripcion = IncoTranslatorFix::translate($dbconn, trim($producto_db['nombre_comercial']), $idioma);
            $txt = $descripcion . ': ' . gettext('precios válidos para') . ' ';
            switch (trim($recargo['penaliza_por'])) {
                case 'PAQ': $penaliza_por = ['p' => ngettext('paquete', 'paquetes', 1), 's' => ngettext('paquete', 'paquetes', 0)]; break;
                case 'PED': $penaliza_por = ['p' => ngettext('pedido', 'pedidos', 1), 's' => ngettext('pedido', 'pedidos', 0)]; break;
                case 'UDS': $penaliza_por = ['p' => ngettext('unidad', 'unidades', 1), 's' => ngettext('unidad', 'unidades', 0)]; break;
                default: $penaliza_por = ['p' => '', 's' => '']; break;
            }
            $txt .= $penaliza_por['p'] . ' ';
            $operadorLogico_inverse = $recargo['operador_logico'] == 'AND' ? gettext('o') : gettext('y');

            $result = pg_query_params($dbconn, 'select rc.funcion_agregado, rc.campo, rc.operador, rc.min, rc.max from recargos_condiciones rc where rc.recargo = $1', [$recargo['codigo']]) or incoLogWrite( 'La consulta fallo [recargos - condiciones]: ' . pg_last_error());
            $condiciones = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
            foreach ($condiciones as $condicion) {
                switch (trim($condicion['campo'])) {
                    case 'cantidad_total':
                        $um_db = pg_select($dbconn, 'unidades_medida', ['codigo' => $producto_db['unidad_medida']])[0];
                        $um = trim($um_db['abreviatura']);
                        break;
                    case 'peso': $um = 'kg'; break;
                    default: $um = ''; break;
                }
                switch (trim($condicion['operador'])) {
                    case '>':
                    case '>=': $operador_inverse = gettext('menores de'); break;
                    case '<':
                    case '<=': $operador_inverse = gettext('mayores de'); break;
                    default: $operador_inverse = ''; break;
                }
                $txt .= $operador_inverse . ' ' . number_format($condicion['min'], 0, ',', '.') . ' ' . $um;
                if (count($condiciones) > 1 && $condicion != end($condiciones)) {
                    $txt .= ' ' . $operadorLogico_inverse . ' ';
                }
            }
            if ($oferta['aplicar_recargos'] == 1) {
                $txt .= sprintf(gettext(' (en caso contrario recargo de %s €/%s)'), number_format($recargo['recargo'], 1, ',', '.'), $penaliza_por['s']);
            }
            $txt .= '.';
            $pdf->MultiCell(0, 4, $txt . PHP_EOL);
        }
    }
    $pdf->Ln(4);

    $txt = sprintf(gettext('%d.%d. FABRICACIÓN'), $indexCondiciones, $subIndexCondiciones[$indexCondiciones]);
    $pdf->Cell(0, 4, $txt, 0, 1);
    $subIndexCondiciones[$indexCondiciones]++;

    $result = pg_query_params($dbconn, 'select r.codigo, min(coalesce(rc.max, rc.min)) as min from recargos r, recargos_productos rp, recargos_condiciones rc where r.tipo_recargo = $1 and rc.campo = $2 and r.codigo = rp.recargo and r.codigo = rc.recargo group by 1', [2, 'c_cantidad']) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
    $tirada_general = pg_num_rows($result) > 0 ? pg_fetch_all($result)[0] : [];
    if (!empty($tirada_general)) {
        // $txt_tirada = 'Número mínimo de ' . number_format($tirada_general['min'], 0, ',', '.') . ' unidades por longitud. En caso contrario se considerará como tirada';
        $txt_tirada = sprintf(gettext('Número mínimo de %s unidades por longitud. En caso contrario se considerará como tirada.'), number_format($tirada_general['min'], 0, ',', '.'));
        $pdf->Cell(0, 4, $txt_tirada, 0, 1);
    }

    foreach ($productos as $producto) {
        $producto_db = pg_select($dbconn, 'productos', ['codigo' => $producto])[0];
        $descripcion = IncoTranslatorFix::translate($dbconn, trim($producto_db['nombre_comercial']), $idioma);
        $txt = $descripcion . ':';

        $result = pg_query_params($dbconn, 'select r.codigo, min(coalesce(rc.max, rc.min)) as min from recargos r, recargos_productos rp, recargos_condiciones rc where r.tipo_recargo = $1 and rp.producto = $2 and rc.campo = $3 and r.codigo = rp.recargo and r.codigo = rc.recargo group by 1', [2, $producto, 'c_cantidad']) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
        $tirada = pg_num_rows($result) > 0 ? pg_fetch_all($result)[0] : [];
        if (!empty($tirada)) {
            $recargo_db = pg_select($dbconn, 'recargos', ['codigo' => $tirada['codigo']])[0];
            // $txt_tirada = ' número mínimo de ' . number_format($tirada['min'], 0, ',', '.') . ' unidades por longitud. En caso contrario se considerará como tirada';
            if ($oferta['aplicar_recargos'] == 1) {
                // $txt_tirada .= ' y se aplicará un recargo de ' . number_format($recargo_db['recargo'], 1, ',', '.') . ' €/unidad';
                // $txt_tirada = ' Se considera como tirada y se aplicará un recargo de ' . number_format($recargo_db['recargo'], 1, ',', '.') . ' €/unidad';
                $txt_tirada = sprintf(gettext(' Se considera como tirada y se aplicará un recargo de %s €/unidad'), number_format($recargo_db['recargo'], 1, ',', '.'));
                $txt .= $txt_tirada . '.';
            }
            // $txt .= $txt_tirada . '.';
        }

        if (empty($espesores[$producto])) {
            $result = pg_query_params($dbconn, 'select max(coalesce(rc.max, rc.min)) as max from recargos r, recargos_productos rp, recargos_condiciones rc where r.tipo_recargo = $1 and rp.producto = $2 and rc.campo = $3 and r.codigo = rp.recargo and r.codigo = rc.recargo', [2, $producto, 'c_longitud']) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
            $max = pg_fetch_all($result)[0]['max'];

            // $txt_precio_validos = ' precios válidos para longitudes entre ' . number_format($max / 1000, 2, ',', '.') . ' m y ' . number_format($producto_db['dimension_maxima'] / 1000, 2, ',', '.') . ' m.';
            $txt_precio_validos = sprintf(gettext(' precios válidos para longitudes entre %s m y %s m.'), number_format($max / 1000, 2, ',', '.'), number_format($producto_db['dimension_maxima'] / 1000, 2, ',', '.'));
            if (isset($txt_tirada)) $txt_precio_validos[1] = strtoupper($txt_precio_validos[1]);
            $txt .= $txt_precio_validos;
        } else {
            foreach ($espesores[$producto] as $espesor) {
                $espesor_db = pg_select($dbconn, 'espesores', ['codigo' => $espesor])[0];

                $result = pg_query_params($dbconn, 'select longitud from productos_dimmaxs where producto = $1 and espesor <= $2 order by espesor desc limit 1', [$producto, $espesor_db['valor']]) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
                $producto_db_dimmax = $result === false || pg_num_rows($result) <= 0 ? $producto_db['dimension_maxima'] : pg_fetch_all($result)[0]['longitud'];

                $result = pg_query_params($dbconn, 'select max(coalesce(rc.max, rc.min)) as max from recargos r, recargos_productos rp, recargos_condiciones rc where r.tipo_recargo = $1 and rp.producto = $2 and rc.campo = $3 and r.codigo = rp.recargo and r.codigo = rc.recargo', [2, $producto, 'c_longitud']) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
                $max = pg_fetch_all($result)[0]['max'];

                // $txt_precio_validos = ' precios válidos para longitudes entre ' . number_format($max / 1000, 2, ',', '.') . ' m y ' . number_format($producto_db_dimmax / 1000, 2, ',', '.') . ' m para el espesor ' . number_format($espesor_db['valor_venta'], 2, ',', '.') . ' mm.';
                $txt_precio_validos = sprintf(gettext(' precios válidos para longitudes entre %s m y %s m para el espesor %s mm.'), number_format($max / 1000, 2, ',', '.'), number_format($producto_db_dimmax / 1000, 2, ',', '.'), number_format($espesor_db['valor_venta'], 2, ',', '.'));
                if (isset($txt_tirada)) $txt_precio_validos[1] = strtoupper($txt_precio_validos[1]);
                $txt .= $txt_precio_validos;
            }
        }

        if ($oferta['aplicar_recargos'] == 1) {
            $result = pg_query_params($dbconn, 'select r.codigo, r.recargo, r.penaliza_por, r.operador_logico from recargos r, recargos_productos rp where r.tipo_recargo = $1 and rp.producto = $2 and r.codigo = rp.recargo', [2, $producto]) or incoLogWrite( 'La consulta fallo [recargos]: ' . pg_last_error());
            $recargos = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
            foreach ($recargos as $recargo) {
                switch (trim($recargo['penaliza_por'])) {
                    case 'PAQ': $penaliza_por = ['p' => ngettext('paquete', 'paquetes', 1), 's' => ngettext('paquete', 'paquetes', 0)]; break;
                    case 'PED': $penaliza_por = ['p' => ngettext('pedido', 'pedidos', 1), 's' => ngettext('pedido', 'pedidos', 0)]; break;
                    case 'UDS': $penaliza_por = ['p' => ngettext('unidad', 'unidades', 1), 's' => ngettext('unidad', 'unidades', 0)]; break;
                    default: $penaliza_por = ['p' => '', 's' => '']; break;
                }
                $operadorLogico_inverse = $recargo['operador_logico'] == 'AND' ? gettext('o') : gettext('y');

                $result = pg_query_params($dbconn, 'select rc.funcion_agregado, rc.campo, rc.operador, rc.min, rc.max from recargos_condiciones rc where rc.recargo = $1 and rc.campo = $2', [$recargo['codigo'], 'c_longitud']) or incoLogWrite( 'La consulta fallo [recargos - condiciones]: ' . pg_last_error());
                $condiciones = pg_num_rows($result) > 0 ? pg_fetch_all($result) : [];
                if ($condiciones === false || empty($condiciones)) continue;

                $txt_condicion = 'Para longitudes ';
                foreach ($condiciones as $condicion) {
                    switch (trim($condicion['operador'])) {
                        case '>':
                        case '>=': $txt_condicion .= sprintf(gettext('mayores de %s m'), number_format($condicion['min'] / 1000, 2, ',', '.')); break;
                        case '<':
                        case '<=': $txt_condicion .= sprintf(gettext('menores de %s m'), number_format($condicion['min'] / 1000, 2, ',', '.')); break;
                        default: $txt_condicion .= sprintf(gettext('entre %s m y %s m'), number_format($condicion['min'] / 1000, 2, ',', '.'), number_format($condicion['max'] / 1000, 2, ',', '.')); break;
                    }
                }
                $txt .= ' ' . $txt_condicion . sprintf(gettext(' se aplicará un recargo de %s €/%s.'), number_format($recargo['recargo'], 1, ',', '.'), $penaliza_por['s']);
            }
        }
        $pdf->MultiCell(0, 4, $txt . PHP_EOL);
    }
    $pdf->Ln(4);
    $indexCondiciones++;
}

if (in_array(2, $secciones)) {
    $subIndexCondiciones[$indexCondiciones] = 1;
    $txt = sprintf(gettext('%d. CONDICIONES DE FABRICACIÓN DE REMATES'), $indexCondiciones);
    $pdf->Cell(0, 4, $txt, 0, 1);
    $pdf->Ln(2);

    $pdf->Cell(92, 4, gettext('FORMATO'), 0, 1, 'C');
    $pdf->Cell(0, 4, gettext('Longitud a determinar'), 0, 1, 'C');
    $pdf->SetX($pdf->GetX() + 72);
    $pdf->Cell(40, 4, '', 0, 1, '', true);
    $pdf->SetX($pdf->GetX() + 72);
    $pdf->Cell(40, 4, '', 0, 0, '', true);
    $pdf->Cell(0, 4, '1250 mm', 0, 1);
    $pdf->SetX($pdf->GetX() + 72);
    $pdf->Cell(40, 4, '', 0, 1, '', true);
    $pdf->Ln(2);
    $pdf->Cell(15, 4, gettext('Nota:'), 0, 0);
    $pdf->MultiCell(0, 4, gettext('Siempre se factura formato completo. Solo se utiliza una longitud de remate por formato. Los restos de chapa generados se facturan por m2 como chapa lisa. El precio del remate se entiende por metro lineal siendo la longitud mínima a facturar de 1 ml/pieza.') . PHP_EOL);
    $pdf->Cell(30, 4, gettext('Exceso de plegados:'), 0, 0);
    $pdf->MultiCell(0, 4, gettext('Los precios son para remates con 6 plegados como máximo. El exceso de plegados se facturará a 0,85 €/ml por plegado añadido.') . PHP_EOL);
    $pdf->Ln(4);
    $indexCondiciones++;
}

$subIndexCondiciones[$indexCondiciones] = 1;
$txt = sprintf(gettext('%d. TRANSPORTE'), $indexCondiciones);
$pdf->Cell(0, 4, $txt, 0, 1);
$txt = gettext('En caso de recogida por parte del cliente, éste será responsable de disponer los vehículos adecuados para la carga. Si la entrega es en obra el cliente dispondrá de 2 horas y de los medios adecuados para su descarga. Ver CGV adjuntas.');
$pdf->MultiCell(0, 4, $txt . PHP_EOL);
$pdf->Ln(4);

if (in_array(8, $secciones)) {
    $productos = array_unique(
        array_unique(
            array_column(
                array_filter($ofertalin, function ($lin) { return $lin['seccion'] == 8; }),
                'producto'
            )
        )
    );
    if (in_array(310, $productos) && in_array(311, $productos)) {
        $txt = sprintf(gettext('%d.%d. CABALLETES DE TRANSPORTE Y CABALLETES DE DESCARGA'), $indexCondiciones, $subIndexCondiciones[$indexCondiciones]);
        $pdf->Cell(0, 4, $txt, 0, 1);
        $txts = [
            gettext('El suministro de los paquetes de perfil curvado se llevará a cabo mediante el uso de caballetes de transporte metálicos para garantizar la seguridad del transporte y la integridad del material.') . ' ' . gettext('Además, con el propósito de asegurar un adecuado almacenamiento en la obra de los paquetes, se podrán facilitar caballetes de descarga adicionales, los cuales el cliente podrá solicitar al aceptar el pedido de suministro del material.'),
            gettext('Tanto los caballetes de transporte como los de descarga son elementos retornables.') . ' ' . gettext('Se establece un plazo de 30 días a partir de la recepción de la mercancía para la devolución del número de caballetes que el cliente considere oportuno. En caso de que sean devueltos en condiciones óptimas, sin mermar su funcionalidad, se procederá a realizar el abono de los mismos al precio indicado en la oferta. En caso contrario, los caballetes que presenten un deterioro que afecte a su correcto funcionamiento pueden no ser objeto de abono.') . ' ' . gettext('La responsabilidad exclusiva de la devolución de los caballetes recae sobre el cliente, quien, utilizando el medio de transporte que considere apropiado, los entregará en las instalaciones de INCOPERFIL ubicadas en Beniparrell (Valencia), España.')
        ];
        foreach ($txts as $txt) $pdf->MultiCell(0, 4, $txt . PHP_EOL);
        $pdf->Ln(4);
        $subIndexCondiciones[$indexCondiciones]++;
    } elseif (in_array(312, $productos) && in_array(311, $productos)) {
        $txt = sprintf(gettext('%d.%d. ESTRUCTURA DE TRANSPORTE Y CABALLETES DE DESCARGA'), $indexCondiciones, $subIndexCondiciones[$indexCondiciones]);
        $pdf->Cell(0, 4, $txt, 0, 1);
        $txts = [
            gettext('El suministro de los paquetes de perfil curvado se llevará a cabo mediante el uso de una estructura de transporte metálica para garantizar la seguridad del transporte y la integridad del material. Esta estructura de transporte es retornable, y a partir del segundo día de la recepción de la mercancía en la obra, INCOPERFIL procederá a su recogida en la misma dirección de entrega.') . ' ' . gettext('Es responsabilidad exclusiva del cliente la correcta descarga, almacenamiento, custodia y carga de la misma en el transporte de retorno. En caso de incumplimiento de estas responsabilidades, INCOPERFIL podrá facturar al cliente la estructura de transporte por un importe de 6.500€.'),
            gettext('Además, con el propósito de asegurar un adecuado almacenamiento en la obra de los paquetes, se podrán facilitar caballetes de descarga adicionales, los cuales el cliente podrá solicitar al aceptar el pedido de suministro del material.'),
            gettext('Tanto los caballetes de transporte como los de descarga son elementos retornables.') . ' ' . gettext('Se establece un plazo de 30 días a partir de la recepción de la mercancía para la devolución del número de caballetes que el cliente considere oportuno. En caso de que sean devueltos en condiciones óptimas, sin mermar su funcionalidad, se procederá a realizar el abono de los mismos al precio indicado en la oferta. En caso contrario, los caballetes que presenten un deterioro que afecte a su correcto funcionamiento pueden no ser objeto de abono.') . ' ' . gettext('La responsabilidad exclusiva de la devolución de los caballetes recae sobre el cliente, quien, utilizando el medio de transporte que considere apropiado, los entregará en las instalaciones de INCOPERFIL ubicadas en Beniparrell (Valencia), España.')
        ];
        foreach ($txts as $txt) $pdf->MultiCell(0, 4, $txt . PHP_EOL);
        $pdf->Ln(4);
        $subIndexCondiciones[$indexCondiciones]++;
    } elseif (in_array(310, $productos)) {
        $txt = sprintf(gettext('%d.%d. CABALLETES DE TRANSPORTE'), $indexCondiciones, $subIndexCondiciones[$indexCondiciones]);
        $pdf->Cell(0, 4, $txt, 0, 1);
        $txts = [
            gettext('El suministro de los paquetes de perfil curvado se llevará a cabo mediante el uso de caballetes de transporte metálicos para garantizar la seguridad del transporte y la integridad del material.'),
            gettext('Los caballetes de transporte son elementos retornables.') . ' ' . gettext('Se establece un plazo de 30 días a partir de la recepción de la mercancía para la devolución del número de caballetes que el cliente considere oportuno. En caso de que sean devueltos en condiciones óptimas, sin mermar su funcionalidad, se procederá a realizar el abono de los mismos al precio indicado en la oferta. En caso contrario, los caballetes que presenten un deterioro que afecte a su correcto funcionamiento pueden no ser objeto de abono.') . ' ' . gettext('La responsabilidad exclusiva de la devolución de los caballetes recae sobre el cliente, quien, utilizando el medio de transporte que considere apropiado, los entregará en las instalaciones de INCOPERFIL ubicadas en Beniparrell (Valencia), España.')
        ];
        foreach ($txts as $txt) $pdf->MultiCell(0, 4, $txt . PHP_EOL);
        $pdf->Ln(4);
        $subIndexCondiciones[$indexCondiciones]++;
    } elseif (in_array(312, $productos)) {
        $txt = sprintf(gettext('%d.%d. ESTRUCTURA DE TRANSPORTE'), $indexCondiciones, $subIndexCondiciones[$indexCondiciones]);
        $pdf->Cell(0, 4, $txt, 0, 1);
        $txts = [
            gettext('El suministro de los paquetes de perfil curvado se llevará a cabo mediante el uso de una estructura de transporte metálica para garantizar la seguridad del transporte y la integridad del material. Esta estructura de transporte es retornable, y a partir del segundo día de la recepción de la mercancía en la obra, INCOPERFIL procederá a su recogida en la misma dirección de entrega.') . ' ' . gettext('Es responsabilidad exclusiva del cliente la correcta descarga, almacenamiento, custodia y carga de la misma en el transporte de retorno. En caso de incumplimiento de estas responsabilidades, INCOPERFIL podrá facturar al cliente la estructura de transporte por un importe de 6.500€.')
        ];
        foreach ($txts as $txt) $pdf->MultiCell(0, 4, $txt . PHP_EOL);
        $pdf->Ln(4);
        $subIndexCondiciones[$indexCondiciones]++;
    }
}
$indexCondiciones++;

$subIndexCondiciones[$indexCondiciones] = 1;
$txt = sprintf(gettext('%d. POLÍTICA DE DATOS'), $indexCondiciones);
$pdf->Cell(0, 4, $txt, 0, 1);
$txt = gettext("Información básica sobre protección de datos:\nResponsable: Ingeniería y Construcción del Perfil, S.A. Finalidades: Gestionar los servicios contratados y facturación. Legitimación: Ejecución de un contrato y consentimiento del interesado. Destinatarios: No se cederán datos a terceros, salvo obligación legal. Derechos: Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, como se explica en la información adicional. Información adicional: Puede consultar la información adicional y detallada sobre Protección de Datos en www.incoperfil.com");
$pdf->MultiCell(0, 4, $txt . PHP_EOL);
$pdf->Ln(4);
$indexCondiciones++;

$subIndexCondiciones[$indexCondiciones] = 1;
$txt = sprintf(gettext('%d. CONDICIONES DE PAGO'), $indexCondiciones);
$pdf->Cell(0, 4, $txt, 0, 1);
$pdf->MultiCell(0, 4, sprintf(gettext('FORMA DE PAGO: %s'), strtoupper($forma_pago)) . PHP_EOL);
$pdf->MultiCell(0, 4, sprintf(gettext('CONDICIONES DE PAGO: %s'), gettext('Sujetas a concesión de crédito por entidad aseguradora. En caso de no obtener crédito, el pago será anticipado. Los precios indicados están sujetos al recargo financiero correspondiente a la forma de pago acordada. Debido a las fluctuaciones en el precio de suministro del acero, nuestra oferta podrá ser revisada en función de dichos incrementos de precio. Precios aplicables exclusivamente a pedidos cuya referencia de obra se haga constar este número de oferta. I.V.A. no incluido. El pedido se ajustará a las condiciones generales de venta CGV (ver hojas anexas).')) . PHP_EOL);
$pdf->Ln(4);
$indexCondiciones++;
/**** TERMINA NUEVO ****/


$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(0, 4, gettext('CARACTERÍSTICAS DE LOS MATERIALES'), 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->Ln(2);
$pdf->MultiCell(37, 8, gettext('Material Denominación'), 'LTB', 'C', false, 0);
$pdf->MultiCell(28, 8, gettext('Anverso Espesor mínimo (um)'), 'TB', 'C', false, 0);
$pdf->MultiCell(28, 8, gettext('Reverso Espesor mínimo (um)'), 'TB', 'C', false, 0);
$pdf->MultiCell(28, 8, gettext('Recubrimiento Zinc o AZ (um)'), 'TB', 'C', false, 0);
$pdf->MultiCell(28, 8, gettext('Clase Acero'), 'TB', 'C', false, 0);
$pdf->MultiCell(18, 8, gettext('CE EN 508'), 'TB', 'C', false, 0);
$pdf->MultiCell(23, 8, gettext('Certificado Material'), 'TBR', 'C', false, 1);
$pdf->Cell(37, 4, 'PRELACADO BASIC', 'L', 0, 'L');
$pdf->Cell(28, 4, '25', 0, 0, 'C');
$pdf->Cell(28, 4, '5', 0, 0, 'C');
$pdf->Cell(28, 4, 'Z>=140 o AZ>=70', 0, 0, 'C');
$pdf->Cell(28, 4, '>=S220GD', 0, 0, 'C');
$pdf->Cell(18, 4, '', 0, 0, 'C');
$pdf->Cell(23, 4, 'X', 'R', 1, 'C');
$pdf->Cell(37, 4, 'PRELACADO', 'L', 0, 'L');
$pdf->Cell(28, 4, '25', 0, 0, 'C');
$pdf->Cell(28, 4, '5', 0, 0, 'C');
$pdf->Cell(28, 4, '>=225', 0, 0, 'C');
$pdf->Cell(28, 4, '>=S220GD', 0, 0, 'C');
$pdf->Cell(18, 4, 'X', 0, 0, 'C');
$pdf->Cell(23, 4, 'X', 'R', 1, 'C');
$pdf->Cell(37, 4, 'HDS 35/225', 'L', 0, 'L');
$pdf->Cell(28, 4, '35', 0, 0, 'C');
$pdf->Cell(28, 4, '7', 0, 0, 'C');
$pdf->Cell(28, 4, '>=225', 0, 0, 'C');
$pdf->Cell(28, 4, '>=S220GD', 0, 0, 'C');
$pdf->Cell(18, 4, 'X', 0, 0, 'C');
$pdf->Cell(23, 4, 'X', 'R', 1, 'C');
$pdf->Cell(37, 4, 'HDX 55/225', 'L', 0, 'L');
$pdf->Cell(28, 4, '55', 0, 0, 'C');
$pdf->Cell(28, 4, '7', 0, 0, 'C');
$pdf->Cell(28, 4, '>=225', 0, 0, 'C');
$pdf->Cell(28, 4, '>=S220GD', 0, 0, 'C');
$pdf->Cell(18, 4, 'X', 0, 0, 'C');
$pdf->Cell(23, 4, 'X', 'R', 1, 'C');
$pdf->Cell(37, 4, 'HDX 55/275', 'L', 0, 'L');
$pdf->Cell(28, 4, '55', 0, 0, 'C');
$pdf->Cell(28, 4, '7', 0, 0, 'C');
$pdf->Cell(28, 4, '>=275', 0, 0, 'C');
$pdf->Cell(28, 4, '>=S220GD', 0, 0, 'C');
$pdf->Cell(18, 4, 'X', 0, 0, 'C');
$pdf->Cell(23, 4, 'X', 'R', 1, 'C');
$pdf->Cell(37, 4, 'HPS200', 'LB', 0, 'L');
$pdf->Cell(28, 4, '200', 'B', 0, 'C');
$pdf->Cell(28, 4, '7', 'B', 0, 'C');
$pdf->Cell(28, 4, '>=225', 'B', 0, 'C');
$pdf->Cell(28, 4, '>=S220GD', 'B', 0, 'C');
$pdf->Cell(18, 4, 'X', 'B', 0, 'C');
$pdf->Cell(23, 4, 'X', 'BR', 1, 'C');

$pdf->Ln(6);
$pdf->SetFont('dejavusans', '', 8);


$pdf = incoPdfAddCGV($pdf, $idioma);

/*if ($pdf->GetY() < $yFirmaSello) {
    $pdf->SetY($yFirmaSello);
} else {
    $pdf->Ln(8);
}
$pdf->SetFont('dejavusans', 'B', 8);
$pdf->Cell(0, 4, $tr->getTranslate('FIRMA Y SELLO DEL CLIENTE'), 0, 1, 'R');*/

//SALIDA
$tipo = $isPotencial ? INCO_DIR_POTENCIALES : INCO_DIR_CLIENTES;
$dir = $tipo . $codigo . '/Ofertas/O_' . $oferta['ejercicio'] . '_' . str_pad($numero_oferta, 6, '0', STR_PAD_LEFT) . '/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'O_' . $oferta['ejercicio'] . '_' . str_pad($numero_oferta, 6, '0', STR_PAD_LEFT) . '_' . $numero_revision . '.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

pg_free_result($result);
pg_close($dbconn);
