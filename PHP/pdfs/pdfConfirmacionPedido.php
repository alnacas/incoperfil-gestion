<?php

require_once __DIR__ . '/../config.php';
require_once __DIR__ . '/ConfirmacionPedido.php';

incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
    incoLogWrite('No hay argumentos');
}

$serie = substr($identificador, 6, 2);
$idioma = isset($argv[3]) ? trim($argv[3]) : 'es';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params('SELECT centro, ejercicio, serie, numero, fecha, cliente, forma_pago, banco, iva, portes, portes_importe as total_portes, identificador_oferta, portes_numero, referencia_cliente, referencia_obra, aplicar_recargos, importe, importe_iva FROM pedidos WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$pedido = pg_fetch_all($result)[0];
$aplicar_recargos = intval($pedido['aplicar_recargos']) == 1;
$pedido['centro'] = intval($pedido['centro']);
$pedido['fecha'] = DateTime::createFromFormat('Y-m-d', $pedido['fecha'])->format('d/m/Y');
$coef_portes = intval($pedido['iva']) == 0 ? 0 : 0.21;
$pedido['iva'] = intval($pedido['iva']) == 0 ? 0 : 21;
if (intval($pedido['portes']) == 0 || intval($pedido['portes']) == 3) {
    $pedido['importe'] += $pedido['total_portes'];
    $pedido['importe_iva'] += round($pedido['total_portes'] * $coef_portes, 2);
}

$result = pg_query_params($dbconn, 'select trim(nombre) as nombre, banco, recargo_financiero, descuento_pronto_pago, tipo from fopago where codigo = $1', [$pedido['forma_pago']]);
$pedido['forma_pago'] = pg_fetch_all($result)[0];
$result = pg_query_params($dbconn, 'SELECT trim(nombre) as nombre, (iban || cbanco || csucursal || cdc || cuenta)::CHARACTER(63) AS cuenta FROM bancos WHERE codigo = $1', [$pedido['banco']]);
$pedido['forma_pago']['banco'] = pg_fetch_all($result)[0];
// incoLogWrite('Pedido: ' . json_encode($pedido), false);

$result = pg_query_params($dbconn, 'SELECT codigo, nif, nombre, direccion, codpo, poblacion, provincia, idioma, cpais as pais, iban, cbanco, csucursal, cdc, cuenta, direccionadm, codpoadm, poblacionadm, provinciaadm, dpago1, dpago2, dpago3 FROM clientes WHERE codigo = $1', [$pedido['cliente']]);
if (pg_num_rows($result) <= 0) incoLogWrite('[cliente] No hay resultado');
$cliente = pg_fetch_all($result)[0];
$iban = str_pad(trim($cliente['iban']), 4, '0', STR_PAD_LEFT);
$banco = str_pad(trim($cliente['cbanco']), 4, '0', STR_PAD_LEFT);
$sucursal = str_pad(trim($cliente['csucursal']), 4, '0', STR_PAD_LEFT);
$dc = str_pad(trim($cliente['cdc']), 2, '0', STR_PAD_LEFT);
$cuenta = str_pad(trim($cliente['cuenta']), 10, '0', STR_PAD_LEFT);
$cliente['cuenta'] = substr($iban . $banco . $sucursal . $dc . $cuenta, 0, 20) . '****';

$result = pg_query_params($dbconn, 'SELECT nombre, nombre2, cee FROM paises WHERE codigo = $1', [$cliente['pais']]);
$cliente['pais'] = pg_fetch_all($result)[0];
if (empty($idioma)) $idioma = trim($cliente['idioma']);
if ($idioma != 'es') {
    $cliente['pais']['nombre'] = trim($cliente['pais']['nombre2']);
}
incoI18nSetLang($idioma, 'confirmacion_pedido');
// incoLogWrite('Cliente: ' . json_encode($cliente), false);

$result = pg_query_params($dbconn, 'SELECT paquete, trim(referencia_plano) as ref_plano, trim(descripcion_comercial) as descripcion, trim(um.abreviatura) as unidad_medida, case seccion when 1 then c_posicion else 0 end as posicion, case seccion when 1 then c_longitud when 2 then r_largo when 3 then p_longitud when 4 then o_longitud when 5 then a_longitud when 6 then l_longitud when 8 then l_longitud else 0 end as longitud, pl.precio, sum(case seccion when 1 then c_cantidad when 2 then r_piezas when 3 then p_cantidad when 4 then o_cantidad when 5 then a_modulos when 6 then l_cantidad when 7 then f_cantidad when 8 then v_cantidad when 9 then e_cantidad end) as uds, sum(vw.cantidad_total) as total, sum(vw.importe) as importe, sum(peso)::numeric(10, 2) as peso FROM pedidoslin pl, vw_pedidoslin_proforma vw, unidades_medida um WHERE pl.identificador = $1 and pl.identificador = vw.identificador and pl.linea = vw.linea and pl.unidad_medida = um.codigo GROUP BY paquete, referencia_plano, descripcion_comercial,abreviatura, posicion, longitud, pl.precio ORDER BY paquete, referencia_plano, descripcion_comercial, abreviatura, posicion, longitud, precio', array($identificador)) or incoLogWrite('La consulta fallo [pedidos]: ' . pg_last_error());
$pedido['lineas'] = pg_num_rows($result) == 0 ? [] : pg_fetch_all($result);
// incoLogWrite('Lineas: ' . json_encode($pedido['lineas']), false);

$result = pg_query_params($dbconn, 'select linea, trim(descripcion) as descripcion, importe from pedidos_recargos where identificador = $1', [$identificador]);
$pedido['recargos'] = pg_num_rows($result) == 0 || !$aplicar_recargos ? [] : pg_fetch_all($result);
// incoLogWrite('Recargos: ' . json_encode($pedido['recargos']), false);

$pedido['lineas'] = array_map(function ($linea) {
    switch ($linea['posicion']) {
        case 1: $linea['descripcion'] .= ' POSICIÓN CUBIERTA'; break;
        case 2: $linea['descripcion'] .= ' POSICIÓN FACHADA'; break;
    }
    return $linea;
}, $pedido['lineas']);

$dir = sprintf('%sClientes/%d/ConfirmacionesPedidos/', INCO_DIR_DOCUMENTOS, $cliente['codigo']);
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}

$fileName = sprintf('CP_%s.pdf', str_pad($pedido['numero'], 6, 0, STR_PAD_LEFT));
$output = $dir . $fileName;
$pdf = new ConfirmacionPedido();
$pdf->setCliente($cliente)
    ->setPedido($pedido);
$pdf->AddContent();
// $pdf = incoPdfAddCGV($pdf);

$pdf->Output($output, 'F');

pg_close($dbconn);