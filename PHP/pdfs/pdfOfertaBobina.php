<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
    incoLogWrite('No hay argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos los diferentes proveedores
$result = pg_query_params('SELECT DISTINCT proveedor FROM ofertas_bobinaslin WHERE identificador = $1 AND estado = 0 ORDER BY proveedor', array($identificador)) or incoLogWrite('La consulta fallo [obertas_bobinas - proveedores]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[obertas_bobinas - proveedores] No hay resultado');
$proveedores = pg_fetch_all($result);

// obtenemos la oferta
$result = pg_query_params('SELECT ejercicio, numero, fecha, plazo, observaciones FROM ofertas_bobinas WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [obertas_bobinas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[obertas_bobinas] No hay resultado');
$oferta = pg_fetch_all($result)[0];

pg_prepare($dbconn, 'ofertas_bobinaslin', 'SELECT obl.linea, e.valor AS espesor, c.abreviatura AS calidad, r.abreviatura AS recubrimiento, p.nombre AS pintura, co.nombre AS color, pb.nombre AS pinturab, cob.nombre AS colorb, obl.ancho, obl.toneladas, obl.precio, obl.certificado FROM ofertas_bobinaslin obl, espesores e, calidades c, recubrimientos r, pinturas p, colores co, pinturas pb, colores cob WHERE obl.identificador = $1 AND obl.proveedor = $2 AND obl.estado = 0 AND obl.espesor = e.codigo AND obl.calidad = c.codigo AND obl.recubrimiento = r.codigo AND obl.pintura = p.codigo AND obl.ccolor = co.codigo AND obl.pinturab = pb.codigo AND obl.ccolorb = cob.codigo ORDER BY obl.linea');

pg_prepare($dbconn, 'proveedor', 'SELECT codigo, nombre, direccion, codpo, poblacion, provincia FROM proveedores WHERE codigo = $1');

class PDF extends TCPDF
{
    function Header()
    {
        /*$this->SetY(14);
        $this->Cell(0, 20, '', 1, 1); // Se cambiara por una cabecera
        $this->SetFont('dejavusans', '', 8);
        $this->Cell(0, 5, 'PEDIDOS A PROVEEDORES', 0, 1, 'C');
        $this->Ln(2);*/

        // $this->SetCellPadding(2);
        // $this->setCellPaddings('0', '0', '4', '0');

        $txt = 'Solicitud de precios';
        $this->SetFont('dejavusans', 'B', 22);
        $this->SetY(10);
        $this->Cell(0, 4, $txt, 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Ln(); // hace salto demasiado grande
        // $this->Cell(0, 4, '', 0, 1);
        $this->Cell(0, 4, 'Hoja: ' . $this->PageNo(), 0, 1, 'R');

        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
    }

    function Footer()
    {
        $this->SetY(-35);
        $this->SetLineWidth(0.51); //1px = 0.085mm
        $this->Line($this->GetX(), $this->GetY(), 197, $this->GetY());
        $this->Ln(2);
        $this->SetFont('dejavusans', 'B', 8);
        $this->setCellPaddings('0', '0', '4', '0');
        $this->Cell(130, 4, '', 0, 0, 'R');
        $this->setCellPaddings('0', '0', '0', '0');
        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->Cell(0, 20, '', 0, 1);

        $this->SetFont('dejavusans', '');
        $this->SetY($this->GetY() - 4);
        $this->Cell(0, 4, 'Página ' . $this->PageNo(), 0, 1, 'C');

        $this->SetFont('dejavusans', '', 5);
        $this->SetY($this->GetY() - 9);
        $this->Cell(0, 3, 'FCO-01-01', 0, 1, 'L');
        $this->Cell(0, 3, 'REV 1', 0, 1, 'L');
        $this->Cell(0, 3, 'FECHA: 29-05-06', 0, 1, 'L');
    }
}

for ($i = 0; $i < count($proveedores); $i++) {
    $codigoProveedor = intval($proveedores[$i]['proveedor']);

    $result = pg_execute($dbconn, 'proveedor', array($codigoProveedor));
    if (pg_num_rows($result) <= 0) continue;
    $proveedor = pg_fetch_all($result)[0];

    $result = pg_execute($dbconn, 'ofertas_bobinaslin', array($identificador, $codigoProveedor));
    if (pg_num_rows($result) <= 0) continue;
    $lineas = pg_fetch_all($result);

    $pdf = new PDF();

    $pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
    $pdf->SetCreator('GESTION');
    $pdf->SetSubject('SOLICITUD DE PRECIOS');
    $pdf->SetTitle('Pedido de bobinas X');
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.51); //1px = 0.085mm
    $pdf->SetMargins(10, 26);
    $pdf->AddPage();

    $pdf->SetFont('dejavusans', 'B');
    $txt = 'Solicitud';
    $pdf->Cell(20, 4, $txt . ' Nº', 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(30, 4, $oferta['numero'], 0, 0);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->SetX(135);
    $pdf->Cell(40, 4, 'Fecha de solicitud', 0, 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(0, 4, DateTime::createFromFormat('Y-m-d', $oferta['fecha'])->format('d-m-Y'), 0, 1);
    $pdf->Ln();
    $yProveedor = $pdf->GetY();

    $pdf->Cell(0, 16, '', 0, 1);
    $yUltimoCampo = $pdf->GetY();

    $pdf->SetY($yProveedor);
    $pdf->SetFont('dejavusans', 'B', 10);
    $pdf->Cell(0, 4, trim($proveedor['nombre']), 0, 1, 'R');
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetX(105);
    $pdf->MultiCell(0, 4, trim($proveedor['direccion']) . ' ' . trim($proveedor['codpo']) . ' ' . trim($proveedor['poblacion']) . ' ' . trim($proveedor['provincia']) . PHP_EOL, 0, 'R');
    $pdf->SetY($yUltimoCampo);

    $pdf->Ln(4);

    $txt = 'Estimado proveedor,' . PHP_EOL . PHP_EOL . 'Le solicitamos que nos facilite precio y plazo de entrega de las partidas que a continuación le detallamos.';
    $obs = trim($oferta['observaciones']);
    if (!empty($obs)) {
        $txt .= PHP_EOL . PHP_EOL . $obs;
    }
    $pdf->MultiCell(0, 4, $txt . PHP_EOL, 0, 'J');
    $pdf->Ln(8);

    $pdf->SetFont('dejavusans', 'B', 12);
    $pdf->Cell(0, 4, 'SOLICITUD DE PRECIOS', 'B', 0, 'C');
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->SetXY(5, $pdf->GetY() + 1.5);
    $pdf->Cell(0, 4, 'ATENCIÓN: Diametro Interior Bobinas = 500 mm', 0, 1, 'R');

    $pdf->SetLineWidth(0.17); //1px = 0.085mm
    $pdf->SetFont('dejavusans', 'B', 8);
    $pdf->Cell(8, 6, 'Nº', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(16, 6, 'Calidad', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(16, 6, 'Recubri.', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(41, 6, 'Revestimiento A/B', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(41, 6, 'Color A/B', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(6, 6, 'Cert.', 'B', 0, 'R', false, '', 0, false, '', 'B');
    $pdf->Cell(18, 6, 'Desarrollo', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(16, 6, 'Espesor', 'B', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(13, 6, 'TN', 'B', 0, 'R', false, '', 0, false, '', 'B');
    $pdf->Cell(15, 6, '€/TN', 'B', 1, 'R', false, '', 0, false, '', 'B');

    $pdf->SetFont('dejavusans', '', 8);
    for ($j = 0; $j < count($lineas); $j++) {
        $pintura = trim($lineas[$j]['pintura']);
        $color = trim($lineas[$j]['color']);
        $pinturab = trim($lineas[$j]['pinturab']);
        $colorb = trim($lineas[$j]['colorb']);
        $precio = '';
        $cert = '';
        if (intval($lineas[$j]['certificado']) == 0) $cert = '2.2';
        if (intval($lineas[$j]['certificado']) == 1) $cert = '3.1';

        if (strtoupper($color) == 'GALVANIZADO') {
            $pintura = $color;
            $color = 'MA';
        } else {
            $pintura .= '/' . $pinturab;
            $color .= '/' . $colorb;
        }

        // if ($pdf->GetStringWidth($pintura) >= 40 || $pdf->GetStringWidth($color) >= 40) {
            $txt = $pdf->GetStringWidth($pintura) >= $pdf->GetStringWidth($color) ? $pintura : $color;
            /*$lines = ceil($pdf->GetStringWidth($txt) / 40);
            $height = $lines * 4;*/
            $height = $pdf->getStringHeight(35, $txt, true);
            $pdf->Cell(8, $height, $lineas[$j]['linea'], 0, 0, '', false, '', 0, false, 'T', 'T');
            $pdf->Cell(16, $height, trim($lineas[$j]['calidad']), 0, 0, '', false, '', 0, false, 'T', 'T');
            $pdf->Cell(16, $height, trim($lineas[$j]['recubrimiento']), 0, 0, '', false, '', 0, false, 'T', 'T');
            $pdf->MultiCell(41, $height, $pintura, 0, 'L', false, 0);
            $pdf->MultiCell(41, $height, $color, 0, 'L', false, 0);
            $pdf->Cell(6, $height, $cert, 0, 0, 'R', false, '', 0, false, 'T', 'T');
            $pdf->Cell(18, $height, $lineas[$j]['ancho'] . ' mm', 0, 0, '', false, '', 0, false, 'T', 'T');
            $pdf->Cell(16, $height, number_format($lineas[$j]['espesor'], 2, ',', '.') . ' mm', 0, 0, '', false, '', 0, false, 'T', 'T');
            $pdf->Cell(13, $height, number_format($lineas[$j]['toneladas'], 2, ',', '.'), 0, 0, 'R', false, '', 0, false, 'T', 'T');
            $pdf->Cell(15, $height, $precio, 0, 1, 'R', false, '', 0, false, 'T', 'T');
        /*} else {
            $pdf->Cell(8, 4, $lineas[$j]['linea'], 0, 0);
            $pdf->Cell(16, 4, trim($lineas[$j]['calidad']), 0, 0);
            $pdf->Cell(16, 4, trim($lineas[$j]['recubrimiento']), 0, 0);
            $pdf->Cell(41, 4, $pintura, 0, 0);
            $pdf->Cell(41, 4, $color, 0, 0);
            $pdf->Cell(6, 4, $cert, 0, 0, 'R');
            $pdf->Cell(18, 4, $lineas[$j]['ancho'] . ' mm', 0, 0);
            $pdf->Cell(16, 4, number_format($lineas[$j]['espesor'], 2, ',', '.') . ' mm', 0, 0);
            $pdf->Cell(13, 4, number_format($lineas[$j]['toneladas'], 2, ',', '.'), 0, 0, 'R');
            $pdf->Cell(15, 4, $precio, 0, 1, 'R');
        }*/
    }

    $pdf->AddPage();
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.51); //1px = 0.085mm
    $pdf->Cell(0, 4, 'CONDICIONES DE SUMINISTRO', 'B', 1, 'C');
    $pdf->SetFontSize(7);
    $pdf->Ln(4);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, 'IDENTIFICACIÓN DEL MATERIAL', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(0, 4, '- Nombre Proveedor', 0, 1);
    $pdf->Cell(0, 4, '- Numero De Identificación De Bulto', 0, 1);
    $pdf->Cell(0, 4, '- Albaran De Entrega donde figuran', 0, 1);
    $pdf->Cell(0, 4, '- Nº Pedido y Referencia Del Proveedor', 0, 1);
    $pdf->Cell(0, 4, '- Nº Referencia Interna (Nº pedido Ingeniería y Construcción del Perfil S.A.)', 0, 1);
    $pdf->Cell(0, 4, '- Características Del Material (incluir ref. de colores Ingeniería y Construcción del Perfil S.A.)', 0, 1);
    $pdf->Cell(0, 4, '- Peso', 0, 1);
    $pdf->Cell(0, 4, '- Certificado De Calidad (Características químicas, mecánicas y recubrimiento)', 0, 1);
    $pdf->Ln(4);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, 'DIMENSIONES', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(0, 4, '- Ancho Bobina Destino Almacén A: 156, 750, 850, 1000, 1250 y 1290 mm', 0, 1);
    $pdf->Cell(0, 4, '- Ancho Bobina Destino Almacén B: 1250 y 1290 mm', 0, 1);
    $pdf->Cell(0, 4, '- Espesores Estándar 0.5, 0.6, 0.7, 0.75, 0.8, 1.0, 1.2, 1.5, 2.0 mm', 0, 1);
    $pdf->Cell(0, 4, '- Peso Máx. Bobina Destino Almacén B: de 7.000 a 11.000 Kg.', 0, 1);
    $pdf->Cell(0, 4, '- Diámetro Cilindro Interior Bobina (Mínimo 505 mm)', 0, 1);
    $pdf->Ln(4);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, 'CARACTERÍSTICAS TÉCNICAS', 0, 1);
    $ySeccion = $pdf->GetY();
    $pdf->Cell(0, 4, '- Generales (S/ NBE - MV 111 - 1980)', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 240 N/mm²', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Limite De Rotura Î (370 , 480) N/mm²', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Alargamiento de rotura min. 25%', 0, 1);
    $pdf->Ln(2);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, '- (S/ EN 10147)', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Material Base Calidad S280 GD', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Recubrimiento min. Z-225 (16µ) 225 gr/m² ambas caras', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 280 N/mm²', 0, 1);
    $pdf->Ln(2);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, '- Galvanizado (S/ EN 10142)', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Material Base Calidad DX51 - D', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Recubrimiento min. Z-275 (19,5µ) 275 gr/m² ambas caras', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 280 N/mm²', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Material Base Calidad S320 GD', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Recubrimiento min. Z-275 (19,5µ) 275 gr/m² ambas caras', 0, 1);
    $pdf->SetX($pdf->GetX() + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 320 N/mm²', 0, 1);
    $yFinIzquierda = $pdf->GetY();
    $xMedio = 105;
    $pdf->SetY($ySeccion);
    $pdf->SetX($xMedio);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, '- Galvanizado Prelacado (S/ EN 10142)', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Material Base Calidad DX51 – D', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Recubrimiento min. Z-225 (16 µ), 225 gr/m² ambas caras', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 280 N/mm2', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Material Base Calidad S320 GD', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Recubrimiento min. Z-225 (16 µ) gr/m² ambas caras', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 320 N/mm²', 0, 1);
    $pdf->Ln(2);
    $pdf->SetX($xMedio);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, '- (S/ EN 10147)', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Material Base Calidad S280 GD', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Recubrimiento min. Z-275 (19,5µ) 275 gr/m2 ambas caras', 0, 1);
    $pdf->SetX($xMedio + 3);
    $pdf->Cell(0, 4, 'Limite Elástico > 280 N/mm²', 0, 1);
    $pdf->Ln(2);
    $pdf->SetX($xMedio);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, '- Recubrimiento Orgánico', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->SetY($yFinIzquierda);
    $pdf->Ln(4);

    $pdf->SetFillColor(233, 233, 233);
    $pdf->SetLineWidth(0.17);
    $pdf->Cell(69, 4, '', 'LRT', 0, '', true);
    $pdf->Cell(23, 4, 'SIMBOLO', 'LRT', 0, 'C', true);
    $pdf->Cell(46, 4, 'ANVERSO (1)', 1, 0, 'C', true);
    $pdf->Cell(46, 4, 'REVERSO (2)', 1, 1, 'C', true);
    $pdf->Cell(69, 4, '', 'LR', 0, '', true);
    $pdf->Cell(23, 4, '', 'LR', 0, '', true);
    $pdf->Cell(46, 4, '', 'LRT', 0, '', true);
    $pdf->Cell(46, 4, '', 'LRT', 1, '', true);
    $pdf->Cell(69, 4, '', 'LR', 0, '', true);
    $pdf->Cell(23, 4, 'ISO/DIS 1043-1', 'LR', 0, 'C', true);
    $pdf->Cell(46, 4, 'Espesor Mínimo µm', 'LR', 0, 'C', true);
    $pdf->Cell(46, 4, 'Espesor Mínimo µm', 'LR', 1, 'C', true);
    $pdf->Cell(69, 4, 'RECUBRIMIENTO ORGANICO', 'LRB', 0, 'C', true);
    $pdf->Cell(23, 4, '', 'LRB', 0, '', true);
    $pdf->Cell(23, 4, 'Imprimación', 1, 0, 'C', true);
    $pdf->Cell(23, 4, 'Lacado', 1, 0, 'C', true);
    $pdf->Cell(23, 4, 'Imprimación', 1, 0, 'C', true);
    $pdf->Cell(23, 4, 'Lacado', 1, 1, 'C', true);
    $pdf->Cell(69, 4, 'Poliéster (1)', 'LRB', 0, 'L');
    $pdf->Cell(23, 4, 'SP', 'LRB', 0, 'C');
    $pdf->Cell(23, 4, '5', 1, 0, 'C');
    $pdf->Cell(23, 4, '20', 1, 0, 'C');
    $pdf->Cell(23, 4, '05- oct', 1, 0, 'C');
    $pdf->Cell(23, 4, '', 1, 1, 'C');
    $pdf->Cell(69, 4, 'Poliéster Alta Durabilidad (1)', 'LRB', 0, 'L');
    $pdf->Cell(23, 4, 'SP', 'LRB', 0, 'C');
    $pdf->Cell(23, 4, '5', 1, 0, 'C');
    $pdf->Cell(23, 4, '30', 1, 0, 'C');
    $pdf->Cell(23, 4, '05- oct', 1, 0, 'C');
    $pdf->Cell(23, 4, '', 1, 1, 'C');
    $pdf->Cell(69, 4, 'Polifluoruro de Vinildeno (1)', 'LRB', 0, 'L');
    $pdf->Cell(23, 4, 'PVDF', 'LRB', 0, 'C');
    $pdf->Cell(23, 4, '5', 1, 0, 'C');
    $pdf->Cell(23, 4, '20', 1, 0, 'C');
    $pdf->Cell(23, 4, '05- oct', 1, 0, 'C');
    $pdf->Cell(23, 4, '', 1, 1, 'C');
    $pdf->Cell(69, 4, 'Plastisol de Policloruro de Vinilo (1)', 'LRB', 0, 'L');
    $pdf->Cell(23, 4, 'PVC(P)', 'LRB', 0, 'C');
    $pdf->Cell(23, 4, '5', 1, 0, 'C');
    $pdf->Cell(23, 4, '100-200', 1, 0, 'C');
    $pdf->Cell(23, 4, '05- oct', 1, 0, 'C');
    $pdf->Cell(23, 4, '', 1, 1, 'C');

    $pdf->SetFontSize(5);
    $pdf->Cell(0, 3, '(1) Adminte prelacado a dos caras', 0, 1, 'L');
    $pdf->Cell(0, 3, '(2) No se tiene en cuenta la capa de protección temporal', 0, 1, 'L');

    $dir = INCO_DIR_PROVEEDORES . $proveedor['codigo'] . '/Ofertas/';
    if (!is_dir($dir)) {
        $oldmask = umask(0);
        mkdir($dir, 0755, true);
        umask($oldmask);
    }
    $fileName = 'SP_' . str_pad($oferta['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
    $output = $dir . $fileName;
    $pdf->Output($output, 'F');
}

// pg_free_result($result);
pg_close($dbconn);
