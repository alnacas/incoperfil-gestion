<?php

class NCsPdf extends TCPDF
{

    const ESTADO = [0 => 'Pendiente', 1 => 'Cerrada', 2 => 'Anulada'],
        RIESGO = [0 => '', 5 => 'Aceptable - Asumir el riesgo', 10 => 'Tolerable - Desatender pero monitorizar', 15 => 'Moredado - Considerar', 20 => 'Moredado - Considerar',
            30 => 'Importante - Planificar respuesta', 40 => 'Importante - Planificar respuesta', 60 => 'Intolerable - Respuesta inmediata'];

    /**
     * @var float
     */
    private $wAvailable;

    /**
     * @var array
     */
    private $ncs;

    /**
     * @var boolean
     */
    private $show_ac_ap;

    /**
     * @param array $ncs
     * @param bool $show_ac_ap
     * @param string $orientation
     * @param string $unit
     * @param string $format
     * @param bool $unicode
     * @param string $encoding
     * @param false $diskcache
     * @param false $pdfa
     */
    public function __construct(array $ncs, bool $show_ac_ap, $orientation = 'L', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {
        $this->ncs = $ncs;
        $this->show_ac_ap = $show_ac_ap;

        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);

        $this->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
        $this->SetCreator('GESTION');
        $this->SetFont('dejavusans', '', 8);
        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->SetMargins(5, 20); // 70
        $this->SetAutoPageBreak(true, 5);

        $this->wAvailable = $this->w - $this->lMargin - $this->rMargin;
    }

    public function Header() {
        $this->Ln(6);
        $y_ini = $this->y;
        $this->SetFillColor(199, 217, 240);

        $this->SetFont('dejavusans', 'b', 12);
        $this->Cell($this->wCol(11), 6, $this->header_title, 0, 1, '', true);
        $this->SetFont('dejavusans', '', 10);
        $this->Cell($this->wCol(11), 4, $this->header_string, 0, 0, '', true);

        $this->SetY($y_ini);
        $w_offset = $this->wCol(11);
        $this->SetFontSize(7);
        $this->x += $w_offset;
        $this->Cell($this->wCol(1), 3.5, 'FCA-03-02', 0, 1, 'R', true);
        $this->x += $w_offset;
        $this->Cell($this->wCol(1), 3.5, 'rev.3', 0, 1, 'R', true);
        $this->x += $w_offset;
        $this->Cell($this->wCol(1), 3.5, date('d/m/Y'), 0, 1, 'R', true);
    }

    public function Footer() {
    }

    public function AddContent() {
        $this->AddPage();

        $this->SetFillColor(242);

        $border = 0;

        $nc_counter = 0;
        foreach ($this->ncs as $nc) {
            $fill = false; // $nc_counter % 2 == 0;
            // row 1
            $wAux = $this->wCol(4) * 0.2;
            $height = 4;
            $this->SetTextBlue();
            $this->Cell($wAux * 0.5, $height, 'Nº', $border, 0, '', $fill);
            $this->Cell($wAux * 0.5, $height, 'Tipo', $border, 0, '', $fill);
            $this->Cell($wAux, $height, 'Area', $border, 0, '', $fill);
            $this->Cell($wAux, $height, 'Fecha', $border, 0, '', $fill);
            $this->Cell($wAux * 2, $height, 'Emisor', $border, 0, '', $fill);
            $this->Cell($this->wCol(4), $height, 'Descripción', $border, 0, '', $fill);
            $this->Cell($this->wCol(4), $height, 'Correción', $border, 1, '', $fill);
            $height = max([4,
                    $this->getStringHeight($wAux * 2, ($nc['emisor']['nombre'] ?? '')),
                    $this->getStringHeight($this->wCol(4), $nc['descripcion']),
                    $this->getStringHeight($this->wCol(4), $nc['correccion'])
                ]) + 0.5;
            $this->SetTextBlack();
            $this->MultiCell($wAux * 0.5, $height, $nc['codigo'], $border, 'L', $fill, 0);
            $this->MultiCell($wAux * 0.5, $height, ($nc['tipo']['nombre'] ?? ''), $border, 'L', $fill, 0);
            $this->MultiCell($wAux, $height, ($nc['area']['nombre'] ?? ''), $border, 'L', $fill, 0);
            $this->MultiCell($wAux, $height, $this->translateDate($nc['fecha']), $border, 'L', $fill, 0);
            $this->MultiCell($wAux * 2, $height, ($nc['emisor']['nombre'] ?? ''), $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(4), $height, $nc['descripcion'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(4), $height, $nc['correccion'], $border, 'L', $fill, 1);

            // row 2
            $height = 4;
            $this->SetTextBlue();
            $this->Cell($this->wCol(8), $height, 'Causa', $border, 0, '', $fill);
            $this->Cell($this->wCol(2), $height, 'Disposición', $border, 0, '', $fill);
            $this->Cell($this->wCol(1) * 0.3, $height, 'P', $border, 0, '', $fill);
            $this->Cell($this->wCol(1) * 0.3, $height, 'I', $border, 0, '', $fill);
            $this->Cell($this->wCol(1) * 1.4, $height, 'Riesgo', $border, 1, '', $fill);
            $riesgo = $nc['riesgo'] == 0 ? '' : $nc['riesgo'] . ' - ' . self::RIESGO[$nc['riesgo']];
            $height = max([4,
                    $this->getStringHeight($this->wCol(8), $nc['causa']),
                    $this->getStringHeight($this->wCol(2), $nc['disposicion']),
                    $this->getStringHeight($this->wCol(1) * 1.4, $riesgo)
                ]) + 0.5;
            $this->SetTextBlack();
            $this->MultiCell($this->wCol(8), $height, $nc['causa'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(2), $height, $nc['disposicion'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(1) * 0.3, $height, $nc['probabilidad'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(1) * 0.3, $height, $nc['impacto'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(1) * 1.4, $height, $riesgo, $border, 'L', $fill, 1);

            // row 3
            $height = 4;
            $this->SetTextBlue();
            $this->Cell($this->wCol(1) * 0.6, $height, 'Estado:', $border, 0, '', $fill);
            $this->SetTextBlack();
            $this->Cell($this->wCol(1) * 1.4, $height, self::ESTADO[$nc['estado']], $border, 0, '', $fill);
            $checks = ['ac' => 'AC', 'ap' => 'AP'];
            foreach ($checks as $prop => $label) {
                $this->SetTextBlack();
                $this->CheckBox($prop . '_' . $nc['codigo'], 5, $nc[$prop] == 1, ['readonly' => true], ['fill' => $fill]);
                $this->SetTextBlue();
                $this->Cell($this->GetStringWidth($label) + 5, $height, $label, $border, 0, '', $fill);
            }
            $this->Cell(0, $height, '', $border, 1, 1, $fill);

            if (!$this->show_ac_ap) {
                $this->Ln(2);
                $this->Line($this->x, $this->y, $this->w - $this->rMargin, $this->y);
                $this->Ln(2);

                $nc_counter++;
                continue;
            }

            // row 4
            $wAux = $this->wCol(4) * 0.25;
            $height = 4;
            $this->SetTextBlue();
            $this->Cell($this->wCol(4), $height, 'Plan Acción Correctiva / Preventiva', $border, 0, '', $fill);
            $this->Cell($wAux * 1.4, $height, 'Proceso', $border, 0, '', $fill);
            $this->Cell($wAux * 1.4, $height, 'Implantación', $border, 0, '', $fill);
            $this->Cell($wAux * 0.6, $height, 'Horas', $border, 0, '', $fill);
            $this->Cell($wAux * 0.6, $height, 'Coste', $border, 0, '', $fill);
            $this->Cell($this->wCol(4), $height, 'Resultados', $border, 1, '', $fill);
            $y = $this->y;
            $height = max([4,
                    $this->getStringHeight($this->wCol(4), $nc['accion_correctora']),
                    $this->getStringHeight($this->wCol(4), $nc['resultado'])
                ]) + 0.5;
            $this->SetTextBlack();
            $this->MultiCell($this->wCol(4), $height, $nc['accion_correctora'], $border, 'L', $fill, 0);
            $this->MultiCell($wAux * 1.4, $height, ($nc['proceso']['nombre'] ?? ''), $border, 'L', $fill, 0);
            $this->MultiCell($wAux * 1.4, $height, $this->translateDate($nc['fecha_implantacion']), $border, 'L', $fill, 0);
            $this->MultiCell($wAux * 0.6, $height, $nc['recursos_horas'], $border, 'L', $fill, 0);
            $this->MultiCell($wAux * 0.6, $height, $nc['recursos_importe'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(4), $height, $nc['resultado'], $border, 'L', $fill, 1);

            $this->y = $y + max([4, $this->getStringHeight($wAux * 1.4, ($nc['proceso']['nombre'] ?? ''))]);
            $this->x += $this->wCol(4);
            $this->SetTextBlue();
            $this->Cell($this->wCol(2), 4, 'Responsable', $border, 0, '', $fill);
            $this->Cell($this->wCol(2), 4, 'Cierre', $border, 1, '', $fill);
            $this->x += $this->wCol(4);
            $this->SetTextBlack();
            $this->MultiCell($this->wCol(2), 4, $nc['responsable'], $border, 'L', $fill, 0);
            $this->MultiCell($this->wCol(2), 4, $this->translateDate($nc['fecha_cierre']), $border, 'L', $fill, 1);

            if ($y + $height > $this->y) {
                $this->y = $y + $height;
            }

            $this->Ln(2);
            $this->Line($this->x, $this->y, $this->w - $this->rMargin, $this->y);
            $this->Ln(2);

            $nc_counter++;
        }
    }

    private function wCol($cols) {
        return $this->wAvailable * ($cols / 12);
    }

    /**
     * @param string|null $str_date
     * @return string
     */
    private function translateDate($str_date) {
        if (is_null($str_date) || empty($str_date)) return '';
        $date = DateTime::createFromFormat('Y-m-d', $str_date);
        if ($date === false) return '';
        return $date->format('d/m/Y');
    }

    private function SetTextBlack() {
        $this->SetTextColor();
    }

    private function SetTextBlue() {
        $this->SetTextColor(130, 170, 220);
    }

}