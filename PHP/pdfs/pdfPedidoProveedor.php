<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10/12/2018
 * Time: 18:10
 */

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}

$firmar = boolval($argv[3]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos el pedido
$result = pg_query_params($dbconn, 'SELECT numero, proveedor, fecha, observaciones_proveedor FROM pedidos_proveedor WHERE identificador = $1', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_proveedor]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_proveedor] No hay resultado');
$pedido = pg_fetch_all($result)[0];

// obtenemos las lineas del pedido
$result = pg_query_params($dbconn, 'SELECT pl.linea, pl.seccion, p.seccion_otros, pl.descripcion_comercial, pl.unidad_medida, pl.unidades, pl.longitud, pl.precio, pl.cantidad_total, p.descripcion_libre FROM pedidos_proveedorlin pl, productos p WHERE identificador = $1 AND pl.producto = p.codigo ORDER BY seccion, p.seccion_otros, linea', array($identificador)) or incoLogWrite('La consulta fallo [pedidos_proveedorlin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_proveedorlin] No hay resultado');
$pedidolin = pg_fetch_all($result);

$result = pg_query_params($dbconn, 'SELECT codigo, nombre FROM secciones_otros WHERE estado = 1 ORDER BY nombre', array()) or incoLogWrite('La consulta fallo [secciones_otros]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[secciones_otros] No hay resultado');
$seccionesOtros = pg_fetch_all($result);
array_push($seccionesOtros, array('codigo' => null, 'nombre' => 'VARIOS'));

$translucidos = array_filter($pedidolin, function ($linea) {
    return $linea['seccion'] == 3;
});

$otros = array_filter($pedidolin, function ($linea) {
    return $linea['seccion'] == 8;
});

$seccionesOtrosLns = array();
foreach ($seccionesOtros as $seccionOtro) {
    $seccionesOtrosLns[$seccionOtro['codigo']] = array_filter($otros, function ($linea) use ($seccionOtro) {
        return $linea['seccion_otros'] == $seccionOtro['codigo'];
    });
}

// obtenemos el proveedor
$result = pg_query_params('SELECT codigo, nombre, direccion, codpo, poblacion, provincia, fopago FROM proveedores WHERE codigo = $1', array($pedido['proveedor'])) or incoLogWrite('La consulta fallo [proveedores]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[proveedores] No hay resultado');
$proveedor = pg_fetch_all($result)[0];

$result = pg_query_params('SELECT nombre FROM fopago WHERE codigo = $1', array($proveedor['fopago'])) or incoLogWrite('La consulta fallo [fopago]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[fopago] No hay resultado');
$fopago = pg_fetch_all($result)[0]['nombre'];

pg_prepare($dbconn, 'unidad_medida', 'SELECT abreviatura FROM unidades_medida WHERE codigo = $1');

class PDF extends TCPDF
{
    function Header()
    {
        $txt = 'Pedido a proveedor';
        $this->SetFont('dejavusans', 'B', 22);
        $this->SetY(10);
        $this->Cell(0, 4, $txt, 0, 1, 'R');
        $this->SetFont('dejavusans', '', 8);
        // $this->Ln(); // hace salto demasiado grande
        // $this->Cell(0, 4, '', 0, 1);
        $this->Cell(0, 4, 'Hoja: ' . $this->PageNo(), 0, 1, 'R');

        $this->Image(INCO_DIR_IMAGENES . 'logo.jpg', $this->GetX(), 10, 95, 0, 'jpg', '', 'C', false, 600);
    }

    function Footer()
    {
        global $firmar;

        $this->SetY(-35);

        if ($firmar) {
            $this->Image(INCO_DIR_IMAGENES . 'valpedidobobina.jpg', 158, $this->GetY(), 0, 28, 'jpg', '', 'C', false, 600);
        }

        $this->SetLineWidth(0.51); //1px = 0.085mm
        $this->Line($this->GetX(), $this->GetY(), 197, $this->GetY());
        $this->Ln(2);
        $this->SetFont('dejavusans', 'B', 8);
        $this->setCellPaddings('0', '0', '4', '0');
        $this->Cell(130, 4, 'Revisado por:', 0, 0, 'R');
        $this->setCellPaddings('0', '0', '0', '0');
        $this->SetLineWidth(0.17); //1px = 0.085mm
        $this->Cell(0, 20, '', 1, 1);

        $this->SetFont('dejavusans', '');
        $this->SetY($this->GetY() - 4);
        $this->Cell(0, 4, 'Página ' . $this->PageNo(), 0, 1, 'C');

        $this->SetFont('dejavusans', '', 5);
        $this->SetY($this->GetY() - 9);
        $this->Cell(0, 3, 'FCO-01-01', 0, 1, 'L');
        $this->Cell(0, 3, 'REV 1', 0, 1, 'L');
        $this->Cell(0, 3, 'FECHA: 29-05-06', 0, 1, 'L');
    }
}

$pdf = new PDF();

$pdf->SetAuthor('INCOPERFIL (Ingeniería y Construcción del Perfil , S.A.)');
$pdf->SetCreator('GESTION');
$pdf->SetSubject('PEDIDO A PROVEEDOR');
$pdf->SetTitle('Pedido a proveedor ' . $pedido['numero']);
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->SetMargins(10, 26);
$pdf->AddPage();

$pdf->SetFont('dejavusans', 'B');
$txt = 'Pedido';
$pdf->Cell(20, 4, $txt . ' Nº', 0, 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(30, 4, $pedido['numero'], 0, 0);
$pdf->SetFont('dejavusans', 'B');
$pdf->SetX(135);
$pdf->Cell(40, 4, 'Fecha de pedido', 0, 0);
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, DateTime::createFromFormat('Y-m-d', $pedido['fecha'])->format('d-m-Y'), 0, 1);
$pdf->Ln();
$yProveedor = $pdf->GetY();

$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Proveedor', 0, 0, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->Cell(10, 4, $proveedor['codigo'], 0, 1, 'L');
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Descarga de Mercancía', 0, 0, 'L');
$pdf->SetFont('dejavusans', '');
$descargar = 'NAVE A (C/NOU, 16)';
if (!empty($translucidos)) $descargar = 'NAVE B (C/NOU, 27)';
$pdf->Cell(0, 4, $descargar, 0, 1);
$pdf->SetFont('dejavusans', 'B');
$pdf->Cell(40, 4, 'Plazo de Entrega', 0, 0, 'L');
$pdf->SetFont('dejavusans', '');
/*if (count($semanas) == 1) {
    $pdf->Cell(0, 4, trim($pedido['plazo']), 0, 1);
} else {
    $pdf->Cell(0, 4, 'VER COMENTARIOS', 0, 1);
}*/
$yUltimoCampo = $pdf->GetY();

$pdf->SetY($yProveedor);
$pdf->SetFont('dejavusans', 'B', 10);
$pdf->Cell(0, 4, trim($proveedor['nombre']), 0, 1, 'R');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetX(105);
$pdf->MultiCell(0, 4, trim($proveedor['direccion']) . ' ' . trim($proveedor['codpo']) . ' ' . trim($proveedor['poblacion']) . ' ' . trim($proveedor['provincia']) . PHP_EOL, 0, 'R');
$pdf->SetY($yUltimoCampo);

if (!empty($translucidos)) {
    $pdf->Ln(8);
    $pdf->SetFont('dejavusans', 'B', 12);
    $pdf->Cell(0, 4, 'TRANSLUCIDOS', 'B', 0, 'C');
    $pdf->SetFont('dejavusans', '', 7);
    $pdf->SetXY(5, $pdf->GetY() + 1.5);
    $pdf->Ln(4);

    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.17); //1px = 0.085mm
    $pdf->SetFont('dejavusans', 'B', 8);
    $pdf->Cell(6, 6, 'Id', 'BT', 0, '', false, '', 0, false, '', 'B');
    $pdf->Cell(94, 6, 'Descripción', 'TB', 0, 'L', false, '', 0, false, '', 'B');
    $pdf->Cell(15, 6, 'Uds', 'TB', 0, 'R', false, 'R', 0, false, '', 'B');
    $pdf->Cell(20, 6, 'Long (mm)', 'TB', 0, 'R', false, '', 0, false, '', 'B');
    $pdf->Cell(20, 6, 'Total m' . html_entity_decode('&sup2;'), 'TB', 0, 'R', false, '', 0, false, '', 'B');
    $pdf->Cell(15, 6, 'Ref.', 'TB', 0, 'R', false, '', 0, false, '', 'B');
    $pdf->Cell(20, 6, '€/m' . html_entity_decode('&sup2;'), 'TB', 1, 'R', false, '', 0, false, '', 'B');
    $pdf->SetFont('dejavusans', '');
    $totalM2 = 0;

    foreach ($translucidos as $translucido) {
        $linea = intval($translucido['linea']);
        $descripcion = trim($translucido['descripcion_comercial']);
        $uds = intval($translucido['unidades']);
        $longitud = intval($translucido['longitud']);
        $total = floatval($translucido['cantidad_total']);
        $totalM2 += $total;
        $totalTxt = number_format($total, 2, ',', '.');
        $ref = '';
        $precio = floatval($translucido['precio']);
        $precioTxt = number_format($precio, 2, ',', '.');

        $height = ceil($pdf->getStringHeight(94, $descripcion));

        $pdf->MultiCell(6, $height, $linea, 0, 'L', false, 0);
        $pdf->MultiCell(94, $height, $descripcion . PHP_EOL, 0, 'J', false, 0);
        $pdf->MultiCell(15, $height, $uds, 0, 'R', false, 0);
        $pdf->MultiCell(20, $height, $longitud, 0, 'R', false, 0);
        $pdf->MultiCell(20, $height, $totalTxt, 0, 'R', false, 0);
        $pdf->MultiCell(15, $height, $ref, 0, 'R', false, 0);
        $pdf->MultiCell(20, $height, $precioTxt, 0, 'R', false, 1);
    }

    $pdf->Ln();
    $pdf->SetFont('dejavusans', 'B', 8);
    $pdf->Cell(120, 6, '', 0, 0);
    $pdf->Cell(10, 6, 'Total:', 'LTB', 0);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(35, 6, number_format($totalM2, 2, ',', '.') . ' m' . html_entity_decode('&sup2;'), 'RTB', 1, 'R');
}

if (!empty($otros)) {
    foreach ($seccionesOtros as $seccionesOtro) {
        if (empty($seccionesOtrosLns[$seccionesOtro['codigo']])) {
            continue;
        }

        $pdf->Ln(8);
        $pdf->SetFont('dejavusans', 'B', 12);
        $pdf->Cell(0, 4, trim($seccionesOtro['nombre']), 'B', 0, 'C');
        $pdf->SetFont('dejavusans', '', 7);
        $pdf->SetXY(5, $pdf->GetY() + 1.5);
        $pdf->Ln(4);

        $pdf->SetLineWidth(0.17); //1px = 0.085mm
        $pdf->SetFont('dejavusans', 'B', 8);
        $pdf->Cell(6, 6, 'Id', 'TB', 0, '', false, '', 0, false, '', 'B');
        $pdf->Cell(94, 6, 'Descripción', 'TB', 0, 'L', false, '', 0, false, '', 'B');
        $pdf->Cell(15, 6, 'Uds', 'TB', 0, 'R', false, 'R', 0, false, '', 'B');
        $pdf->Cell(20, 6, 'Long (m)', 'TB', 0, 'R', false, '', 0, false, '', 'B');
        $pdf->Cell(20, 6, 'Total', 'TB', 0, 'R', false, '', 0, false, '', 'B');
        $pdf->Cell(15, 6, 'UM', 'TB', 0, 'R', false, '', 0, false, '', 'B');
        $pdf->Cell(20, 6, '€/UM', 'TB', 1, 'R', false, '', 0, false, '', 'B');
        $pdf->SetFont('dejavusans', '');

        foreach ($seccionesOtrosLns[$seccionesOtro['codigo']] as $otroLn) {
            $linea = intval($otroLn['linea']);
            $descripcion = trim($otroLn['descripcion_comercial']);
            $uds = intval($otroLn['unidades']);
            $longitud = floatval($otroLn['longitud']);
            if ($longitud == 0) $longitud = '';
            else $longitud /= 1000;
            if ($longitud > 0) $longitud = number_format($longitud, 2, ',', '.');
            $total = floatval($otroLn['cantidad_total']);
            $totalTxt = number_format($total, 2, ',', '.');
            $ref = '';
            $precio = floatval($otroLn['precio']);
            $precioTxt = number_format($precio, 2, ',', '.');
            $um = intval($otroLn['unidad_medida']);
            $result = pg_execute($dbconn, 'unidad_medida', array($um));
            if (pg_num_rows($result) <= 0) incoLogWrite('[unidades_medida] No hay resultado');
            $um = trim(pg_fetch_all($result)[0]['abreviatura']);
            $um = str_replace('2', html_entity_decode('&sup2;'), $um);

            $height = ceil($pdf->getStringHeight(94, $descripcion));

            $pdf->MultiCell(6, $height, $linea, 0, 'L', false, 0);
            $pdf->MultiCell(94, $height, $descripcion . PHP_EOL, 0, 'J', false, 0);
            $pdf->MultiCell(15, $height, $uds, 0, 'R', false, 0);
            $pdf->MultiCell(20, $height, $longitud, 0, 'R', false, 0);
            $pdf->MultiCell(20, $height, $totalTxt, 0, 'R', false, 0);
            $pdf->MultiCell(15, $height, $um, 0, 'R', false, 0);
            $pdf->MultiCell(20, $height, $precioTxt, 0, 'R', false, 1);
        }
    }
}

$pdf->Ln(8);
$pdf->SetLineWidth(0.51); //1px = 0.085mm
$pdf->Cell(0, 4, 'CONDICIONES GENERALES', 'B', 1, 'C');
$pdf->Ln(2);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Entrega de Materiales', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Los materiales deben llevar su correspondiente albarán de entrega, número de pedido y certificado de calidad.' . PHP_EOL);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Facturación', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->MultiCell(0, 4, 'Sus facturas se deberán recibir en nuestras oficinas como máximo a los 10 días de su fecha de emisión acompañadas con una copia de los albaranes y certificados de calidad.' . PHP_EOL);
$pdf->Ln(2);
$pdf->SetFont('dejavusans', 'B', 7);
$pdf->Cell(0, 4, 'Formas de Pago', 0, 1, 'L');
$pdf->SetFont('dejavusans', '');
$pdf->Cell(0, 4, $fopago, 0, 1, 'L');
$pdf->Ln(12);

$obs = trim($pedido['observaciones_proveedor']);
if (!empty($obs)) {
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.51); //1px = 0.085mm
    $pdf->Cell(0, 4, 'COMENTARIOS', 'B', 1, 'C');
    $pdf->SetFontSize(7);
    $pdf->Ln(2);
    $pdf->MultiCell(0, 4, $obs . PHP_EOL);
    $pdf->Ln(4);
}

// TRANSLUCIDOS
if (!empty($translucidos)) {
    $pdf->AddPage();
    $pdf->SetFont('dejavusans', '', 8);
    $pdf->SetLineWidth(0.51); //1px = 0.085mm
    $pdf->Cell(0, 4, 'CONDICIONES DE SUMINISTRO DE TRANSLUCIDOS', 'B', 1, 'C');
    $pdf->SetFontSize(7);
    $pdf->Ln(4);

    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, 'IDENTIFICACIÓN DEL MATERIAL', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(0, 4, '- Nombre Proveedor', 0, 1);
    $pdf->Cell(0, 4, '- Numero De Identificación De Bulto', 0, 1);
    $pdf->Cell(0, 4, '- Albaran De Entrega donde figuran', 0, 1);
    $pdf->Cell(0, 4, '- Nº Pedido y Referencia Del Proveedor', 0, 1);
    $pdf->Cell(0, 4, '- Nº Referencia Interna (Nº pedido Ingeniería y Construcción del Perfil S.A.)', 0, 1);
    $pdf->Cell(0, 4, '- Características Del Material', 0, 1);
    $pdf->Cell(0, 4, '- Peso', 0, 1);
    $pdf->Ln(4);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, 'DIMENSIONES', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(0, 4, '- Espesores Estándar 1 mm', 0, 1);
    $pdf->Cell(0, 4, '- Ancho 1050 y 1100 mm', 0, 1);
    $pdf->Cell(0, 4, '- Colores: Blanco Opal / Blanco Cristal', 0, 1);
    $pdf->Ln(4);
    $pdf->SetFont('dejavusans', 'B');
    $pdf->Cell(0, 4, 'NORMATIVA DE REFERENCIA', 0, 1);
    $pdf->SetFont('dejavusans', '');
    $pdf->Cell(0, 4, '- (S/EN 1013-2)', 0, 1);
}

if (!empty($otros)) {
    foreach ($seccionesOtros as $seccionesOtro) {
        if (empty($seccionesOtrosLns[$seccionesOtro['codigo']])) {
            continue;
        }

        if ($seccionesOtro['codigo'] == 1) {
            // OTROS: AISLAMIENTO
            $pdf->AddPage();
            $pdf->SetFont('dejavusans', '', 8);
            $pdf->SetLineWidth(0.51); //1px = 0.085mm
            $pdf->Cell(0, 4, 'CONDICIONES DE SUMINISTRO DE ' . trim($seccionesOtro['nombre']), 'B', 1, 'C');
            $pdf->SetFontSize(7);
            $pdf->Ln(4);

            $pdf->SetFont('dejavusans', 'B');
            $pdf->Cell(0, 4, 'IDENTIFICACIÓN DEL MATERIAL', 0, 1);
            $pdf->SetFont('dejavusans', '');
            $pdf->Cell(0, 4, '- Nombre Proveedor', 0, 1);
            $pdf->Cell(0, 4, '- Numero De Identificación De Bulto', 0, 1);
            $pdf->Cell(0, 4, '- Albaran De Entrega donde figuran', 0, 1);
            $pdf->Cell(0, 4, '- Nº Pedido y Referencia Del Proveedor', 0, 1);
            $pdf->Cell(0, 4, '- Nº Referencia Interna (Nº pedido Ingeniería y Construcción del Perfil S.A.)', 0, 1);
            $pdf->Cell(0, 4, '- Características Del Material', 0, 1);
            $pdf->Cell(0, 4, '- Peso', 0, 1);
            $pdf->Ln(4);
            $pdf->SetFont('dejavusans', 'B');
            $pdf->Cell(0, 4, 'DIMENSIONES', 0, 1);
            $pdf->SetFont('dejavusans', '');
            $pdf->Cell(0, 4, '- Espesores Estándar 80 mm', 0, 1);
            $pdf->Cell(0, 4, '- Ancho 1200 mm', 0, 1);
            $pdf->Cell(0, 4, '- Largo 6000 mm', 0, 1);
        }

        if ($seccionesOtro['codigo'] == -99) {
            // OTROS: JUNTA ESTANCA
            $pdf->SetFont('dejavusans', 'B');
            $pdf->Cell(0, 4, 'IDENTIFICACIÓN DEL MATERIAL', 0, 1);
            $pdf->SetFont('dejavusans', '');
            $pdf->Cell(0, 4, '- Nombre Proveedor', 0, 1);
            $pdf->Cell(0, 4, '- Numero De Identificación De Bulto', 0, 1);
            $pdf->Cell(0, 4, '- Albaran De Entrega donde figuran', 0, 1);
            $pdf->Cell(0, 4, '- Nº Pedido y Referencia Del Proveedor', 0, 1);
            $pdf->Cell(0, 4, '- Nº Referencia Interna (Nº pedido Ingeniería y Construcción del Perfil S.A.)', 0, 1);
            $pdf->Cell(0, 4, '- Características Del Material', 0, 1);
            $pdf->Cell(0, 4, '- Nº Unidades o Nº de Paquetes', 0, 1);
            $pdf->Ln(4);
            $pdf->SetFont('dejavusans', 'B');
            $pdf->Cell(0, 4, 'DIMENSIONES', 0, 1);
            $pdf->SetFont('dejavusans', '');
            $pdf->Cell(0, 4, '- Perfil 30 superior e inferior', 0, 1);
            $pdf->Cell(0, 4, '- Perfil 44 superior e inferior', 0, 1);
            $pdf->Cell(0, 4, '- Perfil Ondulado', 0, 1);
            $pdf->Cell(0, 4, '- Pquetes de 125 unidades', 0, 1);
        }
    }
}


$dir = INCO_DIR_PROVEEDORES . $proveedor['codigo'] . '/Pedidos/';
if (!is_dir($dir)) {
    $oldmask = umask(0);
    mkdir($dir, 0755, true);
    umask($oldmask);
}
$fileName = 'P_' . str_pad($pedido['numero'], 6, '0', STR_PAD_LEFT) . '.pdf';
// $fileName = 'PedidoBobinas.pdf';
$output = $dir . $fileName;
$pdf->Output($output, 'F');

// pg_free_result($result);
pg_close($dbconn);