<?php
/**
 * Created by PhpStorm.
 * User: alnacas
 * Date: 21/9/17
 * Time: 8:50
 */

date_default_timezone_set('Europe/Madrid');
ini_set('error_log', 1);

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/i18n/IncoTranslatorFix.php';

$dotenv = Dotenv\Dotenv::createMutable(__DIR__);
$dotenv->load();
$dotenv->required(['APP_ENV'])->notEmpty()->allowedValues(['dev', 'prod']);
if (!file_exists(__DIR__ . '/.env.' . $_ENV['APP_ENV'])) {
    exit(sprintf('No existe el fichero del entorno de "%s"', $_ENV['APP_ENV']));
}
$dotenv = Dotenv\Dotenv::createMutable(__DIR__, '.env.' . $_ENV['APP_ENV']);
$dotenv->load();
$dotenv->required(['PROJECT_DIR'])->notEmpty();
$dotenv->required(['DB_LOCAL_HOST', 'DB_LOCAL_DBNAME', 'DB_LOCAL_USER', 'DB_LOCAL_PASSWORD'])->notEmpty();
$dotenv->required(['DB_REMOTE_HOST', 'DB_REMOTE_DBNAME', 'DB_REMOTE_USER', 'DB_REMOTE_PASSWORD'])->notEmpty();
$dotenv->required(['IMAP_HOST', 'IMAP_SENT_BOX', 'SMTP_HOST'])->notEmpty();
$dotenv->required(['SMTP_PORT'])->isInteger();
$dotenv->required(['EMAIL_INFO', 'EMAIL_NOREPLY', 'EMAIL_NOREPLY_PASSWD'])->notEmpty();
$dotenv->required(['EMAIL_COMPRAS_ID'])->isInteger();
$dotenv->required(['EMAIL_MESSAGE_NOMBRE', 'EMAIL_MESSAGE_MOVIL', 'EMAIL_MESSAGE_LANG', 'EMAIL_MESSAGE_CHARSET', 'EMAIL_MESSAGE_IMAGE'])->notEmpty();

define('INCO_ENV', $_ENV['APP_ENV']);
const INCO_DEV_MODE = INCO_ENV == 'dev';

define('INCO_DIR', $_ENV['PROJECT_DIR']);
define('INCO_DIR_DOCUMENTOS', sprintf('%s/Documentos/', INCO_DIR));
define('INCO_DIR_CLIENTES', sprintf('%s/Documentos/Clientes/', INCO_DIR));
define('INCO_DIR_KPIS', sprintf('%s/Documentos/KPIs/', INCO_DIR));
define('INCO_DIR_NCS', sprintf('%s/Documentos/NCs/', INCO_DIR));
define('INCO_DIR_POTENCIALES', sprintf('%s/Documentos/Potenciales/', INCO_DIR));
define('INCO_DIR_PROVEEDORES', sprintf('%s/Documentos/Proveedores/', INCO_DIR));
define('INCO_DIR_TALLER', sprintf('%s/Documentos/Taller/', INCO_DIR));
define('INCO_DIR_ORDEN_CARGA', sprintf('%s/Documentos/OrdenCarga/', INCO_DIR));
define('INCO_DIR_I18N', sprintf('%s/PHP/i18n', INCO_DIR));
define('INCO_DIR_IMAGENES', sprintf('%s/PHP/imagenes/', INCO_DIR));
define('INCO_DIR_LOG', sprintf('%s/PHP/log/', INCO_DIR));
define('INCO_DIR_MAILS_SPOOL', sprintf('%s/PHP/mails/spool', INCO_DIR));
define('INCO_DIR_MAILS_TEMPLATES', sprintf('%s/PHP/mails/templates/', INCO_DIR));
define('INCO_DIR_FACTURAS_EMITIDAS', sprintf('/home/multibase/Scaner/A%s/FraEmiti/', (new DateTime())->format('Y')));

const INCO_DATE_FORMAT = 'd/m/Y';
const INCO_TIME_FORMAT = 'H:i';
define("INCO_DATETIME_FORMAT", sprintf('%s %s', INCO_DATE_FORMAT, INCO_TIME_FORMAT));

define('INCO_DB_LOCAL', [
    'host' => $_ENV['DB_LOCAL_HOST'],
    'dbname' => $_ENV['DB_LOCAL_DBNAME'],
    'user' => $_ENV['DB_LOCAL_USER'],
    'password' => $_ENV['DB_LOCAL_PASSWORD'],
    'options' => '\'--client_encoding=UTF8\'',
]);

define('INCO_DB_REMOTE', [
    'host' => $_ENV['DB_REMOTE_HOST'],
    'dbname' => $_ENV['DB_REMOTE_DBNAME'],
    'user' => $_ENV['DB_REMOTE_USER'],
    'password' => $_ENV['DB_REMOTE_PASSWORD'],
    'options' => '\'--client_encoding=UTF8\'',
]);

define('INCO_DB_REMOTE_MYSQL', [
    'host' => $_ENV['DB_REMOTE_MYSQL_HOST'],
    'port' => $_ENV['DB_REMOTE_MYSQL_PORT'],
    'dbname' => $_ENV['DB_REMOTE_MYSQL_DBNAME'],
    'user' => $_ENV['DB_REMOTE_MYSQL_USER'],
    'password' => $_ENV['DB_REMOTE_MYSQL_PASSWORD'],
    'options' => [
        'charset' => 'utf8'
    ],
]);

define('INCO_IMAP_HOST', $_ENV['IMAP_HOST']);
define('INCO_IMAP_SENT_BOX', $_ENV['IMAP_SENT_BOX']);

define('INCO_SMTP_HOST', $_ENV['SMTP_HOST']);
define('INCO_SMTP_PORT', $_ENV['SMTP_PORT']);

define('INCO_MAILRELAY_SMTP_HOST', $_ENV['MAILRELAY_SMTP_HOST']);
define('INCO_MAILRELAY_SMTP_PORT', $_ENV['MAILRELAY_SMTP_PORT']);
define('INCO_MAILRELAY_SMTP_USER', $_ENV['MAILRELAY_SMTP_USER']);
define('INCO_MAILRELAY_SMTP_PASSWORD', $_ENV['MAILRELAY_SMTP_PASSWORD']);

define('INCO_EMAIL_NOREPLY', $_ENV['EMAIL_NOREPLY']);
define('INCO_EMAIL_NOREPLY_PASSWD', $_ENV['EMAIL_NOREPLY_PASSWD']);
define('INCO_EMAIL_COMPRAS_ID', $_ENV['EMAIL_COMPRAS_ID']);

const INCO_EMAIL_MESSAGE_ADDRESS_SEPARATOR = ';';
define('INCO_EMAIL_MESSAGE', [
    'delivery' => $_ENV['EMAIL_MESSAGE_DELIVERY'],
    'fields' => [
        'LANG' => $_ENV['EMAIL_MESSAGE_LANG'],
        'CHARSET' => $_ENV['EMAIL_MESSAGE_CHARSET'],
        'BODY' => '',
        'NOMBRE' => $_ENV['EMAIL_MESSAGE_NOMBRE'],
        'MOVIL' => $_ENV['EMAIL_MESSAGE_MOVIL'],
        'MAIL' => $_ENV['EMAIL_INFO'],
        'SRC_IMAGE' => INCO_DIR_IMAGENES . $_ENV['EMAIL_MESSAGE_IMAGE']
    ],
    'field_enclosure' => '$',
    'templates' => [
        'usuario' => INCO_DIR_MAILS_TEMPLATES . 'templateUsuario.html',
        'noreply' => INCO_DIR_MAILS_TEMPLATES . 'templateNoreply.html',
        'encuesta' => INCO_DIR_MAILS_TEMPLATES . 'templateEncuesta.html'
    ]
]);

require_once __DIR__ . '/functions.php';
