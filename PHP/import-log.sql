-- **** v22.04.26 **** --
insert into log_versions (version, info) values ('v22.04.26', '1.1. (Maestros - Ficha económica) Se añade un selecctor de ofertas para ver el crédito del cliente en función de la oferta seleccionada');
insert into log_versions (version, info) values ('v22.04.26', '4.2. (Pedidos - Curvado) Se ajusta el cálculo de los parámetro de curvado (H1 ajustada, HBF número). Se revisa la opción de misma configuración y el cálculo se realiza antes de grabar');
insert into log_versions (version, info) values ('v22.04.26', '4.2. (Pedidos) Se ajusta el cálculo de la cantidad total porque tenías que pasar por todos los campos para que se actualizase');
insert into log_versions (version, info) values ('v22.04.26', '5.A. (Envío facturas) El cambio de estado se hace tras el envío del email porque cuando se produce error en alguna factura el resto no actualiza el estado');
update empresa set vcs = 'v22.04.26' where codigo = 1;
-- **** v22.04.28 **** --
alter table diario_tiempo add observaciones character(255), add proyecto character(63);
alter table tiempos_lecturas add observaciones character(255), add proyecto character(63);
insert into log_versions (version, info) values ('v22.04.28', '7.6. (Diario Tiempo) Se añaden dos campos (observaciones y proyecto) al diario de tiempos');
insert into log_versions (version, info) values ('v22.04.28', 'Gasparini. Se corrigen los avisos de los pedidos');
update empresa set vcs = 'v22.04.28' where codigo = 1;
-- **** v22.04.28 **** --
update empresa set vcs = 'v22.04.28' where codigo = 1;
-- **** v22.05.03 **** --
insert into log_versions (version, info) values ('v22.05.03', 'Fabribask. Se corrigen los avisos de los pedidos');
insert into log_versions (version, info) values ('v22.05.03', 'Reliantt. Se corrigen los avisos de los pedidos');
insert into log_versions (version, info) values ('v22.05.03', 'Hoja Taller Chapa. Se añaden las observaciones de fábrica.');
insert into log_versions (version, info) values ('v22.05.03', 'En ficha económica se duplicaban los importes de anticipos. Se separan los anticipos de aplicación general del resto.');
update empresa set vcs = 'v22.05.03' where codigo = 1;
-- **** v22.05.13 **** --
alter table empleados add email character(127);
insert into log_versions (version, info) values ('v22.05.13', '1.E. (Empleados) Se añade el campo email en la ficha del empleado');
insert into log_versions (version, info) values ('v22.05.13', 'Tiempos. Se revisa y se corrigen posibles errores');
update empresa set vcs = 'v22.05.13' where codigo = 1;
-- **** v22.05.13 **** --
update empresa set vcs = 'v22.05.13' where codigo = 1;
-- **** v22.05.13 **** --
update empresa set vcs = 'v22.05.13' where codigo = 1;
-- **** v22.05.18 **** --
insert into log_versions (version, info) values ('v22.05.18', '4.2. (Pedidos - Curvado) Se ajusta el cálculo de los parámetros de curvado (num. prensados - dist. golpes)');
insert into log_versions (version, info) values ('v22.05.18', 'PDFs y emails con ficheros de entorno');
update empresa set vcs = 'v22.05.18' where codigo = 1;
-- **** v22.05.18 **** --
update empresa set vcs = 'v22.05.18' where codigo = 1;
-- **** v22.05.18 **** --
update empresa set vcs = 'v22.05.18' where codigo = 1;
-- **** v22.05.18 **** --
insert into log_versions (version, info) VALUES ('v22.05.18', 'Se modifica el cálculo de crédito para los pedidos');
update empresa set vcs = 'v22.05.18' where codigo = 1;
-- **** v22.05.24 **** --
insert into log_versions (version, info) VALUES ('v22.05.24', '4.2. (Pedidos - Curvado) Se muestras las muestras y las verificaciones para los cambios de profundidad');
update empresa set vcs = 'v22.05.24' where codigo = 1;
-- **** v22.05.26 **** --
insert into log_versions (version, info) VALUES ('v22.05.26', '4.2. (Pedidos - Curvado) El selector de profundidades ha sido corregido, daba error al seleccionar una profundidad');
update empresa set vcs = 'v22.05.26' where codigo = 1;
-- **** v22.05.26 **** --
update empresa set vcs = 'v22.05.26' where codigo = 1;
-- **** v22.06.16 **** --
create table if not exists clientes_contactos (
    codigo integer primary key,
    cliente integer not null,
    cargo varchar(255),
    nombre varchar(127) not null,
    apellidos varchar(255),
    email varchar(255) not null ,
    telefono varchar(31),
    estado smallint not null
);
create table if not exists proveedores_contactos (
    codigo integer primary key,
    proveedor integer not null,
    cargo varchar(255),
    nombre varchar(127) not null,
    apellidos varchar(255),
    email varchar(255) not null ,
    telefono varchar(31),
    estado smallint not null
);
create table if not exists comunicaciones (
    codigo integer primary key,
    nombre varchar(127) not null,
    descripcion varchar(255),
    tipo smallint not null,
    estado smallint not null,
    email_store smallint,
    email_from smallint,
    email_subject varchar(255) not null,
    email_body varchar(1023) not null,
    email_to varchar(255),
    email_cc_interna varchar(255)
);
comment on column comunicaciones.tipo is '0: Interna; 1: Cliente; 2: Proveedor';
comment on column comunicaciones.email_from is '0: No reply; 1: Usuario';
create table if not exists comunicaciones_adjuntos (
    comunicacion integer,
    linea integer,
    archivo varchar(255),
    constraint pk_tc_adj primary key (comunicacion, linea)
);
create table if not exists cli_contactos_comunicaciones (
    contacto integer,
    comunicacion integer,
    constraint pk_ccc primary key (contacto, comunicacion)
);
create table if not exists prov_contactos_comunicaciones (
    contacto integer,
    comunicacion integer,
    constraint pk_pcc primary key (contacto, comunicacion)
);
insert into menus_items (menu, item, nombre, comando, separador, orden, item_padre, ddisabled, nivel_usuario_minimo)
    VALUES (1, (select coalesce(max(item), 0) + 1 from menus_items where menu = 1), 'Comunicaciones', 'MComunicaciones', 0, (select coalesce(max(orden), 0) + 1 from menus_items where menu = 1 and item_padre = 8), 8, 0, 9);
insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso)
    values ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), 0, (select max(item) from menus_items where menu = 1), 8, 2);
do $$
    declare
        ucodigo smallint;
    begin
        for ucodigo in select codigo from wusuario where estado = 1 and codigo > 0 and codigo < 90
        loop
            insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso) VALUES
                ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), ucodigo, (select max(item) from menus_items where menu = 1), 8, 0);
        end loop;
    end;
$$;
insert into log_versions (version, info) VALUES ('v22.06.16', '1.1. (Clientes) Se añade una tabla de contactos para los clientes');
insert into log_versions (version, info) VALUES ('v22.06.16', '4.2. (Pedidos) Se habilita la proforma para pedidos servidos');
insert into log_versions (version, info) VALUES ('v22.06.16', '4.6. (Transferencias) Cuando se recibe un dinero en efectivo pedía el banco');
update empresa set vcs = 'v22.06.16' where codigo = 1;
-- **** v22.06.16 **** --
update empresa set vcs = 'v22.06.16' where codigo = 1;
-- **** v22.06.28 **** --
alter table clientes_contactos
    alter column nombre drop not null,
    alter column email drop not null;
do $$
    declare
        ccodigo integer;
        ceti varchar;
        ctel varchar;
        cemail varchar;
    begin
        for ccodigo, ceti, ctel in select codigo, trim(etiqueta_telefono_1), replace(telefono_1, ' ', '')
                                   from clientes
                                   where estado = 1 and telefono_1 is not null and lower(telefono_1) not like '%x%' and trim(telefono_1) != ''
            loop
                insert into clientes_contactos (codigo, cliente, cargo, telefono, estado) values ((select coalesce(max(codigo), 0) + 1 from clientes_contactos), ccodigo, ceti, ctel, 1);
            end loop;
        for ccodigo, ceti, ctel in select codigo, trim(etiqueta_telefono_2), replace(telefono_2, ' ', '')
                                   from clientes
                                   where estado = 1 and telefono_2 is not null and lower(telefono_2) not like '%x%' and trim(telefono_2) != ''
            loop
                insert into clientes_contactos (codigo, cliente, cargo, telefono, estado) values ((select coalesce(max(codigo), 0) + 1 from clientes_contactos), ccodigo, ceti, ctel, 1);
            end loop;
        for ccodigo, ceti, ctel in select codigo, trim(etiqueta_telefono_3), replace(telefono_3, ' ', '')
                                   from clientes
                                   where estado = 1 and telefono_3 is not null and lower(telefono_3) not like '%x%' and trim(telefono_3) != ''
            loop
                insert into clientes_contactos (codigo, cliente, cargo, telefono, estado) values ((select coalesce(max(codigo), 0) + 1 from clientes_contactos), ccodigo, ceti, ctel, 1);
            end loop;
    end;
$$;
insert into log_versions (version, info) values ('v22.06.28', 'Contactos transferidos a las nuevas tablas');
insert into log_versions (version, info) values ('v22.06.28', '5.A. (Envío de facturas) Opción para marcar como enviadas facturas que ya han sido enviadas por otro medio');
update empresa set vcs = 'v22.06.28' where codigo = 1;
-- **** v22.06.28 **** --
update empresa set vcs = 'v22.06.28' where codigo = 1;
-- **** v22.06.30 **** --
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (1, 'EXCESO DE CONSUMO', null, 0, 1, 1, 0, 'INCOPERFIL | Exceso de Consumo', '<p>PORTE MODIFICADO</p>$cosmos$', 'joseginer@incoperfil.com', null);
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (2, 'CAMBIO EN PORTE', null, 0, 1, 1, 1, 'INCOPERFIL | Porte modificado', '<p>PORTE MODIFICADO</p>$cosmos$', '$de.email$', null);
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (3, 'TRANSFERENCIA REGISTRADA', null, 0, 1, 1, 0, 'INCOPERFIL | Transferencia bancaria', '<p>TRANSFERENCIA BANCARIA</p><p>Cliente: $clientes.numero$ - $clientes.nombre$</p><p>$transferencias.aplicacion$: $transferencias.numero$</p><p>Importe Solicitado: $transferencias.solicitado$</p><p>Importe Recibido: $transferencias.importe$</p>$cosmos$', '$de.email$', 'comercial@incoperfil.com');
insert into log_versions(version, info) VALUES ('v22.06.30', '1.1. (Clientes) Ya se pueden borrar las líneas de contactos y observaciones');
insert into log_versions(version, info) VALUES ('v22.06.30', '8.2. (Comunicaciones) Se añaden las comunicaciones internas para testear su funcionamiento');

update empresa set vcs = 'v22.06.30' where codigo = 1;
-- **** v22.06.30 **** --
update empresa set vcs = 'v22.06.30' where codigo = 1;
-- **** v22.07.05 **** --
alter table pedidoslin add p_radio integer, add p_flecha integer, add p_cuerda integer;
alter table pedidoslin2 add p_radio integer, add p_flecha integer, add p_cuerda integer;
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (4, 'PROCESO ENVIO FACTURAS', 'SE ENVIA DURANTE EL PROCESO DE REVISIÓN, APROBACION Y ENVIO DE FACTURAS', 0, 1, 1, 0, 'INCOPERFIL | $cosmos$', '$cosmos$', '$de.email$', null);
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (5, 'OFERTA', null, 1, 1, 1, 1, 'INCOPERFIL | Oferta $ofertas.numero$$ofertas.revision$', '$cosmos$', '$de.email$', null);
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (6, 'SOLICITUD FACTURA ELECTRONICA', null, 1, 0, 1, 0, 'INCOPERFIL | Solicitud para el envío de facturas electrónicas', 'Estimado cliente,<br><br>Desde INCOPERFIL le informamos que ya podemos enviar la factura electrónica. Rellene este sencillo formulario y disfrute de todas las ventajas de la factura electrónica:<p style="text-align: center; margin-top: 2rem;"><a href="http://145.14.157.246/envio-facturas/?cliente=$clientes.codigo$" style="padding: 10px 25px; background-color: #c41100; color: #FFF; text-decoration: none;">Formulario</a></p><p><img width="100%" src="$SRC_FACTE$" alt="Ventajas Factura Electrónica"></p>', '$de.email$', null);
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (7, 'FACTURA', null, 1, 1, 1, 1, 'INCOPERFIL | Factura Nº $facturas.numero$', 'Estimado cliente,<br><br>le adjuntamos la factura número $facturas.numero$ en relación a:<br><ul><li>Oferta: $ofertas.numero$</li><li>Pedido/s: $pedidos.numero$</li><li>Referencia de Obra: $ofertas.referencia_obra$</li></ul>$CC$<br><br>Saludos cordiales', '$de.email$', null);
update empresa set vcs = 'v22.07.05' where codigo = 1;
-- **** v22.07.05 **** --
update empresa set vcs = 'v22.07.05' where codigo = 1;
-- **** v22.07.05 **** --
update empresa set vcs = 'v22.07.05' where codigo = 1;
-- **** v22.07.05 **** --
update empresa set vcs = 'v22.07.05' where codigo = 1;
-- **** v22.07.07 **** --
insert into log_versions (version, info) VALUES ('v22.07.07', 'Informe de invetario de bobinas ahora inluye todas las bobinas excepto las desactivadas');
update empresa set vcs = 'v22.07.07' where codigo = 1;
-- **** v22.07.12 **** --
insert into cli_contactos_comunicaciones (contacto, comunicacion) select cc.codigo, 7 from clientes_contactos cc where cc.apellidos like '%EFAC%';
update clientes_contactos set apellidos = null where apellidos = 'EFAC';
update clientes_contactos set apellidos = replace(apellidos, 'EFAC', '') where apellidos like '%EFAC%';
insert into log_versions (version, info) VALUES ('v22.07.12', 'Se cambia el envío de facturas por medio de las comunicaciones');
update empresa set vcs = 'v22.07.12' where codigo = 1;
-- **** v22.07.14 **** --
update empresa set vcs = 'v22.07.14' where codigo = 1;
-- **** v22.07.14 **** --
insert into log_versions (version, info) VALUES ('v22.07.14', '8.2. (Comunicaciones) Se corrige la comunicación de factura. No selecciona los destinatarios ni reemplazaba la información dinámicamente');
update empresa set vcs = 'v22.07.14' where codigo = 1;
-- **** v22.07.14 **** --
update ofertaslin set estado = null, partida = 'V' where partida like 'V%';
update ofertaslin set estado = 3 where estado != 3 and identificador in (
    select distinct o.identificador from ofertas o where o.potencial = 1 and o.estado = 4
);
update ofertas set estado = 3 where estado = 4 and potencial = 1 and ejercicio::integer < 2022;
update ofertas set estado = 0 where estado = 4 and potencial = 1 and ejercicio::integer >= 2022;
create or replace function fn_pedidoslin_all() returns trigger
    language plpgsql
as
$$
DECLARE
    ident CHARACTER(15);
    identOferta CHARACTER(15);
    imp NUMERIC(12,2);
    impAnchoUtil NUMERIC(12,2);
    impRecargos NUMERIC(12,2);
    tipoIva SMALLINT;
    pedEstado SMALLINT;
    aplicarRecargos SMALLINT;
    porcentaje NUMERIC(4,2);
    impIva NUMERIC(12,2);
    impTotal NUMERIC(12,2);
    pes NUMERIC(10,3);
    bult INTEGER;
    totLin INTEGER;
    linPen INTEGER;
    linFab INTEGER;
    linTer INTEGER;
    linCan INTEGER;
    linSer INTEGER;
    cerrarOferta smallint;
BEGIN
    IF (TG_OP = 'DELETE') THEN
        ident = OLD.identificador;
        DELETE FROM pedidoslin_curvado WHERE identificador = ident AND linea = OLD.linea;
    ELSE ident = NEW.identificador;
    END IF;

    impIva = 0;
    impTotal = 0;

    -- OBTENEMOS SI APLICAMOS RECARGOS
    SELECT aplicar_recargos INTO aplicarRecargos FROM pedidos where identificador = ident;
    IF (aplicarRecargos = 0) THEN
        impRecargos = 0;
    ELSE
        SELECT sum(importe) into impRecargos from pedidos_recargos where identificador = ident;
        IF (impRecargos IS NULL) THEN impRecargos = 0; END IF;
    end if;

    -- OBTENEMOS EL TOTAL DEL PEDIDO: METROS CUADRADOS
    -- SELECT sum((pl.cantidad_total * (coalesce(p.ancho_util,1000)::numeric(10,3) / 1000))::numeric(10,2) * pl.precio)::numeric(10,2) INTO impAnchoUtil FROM pedidoslin pl, productos p WHERE pl.identificador = ident AND pl.producto = p.codigo AND p.unidad_medida = 1; -- se desactiva 27/05/2021
    /*SELECT sum(case when pl.seccion = 6 or pl.seccion = 7 or pl.seccion = 8 then (pl.cantidad_total * pl.precio)::numeric(10,2) * (1 - pl.descuento / 100)::numeric(10,2)
        else ((pl.cantidad_total * pl.precio)::numeric(10,2) * (p.ancho_util::numeric(10,3) / 1000)::numeric(10,2))::numeric(10,2) * (1 - pl.descuento / 100)::numeric(10,2) end )::numeric(10,2)
        INTO impAnchoUtil FROM pedidoslin pl, productos p WHERE pl.identificador = ident AND pl.producto = p.codigo AND p.unidad_medida = 1;
	IF (impAnchoUtil IS NULL) THEN impAnchoUtil = 0; END IF;*/
    -- OBTENEMOS EL TOTAL DEL PEDIDO: METROS LINEALES Y UNIDADES
    /*SELECT sum(pl.cantidad_total * pl.precio)::numeric(10,2) INTO imp FROM pedidoslin pl, productos p WHERE pl.identificador = ident AND pl.producto = p.codigo AND p.unidad_medida IN (2, 4);
    IF (imp IS NULL) THEN imp = 0; END IF;
    imp = imp + impAnchoUtil + impRecargos;*/

    select sum(importe) into imp from vw_pedidoslin_proforma where identificador = ident;
    IF (imp IS NULL) THEN imp = 0; END IF;
    imp = imp + impRecargos;

    -- OBTENEMOS IVA
    SELECT iva, cerrar_oferta INTO tipoIva, cerrarOferta FROM pedidos WHERE identificador = ident;
    IF (tipoIva = 0) THEN impIva = 0;
    ELSE
        -- SELECCIONAMOS IVA
        SELECT iva0 INTO porcentaje FROM centrofac WHERE ejercicio = EXTRACT(YEAR FROM CURRENT_DATE);
        -- CALCULANDO IMPORTE IVA
        IF (porcentaje = 0) THEN impIva = 0;
        ELSE impIva = imp * (porcentaje / 100);
        END IF;
        IF (impIva IS NULL) THEN impIva = 0; END IF;
    END IF;
    -- CALCULANDO IMPORTE TOTAL
    impTotal = imp + impIva;

    -- OBTENEMOS EL PESO TOTAL DEL PEDIDO
    SELECT SUM(peso) INTO pes FROM pedidoslin WHERE identificador = ident;
    IF (pes IS NULL) THEN pes = 0; END IF;
    -- OBTENEMOS EL NUMERO DE BULTOS
    SELECT COUNT(DISTINCT paquete) INTO bult FROM pedidoslin WHERE identificador = ident;
    IF (bult IS NULL) THEN bult = 0; END IF;
    -- ACTUALIZAMOS EL IMPORTE, EL IMPORTE IVA, EL TOTAL, EL PESO Y EL NUMERO DE BULTOS DEL PEDIDO
    UPDATE pedidos SET importe = imp, importe_iva = impIva, total = impTotal, peso = pes, bultos = bult WHERE identificador = ident;
    -- ACTUALIZAMOS EL ESTADO DEL PEDIDO
    SELECT COUNT(*) INTO totLin FROM pedidoslin WHERE identificador = ident;
    SELECT COUNT(*) INTO linFab FROM pedidoslin WHERE estado >= 4 AND identificador = ident;
    IF (totLin > 0) THEN
        IF (linFab > 0) THEN
            -- PEDIDO EN FABRICACION
            UPDATE pedidos SET estado = 1 WHERE identificador = ident;
            UPDATE pedidos SET fecha_fabricacion = current_date WHERE identificador = ident AND fecha_fabricacion IS NULL;
        ELSE
            SELECT COUNT(*) INTO linPen FROM pedidoslin WHERE estado = 0 AND identificador = ident;
            SELECT COUNT(*) INTO linTer FROM pedidoslin WHERE estado = 1 AND identificador = ident;
            SELECT COUNT(*) INTO linSer FROM pedidoslin WHERE estado = 2 AND identificador = ident;
            SELECT COUNT(*) INTO linCan FROM pedidoslin WHERE estado = 3 AND identificador = ident;

            -- DESCONTAMOS LAS CANCELADAS
            totLin = totLin - linCan;
            -- PEDIDO FABRICACION
            IF (linPen > 0 AND (linTer > 0 OR linSer > 0)) THEN
                UPDATE pedidos SET estado = 1 WHERE identificador = ident;
                UPDATE pedidos SET fecha_fabricacion = current_date WHERE identificador = ident AND fecha_fabricacion IS NULL;
            END IF;
            -- PEDIDO TERMINADO
            IF (linTer = totLin OR (linPen = 0 AND linTer > 0)) THEN
                UPDATE pedidos SET estado = 2 WHERE identificador = ident;
                UPDATE pedidos SET fecha_terminado = current_date WHERE identificador = ident AND fecha_terminado IS NULL;
            END IF;
            -- PEDIDO SERVIDO
            IF (linSer = totLin) THEN
                UPDATE pedidos SET estado = 4 WHERE identificador = ident;
                UPDATE pedidos SET fecha_servido = current_date WHERE identificador = ident AND fecha_servido IS NULL;
                if (cerrarOferta = 1) then
                    UPDATE ofertas set estado = 4 where identificador = identOferta;
                    insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_pedidoslin_all', 'ofertas', 'ACTUALIZA ESTADO', 'OFERTA ' || substr(identOferta, 10) || ' PASA A ESTADO 4');
                end if;
            END IF;
        END IF;
    END IF;
    -- ACTUALIZAR DATOS DE LA OFERTA
    SELECT COALESCE(identificador_oferta, '000000000000000') INTO identOferta FROM pedidos WHERE identificador = ident;
    IF (identOferta <> '000000000000000' AND TG_OP = 'UPDATE') THEN
        IF (identOferta <> '000000000000000' AND OLD.estado <> NEW.estado AND OLD.estado <> 2 AND NEW.estado = 1) THEN
            UPDATE ofertaslin SET metros_fabricados = metros_fabricados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida NOT LIKE '%V%';
            -- UPDATE ofertaslin SET metros_fabricados = metros_fabricados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida != 'V';
            insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_pedidoslin_all', 'ofertaslin', 'ACTUALIZA METROS FAB', 'OFERTA ' || substr(identOferta, 10) || ' +' || NEW.cantidad_total || ' M.FAB');
        END IF;
        IF (identOferta <> '000000000000000' AND OLD.estado <> NEW.estado AND NEW.estado = 2) THEN
            UPDATE ofertaslin SET metros_suministrados = metros_suministrados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida NOT LIKE '%V%';
            -- UPDATE ofertaslin SET metros_suministrados = metros_suministrados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida != 'V';
            insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_pedidoslin_all', 'ofertaslin', 'ACTUALIZA METROS SUM', 'OFERTA ' || substr(identOferta, 10) || ' +' || NEW.cantidad_total || ' M.SUM');
        END IF;
    END IF;

    /*IF (TG_OP = 'DELETE' and position('PI' in ident)) then
        delete from pedidoslin_transformaciones where identificador = ident and linea = old.linea;
    end if;*/

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
END;
$$;
create or replace function fn_ofertaslin_all() returns trigger
    language plpgsql
as
$$
DECLARE
    total DECIMAL(12,2);
    porLimInf DECIMAL(10,2);
    porcentaje DECIMAL(10,2);
    ident CHARACTER(15);
    totLin INTEGER;
    linNum INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') THEN ident = OLD.identificador;
    ELSE ident = NEW.identificador;
    END IF;
    -- OBTENEMOS EL TOTAL DE LA OFERTA
    SELECT SUM(cantidad * precio)::numeric(10,2) INTO total FROM ofertaslin WHERE identificador = ident AND upper(partida) not like '%V%';
    -- COMPROBAR QUE NO SEA NULL
    IF (total IS NULL) THEN total = 0;
    END IF;
    -- ACTUALIZAMOS EL IMPORTE DE LA OFERTA
    UPDATE ofertas SET importe = total WHERE identificador = ident;

    -- ACTUALIZAMOS ESTADO LINEA Y CABECERA
    IF (TG_OP = 'UPDATE') THEN
        select ofertaslin_cantidad_por_lim_inf into porLimInf from empresa where codigo = 1;
        porcentaje = NEW.cantidad * (1 - (porLimInf / 100));
        -- LINEA TERMINADA
        IF (NEW.metros_fabricados >= porcentaje and NEW.partida != 'V') THEN NEW.estado := 2;
        END IF;
        -- LINEA SUMINISTRADA
        IF (NEW.metros_suministrados >= porcentaje and NEW.partida != 'V') THEN NEW.estado := 4;
        END IF;

        SELECT COUNT(*) INTO totLin FROM ofertaslin WHERE identificador = NEW.identificador AND partida NOT LIKE '%V%';
        SELECT COUNT(*) INTO linNum FROM ofertaslin WHERE estado = 4 AND identificador = NEW.identificador AND partida NOT LIKE '%V%';
        IF (linNum = totLin) THEN
            -- OFERTA SERVIDO
            UPDATE ofertas SET estado = 4 WHERE identificador = NEW.identificador;
            insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_ofertaslin_all', 'ofertas', 'ACTUALIZA ESTADO', 'OFERTA ' || substr(NEW.identificador, 10) || ' PASA A ESTADO 4');
        END IF;
    END IF;

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
END;
$$;
create or replace function fn_ofertas_after_update() returns trigger
    language plpgsql
as
$$
DECLARE
BEGIN
    if old.estado != new.estado and new.potencial = 1 then
        insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_ofertas_after_update', 'ofertas', 'ACTUALIZA ESTADO', 'OFERTA ' || new.numero || ' PASA A ESTADO ' || new.estado);
    end if;
    return new;
END
$$;
create trigger tg_ofertas_after_update
    after update
    on ofertas
    for each row
execute procedure fn_ofertas_after_update();
insert into log_versions (version, info) VALUES ('v22.07.14', 'Se añaden marcas para realizar un seguimiento de las ofertas de potenciales que se marcan como servidas');
update empresa set vcs = 'v22.07.14' where codigo = 1;
-- **** v22.07.19 **** --
create or replace function fn_pedidoslin_all() returns trigger
    language plpgsql
as
$$
DECLARE
    ident CHARACTER(15);
    identOferta CHARACTER(15);
    imp NUMERIC(12,2);
    impAnchoUtil NUMERIC(12,2);
    impRecargos NUMERIC(12,2);
    tipoIva SMALLINT;
    pedEstado SMALLINT;
    aplicarRecargos SMALLINT;
    porcentaje NUMERIC(4,2);
    impIva NUMERIC(12,2);
    impTotal NUMERIC(12,2);
    pes NUMERIC(10,3);
    bult INTEGER;
    totLin INTEGER;
    linPen INTEGER;
    linFab INTEGER;
    linTer INTEGER;
    linCan INTEGER;
    linSer INTEGER;
    cerrarOferta smallint;
BEGIN
    IF (TG_OP = 'DELETE') THEN
        ident = OLD.identificador;
        DELETE FROM pedidoslin_curvado WHERE identificador = ident AND linea = OLD.linea;
    ELSE ident = NEW.identificador;
    END IF;

    impIva = 0;
    impTotal = 0;

    -- OBTENEMOS SI APLICAMOS RECARGOS, EL TIPO DE IVA, SI SE CIERRA LA OFERTA Y EL ID DE LA OFERTA
    SELECT aplicar_recargos, iva, cerrar_oferta, COALESCE(identificador_oferta, '000000000000000') INTO aplicarRecargos, tipoIva, cerrarOferta, identOferta FROM pedidos where identificador = ident;
    IF (aplicarRecargos = 0) THEN
        impRecargos = 0;
    ELSE
        SELECT sum(importe) into impRecargos from pedidos_recargos where identificador = ident;
        IF (impRecargos IS NULL) THEN impRecargos = 0; END IF;
    end if;

    -- OBTENEMOS EL TOTAL DEL PEDIDO: METROS CUADRADOS
    -- SELECT sum((pl.cantidad_total * (coalesce(p.ancho_util,1000)::numeric(10,3) / 1000))::numeric(10,2) * pl.precio)::numeric(10,2) INTO impAnchoUtil FROM pedidoslin pl, productos p WHERE pl.identificador = ident AND pl.producto = p.codigo AND p.unidad_medida = 1; -- se desactiva 27/05/2021
    /*SELECT sum(case when pl.seccion = 6 or pl.seccion = 7 or pl.seccion = 8 then (pl.cantidad_total * pl.precio)::numeric(10,2) * (1 - pl.descuento / 100)::numeric(10,2)
        else ((pl.cantidad_total * pl.precio)::numeric(10,2) * (p.ancho_util::numeric(10,3) / 1000)::numeric(10,2))::numeric(10,2) * (1 - pl.descuento / 100)::numeric(10,2) end )::numeric(10,2)
        INTO impAnchoUtil FROM pedidoslin pl, productos p WHERE pl.identificador = ident AND pl.producto = p.codigo AND p.unidad_medida = 1;
	IF (impAnchoUtil IS NULL) THEN impAnchoUtil = 0; END IF;*/
    -- OBTENEMOS EL TOTAL DEL PEDIDO: METROS LINEALES Y UNIDADES
    /*SELECT sum(pl.cantidad_total * pl.precio)::numeric(10,2) INTO imp FROM pedidoslin pl, productos p WHERE pl.identificador = ident AND pl.producto = p.codigo AND p.unidad_medida IN (2, 4);
    IF (imp IS NULL) THEN imp = 0; END IF;
    imp = imp + impAnchoUtil + impRecargos;*/

    select sum(importe) into imp from vw_pedidoslin_proforma where identificador = ident;
    IF (imp IS NULL) THEN imp = 0; END IF;
    imp = imp + impRecargos;

    -- OBTENEMOS IVA
    IF (tipoIva = 0) THEN impIva = 0;
    ELSE
        -- SELECCIONAMOS IVA
        SELECT iva0 INTO porcentaje FROM centrofac WHERE ejercicio = EXTRACT(YEAR FROM CURRENT_DATE);
        -- CALCULANDO IMPORTE IVA
        IF (porcentaje = 0) THEN impIva = 0;
        ELSE impIva = imp * (porcentaje / 100);
        END IF;
        IF (impIva IS NULL) THEN impIva = 0; END IF;
    END IF;
    -- CALCULANDO IMPORTE TOTAL
    impTotal = imp + impIva;

    -- OBTENEMOS EL PESO TOTAL DEL PEDIDO
    SELECT SUM(peso) INTO pes FROM pedidoslin WHERE identificador = ident;
    IF (pes IS NULL) THEN pes = 0; END IF;
    -- OBTENEMOS EL NUMERO DE BULTOS
    SELECT COUNT(DISTINCT paquete) INTO bult FROM pedidoslin WHERE identificador = ident;
    IF (bult IS NULL) THEN bult = 0; END IF;
    -- ACTUALIZAMOS EL IMPORTE, EL IMPORTE IVA, EL TOTAL, EL PESO Y EL NUMERO DE BULTOS DEL PEDIDO
    UPDATE pedidos SET importe = imp, importe_iva = impIva, total = impTotal, peso = pes, bultos = bult WHERE identificador = ident;
    -- ACTUALIZAMOS EL ESTADO DEL PEDIDO
    SELECT COUNT(*) INTO totLin FROM pedidoslin WHERE identificador = ident;
    SELECT COUNT(*) INTO linFab FROM pedidoslin WHERE estado >= 4 AND identificador = ident;
    IF (totLin > 0) THEN
        IF (linFab > 0) THEN
            -- PEDIDO EN FABRICACION
            UPDATE pedidos SET estado = 1 WHERE identificador = ident;
            UPDATE pedidos SET fecha_fabricacion = current_date WHERE identificador = ident AND fecha_fabricacion IS NULL;
        ELSE
            SELECT COUNT(*) INTO linPen FROM pedidoslin WHERE estado = 0 AND identificador = ident;
            SELECT COUNT(*) INTO linTer FROM pedidoslin WHERE estado = 1 AND identificador = ident;
            SELECT COUNT(*) INTO linSer FROM pedidoslin WHERE estado = 2 AND identificador = ident;
            SELECT COUNT(*) INTO linCan FROM pedidoslin WHERE estado = 3 AND identificador = ident;

            -- DESCONTAMOS LAS CANCELADAS
            totLin = totLin - linCan;
            -- PEDIDO FABRICACION
            IF (linPen > 0 AND (linTer > 0 OR linSer > 0)) THEN
                UPDATE pedidos SET estado = 1 WHERE identificador = ident;
                UPDATE pedidos SET fecha_fabricacion = current_date WHERE identificador = ident AND fecha_fabricacion IS NULL;
            END IF;
            -- PEDIDO TERMINADO
            IF (linTer = totLin OR (linPen = 0 AND linTer > 0)) THEN
                UPDATE pedidos SET estado = 2 WHERE identificador = ident;
                UPDATE pedidos SET fecha_terminado = current_date WHERE identificador = ident AND fecha_terminado IS NULL;
            END IF;
            -- PEDIDO SERVIDO
            IF (linSer = totLin) THEN
                UPDATE pedidos SET estado = 4 WHERE identificador = ident;
                UPDATE pedidos SET fecha_servido = current_date WHERE identificador = ident AND fecha_servido IS NULL;
                if (cerrarOferta = 1) then
                    UPDATE ofertas set estado = 4 where identificador = identOferta;
                    insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_pedidoslin_all', 'ofertas', 'ACTUALIZA ESTADO', 'OFERTA ' || substr(identOferta, 10) || ' PASA A ESTADO 4');
                end if;
            END IF;
        END IF;
    END IF;
    -- ACTUALIZAR DATOS DE LA OFERTA
    -- SELECT COALESCE(identificador_oferta, '000000000000000') INTO identOferta FROM pedidos WHERE identificador = ident;
    IF (identOferta <> '000000000000000' AND TG_OP = 'UPDATE') THEN
        IF (identOferta <> '000000000000000' AND OLD.estado <> NEW.estado AND OLD.estado <> 2 AND NEW.estado = 1) THEN
            UPDATE ofertaslin SET metros_fabricados = metros_fabricados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida NOT LIKE '%V%';
            -- UPDATE ofertaslin SET metros_fabricados = metros_fabricados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida != 'V';
            insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_pedidoslin_all', 'ofertaslin', 'ACTUALIZA METROS FAB', 'OFERTA ' || substr(identOferta, 10) || ' +' || NEW.cantidad_total || ' M.FAB');
        END IF;
        IF (identOferta <> '000000000000000' AND OLD.estado <> NEW.estado AND NEW.estado = 2) THEN
            UPDATE ofertaslin SET metros_suministrados = metros_suministrados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida NOT LIKE '%V%';
            -- UPDATE ofertaslin SET metros_suministrados = metros_suministrados + NEW.cantidad_total WHERE identificador LIKE identOferta AND descripcion = NEW.descripcion AND partida != 'V';
            insert into log_acciones (fecha, hora, usuario, omd, tabla, accion, obs) VALUES (current_date, now(), 0, 'fn_pedidoslin_all', 'ofertaslin', 'ACTUALIZA METROS SUM', 'OFERTA ' || substr(identOferta, 10) || ' +' || NEW.cantidad_total || ' M.SUM');
        END IF;
    END IF;

    /*IF (TG_OP = 'DELETE' and position('PI' in ident)) then
        delete from pedidoslin_transformaciones where identificador = ident and linea = old.linea;
    end if;*/

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
END;
$$;
insert into log_versions (version, info) VALUES ('v22.07.19', 'Se modifican las marcas para realizar un seguimiento de las ofertas de potenciales que se marcan como servidas');
update empresa set vcs = 'v22.07.19' where codigo = 1;
-- **** v22.07.21 **** --
insert into log_versions (version, info) VALUES ('v22.07.21', '1.1. (Clientes) Se soluciona un error que no permitía añadir contactos de clientes a las distintas comunicaciones que se envían desde gestión');
insert into log_versions (version, info) VALUES ('v22.07.21', '3.1.3. (Tratamiento de Bobinas) Se soluciona un error que habilitaba los botones tras la modificación de una línea de tratamientos de bobinas');
update empresa set vcs = 'v22.07.21' where codigo = 1;
-- **** v22.08.04 **** --
insert into log_versions (version, info) values ('v22.08.04', '4.1. (Ofertas) Se corrige el proceso de borrar una línea. Cuando ocurría no se podía modificar otra línea.');
insert into log_versions (version, info) values ('v22.08.04', '3.1.1. (Bobinas - Pedidos) Se añade un listado de las facturas relacionadas con el pedido al proveedor');
insert into log_versions (version, info) values ('v22.08.04', '8.1.1. (NCs) Se permite crear consultas al usuario para generar un listado en PHP');

update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.08.04 **** --
update empresa set vcs = 'v22.08.04' where codigo = 1;
-- **** v22.09.01 **** --
update empresa set vcs = 'v22.09.01' where codigo = 1;
-- **** v22.09.06 **** --
create or replace function fn_ofertaslin_all() returns trigger
    language plpgsql
as
$$
DECLARE
    total DECIMAL(12,2);
    ident CHARACTER(15);
    numLns INTEGER;
    numLnsServ INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') THEN ident = OLD.identificador;
    ELSE ident = NEW.identificador;
    END IF;
    -- OBTENEMOS EL TOTAL DE LA OFERTA
    SELECT SUM(cantidad * precio)::numeric(10,2) INTO total FROM ofertaslin WHERE identificador = ident AND upper(partida) not like '%V%';
    -- COMPROBAR QUE NO SEA NULL
    IF (total IS NULL) THEN total = 0;
    END IF;
    -- ACTUALIZAMOS EL IMPORTE DE LA OFERTA
    UPDATE ofertas SET importe = total WHERE identificador = ident;

    -- ACTUALIZAMOS ESTADO LINEA Y CABECERA
    IF (TG_OP = 'UPDATE') THEN
        SELECT COUNT(*) INTO numLns FROM ofertaslin WHERE identificador = NEW.identificador AND partida NOT LIKE '%V%';
        SELECT COUNT(*) INTO numLnsServ FROM ofertaslin WHERE identificador = NEW.identificador AND partida NOT LIKE '%V%' AND estado = 4;

        IF (numLns = numLnsServ) THEN
            -- OFERTA SERVIDO
            UPDATE ofertas SET estado = 4 WHERE identificador = NEW.identificador;
        END IF;
    END IF;

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
END;
$$;

create function fn_ofertaslin_before_update() returns trigger
    language plpgsql
as
$$
DECLARE
    porLimInf DECIMAL(10,2);
    porcentaje DECIMAL(10,2);
BEGIN
    -- ACTUALIZAMOS ESTADO LINEA Y CABECERA
    select ofertaslin_cantidad_por_lim_inf into porLimInf from empresa where codigo = 1;
    porcentaje = NEW.cantidad * (1 - (porLimInf / 100));
    -- LINEA TERMINADA
    IF (NEW.metros_fabricados >= porcentaje and NEW.partida != 'V') THEN
        NEW.estado := 2;
    END IF;
    -- LINEA SUMINISTRADA
    IF (NEW.metros_suministrados >= porcentaje and NEW.partida != 'V') THEN
        NEW.estado := 4;
    END IF;

    RETURN NEW;
END;
$$;

create trigger tg_ofertaslin_before_update
    before update
    on ofertaslin
    for each row
execute procedure fn_ofertaslin_before_update();

insert into log_versions (version, info) values ('v22.09.06', 'Se modifican los triggers de las ofertas para modificar el estado.');
insert into log_versions (version, info) values ('v22.09.06', 'Se modifica el proceso de repetir oferta.');
update empresa set vcs = 'v22.09.06' where codigo = 1;
-- **** v22.09.08 **** --
update clientes_contactos
set nombre = (select comercial from clientes where clientes.codigo = clientes_contactos.cliente)
where apellidos like '%ECOM%';
update clientes_contactos cc
set telefono = (select telefono from clientes_contactos cc2 where cc2.cliente = cc.cliente and lower(cc2.cargo) like '%comercial%' limit 1)
where apellidos like '%ECOM%';
update clientes_contactos cc
set estado = 99
where lower(cc.cargo) like '%comercial%' and cc.telefono in (select distinct telefono from clientes_contactos cc2 where cc.cliente = cc2.cliente and cc2.apellidos like '%ECOM%');
insert into log_versions(version, info) VALUES ('v22.09.08', 'Se eliminan campos de email y teléfonos de la ficha de clientes porque se administran en los contactos');
update empresa set vcs = 'v22.09.08' where codigo = 1;
-- **** v22.09.15 **** --
alter table clientes_contactos
    add dpto_gerencia smallint,
    add dpto_administracion smallint,
    add dpto_comercial smallint,
    add dpto_produccion smallint,
    add dpto_otro smallint;
update clientes_contactos set dpto_gerencia = 0, dpto_administracion = 0, dpto_comercial = 0, dpto_produccion = 0, dpto_otro = 0 where codigo > 0;
update clientes_contactos set dpto_administracion = 1 where apellidos like '%EADM%' or lower(cargo) like '%adm%';
update clientes_contactos set dpto_comercial = 1 where apellidos like '%ECOM%' or lower(cargo) like '%comercial%';
alter table clientes_contactos drop cargo;
insert into log_versions (version, info) VALUES ('v22.09.15', 'Clientes y Potenciales con las fichas de contactos');
alter table proveedores add homologado smallint;
update proveedores set homologado = 0 where homologado is null;
insert into log_versions (version, info) VALUES ('v22.09.15', 'Proveedores. Se añade el campo "homologado" y un botón de acceso a su carpeta');
insert into log_versions (version, info) VALUES ('v22.09.15', 'Proveedores. Se añade listado de NC');
alter table pedidos_bobinas add solicitud character(15);
insert into log_versions (version, info) VALUES ('v22.09.15', 'Pedidos Bobinas. Se añade el campo "solicitud" para tener una trazabilidad en las compras');
update empresa set vcs = 'v22.09.15' where codigo = 1;
-- **** v22.09.15 **** --
alter table facturas add observaciones_internas character(255);
update empresa set vcs = 'v22.09.15' where codigo = 1;
-- **** v22.09.20 **** --
insert into log_versions (version, info) VALUES ('v22.09.20', 'Ofertas. Se añaden las CGV en inglés y francés');
update empresa set vcs = 'v22.09.20' where codigo = 1;
-- **** v22.09.21 **** --
update empresa set vcs = 'v22.09.21' where codigo = 1;
-- **** v22.09.21 **** --
update empresa set vcs = 'v22.09.21' where codigo = 1;
-- **** v22.09.21 **** --
update empresa set vcs = 'v22.09.21' where codigo = 1;
-- **** v22.09.21 **** --
update empresa set vcs = 'v22.09.21' where codigo = 1;
-- **** v22.09.27 **** --
insert into menus_items (menu, item, nombre, comando, separador, orden, item_padre, ddisabled, nivel_usuario_minimo)
VALUES (1, (select coalesce(max(item), 0) + 1 from menus_items where menu = 1), 'Listado Ofertas', 'LOfertasCantidades', 0, (select coalesce(max(orden), 0) + 1 from menus_items where menu = 1 and item_padre = 9), 9, 0, 9);
insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso)
values ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), 0, (select max(item) from menus_items where menu = 1), 9, 2);
do $$
    declare
        ucodigo smallint;
    begin
        for ucodigo in select codigo from wusuario where estado = 1 and codigo > 0 and codigo < 90
            loop
                insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso) VALUES
                    ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), ucodigo, (select max(item) from menus_items where menu = 1), 9, 0);
            end loop;
    end;
$$;
drop trigger tg_ofertaslin_before_update on ofertaslin;
create or replace function fn_ofertaslin_all() returns trigger
    language plpgsql
as
$$
DECLARE
    total DECIMAL(12,2);
    ident CHARACTER(15);
    numLns INTEGER;
    numLnsServ INTEGER;
BEGIN
    IF (TG_OP = 'DELETE') THEN ident = OLD.identificador;
    ELSE ident = NEW.identificador;
    END IF;
    -- OBTENEMOS EL TOTAL DE LA OFERTA
    SELECT SUM(cantidad * precio)::numeric(10,2) INTO total FROM ofertaslin WHERE identificador = ident AND upper(partida) not like '%V%';
    -- COMPROBAR QUE NO SEA NULL
    IF (total IS NULL) THEN total = 0;
    END IF;
    -- ACTUALIZAMOS EL IMPORTE DE LA OFERTA
    UPDATE ofertas SET importe = total WHERE identificador = ident;

    -- ACTUALIZAMOS ESTADO LINEA Y CABECERA
    /*IF (TG_OP = 'UPDATE') THEN
        SELECT COUNT(*) INTO numLns FROM ofertaslin WHERE identificador = NEW.identificador AND partida NOT LIKE '%V%';
        SELECT COUNT(*) INTO numLnsServ FROM ofertaslin WHERE identificador = NEW.identificador AND partida NOT LIKE '%V%' AND estado = 4;

        IF (numLns = numLnsServ) THEN
            -- OFERTA SERVIDO
            UPDATE ofertas SET estado = 4 WHERE identificador = NEW.identificador;
        END IF;
    END IF;*/

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
END;
$$;
insert into log_versions (version, info) VALUES ('v22.09.27', 'Se añade el listado de ofertas según fecha y estado');
insert into log_versions (version, info) VALUES ('v22.09.27', 'Se desactivan los triggers para marcar ofertas servidas. Ahora se marcarán de forma manual');
update empresa set vcs = 'v22.09.27' where codigo = 1;
-- **** v22.09.28 **** --
delete from menus_items_accesos where item_menu in (select item from menus_items where nombre like 'Listado Of%');
delete from menus_items where nombre like 'Listado Of%';
update empresa set vcs = 'v22.09.28' where codigo = 1;
-- **** v22.09.29 **** --
alter table clientes_contactos add potencial integer;
update clientes_contactos set potencial = cliente where cliente >= 10000;
update clientes_contactos set cliente = null where cliente >= 10000;
insert into log_versions (version, info) values ('v22.09.29', 'Se puede seleccionar el contacto del cliente/potencial cuando se genera una oferta');
update empresa set vcs = 'v22.09.29' where codigo = 1;
-- **** v22.10.04 **** --
alter table clientes_contactos alter column cliente drop not null;
update empresa set vcs = 'v22.10.04' where codigo = 1;
-- **** v22.10.06 **** --
insert into log_versions (version, info) values ('v22.10.06', 'Se corrige error con la fecha de alta de los clientes');
update empresa set vcs = 'v22.10.06' where codigo = 1;
-- **** v22.10.27 **** --
alter table productos add codigo_vps integer;
create table maquinas_cambio_programa (
    codigo int not null primary key,
    maquina int,
    fecha date default current_date,
    hora time default current_time,
    programa_actual integer,
    programa_nuevo integer,
    metros numeric(10,2) default 0,
    forzado smallint,
    observaciones character(255)
);
create table maquinas_cambio_bobina (
    codigo int not null primary key,
    maquina int,
    fecha date default current_date,
    hora time default current_time,
    bobina_actual integer,
    bobina_nueva integer,
    metros numeric(10,2) default 0,
    forzado smallint,
    observaciones character(255)
);
alter table programas add minimo_cambio numeric(10,2) default 0;
alter table diario_consumo add maquina_cambio_bobina integer, add maquina_cambio_programa integer;
alter table maquinas add cambio_programa_curso integer, add cambio_bobina_curso integer;
create or replace function fn_diario_consumo_insert() returns trigger
    language plpgsql
as
$$
DECLARE
    estActual SMALLINT;
    estSiguiente SMALLINT;
    codProceso INTEGER;
    canTotal DECIMAL(10,2);
    canFabricada DECIMAL(10,2);
    secProducto INTEGER;
    umProducto INTEGER;
    rCorte INTEGER;
    rFormatos INTEGER;
    rPiezas SMALLINT;
    rPiezasPendientes SMALLINT;
    rPiezasFabricadas SMALLINT;
    rLargo INTEGER;
    linTotales INTEGER;
    linTerminadas INTEGER;
    mLinBob DECIMAL(10,2);
BEGIN
    IF (NEW.metros_lineales is null or NEW.metros_lineales = 0) then
        return null;
    END IF;

    if (NEW.pedido = 'BOB.MLAJUSTE') then
        update bobinas set ml_ajuste = ml_ajuste + new.metros_lineales where codigo = new.bobina;
        return NEW;
    end if;

    -- OBTENEMOS LA SECCION Y LA UNIDAD DE MEDIDA DEL PRODUCTO
    SELECT seccion, unidad_medida INTO secProducto, umProducto FROM productos WHERE codigo = NEW.producto;
    -- OBTENEMOS LA CANTIDAD TOTAL PENDIENTE DE LA LINEA DEL PEDIDO
    IF (NEW.linea2 = 0) THEN
        SELECT cantidad_total_pendiente, estado INTO canTotal, estActual FROM pedidoslin WHERE identificador = NEW.pedido AND linea = NEW.linea;
    ELSE
        SELECT cantidad_total_pendiente, estado INTO canTotal, estActual FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
    END IF;
    -- CALCULAMOS LA CANTIDAD TOTAL CONSUMIDA
    IF (secProducto = 2 AND NEW.cantidad > 0 AND estActual = 5) THEN
        -- REMATES EN CIZALLA
        IF (NEW.linea2 = 0) THEN
            SELECT r_corte, r_formatos, r_piezas, r_largo INTO rCorte, rFormatos, rPiezas, rLargo FROM pedidoslin WHERE identificador = NEW.pedido AND linea = NEW.linea;
        ELSE
            SELECT r_corte, r_formatos, r_piezas, r_largo INTO rCorte, rFormatos, rPiezas, rLargo FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
        END IF;
        IF (rCorte = 0 AND rFormatos = 0) THEN
            canFabricada = NEW.metros_lineales;
        ELSE
            rPiezasFabricadas = ceil(rPiezas::numeric(10,3) / rFormatos) * NEW.cantidad;
            rPiezasPendientes = (canTotal / (rLargo::numeric(10,3) / 1000))::smallint;
            IF (rPiezasFabricadas > rPiezasPendientes) THEN
                rPiezasFabricadas = rPiezasPendientes;
            END IF;
            canFabricada = (rPiezasFabricadas::numeric(10,3) * (rLargo::numeric(10,3) / 1000))::numeric(10,2);
        END IF;
    ELSE
        canFabricada = NEW.metros_lineales;
    END IF;
    -- COMPROBACION DE FABRICACION TOTAL O PARCIAL Y DEPENDIENDO DE ESTO SE ACTUA
    IF (ABS(canTotal - canFabricada) <= 0.5 OR (secProducto = 1 AND canTotal - canFabricada <= 0)) THEN
        -- BUSCAMOS EL SIGUIENTE ESTADO DEL PRODUCTO
        IF (NEW.linea2 = 0) THEN
            SELECT p.codigo INTO codProceso FROM procesos p, procesos_producto pp, pedidoslin pl
            WHERE p.codigo = pp.proceso AND pp.producto = pl.producto AND pl.identificador = NEW.pedido AND pl.linea = NEW.linea
              AND pp.orden = (SELECT pp2.orden + 1 FROM procesos_producto pp2 WHERE pp2.proceso = pl.estado - 3 AND pp2.producto = pl.producto);
        ELSE
            SELECT p.codigo INTO codProceso FROM procesos p, procesos_producto pp, pedidoslin2 pl
            WHERE p.codigo = pp.proceso AND pp.producto = pl.producto AND pl.identificador = NEW.pedido AND pl.linea = NEW.linea AND pl.linea2 = NEW.linea2
              AND pp.orden = (SELECT pp2.orden + 1 FROM procesos_producto pp2 WHERE pp2.proceso = pl.estado - 3 AND pp2.producto = pl.producto);
        END IF;
        -- COMPROBAMOS CUAL ES EL NUEVO ESTADO PARA SABER SI HA TERMINADO O NO
        IF (codProceso IS NULL) THEN estSiguiente = 1;
        ELSE estSiguiente = codProceso + 3;
        END IF;
        -- ACTUALIZAMOS ESTADO DE LA LINEA DEL PEDIDO
        IF (NEW.linea2 = 0) THEN
            UPDATE pedidoslin SET cantidad_total_pendiente = cantidad_total, estado = estSiguiente, c_cantidad_pendiente = c_cantidad, o_cantidad_pendiente = o_cantidad, p_cantidad_pendiente = p_cantidad,
                                  a_modulos_pendiente = a_modulos, e_cantidad_pendiente = e_cantidad, l_cantidad_pendiente = l_cantidad, v_cantidad_pendiente = v_cantidad, f_cantidad_pendiente = f_cantidad
            WHERE identificador = NEW.pedido AND linea = NEW.linea;
            -- ACTUALIZAMOS LA CANTIDAD PENDIENTE PARA LA PLEGADORA
            IF (secProducto = 2 AND ((estActual = 5 AND estSiguiente = 6) OR (estActual = 5 AND estSiguiente = 8))) THEN
                -- DE CORTE A PLEGADO O DE CORTE A PUNZONADO
                UPDATE pedidoslin SET r_cantidad_pendiente = r_piezas WHERE identificador = NEW.pedido AND linea = NEW.linea;
            END IF;
            IF (secProducto = 2 AND estActual = 8 AND estSiguiente = 6) THEN
                -- DE PUNZONADO A PLEGADO
                SELECT SUM(cantidad)::SMALLINT INTO rPiezas FROM diario_consumo WHERE pedido = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2 AND maquina = NEW.maquina;
                UPDATE pedidoslin SET r_cantidad_pendiente = rPiezas WHERE identificador = NEW.pedido AND linea = NEW.linea;
            END IF;

            IF (secProducto = 1 AND estSiguiente = 1) THEN
                -- DELETE FROM chapa_maquinas WHERE numero IN (SELECT numero FROM pedidos WHERE identificador = ident);
                DELETE FROM fabribask_parcial WHERE pedido = NEW.pedido AND linea = NEW.linea;
                DELETE FROM gasparini_parcial WHERE pedido = NEW.pedido AND linea = NEW.linea;
                DELETE FROM reliantt_parcial WHERE pedido = NEW.pedido AND linea = NEW.linea;
            END IF;
        ELSE
            UPDATE pedidoslin2 SET cantidad_total_pendiente = cantidad_total, estado = estSiguiente, c_cantidad_pendiente = c_cantidad, o_cantidad_pendiente = o_cantidad, p_cantidad_pendiente = p_cantidad,
                                   a_modulos_pendiente = a_modulos, e_cantidad_pendiente = e_cantidad, l_cantidad_pendiente = l_cantidad, v_cantidad_pendiente = v_cantidad, f_cantidad_pendiente = f_cantidad
            WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            -- ACTUALIZAMOS LA CANTIDAD PENDIENTE PARA LA PLEGADORA
            IF (secProducto = 2 AND ((estActual = 5 AND estSiguiente = 6) OR (estActual = 5 AND estSiguiente = 8))) THEN
                -- DE CORTE A PLEGADO O DE CORTE A PUNZONADO
                UPDATE pedidoslin2 SET r_cantidad_pendiente = r_piezas WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            END IF;
            IF (secProducto = 2 AND estActual = 8 AND estSiguiente = 6) THEN
                -- DE PUNZONADO A PLEGADO
                SELECT SUM(cantidad)::SMALLINT INTO rPiezas FROM diario_consumo WHERE pedido = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2 AND maquina = NEW.maquina;
                UPDATE pedidoslin2 SET r_cantidad_pendiente = rPiezas WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            END IF;
            -- OBTENEMOS Y ACTUALIZAMOS EL ESTADO DE LA LINEA PADRE
            SELECT COUNT(*) INTO linTotales FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea;
            SELECT COUNT(*) INTO linTerminadas FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND estado = 1;
            IF (linTotales = linTerminadas) THEN
                UPDATE pedidoslin SET estado = 1 WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSE
                -- SELECT MIN(estado) INTO estSiguiente FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND estado > 3;
            END IF;
        END IF;
    ELSE
        -- ACTUALIZAMOS CANTIDADES DE LA LINEA DEL PEDIDO SEGUN LA SECCION DEL PRODUCTO
        IF (NEW.linea2 = 0) THEN
            IF (secProducto = 1) THEN
                -- CHAPA PERFILADA
                UPDATE pedidoslin SET c_cantidad_pendiente = c_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 2) THEN
                -- REMATES
                IF (rCorte = 0 AND rFormatos = 0) THEN
                    UPDATE pedidoslin SET cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea;
                else
                    UPDATE pedidoslin SET r_cantidad_pendiente = r_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea;
                end if;
            ELSEIF (secProducto = 3) THEN
                -- POLIESTER
                UPDATE pedidoslin SET p_cantidad_pendiente = p_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 4) THEN
                -- OMEGAS
                UPDATE pedidoslin SET o_cantidad_pendiente = o_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 5) THEN
                -- VENTILACION LINEAL
                UPDATE pedidoslin SET a_modulos_pendiente = a_modulos_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 6) THEN
                -- VENTANAS DE LAMAS
            ELSEIF (secProducto = 7) THEN
                -- FACHADAS
            ELSEIF (secProducto = 8) THEN
                -- OTROS
                IF (umProducto = 1) THEN
                    -- METROS CUADRADOS
                ELSEIF (umProducto = 2) THEN
                    -- METROS LINEALES
                    UPDATE pedidoslin SET v_cantidad_pendiente = v_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                    WHERE identificador = NEW.pedido AND linea = NEW.linea;
                ELSE
                    -- UNIDADES Y KILOGRAMOS
                END IF;
            ELSEIF (secProducto = 9) THEN
                -- VENTILACION PUNTUAL
            END IF;
        ELSE
            IF (secProducto = 1) THEN
                -- CHAPA PERFILADA
                UPDATE pedidoslin2 SET c_cantidad_pendiente = c_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 2) THEN
                -- REMATES
                IF (rCorte = 0 AND rFormatos = 0) THEN
                    UPDATE pedidoslin2 SET cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
                else
                    UPDATE pedidoslin2 SET r_cantidad_pendiente = r_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
                end if;
            ELSEIF (secProducto = 3) THEN
                -- POLIESTER
                UPDATE pedidoslin2 SET p_cantidad_pendiente = p_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 4) THEN
                -- OMEGAS
                UPDATE pedidoslin2 SET o_cantidad_pendiente = o_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 5) THEN
                -- VENTILACION LINEAL
                UPDATE pedidoslin2 SET a_modulos_pendiente = a_modulos_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 6) THEN
                -- VENTANAS DE LAMAS
            ELSEIF (secProducto = 7) THEN
                -- FACHADAS
            ELSEIF (secProducto = 8) THEN
                -- OTROS
                IF (umProducto = 1) THEN
                    -- METROS CUADRADOS
                ELSEIF (umProducto = 2) THEN
                    -- METROS LINEALES
                    UPDATE pedidoslin2 SET v_cantidad_pendiente = v_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                    WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
                ELSE
                    -- UNIDADES Y KILOGRAMOS
                END IF;
            ELSEIF (secProducto = 9) THEN
                -- VENTILACION PUNTUAL
            END IF;
        END IF;
    END IF;
    -- ACTUALIZAMOS LOS METROS CONSUMIDOS DE LA BOBINA USADA
    IF (NEW.consume_bobina) THEN
        IF (NEW.producto = 6) THEN mLinBob = NEW.metros_lineales / 2;
        ELSE mLinBob = NEW.metros_lineales;
        END IF;
        UPDATE bobinas SET consumo_produccion = consumo_produccion + mLinBob WHERE codigo = NEW.bobina;
    END IF;
    -- ACTUALIZAMOS METROS DEL PROGRAMA
    if (new.maquina_cambio_programa is not null) then
        update maquinas_cambio_programa set metros = metros + NEW.metros_lineales where codigo = NEW.maquina_cambio_programa;
    end if;

    RETURN NEW;
END;
$$;
insert into log_versions (version, info) VALUES ('v22.10.27', 'Se añade control de cambios de programas en chapa');
update empresa set vcs = 'v22.10.27' where codigo = 1;
-- **** v22.10.27 **** --
insert into log_versions (version, info) VALUES ('v22.10.27', 'Se pueden facturar varias ofertas de una misma oferta madre/master');
update empresa set vcs = 'v22.10.27' where codigo = 1;
-- **** v22.11.03 **** --
alter table maquinas_cambio_bobina add continua_pedido smallint;
alter table maquinas add minimo_cambio_bobina numeric(10,2);
create or replace function fn_diario_consumo_insert() returns trigger
    language plpgsql
as
$$
DECLARE
    estActual SMALLINT;
    estSiguiente SMALLINT;
    codProceso INTEGER;
    canTotal DECIMAL(10,2);
    canFabricada DECIMAL(10,2);
    secProducto INTEGER;
    umProducto INTEGER;
    rCorte INTEGER;
    rFormatos INTEGER;
    rPiezas SMALLINT;
    rPiezasPendientes SMALLINT;
    rPiezasFabricadas SMALLINT;
    rLargo INTEGER;
    linTotales INTEGER;
    linTerminadas INTEGER;
    mLinBob DECIMAL(10,2);
BEGIN
    IF (NEW.metros_lineales is null or NEW.metros_lineales = 0) then
        return null;
    END IF;

    if (NEW.pedido = 'BOB.MLAJUSTE') then
        update bobinas set ml_ajuste = ml_ajuste + new.metros_lineales where codigo = new.bobina;
        return NEW;
    end if;

    -- OBTENEMOS LA SECCION Y LA UNIDAD DE MEDIDA DEL PRODUCTO
    SELECT seccion, unidad_medida INTO secProducto, umProducto FROM productos WHERE codigo = NEW.producto;
    -- OBTENEMOS LA CANTIDAD TOTAL PENDIENTE DE LA LINEA DEL PEDIDO
    IF (NEW.linea2 = 0) THEN
        SELECT cantidad_total_pendiente, estado INTO canTotal, estActual FROM pedidoslin WHERE identificador = NEW.pedido AND linea = NEW.linea;
    ELSE
        SELECT cantidad_total_pendiente, estado INTO canTotal, estActual FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
    END IF;
    -- CALCULAMOS LA CANTIDAD TOTAL CONSUMIDA
    IF (secProducto = 2 AND NEW.cantidad > 0 AND estActual = 5) THEN
        -- REMATES EN CIZALLA
        IF (NEW.linea2 = 0) THEN
            SELECT r_corte, r_formatos, r_piezas, r_largo INTO rCorte, rFormatos, rPiezas, rLargo FROM pedidoslin WHERE identificador = NEW.pedido AND linea = NEW.linea;
        ELSE
            SELECT r_corte, r_formatos, r_piezas, r_largo INTO rCorte, rFormatos, rPiezas, rLargo FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
        END IF;
        IF (rCorte = 0 AND rFormatos = 0) THEN
            canFabricada = NEW.metros_lineales;
        ELSE
            rPiezasFabricadas = ceil(rPiezas::numeric(10,3) / rFormatos) * NEW.cantidad;
            rPiezasPendientes = (canTotal / (rLargo::numeric(10,3) / 1000))::smallint;
            IF (rPiezasFabricadas > rPiezasPendientes) THEN
                rPiezasFabricadas = rPiezasPendientes;
            END IF;
            canFabricada = (rPiezasFabricadas::numeric(10,3) * (rLargo::numeric(10,3) / 1000))::numeric(10,2);
        END IF;
    ELSE
        canFabricada = NEW.metros_lineales;
    END IF;
    -- COMPROBACION DE FABRICACION TOTAL O PARCIAL Y DEPENDIENDO DE ESTO SE ACTUA
    IF (ABS(canTotal - canFabricada) <= 0.5 OR (secProducto = 1 AND canTotal - canFabricada <= 0)) THEN
        -- BUSCAMOS EL SIGUIENTE ESTADO DEL PRODUCTO
        IF (NEW.linea2 = 0) THEN
            SELECT p.codigo INTO codProceso FROM procesos p, procesos_producto pp, pedidoslin pl
            WHERE p.codigo = pp.proceso AND pp.producto = pl.producto AND pl.identificador = NEW.pedido AND pl.linea = NEW.linea
              AND pp.orden = (SELECT pp2.orden + 1 FROM procesos_producto pp2 WHERE pp2.proceso = pl.estado - 3 AND pp2.producto = pl.producto);
        ELSE
            SELECT p.codigo INTO codProceso FROM procesos p, procesos_producto pp, pedidoslin2 pl
            WHERE p.codigo = pp.proceso AND pp.producto = pl.producto AND pl.identificador = NEW.pedido AND pl.linea = NEW.linea AND pl.linea2 = NEW.linea2
              AND pp.orden = (SELECT pp2.orden + 1 FROM procesos_producto pp2 WHERE pp2.proceso = pl.estado - 3 AND pp2.producto = pl.producto);
        END IF;
        -- COMPROBAMOS CUAL ES EL NUEVO ESTADO PARA SABER SI HA TERMINADO O NO
        IF (codProceso IS NULL) THEN estSiguiente = 1;
        ELSE estSiguiente = codProceso + 3;
        END IF;
        -- ACTUALIZAMOS ESTADO DE LA LINEA DEL PEDIDO
        IF (NEW.linea2 = 0) THEN
            UPDATE pedidoslin SET cantidad_total_pendiente = cantidad_total, estado = estSiguiente, c_cantidad_pendiente = c_cantidad, o_cantidad_pendiente = o_cantidad, p_cantidad_pendiente = p_cantidad,
                                  a_modulos_pendiente = a_modulos, e_cantidad_pendiente = e_cantidad, l_cantidad_pendiente = l_cantidad, v_cantidad_pendiente = v_cantidad, f_cantidad_pendiente = f_cantidad
            WHERE identificador = NEW.pedido AND linea = NEW.linea;
            -- ACTUALIZAMOS LA CANTIDAD PENDIENTE PARA LA PLEGADORA
            IF (secProducto = 2 AND ((estActual = 5 AND estSiguiente = 6) OR (estActual = 5 AND estSiguiente = 8))) THEN
                -- DE CORTE A PLEGADO O DE CORTE A PUNZONADO
                UPDATE pedidoslin SET r_cantidad_pendiente = r_piezas WHERE identificador = NEW.pedido AND linea = NEW.linea;
            END IF;
            IF (secProducto = 2 AND estActual = 8 AND estSiguiente = 6) THEN
                -- DE PUNZONADO A PLEGADO
                SELECT SUM(cantidad)::SMALLINT INTO rPiezas FROM diario_consumo WHERE pedido = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2 AND maquina = NEW.maquina;
                UPDATE pedidoslin SET r_cantidad_pendiente = rPiezas WHERE identificador = NEW.pedido AND linea = NEW.linea;
            END IF;

            IF (secProducto = 1 AND estSiguiente = 1) THEN
                -- DELETE FROM chapa_maquinas WHERE numero IN (SELECT numero FROM pedidos WHERE identificador = ident);
                DELETE FROM fabribask_parcial WHERE pedido = NEW.pedido AND linea = NEW.linea;
                DELETE FROM gasparini_parcial WHERE pedido = NEW.pedido AND linea = NEW.linea;
                DELETE FROM reliantt_parcial WHERE pedido = NEW.pedido AND linea = NEW.linea;
            END IF;
        ELSE
            UPDATE pedidoslin2 SET cantidad_total_pendiente = cantidad_total, estado = estSiguiente, c_cantidad_pendiente = c_cantidad, o_cantidad_pendiente = o_cantidad, p_cantidad_pendiente = p_cantidad,
                                   a_modulos_pendiente = a_modulos, e_cantidad_pendiente = e_cantidad, l_cantidad_pendiente = l_cantidad, v_cantidad_pendiente = v_cantidad, f_cantidad_pendiente = f_cantidad
            WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            -- ACTUALIZAMOS LA CANTIDAD PENDIENTE PARA LA PLEGADORA
            IF (secProducto = 2 AND ((estActual = 5 AND estSiguiente = 6) OR (estActual = 5 AND estSiguiente = 8))) THEN
                -- DE CORTE A PLEGADO O DE CORTE A PUNZONADO
                UPDATE pedidoslin2 SET r_cantidad_pendiente = r_piezas WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            END IF;
            IF (secProducto = 2 AND estActual = 8 AND estSiguiente = 6) THEN
                -- DE PUNZONADO A PLEGADO
                SELECT SUM(cantidad)::SMALLINT INTO rPiezas FROM diario_consumo WHERE pedido = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2 AND maquina = NEW.maquina;
                UPDATE pedidoslin2 SET r_cantidad_pendiente = rPiezas WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            END IF;
            -- OBTENEMOS Y ACTUALIZAMOS EL ESTADO DE LA LINEA PADRE
            SELECT COUNT(*) INTO linTotales FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea;
            SELECT COUNT(*) INTO linTerminadas FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND estado = 1;
            IF (linTotales = linTerminadas) THEN
                UPDATE pedidoslin SET estado = 1 WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSE
                -- SELECT MIN(estado) INTO estSiguiente FROM pedidoslin2 WHERE identificador = NEW.pedido AND linea = NEW.linea AND estado > 3;
            END IF;
        END IF;
    ELSE
        -- ACTUALIZAMOS CANTIDADES DE LA LINEA DEL PEDIDO SEGUN LA SECCION DEL PRODUCTO
        IF (NEW.linea2 = 0) THEN
            IF (secProducto = 1) THEN
                -- CHAPA PERFILADA
                UPDATE pedidoslin SET c_cantidad_pendiente = c_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 2) THEN
                -- REMATES
                IF (rCorte = 0 AND rFormatos = 0) THEN
                    UPDATE pedidoslin SET cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea;
                else
                    UPDATE pedidoslin SET r_cantidad_pendiente = r_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea;
                end if;
            ELSEIF (secProducto = 3) THEN
                -- POLIESTER
                UPDATE pedidoslin SET p_cantidad_pendiente = p_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 4) THEN
                -- OMEGAS
                UPDATE pedidoslin SET o_cantidad_pendiente = o_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 5) THEN
                -- VENTILACION LINEAL
                UPDATE pedidoslin SET a_modulos_pendiente = a_modulos_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea;
            ELSEIF (secProducto = 6) THEN
                -- VENTANAS DE LAMAS
            ELSEIF (secProducto = 7) THEN
                -- FACHADAS
            ELSEIF (secProducto = 8) THEN
                -- OTROS
                IF (umProducto = 1) THEN
                    -- METROS CUADRADOS
                ELSEIF (umProducto = 2) THEN
                    -- METROS LINEALES
                    UPDATE pedidoslin SET v_cantidad_pendiente = v_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                    WHERE identificador = NEW.pedido AND linea = NEW.linea;
                ELSE
                    -- UNIDADES Y KILOGRAMOS
                END IF;
            ELSEIF (secProducto = 9) THEN
                -- VENTILACION PUNTUAL
            END IF;
        ELSE
            IF (secProducto = 1) THEN
                -- CHAPA PERFILADA
                UPDATE pedidoslin2 SET c_cantidad_pendiente = c_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 2) THEN
                -- REMATES
                IF (rCorte = 0 AND rFormatos = 0) THEN
                    UPDATE pedidoslin2 SET cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
                else
                    UPDATE pedidoslin2 SET r_cantidad_pendiente = r_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
                end if;
            ELSEIF (secProducto = 3) THEN
                -- POLIESTER
                UPDATE pedidoslin2 SET p_cantidad_pendiente = p_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 4) THEN
                -- OMEGAS
                UPDATE pedidoslin2 SET o_cantidad_pendiente = o_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 5) THEN
                -- VENTILACION LINEAL
                UPDATE pedidoslin2 SET a_modulos_pendiente = a_modulos_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
            ELSEIF (secProducto = 6) THEN
                -- VENTANAS DE LAMAS
            ELSEIF (secProducto = 7) THEN
                -- FACHADAS
            ELSEIF (secProducto = 8) THEN
                -- OTROS
                IF (umProducto = 1) THEN
                    -- METROS CUADRADOS
                ELSEIF (umProducto = 2) THEN
                    -- METROS LINEALES
                    UPDATE pedidoslin2 SET v_cantidad_pendiente = v_cantidad_pendiente - NEW.cantidad, cantidad_total_pendiente = cantidad_total_pendiente - canFabricada
                    WHERE identificador = NEW.pedido AND linea = NEW.linea AND linea2 = NEW.linea2;
                ELSE
                    -- UNIDADES Y KILOGRAMOS
                END IF;
            ELSEIF (secProducto = 9) THEN
                -- VENTILACION PUNTUAL
            END IF;
        END IF;
    END IF;
    -- ACTUALIZAMOS LOS METROS CONSUMIDOS DE LA BOBINA USADA
    IF (NEW.consume_bobina) THEN
        IF (NEW.producto = 6) THEN mLinBob = NEW.metros_lineales / 2;
        ELSE mLinBob = NEW.metros_lineales;
        END IF;
        UPDATE bobinas SET consumo_produccion = consumo_produccion + mLinBob WHERE codigo = NEW.bobina;
        -- ACTUALIZAMOS METROS DE LA BOBINA PARA EL CAMBIO DE BOBINA EN CURSO
        if (new.maquina_cambio_bobina is not null) then
            update maquinas_cambio_bobina set metros = metros + mLinBob where codigo = NEW.maquina_cambio_bobina;
        end if;
    END IF;
    -- ACTUALIZAMOS METROS DEL PROGRAMA PARA EL CAMBIO DE PROGRAMA EN CURSO
    if (new.maquina_cambio_programa is not null) then
        update maquinas_cambio_programa set metros = metros + NEW.metros_lineales where codigo = NEW.maquina_cambio_programa;
    end if;

    RETURN NEW;
END;
$$;
insert into log_versions (version, info) VALUES ('v22.11.03', 'Se añade control de cambios de bobina');
update empresa set vcs = 'v22.11.03' where codigo = 1;
-- **** v22.11.03 **** --
update empresa set vcs = 'v22.11.03' where codigo = 1;
-- **** v22.11.03 **** --
update empresa set vcs = 'v22.11.03' where codigo = 1;
-- **** v22.11.03 **** --
alter table ofertaslin add especial smallint;
update ofertaslin set especial = 0 where especial is null;
alter table pedidoslin add especial smallint;
update pedidoslin set especial = 0 where especial is null;
alter table pedidoslin2 add especial smallint;
update pedidoslin2 set especial = 0 where especial is null;
drop view vw_pedidos_validados;
create view vw_pedidos_validados as
SELECT p.identificador,
       p.numero,
       p.cliente,
       p.estado                                                                  AS p_estado,
       p.fecha_entrega,
       p.fecha_produccion,
       p.provincia,
       p.portes,
       pl.seccion,
       pl.descripcion,
       pl.estado                                                                 AS pl_estado,
       pl.especial as pl_especial,
       sum(pl.cantidad_total)::numeric(10, 2)                                    AS cantidad_total,
       max(pl.c_longitud)                                                        AS max_longitud,
       count(DISTINCT pl.paquete)                                                AS paquetes,
       (sum(pl.cantidad_total / pr.metros_minuto) / 60::numeric)::numeric(10, 2) AS tiempo_estimado
FROM pedidos p,
     pedidoslin pl,
     productos pr
WHERE p.identificador = pl.identificador
  AND pl.producto = pr.codigo
GROUP BY p.fecha_entrega, p.identificador, p.numero, p.cliente, p.estado, p.fecha_produccion, pl.seccion,
         pl.descripcion, pl.estado, pl.especial
ORDER BY p.fecha_entrega, p.fecha_produccion, p.identificador;
create view vw_pedidos_validados_portes
as
SELECT p.identificador,
       p.numero,
       p.cliente,
       p.estado                                                                  AS p_estado,
       p.fecha_entrega,
       p.fecha_produccion,
       p.provincia,
       p.portes,
       pl.seccion,
       pl.descripcion,
       pl.estado                                                                 AS pl_estado,
       pol.porte                                                                 AS porte,
       sum(pl.cantidad_total)::numeric(10, 2)                                    AS cantidad_total,
       max(pl.c_longitud)                                                        AS max_longitud,
       count(DISTINCT pl.paquete)                                                AS paquetes,
       (sum(pl.cantidad_total / pr.metros_minuto) / 60::numeric)::numeric(10, 2) AS tiempo_estimado
FROM pedidos p
         LEFT JOIN porteslin pol ON p.identificador = pol.pedido,
     pedidoslin pl,
     productos pr
WHERE p.identificador = pl.identificador
  AND pl.producto = pr.codigo
GROUP BY p.fecha_entrega, p.identificador, p.numero, p.cliente, p.estado, p.fecha_produccion, pl.seccion,
         pl.descripcion, pl.estado, pol.porte
ORDER BY p.fecha_entrega, p.fecha_produccion, p.identificador;update empresa set vcs = 'v22.11.03' where codigo = 1;
-- **** v22.11.09 **** --
insert into log_versions (version, info) values ('v22.11.09', 'Se corrige KPI de producción');
update empresa set vcs = 'v22.11.09' where codigo = 1;
-- **** v22.11.10 **** --
insert into menus_items (menu, item, nombre, comando, separador, orden, item_padre, ddisabled, nivel_usuario_minimo)
VALUES (1, (select coalesce(max(item), 0) + 1 from menus_items where menu = 1), 'Cambios de Programas', 'MCambioProgramas', 0, (select coalesce(max(orden), 0) + 1 from menus_items where menu = 1 and item_padre = 7), 7, 0, 7);
insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso)
values ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), 0, (select max(item) from menus_items where menu = 1), 7, 2);
do $$
    declare
        ucodigo smallint;
    begin
        for ucodigo in select codigo from wusuario where estado = 1 and codigo > 0 and codigo < 90
            loop
                insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso) VALUES
                    ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), ucodigo, (select max(item) from menus_items where menu = 1), 7, 0);
            end loop;
    end;
$$;
insert into menus_items (menu, item, nombre, comando, separador, orden, item_padre, ddisabled, nivel_usuario_minimo)
VALUES (1, (select coalesce(max(item), 0) + 1 from menus_items where menu = 1), 'Cambios de Bobinas', 'MCambioBobinas', 0, (select coalesce(max(orden), 0) + 1 from menus_items where menu = 1 and item_padre = 7), 7, 0, 7);
insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso)
values ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), 0, (select max(item) from menus_items where menu = 1), 7, 2);
do $$
    declare
        ucodigo smallint;
    begin
        for ucodigo in select codigo from wusuario where estado = 1 and codigo > 0 and codigo < 90
            loop
                insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso) VALUES
                    ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), ucodigo, (select max(item) from menus_items where menu = 1), 7, 0);
            end loop;
    end;
$$;
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (9, 'CAMBIO BOBINA', 'SE ENVIA ESTE EMAIL PARA NOTIFICAR QUE SE HA PRODUCIDO UN CAMBIO DE BOBINA FORZADA', 0, 1, 1, 0, 'Cambio de Bobina', '<p>Se ha producido un cambio de bobina de manera forzada</p><p>Código de cambio: $maquinas_cambio_bobina.codigo$</p>', 'produccion@incoperfil.com;joseginer@incoperfil.com', null);
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (10, 'CAMBIO PROGRAMA', 'SE ENVIA ESTE EMAIL PARA NOTIFICAR QUE SE HA PRODUCIDO UN CAMBIO DE PROGRAMA FORZADO', 0, 1, 1, 0, 'Cambio de Programa', '<p>Se ha producido un cambio de programa de manera forzado</p><p>Código de cambio: $maquinas_cambio_programa.codigo$</p>', 'produccion@incoperfil.com;joseginer@incoperfil.com', null);
update empresa set vcs = 'v22.11.10' where codigo = 1;
-- **** v22.11.10 **** --
insert into log_versions (version, info) values ('v22.11.10', 'Se añade un email de aviso cuando se realizan cambios de bobina o programa');
update empresa set vcs = 'v22.11.10' where codigo = 1;
-- **** v22.11.10 **** --
update empresa set vcs = 'v22.11.10' where codigo = 1;
-- **** v22.11.10 **** --
update empresa set vcs = 'v22.11.10' where codigo = 1;
-- **** v22.11.10 **** --
update empresa set vcs = 'v22.11.10' where codigo = 1;
-- **** v22.11.16 **** --
insert into log_versions (version, info) values ('v22.11.16', 'Se corrige el proceso del control de cambio de bobinas');
update empresa set vcs = 'v22.11.16' where codigo = 1;
-- **** v22.11.24 **** --
insert into log_versions (version, info) values ('v22.11.24', 'Se corrige el control de cambio de bobina/programa');
update empresa set vcs = 'v22.11.24' where codigo = 1;
-- **** v22.12.07 **** --
alter table pedidoslin_curvado
    add angulo_bp_real numeric(10,2),
    add altura_bp_ajustada_real numeric(10,2),
    add distancia_ts_real numeric(10,2),
    add altura_ts_ajustada_real numeric(10,2),
    add presion_embrague_real numeric(10,2),
    add distancia_cr_ajustada_real numeric(10,2),
    add h1_ajustada_real numeric(10,2),
    add h2_ajustada_real numeric(10,2),
    add h3_ajustada_real numeric(10,2),
    add h4_ajustada_real numeric(10,2),
    add h5_ajustada_real numeric(10,2),
    add h6_ajustada_real numeric(10,2);
alter table pedidoslin_curvado
    add hbf_numero_real integer,
    add hbf_numero_ext_real character(15),
    add angulo_cr_real integer;
drop view vw_pedidoslin_curvadora_ch_tr;
create view vw_pedidoslin_curvadora_ch_tr
as
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       pl.c_posicion               AS posicion,
       pl.c_curvado                AS curvado,
       pl.c_perforado              AS perforado,
       pl.c_cantidad_pendiente     AS uds,
       pl.c_longitud               AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.c_radio,
       pl.c_flecha,
       pc.c_entrada,
       pc.c_salida,
       pc.entrada,
       pc.proundidad,
       pc.distancia_prensados,
       pc.numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.c_flecha::numeric(10, 2) AS flecha,
       pl.c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 1
  AND (pl.producto IN (SELECT DISTINCT procesos_producto.producto
                       FROM procesos_producto
                       WHERE procesos_producto.maquina = ANY (ARRAY [5, 40])))
UNION ALL
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       0::smallint                 AS posicion,
       pl.p_curvado                AS curvado,
       0                           AS perforado,
       pl.p_cantidad_pendiente     AS uds,
       pl.p_longitud               AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.p_radio                  AS c_radio,
       pl.p_flecha                 AS c_flecha,
       pc.c_entrada,
       pc.c_salida,
       pc.entrada,
       pc.proundidad,
       pc.distancia_prensados,
       pc.numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.p_flecha::numeric(10, 2) AS flecha,
       pl.p_cuerda                 AS c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 3
ORDER BY 16, 15 DESC;
create or replace function fn_pedidoslin_curvado_before_insert_update() returns trigger
    language plpgsql
as
$$
begin
    if (new.hbf_numero > 15) then
        new.hbf_numero_ext := 'Ext. + ' || (new.hbf_numero - 10);
    else
        new.hbf_numero_ext := '' || new.hbf_numero;
    end if;

    if (new.hbf_numero_ext_real is null or new.hbf_numero_ext_real = '') then
        new.hbf_numero_ext_real := new.hbf_numero_ext;
    end if;

    return new;
end
$$;
alter table pedidos add validado_fabricacion smallint;
drop view vw_pedidos_validados;
create view vw_pedidos_validados as
SELECT p.identificador,
       p.numero,
       p.cliente,
       p.estado                                                                  AS p_estado,
       p.fecha_entrega,
       p.fecha_produccion,
       p.provincia,
       p.portes,
       p.validado_fabricacion,
       pl.seccion,
       pl.descripcion,
       pl.estado                                                                 AS pl_estado,
       pl.especial                                                               AS pl_especial,
       sum(pl.cantidad_total)::numeric(10, 2)                                    AS cantidad_total,
       max(pl.c_longitud)                                                        AS max_longitud,
       count(DISTINCT pl.paquete)                                                AS paquetes,
       (sum(pl.cantidad_total / pr.metros_minuto) / 60::numeric)::numeric(10, 2) AS tiempo_estimado
FROM pedidos p,
     pedidoslin pl,
     productos pr
WHERE p.identificador = pl.identificador
  AND pl.producto = pr.codigo
GROUP BY p.fecha_entrega, p.identificador, p.numero, p.cliente, p.estado, p.fecha_produccion, pl.seccion,
         pl.descripcion, pl.estado, pl.especial
ORDER BY p.fecha_entrega, p.fecha_produccion, p.identificador;
insert into log_versions (version, info) values ('v22.12.07', 'Se pueden ajustar los parámetros de la curvadora TAM');
insert into log_versions (version, info) values ('v22.12.07', 'Los pedidos solo se podrán pasar a las máquinas cuando estén con el pago validado y se haya validado para fabricación');
update empresa set vcs = 'v22.12.07' where codigo = 1;
-- **** v22.12.07 **** --
update empresa set vcs = 'v22.12.07' where codigo = 1;
-- **** v22.12.14 **** --
insert into log_versions (version, info) values ('v22.12.14', 'Se añade botón para desvalidar la fabricación de un pedido');
insert into log_versions (version, info) values ('v22.12.14', 'Se modifica el listado del PMP de Chapa');
update empresa set vcs = 'v22.12.14' where codigo = 1;
-- **** v22.12.14 **** --
update empresa set vcs = 'v22.12.14' where codigo = 1;
-- **** v22.12.14 **** --
update empresa set vcs = 'v22.12.14' where codigo = 1;
-- **** v22.12.15 **** --
update empresa set vcs = 'v22.12.15' where codigo = 1;
-- **** v22.12.21 **** --
update productos set producto_equivalente = 1 where codigo = 30;
update productos set producto_equivalente = 5 where codigo = 35;
update productos set producto_equivalente = 4 where codigo = 38;
update productos set producto_equivalente = 2 where codigo = 39;
update productos set producto_equivalente = 139 where codigo = 279;
update productos set producto_equivalente = 140 where codigo = 280;
update perforaciones perf
set producto = coalesce((select coalesce(p.producto_equivalente, p.codigo) from productos p where p.codigo = perf.producto), 0);
update bobinas set perforacion = 21 where perforacion = 22;
update bobinas set perforacion = 17 where perforacion = 23;
update bobinas set perforacion = 10 where perforacion = 27;
update bobinas set perforacion = 12 where perforacion = 28;
update perforaciones set estado = 0 where codigo = 22;
update perforaciones set estado = 0 where codigo = 23;
update perforaciones set estado = 0 where codigo = 27;
update perforaciones set estado = 0 where codigo = 28;
insert into log_versions (version, info) values ('v22.12.21', 'Se modifican las perforaciones para simplificar duplicidades');
insert into log_versions (version, info) values ('v22.12.21', 'Se modifican añaden listados a los cambios de bobinas');
update empresa set vcs = 'v22.12.21' where codigo = 1;
-- **** v22.12.22 **** --
alter table pedidoslin_curvado add aire_salida_bp_real numeric(10,2), add altura_ts_chapa01_real numeric(10,2);
alter table pedidoslin_curvado add proundidad_real numeric(10,2), add distancia_prensados_real numeric(10,2);
create or replace view vw_pedidoslin_curvadora_ch_tr
            (identificador, fecha_entrega, producto, espesor, calidad, recubrimiento, pintura, ccolor, pinturab,
             ccolorb, posicion, curvado, perforado, uds, longitud, paquete, cantidad_total_pendiente, linea,
             referencia_plano, estado, c_radio, c_flecha, c_entrada, c_salida, entrada, proundidad, distancia_prensados,
             numero_prensados, flecha_muestra, observaciones, desviacion, profundidad_ajuste, flecha, c_cuerda, hbf,
             hbf_numero_real, angulo_cr_real, hbf_numero_ext_real, angulo_bp_real, altura_bp_ajustada_real,
             distancia_ts_real, altura_ts_ajustada_real, presion_embrague_real, distancia_cr_ajustada_real,
             h1_ajustada_real, h2_ajustada_real, h3_ajustada_real, h4_ajustada_real, h5_ajustada_real, h6_ajustada_real)
as
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       pl.c_posicion               AS posicion,
       pl.c_curvado                AS curvado,
       pl.c_perforado              AS perforado,
       pl.c_cantidad_pendiente     AS uds,
       pl.c_longitud               AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.c_radio,
       pl.c_flecha,
       pc.c_entrada,
       pc.c_salida,
       pc.entrada,
       pc.proundidad_real          as proundidad,
       pc.distancia_prensados_real as distancia_prensados,
       pc.numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.c_flecha::numeric(10, 2) AS flecha,
       pl.c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 1
  AND (pl.producto IN (SELECT DISTINCT procesos_producto.producto
                       FROM procesos_producto
                       WHERE procesos_producto.maquina = ANY (ARRAY [5, 40])))
UNION ALL
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       0::smallint                 AS posicion,
       pl.p_curvado                AS curvado,
       0                           AS perforado,
       pl.p_cantidad_pendiente     AS uds,
       pl.p_longitud               AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.p_radio                  AS c_radio,
       pl.p_flecha                 AS c_flecha,
       pc.c_entrada,
       pc.c_salida,
       pc.entrada,
       pc.proundidad_real          as proundidad,
       pc.distancia_prensados_real as distancia_prensados,
       pc.numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.p_flecha::numeric(10, 2) AS flecha,
       pl.p_cuerda                 AS c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 3
ORDER BY 16, 15 DESC;
update pedidoslin_curvado set proundidad_real = proundidad where proundidad_real is null;
update pedidoslin_curvado set distancia_prensados_real = distancia_prensados where pedidoslin_curvado.distancia_prensados_real is null;
insert into log_versions (version, info) values ('v22.12.22', 'Se añaden campos nuevos para el proceso de curvado');
insert into log_versions (version, info) values ('v22.12.22', 'Se añaden listados para los cambios de programas');
update empresa set vcs = 'v22.12.22' where codigo = 1;
-- **** v23.01.04 **** --
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 2', 'Hbj_pos2', 'mm', 533);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 3', 'Hbj_pos3', 'mm', 583);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 4', 'Hbj_pos4', 'mm', 633);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 5', 'Hbj_pos5', 'mm', 683);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 6', 'Hbj_pos6', 'mm', 733);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 7', 'Hbj_pos7', 'mm', 783);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 8', 'Hbj_pos8', 'mm', 833);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 9', 'Hbj_pos9', 'mm', 883);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 10', 'Hbj_pos10', 'mm', 933);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 11', 'Hbj_pos11', 'mm', 983);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 12', 'Hbj_pos12', 'mm', 1083);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 13', 'Hbj_pos13', 'mm', 1133);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 14', 'Hbj_pos14', 'mm', 1183);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 15', 'Hbj_pos15', 'mm', 1233);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 16', 'Hbj_pos16', 'mm', 1283);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 17', 'Hbj_pos17', 'mm', 1333);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 18', 'Hbj_pos18', 'mm', 1383);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 19', 'Hbj_pos19', 'mm', 1433);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 20', 'Hbj_pos20', 'mm', 1483);
insert into variables_maquinas (maquina, linea, descripcion, nombre_var, um, valor) values (40, (select max(linea) + 1 from variables_maquinas where maquina = 40), 'AL Altura brazo fijo posición 21', 'Hbj_pos21', 'mm', 1533);
insert into log_versions (version, info) values ('v23.01.04', 'Se modifica el cálculo de curvado para TAM');
update empresa set vcs = 'v23.01.04' where codigo = 1;
-- **** v23.01.04 **** --
update empresa set vcs = 'v23.01.04' where codigo = 1;
-- **** v23.01.04 **** --
update empresa set vcs = 'v23.01.04' where codigo = 1;
-- **** v23.01.05 **** --
insert into log_versions (version, info) values ('v23.01.05', 'Se añade información en la pantalla de la curvadora TAM');
update empresa set vcs = 'v23.01.05' where codigo = 1;
-- **** v23.01.05 **** --
update empresa set vcs = 'v23.01.05' where codigo = 1;
-- **** v23.01.05 **** --
update empresa set vcs = 'v23.01.05' where codigo = 1;
-- **** v23.01.05 **** --
update empresa set vcs = 'v23.01.05' where codigo = 1;
-- **** v23.01.05 **** --
insert into log_versions (version, info) values ('v23.01.05', 'Se corrige el generador de informes de cambios de bobinas/programas');
update empresa set vcs = 'v23.01.05' where codigo = 1;
-- **** v23.01.05 **** --
update empresa set vcs = 'v23.01.05' where codigo = 1;
-- **** v23.01.11 **** --
alter table pedidoslin_curvado add numero_prensados_real numeric(10,2);
update pedidoslin_curvado set numero_prensados_real = numero_prensados where numero_prensados_real is null;
update pedidoslin_curvado set proundidad_real = proundidad where proundidad_real is null;
update pedidoslin_curvado set distancia_prensados_real = distancia_prensados where distancia_prensados_real is null;
create or replace view vw_pedidoslin_curvadora_ch_tr
            (identificador, fecha_entrega, producto, espesor, calidad, recubrimiento, pintura, ccolor, pinturab,
             ccolorb, posicion, curvado, perforado, uds, longitud, paquete, cantidad_total_pendiente, linea,
             referencia_plano, estado, c_radio, c_flecha, c_entrada, c_salida, entrada, proundidad, distancia_prensados,
             numero_prensados, flecha_muestra, observaciones, desviacion, profundidad_ajuste, flecha, c_cuerda, hbf,
             hbf_numero_real, angulo_cr_real, hbf_numero_ext_real, angulo_bp_real, altura_bp_ajustada_real,
             distancia_ts_real, altura_ts_ajustada_real, presion_embrague_real, distancia_cr_ajustada_real,
             h1_ajustada_real, h2_ajustada_real, h3_ajustada_real, h4_ajustada_real, h5_ajustada_real, h6_ajustada_real)
as
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       pl.c_posicion               AS posicion,
       pl.c_curvado                AS curvado,
       pl.c_perforado              AS perforado,
       pl.c_cantidad_pendiente     AS uds,
       pl.c_longitud               AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.c_radio,
       pl.c_flecha,
       pc.c_entrada,
       pc.c_salida,
       pc.entrada,
       pc.proundidad_real          AS proundidad,
       pc.distancia_prensados_real AS distancia_prensados,
       pc.numero_prensados_real    as numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.c_flecha::numeric(10, 2) AS flecha,
       pl.c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 1
  AND (pl.producto IN (SELECT DISTINCT procesos_producto.producto
                       FROM procesos_producto
                       WHERE procesos_producto.maquina = ANY (ARRAY [5, 40])))
UNION ALL
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       0::smallint                 AS posicion,
       pl.p_curvado                AS curvado,
       0                           AS perforado,
       pl.p_cantidad_pendiente     AS uds,
       pl.p_longitud               AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.p_radio                  AS c_radio,
       pl.p_flecha                 AS c_flecha,
       pc.c_entrada,
       pc.c_salida,
       pc.entrada,
       pc.proundidad_real          AS proundidad,
       pc.distancia_prensados_real AS distancia_prensados,
       pc.numero_prensados_real    as numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.p_flecha::numeric(10, 2) AS flecha,
       pl.p_cuerda                 AS c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 3
ORDER BY 16, 15 DESC;
update empresa set vcs = 'v23.01.11' where codigo = 1;
-- **** v23.01.11 **** --
update empresa set vcs = 'v23.01.11' where codigo = 1;
-- **** v23.01.19 **** --
insert into log_versions (version, info) values ('v23.01.19', 'Se añade aviso en pedidos de bobinas, la posibilidad de volver a firmar un documento y se puede dividir una linea en varias');
update empresa set vcs = 'v23.01.19' where codigo = 1;
-- **** v23.01.19 **** --
update empresa set vcs = 'v23.01.19' where codigo = 1;
-- **** v23.02.09 **** --
create table plasma
(
    pedido        char(15) not null,
    pedido_numero integer,
    maquina       integer  not null,
    c_cantidad    integer,
    c_metros      numeric(10, 2),
    constraint plasma_pk
        primary key (pedido, maquina)
);
insert into log_versions (version, info) VALUES ('v23.02.09', 'Se añade tabla para comunicarse con el sistema PLASMA');
update empresa set vcs = 'v23.02.09' where codigo = 1;
-- **** v23.02.09 **** --
create table secciones_estadisticas
(
    codigo integer not null primary key ,
    nombre character(127)
);
alter table productos add seccion_estadistica integer;
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (1, 'Accesorios');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (2, 'Fachadas Arquitectónicas');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (3, 'Forjados Colaborantes');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (4, 'Forjados Colaborantes Nuevos');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (5, 'Perfiles Curvados');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (6, 'Perfiles Curvados Nuevos');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (7, 'Perfiles Estándar');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (8, 'Perfiles Nuevos');
INSERT INTO secciones_estadisticas (codigo, nombre) VALUES (9, 'Rematería');
insert into log_versions (version, info) values ('v23.02.09', 'Se añaden secciones para estadísticas');
update empresa set vcs = 'v23.02.09' where codigo = 1;
-- **** v23.02.23 **** --
insert into log_versions (version, info) values ('v23.02.23', 'Se añade la conexión con plasma');
update empresa set vcs = 'v23.02.23' where codigo = 1;
-- **** v23.02.23 **** --
update empresa set vcs = 'v23.02.23' where codigo = 1;
-- **** v23.03.08 **** --
alter table albaranes add cliente_as_transportista smallint;
update albaranes set cliente_as_transportista = 1, transportista = null where cliente = transportista;
update albaranes set cliente_as_transportista = 0 where cliente_as_transportista is null;
insert into log_versions (version, info) values ('v23.03.08', 'Se corrige el proceso para firmar pedidos de bobinas');
insert into log_versions (version, info) values ('v23.03.08', 'Se añade un campo en albaranes para controlar cuando el clientes es el transportista');
update empresa set vcs = 'v23.03.08' where codigo = 1;
-- **** v23.03.15 **** --
update empresa set vcs = 'v23.03.15' where codigo = 1;
-- **** v23.03.15 **** --
update empresa set vcs = 'v23.03.15' where codigo = 1;
-- **** v23.04.12 **** --
alter table albaranes add encuesta_realizada smallint default 0;
update albaranes set encuesta_realizada = 1 where fecha <= '2022-12-31';
INSERT INTO comunicaciones (codigo, nombre, descripcion, tipo, estado, email_store, email_from, email_subject, email_body, email_to, email_cc_interna) VALUES (11, 'ENCUESTA CALIDAD', null, 1, 1, 0, 0, 'Encuesta de calidad', '<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScL-QsW83hD1lHLNlsdvLZIhlm8ZIGSLYmqI9nqvtQuvTa3QQ/viewform?embedded=true" width="640" height="657" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>', '$de.email$', null);
update empresa set vcs = 'v23.04.12' where codigo = 1;
-- **** v23.04.12 **** --
update empresa set vcs = 'v23.04.12' where codigo = 1;
-- **** v23.04.12 **** --
update empresa set vcs = 'v23.04.12' where codigo = 1;
-- **** v23.04.12 **** --
update empresa set vcs = 'v23.04.12' where codigo = 1;
-- **** v23.04.12 **** --
update empresa set vcs = 'v23.04.12' where codigo = 1;
-- **** v23.04.13 **** --
insert into log_versions (version, info) VALUES ('v23.04.12', 'Se ajusta el traspaso de información a la BD de Plasma');
insert into log_versions (version, info) VALUES ('v23.04.13', 'Se completa el proceso de envío de encuestas (solo falta configurar la tarea programada');
update empresa set vcs = 'v23.04.13' where codigo = 1;
-- **** v23.04.20 **** --
alter table comunicaciones add subtipo smallint;
update comunicaciones set subtipo = 0 where codigo = 5;
update comunicaciones set subtipo = 1 where codigo in (6,7);
update comunicaciones set subtipo = 2 where codigo = 11;
insert into log_versions (version, info) values ('v23.04.20', 'Se añade un subtipo a las comunicaciones de los clientes');
delete from cli_contactos_comunicaciones where comunicacion = 11;
insert into cli_contactos_comunicaciones (contacto, comunicacion)
    select cc.codigo, 11
    from clientes_contactos cc
             left join ofertas o on cc.email = o.mail
    where cc.email != ''
      and cc.email is not null
      and cc.email not like '%@incoperfil.com'
      and cc.cliente is not null
      and o.mail != 'x'
      and o.mail is not null
    group by 1, 2;
delete
from cli_contactos_comunicaciones
where comunicacion = 11
  and contacto in (select max(cc.codigo)
                   from clientes_contactos cc,
                        cli_contactos_comunicaciones ccc
                   where ccc.comunicacion = 11
                     and cc.codigo = ccc.contacto
                   group by cc.cliente, cc.email
                   having count(ccc.*) > 1);
insert into log_versions (version, info) values ('v23.04.20', 'Se relacionan todos los contactos de clientes que han realizado ofertas con la comunicación de encuesta');
update empresa set vcs = 'v23.04.20' where codigo = 1;
-- **** v23.04.20 **** --
update empresa set vcs = 'v23.04.20' where codigo = 1;
-- **** v23.04.20 **** --
insert into log_versions (version, info) values ('v23.04.20', 'Se permite vaciar máquina con paquetes a mitad');
update empresa set vcs = 'v23.04.20' where codigo = 1;
-- **** v23.04.26 **** --
insert into log_versions (version, info) values ('v23.04.26', 'Se modifica la tarea programada de las encuestas');
update empresa set vcs = 'v23.04.26' where codigo = 1;
-- **** v23.05.18 **** --
insert into log_versions (version, info) values ('v23.05.18', 'Se corrige el error de firmado de pedidos de bobinas');
update empresa set vcs = 'v23.05.18' where codigo = 1;
-- **** v23.05.18 **** --
update empresa set vcs = 'v23.05.18' where codigo = 1;
-- **** v23.05.18 **** --
insert into log_versions (version, info) values ('v23.05.18', 'Se permite exportar a CSV los efectos no remesados');
update empresa set vcs = 'v23.05.18' where codigo = 1;
-- **** v23.05.24 **** --
create table comunicaciones_envios
(
    codigo       serial primary key,
    comunicacion integer,
    grupo_envio  integer,
    token        varchar(255),
    cliente      integer,
    fecha        date default ('now'::text)::date                not null,
    hora         time default ('now'::text)::time with time zone not null,
    albaran      char(15)
);
update empresa set vcs = 'v23.05.24' where codigo = 1;
-- **** v23.05.24 **** --
insert into log_versions(version, info) values ('v23.05.24', 'Encuestas de calidad a través del VPS');
update empresa set vcs = 'v23.05.24' where codigo = 1;
-- **** v23.05.29 **** --
insert into log_versions (version, info) values ('@verison@', 'Se corrige el traductor para evitar problemas en la generación de PDFs');
update empresa set vcs = 'v23.05.29' where codigo = 1;
-- **** v23.06.01 **** --
create table comenvrespuestas (
    codigo serial primary key,
    comunicacion_envio int,
    fecha date,
    hora date,
    respuesta_01_recomendacion smallint,
    respuesta_01_comentario varchar(2047)
);
insert into log_versions (version, info) values ('v23.06.01', 'Se añade sincronización de las respuestas');
update empresa set vcs = 'v23.06.01' where codigo = 1;
-- **** v23.06.08 **** --
insert into log_versions (version, info) values ('v23.06.08', 'Se añade la posibilidad de sacar CSV en los listados de remesas');
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
insert into log_versions (version, info) values ('v23.06.08', 'Se cambia la subcuenta de ventas para la contabilización de facturas de chatarra');
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.08 **** --
update empresa set vcs = 'v23.06.08' where codigo = 1;
-- **** v23.06.22 **** --
alter table ofertas add aplicar_recargos smallint;
update ofertas set aplicar_recargos = 0 where aplicar_recargos is null;
drop table comenvrespuestas;
create table comenvrespuestas (
    codigo serial primary key,
    comunicacion_envio int,
    fecha date,
    hora time,
    respuesta_01_recomendacion smallint,
    respuesta_01_comentario varchar(2047)
);
-- **** v23.06.22 **** --
update empresa set vcs = 'v23.06.22' where codigo = 1;
-- **** v23.06.22 **** --
update empresa set vcs = 'v23.06.22' where codigo = 1;
-- **** v23.06.28 **** --
insert into log_versions (version, info) values ('v23.06.28', 'Se corrige el PDF de ofertas a consecuencia de los recargos');
update empresa set vcs = 'v23.06.28' where codigo = 1;
-- **** v23.06.29 **** --
alter table pedidoslin_curvado
    add abp numeric(10,2),
    add altura_brazo_neumatico_real numeric(10,2),
    add abp_real numeric(10,2);
alter table pedidoslin_curvado
    add c_entrada_manual numeric(10,2),
    add proundidad_manual numeric(10,2),
    add distancia_prensados_manual numeric(10,2),
    add numero_prensados_manual numeric(10,2);
create or replace view vw_pedidoslin_curvadora_ch_tr
            (identificador, fecha_entrega, producto, espesor, calidad, recubrimiento, pintura, ccolor, pinturab,
             ccolorb, posicion, curvado, perforado, uds, longitud, paquete, cantidad_total_pendiente, linea,
             referencia_plano, estado, c_radio, c_flecha, c_entrada, c_salida, entrada, proundidad, distancia_prensados,
             numero_prensados, flecha_muestra, observaciones, desviacion, profundidad_ajuste, flecha, c_cuerda, hbf,
             hbf_numero_real, angulo_cr_real, hbf_numero_ext_real, angulo_bp_real, altura_bp_ajustada_real,
             distancia_ts_real, altura_ts_ajustada_real, presion_embrague_real, distancia_cr_ajustada_real,
             h1_ajustada_real, h2_ajustada_real, h3_ajustada_real, h4_ajustada_real, h5_ajustada_real, h6_ajustada_real)
as
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       pl.c_posicion                                                        AS posicion,
       pl.c_curvado                                                         AS curvado,
       pl.c_perforado                                                       AS perforado,
       pl.c_cantidad_pendiente                                              AS uds,
       pl.c_longitud                                                        AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.c_radio,
       pl.c_flecha,
       coalesce(pc.c_entrada_manual, pc.c_entrada)                          as c_entrada,
       pc.c_salida,
       pc.entrada,
       coalesce(pc.proundidad_manual, pc.proundidad_real)                   AS proundidad,
       coalesce(pc.distancia_prensados_manual, pc.distancia_prensados_real) AS distancia_prensados,
       coalesce(pc.numero_prensados_manual, pc.numero_prensados_real)       AS numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.c_flecha::numeric(10, 2)                                          AS flecha,
       pl.c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 1
  AND (pl.producto IN (SELECT DISTINCT procesos_producto.producto
                       FROM procesos_producto
                       WHERE procesos_producto.maquina = ANY (ARRAY [5, 40])))
UNION ALL
SELECT pl.identificador,
       pl.fecha_entrega,
       pl.producto,
       pl.espesor,
       pl.calidad,
       pl.recubrimiento,
       pl.pintura,
       pl.ccolor,
       pl.pinturab,
       pl.ccolorb,
       0::smallint                                                          AS posicion,
       pl.p_curvado                                                         AS curvado,
       0                                                                    AS perforado,
       pl.p_cantidad_pendiente                                              AS uds,
       pl.p_longitud                                                        AS longitud,
       pl.paquete,
       pl.cantidad_total_pendiente,
       pl.linea,
       pl.referencia_plano,
       pl.estado,
       pl.p_radio                                                           AS c_radio,
       pl.p_flecha                                                          AS c_flecha,
       coalesce(pc.c_entrada_manual, pc.c_entrada)                          as c_entrada,
       pc.c_salida,
       pc.entrada,
       coalesce(pc.proundidad_manual, pc.proundidad_real)                   AS proundidad,
       coalesce(pc.distancia_prensados_manual, pc.distancia_prensados_real) AS distancia_prensados,
       coalesce(pc.numero_prensados_manual, pc.numero_prensados_real)       AS numero_prensados,
       pc.flecha_muestra,
       pc.observaciones,
       vc.desviacion,
       vc.profundidad_ajuste,
       pl.p_flecha::numeric(10, 2)                                          AS flecha,
       pl.p_cuerda                                                          AS c_cuerda,
       pc.hbf,
       pc.hbf_numero_real,
       pc.angulo_cr_real,
       pc.hbf_numero_ext_real,
       pc.angulo_bp_real,
       pc.altura_bp_ajustada_real,
       pc.distancia_ts_real,
       pc.altura_ts_ajustada_real,
       pc.presion_embrague_real,
       pc.distancia_cr_ajustada_real,
       pc.h1_ajustada_real,
       pc.h2_ajustada_real,
       pc.h3_ajustada_real,
       pc.h4_ajustada_real,
       pc.h5_ajustada_real,
       pc.h6_ajustada_real
FROM pedidoslin pl
         LEFT JOIN pedidoslin_curvado pc ON pl.identificador = pc.identificador AND pl.linea = pc.linea
         LEFT JOIN LATERAL fn_verificaciones_curvadora_pedln(pl.identificador, pl.linea) vc(pedido, linea, desviacion, profundidad_ajuste, flecha)
                   ON pl.identificador = vc.pedido AND pl.linea = vc.linea
WHERE pl.seccion = 3
ORDER BY 16, 15 DESC;
comment on column vw_pedidoslin_curvadora_ch_tr.estado is '0: Pendiente; 1: Terminado; 2: Servido; 3: Cancelado; [4..X]: Todos los procesos';
insert into log_versions (version, info) values ('v23.06.29', 'Se añaden nuevos campos para el curvado');
update empresa set vcs = 'v23.06.29' where codigo = 1;
-- **** v23.07.06 **** --
update empresa set vcs = 'v23.07.06' where codigo = 1;
-- **** v23.07.06 **** --
create table productos_dimmaxs (
    codigo int not null primary key ,
    producto int not null,
    espesor numeric(4,2),
    longitud integer
);
insert into log_versions (version, info) values ('v23.07.06', 'Se añaden dimensiones máximas por espesores a los productos');
update empresa set vcs = 'v23.07.06' where codigo = 1;
-- **** v23.07.12 **** --
insert into log_versions ('v23.07.12', 'Se habilita el PMP de Chapa en la pantalla de taller');
update empresa set vcs = 'v23.07.12' where codigo = 1;
-- **** v23.07.13 **** --
insert into log_versions (version, info) values ('v23.07.13', 'Se modifica la comunicación de calidad para que no aparezca ninguna referencia a cuestionario o encuesta');
insert into log_versions (version, info) values ('v23.07.13', 'Se cambian los recargos de embalaje en el pdf de las ofertas');
update empresa set vcs = 'v23.07.13' where codigo = 1;
-- **** v23.07.13 **** --
insert into log_versions (version, info) values ('v23.07.13', 'Se pueden visualizar los avisos desde el PMP');
update empresa set vcs = 'v23.07.13' where codigo = 1;
-- **** v23.07.13 **** --
update empresa set vcs = 'v23.07.13' where codigo = 1;
-- **** v23.07.13 **** --
update empresa set vcs = 'v23.07.13' where codigo = 1;
-- **** v23.07.20 **** --
update empresa set vcs = 'v23.07.20' where codigo = 1;
-- **** v23.07.20 **** --
update empresa set vcs = 'v23.07.20' where codigo = 1;
-- **** v23.07.20 **** --
update empresa set vcs = 'v23.07.20' where codigo = 1;
-- **** v23.07.24 **** --
update empresa set vcs = 'v23.07.24' where codigo = 1;
-- **** v23.09.07 **** --
create table proveedores_tipos (
    codigo integer not null primary key,
    nombre varchar(63) not null
);
alter table proveedores
    add tipo_proveedor integer,
    add constraint fk_proveedores_tipos foreign key (tipo_proveedor) references proveedores_tipos(codigo);
insert into log_versions (version, info) values ('v23.09.07', 'Se añade una clasificación para los proveedores');
insert into menus_items (menu, item, nombre, comando, separador, orden, item_padre, ddisabled, nivel_usuario_minimo)
VALUES (1, (select coalesce(max(item), 0) + 1 from menus_items where menu = 1), 'Tipos de Proveedor', 'MProveedoresTipos', 0, (select coalesce(max(orden), 0) + 1 from menus_items where menu = 1 and item_padre = 1), 1, 0, 9);
insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso)
values ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), 0, (select max(item) from menus_items where menu = 1), 1, 2);
do $$
    declare
        ucodigo smallint;
    begin
        for ucodigo in select codigo from wusuario where estado = 1 and codigo > 0 and codigo < 90
            loop
                insert into menus_items_accesos (codigo, usuario, item_menu, item_padre, acceso) VALUES
                    ((select coalesce(max(codigo), 0) + 1 from menus_items_accesos), ucodigo, (select max(item) from menus_items where menu = 1), 1, 0);
            end loop;
    end;
$$;
alter table pedidos add confirmacion_estado smallint;
alter table proveedores add fecha_caducidad_iso9001 date;
create or replace function fn_pedidos_bobinaslin_after_all() returns trigger language plpgsql
as
$$
declare
    ident character(15);
    totLin INTEGER;
    linPen INTEGER;
    linRecPar INTEGER;
    linRec INTEGER;
    linCan INTEGER;
begin
    IF (TG_OP = 'DELETE') THEN ident = OLD.identificador;
    ELSE ident = NEW.identificador;
    END IF;

    SELECT COUNT(*) INTO totLin FROM pedidos_bobinaslin WHERE identificador = ident;
    SELECT COUNT(*) INTO linPen FROM pedidos_bobinaslin WHERE estado = 0 AND identificador = ident;
    SELECT COUNT(*) INTO linRecPar FROM pedidos_bobinaslin WHERE estado = 1 AND identificador = ident;
    SELECT COUNT(*) INTO linRec FROM pedidos_bobinaslin WHERE estado = 2 AND identificador = ident;
    SELECT COUNT(*) INTO linCan FROM pedidos_bobinaslin WHERE estado = 3 AND identificador = ident;

    if (totLin > 0) then
        if (totLin = linCan) then
            update pedidos_bobinas set estado = 3 where identificador = ident;
        else
            -- DESCONTAMOS LAS CANCELADAS
            totLin = totLin - linCan;
            -- PEDIDO RECIBIDO PARCIAL
            if (linRecPar > 0) then
                update pedidos_bobinas set estado = 4 where identificador = ident;
            end if;
            -- PEDIDO RECIBIDO
            if (linRec = totLin) then
                update pedidos_bobinas set estado = 5 where identificador = ident;
            end if;
        end if;
    end if;

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
end;
$$;
create trigger tg_pedidos_bobinaslin_after_all
    after insert or update or delete
    on pedidos_bobinaslin
    for each row execute procedure fn_pedidos_bobinaslin_after_all();
create or replace function fn_pedidos_proveedorlin_after_all() returns trigger language plpgsql
as
$$
declare
    ident character(15);
    totLin INTEGER;
    linPen INTEGER;
    linRecPar INTEGER;
    linRec INTEGER;
    linCan INTEGER;
begin
    IF (TG_OP = 'DELETE') THEN ident = OLD.identificador;
    ELSE ident = NEW.identificador;
    END IF;

    SELECT COUNT(*) INTO totLin FROM pedidos_proveedorlin WHERE identificador = ident;
    SELECT COUNT(*) INTO linPen FROM pedidos_proveedorlin WHERE estado = 0 AND identificador = ident;
    SELECT COUNT(*) INTO linRecPar FROM pedidos_proveedorlin WHERE estado = 1 AND identificador = ident;
    SELECT COUNT(*) INTO linRec FROM pedidos_proveedorlin WHERE estado = 2 AND identificador = ident;
    SELECT COUNT(*) INTO linCan FROM pedidos_proveedorlin WHERE estado = 3 AND identificador = ident;

    if (totLin > 0) then
        if (totLin = linCan) then
            update pedidos_proveedor set estado = 3 where identificador = ident;
        else
            -- DESCONTAMOS LAS CANCELADAS
            totLin = totLin - linCan;
            -- PEDIDO RECIBIDO PARCIAL
            if (linRecPar > 0) then
                update pedidos_proveedor set estado = 4 where identificador = ident;
            end if;
            -- PEDIDO RECIBIDO
            if (linRec = totLin) then
                update pedidos_proveedor set estado = 5 where identificador = ident;
            end if;
        end if;
    end if;

    IF (TG_OP = 'DELETE') THEN RETURN NULL;
    ELSE RETURN NEW;
    END IF;
end;
$$;
create trigger tg_pedidos_proveedorlin_after_all
    after insert or update or delete
    on pedidos_proveedorlin
    for each row execute procedure fn_pedidos_proveedorlin_after_all();
update empresa set vcs = 'v23.09.07' where codigo = 1;
-- **** v23.09.07 **** --
update empresa set vcs = 'v23.09.07' where codigo = 1;
-- **** v23.09.28 **** --
insert into log_versions (version, info) values ('v23.09.28', 'Se corrige la funcionalidad del botón para la recepción de bobinas');
update empresa set vcs = 'v23.09.28' where codigo = 1;
-- **** v23.10.05 **** --
update bobinas set reserva_oferta = null where reserva_oferta = '0' or reserva_oferta = '';
update bobinas set estado = 1 where estado = 2 and reserva_oferta is null;
update bobinas set estado = 2 where estado = 1 and reserva_oferta is not null;
create index log_versions_version_index on log_versions (version);
create index bobinas_reserva_oferta_index on bobinas (reserva_oferta);
insert into log_versions (version, info) values ('v23.10.05', 'Se modifica el comportamiento del estado y reservas de las bobinas');
update empresa set vcs = 'v23.10.05' where codigo = 1;
-- **** v23.10.11 **** --
insert into log_versions (version, info) values ('v23.10.11', 'Se corrige modificación en consulta de bobinas');
update empresa set vcs = 'v23.10.11' where codigo = 1;
-- **** v23.10.11 **** --
update empresa set vcs = 'v23.10.11' where codigo = 1;
-- **** v23.10.19 **** --
update empresa set vcs = 'v23.10.19' where codigo = 1;
-- **** v23.10.19 **** --
update empresa set vcs = 'v23.10.19' where codigo = 1;
-- **** v23.10.19 **** --
update empresa set vcs = 'v23.10.19' where codigo = 1;
-- **** v23.10.25 **** --
alter table empleados
    add l_jornada numeric(4,2),
    add l_manana_entra_teo time,
    add l_manana_descanso_teo time,
    add l_manana_sale_teo time,
    add l_tarde_entra_teo time,
    add l_tarde_descanso_teo time,
    add l_tarde_sale_teo time,
    add m_jornada numeric(4,2),
    add m_manana_entra_teo time,
    add m_manana_descanso_teo time,
    add m_manana_sale_teo time,
    add m_tarde_entra_teo time,
    add m_tarde_descanso_teo time,
    add m_tarde_sale_teo time,
    add x_jornada numeric(4,2),
    add x_manana_entra_teo time,
    add x_manana_descanso_teo time,
    add x_manana_sale_teo time,
    add x_tarde_entra_teo time,
    add x_tarde_descanso_teo time,
    add x_tarde_sale_teo time,
    add j_jornada numeric(4,2),
    add j_manana_entra_teo time,
    add j_manana_descanso_teo time,
    add j_manana_sale_teo time,
    add j_tarde_entra_teo time,
    add j_tarde_descanso_teo time,
    add j_tarde_sale_teo time,
    add v_jornada numeric(4,2),
    add v_manana_entra_teo time,
    add v_manana_descanso_teo time,
    add v_manana_sale_teo time,
    add v_tarde_entra_teo time,
    add v_tarde_descanso_teo time,
    add v_tarde_sale_teo time;
update empleados
set
    l_jornada = jornada_lj,
    l_manana_entra_teo = manana_entra_lj_teo,
    l_manana_sale_teo = manana_sale_lj_teo,
    l_tarde_entra_teo = tarde_entra_lj_teo,
    l_tarde_sale_teo = tarde_sale_lj_teo,
    m_jornada = jornada_lj,
    m_manana_entra_teo = manana_entra_lj_teo,
    m_manana_sale_teo = manana_sale_lj_teo,
    m_tarde_entra_teo = tarde_entra_lj_teo,
    m_tarde_sale_teo = tarde_sale_lj_teo,
    x_jornada = jornada_lj,
    x_manana_entra_teo = manana_entra_lj_teo,
    x_manana_sale_teo = manana_sale_lj_teo,
    x_tarde_entra_teo = tarde_entra_lj_teo,
    x_tarde_sale_teo = tarde_sale_lj_teo,
    j_jornada = jornada_lj,
    j_manana_entra_teo = manana_entra_lj_teo,
    j_manana_sale_teo = manana_sale_lj_teo,
    j_tarde_entra_teo = tarde_entra_lj_teo,
    j_tarde_sale_teo = tarde_sale_lj_teo,
    v_jornada = jornada_v,
    v_manana_entra_teo = manana_entra_v_teo,
    v_manana_sale_teo = manana_sale_v_teo
where codigo > 0;
alter table empleados
    drop jornada_lj,
    drop jornada_v,
    drop manana_entra_lj_teo,
    drop manana_sale_lj_teo,
    drop tarde_entra_lj_teo,
    drop tarde_sale_lj_teo,
    drop manana_entra_v_teo,
    drop manana_sale_v_teo;
insert into log_versions (version, info) values ('v23.10.25', 'Se añaden campos para poder separar los días del horario semanal de los empleados');
insert into log_versions (version, info) values ('v23.10.25', 'Se adaptan los informes y procesos de los tiempos a los cambios realizados');
update empresa set vcs = 'v23.10.25' where codigo = 1;
-- **** v23.10.26 **** --
update empresa set vcs = 'v23.10.26' where codigo = 1;
-- **** v23.10.26 **** --
update empresa set vcs = 'v23.10.26' where codigo = 1;
-- **** v23.10.26 **** --
update empresa set vcs = 'v23.10.26' where codigo = 1;
-- **** v23.10.26 **** --
update empresa set vcs = 'v23.10.26' where codigo = 1;
-- **** v23.11.02 **** --
insert into log_versions (version, info) values ('v23.11.02', 'Se corrige cálculo de bobinas en el proceso de validación de pedidos');
update empresa set vcs = 'v23.11.02' where codigo = 1;
-- **** v23.11.02 **** --
update empresa set vcs = 'v23.11.02' where codigo = 1;
-- **** v23.11.02 **** --
insert into log_versions (version, info) values ('v23.11.02', 'Se corrige informe mensual de tiempos');
update empresa set vcs = 'v23.11.02' where codigo = 1;
-- **** v23.11.02 **** --
alter table historico_tiempos add oberservaciones varchar(255);
update empresa set vcs = 'v23.11.02' where codigo = 1;
-- **** v23.11.02 **** --
update empresa set vcs = 'v23.11.02' where codigo = 1;
-- **** v23.11.06 **** --
update empresa set vcs = 'v23.11.06' where codigo = 1;
-- **** v23.11.09 **** --
insert into log_versions (version, info) values ('v23.11.09', 'Se corrige tiempos');
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.09 **** --
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.09 **** --
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.09 **** --
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.09 **** --
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.09 **** --
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.09 **** --
update empresa set vcs = 'v23.11.09' where codigo = 1;
-- **** v23.11.16 **** --
insert into log_versions (version, info) values ('v23.11.16', 'Se corrigen los procesos de recepción de bobinas');
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.16 **** --
insert into log_versions (version, info) values ('v23.11.16', 'Se modifica el proceso de generación de vencimientos');
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.16 **** --
insert into log_versions (version, info) values ('v23.11.16', 'Se modifica procedimiento del registro de tiempos');
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.16 **** --
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.16 **** --
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.16 **** --
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.16 **** --
update empresa set vcs = 'v23.11.16' where codigo = 1;
-- **** v23.11.23 **** --
update empresa set vcs = 'v23.11.23' where codigo = 1;
-- **** v23.11.23 **** --
alter table tonazzo_gasparini_longitudes add pedidoslin_linea integer, add pedidoslin_c_cantidad integer;
insert into log_versions (version, info) values ('v23.11.23', 'Se cambia la comunicación con la Gasparini');
update empresa set vcs = 'v23.11.23' where codigo = 1;
-- **** v23.11.27 **** --
create or replace function public.fn_pedidos_after_insert() returns trigger
    language plpgsql
as
$$
DECLARE
BEGIN
    IF (TG_OP = 'INSERT') THEN
        IF (position('|' in NEW.identificador_oferta) > 0) THEN
            INSERT INTO logfails (fecha, hora, obs) VALUES (current_date, current_time, 'INSERT: ' || NEW.identificador_oferta);
        END IF;
    END IF;

    IF (TG_OP = 'UPDATE') THEN
        IF (position('|' in NEW.identificador_oferta) > 0) THEN
            INSERT INTO logfails (fecha, hora, obs) VALUES (current_date, current_time, 'UPDATE: ' || OLD.identificador_oferta || ' - ' || NEW.identificador_oferta);
        END IF;

        IF (old.aplicar_recargos != new.aplicar_recargos) THEN
            -- PARA ACTUALIZAR PRECIOS
            update pedidoslin set referencia_plano = trim(referencia_plano) where identificador = new.identificador and linea = 1;
        END IF;
    END IF;

    RETURN NEW;
END;
$$;

create table historico_tgp
(
    nrgto serial not null primary key,
    fecha date not null default current_date,
    hora time default current_time,
    id_pedido_paquete bigint   not null,
    id_pedido         bigint   not null,
    id_paquete        bigint   not null,
    perfil            bigint   not null,
    espesor           real     not null,
    stato             smallint not null,
    inicio            timestamp,
    fin               timestamp,
    pieza_producidas  smallint not null,
    seq_prod          smallint not null,
    cargado           boolean  not null,
    descripcion       text
);
create table historico_tgl
(
    nrgto serial not null primary key,
    fecha date not null default current_date,
    hora time default current_time,
    id_paquete_linea      bigint   not null,
    id_paquete            bigint   not null,
    linea                 smallint not null,
    longitud              real     not null,
    piezas_a_producir     smallint not null,
    piezas_producidas     smallint not null,
    pedidoslin_linea      integer,
    pedidoslin_c_cantidad integer
);

insert into log_versions (version, info) values ('v23.11.27', 'Se crea proceso automático para consumo tras el cambio de la Gasparini');
update empresa set vcs = 'v23.11.27' where codigo = 1;
-- **** v23.12.13 **** --
alter table productos add tonazzo_colaborante int;
update productos set tonazzo_colaborante = 0 where tonazzo_perfil is not null;
update productos set tonazzo_colaborante = 1 where codigo in (34,138);
insert into log_versions (version, info) values ('v23.12.13', 'Se añade el campo de colaborante a los productos para la Gasparini');
insert into log_versions (version, info) values ('v23.12.13', 'Se modifica consumos de Gasparini');
update empresa set vcs = 'v23.12.13' where codigo = 1;
-- **** v23.12.13 **** --
update empresa set vcs = 'v23.12.13' where codigo = 1;
-- **** v23.12.14 **** --
create or replace function public.fn_pedidoslin_curvado_before_insert_update() returns trigger
    language plpgsql
as
$$
begin
    if (new.hbf_numero = 0) then
        new.hbf_numero_ext := 'Desmontar';
    elseif (new.hbf_numero > 15) then
        new.hbf_numero_ext := 'Ext. + ' || (new.hbf_numero - 10);
    else
        new.hbf_numero_ext := '' || new.hbf_numero;
    end if;

    if (new.hbf_numero_ext_real is null or new.hbf_numero_ext_real = '') then
        new.hbf_numero_ext_real := new.hbf_numero_ext;
    end if;

    return new;
end
$$;
insert into log_versions (version, info) values ('v23.12.14', 'Se modifica el listado de muestras y verificaciones de curvado');
alter table muestras_curvadora add mcradio integer;
update muestras_curvadora
set mcradio = ((pow(flecha, 2)::numeric(10,2) + pow(cuerda / 2, 2)::numeric(10,2)) / (2 * flecha))::integer
where mcradio is null;
insert into log_versions (version, info) values ('v23.12.14', 'Se añade el campo radio a la tabla de muestras de curvado');
update empresa set vcs = 'v23.12.14' where codigo = 1;
-- **** v23.12.21 **** --
update empresa set vcs = 'v23.12.21' where codigo = 1;
-- **** v24.01.03 **** --
update empresa set vcs = 'v24.01.03' where codigo = 1;
-- **** v24.01.04 **** --
insert into log_versions (version, info) values ('v24.01.04', 'Se corrige consumo Gasparini');
update empresa set vcs = 'v24.01.04' where codigo = 1;
-- **** v24.01.04 **** --
update empresa set vcs = 'v24.01.04' where codigo = 1;
-- **** v24.01.11 **** --
insert into log_versions (version, info) values ('v24.01.11', 'Se modifica el proceso de consumo de la Gasparini');
insert into log_versions (version, info) values ('v24.01.11', 'Se corrige el proceso de registro de no conformidad de las perfiladoras');
update empresa set vcs = 'v24.01.11' where codigo = 1;
-- **** v24.01.11 **** --
insert into log_versions (version, info) values ('v24.01.11', 'Se revisa el proceso de duplicar pedido tras añadir nuevas funcionalidades a los pedidos');
insert into log_versions (version, info) values ('v24.01.11', 'Se corrigen listados de bobinas');
update empresa set vcs = 'v24.01.11' where codigo = 1;
-- **** v24.01.18 **** --
insert into log_versions (version, info) values ('v24.01.18', 'Se añade el material auxiliar para la sección de rematería');
insert into log_versions (version, info) values ('v24.01.18', 'Se redirige el funcionamiento de consumos de la Gasparini al PLC');
update empresa set vcs = 'v24.01.18' where codigo = 1;
-- **** v24.01.18 **** --
update empresa set vcs = 'v24.01.18' where codigo = 1;
-- **** v24.01.18 **** --
update empresa set vcs = 'v24.01.18' where codigo = 1;
-- **** v24.01.25 **** --
update empresa set vcs = 'v24.01.25' where codigo = 1;
-- **** v24.01.25 **** --
update empresa set vcs = 'v24.01.25' where codigo = 1;
-- **** v24.02.01 **** --
insert into log_versions (version, info) values ('v24.02.01', 'Se añaden nuevas condiciones en la oferta para los caballetes');
update empresa set vcs = 'v24.02.01' where codigo = 1;
-- **** v24.02.01 **** --
update empresa set vcs = 'v24.02.01' where codigo = 1;
-- **** v24.02.07 **** --
insert into log_versions (version, info) values ('v24.02.07', 'Se habilita un selector de impresora cuando producción imprime etiquetas desde las máquinas (52m)');
update empresa set vcs = 'v24.02.07' where codigo = 1;
-- **** v24.02.07 **** --
insert into log_versions (version, info) values ('v24.02.07', 'Se revisan las impresoras de nave B y se ajusta la etiqueta para reducir espacios en blanco para evitar que se salga la información (1h 47m)');
update empresa set vcs = 'v24.02.07' where codigo = 1;
-- **** v24.02.07 **** --
update empresa set vcs = 'v24.02.07' where codigo = 1;
-- **** v24.02.07 **** --
update empresa set vcs = 'v24.02.07' where codigo = 1;
-- **** v24.02.08 **** --
insert into log_versions (version, info) values ('v24.02.08', 'Se corrige el cambio de bobina y programa para que rematería pueda realizar consumos de material auxiliar');
update empresa set vcs = 'v24.02.08' where codigo = 1;
-- **** v24.02.15 **** --
create table historico_parciales(
    codigo serial primary key,
    maquina integer,
    fecha date default current_date,
    hora time default current_time,
    fabricadas integer,
    fabricadas_parcial integer
);
create or replace function fn_fabribask_parcial_insert_update() returns trigger language plpgsql as
$$
DECLARE
    fabricParcial integer;
BEGIN
    fabricParcial = null;
    IF (TG_OP = 'UPDATE') THEN
        fabricParcial = new.fabricadas - old.fabricadas;
    end if;
    insert into historico_parciales (maquina, fabricadas, fabricadas_parcial) values (1, new.fabricadas, fabricParcial);
    return new;
END
$$;
create trigger tg_fabribask_parcial_after_insert_update after insert or update on fabribask_parcial
    for each row execute procedure fn_fabribask_parcial_insert_update();
create or replace function fn_gasparini_parcial_insert_update() returns trigger language plpgsql as
$$
DECLARE
    fabricParcial integer;
BEGIN
    fabricParcial = null;
    IF (TG_OP = 'UPDATE') THEN
        fabricParcial = new.fabricadas - old.fabricadas;
    end if;
    insert into historico_parciales (maquina, fabricadas, fabricadas_parcial) values (2, new.fabricadas, fabricParcial);
    return new;
END
$$;
create trigger tg_gasparini_parcial_after_insert_update after insert or update on gasparini_parcial
    for each row execute procedure fn_gasparini_parcial_insert_update();
create or replace function fn_reliantt_parcial_insert_update() returns trigger language plpgsql as
$$
DECLARE
    fabricParcial integer;
BEGIN
    fabricParcial = null;
    IF (TG_OP = 'UPDATE') THEN
        fabricParcial = new.fabricadas - old.fabricadas;
    end if;
    insert into historico_parciales (maquina, fabricadas, fabricadas_parcial) values (9, new.fabricadas, fabricParcial);
    return new;
END
$$;
create trigger tg_reliantt_parcial_after_insert_update after insert or update on reliantt_parcial
    for each row execute procedure fn_reliantt_parcial_insert_update();
insert into log_versions (version, info) values ('v24.02.15', 'Se corrige la búsqueda de bobinas para que no aparezcan las bobinas que están cargadas en máquina');
insert into log_versions (version, info) values ('v24.02.15', 'Se cambia la ubicación de las bobinas del buscador para indicar aquellas que están cargadas en máquina');
insert into log_versions (version, info) values ('v24.02.15', 'Se crea un histórico de los parciales de las perfiladoras');
update empresa set vcs = 'v24.02.15' where codigo = 1;
-- **** v24.02.15 **** --
drop table if exists historico_parciales;
create table historico_parciales(
    codigo serial primary key,
    fecha date default current_date,
    hora time default current_time,
    maquina integer,
    pedido character(15),
    linea integer,
    fabricadas integer,
    fabricadas_parcial integer
);
create or replace function fn_fabribask_parcial_insert_update() returns trigger language plpgsql as
$$
DECLARE
    fabricParcial integer;
BEGIN
    fabricParcial = null;
    IF (TG_OP = 'UPDATE') THEN
        fabricParcial = new.fabricadas - old.fabricadas;
    end if;
    insert into historico_parciales (maquina, pedido, linea, fabricadas, fabricadas_parcial)
        values (1, new.pedido, new.linea, new.fabricadas, fabricParcial);
    return new;
END
$$;
create or replace function fn_gasparini_parcial_insert_update() returns trigger language plpgsql as
$$
DECLARE
    fabricParcial integer;
BEGIN
    fabricParcial = null;
    IF (TG_OP = 'UPDATE') THEN
        fabricParcial = new.fabricadas - old.fabricadas;
    end if;
    insert into historico_parciales (maquina, pedido, linea, fabricadas, fabricadas_parcial)
        values (2, new.pedido, new.linea, new.fabricadas, fabricParcial);
    return new;
END
$$;
create or replace function fn_reliantt_parcial_insert_update() returns trigger language plpgsql as
$$
DECLARE
    fabricParcial integer;
BEGIN
    fabricParcial = null;
    IF (TG_OP = 'UPDATE') THEN
        fabricParcial = new.fabricadas - old.fabricadas;
    end if;
    insert into historico_parciales (maquina, pedido, linea, fabricadas, fabricadas_parcial)
        values (9, new.pedido, new.linea, new.fabricadas, fabricParcial);
    return new;
END
$$;
update empresa set vcs = 'v24.02.15' where codigo = 1;
-- **** v24.02.16 **** --
insert into log_versions (version, info) values ('v24.02.16', 'Se corrige consulta de bobinas');

update empresa set vcs = 'v24.02.16' where codigo = 1;
-- **** v24.02.22 **** --
insert into log_versions (version, info) values ('v24.02.22', 'Se corrige proceso de importación de bobinas que las marcaba como activas');
insert into log_versions (version, info) values ('v24.02.22', 'Se habilita la impresión de etiquetas de bobina en todas las impresoras');
update empresa set vcs = 'v24.02.22' where codigo = 1;
-- **** v24.02.22 **** --
update empresa set vcs = 'v24.02.22' where codigo = 1;
-- **** v24.02.22 **** --
update empresa set vcs = 'v24.02.22' where codigo = 1;
-- **** v24.03.07 **** --
alter table productos_composicion
    add c_flecha integer,
    add c_cuerda integer,
    add p_radio  integer,
    add p_flecha integer,
    add p_cuerda integer,
    add especial smallint;
create table productos_relacionados
(
    codigo                   integer not null,
    producto_padre           integer not null,
    seccion                  integer,
    producto                 integer,
    espesor                  integer,
    calidad                  integer,
    recubrimiento            integer,
    pintura                  integer,
    ccolor                   integer,
    pinturab                 integer,
    ccolorb                  integer,
    herencia_espesor         smallint,
    herencia_calidad         smallint,
    herencia_recubrimiento   smallint,
    herencia_pintura         smallint,
    herencia_ccolor          smallint,
    herencia_pinturab        smallint,
    herencia_ccolorb         smallint,
    unidad_medida            integer,
    precio                   numeric(10, 2),
    descuento                numeric(4, 2),
    cantidad_total           numeric(10, 2),
    cantidad_total_pendiente numeric(10, 2),
    descripcion              char(255),
    descripcion_comercial    char(255),
    estado                   smallint,
    fecha_fabricacion        date,
    fecha_entrega            date,
    peso                     numeric(10, 3),
    paquete                  smallint,
    referencia_plano         char(31),
    precio_portes            numeric(10, 2),
    c_posicion               smallint,
    c_curvado                smallint,
    c_perforado              integer,
    c_cantidad               integer,
    c_cantidad_pendiente     integer,
    c_longitud               integer,
    r_numero_plegados        smallint,
    r_formacion              smallint,
    r_perforado              integer,
    r_cantidad               numeric(10, 3),
    r_cantidad_pendiente     integer,
    r_desarrollo             integer,
    r_corte                  integer,
    r_formatos               smallint,
    r_largo                  integer,
    r_ancho                  integer,
    r_ancho_util             integer,
    r_piezas                 smallint,
    r_agrupacion_corte       smallint,
    o_cantidad               integer,
    o_cantidad_pendiente     integer,
    o_longitud               integer,
    p_curvado                smallint,
    p_cantidad               integer,
    p_cantidad_pendiente     integer,
    p_longitud               integer,
    a_modulos                integer,
    a_modulos_pendiente      integer,
    a_longitud               integer,
    a_corrido                smallint,
    a_registro               smallint,
    a_malla_antipajaros      smallint,
    e_cantidad               integer,
    e_cantidad_pendiente     integer,
    e_distancia              integer,
    e_pendiente              numeric(7, 3),
    e_malla_antipajaros      smallint,
    l_cantidad               integer,
    l_cantidad_pendiente     integer,
    l_ancho                  integer,
    l_alto                   integer,
    l_longitud               integer,
    l_malla_antipajaros      smallint,
    l_angulo_perimetral      smallint,
    v_cantidad               integer,
    v_cantidad_pendiente     integer,
    v_longitud               integer,
    v_ancho                  integer,
    f_cantidad               integer,
    f_cantidad_pendiente     integer,
    f_m2_reales              numeric(10, 3),
    f_m2_utiles              numeric(10, 3),
    proveedor                integer,
    c_radio                  integer,
    c_flecha                 integer,
    c_cuerda                 integer,
    p_radio                  integer,
    p_flecha                 integer,
    p_cuerda                 integer,
    especial                 smallint,
    primary key (codigo)
);
comment on table productos_relacionados is 'C: Chapa perfilada; R: Remates; P: Poliester; O: Omegas; A: Ventilación Lineal; L: Ventanas de Lamas; S: Casetes; V: Otros; E: Ventilación Puntual; F: Fachadas';
comment on column productos_relacionados.herencia_espesor is '0: No; 1: Si';
comment on column productos_relacionados.estado is '0: Pendiente; 1: Fabricación; 2: Semifabricación; 3: Terminado; 4: Servido; 5: Cancelado';
comment on column productos_relacionados.c_posicion is '0: Cubierta; 1: Fachada';
comment on column productos_relacionados.c_curvado is '0: No; 1: Normal; 2: Cumbrera; 3: Esquina';
comment on column productos_relacionados.r_formacion is '0: Tirada; 1: Pendiente; 2: Ambos';
comment on column productos_relacionados.p_curvado is '0: Normal; 1: Cumbrera; 2: Esquina';
comment on column productos_relacionados.a_corrido is '0: No; 1: Si';
comment on column productos_relacionados.a_registro is '0: No; 1: Si';
comment on column productos_relacionados.a_malla_antipajaros is '0: No; 1: Malla; 2: Mosquitera';
comment on column productos_relacionados.e_malla_antipajaros is '0: Nada; 1: Malla; 2: Mosquitera';
comment on column productos_relacionados.l_ancho is 'Es la profundidad';
comment on column productos_relacionados.l_malla_antipajaros is '0: No; 1: Malla; 2: Mosquitera';
comment on column productos_relacionados.l_angulo_perimetral is '0: No; 1: Si';
comment on column productos_relacionados.herencia_calidad is '0: No; 1: Si';
comment on column productos_relacionados.herencia_recubrimiento is '0: No; 1: Si';
comment on column productos_relacionados.herencia_pintura is '0: No; 1: Si';
comment on column productos_relacionados.herencia_ccolor is '0: No; 1: Si';
comment on column productos_relacionados.herencia_pinturab is '0: No; 1: Si';
comment on column productos_relacionados.herencia_ccolorb is '0: No; 1: Si';
update empresa set vcs = 'v24.03.07' where codigo = 1;
-- **** v24.03.07 **** --
insert into log_versions (version, info) values ('v24.03.07', 'Se añaden los productos relacionados');
insert into log_versions (version, info) values ('v24.03.07', 'Se añaden al listados de bobinas aquellas que están cargadas en máquina');
update empresa set vcs = 'v24.03.07' where codigo = 1;
-- **** v24.03.13 **** --
insert into log_versions (version, info) values ('v24.03.13', 'Se corrige el uso del botón de "Descarga bobina"');
update empresa set vcs = 'v24.03.13' where codigo = 1;
-- **** v24.03.19 **** --
update empresa set vcs = 'v24.03.19' where codigo = 1;
-- **** v24.03.21 **** --
insert into log_versions (version, info) values ('v24.03.21', 'Se añade la gestión de productos relacionados hacia la oferta');
update empresa set vcs = 'v24.03.21' where codigo = 1;
-- **** v24.04.04 **** --
insert into log_versions (version, info) values ('v24.04.04', 'Se realiza corrección en la gestión del productos relacionados');
update empresa set vcs = 'v24.04.04' where codigo = 1;
-- **** v24.04.04 **** --
update empresa set vcs = 'v24.04.04' where codigo = 1;
-- **** v24.04.04 **** --
alter table tarifas add codigo integer;

update empresa set vcs = 'v24.04.04' where codigo = 1;
-- **** v24.04.11 **** --
update empresa set vcs = 'v24.04.11' where codigo = 1;
-- **** v24.04.18 **** --
insert into log_versions (version, info) values ('v24.04.18', 'Se modifica el proceso de solicitud de MP desde una oferta de ventas');
update empresa set vcs = 'v24.04.18' where codigo = 1;
-- **** v24.04.18 **** --
insert into log_versions (version, info) values ('v24.04.18', 'Se corrigen traducciones de las ofertas de ventas');
update empresa set vcs = 'v24.04.18' where codigo = 1;
-- **** v24.04.18 **** --
update empresa set vcs = 'v24.04.18' where codigo = 1;
-- **** v24.05.09 **** --
create or replace function public.fn_pedidoslin_curvado_before_insert_update() returns trigger
    language plpgsql
as
$$
begin
    if (new.hbf_numero = 0) then
        new.hbf_numero_ext := 'Desmontar';
    elseif (new.hbf_numero > 11) then
        new.hbf_numero_ext := 'Ext. + ' || (new.hbf_numero - 10);
    else
        new.hbf_numero_ext := '' || new.hbf_numero;
    end if;

    if (new.hbf_numero_ext_real is null or new.hbf_numero_ext_real = '') then
        new.hbf_numero_ext_real := new.hbf_numero_ext;
    end if;

    if (new.hbf_numero_real = 0) then
        new.hbf_numero_ext_real := 'Desmontar';
    elseif (new.hbf_numero_real > 11) then
        new.hbf_numero_ext_real := 'Ext. + ' || (new.hbf_numero_real - 10);
    else
        new.hbf_numero_ext_real := '' || new.hbf_numero_real;
    end if;

    return new;
end
$$;
alter table pedidoslin_curvado add d numeric(10,2);
comment on column pedidoslin_curvado.d is 'Por defecto es el valor D de la máquina';

update empresa set vcs = 'v24.05.09' where codigo = 1;
-- **** v24.05.16 **** --
insert into log_versions (version, info) values ('v24.05.16', 'Se corrigen traducciones de la oferta');
update empresa set vcs = 'v24.05.16' where codigo = 1;
-- **** v24.05.23 **** --
insert into log_versions (version, info) values ('v24.05.23', 'Se añade botón de acceso directo a oferta/pedido desde transferencias');
update empresa set vcs = 'v24.05.23' where codigo = 1;
-- **** v24.05.23 **** --
update empresa set vcs = 'v24.05.23' where codigo = 1;
-- **** v24.05.30 **** --
insert into log_versions (version, info) values ('v24.05.30', 'Se envían todos los correos a través de MailRelay');
update empresa set vcs = 'v24.05.30' where codigo = 1;
-- **** v24.06.27 **** --
update empresa set vcs = 'v24.06.27' where codigo = 1;
-- **** v24.07.04 **** --
update empresa set vcs = 'v24.06.27' where codigo = 1;
update empresa set vcs = 'v24.07.04' where codigo = 1;
-- **** v24.07.04 **** --
update empresa set vcs = 'v24.07.04' where codigo = 1;
-- **** v24.07.18 **** --
update empresa set vcs = 'v24.07.18' where codigo = 1;
-- **** v24.09.12 **** --
create table tonazzo_fabribask_paquetes
(
    id_pedido_paquete bigint   not null
        constraint pk_tonazzo_fabribask_paquetes
            primary key,
    id_pedido         bigint   not null,
    id_paquete        bigint   not null,
    perfil            bigint   not null,
    espesor           real     not null,
    stato             smallint not null,
    inicio            timestamp,
    fin               timestamp,
    pieza_producidas  smallint not null,
    seq_prod          smallint not null,
    cargado           boolean  not null,
    descripcion       text,
    colaborante       integer
);
create index tonazzo_fabribask_paquetes_id_paquete_index
    on tonazzo_fabribask_paquetes (id_paquete);

create index tonazzo_fabribask_paquetes_id_pedido_index
    on tonazzo_fabribask_paquetes (id_pedido);
create table tonazzo_fabribask_longitudes
(
    id_paquete_linea      bigint   not null
        constraint pk_tonazzo_fabribask_lonigtudes
            primary key,
    id_paquete            bigint   not null,
    linea                 smallint not null,
    longitud              real     not null,
    piezas_a_producir     smallint not null,
    piezas_producidas     smallint not null,
    pedidoslin_linea      integer,
    pedidoslin_c_cantidad integer
);
create index tonazzo_fabribask_longitudes_id_paquete_index
    on tonazzo_fabribask_longitudes (id_paquete);

create index tonazzo_fabribask_longitudes_id_paquete_linea_index
    on tonazzo_fabribask_longitudes (id_paquete, linea);
create table historico_tfp
(
    nrgto serial not null primary key,
    fecha date not null default current_date,
    hora time default current_time,
    id_pedido_paquete bigint   not null,
    id_pedido         bigint   not null,
    id_paquete        bigint   not null,
    perfil            bigint   not null,
    espesor           real     not null,
    stato             smallint not null,
    inicio            timestamp,
    fin               timestamp,
    pieza_producidas  smallint not null,
    seq_prod          smallint not null,
    cargado           boolean  not null,
    descripcion       text
);
create table historico_tfl
(
    nrgto serial not null primary key,
    fecha date not null default current_date,
    hora time default current_time,
    id_paquete_linea      bigint   not null,
    id_paquete            bigint   not null,
    linea                 smallint not null,
    longitud              real     not null,
    piezas_a_producir     smallint not null,
    piezas_producidas     smallint not null,
    pedidoslin_linea      integer,
    pedidoslin_c_cantidad integer
);
insert into log_versions (version, info) VALUES ('v24.09.12', 'Se prepara la Fabribask para el cambio de Tonazzo');
update empresa set vcs = 'v24.09.12' where codigo = 1;
-- **** v24.09.12 **** --
update empresa set vcs = 'v24.09.12' where codigo = 1;
-- **** v24.09.13 **** --
update empresa set vcs = 'v24.09.13' where codigo = 1;
-- **** v24.09.13 **** --
update empresa set vcs = 'v24.09.13' where codigo = 1;
-- **** v24.09.13 **** --
update empresa set vcs = 'v24.09.13' where codigo = 1;
-- **** v24.09.19 **** --
create or replace function fn_pedidoslin_curvado_before_insert_update() returns trigger
    language plpgsql
as
$$
begin
    if (new.hbf_numero = 0) then
        new.hbf_numero_ext := 'Desmontar';
    elseif (new.hbf_numero > 11) then
        new.hbf_numero_ext := 'Ext. + ' || (new.hbf_numero - 10);
    else
        new.hbf_numero_ext := 'Sin Ext. ' || new.hbf_numero;
    end if;

    if (new.hbf_numero_ext_real is null or new.hbf_numero_ext_real = '') then
        new.hbf_numero_ext_real := new.hbf_numero_ext;
    end if;

    if (new.hbf_numero_real = 0) then
        new.hbf_numero_ext_real := 'Desmontar';
    elseif (new.hbf_numero_real > 11) then
        new.hbf_numero_ext_real := 'Ext. + ' || (new.hbf_numero_real - 10);
    else
        new.hbf_numero_ext_real := 'Sin Ext. ' || new.hbf_numero_real;
    end if;

    return new;
end
$$;
insert into log_versions (version, info) values ('v24.09.19', 'Se ajustan parámetros de la curvadora TAM');
insert into log_versions (version, info) values ('v24.09.19', 'Se añaden modificaciones en tratamiento de bobinas');
update empresa set vcs = 'v24.09.19' where codigo = 1;
-- **** v24.09.26 **** --
alter table albaranes add column firmado smallint;
comment on column albaranes.firmado is '0: No; 1: Si';
insert into log_versions (version, info) values ('v24.09.26', 'Se añade gestión documental para empleados y máquinas');
insert into log_versions (version, info) values ('v24.09.26', 'Se añade campo a los albaranes para saber si están fimrados');
update empresa set vcs = 'v24.09.26' where codigo = 1;
-- **** v24.09.26 **** --
update empresa set vcs = 'v24.09.26' where codigo = 1;
-- **** v24.10.10 **** --
insert into log_versions (version, info) values ('v24.10.10', 'Se corrigen NCs');
update empresa set vcs = 'v24.10.10' where codigo = 1;
-- **** v24.10.10 **** --
alter table porteslin add bultos_porte integer;
update empresa set vcs = 'v24.10.10' where codigo = 1;
-- **** v24.10.17 **** --
insert into log_versions (version, info) values ('v24.10.17', 'Se corrige el funcionamiento de los botones ver de ofertas y pedidos de bobinas');
update empresa set vcs = 'v24.10.17' where codigo = 1;
-- **** v24.10.17 **** --
alter table ofertas add fecha_aceptacion date;
insert into log_versions (version, info) values ('v24.10.17', 'Se añade fecha de aceptación a las ofertas');
update empresa set vcs = 'v24.10.17' where codigo = 1;
-- **** v24.11.21 **** --
create or replace view vw_pedidoslin_gasparini as
SELECT pedl.identificador,
       pedl.fecha_fabricacion                                                                                      AS fecha_entrega,
       pedl.producto,
       pedl.espesor,
       pedl.calidad,
       pedl.recubrimiento,
       pedl.pintura,
       pedl.ccolor,
       pedl.pinturab,
       pedl.ccolorb,
       pedl.c_posicion,
       pedl.c_curvado,
       pedl.c_perforado,
       sum(pedl.cantidad_total_pendiente)                                                                          AS cantidad_total_pendiente,
       sum(pedl.peso)                                                                                              AS peso,
       count(DISTINCT pedl.paquete)                                                                                AS paqs,
       (select count(distinct pedl2.paquete)
        from pedidoslin pedl2
        where pedl.identificador = pedl2.identificador)                                                            as paqs_tot
FROM pedidos ped,
     pedidoslin pedl,
     procesos_producto pp
WHERE ped.identificador = pedl.identificador
  AND pedl.estado = 4
  AND pedl.producto = pp.producto
  AND pp.maquina = 2
GROUP BY pedl.identificador, pedl.fecha_fabricacion, pedl.producto, pedl.espesor, pedl.calidad, pedl.recubrimiento,
         pedl.pintura, pedl.ccolor, pedl.pinturab, pedl.ccolorb, pedl.c_posicion, pedl.c_curvado, pedl.c_perforado
ORDER BY pedl.fecha_fabricacion;

create or replace view vw_pedidoslin_fabribak as
SELECT pedl.identificador,
       pedl.fecha_fabricacion             AS fecha_entrega,
       pedl.producto,
       pedl.espesor,
       pedl.calidad,
       pedl.recubrimiento,
       pedl.pintura,
       pedl.ccolor,
       pedl.pinturab,
       pedl.ccolorb,
       pedl.c_posicion,
       pedl.c_curvado,
       pedl.c_perforado,
       sum(pedl.cantidad_total_pendiente) AS cantidad_total_pendiente,
       sum(pedl.peso)                     AS peso,
       count(DISTINCT pedl.paquete)       AS paqs,
       (select count(distinct pedl2.paquete)
        from pedidoslin pedl2
        where pedl.identificador = pedl2.identificador)                                                            as paqs_tot
FROM pedidos ped,
     pedidoslin pedl,
     procesos_producto pp
WHERE ped.identificador = pedl.identificador
  AND pedl.estado = 4
  AND pedl.producto = pp.producto
  AND pp.maquina = 1
GROUP BY pedl.identificador, pedl.fecha_fabricacion, pedl.producto, pedl.espesor, pedl.calidad, pedl.recubrimiento,
         pedl.pintura, pedl.ccolor, pedl.pinturab, pedl.ccolorb, pedl.c_posicion, pedl.c_curvado, pedl.c_perforado
ORDER BY pedl.fecha_fabricacion;

create or replace view vw_pedidoslin_reliant as
SELECT pedl.identificador,
       pedl.fecha_fabricacion             AS fecha_entrega,
       pedl.producto,
       pedl.espesor,
       pedl.calidad,
       pedl.recubrimiento,
       pedl.pintura,
       pedl.ccolor,
       pedl.pinturab,
       pedl.ccolorb,
       pedl.c_posicion,
       pedl.c_curvado,
       pedl.c_perforado,
       sum(pedl.cantidad_total_pendiente) AS cantidad_total_pendiente,
       sum(pedl.peso)                     AS peso,
       count(DISTINCT pedl.paquete)       AS paqs,
       (select count(distinct pedl2.paquete)
        from pedidoslin pedl2
        where pedl.identificador = pedl2.identificador)                                                            as paqs_tot
FROM pedidos ped,
     pedidoslin pedl,
     procesos_producto pp
WHERE ped.identificador = pedl.identificador
  AND pedl.estado = 4
  AND pedl.producto = pp.producto
  AND pp.maquina = 9
GROUP BY pedl.identificador, pedl.fecha_fabricacion, pedl.producto, pedl.espesor, pedl.calidad, pedl.recubrimiento,
         pedl.pintura, pedl.ccolor, pedl.pinturab, pedl.ccolorb, pedl.c_posicion, pedl.c_curvado, pedl.c_perforado
ORDER BY pedl.fecha_fabricacion;

insert into log_versions (version, info) values ('v24.11.21', 'Se añaden los paquetes totales a las pantallas de las máquinas');
insert into log_versions (version, info) values ('v24.11.21', 'Se añade la firma a los pdf de ofertas');
update empresa set vcs = 'v24.11.21' where codigo = 1;
-- **** v24.11.21 **** --
update empresa set vcs = 'v24.11.21' where codigo = 1;
-- **** v24.11.21 **** --
update empresa set vcs = 'v24.11.21' where codigo = 1;
-- **** v24.11.21 **** --
update empresa set vcs = 'v24.11.21' where codigo = 1;
-- **** v24.11.28 **** --
insert into log_versions (version, info) values ('@evrsion@', 'Se realizan modificaciones sobre perfiladoras');
insert into log_versions (version, info) values ('@evrsion@', 'Se permite introducir números de bultos alfanuméricos para las bobinas');
update empresa set vcs = 'v24.11.28' where codigo = 1;
-- **** v25.02.06 **** --
insert into log_versions (version, info) values ('v25.02.06', 'Se añade control de cliente activo al realizar oferta, pedido, albarán');
update empresa set vcs = 'v25.02.06' where codigo = 1;
-- **** v25.02.19 **** --
insert into log_versions (version, info) values ('v25.02.19', 'Se corrige la comprobación del estado del cliente en ofertas, pedidos y albaranes');
insert into log_versions (version, info) values ('v25.02.19', 'Se corrige el comportamiento de los botones de los pedidos a proveedor');
update empresa set vcs = 'v25.02.19' where codigo = 1;
-- **** v25.02.19 **** --
update empresa set vcs = 'v25.02.19' where codigo = 1;
-- **** v25.03.06 **** --
insert into log_versions (version, info) values ('v25.03.06', 'Se cambia el logo en los documentos');
update empresa set vcs = 'v25.03.06' where codigo = 1;
-- **** v25.03.06 **** --
update empresa set vcs = 'v25.03.06' where codigo = 1;
-- **** v25.03.06 **** --
update empresa set vcs = 'v25.03.06' where codigo = 1;
-- **** v25.03.06 **** --
update empresa set vcs = 'v25.03.06' where codigo = 1;
-- **** v25.03.06 **** --
update empresa set vcs = 'v25.03.06' where codigo = 1;
-- **** v25.03.06 **** --
update empresa set vcs = 'v25.03.06' where codigo = 1;
-- **** v25.03.06 **** --
update empresa set vcs = 'v25.03.06' where codigo = 1;
