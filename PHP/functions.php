<?php

use setasign\Fpdi\Tcpdf\Fpdi;

/**
 * @param string $name
 */
function incoLogSetFile($name) {
	if (empty($name)) {
		$name = 'default';
	}
	ini_set('error_log',  sprintf('%s%s.log', INCO_DIR_LOG, $name));
}

/**
 * @param string $msg
 * @param bool $die
 */
function incoLogWrite(string $msg, $die = true) {
	error_log($msg);
	if ($die) die();
}

/**
 * @param array $db_config
 * @param string $glue
 *
 * @return string
 */
function incoDbGetString($db_config, $glue = ' ') {
	$parts = [];
	foreach ($db_config as $key => $value) {
		array_push($parts, $key .'=' . $value);
	}
	return implode($glue, $parts);
}

/**
 * @param array $db_config
 *
 * @return resource
 */
function incoDbGetConnection($db_config) {
	$conn = pg_connect(incoDbGetString($db_config));
	if ($conn === false) {
		incoLogWrite( 'No se ha podido establecer la conexión con la base de datos: ' . json_encode($db_config));
	}
	return $conn;
}

/**
 * @param string $num_ped
 * @param string $producto
 * @param string $actividad
 * @param string $nave
 * @param string $maquina
 *
 * @return string
 */
function incoBarcodeGenerate($num_ped, $producto, $actividad, $nave, $maquina) {
	// CODBAR: [NUM PED:6][PRODUCTO:3][ACTIVIDAD:2][NAVE:1][MAQUINA:2]

	$num_ped = trim($num_ped);
	$producto = trim($producto);
	$actividad = trim($actividad);
	$nave = trim($nave);
	$maquina = trim($maquina);

	if (strlen($num_ped) > 6) $num_ped = substr($num_ped, 0, 6);
	if (strlen($producto) > 3) $producto = substr($producto, 0, 3);
	if (strlen($actividad) > 2) $actividad = substr($actividad, 0, 2);
	if (strlen($nave) > 1) $nave = substr($nave, 0, 1);
	if (strlen($maquina) > 2) $maquina = substr($maquina, 0, 2);

	return str_pad($num_ped, 6, '0', STR_PAD_LEFT)
	       . str_pad($producto, 3, '0', STR_PAD_LEFT)
	       . str_pad($actividad, 2, '0', STR_PAD_LEFT)
	       . $nave
	       . str_pad($maquina, 2, '0', STR_PAD_LEFT);
}

/**
 * @param Fpdi $pdf
 *
 * @return Fpdi
 */
function incoPdfAddCGV($pdf, $idioma = 'es')
{
    $filename = INCO_DIR_DOCUMENTOS . sprintf('CGV_%s.pdf', trim($idioma));
    if (!file_exists($filename)) return $pdf;

    $pdf->endPage();
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    try {
        $pageCount = $pdf->setSourceFile($filename);
        for ($i = 1; $i <= $pageCount; $i++) {
            $tplIdx = $pdf->importPage($i);
            $pdf->AddPage();
            $pdf->useTemplate($tplIdx);
        }
    } catch (Exception $exception) {
        incoLogWrite($exception->getMessage());
    }
    return $pdf;
}

/**
 * @param $email
 * @return bool
 */
function incoIsEmailValid($email) {
    if (empty($email)) return false;
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
}

/**
 * @param string $raw_body
 * @return array|false|string|string[]
 */
function incoMailClearBody($raw_body) {
    $body = iconv('windows-1252', 'utf-8', trim($raw_body));
    $body = str_replace('|', '<br>', $body);
    return $body;
}

/**
 * @param string|null $emails
 * @param string $separator
 *
 * @return array
 */
function incoGetEmails($emails, $separator = ';') {
    if (is_null($emails)) {
        return [];
    }

	$final_emails = explode($separator, trim($emails));
    return array_filter($final_emails, 'incoIsEmailValid');
}


/**
 * @param string $box
 *
 * @return string
 */
function incoImapGetMailbox($box = '') {
	return sprintf('{%s/imap/ssl/novalidate-cert}%s', INCO_IMAP_HOST, $box);
}

/**
 * @param string $user
 * @param string $password
 *
 * @return false|resource
 */
function incoImapOpen($user, $password) {
	return imap_open(incoImapGetMailbox(), $user, $password, null, 1, ['DISABLE_AUTHENTICATOR' => 'GSSAPI']);
}

/**
 * Para saber el nombre del buzón utilizar imap_list
 *
 * @param false|resource $resource
 * @param string $box
 * @param string $message_mime
 */
function incoImapAppend($resource, $box, $message_mime) {
	if ($resource === false) {
		return;
	}

	imap_append($resource, incoImapGetMailbox($box), $message_mime . "\r\n","\\Seen");
}

/**
 * @param false|resource $resource
 */
function incoImapClose($resource) {
	if ($resource === false) {
		return;
	}

	imap_close($resource);
}

/**
 * @param string $user
 * @param string $password
 * @param string $box
 * @param string $message_mime
 */
function incoImapStoreMessage($user, $password, $box, $message_mime) {
    return;

	$imapStream = incoImapOpen($user, $password);
	incoImapAppend($imapStream, $box, $message_mime);
	incoImapClose($imapStream);

	if ($imapStream === false) {
		incoLogWrite('El correo se ha enviado pero no se ha podido guardar en la carpeta ' . $box);
	}
}

/**
 * @param resource $dbconn
 * @param $comunicacion
 * @param int $codigo
 * @return array
 */
function incoSmtpGetSenderUser($dbconn, $comunicacion, $codigo = null) {
    if ($comunicacion['email_from'] == 0) {
        $sender['nombreusuario'] = 'INGENIERÍA Y CONSTRUCCIÓN DEL PERFIL S.A.';
        $sender['email'] = INCO_EMAIL_NOREPLY;
        $sender['passwdmail'] = INCO_EMAIL_NOREPLY_PASSWD;
        $sender['movil'] = '(+34) 961 211 778';
    } else {
        $result = pg_query_params($dbconn, 'select nombreusuario, movil, email, passwdmail from wusuario where codigo = $1', [$codigo]);
        if (pg_num_rows($result) <= 0) incoLogWrite('[incoSmtpGetSenderUser] No hay resultado');
        $sender = pg_fetch_all($result)[0];
        $sender['nombreusuario'] = ucwords(mb_strtolower(trim($sender['nombreusuario'])));
        $sender['email'] = trim($sender['email']);
        $sender['passwdmail'] = trim($sender['passwdmail']);
        $sender['movil'] = trim($sender['movil']);
    }
    return $sender;
}

function incoSmtpGetReceiverUsers($dbconn, $comunicacion, $codigo = 0, $die = true) {
    if (INCO_DEV_MODE) return [INCO_EMAIL_MESSAGE['delivery']];

    switch ($comunicacion['tipo']) {
        case 0: // interna
            $receivers = explode(INCO_EMAIL_MESSAGE_ADDRESS_SEPARATOR, trim($comunicacion['email_to']));
            break;
        case 1: // cliente
            $result = pg_query_params($dbconn, 'select trim(cc.nombre) as nombre, trim(cc.apellidos) as apellidos, trim(cc.email) as email, trim(cc.telefono) as telefono from cli_contactos_comunicaciones ccc, clientes_contactos cc where ccc.comunicacion = $1 and ccc.contacto = cc.codigo and cc.cliente = $2 and cc.estado = 1', [$comunicacion['codigo'], $codigo]);
            if (pg_num_rows($result) <= 0) {
                incoLogWrite('[incoSmtpGetReceiverUsers - contacto/s cliente] No hay resultado', $die);
                if (!$die) return [];
            }
            $receivers = pg_fetch_all($result);
            break;
        case 2: // proveedor
            $result = pg_query_params($dbconn, 'select trim(pc.nombre) as nombre, trim(pc.apellidos) as apellidos, trim(pc.email) as email, trim(pc.telefono) as telefono from prov_contactos_comunicaciones pcc, proveedores_contactos pc where pcc.comunicacion = $1 and pcc.contacto = pc.codigo and pc.proveedor = $2 and pc.estado = 1', [$comunicacion['codigo'], $codigo]);
            if (pg_num_rows($result) <= 0) {
                incoLogWrite('[incoSmtpGetReceiverUsers - contacto/s proveedor] No hay resultado', $die);
                if (!$die) return [];
            }
            $receivers = pg_fetch_all($result);
            break;
        default:
            incoLogWrite('Tipo de comunicación no reconocido');
            $receivers = [];
            break;
    }
    if (in_array($comunicacion['tipo'], [1,2])) {
        /*$receivers = array_map(function ($contacto) {
            return [$contacto['email'] => implode(' ', array_map('trim', [$contacto['nombre'], $contacto['apellidos']]))];
        }, $receivers);*/
        $receivers = array_values(array_column($receivers, 'email'));
    }

    return $receivers;
}

function incoSmtpParseDynamicTags($text) {
    $datetime = new DateTime();
    return str_replace(
        [incoSmtpGetTemplateField('fecha'), incoSmtpGetTemplateField('hora')],
        [$datetime->format('d/m/Y'), $datetime->format('H:i')],
        $text
    );
}

/**
 * @param string $user
 * @param string $password
 *
 * @return Swift_SmtpTransport
 */
function incoSmtpGetTransport($user, $password) {
	return (new Swift_SmtpTransport(INCO_SMTP_HOST, INCO_SMTP_PORT))
		->setUsername($user)
		->setPassword($password);
}

/**
 * @param string $user
 * @param string $password
 *
 * @return Swift_Mailer
 */
function incoSmtpGetMailer($user, $password) {
	return new Swift_Mailer(incoSmtpGetTransport($user, $password));
}

/**
 * @return Swift_Mailer
 */
function incoSmtpGetSpoolMailer() {
	try {
		$spool = new Swift_FileSpool( INCO_DIR_MAILS_SPOOL );
	} catch (Swift_IoException $e) {
		incoLogWrite('Error al crear el spool. ' . $e->getTraceAsString());
	}

	$spool_transport = new Swift_SpoolTransport($spool);

	return new Swift_Mailer($spool_transport);
}

/**
 * @param $template
 *
 * @return string
 */
function incoSmtpGetTemplate($template) {
	if (!isset(INCO_EMAIL_MESSAGE['templates'][$template])) {
		incoLogWrite('La template de correo a utilizar no existe: ' . $template);
	}
	return file_get_contents(INCO_EMAIL_MESSAGE['templates'][$template]);
}

/**
 * @param string $field
 *
 * @return string
 */
function incoSmtpGetTemplateField($field) {
	return sprintf('%s%s%s', INCO_EMAIL_MESSAGE['field_enclosure'], $field, INCO_EMAIL_MESSAGE['field_enclosure']);
}

/**
 * @param Swift_Message $message
 * @param string $template
 * @param array $data
 *
 * @return string
 */
function incoSmtpGetBody($message, $template, $data) {
	$body = incoSmtpGetTemplate($template);

    $date = new DateTime();
    $data['fecha'] = $date->format(INCO_DATE_FORMAT);
    $data['hora'] = $date->format(INCO_TIME_FORMAT);
    $data['fecha.hora'] = $date->format(INCO_DATETIME_FORMAT);

	foreach ( $data as $field => $value ) {
		$body = str_replace(incoSmtpGetTemplateField($field), $value, $body);
	}

	foreach (INCO_EMAIL_MESSAGE['fields'] as $field => $default_value ) {
		if ( $field == 'SRC_IMAGE' ) {
			$default_value = $message->embed(Swift_Image::fromPath($default_value));
		}
		$body = str_replace(incoSmtpGetTemplateField($field), $default_value, $body);
	}

	return $body;
}

/**
 * @param Swift_Mailer $mailer
 * @param string $subject
 * @param array $raw_body_data
 * @param array|string $from
 * @param array|string $to
 * @param array $attachments
 * @param array|string $cc
 * @param array|string $bcc
 * @return array
 */
function incoSmtpSendMail_NoReply($mailer, $subject, $raw_body_data, $from, $to, $attachments = [], $cc = [], $bcc = []) {
    $raw_body = [
        'template' => 'noreply',
        'data' => $raw_body_data
    ];
    return incoSmtpSendMail($mailer, $subject, $raw_body, $from, $to, $attachments, $cc, $bcc);
}

/**
 * @param Swift_Mailer $mailer
 * @param string $subject
 * @param array $raw_body_data
 * @param array|string $from
 * @param array|string $to
 * @param array $attachments
 * @param array|string $cc
 * @param array|string $bcc
 * @return array
 */
function incoSmtpSendMail_Usuario($mailer, $subject, $raw_body_data, $from, $to, $attachments = [], $cc = [], $bcc = []) {
    $raw_body = [
        'template' => 'usuario',
        'data' => $raw_body_data
    ];
    return incoSmtpSendMail($mailer, $subject, $raw_body, $from, $to, $attachments, $cc, $bcc);
}

/**
 * @param Swift_Mailer $mailer
 * @param string $subject
 * @param array $raw_body
 * @param array|string $from
 * @param array|string $to
 * @param array $attachments
 * @param array|string $cc
 * @param array|string $bcc
 * @return array
 */
function incoSmtpSendMail($mailer, $subject, $raw_body, $from, $to, $attachments = [], $cc = [], $bcc = []) {
    $message = new Swift_Message($subject, null, 'text/html');

    $body = incoSmtpGetBody($message, $raw_body['template'], $raw_body['data']);

    if (INCO_DEV_MODE) {
        $to = INCO_EMAIL_MESSAGE['delivery'];
        if (empty($to)) {
            incoLogWrite('No hay un email definido para DEV');
        }
    }
    $message->setBody($body)
        ->setFrom($from)
        ->setTo($to);

    foreach ($attachments as $attachment) {
        $message->attach(Swift_Attachment::fromPath($attachment));
    }

    if (!empty($cc)) $message->setCc($cc);
    if (!empty($bcc)) $message->setCc($bcc);

    $sends = $mailer->send($message);
    return [$message, $sends];
}

include_once __DIR__ . '/functions/db.php';
include_once __DIR__ . '/functions/i18n.php';
include_once __DIR__ . '/functions/mailrelay.php';
include_once __DIR__ . '/functions/paths.php';