<?php

require_once __DIR__ . '/../config.php';

class testi18n extends TCPDF {

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4', $unicode = true, $encoding = 'UTF-8', $diskcache = false, $pdfa = false) {
        parent::__construct($orientation, $unit, $format, $unicode, $encoding, $diskcache, $pdfa);

        $this->AddPage();
        $this->Cell(0, 4, gettext('Factura'), 1, 1);
    }


    public function Header() {
    }

    public function Footer() {
    }

}

incoI18nSetLang('en', 'factura');

$pdf = new testi18n();
$pdf->Output(__DIR__ . '/testi18n.pdf', 'F');