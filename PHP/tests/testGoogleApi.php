<?php

require_once __DIR__ . '/../vendor/autoload.php';

session_start();

$REDIRECT_URI = __FILE__; // 'http://localhost:8080';
$KEY_LOCATION = __DIR__ . '/../docs/client_secret_1023110302529-a21p7bn9h8u882851nbnvf6qstivpa8p.apps.googleusercontent.com.json';
$TOKEN_FILE = "token.txt";

$SCOPES = array(
    Google_Service_Forms::FORMS_RESPONSES_READONLY
);

$client = new Google\Client();
$client->setApplicationName("erp-crm Application");
$client->setAuthConfig($KEY_LOCATION);

// Incremental authorization
$client->setIncludeGrantedScopes(true);

// Allow access to Google API when the user is not present.
$client->setAccessType('offline');
$client->setRedirectUri($REDIRECT_URI);
$client->setScopes($SCOPES);

if (isset($_GET['code']) && !empty($_GET['code'])) {
    try {
        // Exchange the one-time authorization code for an access token
        $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        echo $accessToken . PHP_EOL;

        // Save the access token and refresh token in local filesystem
        file_put_contents($TOKEN_FILE, json_encode($accessToken));

        $_SESSION['accessToken'] = $accessToken;
        header('Location: ' . filter_var($REDIRECT_URI, FILTER_SANITIZE_URL));
        exit('1');
    } catch (\Google_Service_Exception $e) {
        print_r($e);
    }
}

if (!isset($_SESSION['accessToken'])) {
    $token = @file_get_contents($TOKEN_FILE);
    if ($token == null) {
        // Generate a URL to request access from Google's OAuth 2.0 server:
        $authUrl = $client->createAuthUrl();
        echo $authUrl . PHP_EOL;

        // Redirect the user to Google's OAuth server
        header('Location: ' . filter_var($authUrl, FILTER_SANITIZE_URL));
        exit('2');
    } else {
        $_SESSION['accessToken'] = json_decode($token, true);
    }
}

$client->setAccessToken($_SESSION['accessToken']);

/* Refresh token when expired */
if ($client->isAccessTokenExpired()) {
    // the new access token comes with a refresh token as well
    $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
    file_put_contents($TOKEN_FILE, json_encode($client->getAccessToken()));
}

$forms = new \Google\Service\Forms($client);

var_dump($forms);
