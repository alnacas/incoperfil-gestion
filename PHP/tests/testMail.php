<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile('test');

/*$idioma = $argv[1];
if (is_null($idioma) || empty($idioma)) $idioma = 'es';
$tr = new IncoTranslator($DB_STRING, $idioma);
$des = trim($argv[2]);
// $des = 'PERFIL INCO 70.4 COLLABORANT 1,00 mm GALVANIZADO Acero S320GD +Z200';
echo $tr->getTranslate($des) . PHP_EOL;*/

if (!extension_loaded('openssl')) {
    incoLogWrite('SSL not loaded');
}

$name = 'purebas';
$movil = '0000000';
$mail = INCO_EMAIL_NOREPLY;
$pass = INCO_EMAIL_NOREPLY_PASSWD;

$mailer = incoMailrelayGetMailer();

$message = new Swift_Message('TEST MAILS | ' . $name);
$msgBody = trim('Hola mundo');
$msgBody = str_replace('|', '<br>', $msgBody);
$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgBody,
    'NOMBRE' => $name,
    'MOVIL' => $movil,
    'MAIL' => $mail,
]);
$message->setFrom([$mail => $name])
    // ->setBcc([$mail => $name])
    ->setTo(INCO_EMAIL_MESSAGE['delivery'])
    ->setBody($body, 'text/html');

$res = $mailer->send($message);

// incoImapStoreMessage($mail, $pass, INCO_IMAP_SENT_BOX, $message->toString());

echo json_encode($res) . PHP_EOL;
