<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile('test');

if (!extension_loaded('openssl')) {
    incoLogWrite('SSL not loaded');
}

$mailer = incoSmtpGetMailer(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD);

// Create a message
$message = new Swift_Message('TEST MAILS | Probando las funciones');

$msgBody = trim('Hola mundo');
$msgBody = str_replace('|', '<br>', $msgBody);
$body = incoSmtpGetBody($message, 'noreply', [ 'BODY' => $msgBody]);

$message->setFrom(INCO_EMAIL_NOREPLY)
    ->setTo(INCO_EMAIL_MESSAGE['delivery'])
    ->setBody($body, 'text/html');

// Send the message
$res = $mailer->send($message);

// incoImapStoreMessage($mail, $pass, INCO_IMAP_SENT_BOX, $message->toString());
