<?php


require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../functions.php';

ini_set('error_log', __DIR__ . '/error.log');

$dotenv = Dotenv\Dotenv::createMutable(__DIR__ . '/..');
$dotenv->load();

$dotenv->required(['APP_ENV'])->notEmpty()->allowedValues(['dev', 'prod']);

echo '--- DEFAULT ENV ---' . PHP_EOL;
echo $_ENV['APP_ENV'] . PHP_EOL;

if (!file_exists(__DIR__ . '/../.env.' . $_ENV['APP_ENV'])) {
    incoLogWrite('no están las variables de entorno');
}

echo '--- ' . $_ENV['APP_ENV'] . ' ENV ---' . PHP_EOL;
$dotenv = Dotenv\Dotenv::createMutable(__DIR__ . '/..', '.env.' . $_ENV['APP_ENV']);
$dotenv->load();

echo $_ENV['PROJECT_DIR'] . PHP_EOL;

$dotenv->required(['PROJECT_DIR'])->notEmpty();
$dotenv->required(['DB_LOCAL_HOST', 'DB_LOCAL_DBNAME', 'DB_LOCAL_USER', 'DB_LOCAL_PASSWORD'])->notEmpty();
// $dotenv->required(['DB_REMOTE_HOST', 'DB_REMOTE_DBNAME', 'DB_REMOTE_USER', 'DB_REMOTE_PASSWORD'])->notEmpty();
$dotenv->required(['IMAP_HOST', 'IMAP_SENT_BOX', 'SMTP_HOST'])->notEmpty();
$dotenv->required(['SMTP_PORT'])->isInteger();
$dotenv->required(['USER_NOREPLY', 'USER_NOREPLY_PASSWD', 'USER_NOREPLY_NOMBRE'])->notEmpty();
$dotenv->required(['EMAIL', 'EMAIL_NOMBRE', 'EMAIL_MOVIL', 'EMAIL_LANG', 'EMAIL_CHARSET', 'EMAIL_IMAGE'])->notEmpty();