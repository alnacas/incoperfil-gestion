<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile('test_relay');

/*$idioma = $argv[1];
if (is_null($idioma) || empty($idioma)) $idioma = 'es';
$tr = new IncoTranslator($DB_STRING, $idioma);
$des = trim($argv[2]);
// $des = 'PERFIL INCO 70.4 COLLABORANT 1,00 mm GALVANIZADO Acero S320GD +Z200';
echo $tr->getTranslate($des) . PHP_EOL;*/

$name = 'Arturo Sánchez';
$movil = '657893999';
$mail = INCO_EMAIL_NOREPLY;

$sender['nombreusuario'] = 'INGENIERÍA Y CONSTRUCCIÓN DEL PERFIL S.A.';
$sender['email'] = 'joseginer@incoperfil.com';
$sender['movil'] = '(+34) 961 211 778';

$mailer = incoMailrelayGetMailer();

$message = new Swift_Message('Hola');

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => 'prueba',
    'NOMBRE' => $sender['nombreusuario'],
    'MOVIL' => $sender['movil'],
    'MAIL' => $sender['email'],
]);

$message->setFrom('joseginer@incoperfil.com')
    ->setTo(INCO_EMAIL_MESSAGE['delivery'])
    ->setBody($body, 'text/html');
$sent = $mailer->send($message);

echo json_encode($sent) . PHP_EOL;
