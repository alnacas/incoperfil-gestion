<?php

use setasign\Fpdi\Tcpdf\Fpdi;

require_once __DIR__ . '/../config.php';

$pdf = new Fpdi();

$pdf->AddPage();
$pdf->Cell(0, 4, 'Hola');

//Merging of the existing PDF pages to the final PDF
if (file_exists(INCO_DIR_DOCUMENTOS . 'CGV.pdf')) {
    try {
        $pageCount = $pdf->setSourceFile(INCO_DIR_DOCUMENTOS . 'CGV.pdf');
        for ($i = 1; $i <= $pageCount; $i++) {
            $tplIdx = $pdf->importPage($i);
            $pdf->AddPage();
            $pdf->useTemplate($tplIdx);
        }
    } catch (Exception $exception) {
        error_log($exception->getMessage());
    }
}

//Your code relative to the invoice here

$pdf->Output(__DIR__ . '/test.pdf', 'F');