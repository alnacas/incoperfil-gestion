<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}
$pdf = empty($argv[3]) ? '' : str_replace('\\', '/', $argv[3]);
$coduser = intval($argv[4]);
$msgbody = trim($argv[5]);
$msgbody = str_replace('|', '<br>', $msgbody);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 4]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];

// obtenemos la oferta
$result = pg_query_params('SELECT potencial, cliente, ejercicio, numero, revision, fecha, contacto, mail FROM ofertas WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ofertas] No hay resultado');
$oferta = pg_fetch_all($result)[0];

/*$tipo = $oferta['potencial'] ? INCO_DIR_POTENCIALES : INCO_DIR_CLIENTES;
$oferta['pdf'] = $tipo . sprintf('%d/Ofertas/O_%s_%06d/O_%s_%06d_%d.pdf', $oferta['cliente'], $oferta['ejercicio'], $oferta['numero'], $oferta['ejercicio'], $oferta['numero'], $oferta['revision']);*/

// obtenemos el usuario
/*$result = pg_query_params('SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', array($coduser)) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];*/
$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion, $coduser);

$receivers = incoSmtpGetReceiverUsers($dbconn, $comunicacion, $oferta['cliente']);

/*$fecha = $oferta['fecha']; // date_format(date_create($oferta['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$contacto = trim($oferta['contacto']);
$email = trim($oferta['mail']);
$numero = intval($oferta['numero']);*/
$revision = intval($oferta['revision']);
/*if (!incoIsEmailValid($email)) {
    incoLogWrite('El email no es válido');
}/

/*$userName = trim($usuario['nombreusuario']);
$userName = ucwords(mb_strtolower($userName));
$userEmail = trim($usuario['email']);
$userPasswd = trim($usuario['passwdmail']);
$userMovil = trim($usuario['movil']);*/

/*if (is_null($contacto) || empty($contacto)) {
    $contacto = 'cliente';
} else {
    $aux = explode(' ', $contacto);
    $contacto = '';
    for ($i = 0; $i < count($aux); $i++) {
        if ($i < count($aux) - 1) {
            $contacto .= strtoupper(substr($aux[$i], 0, 1)) . strtolower(substr($aux[$i], 1)) . ' ';
        } else {
            $contacto .= strtoupper(substr($aux[$i], 0, 1)) . strtolower(substr($aux[$i], 1));
        }
    }
}*/

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

if ($revision > 0) $revision = ' Revisión ' . $revision;
$subject = str_replace(
    [incoSmtpGetTemplateField('ofertas.numero'), incoSmtpGetTemplateField('ofertas.revision')],
    [$oferta['numero'], $revision],
    $comunicacion['email_subject']
);
$message = new Swift_Message($subject);

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $senderUser['nombreusuario'],
    'MOVIL' => $senderUser['movil'],
    'MAIL' => $senderUser['email'],
]);

/*$message->setFrom([$userEmail => $userName])
    ->setTo([$email => $contacto])
    ->setBody($body, 'text/html');*/
$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($receivers)
    ->setBody($body, 'text/html');
if (!empty($pdf)) {
    $message->attach(Swift_Attachment::fromPath($pdf));
}
if (!INCO_DEV_MODE && $comunicacion['tipo'] == 0) {
    if ($comunicacion['email_cc_interna']) $message->setCc($comunicacion['email_cc_interna']);
}

$mailer->send($message);

// incoImapStoreMessage($userEmail, $userPasswd, INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);