<?php

// php mailSolicitudFactE.php [uniqueid] [mensajeria];
// crontab: php /home/gesic21/PHP/mails/mailSolicitudFactE.php mailSolicitudFactE '0,1,2,3' 0

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$mensajeria = $argv[2];
$codCliente = intval($argv[3]);
if (empty($mensajeria) && strlen($mensajeria) <= 0 && $codCliente == null) {
	incoLogWrite('Faltan argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);
$remoteConn = incoDbGetConnection(INCO_DB_REMOTE);

// DESCARGAMOS LOS DATOS QUE SE HAYAN MODIFICADO POR LOS CLIENTES
$res = pg_query($remoteConn, 'select codigo, mensajeria, email_factura, persona_acepta, fecha_actualiza from clientes order by codigo');
if (!$res) incoLogWrite('No se ha podido ejecutar la query');
if (pg_num_rows($res) <= 0) incoLogWrite('No se han encontrado resultados');
$clientes = pg_fetch_all($res);
foreach ( $clientes as $cliente ) {
	$remoteFechaActualiza = DateTime::createFromFormat('Y-m-d', $cliente['fecha_actualiza']) or false;

	$res = pg_query_params($dbconn, 'select fecha_actualiza from clientes where codigo = $1', [$cliente['codigo']]);
	if (!$res) continue;
	if (pg_num_rows($res) <= 0) continue;
	$localFechaActualiza = DateTime::createFromFormat('Y-m-d', pg_fetch_all($res)[0]['fecha_actualiza']) or false;

	if ($localFechaActualiza >= $remoteFechaActualiza) continue;

	$data = [
		'mensajeria' => $cliente['mensajeria'],
		'email_factura' => $cliente['email_factura'],
		'fecha_actualiza' => $cliente['fecha_actualiza']
	];
	$condition = [
		'codigo' => $cliente['codigo']
	];
	$updated = pg_update($dbconn, 'clientes', $data, $condition);
	if (!$updated) {
		incoLogWrite( 'No se ha podido acualizar el cliente ' . $cliente['codigo'] . '. ' . pg_last_error($dbconn));
	}

	if (trim($cliente['persona_acepta'])) {
		$res = pg_query_params($dbconn, 'select max(linea) as linea from clientesobs where codigo = $1', [$cliente['codigo']]);
		if (!$res || pg_num_rows($res) <= 0) continue;
		$clienteobs = pg_fetch_all($res)[0];
		$linea = is_null($clienteobs['linea']) ? 1 : intval($clienteobs['linea']) + 1;
		if (!$clienteobs) continue;
		pg_insert($dbconn, 'clientesobs', [
			'codigo' => $cliente['codigo'],
			'linea' => $linea,
			'fecha' => $cliente['fecha_actualiza'],
			'usuario' => 0,
			'observacion' => $cliente['persona_acepta'] . ' ha cambiado la forma de envío de facturas'
		]);
	}
}

// OBTENEMOS LOS CLIENTES QUE SE VAN A SUBIR, BORRANDO PRIMERO LOS QUE HAYAN
$res = pg_query($dbconn, 'select codigo, nif as cif, nombre, idioma, mensajeria, email_factura from clientes where estado = 1 and codigo > 0');
if (!$res) incoLogWrite('No se ha podido ejecutar la query');
if (pg_num_rows($res) <= 0) incoLogWrite('No se han encontrado resultados');
$clientes = pg_fetch_all($res);
pg_prepare($remoteConn, 'truncate_clientes', 'truncate table clientes') or incoLogWrite('No se ha podido preparar la sententica');
pg_execute($remoteConn, 'truncate_clientes', []) or incoLogWrite('No se ha podido ejecutar la sententica');
foreach ( $clientes as $cliente ) {
	if (!isset($cliente['persona_acepta'])) {
		$cliente['persona_acepta'] = '';
	}
	$inserted = pg_insert($remoteConn, 'clientes', $cliente);
	if (!$inserted) {
		incoLogWrite( 'No se ha podido insertar el cliente ' . $cliente['codigo'] . '. ' . pg_last_error($dbconn));
	}
}

// MANDAMOS LA SOLICITUD DE FACTURA ELECTRONICA
/*$result = pg_query($dbconn, 'SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = 0 LIMIT 1') or incoLog('La consulta fallo [wusuario]: ' . pg_last_error($dbconn));
if (pg_num_rows($result) <= 0) incoLog('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];
$usuario['nombreusuario'] = ucwords(mb_strtolower(trim($usuario['nombreusuario'])));
$usuario['email'] = trim($usuario['email']);
$usuario['passwdmail'] = trim($usuario['passwdmail']);
$usuario['movil'] = trim($usuario['movil']);*/

if ($codCliente <= 0) {
	$numClientes = pg_fetch_all(pg_query('select count(*) as total from clientes where estado = 1'))[0]['total'];
	$file = __DIR__ . '/mailSolicitudFactE_offset.txt';
	$offset = file_exists($file) ? file_get_contents($file) : 0;
	if ($offset >= $numClientes) $offset = 0;
	$result = pg_query_params( $dbconn, 'select codigo, emailcom, emailadm from clientes where estado = 1 and mensajeria in (' . $mensajeria . ') limit 100 offset $1', [$offset] ) or incoLogWrite( 'La consulta fallo [clientes]' );
	file_put_contents($file, $offset + 100);
} else {
	$result = pg_query_params( $dbconn, 'select codigo, emailcom, emailadm from clientes where codigo = $1', [$codCliente]) or incoLogWrite( 'La consulta fallo [clientes]' );
}
if (pg_num_rows($result) <= 0) incoLogWrite('[clientes] No hay resultados con los filtros aplicados');
$clientes = pg_fetch_all($result);

$result = pg_query_params($dbconn, 'SELECT nombreusuario, trim(email) as email, trim(passwdmail) as passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', [INCO_EMAIL_COMPRAS_ID]) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario_gloria = pg_fetch_all($result)[0];

$subject = 'INCOPERFIL | Solicitud envío de facturas electrónicas';
$pattMsgBody = '<!DOCTYPE html><html lang="es">
	<head>
	    <title></title>
		<meta charset="utf-8">
	</head>
	<body>
		<div style="text-align: center;">
			<table style="display: inline-table; text-align: justify; width: 528.85pt; border-collapse: collapse;">
				<tr style="background-color: #FBFBFB; border-top: solid 1px #C00000;">
					<td style="padding: 15pt; font-family: Calibri, sans-serif; font-size: 11pt; color: #0d0d0d" colspan="2">
						Estimado cliente,<br><br>Desde INCOPERFIL le informamos que ya podemos enviar la factura electrónica. Rellene este sencillo formulario y disfrute de todas las ventajas de la factura electrónica:
						<p style="text-align: center; margin-top: 2rem;"><a href="https://apps.incoperfil.com/envio-facturas/?cliente=$CODIGO$" style="padding: 10px 25px; background-color: #c41100; color: #FFF; text-decoration: none;">Formulario</a></p>
						<p><img width="100%" src="$SRC_FACTE$" alt="Ventajas Factura Electrónica"></p>
					</td>
				</tr>
				<tr style="background-color: #C00000; color: #FBFBFB;">
					<td style="padding: 10pt; font-family: Calibri, sans-serif; font-size: 11pt; line-height: 12pt;">
						<b>INCOPERFIL</b><br>
						C/ NOU, 16<br>
						46469 BENIPARRELL (VALENCIA)<br>
						Tel.  96 1211778<br>
						<a href="https://www.incoperfil.com" style="color: #FBFBFB;">www.incoperfil.com</a>
					</td>
					<td style="text-align: center; width: 22%;">
						<img src="$SRC_IMAGE$" style="width: 80%; padding-right: 30px;" alt="INGENIERIA Y CONSTRUCCION DEL PERFIL S.A."/>
					</td>
				</tr>
				<tr>
					<td style="padding: 15pt; font-family: Calibri, sans-serif; font-size: 7pt; color: gray;" colspan="2">
						<p style="padding: 0; margin: 0 0 12pt 0;">&nbsp;|&nbsp;&nbsp;&nbsp;NOTA LEGAL&nbsp;&nbsp;&nbsp;|&nbsp;</p>
						<p style="padding: 0; margin: 0; line-height: 13pt;">La información contenida en este correo electrónico, es de carácter privado y confidencial, siendo para uso exclusivo de su destinatario. Si usted no es el destinatario correcto, o ha recibido esta comunicación por error, le informamos que está totalmente prohibida cualquier divulgación, distribución o reproducción de esta comunicación según la legislación vigente y le rogamos que nos lo notifique inmediatamente, procediendo a su destrucción sin continuar su lectura. Le informamos que su dirección de correo electrónico, así como el resto de los datos de carácter personal que nos facilite, podrían ser objeto de tratamiento automatizado en nuestros ficheros, con la finalidad de gestionar la agenda de contactos de Ingeniería y Construcción del Perfil S.A. Usted podrá en cualquier momento ejercer el derecho de acceso, rectificación, cancelación y oposición según la Ley Orgánica 15/1999 mediante notificación escrita a la siguiente dirección: Pol. Ind Mas del Polio, Calle 16, 46469 Beniparrell</p>
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>';

$spool = new Swift_FileSpool( __DIR__ . '/spool' );
$spool_transport = new Swift_SpoolTransport($spool);
$mailer = new Swift_Mailer($spool_transport);

foreach ( $clientes as $cliente ) {
	$to = [];
    if (incoIsEmailValid(trim($cliente['emailcom']))) array_push($to, trim($cliente['emailcom']));
    if (incoIsEmailValid(trim($cliente['emailadm']))) array_push($to, trim($cliente['emailadm']));
	if (empty($to)) {
		if ($codCliente <= 0) {
			continue;
		} else {
			incoLogWrite('El cliente no tiene correos en la ficha al que enviar la solicitud de factura electrónica');
		}
	}

	$message = new Swift_Message($subject);
	$body = str_replace('$CODIGO$', $cliente['codigo'], $pattMsgBody);
	$body = str_replace('$SRC_FACTE$', $message->embed(Swift_Image::fromPath(INCO_DIR_IMAGENES . 'facte.png')), $body);
	$body = str_replace('$SRC_IMAGE$', $message->embed(Swift_Image::fromPath(INCO_DIR_IMAGENES . 'logo_blanco.png')), $body);
	$message->setFrom(INCO_EMAIL_NOREPLY)
	        ->setTo($to)
            // ->setCc('compras@incoperfil.com')
	        ->setBody($body, 'text/html');
	$mailer->send($message);

    incoImapStoreMessage($usuario_gloria['email'], $usuario_gloria['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());
}

// $transport = incoSmtpGetTransport(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD);
$transport = incoMailrelayGetTransport();
$spool = $spool_transport->getSpool();
/*$spool->setMessageLimit(125);
$spool->setTimeLimit(60 * 60 * 24.25);*/
$sent = $spool->flushQueue($transport);

pg_close($remoteConn);
pg_close($dbconn);