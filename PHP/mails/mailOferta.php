<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}
$pdf = empty($argv[3]) ? '' : str_replace('\\', '/', $argv[3]);
$coduser = intval($argv[4]);
$msgbody = trim($argv[5]);
$msgbody = str_replace('|', '<br>', $msgbody);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos la oferta
$result = pg_query_params('SELECT numero, revision, fecha, contacto, mail FROM ofertas WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite('La consulta fallo [ofertas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ofertas] No hay resultado');
$oferta = pg_fetch_all($result)[0];

// obtenemos el usuario
$result = pg_query_params('SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', array($coduser)) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];

$fecha = $oferta['fecha']; // date_format(date_create($oferta['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$contacto = trim($oferta['contacto']);
$email = trim($oferta['mail']);
$numero = intval($oferta['numero']);
$revision = intval($oferta['revision']);
if (!incoIsEmailValid($email)) {
    incoLogWrite('El email no es válido');
}

$userName = trim($usuario['nombreusuario']);
$userName = ucwords(mb_strtolower($userName));
$userEmail = trim($usuario['email']);
$userPasswd = trim($usuario['passwdmail']);
$userMovil = trim($usuario['movil']);

if (is_null($contacto) || empty($contacto)) {
    $contacto = 'cliente';
} else {
    $aux = explode(' ', $contacto);
    $contacto = '';
    for ($i = 0; $i < count($aux); $i++) {
        if ($i < count($aux) - 1) {
            $contacto .= strtoupper(substr($aux[$i], 0, 1)) . strtolower(substr($aux[$i], 1)) . ' ';
        } else {
            $contacto .= strtoupper(substr($aux[$i], 0, 1)) . strtolower(substr($aux[$i], 1));
        }
    }
}

// $mailer = incoSmtpGetMailer($userEmail, $userPasswd);
$mailer = incoMailrelayGetMailer();

$subject = 'INCOPERFIL | Oferta ' . $numero;
if ($revision > 0) $subject .= ' Revisión ' . $revision;
$message = new Swift_Message($subject);

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $userName,
    'MOVIL' => $userMovil,
    'MAIL' => $userEmail,
]);

$message->setFrom([$userEmail => $userName])
    ->setTo([$email => $contacto])
    ->setBody($body, 'text/html');

if (!empty($pdf)) {
    $message->attach(Swift_Attachment::fromPath($pdf));
}

$mailer->send($message);

incoImapStoreMessage($userEmail, $userPasswd, INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);