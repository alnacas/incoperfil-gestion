<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$comunicacion = intval($argv[2]);
$codigo = $argv[3];
if ($comunicacion <= 0 || empty($codigo)) {
    incoLogWrite('No se han recibido todos los parámetros necesarios');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => $comunicacion]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];
$to = incoGetEmails($comunicacion['email_to']);
if (empty($to)) {
    incoLogWrite('No se han encontrado emails válidos');
}

// obtenemos el usuario
$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion);

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

$message = new Swift_Message(trim($comunicacion['email_subject']));

$msgbody = str_replace(
    [
        incoSmtpGetTemplateField('maquinas_cambio_bobina.codigo'),
        incoSmtpGetTemplateField('maquinas_cambio_programa.codigo'),
    ],
    [
        $codigo,
        $codigo,
    ],
    $comunicacion['email_body']
);
$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $senderUser['nombreusuario'],
    'MOVIL' => $senderUser['movil'],
    'MAIL' => $senderUser['email'],
]);

$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($to)
    ->setBody($body, 'text/html');

$mailer->send($message);

// incoImapStoreMessage($usuario['email'], $usuario['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);