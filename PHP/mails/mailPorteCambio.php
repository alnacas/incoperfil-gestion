<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$from_user = $argv[2];
$to_user = $argv[3];
$msg = utf8_encode(trim($argv[4]));

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

pg_prepare($dbconn, 'user', 'SELECT initcap(lower(trim(nombreusuario))) as nombre, trim(email) as email, trim(passwdmail) as passwdmail, trim(movil) as movil FROM wusuario WHERE codigo = $1 LIMIT 1');

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 2]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];

/*$result = pg_execute($dbconn, 'user', [$from_user]) or incoLogWrite( 'La consulta fallo [wusuario - from]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario - from] No hay resultado');
$from_user = pg_fetch_all($result)[0];*/
$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion, $from_user);

$result = pg_execute($dbconn, 'user', [$to_user]) or incoLogWrite( 'La consulta fallo [wusuario - to]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario - to] No hay resultado');
$to_user = pg_fetch_all($result)[0];
$receivers = incoSmtpGetReceiverUsers($dbconn, $comunicacion);
$field_deEmail = incoSmtpGetTemplateField('de.email');
if (in_array($field_deEmail, $receivers)) {
    $pos = array_search($field_deEmail, $receivers);
    $receivers[$pos] = $to_user['email'];
}

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

// $message = new Swift_Message('INCOPERFIL | Porte modificado');
// $message = new Swift_Message(sprintf('INCOPERFIL | %s', $comunicacion['email_subject']));
$message = new Swift_Message($comunicacion['email_subject']);

// $msgbody = '<p>PORTE MODIFICADO</p><p>' . $msg . '</p>';
$msgbody = str_replace(incoSmtpGetTemplateField('cosmos'), $msg, $comunicacion['email_body']);
$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $senderUser['nombreusuario'],
    'MOVIL' => $senderUser['movil'],
    'MAIL' => $senderUser['email'],
]);

/*$message->setFrom([$from_user['email'] => $from_user['nombre']])
        ->setTo([$to_user['email'] => $to_user['nombre']])
        ->setBody($body, 'text/html');*/
$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($receivers)
    ->setBody($body, 'text/html');
if (!INCO_DEV_MODE && $comunicacion['tipo'] == 0) {
    if ($comunicacion['email_cc_interna']) $message->setCc($comunicacion['email_cc_interna']);
}

$mailer->send($message);


// incoImapStoreMessage($from_user['email'], $from_user['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);
