<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$coduser = intval($argv[2]);
$identificador = $argv[3];
if (is_null($identificador) || empty($identificador)) {
	incoLogWrite('Se han recibido todos los parámetros necesarios');
}
$to = INCO_DEV_MODE ? [INCO_EMAIL_MESSAGE['delivery']] : incoGetEmails($argv[4]);
if (empty($to)) {
    incoLogWrite('No se han encontrado emails válidos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 8]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];

// obtenemos el usuario
$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion, $coduser);

// obtenemos el pedido
$result = pg_query_params('SELECT numero, numero_oferta, trim(referencia_obra) as referencia_obra, cliente FROM pedidos WHERE identificador = $1 LIMIT 1', [$identificador]) or incoLogWrite( 'La consulta fallo [pedidos_bobinas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos] No hay resultado');
$pedido = pg_fetch_all($result)[0];

$pdf = incoPathConfirmacionPedidoPdf($pedido['cliente'], $pedido['numero']);
if (!file_exists($pdf)) {
    incoLogWrite('El PDF de la confirmación del pedido no existe. PDF: ' . $pdf);
}

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

$subject = str_replace(incoSmtpGetTemplateField('pedidos.numero'), $pedido['numero'], $comunicacion['email_subject']);
$message = new Swift_Message($subject);

$msgbody = str_replace(
    [
        incoSmtpGetTemplateField('pedidos.numero'),
        incoSmtpGetTemplateField('ofertas.numero'),
        incoSmtpGetTemplateField('ofertas.referencia_obra'),
    ],
    [
        $pedido['numero'],
        $pedido['numero_oferta'],
        $pedido['referencia_obra']
    ],
    $comunicacion['email_body']
);
$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $senderUser['nombreusuario'],
    'MOVIL' => $senderUser['movil'],
    'MAIL' => $senderUser['email'],
]);

$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($to)
    ->setBody($body, 'text/html');
$message->attach(Swift_Attachment::fromPath($pdf));

$mailer->send($message);

// incoImapStoreMessage($usuario['email'], $usuario['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);