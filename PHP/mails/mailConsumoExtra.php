<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

if (empty($argv[2])) {
    incoLogWrite('No hay argumentos');
}
$msgBody = trim($argv[2]);
$msgBody = str_replace('|', '<br>', $msgBody);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 1]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];

$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion);

$receivers = incoSmtpGetReceiverUsers($dbconn, $comunicacion);

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

// $message = new Swift_Message('INCOPERFIL | Exceso de consumo');
$message = new Swift_Message($comunicacion['email_subject']);

$msgbody = str_replace(incoSmtpGetTemplateField('cosmos'), $msgBody, $comunicacion['email_body']);
$body = incoSmtpGetBody($message, 'noreply', ['BODY' => $msgBody]);

/*$message->setFrom(INCO_EMAIL_NOREPLY)
    ->setTo('joseginer@incoperfil.com')
    ->setBody($body, 'text/html');*/
$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($receivers)
    ->setBody($body, 'text/html');
if (!INCO_DEV_MODE && $comunicacion['tipo'] == 0) {
    if ($comunicacion['email_cc_interna']) $message->setCc($comunicacion['email_cc_interna']);
}

$mailer->send($message);

// incoImapStoreMessage(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD, INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);