<?php

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$copier = $argv[2];
$identificador = $argv[3];
$msgbody = incoMailClearBody($argv[4]);
$proveedor_codigo = intval($argv[5]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params($dbconn, 'select distinct p.codigo, p.email_solicitud, p.comercial from ofertas_bobinaslin ol, proveedores p where ol.identificador = $1 and ol.estado = 0 and ol.proveedor = p.codigo and p.codigo = $2', [$identificador, $proveedor_codigo]) or incoLogWrite( 'La consulta fallo [ofertas_bobinaslin - proveedores]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ofertas_bobinas - proveedores] No hay resultado');
$proveedor = pg_fetch_all($result)[0];

$result = pg_query_params($dbconn, 'SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', [INCO_EMAIL_COMPRAS_ID]) or incoLogWrite( 'La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$sender = pg_fetch_all($result)[0];
$sender['nombreusuario'] = ucwords(mb_strtolower(trim($sender['nombreusuario'])));
$sender['email'] = trim($sender['email']);
$sender['passwdmail'] = trim($sender['passwdmail']);
$sender['movil'] = trim($sender['movil']);

if ($copier == INCO_EMAIL_COMPRAS_ID) {
    $copier = null;
} else {
    $result = pg_query_params('SELECT nombreusuario, email FROM wusuario WHERE codigo = $1 LIMIT 1', [$copier]) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
    if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
    $copier = pg_fetch_all($result)[0];
    $copier['nombreusuario'] = ucwords(mb_strtolower(trim($copier['nombreusuario'])));
    $copier['email'] = trim($copier['email']);
}

$patt_subject = 'INCOPERFIL | Solicitud de precios de bobinas - OB%s';
$patt_pdf = INCO_DIR_PROVEEDORES . '%s/Ofertas/SP_%s.pdf';
$numero_oferta = intval(substr($identificador, -7));

// $mailer = incoSmtpGetMailer($sender['email'], $sender['passwdmail']);
$mailer = incoMailrelayGetMailer();

// foreach ($proveedores as $proveedor) {
	$to = incoGetEmails($proveedor['email_solicitud']);
	if (empty($to)) {
		incoLogWrite( 'El proveedor ' . $proveedor['codigo'] . ' no tiene email/s válidos para enviar la solicitud');
	}

	$pdf = sprintf($patt_pdf, $proveedor['codigo'], $numero_oferta);
	if (!file_exists($pdf)) {
		incoLogWrite( 'No existe el PDF de la oferta para el proveedor ' . $proveedor['codigo']);
	}

	$subject = sprintf($patt_subject, $numero_oferta);
	$message = new Swift_Message($subject);

    $body = incoSmtpGetBody($message, 'usuario', [
        'BODY' => $msgbody,
        'NOMBRE' => $sender['nombreusuario'],
        'MOVIL' => $sender['movil'],
        'MAIL' => $sender['email'],
    ]);

	$message->setFrom([$sender['email'] => $sender['nombreusuario']])
        ->setTo($to)
        ->setBcc($sender['email'])
		->setBody($body, 'text/html')
		->attach(Swift_Attachment::fromPath($pdf));
    if (!is_null($copier)) {
        $message->setCc([$copier['email'] => $copier['nombreusuario']]);
    }
	$mailer->send($message);

    incoImapStoreMessage($sender['email'], $sender['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());
// }

pg_close($dbconn);