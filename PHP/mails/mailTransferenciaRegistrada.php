<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$codTransfer = trim($argv[2]);
if (empty($codTransfer)) {
    incoLogWrite('No hay argumentos');
}
$codTransfer = intval($argv[2]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 3]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];

// obtenemos la transferencia
$result = pg_query_params($dbconn, 'SELECT t.aplicacion, t.numero, t.cliente, c.nombre, t.solicitado, t.importe, t.factura_anticipo, t.solicitante, u.nombreusuario, u.email FROM transferencias t, clientes c, wusuario u WHERE t.codigo = $1 AND t.cliente = c.codigo AND t.solicitante = u.codigo', array($codTransfer)) or incoLogWrite( 'La consulta fallo [transferencias]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[transferencias] No hay resultado');
$transferencia = pg_fetch_all($result)[0];
$transferencia['email'] = trim($transferencia['email']);

$result = pg_query_params($dbconn, 'select sum(importe) as importe from anticipos where cliente = $1 and tipo = 0 and aplicacion = 2', [intval($transferencia['cliente'])]) or incoLogWrite( 'La consulta fallo [anticipos generales]: ' . pg_last_error());
$extra = '';
if (pg_num_rows($result) > 0) {
    $importe = pg_fetch_all($result)[0]['importe'];
    if (!is_null($importe) && $importe > 0) {
        $extra = '<br><p>El cliente dispone de ' . $importe . ' € a su favor</p>';
    }
}

$aplicacion = '';
if (intval($transferencia['aplicacion']) == 0) $aplicacion = 'Oferta';
if (intval($transferencia['aplicacion']) == 1) $aplicacion = 'Pedido';

$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion);

$receivers = incoSmtpGetReceiverUsers($dbconn, $comunicacion);
$field_deEmail = incoSmtpGetTemplateField('de.email');
if (in_array($field_deEmail, $receivers)) {
    $pos = array_search($field_deEmail, $receivers);
    $receivers[$pos] = $transferencia['email'];
}

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

// $message = new Swift_Message('INCOPERFIL | Transferencia bancaria');
$message = new Swift_Message($comunicacion['email_subject']);

/*$msgBody = '<p>TRANSFERENCIA BANCARIA</p>';
$msgBody .= '<p>Cliente: ' . intval($transferencia['cliente']) . ' - ' . trim($transferencia['nombre']) . '</p>';
$msgBody .= '<p>' . $aplicacion . ': ' . intval($transferencia['numero']) . '</p>';
$msgBody .= '<p>Importe Solicitado: ' . number_format(floatval($transferencia['solicitado']), 2, ',', '.') . '</p>';
$msgBody .= '<p>Importe Recibido: ' . number_format(floatval($transferencia['importe']), 2, ',', '.') . '</p>';*/
$msgBody = str_replace(
    [
        incoSmtpGetTemplateField('clientes.numero'),
        incoSmtpGetTemplateField('clientes.nombre'),
        incoSmtpGetTemplateField('transferencias.aplicacion'),
        incoSmtpGetTemplateField('transferencias.numero'),
        incoSmtpGetTemplateField('transferencias.solicitado'),
        incoSmtpGetTemplateField('transferencias.importe'),
        incoSmtpGetTemplateField('cosmos'),
    ],
    [
        $transferencia['cliente'],
        trim($transferencia['nombre']),
        $aplicacion,
        $transferencia['numero'],
        number_format(floatval($transferencia['solicitado']), 2, ',', '.'),
        number_format(floatval($transferencia['importe']), 2, ',', '.'),
        $extra
    ],
    $comunicacion['email_body']
);
$body = incoSmtpGetBody($message, 'noreply', [ 'BODY' => $msgBody]);

/*$message->setFrom(INCO_EMAIL_NOREPLY)
    ->setTo($transferencia['email'])
    ->setBody($body, 'text/html');
if (intval($transferencia['solicitante']) != 4) {
    $message->setCc('comercial@incoperfil.com');
}*/
$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($receivers)
    ->setBody($body, 'text/html');
if (!INCO_DEV_MODE && $comunicacion['tipo'] == 0) {
    if ($comunicacion['email_cc_interna']) $message->setCc($comunicacion['email_cc_interna']);
}

$mailer->send($message);

// incoImapStoreMessage(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD, INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);