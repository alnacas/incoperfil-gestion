<?php

// php mailProcesoEnvioFacturas.php [uniqueid] [receiver] [subject] [body];

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

if (empty($argv[2]) or empty($argv[3])) {
    incoLogWrite('No hay argumentos');
}
$codigoReceiverUser = intval($argv[2]);
$subject = trim($argv[3]);
$msgBody = '<p>' . trim($argv[4]) . '</p>';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query_params('SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', [$codigoReceiverUser]) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) {
	incoLogWrite('[wusuario] No hay resultado');
}
$receiver = pg_fetch_all($result)[0];
$receiver['email'] = trim($receiver['email']);

// $mailer = incoSmtpGetMailer(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD);
$mailer = incoMailrelayGetMailer();

$message = new Swift_Message('INCOPERFIL | ' . $subject);

$body = incoSmtpGetBody($message, 'noreply', [ 'BODY' => $msgBody]);

$message->setFrom(INCO_EMAIL_NOREPLY)
    ->setTo($receiver['email'])
    ->setBody($body, 'text/html');

$mailer->send($message);

incoImapStoreMessage(INCO_EMAIL_NOREPLY, INCO_EMAIL_NOREPLY_PASSWD, INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);