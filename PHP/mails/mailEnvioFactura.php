<?php

// php mailEnvioFactura.php [uniqueid] [identificador] [cc] [coduser];

require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$identificador = $argv[2];
$cc = boolval($argv[3]);
$coduser = intval($argv[4]);

if (is_null($identificador) || empty($identificador) || is_null($cc) || is_null($coduser)) {
	incoLogWrite('Faltan argumentos');
}

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$comunicacion = pg_select($dbconn, 'comunicaciones', ['codigo' => 7]);
if ($comunicacion === false) incoLogWrite('La comunicación no existe');
$comunicacion = $comunicacion[0];

/*$result = pg_query_params($dbconn, 'SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', [$coduser]) or incoLogWrite('La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];
$usuario['nombreusuario'] = ucwords(mb_strtolower(trim($usuario['nombreusuario'])));
$usuario['email'] = trim($usuario['email']);
$usuario['passwdmail'] = trim($usuario['passwdmail']);
$usuario['movil'] = trim($usuario['movil']);*/
$senderUser = incoSmtpGetSenderUser($dbconn, $comunicacion, $coduser);

$result = pg_query_params($dbconn, 'select f.ejercicio, f.numero, f.cliente, c.nombre, c.email_factura, c.idioma from facturas f, clientes c where f.identificador = $1 and f.cliente = c.codigo', [$identificador]);
if (pg_num_rows($result) <= 0) incoLogWrite('No hay resultados [factura]: ' . pg_last_error());
$factura = pg_fetch_all($result)[0];
$factura['nombre'] = trim($factura['nombre']);
$factura['email_factura'] = trim($factura['email_factura']);
$factura['idioma'] = trim($factura['idioma']);
if (empty($factura['ejercicio']) || empty($factura['numero']) || empty($factura['idioma'])) incoLogWrite('Faltan datos [factura/cliente]: la factura tiene el ejercicio y/o el número vacío o el cliente no tiene idioma');
$receivers = incoSmtpGetReceiverUsers($dbconn, $comunicacion, $factura['cliente']);

$result = pg_query_params($dbconn, 'select distinct al.identificador_pedido from albaranes a, albaraneslin al where a.factura = $1 and a.identificador = al.identificador', [$identificador]);
if (pg_num_rows($result) <= 0) incoLogWrite('No hay resultados [pedidos]: ' . pg_last_error());
$pedidos = pg_fetch_all($result);
$pedidos = array_map(function($pedido) {
	return intval(substr($pedido['identificador_pedido'], -7));
}, $pedidos);
$pedidos = implode(', ', $pedidos);

$result = pg_query_params($dbconn, 'select identificador_oferta, referencia_obra from albaranes where factura = $1 limit 1', [$identificador]);
if (pg_num_rows($result) <= 0) incoLogWrite('No hay resultados [oferta]: ' . pg_last_error());
$oferta = pg_fetch_all($result)[0];
$oferta['numero'] = intval(substr($oferta['identificador_oferta'], -7));

// $mailer = incoSmtpGetMailer($senderUser['email'], $senderUser['passwdmail']);
$mailer = incoMailrelayGetMailer();

$pattSubject = 'INCOPERFIL | Factura Nº %s';
$pattMsgBody = 'Estimado cliente,<br><br>le adjuntamos la factura número %s en relación a:<br><ul><li>Oferta: %s</li><li>Pedido/s: %s</li><li>Referencia de Obra: %s</li></ul>$CC$<br><br>Saludos cordiales';

$pattFacturaPdf = 'F_%s_%s_%s.pdf'; // F_[ejercicio]_[numero]_[idioma].pdf
$pattPathFactura = INCO_DIR_CLIENTES . '%s/Facturas/%s';
$pattCertCalPdf = 'CC_%s_%s.pdf';
$pattPathCertCal = INCO_DIR_CLIENTES . '%s/Certificados/%s';

$facturaPdf = sprintf($pattFacturaPdf, $factura['ejercicio'], $factura['numero'], $factura['idioma']);
$pathFactura = sprintf($pattPathFactura, $factura['cliente'], $facturaPdf);
if (!file_exists($pathFactura)) {
	incoLogWrite('No existe el PDF de la factura ' . $factura['numero'] . ': ' . $pathFactura);
}

if ($cc) {
	$certCalPdf = sprintf($pattCertCalPdf, $factura['ejercicio'], $factura['numero']);
	$pathCertCal = sprintf($pattPathCertCal, $factura['cliente'], $certCalPdf);
	if (!file_exists($pathCertCal)) {
		incoLogWrite('No existe el PDF del certificado de calidad para la factura ' . $factura['numero']);
	}
}

$subject = str_replace(incoSmtpGetTemplateField('facturas.numero'), $factura['numero'], $comunicacion['email_subject']);
// $message = new Swift_Message(sprintf($pattSubject, $factura['numero']));
$message = new Swift_Message($subject);

// $msgbody = sprintf($pattMsgBody, $factura['numero'], $oferta['numero'], $pedidos, $oferta['referencia_obra']);
$msgbody = str_replace(
    [
        incoSmtpGetTemplateField('facturas.numero'),
        incoSmtpGetTemplateField('ofertas.numero'),
        incoSmtpGetTemplateField('pedidos.numero'),
        incoSmtpGetTemplateField('ofertas.referencia_obra'),
    ],
    [
        $factura['numero'],
        $oferta['numero'],
        $pedidos,
        $oferta['referencia_obra']
    ],
    $comunicacion['email_body']
);

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $senderUser['nombreusuario'],
    'MOVIL' => $senderUser['movil'],
    'MAIL' => $senderUser['email'],
]);
$body = str_replace('$CC$', ($cc ? '<br><br>Le adjuntamos también el certificado de calidad de las bobinas utilizadas.<br><br>' : ''), $body);

/*$message->setFrom($usuario['email'])
	->setTo($factura['email_factura'])
	->setBody($body, 'text/html');*/
$message->setFrom([$senderUser['email'] => $senderUser['nombreusuario']])
    ->setTo($receivers)
    ->setBcc($senderUser['email'])
    ->setBody($body, 'text/html');
$message->attach(Swift_Attachment::fromPath($pathFactura));
if ($cc) {
	$message->attach(Swift_Attachment::fromPath($pathCertCal));
}
if (!INCO_DEV_MODE && $comunicacion['tipo'] == 0) {
    if ($comunicacion['email_cc_interna']) $message->setCc($comunicacion['email_cc_interna']);
}

$mailer->send($message);

pg_update($dbconn, 'facturas', ['estado' => 3], ['identificador' => $identificador]);

// incoImapStoreMessage($usuario['email'], $usuario['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());
if (!INCO_DEV_MODE && $comunicacion['email_store']) incoImapStoreMessage($senderUser['email'], $senderUser['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);
