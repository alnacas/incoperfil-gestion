<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = trim($argv[2]);
if (empty($identificador)) {
	incoLogWrite('No hay argumentos');
}
$pdf = empty($argv[3]) ? '' : str_replace('\\', '/', $argv[3]);
$coduser = intval($argv[4]);
$msgbody = incoMailClearBody($argv[5]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos la oferta
$result = pg_query_params('SELECT fecha, proveedor, observaciones_proveedor FROM pedidos_proveedor WHERE identificador = $1 LIMIT 1', array($identificador)) or incoLogWrite( 'La consulta fallo [pedidos_proveedor]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_proveedor] No hay resultado');
$pedido = pg_fetch_all($result)[0];

$fecha = $pedido['fecha'];

$result = pg_query_params('SELECT nombre, contacto, emailcompra FROM proveedores WHERE codigo = $1 LIMIT 1', array($pedido['proveedor'])) or incoLogWrite( 'La consulta fallo [proveedores]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[proveedores] No hay resultado');
$proveedor = pg_fetch_all($result)[0];

$contacto = trim($proveedor['contacto']);
$email = trim($proveedor['emailcompra']);
if (!incoIsEmailValid($email)) {
    incoLogWrite('El email no es válido [proveedor]');
}

// obtenemos el usuario
$result = pg_query_params('SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', array($coduser)) or incoLogWrite( 'La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];

$userName = trim($usuario['nombreusuario']);
$userName = ucwords(mb_strtolower($userName));
$userEmail = trim($usuario['email']);
$userPasswd = trim($usuario['passwdmail']);
$userMovil = trim($usuario['movil']);

if (is_null($contacto) || empty($contacto)) {
    $contacto = trim($proveedor['nombre']);
}
$contacto = ucwords($contacto);

// $mailer = incoSmtpGetMailer($userEmail, $userPasswd);
$mailer = incoMailrelayGetMailer();

$message = new Swift_Message('INCOPERFIL | Pedido de material');

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $userName,
    'MOVIL' => $userMovil,
    'MAIL' => $userEmail,
]);

$message->setFrom([$userEmail => $userName])
    ->setTo([$email => $contacto])
    ->setBcc($userEmail)
    ->setBody($body, 'text/html');

if (!empty($pdf)) {
    $message->attach(Swift_Attachment::fromPath($pdf));
}

$mailer->send($message);

incoImapStoreMessage($userEmail, $userPasswd, INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);