<?php

// php mailTramitarReclamacionNC [uniqueid] [nc] [body]
require_once __DIR__ . '/../config.php';

incoLogSetFile($argv[1]);

$nc_codigo = intval($argv[2]);
if ($nc_codigo == null) {
    incoLogWrite('Faltan argumentos');
}
$msg_body = incoMailClearBody($argv[3]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$result = pg_query($dbconn, 'SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = 3 LIMIT 1') or incoLogWrite( 'La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$sender = pg_fetch_all($result)[0];
$sender['nombreusuario'] = ucwords(mb_strtolower(trim($sender['nombreusuario'])));
$sender['email'] = trim($sender['email']);
$sender['passwdmail'] = trim($sender['passwdmail']);
$sender['movil'] = trim($sender['movil']);

// obtenemos la nc
$result = pg_query_params($dbconn, 'SELECT codigo, emisor, proveedor FROM ncs WHERE codigo = $1', array($nc_codigo)) or incoLogWrite('La consulta fallo [ncs]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ncs] No hay resultado');
$nc = pg_fetch_all($result)[0];

$result = pg_query_params($dbconn, 'select ncsb.nc, ncsb.bobina, ncsb.ml_nc, b.pedido_proveedor, b.albaran_proveedor, b.fecha_albaran, b.numero_bulto from ncs_bobinas ncsb, bobinas b where ncsb.nc = $1 and ncsb.bobina = b.codigo order by b.fecha_albaran', [$nc_codigo]) or incoLogWrite('La consulta fallo [ncs bobinas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[ncs bobinas] No hay resultados');
$nc_bobinas = pg_fetch_all($result);

$result = pg_query_params($dbconn, 'SELECT codigo, nombre, email_nc FROM proveedores WHERE codigo = $1', [$nc['proveedor']]) or incoLogWrite('La consulta fallo [proveedor]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[proveedor] No hay resultado');
$proveedor = pg_fetch_all($result)[0];
$proveedor['email_nc'] = incoGetEmails($proveedor['email_nc']);
if (empty($proveedor['email_nc'])) {
    incoLogWrite('[proveedor] No tiene correos válidos para enviarle la NC');
}

$result = pg_query_params($dbconn, 'SELECT nombreusuario, trim(email) as email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', [$nc['emisor']]) or incoLogWrite( 'La consulta fallo [wusuario emisor]: ' . pg_last_error());
$emisor = pg_num_rows($result) == 0 ? [] : pg_fetch_all($result)[0];

$patt_bobinas = '<br><br><p style="margin-bottom: 10px;"><b>Listado de bobinas afectadas</b></p><table style="width: 100%; border-collapse: collapse; font-size: 11pt; text-align: center;"><thead style="border-bottom: solid 1px #00000050;"><tr><td>Bobina</td><td>ML Afectados</td><td>Pedido Proveedor</td><td>Albarán</td><td>Fecha Albarán</td><td>Nº Bulto</td></tr></thead><tbody>$ROWS$</tbody></table>';
$patt_row = '<tr><td>$BOB$</td><td>$ML_AFEC$</td><td>$PED_PROV$</td><td>$ALB$</td><td>$FECHA_ALB$</td><td>$N_BULTO$</td></tr>';

$rows = '';
foreach ($nc_bobinas as $nc_bobina) {
    $ml = number_format($nc_bobina['ml_nc'], 0, ',', '.');
    $ped_prov = substr($nc_bobina['pedido_proveedor'], -6);
    $fecha = implode('/', array_reverse(explode('-', $nc_bobina['fecha_albaran'])));
    $rows .= str_replace(
        ['$BOB$', '$ML_AFEC$', '$PED_PROV$', '$ALB$', '$FECHA_ALB$', '$N_BULTO$'],
        [$nc_bobina['bobina'], $ml, $ped_prov, $nc_bobina['albaran_proveedor'], $fecha, $nc_bobina['numero_bulto']],
        $patt_row
    );
}
$msg_body .= str_replace('$ROWS$', $rows, $patt_bobinas);
$msg_body .= '<p><small>Código: FCA-03-03 | Revisión: 1 | Jul-06</small></p>';

// $mailer = incoSmtpGetMailer($sender['email'], $sender['passwdmail']);
$mailer = incoMailrelayGetMailer();

$subject = 'Informe de Control de Productos No Conformes | RH-' . $nc_codigo;
$message = new Swift_Message($subject);

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msg_body,
    'NOMBRE' => $sender['nombreusuario'],
    'MOVIL' => $sender['movil'],
    'MAIL' => $sender['email'],
]);

$message->setFrom($sender['email'])
    ->setTo($proveedor['email_nc'])
    ->setBcc($sender['email'])
    ->setBody($body, 'text/html');

if ($nc['emisor'] != 3 && !empty($emisor)) {
    if (!incoIsEmailValid($emisor['email'])) incoLogWrite('El email del emisor no es válido: "' . $emisor['email'] . '"');
    $message->setCc($emisor['email']);
}

$path_nc = INCO_DIR_NCS . $nc_codigo;
if (is_dir($path_nc)) {
    foreach (glob($path_nc . '/*') as $img) {
        $message->attach(Swift_Attachment::fromPath($img));
    }
}

$mailer->send($message);

incoImapStoreMessage($sender['email'], $sender['passwdmail'], INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);