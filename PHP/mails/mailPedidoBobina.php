<?php

require_once __DIR__ . '/../config.php';

// $argv[0] is the script
incoLogSetFile($argv[1]);

$identificador = $argv[2];
if (is_null($identificador) || empty($identificador)) {
	incoLogWrite('Se han recibido todos los parámetros necesarios');
}
$pdf = empty($argv[3]) ? '' : str_replace('\\', '/', $argv[3]);
$coduser = intval($argv[4]);
$msgbody = incoMailClearBody($argv[5]);

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

// obtenemos la oferta
$result = pg_query_params('SELECT fecha, contacto, mail FROM pedidos_bobinas WHERE identificador = $1 LIMIT 1', [$identificador]) or incoLogWrite( 'La consulta fallo [pedidos_bobinas]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinas] No hay resultado');
$oferta = pg_fetch_all($result)[0];

$fecha = $oferta['fecha']; // date_format(date_create($oferta['fecha']), 'd/M/Y'); // 12 Noviembre 2015 --> j F Y
$contacto = trim($oferta['contacto']);
$emails = incoGetEmails(trim($oferta['mail']));
if (empty($emails)) {
	incoLogWrite('No se han encontrado emails válidos');
}

$result = pg_query_params('SELECT toneladas, descripcion_comercial, semana_prevista FROM pedidos_bobinaslin WHERE identificador = $1', [$identificador]) or incoLogWrite( 'La consulta fallo [pedidos_bobinaslin]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[pedidos_bobinaslin] No hay resultado');
$ofertalin = pg_fetch_all($result);

// obtenemos el usuario
$result = pg_query_params('SELECT nombreusuario, email, passwdmail, movil FROM wusuario WHERE codigo = $1 LIMIT 1', [$coduser]) or incoLogWrite( 'La consulta fallo [wusuario]: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('[wusuario] No hay resultado');
$usuario = pg_fetch_all($result)[0];
$userName = ucwords(mb_strtolower(trim($usuario['nombreusuario'])));
$userEmail = trim($usuario['email']);
$userPasswd = trim($usuario['passwdmail']);
$userMovil = trim($usuario['movil']);

if (is_null($contacto) || empty($contacto)) {
    $contacto = 'proveedor';
} else {
    $aux = explode(' ', $contacto);
    $contacto = '';
    for ($i = 0; $i < count($aux); $i++) {
        if ($i < count($aux) - 1) {
            $contacto .= strtoupper(substr($aux[$i], 0, 1)) . strtolower(substr($aux[$i], 1)) . ' ';
        } else {
            $contacto .= strtoupper(substr($aux[$i], 0, 1)) . strtolower(substr($aux[$i], 1));
        }
    }
}

// $mailer = incoSmtpGetMailer($userEmail, $userPasswd);
$mailer = incoMailrelayGetMailer();

$message = new Swift_Message('INCOPERFIL | Pedido de Bobina');

$body = incoSmtpGetBody($message, 'usuario', [
    'BODY' => $msgbody,
    'NOMBRE' => $userName,
    'MOVIL' => $userMovil,
    'MAIL' => $userEmail,
]);

$message->setFrom([$userEmail => $userName])
    ->setTo($emails)
    ->setBcc($userEmail)
    ->setBody($body, 'text/html');

if (!empty($pdf)) {
    $message->attach(Swift_Attachment::fromPath($pdf));
}

$mailer->send($message);

incoImapStoreMessage($userEmail, $userPasswd, INCO_IMAP_SENT_BOX, $message->toString());

pg_close($dbconn);