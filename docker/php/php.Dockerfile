FROM php:8.1-cli

ENV TZ=Europe/Madrid

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
      vim curl debconf git apt-transport-https apt-utils build-essential locales acl mailutils wget zip unzip gettext \
      gnupg gnupg1 gnupg2 libpq-dev iputils-ping libicu-dev libpng-dev libzip-dev libxml2-dev libzip-dev libpng-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install mysqli pgsql pdo pdo_pgsql zip intl gd gettext
RUN docker-php-ext-enable mysqli pgsql pdo pdo_pgsql zip intl gd gettext

RUN rm -rf /var/lib/apt/lists/*
RUN echo "es_ES.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
#    php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --filename=composer --install-dir=/usr/local/bin/ && \
    php -r "unlink('composer-setup.php');"

WORKDIR /gestion-php