#!/bin/bash
FILE="/home/gesic21/intercom/logacciones.csv";
if [ -f "$FILE" ]; then
  psql db_incoperfil -c "COPY (SELECT * FROM log_acciones ORDER BY fecha, hora) TO '$FILE' DELIMITER ';' CSV"
else
  psql db_incoperfil -c "COPY (SELECT * FROM log_acciones ORDER BY fecha, hora) TO '$FILE' DELIMITER ';' CSV HEADER"
fi
psql db_incoperfil -c "DELETE FROM log_acciones WHERE fecha < current_date"
