<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02/07/2018
 * Time: 10:50
 */

require_once '/home/gesic21/PHP/config.php';

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

pg_prepare($dbconn, 'upd_ubicacion', 'UPDATE bobinas SET ubicacion = $1, almacen = $2, pasillo = $3, cuna = $4, nivel = $5, estado = $6 WHERE codigo = $7');

$dir = '/home/gesic21/intercom/hnyw/';
$dirread = $dir . 'readed/';
$extension = 'ebt';
$separador = ';';

if (!is_dir($dirread)) {
    $oldmask = umask(0);
    mkdir($dirread, 0755, true);
    umask($oldmask);
}

if ($folder = opendir($dir)) {
    while (($file = readdir($folder)) !== false) {
        if (filetype($dir . $file) == 'dir') {
            continue;
        }

        $aux = explode('.', $file);
        $name = $aux[0];
        $ext = strtolower($aux[1]);
        if ($ext != $extension) {
            continue;
        }

        $content = trim(file_get_contents($dir . $file));
        $lines = explode(PHP_EOL, $content);
        for ($i = 0; $i < count($lines); $i++) {
            $fields = explode($separador, $lines[$i]);
            $bobina = $fields[1];
            $ubicacion = $fields[2];

            $almacen = intval(substr($ubicacion, 0, 1));
            switch ($almacen) {
                case 1: $almacen = 'A'; break;
                case 2: $almacen = 'B'; break;
                case 3: $almacen = 'C'; break;
                default: $almacen = 'E'; break;
            }
            $pasillo = substr($ubicacion, 1, 1);
            $cuna = substr($ubicacion, 2, 2);
            $nivel = substr($ubicacion, 4, 1);

            $ubicacion = implode('', [$almacen, $pasillo, $cuna, $nivel]);

            $bobinasDb = pg_select($dbconn, 'bobinas', ['codigo' => $bobina])[0];
            $estado = $bobinasDb['reserva_oferta'] ? 2 : 1;

            pg_execute($dbconn, 'upd_ubicacion', [$ubicacion, $almacen, $pasillo, $cuna, $nivel, $estado, $bobina]);
        }

        rename($dir . $file, $dirread . $file);
    }

    closedir($folder);
}