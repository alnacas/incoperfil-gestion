<?php

require __DIR__ . '/../../PHP/vendor/autoload.php';
require __DIR__ . '/../../PHP/config.php';

$fecha = new DateTime('yesterday');
$fechaDOW = intval($fecha->format('N'));

$dbconn = incoDbGetConnection(INCO_DB_LOCAL);

$sql = 'select trim(nomape) as nomape, trim(grupo) as grupo from empleados where codigo = $1';
pg_prepare($dbconn, 'empleado', $sql);

$sql = 'select empleado, manana_entra, manana_sale, tarde_entra, tarde_sale, horas_efectivas, horas_teoricas, horas_extras, total_jornada, horas from historico_tiempos where fecha = $1 order by empleado';
$result = pg_query_params($dbconn, $sql, array($fecha->format('d/m/Y'))) or incoLogWrite('La consulta fallo: ' . pg_last_error());
if (pg_num_rows($result) <= 0) incoLogWrite('No hay empleados');
$his_tiempos = pg_fetch_all($result);

$msgBody = '';

foreach ($his_tiempos as $his_tiempo) {
    $result = pg_execute($dbconn, 'empleado', array($his_tiempo['empleado']));
    if (pg_num_rows($result) <= 0) {
        incoLogWrite('No existe el empleado ' . $his_tiempo['empleado'], false);
        continue;
    }
    $empleado = pg_fetch_all($result)[0];

    if ($fechaDOW == 5 || $empleado['grupo'] == 'OF3') {
        if (is_null($his_tiempo['manana_entra']) || is_null($his_tiempo['manana_sale'])) {
            $msgBody .= '<p>' . $empleado['nomape'] . ' no tiene entrada/salida</p>';
        }
    } else {
        if (is_null($his_tiempo['manana_entra']) || is_null($his_tiempo['manana_sale']) || is_null($his_tiempo['tarde_entra']) || is_null($his_tiempo['tarde_sale'])) {
            $msgBody .= '<p>' . $empleado['nomape'] . ' no tiene entrada/salida</p>';
        }
    }

    if ($his_tiempo['horas_extras'] > 0) {
        $msgBody .= '<p>' . $empleado['nomape'] . ' tiene ' . $his_tiempo['horas_extras'] . '  horas extras</p>';
    }
}

if (!empty($msgBody)) {
    $mailer = incoSmtpGetMailer(INCO_MAIL_USER_NOREPLY['email'], INCO_MAIL_USER_NOREPLY['password']);

    $message = new Swift_Message('INCOPERFIL | Error en tiempos dia ' . $fecha->format('d-m-Y'));

    $body = incoSmtpGetBody($message, 'noreply', ['BODY' => $msgBody]);

    $message->setFrom(INCO_MAIL_USER_NOREPLY['email'])
        ->setTo(['joseginer@incoperfil.com', 'produccion@incoperfil.com'])
//        ->setBcc('albertonavarrocastellar@outlook.com')
        ->setBody($body, 'text/html');

    $mailer->send($message);

    incoImapStoreMessage(INCO_MAIL_USER_NOREPLY['email'], INCO_MAIL_USER_NOREPLY['password'], INCO_MAIL_IMAP['sent_box'], $message->toString());
}