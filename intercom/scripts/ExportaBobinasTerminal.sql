﻿copy (select codigo,(select nombre from perforaciones where codigo = bobinas.perforacion),
((select abreviatura from calidades where codigo = bobinas.calidad) || ' - ' || (select abreviatura from recubrimientos where codigo = bobinas.recubrimiento)),
((select abreviatura from pinturas where codigo = bobinas.pintura) || ' - ' || (select abreviatura from colores where codigo = bobinas.ccolor)),
(select valor from espesores where codigo = bobinas.espesor),peso_albaran::integer,peso_actual::integer,ubicacion
from bobinas where estado = '1')
to
'/home/gesic21/intercom/hnyw/bobinas.tli' with delimiter '|';