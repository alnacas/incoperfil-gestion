echo "--> Configuración para Incoperfil..."
#awk '{gsub(/192.168.0.100/,"192.168.0.12")}1' Config.ini > temp.ini && mv temp.ini Config.ini
#awk '{gsub(/cosmos\/Incoperfil/,"home/gesic21")}1' Config.ini > temp.ini && mv temp.ini Config.ini
#awk '{gsub(/cosmos\\Incoperfil/,"gesic21")}1' Config.ini > temp.ini && mv temp.ini Config.ini
#awk '{gsub(/192.168.0.100/,"192.168.0.12")}1' PHP/config.php > PHP/temp.php && mv PHP/temp.php PHP/config.php
#awk '{gsub(/cosmos\/Incoperfil/,"home/gesic21")}1' PHP/config.php > PHP/temp.php && mv PHP/temp.php PHP/config.php

sed -i "s/APP_ENV=dev/APP_ENV=prod/g" PHP/.env
vcs=$(date +"v%y.%m.%d")
echo "--> Actualizando a la version: $vcs..."
sed -i "s/@version@/$vcs/g" PHP/import.sql
echo "update empresa set vcs = '$vcs' where codigo = 1;" >> PHP/import.sql

echo "--> Actulizando el log de sql..."
echo "-- **** $vcs **** --" >> PHP/import-log.sql
cat PHP/import.sql >> PHP/import-log.sql
