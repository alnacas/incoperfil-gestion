INSERT INTO ttCobros 
SELECT e.efecto, e.orden, e.femision, e.fvto, e.cliente, c.nombre, 0 AS riesgo, 0 AS cred_nume, 0 AS riesgogcn, '01/01/2018' AS fvalidezgcn, 
	(e.importe - e.cobrado), e.acepto, e.estado, e.tipo_efec, e.cobrador, e.nrgto 
FROM efectos e, clientes c, facturas f 
WHERE c.codigo = e.cliente AND e.identif_fra = f.identificador AND e.tipo_efec = 'P' AND (e.acepto <> 'S' OR e.acepto is NULL) AND 
	e.efecto > 0 AND cobrado <> importe;

INSERT INTO ttCobros
SELECT e.efecto, e.orden, e.femision, e.fvto, e.cliente, c.nombre, 0 AS riesgo, 0 AS cred_nume, 0 AS riesgogcn, '01/01/2018' AS fvalidezgcn,
	(e.importe - e.cobrado), e.acepto, e.estado, 'D', e.cobrador, e.nrgto
FROM efectos e,clientes c, facturas f
WHERE c.codigo = e.cliente AND e.efecto = f.numero AND e.cliente = f.cliente AND e.devuelto = 'S' AND e.efecto > 0 AND cobrado <> importe;

INSERT INTO ttCobros
SELECT e.efecto, e.orden, e.femision, e.fvto, e.cliente, c.nombre, 0 AS riesgo, 0 AS cred_nume, 0 AS riesgogcn, '01/01/2018' AS fvalidezgcn,
	(e.importe - e.cobrado), e.acepto, e.estado, e.tipo_efec, e.cobrador, e.nrgto
FROM efectos e, clientes c, facturas f
WHERE c.codigo = e.cliente AND e.identif_fra = f.identificador AND e.tipo_efec = 'R' AND e.cliente <> 1202 AND e.cliente <> 568 AND e.cliente <> 1276 AND 
	(e.acepto <> 'S' OR e.acepto is NULL) AND e.efecto > 0 AND cobrado <> importe;

INSERT INTO ttCobros
SELECT e.efecto, e.orden, e.femision, e.fvto, e.cliente, c.nombre, 0 AS riesgo, 0 AS cred_nume, 0 AS riesgogcn, '01/01/2018' AS fvalidezgcn,
	(e.importe - e.cobrado), e.acepto, e.estado, e.tipo_efec, e.cobrador, e.nrgto
FROM efectos e, clientes c
WHERE c.codigo = e.cliente AND e.cliente <> 1202 AND e.cliente <> 568 AND e.cliente <> 1276 AND (e.acepto = 'S' AND e.devuelto = 'S') AND e.efecto = 0 AND 
	cobrado <> importe;