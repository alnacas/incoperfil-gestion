echo "--> Configuración para desarrollo..."
#awk '{gsub(/192.168.0.12/,"192.168.0.100")}1' Config.ini > temp.ini && mv temp.ini Config.ini
#awk '{gsub(/home\/gesic21/,"cosmos/Incoperfil")}1' Config.ini > temp.ini && mv temp.ini Config.ini
#awk '{gsub(/gesic21/,"cosmos\\Incoperfil")}1' Config.ini > temp.ini && mv temp.ini Config.ini
#awk '{gsub(/192.168.0.12/,"192.168.0.100")}1' PHP/config.php > PHP/temp.php && mv PHP/temp.php PHP/config.php
#awk '{gsub(/home\/gesic21/,"cosmos/Incoperfil")}1' PHP/config.php > PHP/temp.php && mv PHP/temp.php PHP/config.php

sed -i "s/APP_ENV=prod/APP_ENV=dev/g" PHP/.env

echo "--> Limpiando importador de cambios para BD..."
cat /dev/null > PHP/import.sql

echo "--> Limpiando importador de cambios con ayuda de PHP..."
cat /dev/null > PHP/import.php
